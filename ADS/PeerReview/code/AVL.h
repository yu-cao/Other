#ifndef AVL_H
#define AVL_H
#include<iostream>
typedef struct AVLNode *AVLTree;

struct AVLNode {
	int Element;
	int Height;
	struct AVLNode *Left;
	struct AVLNode *Right;
};

//In this function, child K1 is the left child of its father K2(input root)
//and after rotation, K1 become the root and K2 become the right child of K1
//and finally return the root(K1) of tree
AVLTree SingleLeft(AVLTree K2);

//In this function, child K1 is the right child of its father K2(input root)
//and after rotation, K1 become the root and K2 become the left child of K1
//and finally return the root(K1) of tree
AVLTree SingleRight(AVLTree K2);

//In this situation, K2 is right child of its father K1
//and K1 is the left child of its father K3(which is also K2's grandfather) 
//finally return the K2 which become the root after rotation
AVLTree DoubleLeft(AVLTree K3);

//In this situation, K2 is left child of its father K1
//and K1 is the right child of its father K3(which is also K2's grandfather) 
//finally return the K2 which become the root after rotation
AVLTree DoubleRight(AVLTree K3);

//Insert an element in AVL Tree
//Input is the root of the tree and the element
//Output is the root of the tree after insert and adjust
AVLTree InsertAVL(int X, AVLTree Tree);

//This is the function of delete operation
//The input is the root of the tree and the element of a node which want to be deleted
//The output is the root of the tree after the delete the ordered node
//Nomarlly, empty tree which is in the test is impossible and will throw a error
AVLTree DeleteAVL(int X, AVLTree Tree);

//return the Height of a AVLNode
int Height(struct AVLNode *P);

//Find the Node which element is minimum of the whole tree which input this function
AVLTree FindMinAVL(AVLTree Tree);

#endif