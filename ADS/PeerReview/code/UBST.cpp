#include"UBST.h"
//Some of Code are referred by textbook
//The interface usage is shown in UBST.h

NodeUBST* Insert(int X, NodeUBST* T)
{
	//If input T become the empty tree, we should malloc to store it.
	if (T == NULL)
	{
		T = (NodeUBST*)malloc(sizeof(struct NodeUBST));
		//memory is too full to store any data
		if (T == NULL)
		{
			printf("Out of Space��");
			exit(1);
		}
		T->Element = X;
		T->Left = T->Right = NULL;
	}

	//Search the suitable position and insert the X in recursion way
	else if (X < T->Element)
		T->Left = Insert(X, T->Left);
	else if (X > T->Element)
		T->Right = Insert(X, T->Right);

	return T;
}

NodeUBST* FindMin(NodeUBST *T)
{
	if (T == NULL) return NULL;
	else if (T->Left == NULL) return T;
	else return FindMin(T->Left);
}

NodeUBST* Delete(int X, NodeUBST* T)
{
	NodeUBST* Temp;

	//Exception Handling
	if (T == NULL)
	{
		printf("Element Not Find");
		exit(1);
	}

	//Find and delete the X in recursion way
	if (X < T->Element)
	{
		T->Left = Delete(X, T->Left);
	}
	else if (X > T->Element)
	{
		T->Right = Delete(X, T->Right);
	}

	//T has two children and it adjust the node to make tree still maintain the property of BST
	else if (T->Left&&T->Right)
	{
		Temp = FindMin(T->Right);
		T->Element = Temp->Element;
		T->Right = Delete(T->Element, T->Right);
	}

	//T only has one or zero child
	//The purpose is that delete this node and this node position is replaced by its child
	else
	{
		Temp = T;
		if (T->Left == NULL)
			T = T->Right;
		else if (T->Right == NULL)
			T = T->Left;
		free(Temp);
	}

	return T;
}
