//Some of Code are referred by textbook in P121-123
//The interface usage is shown in AVL.h
#include"AVL.h"

int Height(struct AVLNode *P)
{
	if (P == NULL)
		return -1;
	return P->Height;
}

AVLTree FindMinAVL(struct AVLNode *Tree)
{
	if (Tree == NULL)
		return NULL;
	if (Tree->Left == NULL)
		return Tree;
	FindMinAVL(Tree->Left);
}

AVLTree InsertAVL(int X, struct AVLNode *Tree)
{
	//If this tree is NULL, add a node to insert.
	if (Tree == NULL) {
		Tree = (AVLTree)malloc(sizeof(struct AVLNode));
		Tree->Element = X;
		Tree->Height = 0;
		Tree->Left = Tree->Right = NULL;
	}
	//If X is lower than the Element of Node, it is obvious that X is in the left sub-tree of this node
	else if (X < Tree->Element) {
		//So insert function will choose the left sub-tree to try in recursion
		Tree->Left = InsertAVL(X, Tree->Left);
		//If this tree is not balanced.
		if (Height(Tree->Left) - Height(Tree->Right) >= 2) {
			if (X < Tree->Left->Element)
				//It becomes a single line which father is the left child of Grandfather
				//and this X is also the left child of its father
				Tree = SingleLeft(Tree);
			else
				//It becomes a zig line which father is the left child of Grandfather
				//but this X is the right child of its father
				Tree = DoubleLeft(Tree);
		}
	}
	//If X is larger than the Element of Node, it is obvious that X is in the right sub-tree of this node	
	else if (X > Tree->Element) {
		//Insert function will choose the right sub-tree to try in recursion
		Tree->Right = InsertAVL(X, Tree->Right);
		//If this tree is not balanced
		if (Height(Tree->Right) - Height(Tree->Left) >= 2) {
			if (X > Tree->Right->Element)
				//It becomes a single line which father is the right child of Grandfather
				//and this X is also the right child of its father			
				Tree = SingleRight(Tree);
			else
				//It becomes a zig line which father is the left child of Grandfather
				//but this X is the left child of its father
				Tree = DoubleRight(Tree);
		}
	}
	//Refresh the Node of path which X insert
	Tree->Height = __max(Height(Tree->Left), Height(Tree->Right)) + 1;
	return Tree;
}

AVLTree SingleLeft(struct AVLNode *K2)
{
	struct AVLNode *K1;
	K1 = K2->Left;
	K2->Left = K1->Right;
	K1->Right = K2;

	//Refresh Height element is significant to control the loop invariant
	K2->Height = __max(Height(K2->Left), Height(K2->Right)) + 1;
	K1->Height = __max(Height(K1->Left), Height(K1->Right)) + 1;

	return K1;
}

struct AVLNode *SingleRight(struct AVLNode *K2)
{
	struct AVLNode *K1;
	K1 = K2->Right;
	K2->Right = K1->Left;
	K1->Left = K2;

	//Refresh Height element is significant to control the loop invariant
	K2->Height = __max(Height(K2->Left), Height(K2->Right)) + 1;
	K1->Height = __max(Height(K1->Left), Height(K1->Right)) + 1;

	return K1;
}

struct AVLNode *DoubleLeft(struct AVLNode *K3)
{
	//Rotation between K1 and K2
	K3->Left = SingleRight(K3->Left);
	//Rotation between K3 and K2
	return SingleLeft(K3);
}

struct AVLNode *DoubleRight(struct AVLNode *K3)
{
	K3->Right = SingleLeft(K3->Right);
	return SingleRight(K3);
}

AVLTree DeleteAVL(int X, struct AVLNode *Tree)
{
	struct AVLNode *temp;
	//Exception Handling
	if (Tree == NULL)
	{
		printf("ERROR:The tree is NULL!\n");
		exit(1);
	}

	if (X < Tree->Element)
		//If the X is lower than the element of this Node, we must find the X in this left sub-tree in recursion 
		Tree->Left = DeleteAVL(X, Tree->Left);
	else if (X > Tree->Element)
		//If the X is larger than the element of this Node, we must find the X in the right sub-tree in recursion
		Tree->Right = DeleteAVL(X, Tree->Right);

	//Now the Node which element equal X has been found

	//This Node have two children
	else if (Tree->Left != NULL && Tree->Right != NULL) {
		//Try to make the min element Node of right sub-tree to replace the node(like ordinary BST)
		temp = FindMinAVL(Tree->Right);
		Tree->Element = temp->Element;
		Tree->Right = DeleteAVL(Tree->Element, Tree->Right);//Adjust the right sub-tree and still make the whole tree be balanced
	}
	else {
		//This Node has one or zero children
		//The purpose is that delete this node and this node position is replaced by its child
		temp = Tree;
		if (Tree->Left == NULL)
			Tree = Tree->Right;
		else if (Tree->Right == NULL)
			Tree = Tree->Left;
		free(temp);
	}
	return Tree;
}