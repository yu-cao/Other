//This .cpp is test the time cost
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "SplayTree.h"
#include"AVL.h"
#include"UBST.h"

clock_t start, stop;
double duration;

int *ins, *del;

void swap(int *i, int *j)
{
	int temp = *i;
	*i = *j;
	*j = temp;
}
//Reference: Fisher Yates shuffle 
int* RandInts(int N, int *a)
{
	for (int i = 0; i < N; i++)
		a[i] = i;
	for (int i = 0; i < N; i++)
	{
		swap(&(a[i]), &(a[rand() % N]));
	}
	return a;
}

int main()
{
	int n;//number to test
	AVLNode* a = NULL;
	Node* s = NULL;
	NodeUBST* u = NULL;
	scanf("%d", &n);

	printf("The first case:array is increasing.\n");
	printf("The second case:array is decreasing.\n");
	printf("The third case:array is random.\n");

	for (int count = 0; count < 3; count++)
	{
		printf("\nThe %d test\n", count + 1);//Show which the test case
		ins = (int*)malloc(sizeof(int) * n);//insert array
		del = (int*)malloc(sizeof(int) * n);//delete array
		if (count == 0)
		{
			for (int i = 0; i < n; i++)
			{
				ins[i] = i;
				del[i] = i;
			}
		}
		else if (count == 1)
		{
			for (int i = 0; i < n; i++)
			{
				ins[i] = i;
				del[i] = n - 1 - i;
			}
		}
		else if (count == 2)
		{
			ins = RandInts(n, ins);
			del = RandInts(n, del);
		}

		//AVL 
		//test the insert
		printf("AVL Insert:\t");
		start = clock();

		for (int ri = 0; ri < n; ri++) {
			a = InsertAVL(ins[ri], a);
		}
		stop = clock();
		duration = ((double)(stop - start)) / CLK_TCK;
		printf("%f ", duration);

		//test the delete
		printf("AVL Delete:\t");
		start = clock();
		for (int ri = 0; ri < n; ri++) {
			a = DeleteAVL(del[ri], a);
		}
		stop = clock();
		duration = ((double)(stop - start)) / CLK_TCK;
		printf("%f\n", duration);

		//Splay
		//test the insert
		printf("Splay Insert:\t");
		start = clock();
		for (int ri = 0; ri < n; ri++)
		{
			s = InsertSplay(ins[ri], s);
		}
		stop = clock();
		duration = ((double)(stop - start)) / CLK_TCK;
		printf("%f ", duration);
		//test the delete
		printf("Splay Delete:\t");
		start = clock();
		for (int ri = 0; ri < n; ri++)
		{
			s = DeleteSplay(del[ri], s);
		}
		stop = clock();
		duration = ((double)(stop - start)) / CLK_TCK;
		printf("%f\n", duration);

		//UBST
		//test the insert
		printf("UBST Insert:\t");
		start = clock();
		for (int ri = 0; ri < n; ri++)
		{
			u = Insert(ins[ri], u);
		}
		stop = clock();
		duration = ((double)(stop - start)) / CLK_TCK;
		printf("%f ", duration);
		//test the delete
		printf("UBST Delete:\t");
		start = clock();
		for (int ri = 0; ri < n; ri++)
		{
			u = Delete(del[ri], u);
		}
		stop = clock();
		duration = ((double)(stop - start)) / CLK_TCK;
		printf("%f\n", duration);

		free(ins);
		free(del);
	}

	return 0;
}