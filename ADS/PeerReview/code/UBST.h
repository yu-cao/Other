#ifndef UBST_H
#define UBST_H
#include<iostream>
struct NodeUBST
{
	int Element;
	NodeUBST* Left;
	NodeUBST* Right;
};

//Insert an element in UBST
//Input is the root of the tree and the element
//Output is the root of the tree after insert
NodeUBST* Insert(int X, NodeUBST* T);

//Find the Node which element is minimum of the whole tree which is input this function
NodeUBST* FindMin(NodeUBST *T);

//This is the function of delete operation
//The input is the root of the tree and the element of a node which want to be deleted
//The output is the root of the tree after the delete the ordered node
//Nomarlly, empty tree which is in the test is impossible and will throw a error
NodeUBST* Delete(int X, NodeUBST* T);

#endif
