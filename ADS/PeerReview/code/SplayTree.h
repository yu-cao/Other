#ifndef STT_H
#define STT_H
#include <iostream>
struct Node
{
	int Element;
	Node *Parent;
	Node *Left;
	Node *Right;
};

//This function is input a Node which will be change
//finally this Node will be the root of the whole tree and return this root
//in the meantime, the whole transformation obey the rule of splay tree
Node *Splay(Node *T);

//Insert an element in Splay Tree
//Input is the root of the tree and the element
//Output is the root of the tree after insert and adjust
Node *InsertSplay(int X, Node *T);

//This is the function of delete operation
//The input is the root of the tree and the element of a node which want to be deleted
//The output is the root of the tree after the delete the ordered node
//Nomarlly, empty tree which is in the test is impossible and will throw a error
Node *DeleteSplay(int X, Node *T);

//A simple Find operation in BST(not specialize in SplayTree)
//The inplement is using property of BST
Node *Find(int X, Node *T);

//Find the Node which element is maximum of the whole tree which is input this function
//The inplement is also using property of BST
Node *FindMax(Node *T);

//Node X rotates clockwise around P(X)
Node *RightRotation(Node *X);

//Node X revolves counterclockwise around P(X)
Node *LeftRotation(Node *X);

//Get the T's father node and the grandfather node
Node *P(Node *T);
Node *G(Node *T);

#endif