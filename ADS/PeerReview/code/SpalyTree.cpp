#include "SplayTree.h"
//The interface usage is shown in SplayTree.h

//Get its the father Node of input node
Node *P(Node *T)
{
	return T->Parent;
}

//Get its the grandfather Node of input node
Node *G(Node *T)
{
	return T->Parent->Parent;
}

Node *Find(int X, Node *T)
{
	if (T == NULL)
		return NULL;
	for (;;)
	{
		if (X > T->Element)
			T = T->Right;
		else if (X < T->Element)
			T = T->Left;
		else if (X == T->Element)
			return T;
	}
}

Node *FindMax(Node *T)
{
	if (T == NULL)
		return NULL;
	if (T->Right == NULL)
		return T;
	FindMax(T->Right);
}

Node *RightRotation(Node *X)
{
	//Judge whether the grandfather of X exist or not
	//and Parent of X is either the left or right child of grandfather
	//If exist, make G(X)'s point to the X and maintain the tree property
	if (G(X) != NULL && G(X)->Left == P(X))
	{
		G(X)->Left = X;
	}
	else if (G(X) != NULL && G(X)->Right == P(X))
	{
		G(X)->Right = X;
	}

	//Make the rotation operation,be cautious that the X's child may be NULL
	//So it is necessary to add a 'if' to fix it.
	P(X)->Left = X->Right;
	if (X->Right != NULL)
	{
		X->Right->Parent = P(X);
	}
	X->Right = P(X);
	X->Parent = P(X)->Parent;
	X->Right->Parent = X;
	return X;
}

//The commeit is similar with "RightRotation". Besides, it is easy for you to
//understand by the graph in report, so don't commit in it again
Node *LeftRotation(Node *X)
{
	if (G(X) != NULL && G(X)->Left == P(X))
	{
		G(X)->Left = X;
	}
	else if (G(X) != NULL && G(X)->Right == P(X))
	{
		G(X)->Right = X;
	}

	P(X)->Right = X->Left;
	if (X->Left != NULL)
	{
		X->Left->Parent = P(X);
	}
	X->Left = P(X);
	X->Parent = P(X)->Parent;
	X->Left->Parent = X;
	return X;
}


Node *Splay(Node *T)
{
	if (T == NULL) return T;
	//T is not become the root of whole tree
	while (P(T) != NULL)
	{
		if (T == P(T)->Left)
		{
			//case 1:P(T) is the root of the whole tree and T is the left child of P(T)
			if (G(T) == NULL)
			{
				//T rotates clockwise around P(T)
				T = RightRotation(T);
			}
			//case 2:T is the left child of P(T) and P(T) is also the left child of G(T)
			//and these arrangement become a single line 
			//so it should rotate twice to make T become the root of this three node
			else if (P(T) == G(T)->Left)
			{
				//P(T) rotates clockwise around G(T)
				T->Parent = RightRotation(P(T));
				//T rotates clockwise around P(T)
				T = RightRotation(T);
			}
			//case 3:T is the left child of P(T) but P(T) is the right child of G(T)
			//and these arrangement become a zig-zag line
			else
			{
				//T rotates clockwise around P(T)
				T = RightRotation(T);
				//T revolves counterclockwise around P(T)
				/*Note: this rotation is running after the first rotation
				 *In fact, P(T) is not the simple P(T), which is the G(T) of T in initial condition
				 *because T is now between the previous(G(T) and P(T)) and then This will another rotation*/
				T = LeftRotation(T);
			}
		}
		else if (T == P(T)->Right)
		{
			//case 4:P(T) is the root of the whole tree and T is the right child of P(T)
			if (G(T) == NULL)
			{
				//T revolves counterclockwise around P(T)
				T = LeftRotation(T);
			}
			//case 5:T is the right child of P(T) and P(T) is also the right child of G(T)
			//and these arrangement become a single line 
			//so it should rotate twice to make T become the root of this three node
			else if (P(T) == G(T)->Right)
			{
				//P(T) revolves counterclockwise around G(T)
				T->Parent = LeftRotation(P(T));
				//T revolves counterclockwise around P(T)
				T = LeftRotation(T);
			}
			//case 6:T is the right child of P(T) but P(T) is the left child of G(T)
			//and these arrangement become a zig-zag line
			else
			{
				//T revolves counterclockwise around P(T)
				T = LeftRotation(T);
				//T rotates clockwise around P(T)
				//Note:This is the same reason of case 3,but the operation way is just opposite
				T = RightRotation(T);
			}
		}
	}
	return T;
}

Node *InsertSplay(int X, Node *T)
{
	//When T is NULL, we must build it to store the element.
	if (T == NULL)
	{
		Node *Root = (Node *)malloc(sizeof(Node));
		Root->Element = X;
		Root->Parent = T;
		Root->Left = Root->Right = NULL;
		return Root;
	}

	for (;;)
	{
		//X is larger than element of T,it means X should be the right sub-tree of the T
		if (X > T->Element)
		{
			//this tree isn't have a right sub-tree, so we must build one to add and make it obey the property of Splay Tree
			if (T->Right == NULL)
			{
				Node *p = (Node *)malloc(sizeof(Node));
				p->Element = X;
				p->Parent = T;
				p->Left = p->Right = NULL;
				T->Right = p;
				return Splay(p);
			}
			//else search the right sub-tree in loop way
			T = T->Right;
		}
		//X is lower than element of T,it means X should be the left sub-tree of the T
		else if (X < T->Element)
		{
			//this tree isn't have a left sub-tree, so we must build one to add and make it obey the property of Splay Tree
			if (T->Left == NULL)
			{
				Node *p = (Node *)malloc(sizeof(Node));
				p->Element = X;
				p->Parent = T;
				p->Left = p->Right = NULL;
				T->Left = p;
				return Splay(p);
			}
			//else search the left sub-tree in loop way
			T = T->Left;
		}
	}
}

Node *DeleteSplay(int X, Node *T)
{
	//Exception Handling
	if (T == NULL)
	{
		printf("ERROR:The tree is empty");
		exit(1);
	}

	//Find the Node which will be delete and move it to the root in Splay Tree order.
	Node *Root = Find(X, T);
	Root = Splay(Root);

	//Delete the root
	Node *L = Root->Left;
	if (L != NULL)	L->Parent = NULL;
	Node *R = Root->Right;
	if (R != NULL) R->Parent = NULL;

	free(Root);

	//Find the max-key node of the root of left sub-tree and move it to the root
	//next, make this new root right sub-tree become the old root right sub-tree
	Node *NewRoot = FindMax(L);

	//If the left sub-tree is empty, it should be replaced by the root of right sub-tree directly.
	if (NewRoot == NULL) {
		NewRoot = R;
	}
	else
	{
		NewRoot = Splay(NewRoot);
		NewRoot->Right = R;
		if (R != NULL)
		{
			R->Parent = NewRoot;
		}
	}

	return NewRoot;
}
