#include<stdio.h>
#include<stdlib.h>
//This Code is mainly refer by textbook in P121-123
typedef struct AVLNode *AVLTree;

struct AVLNode {
	int Element;
	int Height;
	struct AVLNode *Left;
	struct AVLNode *Right;
};

AVLTree SingleLeft(AVLTree Tree);
AVLTree SingleRight(AVLTree Tree);
AVLTree DoulbeLeft(AVLTree Tree);
AVLTree DoulbeRight(AVLTree Tree);
AVLTree FindMinAVL(AVLTree Tree);
AVLTree InsertAVL(int X, AVLTree Tree);
AVLTree DeleteAVL(int X, AVLTree Tree);
int Height(struct AVLNode *P);

//return the Height of a AVLNode
int Height(struct AVLNode *P) {
	if (P == NULL)
		return -1;
	else
		return P->Height;
}

//Insert an element in AVL Tree
AVLTree InsertAVL(int X, struct AVLNode *Tree) {
	//If this tree is NULL, add a node to insert.
	if (Tree == NULL) {
		Tree = (AVLTree) malloc(sizeof(struct AVLNode));
		Tree->Element = X;
		Tree->Height = 0;
		Tree->Left = Tree->Right = NULL;
	}
	//If X is lower than the Element of Node, it is obvious that X is in the left sub-tree of this node
	else if (X < Tree->Element) {
		//So insert function will choose the left sub-tree to try in recursion
		Tree->Left = InsertAVL(X, Tree->Left);
		//If this tree is not balanced.
		if (Height(Tree->Left) - Height(Tree->Right) >= 2) {
			if (X < Tree->Left->Element)
				//It becomes a single line which father is the left child of Grandfather
				//and this X is also the left child of its father
				Tree = SingleLeft(Tree);
			else
				//It becomes a zig line which father is the left child of Grandfather
				//but this X is the right child of its father
				Tree = DoulbeLeft(Tree);
		}
	}
	//If X is larger than the Element of Node, it is obvious that X is in the right sub-tree of this node	
	else if (X > Tree->Element) {
		//Insert function will choose the right sub-tree to try in recursion
		Tree->Right = InsertAVL(X, Tree->Right);
		//If this tree is not balanced
		if (Height(Tree->Right) - Height(Tree->Left) >= 2) {
			if (X > Tree->Right->Element)
				//It becomes a single line which father is the right child of Grandfather
				//and this X is also the right child of its father			
				Tree = SingleRight(Tree);
			else
				//It becomes a zig line which father is the left child of Grandfather
				//but this X is the left child of its father
				Tree = DoulbeRight(Tree);
		}
	}
	//Refresh the Node of path which X insert
	Tree->Height = __max(Height(Tree->Left), Height(Tree->Right)) + 1;
	return Tree;
}

//In this function, child K1 is the left child of its father K1
//and after rotation, K1 become the root and K2 become the right child of K1
//and finally return the root of tree
AVLTree SingleLeft(struct AVLNode *K2) {
	struct AVLNode *K1;
	K1 = K2->Left;
	K2->Left = K1->Right;
	K1->Right = K2;

	//Refresh Height element is significant to control the loop invariant
	K2->Height = __max(Height(K2->Left), Height(K2->Right)) + 1;
	K1->Height = __max(Height(K1->Left), Height(K1->Right)) + 1;

	return K1;
}

//In this function, child K1 is the right child of its father K1
//and after rotation, K1 become the root and K2 become the left child of K1
//and finally return the root of tree
struct AVLNode *SingleRight(struct AVLNode *K2) {
	struct AVLNode *K1;
	K1 = K2->Right;
	K2->Right = K1->Left;
	K1->Left = K2;

	//Refresh Height element is significant to control the loop invariant
	K2->Height = __max(Height(K2->Left), Height(K2->Right)) + 1;
	K1->Height = __max(Height(K1->Left), Height(K1->Right)) + 1;

	return K1;
}

//In this situation, K2 is right child of its father K1
//and K1 is the left child of its father K3(which is also K2's grandfather) 
//finally return the K2 which become the root after rotation
struct AVLNode *DoulbeLeft(struct AVLNode *K3) {
	//Rotation between K1 and K2
	K3->Left = SingleRight(K3->Left);
	//Rotation between K3 and K2
	return SingleLeft(K3);
}

//In this situation, K2 is left child of its father K1
//and K1 is the right child of its father K3(which is also K2's grandfather) 
//finally return the K2 which become the root after rotation
struct AVLNode *DoulbeRight(struct AVLNode *K3) {
	K3->Right = SingleLeft(K3->Right);
	return SingleRight(K3);
}

//This is the function of delete operation
//The input is the root of the tree and the element of a node which want to be deleted
//The output is the root of the tree after the delete the ordered node
//Tree is empty which is not concerned, because in the test it is impossible. So, Don't input the NULL Tree
AVLTree DeleteAVL(int X, struct AVLNode *Tree) {
	struct AVLNode *temp;

	if (X < Tree->Element)
		//If the X is lower than the element of this Node, we must find the X in this left sub-tree in recursion 
		Tree->Left = DeleteAVL(X, Tree->Left);
	else if (X > Tree->Element)
		//If the X is larger than the element of this Node, we must find the X in the right sub-tree in recursion
		Tree->Right = DeleteAVL(X, Tree->Right);

	//Now the Node which element equal X has been found

	//This Node have two children
	else if (Tree->Left != NULL && Tree->Right != NULL) {
		//try to make the min element Node of right sub-tree to replace the node(like ordinary BST)
		temp = FindMinAVL(Tree->Right);
		Tree->Element = temp->Element;
		Tree->Right = DeleteAVL(Tree->Element, Tree->Right);//adjust the right sub-tree and still make the whole tree be balanced
	}
	else {
		//This Node has one or zero children
		//The purpose is that delete this node and this node position is replaced by its child
		temp = Tree;
		if (Tree->Left == NULL)
			Tree = Tree->Right;
		else if (Tree->Right == NULL)
			Tree = Tree->Left;
		free(temp);
	}
	return Tree;
}

//Find the Node which element is minimum of the whole tree which input this function
AVLTree FindMinAVL(struct AVLNode *Tree) {
	if (Tree == NULL)
		return NULL;
	else if (Tree->Left == NULL)
		return Tree;
	else
		FindMinAVL(Tree->Left);
}

/*
int main(void)
{
    printf("Hello");
    return 0;
}*/
