#include <iostream>

struct Node
{
	int Element;
	Node *Parent;
	Node *Left;
	Node *Right;
};
Node *RightRotation(Node *X);
Node *LeftRotation(Node *X);
Node *Insert(int X, Node *T);
Node *Find(int X, Node *T);
Node *Delete(int X, Node *T);
Node *FindMax(Node *T);

//获得节点T的父节点
Node *P(Node *T)
{
	return T->Parent;
}

//获得节点T的祖父节点
Node *G(Node *T)
{
	return T->Parent->Parent;
}

//对一个节点调整使其变成Root符合splay tree
Node *Splay(Node *T)
{
	while (P(T) != NULL)
	{
		if (T == P(T)->Left)
		{
			if (G(T) == NULL)
			{
				//T绕P(T)右旋
				T = RightRotation(T);
			}
			else if (P(T) == G(T)->Left)
			{
				//P(T)绕G(T)右旋
				T->Parent = RightRotation(P(T));
				//T绕P(T)右旋
				T = RightRotation(T);
			}
			else
			{
				//T绕P(T)右旋
				T = RightRotation(T);
				//T绕P(T)左旋 (P(T)和上面一句的不同，是原来的G(X))
				T = LeftRotation(T);
			}
		}
		else if (T == P(T)->Right)
		{
			if (G(T) == NULL)
			{
				//T绕P(T)左旋
				T = LeftRotation(T);
			}
			else if (P(T) == G(T)->Right)
			{
				//P(T)绕G(T)左旋
				T->Parent = LeftRotation(P(T));
				//T绕P(T)左旋
				T = LeftRotation(T);
			}
			else
			{
				//T绕P(T)左旋
				T = LeftRotation(T);
				//T绕P(T)右旋 (P(T)和上面一句的不同，是原来的G(X))
				T = RightRotation(T);
			}
		}
	}
	return T;
}

//右旋
Node *RightRotation(Node *X)
{
	if (G(X) != NULL && G(X)->Left == P(X))
	{
		G(X)->Left = X;
	}
	else if (G(X) != NULL && G(X)->Right == P(X))
	{
		G(X)->Right = X;
	}

	P(X)->Left = X->Right;
	if (X->Right != NULL)
	{
		X->Right->Parent = P(X);
	}
	X->Right = P(X);
	X->Parent = P(X)->Parent;
	X->Right->Parent = X;
	return X;
}

//左旋
Node *LeftRotation(Node *X)
{
	if (G(X) != NULL && G(X)->Left == P(X))
	{
		G(X)->Left = X;
	}
	else if (G(X) != NULL && G(X)->Right == P(X))
	{
		G(X)->Right = X;
	}

	P(X)->Right = X->Left;
	if (X->Left != NULL) //如果X的左孩子存在
	{
		X->Left->Parent = P(X);
	}
	X->Left = P(X);
	X->Parent = P(X)->Parent;
	X->Left->Parent = X;
	return X;
}

//插入
Node *Insert(int X, Node *T)
{
	if (T == NULL)
	{
		Node *Root = (Node *)malloc(sizeof(Node));
		Root->Element = X;
		Root->Parent = T;
		Root->Left = Root->Right = NULL;
		return Root;
	}
	else
	{
		for (;;)
		{
			if (X > T->Element)
			{
				if (T->Right == NULL)
				{
					Node *p = (Node *)malloc(sizeof(Node));
					p->Element = X;
					p->Parent = T;
					p->Left = p->Right = NULL;
					T->Right = p;
					return Splay(p);
				}
				T = T->Right;
			}
			else if (X < T->Element)
			{
				if (T->Left == NULL)
				{
					Node *p = (Node *)malloc(sizeof(Node));
					p->Element = X;
					p->Parent = T;
					p->Left = p->Right = NULL;
					T->Left = p;
					return Splay(p);
				}
				T = T->Left;
			}
		}
	}
}

//删除
Node *Delete(int X, Node *T)
{
	//找到要被删除的点并移动到根上
	Node *Root = Find(X, T);
	Root = Splay(Root);

	Node *L = Root->Left;
	L->Parent = NULL;
	Node *R = Root->Right;
	R->Parent = NULL;

	free(Root);

	Node *NewRoot = FindMax(L);
	NewRoot = Splay(NewRoot);
	NewRoot->Right = R;
	R->Parent = NewRoot;

	return NewRoot;
}

//在二叉搜索树中找到key为X的那个节点
Node *Find(int X, Node *T)
{
	if (T == NULL)
		return NULL;
	for (;;)
	{
		if (X > T->Element)
			T = T->Right;
		else if (X < T->Element)
			T = T->Left;
		else if (X == T->Element)
			return T;
	}
}

//找到一棵二叉搜索树中的最大key值的节点
Node *FindMax(Node *T)
{
	if (T == NULL)
		return NULL;
	else if (T->Right == NULL)
		return T;
	else
		FindMax(T->Right);
}

//Test Case
int main(void)
{
	Node *T = NULL;

	T = Insert(1, T);
	T = Insert(3, T);
	T = Insert(2, T);
	T = Insert(5, T);
	T = Insert(4, T);

	T = Delete(3, T);

	printf("%d", T->Element);
	return 0;
}