#include<iostream>
//主要参考课本
struct Node
{
	int Element;
	Node* Left;
	Node* Right;
};

Node* Insert(int X, Node* T);
Node* FindMin(Node *T);
Node* Delete(int X, Node* T);

Node* Insert(int X, Node* T)
{
	if (T == NULL)
	{
		T = (Node*)malloc(sizeof(struct Node));
		if (T == NULL)
		{
			printf("Out of Space！");
			exit(1);
		}
		else
		{
			T->Element = X;
			T->Left = T->Right = NULL;
		}
	}
	else if (X < T->Element)
		T->Left = Insert(X, T->Left);
	else if (X > T->Element)
		T->Right = Insert(X, T->Right);

	return T;
}

Node* FindMin(Node *T)
{
	if (T == NULL) return NULL;
	else if (T->Left == NULL) return T;
	else return FindMin(T->Left);
}

Node* Delete(int X, Node* T)
{
	Node* Temp;
	if (T == NULL)
	{
		printf("Element Not Find");
		exit(1);
	}
	else if (X < T->Element)
	{
		T->Left = Delete(X, T->Left);
	}
	else if (X > T->Element)
	{
		T->Right = Delete(X, T->Right);
	}
	else if (T->Left&&T->Right)//T has two children
	{
		Temp = FindMin(T->Right);
		T->Element = Temp->Element;
		T->Right = Delete(T->Element, T->Right);
	}
	else//T only has one or zero child
	{
		Temp = T;
		if (T->Left == NULL)
			T = T->Right;
		else if (T->Right == NULL)
			T = T->Left;
		free(Temp);
	}
	return T;
}

//Test Case
int main(void)
{
	Node *T = NULL;
	T = Insert(6, T);
	T = Insert(2, T);
	T = Insert(8, T);
	T = Insert(1, T);
	T = Insert(5, T);
	T = Insert(3, T);
	T = Insert(4, T);
	T = Insert(6, T);

	T = Delete(2, T);

	printf("%d", T->Element);
}