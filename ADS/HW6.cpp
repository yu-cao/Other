#include <iostream>
using namespace std;
#define max_problem 10
struct Problem
{
	char s[21];
	int finish; //第一次写完程序的用时
	int fixed;  //每一次被rejected后的debug时间
};
struct Problem Prob[max_problem];	//题目数
int Hour, num_problem, read_time;
int sequence_ans_temp[max_problem];	//依次记录做出来的题目序号
int sequence_ans[max_problem];		//依次记录最后的结果
int visit[max_problem];				//记录是否完成
int accept_num;						//题目数量
int count_time;						//被统计的时间

void update(int num, int total)
{
	if (num < accept_num)
		return;
	if (num > accept_num || (num == accept_num && total < count_time))
	{
		accept_num = num;
		count_time = total;
		for (int i = 0; i < num_problem; i++)
			sequence_ans[i] = sequence_ans_temp[i];//不然无法保证序列最小那个要求
	}
}

void Calculate(int num, int real_time, int count_time)
{
	int fail_count = 0;
	int submit_time_first = real_time + Prob[sequence_ans_temp[num]].finish;
	int submit_time_final = submit_time_first;
	if (submit_time_first > 60)
	{
		if (submit_time_first % 60 == 0)
		{
			fail_count = submit_time_first / 60 - 1;
			submit_time_final += Prob[sequence_ans_temp[num]].fixed * fail_count;
		}
		else
		{
			fail_count = submit_time_first / 60;
			submit_time_final += Prob[sequence_ans_temp[num]].fixed * fail_count;
		}
	}
	int count_time_final = count_time + submit_time_final + fail_count * 20;

	if (submit_time_final <= Hour * 60)//还有多余的时间
	{
		num++;
		if (num < num_problem)
		{
			for (int i = 0; i < num_problem; i++)
			{
				if (visit[i] == 1)
					continue;
				visit[i] = 1;
				sequence_ans_temp[num] = i;
				Calculate(num, submit_time_final, count_time_final);
				visit[i] = 0;
			}
		}
		else update(num, count_time_final);
	}
	else update(num, count_time);
}

int main()
{
	while (1)
	{
		count_time = 0;
		accept_num = 0;
		for (int i = 0; i < max_problem; i++) {
			sequence_ans_temp[i] = 0;
			visit[i] = 0;
		}

		scanf("%d", &Hour);
		if (Hour < 0)//跳出语句
			return 0;

		scanf("%d%d", &num_problem, &read_time);

		for (int num = 0; num < num_problem; num++)
			scanf("%s %d%d", &Prob[num].s, &Prob[num].finish, &Prob[num].fixed);

		int real_time = read_time;
		for (int i = 0; i < num_problem; i++)
		{
			visit[i] = 1;
			sequence_ans_temp[0] = i;//对于第一层的选择遍历
			Calculate(0, real_time, 0);
			visit[i] = 0;//复原
		}

		printf("Total Time = %d\n", count_time);
		for (int i = 0; i < accept_num; i++)
			printf("%s\n", Prob[sequence_ans[i]].s);
	}
}