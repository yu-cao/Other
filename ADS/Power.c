/*6-1 Quick Power?4 ??
The function Power calculates the exponential function N^?k. But since the exponential function grows rapidly, you are supposed to return (N^?k)%10007 instead.

Format of function:
int Power(int N, int k);
Both N and k are integers, which are no more than 2147483647.*/
#include <stdio.h>

int Power(int, int);

const int MOD = 10007;
int main()
{
    int N, k;
    scanf("%d%d", &N, &k);
    printf("%d\n", Power(N, k));
    return 0;
}

int Power(int N, int k)
{
    int di; //底数
    if (N >= MOD)
        di = N % MOD;
    else
        di = N;
    if (k == 1)
        return di;
    if (k == 0)
        return 1;

    if (k % 2 == 0) //K is the even
        return ((Power(di, k / 2) % MOD) * (Power(di, k / 2)) % MOD) % MOD;
    else
        return ((Power(di, (k - 1) / 2 + 1) % MOD) * (Power(di, (k - 1) / 2)) % MOD) % MOD;//k+1 will overflow
}
