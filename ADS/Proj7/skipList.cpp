#include<iostream>
//Using the random probablity is p = 0.5
//MAX_LEVEL is equal to N when (1/p)^N almost == size of input
const int MAX_LEVEL = 5;//It should be redefine in the main function!! Now the input should be about 32.
const int INF = 100000;//Give a largest number of whole input, this just a test

//Define the Node of storing the data
template<typename T>
struct Node
{
	T Key;//The Key data to be compare and sort
	T Value;//The store data, it is not necessary of the whole function
	Node* forward[1];//The pointer to the next Node
};

//Define the list of header of each layer
template<typename T>
struct List
{
	int level;//Show the level of list
	Node<T>* header;//The head of the list
};

template<typename T>
Node<T>* createNode(int level, int key, T value)
{
	//Node<T> is to store the data, Node<T>* is the pointer link the whole list
	Node<T> *newNode = (Node<T>*)malloc(sizeof(Node<T>) + level * sizeof(Node<T>*));
	newNode->Key = key;
	newNode->Value = value;
	return newNode;
}

//Build an empty list for initialization
//Header's Key is INF as the tail
template<typename T>
List<T>* createList()
{
	//Set the final Node as the NIL, which Key is INF, value is not care
	Node<T> *NIL = createNode(0, INF, 0);

	//Set the link list
	List<T> *newList = new List<T>;
	newList->level = 0;

	//Set the head node
	newList->header = createNode(MAX_LEVEL, -INF, 0);
	for (auto i = 0; i < MAX_LEVEL; ++i)
	{
		newList->header->forward[i] = NIL;
	}
	return newList;
}

template<typename T>
bool search(List<T> *list, T searchKey)
{
	auto x = list->header;
	//loop invariant:x->value < searchKey
	for (auto i = list->level; i >= 0; --i)
	{
		while (x->forward[i]->Key < searchKey)
		{
			x = x->forward[i];
		}
	}
	//Now, x->value < searchKey <= x->forward[1]->value
	x = x->forward[0];
	if (x->Key == searchKey)
		return true;//exist
	return false;//Not exist
}

//If extreme case, the type of "int" is enough
//This function decide the new node will up to which level
int randomLevel()
{
	auto v = 0;
	//Just like fliping the coin, if Heads, up the level; else if tails, maintain the level
	while (rand() % 2 && v < MAX_LEVEL)
		v++;
	return v;
}

template<typename T>
bool Insert(List<T> *list, T searchKey, T newValue)
{
	Node<T> *update[MAX_LEVEL];
	auto x = list->header;
	//Find the node and record the front nodes before it
	for (auto i = list->level; i >= 0; --i)
	{
		while (x->forward[i]->Key < searchKey)
		{
			x = x->forward[i];
		}
		update[i] = x;
	}

	x = x->forward[0];

	//This Key has been exist, just update the value to a new value
	if (x->Key == searchKey) {
		x->Value = newValue;
		return false;
	}

	auto v = randomLevel();
	if (v > list->level)
	{
		for (auto i = list->level + 1; i <= v; ++i)
		{
			update[i] = list->header;
		}
		list->level = v;
	}

	//adjust the link list and insert
	auto newNode = createNode(v, searchKey, newValue);
	for (auto i = 0; i <= v; ++i)//should insert in the level [0,v]
	{
		newNode->forward[i] = update[i]->forward[i];
		update[i]->forward[i] = newNode;
	}
	return true;
}

template<typename T>
bool Delete(List<T>* list, T searchKey)
{
	Node<T> *update[MAX_LEVEL];
	auto x = list->header;

	//Find the node and record the front nodes before it
	for (auto i = list->level; i >= 0; --i)
	{
		while (x->forward[i]->Key < searchKey)
		{
			x = x->forward[i];
		}
		update[i] = x;
	}

	x = x->forward[0];

	if (x->Key != searchKey) return false;//the node is not exist

	//Delete the node like the normal link list
	for (auto i = 0; i <= list->level; ++i)
	{
		if (update[i]->forward[i] != x) break;
		update[i]->forward[i] = x->forward[i];
	}
	delete x;

	//If the higest level is empty, delete this level
	while (list->level > 0 && list->header->forward[list->level]->Key == INF)
	{
		--list->level;
	}
	return true;
}

int main(int argc, char* argv[])
{
	int N;//The number of input
	scanf("%d", &N);//you must be configuration to the MAX_LEVEL
	//int temp = N;
	//for(int i=0;i<N;i++)
	//{
	//	temp /= 2;
	//	if (temp != 0) MAX_LEVEL++;
	//}

	List<int> *test = createList<int>();
	int ok;
	//Insert Fuction pass!
	for (auto i = 0; i < N; i++)
	{
		std::cin >> ok;
		bool flag = Insert<int>(test, ok, i);
		if (flag == false) std::cout << "Not Insert";
	}
	//Find function pass!
	for (auto i = 0; i <= 1; ++i)
	{
		std::cin >> ok;
		bool flag = search(test, ok);
		if (flag == true) std::cout << "Find";
	}
	//Delete Function pass!
	for(auto i=0;i<=0;++i)
	{
		std::cin >> ok;
		bool flag = Delete(test, ok);
		if (flag == true) std::cout << "Delete";
	}

	bool finally = search(test, ok);
	if (finally == true) std::cout << "FIND";
}