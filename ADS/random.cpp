#include <stdio.h>
#include <time.h>
#include <stdlib.h>

//rand()会返回一个范围在0到RAND_MAX（32767）之间的伪随机数（整数）
int main(void)
{
    int N = 264346; //恰好：8*Rand_MAX
    for (int i = 0; i < 1000; i++)
    {
        int f = (int)(rand() * 8.1) % N;
        printf("%d\n", f);
    }
    while (1);
}