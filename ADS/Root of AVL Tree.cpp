//本程序部分实现参考课本中的P121-123的代码
#include <iostream>
using namespace std;
#define max(a, b) (((a) > (b)) ? (a) : (b))
struct Node
{
    int Height;
    int Element;
    Node *LeftChild;
    Node *RightChild;
};

Node *Insert(int X, Node *T);
int Height(Node *T);
Node *SingleLeft(Node *K2);
Node *SingleRight(Node *K2);
Node *DoubleLeft(Node *K3);
Node *DoubleRight(Node *K3);

int main(void)
{
    int N, X;
    cin >> N;
    Node *p = NULL;
    for (int i = 0; i < N; i++)
    {
        cin >> X;
        p = Insert(X, p);
    }
    cout << p->Element;
}

int Height(Node *T)
{
    if (T == NULL)
        return -1;
    else
        return T->Height;
}

Node *SingleLeft(Node *K2)
{
    Node *K1;
    K1 = K2->LeftChild;
    K2->LeftChild = K1->RightChild;
    K1->RightChild = K2;

    K2->Height = max(Height(K2->LeftChild), Height(K2->RightChild)) + 1;
    K1->Height = max(Height(K1->LeftChild), Height(K1->RightChild)) + 1;

    return K1;
}

Node *SingleRight(Node *K2)
{
    Node *K1;
    K1 = K2->RightChild;
    K2->RightChild = K1->LeftChild;
    K1->LeftChild = K2;

    K2->Height = max(Height(K2->LeftChild), Height(K2->RightChild)) + 1;
    K1->Height = max(Height(K1->LeftChild), Height(K1->RightChild)) + 1;

    return K1;
}

Node *DoubleLeft(Node *K3)
{
    K3->LeftChild = SingleRight(K3->LeftChild);
    return SingleLeft(K3);
}

Node *DoubleRight(Node *K3)
{
    K3->RightChild = SingleLeft(K3->RightChild);
    return SingleRight(K3);
}

Node *Insert(int X, Node *T)
{
    if (T == NULL)
    {
        T = new Node;
        T->Element = X;
        T->LeftChild = T->RightChild = NULL;
        T->Height = 0;
    }
    else if (X < T->Element)
    {
        T->LeftChild = Insert(X, T->LeftChild);
        if (Height(T->LeftChild) - Height(T->RightChild) >= 2)
        {
            if (X < T->LeftChild->Element) //case of "Left Left"
                T = SingleLeft(T);
            else //case of Double Rotation "Left and Right"
                T = DoubleLeft(T);
        }
    }
    else if (X > T->Element)
    {
        T->RightChild = Insert(X, T->RightChild);
        if (Height(T->RightChild) - Height(T->LeftChild) >= 2)
        {
            if (X > T->RightChild->Element) //case of "Right Right"
                T = SingleRight(T);
            else //case of Double Rotation "Right and Left"
                T = DoubleRight(T);
        }
    }
    T->Height = max(Height(T->LeftChild), Height(T->RightChild)) + 1;
    return T;
}