#include<iostream>
using namespace std;
struct gas {
	double price;
	int distance;
};
void sort_g(struct gas *G, int N);
int main(int argc, char const *argv[])
{
	//C容量 D总距离 d距离/容量 N加油站数目
	int C, D, d, N;
	struct gas G[500];//max possible number
	scanf("%d%d%d%d", &C, &D, &d, &N);
	const int max_per = C * d;//加满油的最大距离

	for (int i = 0; i < N; i++)
	{
		scanf("%lf%d", &(G[i].price), &(G[i].distance));
	}
	sort_g(G, N);
	if (G[0].distance != 0)
	{
		printf("The maximum travel distance = 0.00");
		return 0;
	}

	int now = 0;//现在车子还能跑多远
	double max_length = 0;//最大距离,已经开的路
	double money = 0;//一路的油费
	int count = 0;//第n个加油站

	while (max_length != D)
	{
		int select_min = count + 1;
		for (int i = count + 1; i < N; i++)
		{
			if (G[i].distance <= G[count].distance + max_per)
			{
				//记录前路上的加油站最便宜的且相对较远的
				if (G[select_min].price >= G[i].price)
					select_min = i;

				//当可以跑到的距离内有离自己最近的价格低于自己的加油站时就把自己的油量设置在刚好跑到为止
				if (G[i].price < G[count].price)
				{
					money += (G[i].distance - G[count].distance) * G[count].price / d;
					max_length += G[i].distance - G[count].distance;
					count = i;
					select_min = count + 1;//前路最便宜的加油站
					now = 0;
				}

			}
		}
		//前路上的所有加油站价格都比初始点贵，就先在初始点把油箱加满
		if (now == 0)
		{
			if (D - G[count].distance < max_per)
			{
				money += G[count].price * (D - G[count].distance) / d;
				max_length = D;
				now = 0;
			}
			else
			{
				money += G[count].price * (max_per - now) / d; //如果一路都是比自己贵的，就先把自己的油箱加满
				max_length = G[count].distance;
				now = max_per;
				if (count == N - 1)
				{
					printf("The maximum travel distance = %.2f", max_length + max_per);
					return 0;
				}
			}

		}
		else if (G[select_min].price > G[count].price && (G[count].distance + now) < D)
		{
			now -= G[select_min].distance - G[count].distance;
			if (G[select_min].distance + max_per > D)//如果加满会导致油量过多
			{
				//去查之后有没有比自己还便宜的
				int i;
				for (i = select_min + 1; i < N; i++)
				{
					if (G[i].price < G[select_min].price)//如果有更便宜的，就加油到那边为止
					{
						int diff = G[i].distance - G[select_min].distance - now;
						now = 0;
						money += G[select_min].price * diff / d;
						count = i;
						max_length = G[i].distance;
						break;
					}
				}
				if (i == N)
				{
					int diff = D - G[select_min].distance - now;
					now += diff;
					money += G[select_min].price * diff / d;
					max_length = G[select_min].distance;
				}
			}
			else//即使加满也到不了终点
			{
				int i = 0;
				int xiao = select_min;
				for (i = select_min; i < N; i++)
				{
					if (G[i].distance - G[select_min].distance > max_per)
					{
						break;
					}
					if (G[i].price <= G[select_min].price)
					{
						xiao = i;
					}
				}
				if (i == N)
				{
					printf("The maximum travel distance = %.2f", max_length + max_per);
					return 0;
				}

				if (xiao == select_min)//后面的加油站都比这个贵
				{
					money += G[select_min].price * (max_per - now) / d; //如果一路都是比自己贵的，就先把自己的油箱加满
					max_length = G[select_min].distance;
					now = max_per;
					count = select_min;
				}
				else
				{
					money += G[select_min].price * (G[xiao].distance - G[select_min].distance - now) / d;
					max_length = G[xiao].distance;
					now = 0;
					count = xiao;
				}

			}
		}
		//可以直接开完全程
		else if (G[select_min].price > G[count].price && (G[count].distance + now) == D)
			max_length = D;

	}
	printf("%.2f", money);

	return 0;
}

//N:加油站数目
void sort_g(struct gas *G, int N)
{
	for (int i = 0; i < N; i++)
		for (int j = i; j < N; j++)
			if (G[i].distance > G[j].distance)
				swap(G[i], G[j]);
}