//堆栈建议开64MB，否则有爆栈风险
#include <cstdio>
#include <vector>
#include <fstream>
#include <string>
#include <stack>
#include <iostream>

using namespace std;
#define MAX 875714

struct node {
	int n;
	bool visited;
	vector<int> toNode;
	vector<int> fromNode;
	int SCC;
};

node Graph[MAX + 1];
stack<int> order;
int SCC_num = 0;
vector<int> SCC;
int top[6] = { 0 };

void init_graph() {
	for (auto i = 0; i < MAX + 1; i++) {
		Graph[i].n = i;
		Graph[i].visited = false;
	}
}

void read_data(const char path[]) {
	int from, to;
	FILE * fp;
	fp = fopen(path, "r");

	if (fp == nullptr) {
		cout << "FILE OPEN FAILED!" << endl;
		return;
	}
	// Set pointer to beginning of file:
	fseek(fp, 0L, SEEK_SET);

	while (fscanf(fp, "%d %d", &from, &to) > 0) {
		Graph[from].toNode.push_back(to);
		Graph[to].fromNode.push_back(from);
	}
}

void DFS_Graph(int i) {
	if (Graph[i].visited) return;
	Graph[i].visited = true;
	for (auto k : Graph[i].toNode)
	{
		DFS_Graph(k);
	}
	order.push(i);
	return;
}

void rDFS_Graph(int i) {
	if (!Graph[i].visited) return;

	Graph[i].visited = false;
	auto index = 0;
	for (auto k : Graph[i].fromNode)
	{
		index = k;
		rDFS_Graph(index);
	}
	Graph[i].SCC = SCC_num;
	SCC[SCC_num - 1]++;
	return;
}

void TopFive() {
	for (auto i : SCC)
	{
		top[5] = i;
		for (auto j = 4; j >= 0; j--) {
			if (top[j + 1] > top[j]) {
				auto temp = top[j];
				top[j] = top[j + 1];
				top[j + 1] = temp;
			}
			else break;
		}
	}
}

int main() {
	init_graph();
	read_data("E:/GitHub_sync/Other/Coursera_Algorithm/WEEK1_SCC/SCC.txt");

	//traverse in DFS
	for (auto j = 1; j < MAX + 1; j++) {
		if (!Graph[j].visited) DFS_Graph(j);
	}

	//reverse DFS
	while (!order.empty()) {
		auto index = order.top();
		order.pop();
		if (Graph[index].visited) {
			SCC_num++;
			SCC.push_back(0);
			rDFS_Graph(index);
		}
	}

	//Find the top 5 element in SCC
	TopFive();

	ofstream fout("out.txt");
	for (int i = 0; i < 5; i++) {
		fout << top[i] << endl;
		cout << top[i] << endl;
	}
	fout.close();

	return 0;
}