`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:36:46 01/10/2018 
// Design Name: 
// Module Name:    level 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module level(clk_100mhz,Level_Up,level, clk);
input wire clk_100mhz;
input wire Level_Up;
output reg clk;
input wire[1:0] level;
reg [31:0] cnt;
reg[1:0] lv_0 = 2'b00;
reg[1:0] lv_1 = 2'b01;
reg[1:0] lv_2 = 2'b10;
reg[1:0] lv_3 = 2'b11;
integer lv,n,nn=1,n1,n2;
reg rst=1;
always @* begin
case (level)
lv_0: begin
lv = 1;
end
lv_1: begin
lv = 2;
end
lv_2: begin
lv = 4;
end
lv_3: begin
lv = 8;
end
endcase//用以手动调节难度
end
always @ (posedge Level_Up )begin
nn<=nn*2;
end//升级时难度增加
always @ (posedge level[0] )begin
n1<=nn;
end
always @ (negedge level[0])begin
n2<=nn;
end
always@*begin
if(n1>n2)n<=n1;
else n<=n2;
end
	always @ (posedge clk_100mhz) begin
	if (cnt < 8_000_000_0*n/lv/nn) begin //50M*(1/100M)S / lv / n = T/2
	cnt <= cnt + 1;
	end else begin
	cnt <= 0;
	clk <= ~clk; 
	end
	
	if(rst)begin
	clk<=0;
	rst<=0;//初始化
	end 
	end
	endmodule
