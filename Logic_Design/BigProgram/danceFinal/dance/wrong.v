`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:47:36 01/10/2018 
// Design Name: 
// Module Name:    wrong 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module wrong(
	 input clk,
    input [3:0] BTN,
    input [1:0] Q,
    output Right,
	 output reg Wrong
    );
	 reg x;
	 reg Wrong2;
	 reg Wrong21;
	 reg Wrong22;
	 reg Wrong23;
	 reg Wrong24;
INV A(.I(Q[0]),.O(nQ0)),
	 B(.I(Q[1]),.O(nQ1)),
	 C(.I(BTN[0]),.O(nB0)),
	 D(.I(BTN[1]),.O(nB1)),
	 E(.I(BTN[2]),.O(nB2)),
	 F(.I(BTN[3]),.O(nB3));
AND3 G1(.I0(nQ1),.I1(nQ0),.I2(BTN[0]),.O(W1)),
	  G2(.I0(nB1),.I1(nB2),.I2(nB3),.O(W2)),
	  H1(.I0(nQ1),.I1(Q[0]),.I2(nB0),.O(X1)),
	  H2(.I0(BTN[1]),.I1(nB2),.I2(nB3),.O(X2)),
	  I1(.I0(Q[1]),.I1(nQ0),.I2(nB0),.O(Y1)),
	  I2(.I0(nB1),.I1(BTN[2]),.I2(nB3),.O(Y2)),
	  J1(.I0(Q[1]),.I1(Q[0]),.I2(nB0),.O(Z1)),
	  J2(.I0(nB1),.I1(nB2),.I2(BTN[3]),.O(Z2));
AND2 G(.I0(W1),.I1(W2),.O(W)),
	  H(.I0(X1),.I1(X2),.O(X)),
	  I(.I0(Y1),.I1(Y2),.O(Y)),
	  J(.I0(Z1),.I1(Z2),.O(Z));
OR3 K(.I0(BTN[1]),.I1(BTN[2]),.I2(BTN[3]),.O(WW)),
	 L(.I0(BTN[0]),.I1(BTN[2]),.I2(BTN[3]),.O(XX)),
	 M(.I0(BTN[0]),.I1(BTN[1]),.I2(BTN[3]),.O(YY)),
	 N(.I0(BTN[0]),.I1(BTN[1]),.I2(BTN[2]),.O(ZZ));
AND3	O(.I0(nQ1),.I1(nQ0),.I2(WW),.O(WWW)),
		P(.I0(nQ1),.I1(Q[0]),.I2(XX),.O(XXX)),
		QQ(.I0(Q[1]),.I1(nQ0),.I2(YY),.O(YYY)),
		R(.I0(Q[1]),.I1(Q[0]),.I2(ZZ),.O(ZZZ));
OR4 S(.I0(W),.I1(X),.I2(Y),.I3(Z),.O(Right)),
	 T(.I0(WWW),.I1(XXX),.I2(YYY),.I3(ZZZ),.O(Wrong1));
	 
	 always@(posedge clk or posedge Right)begin
if(Right)x<=1;//当按下正确按键时,进入状态1,表示已曾正确按下
else begin
if(x==0)begin Wrong2<=1;
#5;
Wrong2<=0;
end
x<=0;
end//若始终未按下正确按键,判为Wrong;并回到状态0
end

always@*begin
if(Wrong2==1)Wrong <= Wrong2;
else Wrong <= Wrong1;
end
endmodule

