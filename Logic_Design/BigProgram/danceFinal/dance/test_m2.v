`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:54:29 01/16/2018
// Design Name:   level
// Module Name:   E:/dance/test_m2.v
// Project Name:  dance
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: level
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_m2;

	// Inputs
	reg clk_100mhz;
	reg Level_Up;
	reg [1:0] level;

	// Outputs
	wire clk;

	// Instantiate the Unit Under Test (UUT)
	level uut (
		.clk_100mhz(clk_100mhz), 
		.Level_Up(Level_Up), 
		.level(level), 
		.clk(clk)
	);

	initial begin
		// Initialize Inputs
		clk_100mhz = 0;
		Level_Up = 0;
		level = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		clk_100mhz = 0;
		Level_Up = 0;
		level = 0;
		#0;
		Level_Up = 0;
		#1300;
		Level_Up = 1;
		#10;
		Level_Up = 0;
		#490;
		Level_Up = 1;
		#10;
		Level_Up = 0;
		#500;
		level = 1;
		#390;
	end
	integer i;
	always@* for(i=1;i<999999;i=i+1)begin #50;clk_100mhz=~clk_100mhz;end
      
endmodule

