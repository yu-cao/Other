`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:50:13 01/10/2018 
// Design Name: 
// Module Name:    Mark 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mark(
    input [2:0]Mark,
    output reg[7:0] Mark_Seg7
    );
always@*begin
Mark_Seg7[7]<=1;
Mark_Seg7[6:0]<={g,f,e,d,c,b,a};
end
INV	M0(.I(Mark[0]),.O(nm0)),
		M1(.I(Mark[1]),.O(nm1)),
		M2(.I(Mark[2]),.O(nm2));
AND3  A1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(a1)),
		A2(.I0(nm0),.I1(nm1),.I2(Mark[2]),.O(a2));
AND3  B1(.I0(Mark[0]),.I1(nm1),.I2(Mark[2]),.O(b1)),
		B2(.I0(nm0),.I1(Mark[1]),.I2(Mark[2]),.O(b2));
AND3  C1(.I0(nm0),.I1(Mark[1]),.I2(nm2),.O(c));
AND3  D1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(d1)),
		D2(.I0(nm0),.I1(nm1),.I2(Mark[2]),.O(d2)),
		D3(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(d3));
AND3  E1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(e1)),
		E2(.I0(Mark[0]),.I1(Mark[1]),.I2(nm2),.O(e2)),
		E3(.I0(nm0),.I1(nm1),.I2(Mark[2]),.O(e3)),
		E4(.I0(Mark[0]),.I1(nm1),.I2(Mark[2]),.O(e4)),
		E5(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(e5));
AND3  F1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(f1)),
		F2(.I0(nm0),.I1(Mark[1]),.I2(nm2),.O(f2)),
		F3(.I0(Mark[0]),.I1(Mark[1]),.I2(nm2),.O(f3)),
		F4(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(f4));
AND3  G1(.I0(nm0),.I1(nm1),.I2(nm2),.O(g1)),
		G2(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(g2)),
		G3(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(g3));
OR2	A(.I0(a1),.I1(a2),.O(a));
OR2	B(.I0(b1),.I1(b2),.O(b));
OR3	D(.I0(d1),.I1(d2),.I2(d3),.O(d));
OR5	E(.I0(e1),.I1(e2),.I2(e3),.I3(e4),.I4(e5),.O(e));
OR4	F(.I0(f1),.I1(f2),.I2(f3),.I3(f4),.O(f));
OR3	G(.I0(g1),.I1(g2),.I2(g3),.O(g));

endmodule

