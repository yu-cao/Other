`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:49:25 01/10/2018 
// Design Name: 
// Module Name:    score 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module score(
	 input clk,
    input Right,
	 input Wrong,
    output reg[2:0] Mark,
    output reg Level_Up
    );
reg rst=1;
reg[1:0] temp;
integer x;
always @* begin
if(rst)begin
Mark<=0;
Level_Up<=0;
rst<=0;
end
if(Wrong)begin
Mark<=0;
end
if(Mark==7)begin
Level_Up <= 1;
Mark <= 0;
#5;
Level_Up <= 0;
end
if(Right&&x)begin
Mark <= Mark + 1;
x <= 0;
end//分数满8分时进入下一等级,并重新积分
if(temp==2'b01)
x<=1;
end


always@*begin
temp[0]<=clk;
temp[1]<=temp[0];
end//Result=1的信号到达以后Mark自增1并且在clk上升缘来到之前（也就是显示的符号改变之前）再也不自增1.


endmodule
