`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:58:03 01/10/2018 
// Design Name: 
// Module Name:    display 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module display(
    input [7:0] Image,
    input [7:0] Mark,
    input clk,
    output reg[7:0] Segment,
    output reg[3:0] AN
    );
always @* begin 
case (clk)
1'b0 : begin Segment <= Image; AN <= 4'b0111; end
1'b1 : begin Segment <= Mark; AN <= 4'b1110; end
endcase
end

endmodule

