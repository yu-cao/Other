`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:39:50 01/10/2018 
// Design Name: 
// Module Name:    random 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module random(
    input clk,
	output reg[1:0] Q
);
wire CR = 1;
wire[31:0]O;
reg S1 = 1;
reg S0 = 1;
wire[31:0]PData = 32'b10010110100100110101011001101010;
always @* begin
Q={O[1],O[0]};
end

always@(posedge clk)begin
S1 = 0;
end
DM74LS194 
SH0(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[31]), .SR(O[4]),
.A(PData[3]), .B(PData[2]), .C(PData[1]), .D(PData[0]),
.QA(O[3]), .QB(O[2]), .QC(O[1]), .QD(O[0])),
SH1(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[3]), .SR(O[8]),
.A(PData[7]), .B(PData[6]), .C(PData[5]), .D(PData[4]),
.QA(O[7]), .QB(O[6]), .QC(O[5]), .QD(O[4])),
SH2(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[7]), .SR(O[12]),
.A(PData[11]),.B(PData[10]),.C(PData[9]),.D(PData[8]),
.QA(O[11]), .QB(O[10]), .QC(O[9]), .QD(O[8])),
SH3(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[11]), .SR(O[16]),
.A(PData[15]),.B(PData[14]),.C(PData[13]),.D(PData[12]),
.QA(O[15]), .QB(O[14]), .QC(O[13]), .QD(O[12]));
DM74LS194 
SH4(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[15]), .SR(O[20]),
.A(PData[19]),.B(PData[18]),.C(PData[17]),.D(PData[16]),
.QA(O[19]), .QB(O[18]), .QC(O[17]), .QD(O[16])),
SH5(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[19]), .SR(O[24]),
.A(PData[23]),.B(PData[22]),.C(PData[21]),.D(PData[20]),
.QA(O[23]), .QB(O[22]), .QC(O[21]), .QD(O[20])),
SH6(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[23]), .SR(O[28]),
.A(PData[27]),.B(PData[26]),.C(PData[25]),.D(PData[24]),
.QA(O[27]), .QB(O[26]), .QC(O[25]), .QD(O[24])),
SH7(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[27]), .SR(O[0]),
.A(PData[31]),.B(PData[30]),.C(PData[29]),.D(PData[28]),
.QA(O[31]), .QB(O[30]), .QC(O[29]), .QD(O[28]));
endmodule
