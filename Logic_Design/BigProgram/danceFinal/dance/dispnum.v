`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:32:02 01/10/2018 
// Design Name: 
// Module Name:    dispnum 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dispnum(Q, 
          SEG);

    input [1:0] Q;
   output [7:0] SEG;
   
   wire G0;
   wire V5;
   wire XLXN_65;
   wire XLXN_90;
   wire XLXN_91;
   wire XLXN_97;
   
   INV  XLXI_18 (.I(Q[1]), 
                .O(XLXN_65));
   INV  XLXI_19 (.I(Q[0]), 
                .O(XLXN_90));
   AND2  XLXI_32 (.I0(Q[0]), 
                 .I1(Q[1]), 
                 .O(XLXN_97));
   AND2  XLXI_35 (.I0(Q[0]), 
                 .I1(XLXN_65), 
                 .O(XLXN_91));
   VCC  XLXI_36 (.P(V5));
   GND  XLXI_37 (.G(G0));
   BUF  XLXI_38 (.I(V5), 
                .O(SEG[0]));
   BUF  XLXI_39 (.I(G0), 
                .O(SEG[4]));
   BUF  XLXI_40 (.I(V5), 
                .O(SEG[7]));
   BUF  XLXI_41 (.I(Q[1]), 
                .O(SEG[1]));
   BUF  XLXI_42 (.I(Q[1]), 
                .O(SEG[2]));
   BUF  XLXI_43 (.I(XLXN_90), 
                .O(SEG[6]));
   BUF  XLXI_44 (.I(XLXN_91), 
                .O(SEG[5]));
   BUF  XLXI_48 (.I(XLXN_97), 
                .O(SEG[3]));
endmodule
