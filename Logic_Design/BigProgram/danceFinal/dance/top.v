`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:27:09 01/10/2018 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(clk_100mhz, 
                 K_COL, 
                 RSTN, 
                 SW, 
                 AN, 
                 Buzzer, 
                 SEG);

    input clk_100mhz;
    input [3:0] K_COL;
    input RSTN;
    input [15:0] SW;
   output [3:0] AN;
   output Buzzer;
   output [7:0] SEG;
   
   wire [31:0] clkdiv;
   wire [1:0] SW_OK;
   wire [1:0] Q;
   wire [3:0] BTN;
   wire Right;
   wire clk;
   wire [2:0] mark;
   wire [7:0] Mark_Seg7;
   wire [7:0] Image;
   //wire RSTN;
   wire wrong;
   wire Level_Up;
   wire bu;
   
	level  M2 (.clk_100mhz(clk_100mhz), .level({SW[0], SW[1]}), .Level_Up(Level_Up), .clk(clk));
	random M3 (.clk(clk), .Q(Q[1:0]));
	//SAnti_jitter  M0 (.clk(clk_100mhz), .Key_y(K_COL[3:0]), .readn(), .RSTN(RSTN), .SW(SW[15:0]), .BTN_OK(BTN[3:0]), .CR(), .Key_out(), .Key_ready(), .Key_x(), .pulse_out(), .rst(RSTN), .SW_OK());
   dispnum M1(.Q(Q[1:0]), .SEG(Image[7:0]));
   score M5 (.clk(clk), .Right(Right), .Wrong(wrong), .Level_Up(Level_Up), .Mark(mark[2:0]));
   mark  M6 (.Mark(mark[2:0]), .Mark_Seg7(Mark_Seg7[7:0]));
   wrong  M4 (.BTN(K_COL[3:0]), .clk(clk), .Q(Q[1:0]), .Right(Right), .Wrong(wrong));
   display M7 (.clk(clkdiv[18]), .Image(Image[7:0]), .Mark(Mark_Seg7[7:0]), .AN(AN[3:0]), .Segment(SEG[7:0]));
   INV XLXI_28(.I(wrong), .O(bu));
   INV XLXI_99(.I(bu), .O(Buzzer));
   //disp_num M7(.clk(clk_100mhz), .RST(RSTN), .HEXS()) 
   //CreateNumber M8(.btn(btn), .Mark(mark[2:0]), .Q(Q[1:0]). num(num)); 
   clkdiv  m8 (.clk(clk_100mhz), .rst(RSTN), .clkdiv(clkdiv[31:0]));
endmodule
