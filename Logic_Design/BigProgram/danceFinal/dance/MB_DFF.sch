<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="Q" />
        <signal name="Qn" />
        <signal name="Sn" />
        <signal name="XLXN_11" />
        <signal name="Cp" />
        <signal name="D" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="Rn" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qn" />
        <port polarity="Input" name="Sn" />
        <port polarity="Input" name="Cp" />
        <port polarity="Input" name="D" />
        <port polarity="Input" name="Rn" />
        <blockdef name="nand3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="216" y1="-128" y2="-128" x1="256" />
            <circle r="12" cx="204" cy="-128" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <block symbolname="nand3" name="XLXI_1">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="Sn" name="I1" />
            <blockpin signalname="XLXN_2" name="I2" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_2">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="Rn" name="I1" />
            <blockpin signalname="D" name="I2" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_3">
            <blockpin signalname="Rn" name="I0" />
            <blockpin signalname="Cp" name="I1" />
            <blockpin signalname="XLXN_3" name="I2" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_4">
            <blockpin signalname="Cp" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="XLXN_2" name="I2" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_7">
            <blockpin signalname="Qn" name="I0" />
            <blockpin signalname="XLXN_2" name="I1" />
            <blockpin signalname="Sn" name="I2" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_8">
            <blockpin signalname="Rn" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="Q" name="I2" />
            <blockpin signalname="Qn" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1040" y="1008" name="XLXI_1" orien="R0" />
        <instance x="1040" y="1424" name="XLXI_2" orien="R0" />
        <instance x="1648" y="1008" name="XLXI_3" orien="R0" />
        <instance x="1648" y="1424" name="XLXI_4" orien="R0" />
        <instance x="2336" y="1008" name="XLXI_7" orien="R0" />
        <instance x="2336" y="1424" name="XLXI_8" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1040" y1="800" y2="816" x1="1040" />
            <wire x2="2064" y1="800" y2="800" x1="1040" />
            <wire x2="2064" y1="800" y2="880" x1="2064" />
            <wire x2="2336" y1="880" y2="880" x1="2064" />
            <wire x2="2064" y1="880" y2="1056" x1="2064" />
            <wire x2="1648" y1="1056" y2="1232" x1="1648" />
            <wire x2="2064" y1="1056" y2="1056" x1="1648" />
            <wire x2="2064" y1="880" y2="880" x1="1904" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1472" y1="880" y2="880" x1="1296" />
            <wire x2="1472" y1="816" y2="880" x1="1472" />
            <wire x2="1648" y1="816" y2="816" x1="1472" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1040" y1="944" y2="1120" x1="1040" />
            <wire x2="1376" y1="1120" y2="1120" x1="1040" />
            <wire x2="1376" y1="1120" y2="1296" x1="1376" />
            <wire x2="1648" y1="1296" y2="1296" x1="1376" />
            <wire x2="1376" y1="1296" y2="1296" x1="1296" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1040" y1="1360" y2="1376" x1="1040" />
            <wire x2="2064" y1="1376" y2="1376" x1="1040" />
            <wire x2="2064" y1="1296" y2="1296" x1="1904" />
            <wire x2="2336" y1="1296" y2="1296" x1="2064" />
            <wire x2="2064" y1="1296" y2="1376" x1="2064" />
        </branch>
        <branch name="Q">
            <wire x2="2336" y1="1056" y2="1232" x1="2336" />
            <wire x2="2656" y1="1056" y2="1056" x1="2336" />
            <wire x2="2656" y1="880" y2="880" x1="2592" />
            <wire x2="2832" y1="880" y2="880" x1="2656" />
            <wire x2="2656" y1="880" y2="1056" x1="2656" />
        </branch>
        <branch name="Qn">
            <wire x2="2336" y1="944" y2="944" x1="2256" />
            <wire x2="2256" y1="944" y2="1136" x1="2256" />
            <wire x2="2656" y1="1136" y2="1136" x1="2256" />
            <wire x2="2656" y1="1136" y2="1296" x1="2656" />
            <wire x2="2832" y1="1296" y2="1296" x1="2656" />
            <wire x2="2656" y1="1296" y2="1296" x1="2592" />
        </branch>
        <branch name="Sn">
            <wire x2="1008" y1="704" y2="704" x1="928" />
            <wire x2="2336" y1="704" y2="704" x1="1008" />
            <wire x2="2336" y1="704" y2="816" x1="2336" />
            <wire x2="1008" y1="704" y2="880" x1="1008" />
            <wire x2="1040" y1="880" y2="880" x1="1008" />
        </branch>
        <branch name="Cp">
            <wire x2="1632" y1="1056" y2="1056" x1="928" />
            <wire x2="1632" y1="1056" y2="1360" x1="1632" />
            <wire x2="1648" y1="1360" y2="1360" x1="1632" />
            <wire x2="1648" y1="880" y2="880" x1="1632" />
            <wire x2="1632" y1="880" y2="1056" x1="1632" />
        </branch>
        <branch name="D">
            <wire x2="1040" y1="1232" y2="1232" x1="928" />
        </branch>
        <branch name="Rn">
            <wire x2="1008" y1="1472" y2="1472" x1="928" />
            <wire x2="1520" y1="1472" y2="1472" x1="1008" />
            <wire x2="2336" y1="1472" y2="1472" x1="1520" />
            <wire x2="1040" y1="1296" y2="1296" x1="1008" />
            <wire x2="1008" y1="1296" y2="1472" x1="1008" />
            <wire x2="1648" y1="944" y2="944" x1="1520" />
            <wire x2="1520" y1="944" y2="1472" x1="1520" />
            <wire x2="2336" y1="1360" y2="1472" x1="2336" />
        </branch>
        <iomarker fontsize="28" x="928" y="704" name="Sn" orien="R180" />
        <iomarker fontsize="28" x="928" y="1056" name="Cp" orien="R180" />
        <iomarker fontsize="28" x="928" y="1232" name="D" orien="R180" />
        <iomarker fontsize="28" x="928" y="1472" name="Rn" orien="R180" />
        <iomarker fontsize="28" x="2832" y="880" name="Q" orien="R0" />
        <iomarker fontsize="28" x="2832" y="1296" name="Qn" orien="R0" />
    </sheet>
</drawing>