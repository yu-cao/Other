//Top的解释请参考报告的原理图即可理解，在此不做赘述
module top(clk_100mhz, K_COL, RSTN, SW, AN, Buzzer, SEG);
    input clk_100mhz;
    input [3:0] K_COL;
    input RSTN;
    input [15:0] SW;
   output [3:0] AN;
   output Buzzer;
   output [7:0] SEG;
   
   wire [31:0] clkdiv;
   wire [1:0] SW_OK;
   wire [1:0] Q;
   wire [3:0] BTN;
   wire Right;
   wire clk;
   wire [2:0] mark;
   wire [7:0] Mark_Seg7;
   wire [7:0] Image;
   wire wrong;
   wire Level_Up;
   wire bu;
   
    level  M2 (.clk_100mhz(clk_100mhz), .level({SW[0], SW[1]}), .Level_Up(Level_Up), .clk(clk));
	random M3 (.clk(clk), .Q(Q[1:0]));
    dispnum M1(.Q(Q[1:0]), .SEG(Image[7:0]));
    score M5 (.clk(clk), .Right(Right), .Wrong(wrong), .Level_Up(Level_Up), .Mark(mark[2:0]));
    mark  M6 (.Mark(mark[2:0]), .Mark_Seg7(Mark_Seg7[7:0]));
    wrong  M4 (.BTN(K_COL[3:0]), .clk(clk), .Q(Q[1:0]), .Right(Right), .Wrong(wrong));
    display M7 (.clk(clkdiv[18]), .Image(Image[7:0]), .Mark(Mark_Seg7[7:0]), .AN(AN[3:0]), .Segment(SEG[7:0]));
    INV XLXI_28(.I(wrong), .O(bu));
    INV XLXI_99(.I(bu), .O(Buzzer));
    clkdiv  m8 (.clk(clk_100mhz), .rst(RSTN), .clkdiv(clkdiv[31:0]));
endmodule

//关于M1的解释请参考报告的原理图也即可理解，在此也不多做赘述
module M1(Q, SEG);
    input [1:0] Q;
    output [7:0] SEG;
   
    wire G0;
    wire V5;
    wire XLXN_65;
    wire XLXN_90;
    wire XLXN_91;
    wire XLXN_97;
   
    INV XLXI_18 (.I(Q[1]),.O(XLXN_65));
    INV XLXI_19 (.I(Q[0]),.O(XLXN_90));
    AND2 XLXI_32 (.I0(Q[0]),.I1(Q[1]),.O(XLXN_97));
    AND2 XLXI_35 (.I0(Q[0]),.I1(XLXN_65),.O(XLXN_91));
    VCC XLXI_36 (.P(V5));
    GND XLXI_37 (.G(G0));
    BUF XLXI_38 (.I(V5),.O(SEG[0]));
    BUF XLXI_39 (.I(G0),.O(SEG[4]));
    BUF XLXI_40 (.I(V5),.O(SEG[7]));
    BUF XLXI_41 (.I(Q[1]),.O(SEG[1]));
    BUF XLXI_42 (.I(Q[1]),.O(SEG[2]));
    BUF XLXI_43 (.I(XLXN_90),.O(SEG[6]));
    BUF XLXI_44 (.I(XLXN_91),.O(SEG[5]));
    BUF XLXI_48 (.I(XLXN_97),.O(SEG[3]));

endmodule

//这是报告中的M2模块的源代码，设计目标是难度设定
//可以在一开始设定好初始难度，也可以随着游戏的进行难度逐渐上升
module level(clk_100mhz,Level_Up,level, clk);
    input wire clk_100mhz;
    input wire Level_Up;
    input wire[1:0] level;
    output reg clk;//输出是一个寄存器，还是可以存在保持而且可以赋值与进行微操作

    reg [31:0] cnt;
    reg[1:0] lv_0 = 2'b00;
    reg[1:0] lv_1 = 2'b01;
    reg[1:0] lv_2 = 2'b10;
    reg[1:0] lv_3 = 2'b11;
    integer lv,n,nn=1,n1,n2;
    reg rst=1;

    //先依靠输入的level[1:0]确定初始的难度
    //每个level用lv数字表示
    always @* begin
    case (level)
        lv_0: begin
        lv = 1;
        end
        lv_1: begin
        lv = 2;
        end
        lv_2: begin
        lv = 4;
        end
        lv_3: begin
        lv = 8;
        end
    endcase//用以手动调节难度
    end

    //当Level_Up从0->1时，nn提高一个难度等级（nn最初是1，即level_0的难度）
    always @ (posedge Level_Up )begin
        nn<=nn*2;
        end//升级时难度增加

    always @ (posedge level[0] )begin
        n1<=nn;
    end

    always @ (negedge level[0])begin
        n2<=nn;
    end

    //n取n1与n2的大者
    always@*begin
        if(n1>n2)n<=n1;
        else n<=n2;
    end

	always @ (posedge clk_100mhz) begin
	    if (cnt < 8_000_000_0*n/lv/nn) begin //50M*(1/100M)S / lv / n = T/2
	        cnt <= cnt + 1;
	    end else begin
	    cnt <= 0;
	    clk <= ~clk; 
	    end

	    //初始化
	    if(rst)begin
            clk<=0;
            rst<=0;
        end 
	end

endmodule

//这是报告中的M2模块的源代码
//目的是产生随着clk的posedge触发产生近似随机的随机数
module random(
    input clk,
	output reg[1:0] Q
);
    wire CR = 1;
    wire[31:0]O;
    reg S1 = 1;
    reg S0 = 1;
    wire[31:0]PData = 32'b10010110100100110101011001101010;
    always @* begin
        Q={O[1],O[0]};
    end

    always@(posedge clk)begin
        S1 = 0;
    end

//包含8个DM74LS194模块，环状连接，目的是为了产生足够量的近似随机数
//DM74LS194模块的Verilog代码见下面用注释框起来的部分
DM74LS194 
SH0(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[31]), .SR(O[4]),
.A(PData[3]), .B(PData[2]), .C(PData[1]), .D(PData[0]),
.QA(O[3]), .QB(O[2]), .QC(O[1]), .QD(O[0])),
SH1(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[3]), .SR(O[8]),
.A(PData[7]), .B(PData[6]), .C(PData[5]), .D(PData[4]),
.QA(O[7]), .QB(O[6]), .QC(O[5]), .QD(O[4])),
SH2(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[7]), .SR(O[12]),
.A(PData[11]),.B(PData[10]),.C(PData[9]),.D(PData[8]),
.QA(O[11]), .QB(O[10]), .QC(O[9]), .QD(O[8])),
SH3(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[11]), .SR(O[16]),
.A(PData[15]),.B(PData[14]),.C(PData[13]),.D(PData[12]),
.QA(O[15]), .QB(O[14]), .QC(O[13]), .QD(O[12]));
DM74LS194 
SH4(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[15]), .SR(O[20]),
.A(PData[19]),.B(PData[18]),.C(PData[17]),.D(PData[16]),
.QA(O[19]), .QB(O[18]), .QC(O[17]), .QD(O[16])),
SH5(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[19]), .SR(O[24]),
.A(PData[23]),.B(PData[22]),.C(PData[21]),.D(PData[20]),
.QA(O[23]), .QB(O[22]), .QC(O[21]), .QD(O[20])),
SH6(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[23]), .SR(O[28]),
.A(PData[27]),.B(PData[26]),.C(PData[25]),.D(PData[24]),
.QA(O[27]), .QB(O[26]), .QC(O[25]), .QD(O[24])),
SH7(.clk(clk), .CR(CR), .S1(S1), .S0(S0), .SL(O[27]), .SR(O[0]),
.A(PData[31]),.B(PData[30]),.C(PData[29]),.D(PData[28]),
.QA(O[31]), .QB(O[30]), .QC(O[29]), .QD(O[28]));

endmodule

//////////////////////////////////////////////////////////////////////////////////
module DM74LS194( 
    input clk,
    input CR,
    input S0,
    input S1,
    input A,
    input B,
    input C,
    input D,
    input SL,
    input SR,
    output QD,
    output QC,
    output QB,
    output QA
);

INV GS1(.I(S1), .O(nS1)),GS0(.I(S0), .O(nS0));
//此处的MB_DFF实现依靠原理图形式完成，原理图见设计报告所示
MB_DFF  Shift0(.Cp(clk), .D(D0), .Rn(CR), .Sn(1'b1), .Q(QD), .Qn()),
        Shift1(.Cp(clk), .D(D1), .Rn(CR), .Sn(1'b1), .Q(QC), .Qn()),
        Shift2(.Cp(clk), .D(D2), .Rn(CR), .Sn(1'b1), .Q(QB), .Qn()),
        Shift3(.Cp(clk), .D(D3), .Rn(CR), .Sn(1'b1), .Q(QA), .Qn());
OR4 GD0(.I0(HD0), .I1(RD0), .I2(LD0), .I3(PD0), .O(D0)),
    GD1(.I0(HD1), .I1(RD1), .I2(LD1), .I3(PD1), .O(D1)),
    GD2(.I0(HD2), .I1(RD2), .I2(LD2), .I3(PD2), .O(D2)),
    GD3(.I0(HD3), .I1(RD3), .I2(LD3), .I3(PD3), .O(D3));
AND3 GH0(.I0(nS0), .I1(nS1), .I2(QD), .O(HD0)),
    GH1(.I0(nS0), .I1(nS1), .I2(QC), .O(HD1)),
    GH2(.I0(nS0), .I1(nS1), .I2(QB), .O(HD2)),
    GH3(.I0(nS0), .I1(nS1), .I2(QA), .O(HD3));
AND3 SR0(.I0(nS0), .I1(S1), .I2(SL), .O(RD0)),
    SR1(.I0(nS0), .I1(S1), .I2(QD), .O(RD1)),
    SR2(.I0(nS0), .I1(S1), .I2(QC), .O(RD2)),
    SR3(.I0(nS0), .I1(S1), .I2(QB), .O(RD3));
AND3 SL0(.I0(S0), .I1(nS1), .I2(QC), .O(LD0)),
    SL1(.I0(S0), .I1(nS1), .I2(QB), .O(LD1)),
    SL2(.I0(S0), .I1(nS1), .I2(QA), .O(LD2)),
    SL3(.I0(S0), .I1(nS1), .I2(SR), .O(LD3));
AND3 P0(.I0(S0), .I1(S1), .I2(D), .O(PD0)),
    P1(.I0(S0), .I1(S1), .I2(C), .O(PD1)),
    P2(.I0(S0), .I1(S1), .I2(B), .O(PD2)),
    P3(.I0(S0), .I1(S1), .I2(A), .O(PD3));

endmodule
//////////////////////////////////////////////////////////////////////////////////

//此处是报告中M4模块的代码
//设计目的是判断玩家按键是否正确的模块，这也是整个工程的core模块
//Q是M3模块生成提供的一个随机数，而BTN是用户的实际按键
module wrong(
	input clk,
    input [3:0] BTN,
    input [1:0] Q,
    output Right,
	output reg Wrong
);
	reg x;
    reg Wrong2;
    reg Wrong21;
    reg Wrong22;
    reg Wrong23;
    reg Wrong24;

//对于所有的输入量都进行1to2的decoder
INV A(.I(Q[0]),.O(nQ0)),
    B(.I(Q[1]),.O(nQ1)),
    C(.I(BTN[0]),.O(nB0)),
    D(.I(BTN[1]),.O(nB1)),
    E(.I(BTN[2]),.O(nB2)),
    F(.I(BTN[3]),.O(nB3));

//找到每个的最小项
//注意，AND3与AND2的组件名称是有一定的内在联系的，最后合成一个整体
//如在G1，G2中体现的是Q=00，BTN输入时0001，在AND2中把这个合起来打包成为一个最小项
//下面注释都写在AND3处了
AND3 G1(.I0(nQ1),.I1(nQ0),.I2(BTN[0]),.O(W1)),//Q=00,BTN=0001
    G2(.I0(nB1),.I1(nB2),.I2(nB3),.O(W2)),
    H1(.I0(nQ1),.I1(Q[0]),.I2(nB0),.O(X1)),//Q=01,BTN=0010
    H2(.I0(BTN[1]),.I1(nB2),.I2(nB3),.O(X2)),
    I1(.I0(Q[1]),.I1(nQ0),.I2(nB0),.O(Y1)),//Q=10,,BTN=0100
    I2(.I0(nB1),.I1(BTN[2]),.I2(nB3),.O(Y2)),
    J1(.I0(Q[1]),.I1(Q[0]),.I2(nB0),.O(Z1)),//Q=11,BTN=1000
    J2(.I0(nB1),.I1(nB2),.I2(BTN[3]),.O(Z2));
AND2 G(.I0(W1),.I1(W2),.O(W)),
    H(.I0(X1),.I1(X2),.O(X)),
    I(.I0(Y1),.I1(Y2),.O(Y)),
    J(.I0(Z1),.I1(Z2),.O(Z));
    //所以上述代码定义了这四种正确的输出:W,X,Y,Z

OR3 K(.I0(BTN[1]),.I1(BTN[2]),.I2(BTN[3]),.O(WW)),//类型W要求是BTN=0001,所以只要前三个有哪一个是1，就不可能是W那种类型了
    L(.I0(BTN[0]),.I1(BTN[2]),.I2(BTN[3]),.O(XX)),
    M(.I0(BTN[0]),.I1(BTN[1]),.I2(BTN[3]),.O(YY)),
    N(.I0(BTN[0]),.I1(BTN[1]),.I2(BTN[2]),.O(ZZ));
AND3 O(.I0(nQ1),.I1(nQ0),.I2(WW),.O(WWW)),//当Q=00时，正确输入显然是W类型，而输入时非W类型的，所以输入时WWW，代表错误
    P(.I0(nQ1),.I1(Q[0]),.I2(XX),.O(XXX)),
    QQ(.I0(Q[1]),.I1(nQ0),.I2(YY),.O(YYY)),
    R(.I0(Q[1]),.I1(Q[0]),.I2(ZZ),.O(ZZZ));

//组合最小项
OR4 S(.I0(W),.I1(X),.I2(Y),.I3(Z),.O(Right)),
    T(.I0(WWW),.I1(XXX),.I2(YYY),.I3(ZZZ),.O(Wrong1));
	
    //当时钟刷新或者出现了right的情况的时候
    always@(posedge clk or posedge Right)begin
        if(Right)x<=1;//当按下正确按键时,进入状态1,表示已曾正确按下
        else begin
            if(x==0)begin Wrong2<=1;//当时间到却没有按下正确按键时，Wrong2=1持续#5然后恢复为0
               #5;
               Wrong2<=0;
            end
        //当x==1时，不经历上述的if过程就刷新到x=0
        //当x==0时，经历上述过程再刷新回0
        x<=0;
        end//若始终未按下正确按键,判为Wrong;并回到状态0
    end

    //always@*意味着敏感参数列表
    //代表着always块里面的输入信号一旦改变就开始运行always
    always@*begin
        if(Wrong2==1)Wrong <= Wrong2;//当Wrong2==1一旦出现就将其捕获植入Wrong的输出之中
        else Wrong <= Wrong1;//输出Wrong1（==1代表玩家输入了与要求不一致的按键）
    end//若始终未按下正确按键,判为Wrong;并回到状态0

endmodule

//该模块是报告中的M5模块的源代码
//该模块为处理分数以及等级的模块
module score(
    input clk,
    input Right,
    input Wrong,
    output reg[2:0] Mark,
    output reg Level_Up
);

    reg rst=1;
    reg[1:0] temp;
    integer x;

    always @* begin
        if(rst)begin//初始化
        Mark<=0;
        Level_Up<=0;
        rst<=0;
        end

        if(Wrong)begin
        Mark<=0;
        end

        if(Mark==7)begin
        Level_Up <= 1;
        Mark <= 0;
        #5;
        Level_Up <= 0;
        end

        if(Right&&x)begin
        Mark <= Mark + 1;
        x <= 0;
        end//分数满8分时进入下一等级,并重新积分

        if(temp==2'b01)//Result=1的信号到达以后Mark自增1并且在clk上升缘来到之前（也就是显示的符号改变之前）再也不自增1.
        x<=1;
    end

    always@*begin
        temp[0]<=clk;//temp[0]恒代表clk
        temp[1]<=temp[0];
    end//为了完成上面的目标在一个clk里面锁死x

endmodule

//该模块是报告中的M6模块的源代码
//该模块将三位的Mark数值转化为七段码以准备显示
module mark(
    input [2:0]Mark,
    output reg[7:0] Mark_Seg7
);

    always@*begin
        Mark_Seg7[7]<=1;
        Mark_Seg7[6:0]<={g,f,e,d,c,b,a};
    end

    //1to2的decoder
    INV	M0(.I(Mark[0]),.O(nm0)),
        M1(.I(Mark[1]),.O(nm1)),
        M2(.I(Mark[2]),.O(nm2));

    //此处请对应AN上的七段码真值表进行查阅，也就类似于平时实验中的DispNum模块
    AND3 A1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(a1)),
        A2(.I0(nm0),.I1(nm1),.I2(Mark[2]),.O(a2));
    AND3 B1(.I0(Mark[0]),.I1(nm1),.I2(Mark[2]),.O(b1)),
        B2(.I0(nm0),.I1(Mark[1]),.I2(Mark[2]),.O(b2));
    AND3 C1(.I0(nm0),.I1(Mark[1]),.I2(nm2),.O(c));
    AND3 D1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(d1)),
        D2(.I0(nm0),.I1(nm1),.I2(Mark[2]),.O(d2)),
        D3(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(d3));
    AND3  E1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(e1)),
        E2(.I0(Mark[0]),.I1(Mark[1]),.I2(nm2),.O(e2)),
        E3(.I0(nm0),.I1(nm1),.I2(Mark[2]),.O(e3)),
        E4(.I0(Mark[0]),.I1(nm1),.I2(Mark[2]),.O(e4)),
        E5(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(e5));
    AND3 F1(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(f1)),
        F2(.I0(nm0),.I1(Mark[1]),.I2(nm2),.O(f2)),
        F3(.I0(Mark[0]),.I1(Mark[1]),.I2(nm2),.O(f3)),
        F4(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(f4));
    AND3 G1(.I0(nm0),.I1(nm1),.I2(nm2),.O(g1)),
        G2(.I0(Mark[0]),.I1(nm1),.I2(nm2),.O(g2)),
        G3(.I0(Mark[0]),.I1(Mark[1]),.I2(Mark[2]),.O(g3));
    OR2	A(.I0(a1),.I1(a2),.O(a));
    OR2	B(.I0(b1),.I1(b2),.O(b));
    OR3	D(.I0(d1),.I1(d2),.I2(d3),.O(d));
    OR5	E(.I0(e1),.I1(e2),.I2(e3),.I3(e4),.I4(e5),.O(e));
    OR4	F(.I0(f1),.I1(f2),.I2(f3),.I3(f4),.O(f));
    OR3	G(.I0(g1),.I1(g2),.I2(g3),.O(g));

endmodule

//该模块是报告中的M7模块的源代码
//该模块为七段码扫描模块，目的是将显示的U、d、L、R显示上去，同时显示上去得分情况
module display(
    input [7:0] Image,//M1模块的输出，就是Q的七段码
    input [7:0] Mark,//M6模块的输出，是获得成绩的七段码
    input clk,
    output reg[7:0] Segment,
    output reg[3:0] AN
);

    //当clk=0时显示Image的图像；当clk=1时显示Mark的数值
    //当clk为一个合理值左右时，依靠人眼的视觉暂留所以显示仿佛两个都在显示板上
    always @* begin //信号变化触发 (组合电路不用时钟触发)
    case (clk)
        1'b0 : begin Segment <= Image; AN <= 4'b0111; end
        1'b1 : begin Segment <= Mark; AN <= 4'b1110; end
    endcase
    end

endmodule

//clkdiv模块，与之前实验使用的clkdiv模块完全相同，不需要改写
//该模块设计目的是为了将主板给的时钟频率降低，
//设计者自行选择需要的时钟频率
module clkdiv(
    input clk,
    input rst,
	output reg[31:0] clkdiv
); 
	 always @ (posedge clk or posedge rst) begin
	    if(rst)
		    clkdiv<=0;
		else
		    clkdiv<=clkdiv+1'b1;
	end

endmodule



//UCF文件
NET "clk_100mhz"		LOC = AC18      | IOSTANDARD = LVCMOS18 ;
NET "clk_100mhz"		TNM_NET = TM_CLK ;
TIMESPEC TS_CLK_100M = PERIOD "TM_CLK"  10 ns HIGH 50%;
NET "RSTN"		LOC = W13      | IOSTANDARD = LVCMOS18 ;

NET "SW[0]"          Loc = AA10 | IOSTANDARD = LVCMOS15;
NET "SW[1]"          Loc = AB10 | IOSTANDARD = LVCMOS15;

#因为使用阵列式键盘clk出现一些bug，我们最后选择用SW代替键盘操作
NET "K_COL[0]"       Loc = AA13 | IOSTANDARD = LVCMOS15;#using SW[2]
NET "K_COL[1]"       Loc = AA12 | IOSTANDARD = LVCMOS15;#using SW[3]
NET "K_COL[2]"       Loc = Y13  | IOSTANDARD = LVCMOS15;#using SW[4]
NET "K_COL[3]"       Loc = Y12  | IOSTANDARD = LVCMOS15;#using SW[5]

NET "Buzzer"         LOC = AF24 |  IOSTANDARD = LVCMOS33;
NET "SEG[0]"         LOC = AB22 | IOSTANDARD = LVCMOS33 ;#a
NET "SEG[1]"         LOC = AD24 | IOSTANDARD = LVCMOS33 ;#b
NET "SEG[2]"         LOC = AD23 | IOSTANDARD = LVCMOS33 ;#c
NET "SEG[3]"         LOC = Y21  | IOSTANDARD = LVCMOS33 ;#d
NET "SEG[4]"         LOC = W20  | IOSTANDARD = LVCMOS33 ;#e
NET "SEG[5]"         LOC = AC24 | IOSTANDARD = LVCMOS33 ;#f
NET "SEG[6]"         LOC = AC23 | IOSTANDARD = LVCMOS33 ;#g
NET "SEG[7]"         LOC = AA22 | IOSTANDARD = LVCMOS33 ;#p
NET "AN[0]"       	LOC = AD21 | IOSTANDARD = LVCMOS33 ;
NET "AN[1]"       	LOC = AC21 | IOSTANDARD = LVCMOS33 ;
NET "AN[2]" 	      LOC = AB21 | IOSTANDARD = LVCMOS33 ;
NET "AN[3]" 	      LOC = AC22 | IOSTANDARD = LVCMOS33 ;