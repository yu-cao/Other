<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="A" />
        <signal name="B" />
        <signal name="C" />
        <signal name="S" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="Co" />
        <port polarity="Input" name="A" />
        <port polarity="Input" name="B" />
        <port polarity="Input" name="C" />
        <port polarity="Output" name="S" />
        <port polarity="Output" name="Co" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="B" name="I0" />
            <blockpin signalname="A" name="I1" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="B" name="I1" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="A" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_4">
            <blockpin signalname="B" name="I0" />
            <blockpin signalname="A" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_5">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="XLXN_1" name="I1" />
            <blockpin signalname="S" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_6">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_7" name="I2" />
            <blockpin signalname="Co" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1136" y="1408" name="XLXI_1" orien="R0" />
        <instance x="1136" y="1632" name="XLXI_2" orien="R0" />
        <instance x="1136" y="1888" name="XLXI_3" orien="R0" />
        <instance x="1104" y="384" name="XLXI_4" orien="R0" />
        <instance x="1600" y="704" name="XLXI_5" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="1472" y1="288" y2="288" x1="1360" />
            <wire x2="1472" y1="288" y2="576" x1="1472" />
            <wire x2="1600" y1="576" y2="576" x1="1472" />
        </branch>
        <branch name="A">
            <wire x2="592" y1="256" y2="256" x1="528" />
            <wire x2="640" y1="256" y2="256" x1="592" />
            <wire x2="1104" y1="256" y2="256" x1="640" />
            <wire x2="640" y1="256" y2="1280" x1="640" />
            <wire x2="640" y1="1280" y2="1760" x1="640" />
            <wire x2="1136" y1="1760" y2="1760" x1="640" />
            <wire x2="1136" y1="1280" y2="1280" x1="640" />
        </branch>
        <branch name="B">
            <wire x2="720" y1="320" y2="320" x1="528" />
            <wire x2="1104" y1="320" y2="320" x1="720" />
            <wire x2="720" y1="320" y2="1344" x1="720" />
            <wire x2="720" y1="1344" y2="1504" x1="720" />
            <wire x2="1136" y1="1504" y2="1504" x1="720" />
            <wire x2="1136" y1="1344" y2="1344" x1="720" />
        </branch>
        <iomarker fontsize="28" x="528" y="256" name="A" orien="R180" />
        <iomarker fontsize="28" x="528" y="320" name="B" orien="R180" />
        <branch name="C">
            <wire x2="848" y1="640" y2="640" x1="528" />
            <wire x2="1584" y1="640" y2="640" x1="848" />
            <wire x2="1600" y1="640" y2="640" x1="1584" />
            <wire x2="848" y1="640" y2="1568" x1="848" />
            <wire x2="848" y1="1568" y2="1824" x1="848" />
            <wire x2="1136" y1="1824" y2="1824" x1="848" />
            <wire x2="1136" y1="1568" y2="1568" x1="848" />
        </branch>
        <iomarker fontsize="28" x="528" y="640" name="C" orien="R180" />
        <branch name="S">
            <wire x2="2160" y1="608" y2="608" x1="1856" />
        </branch>
        <iomarker fontsize="28" x="2160" y="608" name="S" orien="R0" />
        <instance x="1968" y="1664" name="XLXI_6" orien="R0" />
        <branch name="XLXN_6">
            <wire x2="1968" y1="1536" y2="1536" x1="1392" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1968" y1="1312" y2="1312" x1="1392" />
            <wire x2="1968" y1="1312" y2="1472" x1="1968" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1968" y1="1792" y2="1792" x1="1392" />
            <wire x2="1968" y1="1600" y2="1792" x1="1968" />
        </branch>
        <branch name="Co">
            <wire x2="2608" y1="1536" y2="1536" x1="2224" />
        </branch>
        <iomarker fontsize="28" x="2608" y="1536" name="Co" orien="R0" />
    </sheet>
</drawing>