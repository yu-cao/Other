`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:07:07 11/29/2017 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Top(
	input wire clk,
	input wire [3:0]SW,
	input wire [1:0]SW2,
	output wire [3:0]AN,
	output wire [7:0]SEGMENT,
	//output wire [7:0]LED,
	output wire Buzzer
	);
	 
	wire [15:0] num;
	wire [1:0] btn_out;
	wire [3:0] C;
	wire Co;
	wire [31:0] clk_div;

	pbdebounce m0(clk_div[17],SW[0],btn_out[0]);
 	pbdebounce m1(clk_div[17],SW[1],btn_out[1]);
	clkdiv m2(clk,0,clk_div);
	
	CreateNumber({2'b0,btn_out},{2'b0,SW[3:2]}, num);
		
	myALU m5(.A(num[3:0]),.B(num[7:4]),.S(SW2),.C(C),.Co(Co));
	DispNum m6(clk, {num[7:0],3'b0,Co,C}, 4'b0, 4'b0, 1'b0, AN, SEGMENT);
	
	//assign LED = 8'b11111111;
	assign Buzzer = 1'b1;
endmodule
