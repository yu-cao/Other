// Verilog test fixture created from schematic C:\Users\CST\Desktop\2017-11-22\MyALU\myALU.sch - Wed Nov 29 14:00:10 2017

`timescale 1ns / 1ps

module myALU_myALU_sch_tb();

// Inputs
   reg [3:0] A;
   reg [3:0] B;
   reg [1:0] S;

// Output
   wire Co;
   wire [3:0] C;

// Bidirs

// Instantiate the UUT
   myALU UUT (
		.A(A), 
		.B(B), 
		.S(S), 
		.Co(Co), 
		.C(C)
   );
// Initialize Inputs
   //`ifdef auto_init
       initial begin
		A = 0;
		B = 0;
		S = 0;
        
        A[0]=0;
        A[1]=1;
        A[2]=0;
        A[3]=1;
        
        B[0]=1;
        B[1]=1;
        B[2]=1;
        B[3]=0;
        S[0]=0;
        S[1]=0;
        #50
        
        B[2]=0;
        #50
        
        S[0]=1;
        #50
        
        S[1]=1;
        S[0]=0;
        #50
        
        S[0]=1;
        S[1]=1;
    end
   //`endif
endmodule
