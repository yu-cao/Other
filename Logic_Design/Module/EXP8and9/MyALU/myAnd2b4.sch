<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="A(3:0)" />
        <signal name="B(3:0)" />
        <signal name="A(3)" />
        <signal name="A(2)" />
        <signal name="A(1)" />
        <signal name="A(0)" />
        <signal name="B(3)" />
        <signal name="B(2)" />
        <signal name="B(1)" />
        <signal name="B(0)" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="C(3:0)" />
        <signal name="C(3)" />
        <signal name="C(2)" />
        <signal name="C(1)" />
        <signal name="C(0)" />
        <port polarity="Input" name="A(3:0)" />
        <port polarity="Input" name="B(3:0)" />
        <port polarity="Output" name="C(3:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="B(0)" name="I0" />
            <blockpin signalname="A(0)" name="I1" />
            <blockpin signalname="C(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="B(1)" name="I0" />
            <blockpin signalname="A(1)" name="I1" />
            <blockpin signalname="C(1)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="B(2)" name="I0" />
            <blockpin signalname="A(2)" name="I1" />
            <blockpin signalname="C(2)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="B(3)" name="I0" />
            <blockpin signalname="A(3)" name="I1" />
            <blockpin signalname="C(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="A(3:0)">
            <wire x2="656" y1="448" y2="448" x1="416" />
            <wire x2="656" y1="448" y2="496" x1="656" />
            <wire x2="656" y1="496" y2="576" x1="656" />
            <wire x2="656" y1="576" y2="656" x1="656" />
            <wire x2="656" y1="656" y2="736" x1="656" />
            <wire x2="656" y1="736" y2="784" x1="656" />
        </branch>
        <branch name="B(3:0)">
            <wire x2="672" y1="976" y2="976" x1="432" />
            <wire x2="672" y1="976" y2="1008" x1="672" />
            <wire x2="672" y1="1008" y2="1040" x1="672" />
            <wire x2="672" y1="1040" y2="1104" x1="672" />
            <wire x2="672" y1="1104" y2="1168" x1="672" />
            <wire x2="672" y1="1168" y2="1216" x1="672" />
            <wire x2="672" y1="1216" y2="1216" x1="656" />
        </branch>
        <iomarker fontsize="28" x="416" y="448" name="A(3:0)" orien="R180" />
        <iomarker fontsize="28" x="432" y="976" name="B(3:0)" orien="R180" />
        <instance x="1392" y="592" name="XLXI_1" orien="R0" />
        <instance x="1392" y="800" name="XLXI_2" orien="R0" />
        <instance x="1392" y="992" name="XLXI_3" orien="R0" />
        <instance x="1392" y="1184" name="XLXI_4" orien="R0" />
        <bustap x2="752" y1="736" y2="736" x1="656" />
        <branch name="A(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="776" y="736" type="branch" />
            <wire x2="776" y1="736" y2="736" x1="752" />
            <wire x2="800" y1="736" y2="736" x1="776" />
            <wire x2="1088" y1="736" y2="736" x1="800" />
            <wire x2="1088" y1="736" y2="1056" x1="1088" />
            <wire x2="1392" y1="1056" y2="1056" x1="1088" />
        </branch>
        <bustap x2="752" y1="656" y2="656" x1="656" />
        <branch name="A(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="776" y="656" type="branch" />
            <wire x2="776" y1="656" y2="656" x1="752" />
            <wire x2="800" y1="656" y2="656" x1="776" />
            <wire x2="992" y1="656" y2="656" x1="800" />
            <wire x2="992" y1="656" y2="864" x1="992" />
            <wire x2="1392" y1="864" y2="864" x1="992" />
        </branch>
        <bustap x2="752" y1="576" y2="576" x1="656" />
        <branch name="A(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="768" y="576" type="branch" />
            <wire x2="768" y1="576" y2="576" x1="752" />
            <wire x2="1008" y1="576" y2="576" x1="768" />
            <wire x2="1008" y1="576" y2="672" x1="1008" />
            <wire x2="1392" y1="672" y2="672" x1="1008" />
        </branch>
        <bustap x2="752" y1="496" y2="496" x1="656" />
        <branch name="A(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="768" y="496" type="branch" />
            <wire x2="768" y1="496" y2="496" x1="752" />
            <wire x2="784" y1="496" y2="496" x1="768" />
            <wire x2="1392" y1="464" y2="464" x1="784" />
            <wire x2="784" y1="464" y2="496" x1="784" />
        </branch>
        <bustap x2="768" y1="1168" y2="1168" x1="672" />
        <branch name="B(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="800" y="1168" type="branch" />
            <wire x2="800" y1="1168" y2="1168" x1="768" />
            <wire x2="832" y1="1168" y2="1168" x1="800" />
            <wire x2="1392" y1="1120" y2="1120" x1="832" />
            <wire x2="832" y1="1120" y2="1168" x1="832" />
        </branch>
        <bustap x2="768" y1="1104" y2="1104" x1="672" />
        <branch name="B(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="792" y="1104" type="branch" />
            <wire x2="792" y1="1104" y2="1104" x1="768" />
            <wire x2="816" y1="1104" y2="1104" x1="792" />
            <wire x2="1072" y1="1104" y2="1104" x1="816" />
            <wire x2="1072" y1="928" y2="1104" x1="1072" />
            <wire x2="1392" y1="928" y2="928" x1="1072" />
        </branch>
        <bustap x2="768" y1="1040" y2="1040" x1="672" />
        <branch name="B(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="784" y="1040" type="branch" />
            <wire x2="784" y1="1040" y2="1040" x1="768" />
            <wire x2="800" y1="1040" y2="1040" x1="784" />
            <wire x2="1104" y1="1040" y2="1040" x1="800" />
            <wire x2="1104" y1="736" y2="1040" x1="1104" />
            <wire x2="1392" y1="736" y2="736" x1="1104" />
        </branch>
        <bustap x2="768" y1="1008" y2="1008" x1="672" />
        <branch name="B(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="792" y="1008" type="branch" />
            <wire x2="792" y1="1008" y2="1008" x1="768" />
            <wire x2="816" y1="1008" y2="1008" x1="792" />
            <wire x2="1392" y1="528" y2="528" x1="816" />
            <wire x2="816" y1="528" y2="1008" x1="816" />
        </branch>
        <branch name="C(3:0)">
            <wire x2="1952" y1="448" y2="464" x1="1952" />
            <wire x2="1968" y1="464" y2="464" x1="1952" />
            <wire x2="1968" y1="464" y2="496" x1="1968" />
            <wire x2="1968" y1="496" y2="704" x1="1968" />
            <wire x2="1968" y1="704" y2="896" x1="1968" />
            <wire x2="1968" y1="896" y2="1088" x1="1968" />
            <wire x2="1968" y1="1088" y2="1200" x1="1968" />
            <wire x2="2160" y1="448" y2="448" x1="1952" />
            <wire x2="2160" y1="448" y2="464" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2160" y="464" name="C(3:0)" orien="R90" />
        <bustap x2="1872" y1="1088" y2="1088" x1="1968" />
        <branch name="C(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1760" y="1088" type="branch" />
            <wire x2="1760" y1="1088" y2="1088" x1="1648" />
            <wire x2="1872" y1="1088" y2="1088" x1="1760" />
        </branch>
        <bustap x2="1872" y1="896" y2="896" x1="1968" />
        <branch name="C(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1760" y="896" type="branch" />
            <wire x2="1760" y1="896" y2="896" x1="1648" />
            <wire x2="1872" y1="896" y2="896" x1="1760" />
        </branch>
        <bustap x2="1872" y1="704" y2="704" x1="1968" />
        <branch name="C(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1760" y="704" type="branch" />
            <wire x2="1760" y1="704" y2="704" x1="1648" />
            <wire x2="1872" y1="704" y2="704" x1="1760" />
        </branch>
        <bustap x2="1872" y1="496" y2="496" x1="1968" />
        <branch name="C(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1760" y="496" type="branch" />
            <wire x2="1760" y1="496" y2="496" x1="1648" />
            <wire x2="1872" y1="496" y2="496" x1="1760" />
        </branch>
    </sheet>
</drawing>