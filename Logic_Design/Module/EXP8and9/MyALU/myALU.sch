<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="A(3:0)" />
        <signal name="B(3:0)" />
        <signal name="S(1:0)" />
        <signal name="S(0)" />
        <signal name="XLXN_7" />
        <signal name="XLXN_9(3:0)" />
        <signal name="XLXN_10(3:0)" />
        <signal name="XLXN_11(3:0)" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="Co" />
        <signal name="C(3:0)" />
        <port polarity="Input" name="A(3:0)" />
        <port polarity="Input" name="B(3:0)" />
        <port polarity="Input" name="S(1:0)" />
        <port polarity="Output" name="Co" />
        <port polarity="Output" name="C(3:0)" />
        <blockdef name="myAnd2b4">
            <timestamp>2017-11-29T5:22:13</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="myOr2b4">
            <timestamp>2017-11-29T5:26:33</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="AddSub4b">
            <timestamp>2017-11-29T5:32:47</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Mux4to14b">
            <timestamp>2017-11-9T6:26:22</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="MUX4TO1">
            <timestamp>2017-11-9T9:3:12</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="myAnd2b4" name="XLXI_1">
            <blockpin signalname="A(3:0)" name="A(3:0)" />
            <blockpin signalname="B(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_10(3:0)" name="C(3:0)" />
        </block>
        <block symbolname="myOr2b4" name="XLXI_2">
            <blockpin signalname="A(3:0)" name="A(3:0)" />
            <blockpin signalname="B(3:0)" name="B(3:0)" />
            <blockpin signalname="XLXN_11(3:0)" name="C(3:0)" />
        </block>
        <block symbolname="AddSub4b" name="XLXI_3">
            <blockpin signalname="A(3:0)" name="A(3:0)" />
            <blockpin signalname="B(3:0)" name="B(3:0)" />
            <blockpin signalname="S(0)" name="Ctrl" />
            <blockpin signalname="XLXN_12" name="Co" />
            <blockpin signalname="XLXN_9(3:0)" name="S(3:0)" />
        </block>
        <block symbolname="Mux4to14b" name="XLXI_4">
            <blockpin signalname="S(1:0)" name="s(1:0)" />
            <blockpin signalname="XLXN_9(3:0)" name="I0(3:0)" />
            <blockpin signalname="XLXN_9(3:0)" name="I1(3:0)" />
            <blockpin signalname="XLXN_10(3:0)" name="I2(3:0)" />
            <blockpin signalname="XLXN_11(3:0)" name="I3(3:0)" />
            <blockpin signalname="C(3:0)" name="o(3:0)" />
        </block>
        <block symbolname="MUX4TO1" name="XLXI_5">
            <blockpin signalname="S(1:0)" name="s(1:0)" />
            <blockpin signalname="XLXN_12" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="XLXN_13" name="I2" />
            <blockpin signalname="XLXN_13" name="I3" />
            <blockpin signalname="Co" name="o" />
        </block>
        <block symbolname="gnd" name="XLXI_6">
            <blockpin signalname="XLXN_13" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1136" y="1504" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1136" y="1824" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1136" y="848" name="XLXI_3" orien="R0">
        </instance>
        <branch name="A(3:0)">
            <wire x2="768" y1="688" y2="688" x1="544" />
            <wire x2="1136" y1="688" y2="688" x1="768" />
            <wire x2="768" y1="688" y2="1408" x1="768" />
            <wire x2="768" y1="1408" y2="1728" x1="768" />
            <wire x2="1136" y1="1728" y2="1728" x1="768" />
            <wire x2="1136" y1="1408" y2="1408" x1="768" />
        </branch>
        <branch name="B(3:0)">
            <wire x2="688" y1="752" y2="752" x1="544" />
            <wire x2="1136" y1="752" y2="752" x1="688" />
            <wire x2="688" y1="752" y2="1472" x1="688" />
            <wire x2="688" y1="1472" y2="1792" x1="688" />
            <wire x2="1136" y1="1792" y2="1792" x1="688" />
            <wire x2="1136" y1="1472" y2="1472" x1="688" />
        </branch>
        <iomarker fontsize="28" x="544" y="688" name="A(3:0)" orien="R180" />
        <iomarker fontsize="28" x="544" y="752" name="B(3:0)" orien="R180" />
        <branch name="S(1:0)">
            <wire x2="992" y1="432" y2="432" x1="688" />
            <wire x2="1120" y1="432" y2="432" x1="992" />
            <wire x2="2096" y1="432" y2="432" x1="1120" />
            <wire x2="2096" y1="432" y2="448" x1="2096" />
            <wire x2="2096" y1="448" y2="800" x1="2096" />
            <wire x2="2096" y1="800" y2="1504" x1="2096" />
            <wire x2="2320" y1="1504" y2="1504" x1="2096" />
            <wire x2="2320" y1="800" y2="800" x1="2096" />
        </branch>
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="992" y="560" type="branch" />
            <wire x2="992" y1="528" y2="560" x1="992" />
            <wire x2="992" y1="560" y2="592" x1="992" />
            <wire x2="992" y1="592" y2="816" x1="992" />
            <wire x2="1136" y1="816" y2="816" x1="992" />
        </branch>
        <iomarker fontsize="28" x="688" y="432" name="S(1:0)" orien="R180" />
        <bustap x2="992" y1="432" y2="528" x1="992" />
        <instance x="2320" y="1088" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2320" y="1792" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_9(3:0)">
            <wire x2="2144" y1="816" y2="816" x1="1520" />
            <wire x2="2144" y1="816" y2="864" x1="2144" />
            <wire x2="2144" y1="864" y2="928" x1="2144" />
            <wire x2="2320" y1="928" y2="928" x1="2144" />
            <wire x2="2320" y1="864" y2="864" x1="2144" />
        </branch>
        <branch name="XLXN_10(3:0)">
            <wire x2="1920" y1="1408" y2="1408" x1="1520" />
            <wire x2="1920" y1="992" y2="1408" x1="1920" />
            <wire x2="2320" y1="992" y2="992" x1="1920" />
        </branch>
        <branch name="XLXN_11(3:0)">
            <wire x2="1936" y1="1728" y2="1728" x1="1520" />
            <wire x2="1936" y1="1056" y2="1728" x1="1936" />
            <wire x2="2320" y1="1056" y2="1056" x1="1936" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1776" y1="688" y2="688" x1="1520" />
            <wire x2="1776" y1="688" y2="1568" x1="1776" />
            <wire x2="1776" y1="1568" y2="1632" x1="1776" />
            <wire x2="2320" y1="1632" y2="1632" x1="1776" />
            <wire x2="2320" y1="1568" y2="1568" x1="1776" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="2176" y1="1680" y2="1680" x1="2096" />
            <wire x2="2176" y1="1680" y2="1696" x1="2176" />
            <wire x2="2176" y1="1696" y2="1760" x1="2176" />
            <wire x2="2320" y1="1760" y2="1760" x1="2176" />
            <wire x2="2320" y1="1696" y2="1696" x1="2176" />
            <wire x2="2096" y1="1680" y2="1792" x1="2096" />
        </branch>
        <instance x="2032" y="1920" name="XLXI_6" orien="R0" />
        <branch name="Co">
            <wire x2="2880" y1="1504" y2="1504" x1="2704" />
        </branch>
        <iomarker fontsize="28" x="2880" y="1504" name="Co" orien="R0" />
        <branch name="C(3:0)">
            <wire x2="2960" y1="800" y2="800" x1="2704" />
        </branch>
        <iomarker fontsize="28" x="2960" y="800" name="C(3:0)" orien="R0" />
    </sheet>
</drawing>