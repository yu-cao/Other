<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clk" />
        <signal name="RST" />
        <signal name="XLXN_3(31:0)" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="Segment(7:0)" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_3(18:17)" />
        <signal name="Hexs(15:0)" />
        <signal name="points(3:0)" />
        <signal name="LES(3:0)" />
        <signal name="AN(3:0)" />
        <signal name="XLXN_43(3:0)" />
        <signal name="XLXN_43(3)" />
        <signal name="XLXN_43(2)" />
        <signal name="XLXN_43(1)" />
        <signal name="XLXN_43(0)" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="Segment(7)" />
        <signal name="Segment(6)" />
        <signal name="Segment(5)" />
        <signal name="Segment(4)" />
        <signal name="Segment(3)" />
        <signal name="Segment(2)" />
        <signal name="Segment(1)" />
        <signal name="Segment(0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="RST" />
        <port polarity="Output" name="Segment(7:0)" />
        <port polarity="Input" name="Hexs(15:0)" />
        <port polarity="Input" name="points(3:0)" />
        <port polarity="Input" name="LES(3:0)" />
        <port polarity="Output" name="AN(3:0)" />
        <blockdef name="clkdiv">
            <timestamp>2017-11-9T6:33:30</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="MyMC14495">
            <timestamp>2017-11-2T6:43:33</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-400" y2="-400" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="hhh">
            <timestamp>2017-11-9T8:26:59</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="clkdiv" name="XLXI_1">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="RST" name="rst" />
            <blockpin signalname="XLXN_3(31:0)" name="clkdiv(31:0)" />
        </block>
        <block symbolname="MyMC14495" name="XLXI_2">
            <blockpin signalname="XLXN_49" name="LE" />
            <blockpin signalname="XLXN_43(2)" name="D2" />
            <blockpin signalname="XLXN_43(3)" name="D3" />
            <blockpin signalname="XLXN_43(0)" name="D0" />
            <blockpin signalname="XLXN_43(1)" name="D1" />
            <blockpin signalname="XLXN_48" name="point" />
            <blockpin signalname="Segment(6)" name="g" />
            <blockpin signalname="Segment(5)" name="f" />
            <blockpin signalname="Segment(4)" name="e" />
            <blockpin signalname="Segment(3)" name="d" />
            <blockpin signalname="Segment(2)" name="c" />
            <blockpin signalname="Segment(1)" name="b" />
            <blockpin signalname="Segment(0)" name="a" />
            <blockpin signalname="Segment(7)" name="p" />
        </block>
        <block symbolname="hhh" name="XLXI_4">
            <blockpin signalname="XLXN_3(18:17)" name="Scan(1:0)" />
            <blockpin signalname="Hexs(15:0)" name="Hexs(15:0)" />
            <blockpin signalname="points(3:0)" name="point(3:0)" />
            <blockpin signalname="LES(3:0)" name="LES(3:0)" />
            <blockpin signalname="AN(3:0)" name="AN(3:0)" />
            <blockpin signalname="XLXN_48" name="p" />
            <blockpin signalname="XLXN_49" name="LE" />
            <blockpin signalname="XLXN_43(3:0)" name="HEX(3:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="464" y="624" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1968" y="1168" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clk">
            <wire x2="464" y1="528" y2="528" x1="352" />
        </branch>
        <branch name="RST">
            <wire x2="464" y1="592" y2="592" x1="384" />
        </branch>
        <iomarker fontsize="28" x="352" y="528" name="clk" orien="R180" />
        <iomarker fontsize="28" x="384" y="592" name="RST" orien="R180" />
        <branch name="XLXN_3(31:0)">
            <wire x2="1056" y1="1312" y2="1312" x1="608" />
            <wire x2="608" y1="1312" y2="1824" x1="608" />
            <wire x2="608" y1="1824" y2="2304" x1="608" />
            <wire x2="1056" y1="528" y2="528" x1="848" />
            <wire x2="1056" y1="528" y2="1312" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="432" y="1952" name="points(3:0)" orien="R180" />
        <iomarker fontsize="28" x="2464" y="2016" name="AN(3:0)" orien="R0" />
        <iomarker fontsize="28" x="496" y="2016" name="LES(3:0)" orien="R180" />
        <branch name="Segment(7:0)">
            <wire x2="2800" y1="576" y2="688" x1="2800" />
            <wire x2="2800" y1="688" y2="752" x1="2800" />
            <wire x2="2800" y1="752" y2="832" x1="2800" />
            <wire x2="2800" y1="832" y2="880" x1="2800" />
            <wire x2="2800" y1="880" y2="944" x1="2800" />
            <wire x2="2800" y1="944" y2="992" x1="2800" />
            <wire x2="2800" y1="992" y2="1056" x1="2800" />
            <wire x2="2800" y1="1056" y2="1152" x1="2800" />
            <wire x2="2800" y1="1152" y2="1568" x1="2800" />
            <wire x2="2944" y1="1568" y2="1568" x1="2800" />
        </branch>
        <iomarker fontsize="28" x="2944" y="1568" name="Segment(7:0)" orien="R0" />
        <bustap x2="704" y1="1824" y2="1824" x1="608" />
        <instance x="816" y="2032" name="XLXI_4" orien="R0">
        </instance>
        <branch name="XLXN_3(18:17)">
            <wire x2="752" y1="1824" y2="1824" x1="704" />
            <wire x2="752" y1="1808" y2="1824" x1="752" />
            <wire x2="816" y1="1808" y2="1808" x1="752" />
        </branch>
        <branch name="Hexs(15:0)">
            <wire x2="816" y1="1872" y2="1872" x1="400" />
        </branch>
        <iomarker fontsize="28" x="400" y="1872" name="Hexs(15:0)" orien="R180" />
        <branch name="points(3:0)">
            <wire x2="624" y1="1952" y2="1952" x1="432" />
            <wire x2="816" y1="1936" y2="1936" x1="624" />
            <wire x2="624" y1="1936" y2="1952" x1="624" />
        </branch>
        <branch name="LES(3:0)">
            <wire x2="656" y1="2016" y2="2016" x1="496" />
            <wire x2="816" y1="2000" y2="2000" x1="656" />
            <wire x2="656" y1="2000" y2="2016" x1="656" />
        </branch>
        <branch name="AN(3:0)">
            <wire x2="1824" y1="1808" y2="1808" x1="1200" />
            <wire x2="1824" y1="1808" y2="2016" x1="1824" />
            <wire x2="2464" y1="2016" y2="2016" x1="1824" />
        </branch>
        <branch name="XLXN_43(3:0)">
            <wire x2="1280" y1="2000" y2="2000" x1="1200" />
            <wire x2="1280" y1="608" y2="784" x1="1280" />
            <wire x2="1280" y1="784" y2="864" x1="1280" />
            <wire x2="1280" y1="864" y2="960" x1="1280" />
            <wire x2="1280" y1="960" y2="1056" x1="1280" />
            <wire x2="1280" y1="1056" y2="2000" x1="1280" />
        </branch>
        <bustap x2="1376" y1="864" y2="864" x1="1280" />
        <branch name="XLXN_43(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="864" type="branch" />
            <wire x2="1392" y1="864" y2="864" x1="1376" />
            <wire x2="1712" y1="864" y2="864" x1="1392" />
            <wire x2="1712" y1="848" y2="864" x1="1712" />
            <wire x2="1968" y1="848" y2="848" x1="1712" />
        </branch>
        <bustap x2="1376" y1="784" y2="784" x1="1280" />
        <branch name="XLXN_43(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="784" type="branch" />
            <wire x2="1392" y1="784" y2="784" x1="1376" />
            <wire x2="1712" y1="784" y2="784" x1="1392" />
            <wire x2="1712" y1="768" y2="784" x1="1712" />
            <wire x2="1968" y1="768" y2="768" x1="1712" />
        </branch>
        <bustap x2="1376" y1="1056" y2="1056" x1="1280" />
        <branch name="XLXN_43(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="1056" type="branch" />
            <wire x2="1392" y1="1056" y2="1056" x1="1376" />
            <wire x2="1456" y1="1056" y2="1056" x1="1392" />
            <wire x2="1712" y1="1040" y2="1040" x1="1456" />
            <wire x2="1456" y1="1040" y2="1056" x1="1456" />
            <wire x2="1712" y1="1008" y2="1040" x1="1712" />
            <wire x2="1968" y1="1008" y2="1008" x1="1712" />
        </branch>
        <bustap x2="1376" y1="960" y2="960" x1="1280" />
        <branch name="XLXN_43(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1392" y="960" type="branch" />
            <wire x2="1392" y1="960" y2="960" x1="1376" />
            <wire x2="1456" y1="960" y2="960" x1="1392" />
            <wire x2="1712" y1="944" y2="944" x1="1456" />
            <wire x2="1456" y1="944" y2="960" x1="1456" />
            <wire x2="1712" y1="928" y2="944" x1="1712" />
            <wire x2="1968" y1="928" y2="928" x1="1712" />
        </branch>
        <branch name="XLXN_48">
            <wire x2="1584" y1="1872" y2="1872" x1="1200" />
            <wire x2="1584" y1="1088" y2="1872" x1="1584" />
            <wire x2="1968" y1="1088" y2="1088" x1="1584" />
        </branch>
        <branch name="XLXN_49">
            <wire x2="1568" y1="1936" y2="1936" x1="1200" />
            <wire x2="1968" y1="688" y2="688" x1="1568" />
            <wire x2="1568" y1="688" y2="1936" x1="1568" />
        </branch>
        <bustap x2="2704" y1="1152" y2="1152" x1="2800" />
        <branch name="Segment(7)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2688" y="1152" type="branch" />
            <wire x2="2480" y1="1136" y2="1136" x1="2352" />
            <wire x2="2480" y1="1136" y2="1152" x1="2480" />
            <wire x2="2688" y1="1152" y2="1152" x1="2480" />
            <wire x2="2704" y1="1152" y2="1152" x1="2688" />
        </branch>
        <bustap x2="2704" y1="688" y2="688" x1="2800" />
        <branch name="Segment(6)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2696" y="688" type="branch" />
            <wire x2="2696" y1="688" y2="688" x1="2352" />
            <wire x2="2704" y1="688" y2="688" x1="2696" />
        </branch>
        <bustap x2="2704" y1="752" y2="752" x1="2800" />
        <branch name="Segment(5)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2696" y="752" type="branch" />
            <wire x2="2480" y1="752" y2="752" x1="2352" />
            <wire x2="2480" y1="752" y2="768" x1="2480" />
            <wire x2="2688" y1="768" y2="768" x1="2480" />
            <wire x2="2696" y1="752" y2="752" x1="2688" />
            <wire x2="2704" y1="752" y2="752" x1="2696" />
            <wire x2="2688" y1="752" y2="768" x1="2688" />
        </branch>
        <bustap x2="2704" y1="832" y2="832" x1="2800" />
        <branch name="Segment(4)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2688" y="832" type="branch" />
            <wire x2="2672" y1="816" y2="816" x1="2352" />
            <wire x2="2672" y1="816" y2="832" x1="2672" />
            <wire x2="2688" y1="832" y2="832" x1="2672" />
            <wire x2="2704" y1="832" y2="832" x1="2688" />
        </branch>
        <bustap x2="2704" y1="880" y2="880" x1="2800" />
        <branch name="Segment(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2696" y="880" type="branch" />
            <wire x2="2696" y1="880" y2="880" x1="2352" />
            <wire x2="2704" y1="880" y2="880" x1="2696" />
        </branch>
        <bustap x2="2704" y1="944" y2="944" x1="2800" />
        <branch name="Segment(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2688" y="944" type="branch" />
            <wire x2="2480" y1="944" y2="944" x1="2352" />
            <wire x2="2480" y1="928" y2="944" x1="2480" />
            <wire x2="2672" y1="928" y2="928" x1="2480" />
            <wire x2="2672" y1="928" y2="944" x1="2672" />
            <wire x2="2688" y1="944" y2="944" x1="2672" />
            <wire x2="2704" y1="944" y2="944" x1="2688" />
        </branch>
        <bustap x2="2704" y1="992" y2="992" x1="2800" />
        <branch name="Segment(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2688" y="992" type="branch" />
            <wire x2="2672" y1="1008" y2="1008" x1="2352" />
            <wire x2="2688" y1="992" y2="992" x1="2672" />
            <wire x2="2704" y1="992" y2="992" x1="2688" />
            <wire x2="2672" y1="992" y2="1008" x1="2672" />
        </branch>
        <bustap x2="2704" y1="1056" y2="1056" x1="2800" />
        <branch name="Segment(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2696" y="1056" type="branch" />
            <wire x2="2688" y1="1072" y2="1072" x1="2352" />
            <wire x2="2696" y1="1056" y2="1056" x1="2688" />
            <wire x2="2704" y1="1056" y2="1056" x1="2696" />
            <wire x2="2688" y1="1056" y2="1072" x1="2688" />
        </branch>
    </sheet>
</drawing>