<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(1:0)" />
        <signal name="XLXN_7" />
        <signal name="XLXN_11" />
        <signal name="s(1)" />
        <signal name="s(0)" />
        <signal name="XLXN_16" />
        <signal name="XLXN_20" />
        <signal name="XLXN_26" />
        <signal name="XLXN_29" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="I0(3:0)" />
        <signal name="I1(3:0)" />
        <signal name="I2(3:0)" />
        <signal name="I3(3:0)" />
        <signal name="I0(0)" />
        <signal name="I0(1)" />
        <signal name="I0(2)" />
        <signal name="I0(3)" />
        <signal name="I1(3)" />
        <signal name="I1(2)" />
        <signal name="I1(1)" />
        <signal name="I1(0)" />
        <signal name="I2(0)" />
        <signal name="I2(1)" />
        <signal name="I2(2)" />
        <signal name="I2(3)" />
        <signal name="o(3:0)" />
        <signal name="o(0)" />
        <signal name="o(1)" />
        <signal name="o(2)" />
        <signal name="o(3)" />
        <signal name="I3(0)" />
        <signal name="I3(1)" />
        <signal name="I3(2)" />
        <signal name="I3(3)" />
        <port polarity="Input" name="s(1:0)" />
        <port polarity="Input" name="I0(3:0)" />
        <port polarity="Input" name="I1(3:0)" />
        <port polarity="Input" name="I2(3:0)" />
        <port polarity="Input" name="I3(3:0)" />
        <port polarity="Output" name="o(3:0)" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="s(1)" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="s(0)" name="I" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="XLXN_11" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="s(0)" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="XLXN_11" name="I1" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="s(0)" name="I1" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I0(0)" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="I1(0)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_35" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="I2(0)" name="I0" />
            <blockpin signalname="XLXN_26" name="I1" />
            <blockpin signalname="XLXN_36" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="I3(0)" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="I0(1)" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_38" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="I1(1)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_39" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="I2(1)" name="I0" />
            <blockpin signalname="XLXN_26" name="I1" />
            <blockpin signalname="XLXN_40" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_15">
            <blockpin signalname="I3(1)" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_41" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="I0(2)" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_42" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_17">
            <blockpin signalname="I1(2)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_18">
            <blockpin signalname="I2(2)" name="I0" />
            <blockpin signalname="XLXN_26" name="I1" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_19">
            <blockpin signalname="I3(2)" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_45" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="I0(3)" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="I1(3)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_22">
            <blockpin signalname="I2(3)" name="I0" />
            <blockpin signalname="XLXN_26" name="I1" />
            <blockpin signalname="XLXN_48" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_23">
            <blockpin signalname="I3(3)" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_49" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_24">
            <blockpin signalname="XLXN_37" name="I0" />
            <blockpin signalname="XLXN_36" name="I1" />
            <blockpin signalname="XLXN_35" name="I2" />
            <blockpin signalname="XLXN_34" name="I3" />
            <blockpin signalname="o(0)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_25">
            <blockpin signalname="XLXN_41" name="I0" />
            <blockpin signalname="XLXN_40" name="I1" />
            <blockpin signalname="XLXN_39" name="I2" />
            <blockpin signalname="XLXN_38" name="I3" />
            <blockpin signalname="o(1)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_26">
            <blockpin signalname="XLXN_45" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_43" name="I2" />
            <blockpin signalname="XLXN_42" name="I3" />
            <blockpin signalname="o(2)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_27">
            <blockpin signalname="XLXN_49" name="I0" />
            <blockpin signalname="XLXN_48" name="I1" />
            <blockpin signalname="XLXN_47" name="I2" />
            <blockpin signalname="XLXN_46" name="I3" />
            <blockpin signalname="o(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <branch name="s(1:0)">
            <wire x2="464" y1="304" y2="304" x1="384" />
            <wire x2="464" y1="304" y2="368" x1="464" />
            <wire x2="464" y1="368" y2="416" x1="464" />
            <wire x2="464" y1="208" y2="240" x1="464" />
            <wire x2="464" y1="240" y2="304" x1="464" />
        </branch>
        <iomarker fontsize="28" x="384" y="304" name="s(1:0)" orien="R180" />
        <bustap x2="560" y1="240" y2="240" x1="464" />
        <bustap x2="560" y1="368" y2="368" x1="464" />
        <instance x="672" y="272" name="XLXI_1" orien="R0" />
        <instance x="688" y="400" name="XLXI_2" orien="R0" />
        <instance x="1152" y="272" name="XLXI_4" orien="R0" />
        <instance x="1152" y="496" name="XLXI_5" orien="R0" />
        <instance x="1152" y="704" name="XLXI_6" orien="R0" />
        <instance x="1152" y="928" name="XLXI_7" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="1024" y1="240" y2="240" x1="896" />
            <wire x2="1152" y1="144" y2="144" x1="1024" />
            <wire x2="1024" y1="144" y2="192" x1="1024" />
            <wire x2="1024" y1="192" y2="240" x1="1024" />
            <wire x2="1088" y1="192" y2="192" x1="1024" />
            <wire x2="1088" y1="192" y2="368" x1="1088" />
            <wire x2="1152" y1="368" y2="368" x1="1088" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="960" y1="368" y2="368" x1="912" />
            <wire x2="1040" y1="368" y2="368" x1="960" />
            <wire x2="960" y1="368" y2="576" x1="960" />
            <wire x2="1152" y1="576" y2="576" x1="960" />
            <wire x2="1152" y1="208" y2="208" x1="1040" />
            <wire x2="1040" y1="208" y2="368" x1="1040" />
        </branch>
        <branch name="s(1)">
            <wire x2="576" y1="240" y2="240" x1="560" />
            <wire x2="672" y1="240" y2="240" x1="576" />
            <wire x2="576" y1="240" y2="640" x1="576" />
            <wire x2="1152" y1="640" y2="640" x1="576" />
            <wire x2="576" y1="640" y2="864" x1="576" />
            <wire x2="1152" y1="864" y2="864" x1="576" />
        </branch>
        <branch name="s(0)">
            <wire x2="672" y1="368" y2="368" x1="560" />
            <wire x2="688" y1="368" y2="368" x1="672" />
            <wire x2="672" y1="368" y2="432" x1="672" />
            <wire x2="1152" y1="432" y2="432" x1="672" />
            <wire x2="672" y1="432" y2="800" x1="672" />
            <wire x2="1152" y1="800" y2="800" x1="672" />
        </branch>
        <instance x="1744" y="352" name="XLXI_8" orien="R0" />
        <instance x="1728" y="496" name="XLXI_9" orien="R0" />
        <instance x="1728" y="640" name="XLXI_10" orien="R0" />
        <instance x="1728" y="768" name="XLXI_11" orien="R0" />
        <instance x="1728" y="1040" name="XLXI_12" orien="R0" />
        <instance x="1728" y="1200" name="XLXI_13" orien="R0" />
        <instance x="1712" y="1376" name="XLXI_14" orien="R0" />
        <instance x="1696" y="1520" name="XLXI_15" orien="R0" />
        <instance x="1696" y="1808" name="XLXI_16" orien="R0" />
        <instance x="1680" y="1968" name="XLXI_17" orien="R0" />
        <instance x="1680" y="2160" name="XLXI_18" orien="R0" />
        <instance x="1680" y="2336" name="XLXI_19" orien="R0" />
        <instance x="1664" y="2624" name="XLXI_20" orien="R0" />
        <instance x="1664" y="2800" name="XLXI_21" orien="R0" />
        <instance x="1664" y="2960" name="XLXI_22" orien="R0" />
        <instance x="1648" y="3136" name="XLXI_23" orien="R0" />
        <instance x="2272" y="576" name="XLXI_24" orien="R0" />
        <instance x="2336" y="1344" name="XLXI_25" orien="R0" />
        <instance x="2336" y="2096" name="XLXI_26" orien="R0" />
        <instance x="2336" y="2928" name="XLXI_27" orien="R0" />
        <branch name="XLXN_20">
            <wire x2="1552" y1="400" y2="400" x1="1408" />
            <wire x2="1552" y1="400" y2="1072" x1="1552" />
            <wire x2="1728" y1="1072" y2="1072" x1="1552" />
            <wire x2="1552" y1="1072" y2="1840" x1="1552" />
            <wire x2="1680" y1="1840" y2="1840" x1="1552" />
            <wire x2="1552" y1="1840" y2="2672" x1="1552" />
            <wire x2="1664" y1="2672" y2="2672" x1="1552" />
            <wire x2="1728" y1="368" y2="368" x1="1552" />
            <wire x2="1552" y1="368" y2="400" x1="1552" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="2272" y1="256" y2="256" x1="2000" />
            <wire x2="2272" y1="256" y2="320" x1="2272" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="2128" y1="400" y2="400" x1="1984" />
            <wire x2="2128" y1="384" y2="400" x1="2128" />
            <wire x2="2272" y1="384" y2="384" x1="2128" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="2128" y1="544" y2="544" x1="1984" />
            <wire x2="2128" y1="448" y2="544" x1="2128" />
            <wire x2="2272" y1="448" y2="448" x1="2128" />
        </branch>
        <branch name="XLXN_37">
            <wire x2="2272" y1="672" y2="672" x1="1984" />
            <wire x2="2272" y1="512" y2="672" x1="2272" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="2336" y1="944" y2="944" x1="1984" />
            <wire x2="2336" y1="944" y2="1088" x1="2336" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="2160" y1="1104" y2="1104" x1="1984" />
            <wire x2="2160" y1="1104" y2="1152" x1="2160" />
            <wire x2="2336" y1="1152" y2="1152" x1="2160" />
        </branch>
        <branch name="XLXN_40">
            <wire x2="2144" y1="1280" y2="1280" x1="1968" />
            <wire x2="2144" y1="1216" y2="1280" x1="2144" />
            <wire x2="2336" y1="1216" y2="1216" x1="2144" />
        </branch>
        <branch name="XLXN_41">
            <wire x2="2336" y1="1424" y2="1424" x1="1952" />
            <wire x2="2336" y1="1280" y2="1424" x1="2336" />
        </branch>
        <branch name="XLXN_42">
            <wire x2="2336" y1="1712" y2="1712" x1="1952" />
            <wire x2="2336" y1="1712" y2="1840" x1="2336" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="2128" y1="1872" y2="1872" x1="1936" />
            <wire x2="2128" y1="1872" y2="1904" x1="2128" />
            <wire x2="2336" y1="1904" y2="1904" x1="2128" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="2128" y1="2064" y2="2064" x1="1936" />
            <wire x2="2128" y1="1968" y2="2064" x1="2128" />
            <wire x2="2336" y1="1968" y2="1968" x1="2128" />
        </branch>
        <branch name="XLXN_45">
            <wire x2="2336" y1="2240" y2="2240" x1="1936" />
            <wire x2="2336" y1="2032" y2="2240" x1="2336" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="2336" y1="2528" y2="2528" x1="1920" />
            <wire x2="2336" y1="2528" y2="2672" x1="2336" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="2128" y1="2704" y2="2704" x1="1920" />
            <wire x2="2128" y1="2704" y2="2736" x1="2128" />
            <wire x2="2336" y1="2736" y2="2736" x1="2128" />
        </branch>
        <branch name="XLXN_48">
            <wire x2="2128" y1="2864" y2="2864" x1="1920" />
            <wire x2="2128" y1="2800" y2="2864" x1="2128" />
            <wire x2="2336" y1="2800" y2="2800" x1="2128" />
        </branch>
        <branch name="XLXN_49">
            <wire x2="2336" y1="3040" y2="3040" x1="1904" />
            <wire x2="2336" y1="2864" y2="3040" x1="2336" />
        </branch>
        <branch name="I2(3:0)">
            <wire x2="1488" y1="2048" y2="2048" x1="592" />
            <wire x2="1488" y1="2048" y2="2112" x1="1488" />
            <wire x2="1488" y1="2112" y2="2800" x1="1488" />
            <wire x2="1488" y1="2800" y2="2864" x1="1488" />
            <wire x2="1488" y1="512" y2="576" x1="1488" />
            <wire x2="1488" y1="576" y2="1312" x1="1488" />
            <wire x2="1488" y1="1312" y2="2048" x1="1488" />
        </branch>
        <iomarker fontsize="28" x="592" y="1760" name="I0(3:0)" orien="R180" />
        <iomarker fontsize="28" x="592" y="1904" name="I1(3:0)" orien="R180" />
        <iomarker fontsize="28" x="592" y="2048" name="I2(3:0)" orien="R180" />
        <iomarker fontsize="28" x="592" y="2208" name="I3(3:0)" orien="R180" />
        <branch name="XLXN_16">
            <wire x2="1568" y1="176" y2="176" x1="1408" />
            <wire x2="1568" y1="176" y2="224" x1="1568" />
            <wire x2="1744" y1="224" y2="224" x1="1568" />
            <wire x2="1568" y1="224" y2="912" x1="1568" />
            <wire x2="1728" y1="912" y2="912" x1="1568" />
            <wire x2="1568" y1="912" y2="1680" x1="1568" />
            <wire x2="1696" y1="1680" y2="1680" x1="1568" />
            <wire x2="1568" y1="1680" y2="2496" x1="1568" />
            <wire x2="1664" y1="2496" y2="2496" x1="1568" />
        </branch>
        <branch name="I0(3:0)">
            <wire x2="1280" y1="1760" y2="1760" x1="592" />
            <wire x2="1280" y1="1760" y2="2544" x1="1280" />
            <wire x2="1280" y1="2544" y2="2576" x1="1280" />
            <wire x2="1104" y1="320" y2="944" x1="1104" />
            <wire x2="1280" y1="944" y2="944" x1="1104" />
            <wire x2="1280" y1="944" y2="992" x1="1280" />
            <wire x2="1280" y1="992" y2="1728" x1="1280" />
            <wire x2="1280" y1="1728" y2="1760" x1="1280" />
            <wire x2="1360" y1="320" y2="320" x1="1104" />
            <wire x2="1360" y1="272" y2="304" x1="1360" />
            <wire x2="1360" y1="304" y2="320" x1="1360" />
        </branch>
        <bustap x2="1376" y1="2544" y2="2544" x1="1280" />
        <bustap x2="1376" y1="1728" y2="1728" x1="1280" />
        <bustap x2="1376" y1="992" y2="992" x1="1280" />
        <bustap x2="1456" y1="304" y2="304" x1="1360" />
        <branch name="I0(0)">
            <wire x2="1600" y1="304" y2="304" x1="1456" />
            <wire x2="1600" y1="288" y2="304" x1="1600" />
            <wire x2="1744" y1="288" y2="288" x1="1600" />
        </branch>
        <branch name="I0(1)">
            <wire x2="1504" y1="992" y2="992" x1="1376" />
            <wire x2="1504" y1="976" y2="992" x1="1504" />
            <wire x2="1728" y1="976" y2="976" x1="1504" />
        </branch>
        <branch name="I0(2)">
            <wire x2="1504" y1="1728" y2="1728" x1="1376" />
            <wire x2="1504" y1="1728" y2="1744" x1="1504" />
            <wire x2="1696" y1="1744" y2="1744" x1="1504" />
        </branch>
        <branch name="I0(3)">
            <wire x2="1504" y1="2544" y2="2544" x1="1376" />
            <wire x2="1504" y1="2544" y2="2560" x1="1504" />
            <wire x2="1664" y1="2560" y2="2560" x1="1504" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="1520" y1="832" y2="832" x1="1408" />
            <wire x2="1520" y1="832" y2="1392" x1="1520" />
            <wire x2="1696" y1="1392" y2="1392" x1="1520" />
            <wire x2="1520" y1="1392" y2="2208" x1="1520" />
            <wire x2="1680" y1="2208" y2="2208" x1="1520" />
            <wire x2="1520" y1="2208" y2="3008" x1="1520" />
            <wire x2="1648" y1="3008" y2="3008" x1="1520" />
            <wire x2="1520" y1="640" y2="832" x1="1520" />
            <wire x2="1728" y1="640" y2="640" x1="1520" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="1536" y1="608" y2="608" x1="1408" />
            <wire x2="1536" y1="608" y2="1248" x1="1536" />
            <wire x2="1712" y1="1248" y2="1248" x1="1536" />
            <wire x2="1536" y1="1248" y2="2000" x1="1536" />
            <wire x2="1600" y1="2000" y2="2000" x1="1536" />
            <wire x2="1600" y1="2000" y2="2032" x1="1600" />
            <wire x2="1680" y1="2032" y2="2032" x1="1600" />
            <wire x2="1536" y1="2000" y2="2832" x1="1536" />
            <wire x2="1664" y1="2832" y2="2832" x1="1536" />
            <wire x2="1536" y1="512" y2="608" x1="1536" />
            <wire x2="1728" y1="512" y2="512" x1="1536" />
        </branch>
        <bustap x2="1456" y1="2720" y2="2720" x1="1360" />
        <branch name="I1(3:0)">
            <wire x2="1360" y1="1904" y2="1904" x1="592" />
            <wire x2="1392" y1="1904" y2="1904" x1="1360" />
            <wire x2="1360" y1="1904" y2="1968" x1="1360" />
            <wire x2="1360" y1="1968" y2="2480" x1="1360" />
            <wire x2="1376" y1="496" y2="496" x1="1120" />
            <wire x2="1120" y1="496" y2="1104" x1="1120" />
            <wire x2="1120" y1="1104" y2="1248" x1="1120" />
            <wire x2="1392" y1="1248" y2="1248" x1="1120" />
            <wire x2="1392" y1="1248" y2="1904" x1="1392" />
            <wire x2="1360" y1="2480" y2="2480" x1="1264" />
            <wire x2="1264" y1="2480" y2="2752" x1="1264" />
            <wire x2="1360" y1="2752" y2="2752" x1="1264" />
            <wire x2="1360" y1="2656" y2="2720" x1="1360" />
            <wire x2="1360" y1="2720" y2="2752" x1="1360" />
            <wire x2="1376" y1="464" y2="496" x1="1376" />
        </branch>
        <bustap x2="1472" y1="496" y2="496" x1="1376" />
        <bustap x2="1216" y1="1104" y2="1104" x1="1120" />
        <bustap x2="1456" y1="1968" y2="1968" x1="1360" />
        <branch name="I1(3)">
            <wire x2="1552" y1="2720" y2="2720" x1="1456" />
            <wire x2="1552" y1="2720" y2="2736" x1="1552" />
            <wire x2="1664" y1="2736" y2="2736" x1="1552" />
        </branch>
        <branch name="I1(2)">
            <wire x2="1584" y1="1968" y2="1968" x1="1456" />
            <wire x2="1584" y1="1904" y2="1968" x1="1584" />
            <wire x2="1680" y1="1904" y2="1904" x1="1584" />
        </branch>
        <branch name="I1(1)">
            <wire x2="1472" y1="1104" y2="1104" x1="1216" />
            <wire x2="1472" y1="1104" y2="1136" x1="1472" />
            <wire x2="1728" y1="1136" y2="1136" x1="1472" />
        </branch>
        <branch name="I1(0)">
            <wire x2="1600" y1="496" y2="496" x1="1472" />
            <wire x2="1600" y1="432" y2="496" x1="1600" />
            <wire x2="1728" y1="432" y2="432" x1="1600" />
        </branch>
        <branch name="I3(3:0)">
            <wire x2="880" y1="2208" y2="2208" x1="592" />
            <wire x2="880" y1="2208" y2="2288" x1="880" />
            <wire x2="880" y1="2288" y2="3040" x1="880" />
            <wire x2="880" y1="3040" y2="3120" x1="880" />
            <wire x2="880" y1="720" y2="784" x1="880" />
            <wire x2="880" y1="784" y2="1456" x1="880" />
            <wire x2="880" y1="1456" y2="2208" x1="880" />
        </branch>
        <bustap x2="1584" y1="2800" y2="2800" x1="1488" />
        <bustap x2="1584" y1="2112" y2="2112" x1="1488" />
        <bustap x2="1584" y1="1312" y2="1312" x1="1488" />
        <bustap x2="1584" y1="576" y2="576" x1="1488" />
        <branch name="I2(0)">
            <wire x2="1728" y1="576" y2="576" x1="1584" />
        </branch>
        <branch name="I2(1)">
            <wire x2="1712" y1="1312" y2="1312" x1="1584" />
        </branch>
        <branch name="I2(2)">
            <wire x2="1632" y1="2112" y2="2112" x1="1584" />
            <wire x2="1632" y1="2096" y2="2112" x1="1632" />
            <wire x2="1680" y1="2096" y2="2096" x1="1632" />
        </branch>
        <branch name="I2(3)">
            <wire x2="1616" y1="2800" y2="2800" x1="1584" />
            <wire x2="1616" y1="2800" y2="2896" x1="1616" />
            <wire x2="1664" y1="2896" y2="2896" x1="1616" />
        </branch>
        <branch name="o(3:0)">
            <wire x2="3312" y1="3072" y2="3072" x1="3296" />
            <wire x2="3312" y1="240" y2="448" x1="3312" />
            <wire x2="3312" y1="448" y2="1024" x1="3312" />
            <wire x2="3312" y1="1024" y2="1120" x1="3312" />
            <wire x2="4224" y1="1120" y2="1120" x1="3312" />
            <wire x2="3312" y1="1120" y2="1920" x1="3312" />
            <wire x2="3312" y1="1920" y2="2752" x1="3312" />
            <wire x2="3312" y1="2752" y2="3072" x1="3312" />
        </branch>
        <iomarker fontsize="28" x="4224" y="1120" name="o(3:0)" orien="R0" />
        <bustap x2="3216" y1="448" y2="448" x1="3312" />
        <bustap x2="3216" y1="1024" y2="1024" x1="3312" />
        <bustap x2="3216" y1="1920" y2="1920" x1="3312" />
        <bustap x2="3216" y1="2752" y2="2752" x1="3312" />
        <branch name="o(0)">
            <wire x2="2864" y1="416" y2="416" x1="2528" />
            <wire x2="2864" y1="416" y2="448" x1="2864" />
            <wire x2="3216" y1="448" y2="448" x1="2864" />
        </branch>
        <branch name="o(1)">
            <wire x2="2896" y1="1184" y2="1184" x1="2592" />
            <wire x2="2896" y1="1024" y2="1184" x1="2896" />
            <wire x2="3216" y1="1024" y2="1024" x1="2896" />
        </branch>
        <branch name="o(2)">
            <wire x2="2896" y1="1936" y2="1936" x1="2592" />
            <wire x2="2896" y1="1920" y2="1936" x1="2896" />
            <wire x2="3216" y1="1920" y2="1920" x1="2896" />
        </branch>
        <branch name="o(3)">
            <wire x2="2896" y1="2768" y2="2768" x1="2592" />
            <wire x2="2896" y1="2752" y2="2768" x1="2896" />
            <wire x2="3216" y1="2752" y2="2752" x1="2896" />
        </branch>
        <bustap x2="976" y1="3040" y2="3040" x1="880" />
        <bustap x2="976" y1="2288" y2="2288" x1="880" />
        <bustap x2="976" y1="1456" y2="1456" x1="880" />
        <bustap x2="976" y1="784" y2="784" x1="880" />
        <branch name="I3(0)">
            <wire x2="1056" y1="784" y2="784" x1="976" />
            <wire x2="1056" y1="704" y2="784" x1="1056" />
            <wire x2="1728" y1="704" y2="704" x1="1056" />
        </branch>
        <branch name="I3(1)">
            <wire x2="1696" y1="1456" y2="1456" x1="976" />
        </branch>
        <branch name="I3(2)">
            <wire x2="1328" y1="2288" y2="2288" x1="976" />
            <wire x2="1328" y1="2272" y2="2288" x1="1328" />
            <wire x2="1680" y1="2272" y2="2272" x1="1328" />
        </branch>
        <branch name="I3(3)">
            <wire x2="1312" y1="3040" y2="3040" x1="976" />
            <wire x2="1312" y1="3040" y2="3072" x1="1312" />
            <wire x2="1648" y1="3072" y2="3072" x1="1312" />
        </branch>
    </sheet>
</drawing>