// Verilog test fixture created from schematic E:\Logic_Design_Other\MyALU\MyALU\AddSub4b.sch - Thu Dec 07 13:38:53 2017

`timescale 1ns / 1ps

module AddSub4b_AddSub4b_sch_tb();

// Inputs
   reg [3:0] A;
   reg [3:0] B;
   reg Ctrl;

// Output
   wire Co;
   wire [3:0] S;

// Bidirs

// Instantiate the UUT
   AddSub4b UUT (
		.A(A), 
		.B(B), 
		.Co(Co), 
		.S(S), 
		.Ctrl(Ctrl)
   );
// Initialize Inputs
       initial begin
		A = 0;
		B = 0;
		Ctrl = 0;
		
		        A[3]=1;
        A[2]=1;
        B[1]=1;
        B[0]=1;
        #50;
        
        A[0]=1;
        A[1]=0;
        A[2]=0;
        A[3]=1;
        B[3]=1;
        #50;
        
		  Ctrl=1;
		  
		  A[3]=1;
		  A[2]=1;
		  A[1]=1;
		  A[0]=1;
		  B[3]=0;
		  #50;
		  
		  A[3]=1;
		  A[2]=1;
		  A[1]=0;
		  A[0]=1;
		  B[1]=1;
		  B[2]=1;
		  B[0]=1;
		  #50;
		
		
	end
		

endmodule
