<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="A(3:0)" />
        <signal name="B(3:0)" />
        <signal name="A(3)" />
        <signal name="A(2)" />
        <signal name="A(1)" />
        <signal name="A(0)" />
        <signal name="Co" />
        <signal name="S(3:0)" />
        <signal name="B(3)" />
        <signal name="B(2)" />
        <signal name="B(1)" />
        <signal name="B(0)" />
        <signal name="Ctrl" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="S(3)" />
        <signal name="S(2)" />
        <signal name="S(1)" />
        <signal name="S(0)" />
        <port polarity="Input" name="A(3:0)" />
        <port polarity="Input" name="B(3:0)" />
        <port polarity="Output" name="Co" />
        <port polarity="Output" name="S(3:0)" />
        <port polarity="Input" name="Ctrl" />
        <blockdef name="AddSub1b">
            <timestamp>2017-11-22T6:45:20</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="AddSub1b" name="XLXI_5">
            <blockpin signalname="A(0)" name="A" />
            <blockpin signalname="Ctrl" name="Ctrl" />
            <blockpin signalname="B(0)" name="B" />
            <blockpin signalname="Ctrl" name="Ci" />
            <blockpin signalname="S(0)" name="S" />
            <blockpin signalname="XLXN_33" name="Co" />
        </block>
        <block symbolname="AddSub1b" name="XLXI_6">
            <blockpin signalname="A(1)" name="A" />
            <blockpin signalname="Ctrl" name="Ctrl" />
            <blockpin signalname="B(1)" name="B" />
            <blockpin signalname="XLXN_33" name="Ci" />
            <blockpin signalname="S(1)" name="S" />
            <blockpin signalname="XLXN_34" name="Co" />
        </block>
        <block symbolname="AddSub1b" name="XLXI_7">
            <blockpin signalname="A(2)" name="A" />
            <blockpin signalname="Ctrl" name="Ctrl" />
            <blockpin signalname="B(2)" name="B" />
            <blockpin signalname="XLXN_34" name="Ci" />
            <blockpin signalname="S(2)" name="S" />
            <blockpin signalname="XLXN_35" name="Co" />
        </block>
        <block symbolname="AddSub1b" name="XLXI_8">
            <blockpin signalname="A(3)" name="A" />
            <blockpin signalname="Ctrl" name="Ctrl" />
            <blockpin signalname="B(3)" name="B" />
            <blockpin signalname="XLXN_35" name="Ci" />
            <blockpin signalname="S(3)" name="S" />
            <blockpin signalname="Co" name="Co" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="A(3:0)">
            <wire x2="816" y1="816" y2="864" x1="816" />
            <wire x2="816" y1="864" y2="928" x1="816" />
            <wire x2="816" y1="928" y2="976" x1="816" />
            <wire x2="816" y1="976" y2="1056" x1="816" />
            <wire x2="816" y1="1056" y2="1104" x1="816" />
        </branch>
        <branch name="B(3:0)">
            <wire x2="816" y1="1360" y2="1440" x1="816" />
            <wire x2="816" y1="1440" y2="1536" x1="816" />
            <wire x2="816" y1="1536" y2="1600" x1="816" />
            <wire x2="816" y1="1600" y2="1664" x1="816" />
            <wire x2="816" y1="1664" y2="1712" x1="816" />
        </branch>
        <bustap x2="912" y1="1056" y2="1056" x1="816" />
        <branch name="A(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1344" y="1056" type="branch" />
            <wire x2="1344" y1="1056" y2="1056" x1="912" />
            <wire x2="1344" y1="1056" y2="1936" x1="1344" />
            <wire x2="1728" y1="1936" y2="1936" x1="1344" />
        </branch>
        <bustap x2="912" y1="976" y2="976" x1="816" />
        <branch name="A(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="928" y="976" type="branch" />
            <wire x2="928" y1="976" y2="976" x1="912" />
            <wire x2="1424" y1="976" y2="976" x1="928" />
            <wire x2="1424" y1="976" y2="1568" x1="1424" />
            <wire x2="1728" y1="1568" y2="1568" x1="1424" />
        </branch>
        <bustap x2="912" y1="928" y2="928" x1="816" />
        <branch name="A(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="928" y="928" type="branch" />
            <wire x2="928" y1="928" y2="928" x1="912" />
            <wire x2="1392" y1="928" y2="928" x1="928" />
            <wire x2="1392" y1="928" y2="1248" x1="1392" />
            <wire x2="1728" y1="1248" y2="1248" x1="1392" />
        </branch>
        <bustap x2="912" y1="864" y2="864" x1="816" />
        <branch name="A(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="928" y="864" type="branch" />
            <wire x2="928" y1="864" y2="864" x1="912" />
            <wire x2="944" y1="864" y2="864" x1="928" />
            <wire x2="944" y1="864" y2="880" x1="944" />
            <wire x2="1728" y1="880" y2="880" x1="944" />
        </branch>
        <branch name="S(3:0)">
            <wire x2="2512" y1="1168" y2="1248" x1="2512" />
            <wire x2="2512" y1="1248" y2="1312" x1="2512" />
            <wire x2="2512" y1="1312" y2="1376" x1="2512" />
            <wire x2="2512" y1="1376" y2="1440" x1="2512" />
            <wire x2="2512" y1="1440" y2="1488" x1="2512" />
        </branch>
        <iomarker fontsize="28" x="816" y="816" name="A(3:0)" orien="R270" />
        <iomarker fontsize="28" x="816" y="1360" name="B(3:0)" orien="R270" />
        <iomarker fontsize="28" x="2512" y="1168" name="S(3:0)" orien="R270" />
        <instance x="1728" y="1104" name="XLXI_5" orien="R0">
        </instance>
        <instance x="1728" y="1472" name="XLXI_6" orien="R0">
        </instance>
        <instance x="1728" y="1792" name="XLXI_7" orien="R0">
        </instance>
        <branch name="Co">
            <wire x2="2384" y1="2128" y2="2128" x1="2112" />
        </branch>
        <instance x="1728" y="2160" name="XLXI_8" orien="R0">
        </instance>
        <bustap x2="912" y1="1664" y2="1664" x1="816" />
        <branch name="B(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="920" y="1664" type="branch" />
            <wire x2="944" y1="1664" y2="1664" x1="912" />
            <wire x2="944" y1="1664" y2="2064" x1="944" />
            <wire x2="1728" y1="2064" y2="2064" x1="944" />
        </branch>
        <bustap x2="912" y1="1600" y2="1600" x1="816" />
        <branch name="B(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="920" y="1600" type="branch" />
            <wire x2="1328" y1="1600" y2="1600" x1="912" />
            <wire x2="1328" y1="1600" y2="1696" x1="1328" />
            <wire x2="1728" y1="1696" y2="1696" x1="1328" />
        </branch>
        <bustap x2="912" y1="1536" y2="1536" x1="816" />
        <branch name="B(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="928" y="1536" type="branch" />
            <wire x2="928" y1="1536" y2="1536" x1="912" />
            <wire x2="960" y1="1536" y2="1536" x1="928" />
            <wire x2="1728" y1="1376" y2="1376" x1="960" />
            <wire x2="960" y1="1376" y2="1536" x1="960" />
        </branch>
        <bustap x2="912" y1="1440" y2="1440" x1="816" />
        <branch name="B(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="928" y="1440" type="branch" />
            <wire x2="928" y1="1440" y2="1440" x1="912" />
            <wire x2="944" y1="1440" y2="1440" x1="928" />
            <wire x2="1728" y1="1008" y2="1008" x1="944" />
            <wire x2="944" y1="1008" y2="1440" x1="944" />
        </branch>
        <branch name="Ctrl">
            <wire x2="1600" y1="752" y2="752" x1="1392" />
            <wire x2="1600" y1="752" y2="944" x1="1600" />
            <wire x2="1728" y1="944" y2="944" x1="1600" />
            <wire x2="1600" y1="944" y2="1072" x1="1600" />
            <wire x2="1600" y1="1072" y2="1312" x1="1600" />
            <wire x2="1600" y1="1312" y2="1632" x1="1600" />
            <wire x2="1728" y1="1632" y2="1632" x1="1600" />
            <wire x2="1600" y1="1632" y2="2000" x1="1600" />
            <wire x2="1728" y1="2000" y2="2000" x1="1600" />
            <wire x2="1728" y1="1312" y2="1312" x1="1600" />
            <wire x2="1728" y1="1072" y2="1072" x1="1600" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="1728" y1="1440" y2="1440" x1="1664" />
            <wire x2="1664" y1="1440" y2="1520" x1="1664" />
            <wire x2="2160" y1="1520" y2="1520" x1="1664" />
            <wire x2="2160" y1="1072" y2="1072" x1="2112" />
            <wire x2="2160" y1="1072" y2="1520" x1="2160" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="1712" y1="1504" y2="1760" x1="1712" />
            <wire x2="1728" y1="1760" y2="1760" x1="1712" />
            <wire x2="2176" y1="1504" y2="1504" x1="1712" />
            <wire x2="2176" y1="1440" y2="1440" x1="2112" />
            <wire x2="2176" y1="1440" y2="1504" x1="2176" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="1728" y1="2128" y2="2128" x1="1648" />
            <wire x2="1648" y1="2128" y2="2224" x1="1648" />
            <wire x2="2192" y1="2224" y2="2224" x1="1648" />
            <wire x2="2192" y1="1760" y2="1760" x1="2112" />
            <wire x2="2192" y1="1760" y2="2224" x1="2192" />
        </branch>
        <iomarker fontsize="28" x="2384" y="2128" name="Co" orien="R0" />
        <bustap x2="2416" y1="1440" y2="1440" x1="2512" />
        <branch name="S(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2384" y="1440" type="branch" />
            <wire x2="2352" y1="1936" y2="1936" x1="2112" />
            <wire x2="2384" y1="1440" y2="1440" x1="2352" />
            <wire x2="2416" y1="1440" y2="1440" x1="2384" />
            <wire x2="2352" y1="1440" y2="1936" x1="2352" />
        </branch>
        <bustap x2="2416" y1="1376" y2="1376" x1="2512" />
        <branch name="S(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2392" y="1376" type="branch" />
            <wire x2="2368" y1="1568" y2="1568" x1="2112" />
            <wire x2="2368" y1="1376" y2="1568" x1="2368" />
            <wire x2="2416" y1="1376" y2="1376" x1="2368" />
        </branch>
        <bustap x2="2416" y1="1312" y2="1312" x1="2512" />
        <branch name="S(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2400" y="1312" type="branch" />
            <wire x2="2240" y1="1248" y2="1248" x1="2112" />
            <wire x2="2240" y1="1248" y2="1312" x1="2240" />
            <wire x2="2400" y1="1312" y2="1312" x1="2240" />
            <wire x2="2416" y1="1312" y2="1312" x1="2400" />
        </branch>
        <bustap x2="2416" y1="1248" y2="1248" x1="2512" />
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2400" y="1248" type="branch" />
            <wire x2="2384" y1="880" y2="880" x1="2112" />
            <wire x2="2384" y1="880" y2="1248" x1="2384" />
            <wire x2="2400" y1="1248" y2="1248" x1="2384" />
            <wire x2="2416" y1="1248" y2="1248" x1="2400" />
        </branch>
        <iomarker fontsize="28" x="1392" y="752" name="Ctrl" orien="R180" />
    </sheet>
</drawing>