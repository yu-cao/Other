<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="A(3:0)" />
        <signal name="B(3:0)" />
        <signal name="A(3)" />
        <signal name="A(2)" />
        <signal name="A(1)" />
        <signal name="A(0)" />
        <signal name="B(3)" />
        <signal name="B(2)" />
        <signal name="B(1)" />
        <signal name="B(0)" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="C(3:0)" />
        <signal name="C(3)" />
        <signal name="C(2)" />
        <signal name="C(1)" />
        <signal name="C(0)" />
        <port polarity="Input" name="A(3:0)" />
        <port polarity="Input" name="B(3:0)" />
        <port polarity="Output" name="C(3:0)" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="or2" name="XLXI_1">
            <blockpin signalname="B(0)" name="I0" />
            <blockpin signalname="A(0)" name="I1" />
            <blockpin signalname="C(0)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_2">
            <blockpin signalname="B(1)" name="I0" />
            <blockpin signalname="A(1)" name="I1" />
            <blockpin signalname="C(1)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="B(2)" name="I0" />
            <blockpin signalname="A(2)" name="I1" />
            <blockpin signalname="C(2)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="B(3)" name="I0" />
            <blockpin signalname="A(3)" name="I1" />
            <blockpin signalname="C(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="A(3:0)">
            <wire x2="960" y1="464" y2="464" x1="544" />
            <wire x2="960" y1="464" y2="544" x1="960" />
            <wire x2="960" y1="544" y2="624" x1="960" />
            <wire x2="960" y1="624" y2="704" x1="960" />
            <wire x2="960" y1="704" y2="784" x1="960" />
            <wire x2="960" y1="784" y2="848" x1="960" />
        </branch>
        <branch name="B(3:0)">
            <wire x2="960" y1="1216" y2="1216" x1="592" />
            <wire x2="960" y1="1216" y2="1248" x1="960" />
            <wire x2="960" y1="1248" y2="1328" x1="960" />
            <wire x2="960" y1="1328" y2="1408" x1="960" />
            <wire x2="960" y1="1408" y2="1488" x1="960" />
            <wire x2="960" y1="1488" y2="1552" x1="960" />
        </branch>
        <iomarker fontsize="28" x="544" y="464" name="A(3:0)" orien="R180" />
        <iomarker fontsize="28" x="592" y="1216" name="B(3:0)" orien="R180" />
        <bustap x2="1056" y1="784" y2="784" x1="960" />
        <branch name="A(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="784" type="branch" />
            <wire x2="1088" y1="784" y2="784" x1="1056" />
            <wire x2="1376" y1="784" y2="784" x1="1088" />
            <wire x2="1376" y1="784" y2="1280" x1="1376" />
            <wire x2="1744" y1="1280" y2="1280" x1="1376" />
        </branch>
        <bustap x2="1056" y1="704" y2="704" x1="960" />
        <branch name="A(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="704" type="branch" />
            <wire x2="1088" y1="704" y2="704" x1="1056" />
            <wire x2="1424" y1="704" y2="704" x1="1088" />
            <wire x2="1424" y1="704" y2="1040" x1="1424" />
            <wire x2="1744" y1="1040" y2="1040" x1="1424" />
        </branch>
        <bustap x2="1056" y1="624" y2="624" x1="960" />
        <branch name="A(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1080" y="624" type="branch" />
            <wire x2="1080" y1="624" y2="624" x1="1056" />
            <wire x2="1344" y1="624" y2="624" x1="1080" />
            <wire x2="1344" y1="624" y2="800" x1="1344" />
            <wire x2="1744" y1="800" y2="800" x1="1344" />
        </branch>
        <bustap x2="1056" y1="544" y2="544" x1="960" />
        <branch name="A(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="544" type="branch" />
            <wire x2="1088" y1="544" y2="544" x1="1056" />
            <wire x2="1744" y1="544" y2="544" x1="1088" />
        </branch>
        <bustap x2="1056" y1="1488" y2="1488" x1="960" />
        <branch name="B(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1096" y="1488" type="branch" />
            <wire x2="1096" y1="1488" y2="1488" x1="1056" />
            <wire x2="1136" y1="1488" y2="1488" x1="1096" />
            <wire x2="1744" y1="1344" y2="1344" x1="1136" />
            <wire x2="1136" y1="1344" y2="1488" x1="1136" />
        </branch>
        <bustap x2="1056" y1="1408" y2="1408" x1="960" />
        <branch name="B(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="1408" type="branch" />
            <wire x2="1088" y1="1408" y2="1408" x1="1056" />
            <wire x2="1120" y1="1408" y2="1408" x1="1088" />
            <wire x2="1424" y1="1408" y2="1408" x1="1120" />
            <wire x2="1424" y1="1104" y2="1408" x1="1424" />
            <wire x2="1744" y1="1104" y2="1104" x1="1424" />
        </branch>
        <bustap x2="1056" y1="1328" y2="1328" x1="960" />
        <branch name="B(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1080" y="1328" type="branch" />
            <wire x2="1080" y1="1328" y2="1328" x1="1056" />
            <wire x2="1104" y1="1328" y2="1328" x1="1080" />
            <wire x2="1360" y1="1328" y2="1328" x1="1104" />
            <wire x2="1360" y1="864" y2="1328" x1="1360" />
            <wire x2="1744" y1="864" y2="864" x1="1360" />
        </branch>
        <bustap x2="1056" y1="1248" y2="1248" x1="960" />
        <branch name="B(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1080" y="1248" type="branch" />
            <wire x2="1080" y1="1248" y2="1248" x1="1056" />
            <wire x2="1104" y1="1248" y2="1248" x1="1080" />
            <wire x2="1120" y1="1248" y2="1248" x1="1104" />
            <wire x2="1744" y1="608" y2="608" x1="1120" />
            <wire x2="1120" y1="608" y2="1248" x1="1120" />
        </branch>
        <instance x="1744" y="672" name="XLXI_1" orien="R0" />
        <instance x="1744" y="928" name="XLXI_2" orien="R0" />
        <instance x="1744" y="1168" name="XLXI_3" orien="R0" />
        <instance x="1744" y="1408" name="XLXI_4" orien="R0" />
        <branch name="C(3:0)">
            <wire x2="2528" y1="496" y2="496" x1="2384" />
            <wire x2="2384" y1="496" y2="576" x1="2384" />
            <wire x2="2384" y1="576" y2="832" x1="2384" />
            <wire x2="2384" y1="832" y2="1072" x1="2384" />
            <wire x2="2384" y1="1072" y2="1312" x1="2384" />
            <wire x2="2384" y1="1312" y2="1344" x1="2384" />
        </branch>
        <iomarker fontsize="28" x="2528" y="496" name="C(3:0)" orien="R0" />
        <bustap x2="2288" y1="1312" y2="1312" x1="2384" />
        <branch name="C(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="1312" type="branch" />
            <wire x2="2144" y1="1312" y2="1312" x1="2000" />
            <wire x2="2288" y1="1312" y2="1312" x1="2144" />
        </branch>
        <bustap x2="2288" y1="1072" y2="1072" x1="2384" />
        <branch name="C(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="1072" type="branch" />
            <wire x2="2144" y1="1072" y2="1072" x1="2000" />
            <wire x2="2288" y1="1072" y2="1072" x1="2144" />
        </branch>
        <bustap x2="2288" y1="832" y2="832" x1="2384" />
        <branch name="C(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="832" type="branch" />
            <wire x2="2144" y1="832" y2="832" x1="2000" />
            <wire x2="2288" y1="832" y2="832" x1="2144" />
        </branch>
        <bustap x2="2288" y1="576" y2="576" x1="2384" />
        <branch name="C(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="576" type="branch" />
            <wire x2="2144" y1="576" y2="576" x1="2000" />
            <wire x2="2288" y1="576" y2="576" x1="2144" />
        </branch>
    </sheet>
</drawing>