<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(1:0)" />
        <signal name="s(1)" />
        <signal name="s(0)" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="I0" />
        <signal name="I1" />
        <signal name="I2" />
        <signal name="I3" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="o" />
        <port polarity="Input" name="s(1:0)" />
        <port polarity="Input" name="I0" />
        <port polarity="Input" name="I1" />
        <port polarity="Input" name="I2" />
        <port polarity="Input" name="I3" />
        <port polarity="Output" name="o" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="s(1)" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="s(0)" name="I" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_9" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="s(0)" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="s(0)" name="I1" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="I0" name="I0" />
            <blockpin signalname="XLXN_14" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="I3" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_11">
            <blockpin signalname="XLXN_28" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="XLXN_26" name="I2" />
            <blockpin signalname="XLXN_25" name="I3" />
            <blockpin signalname="o" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="s(1:0)">
            <wire x2="384" y1="656" y2="672" x1="384" />
            <wire x2="496" y1="672" y2="672" x1="384" />
            <wire x2="496" y1="672" y2="736" x1="496" />
            <wire x2="496" y1="736" y2="800" x1="496" />
            <wire x2="496" y1="480" y2="560" x1="496" />
            <wire x2="496" y1="560" y2="672" x1="496" />
        </branch>
        <iomarker fontsize="28" x="384" y="656" name="s(1:0)" orien="R270" />
        <bustap x2="592" y1="560" y2="560" x1="496" />
        <branch name="s(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="624" y="560" type="branch" />
            <wire x2="624" y1="560" y2="560" x1="592" />
            <wire x2="768" y1="560" y2="560" x1="624" />
            <wire x2="912" y1="560" y2="560" x1="768" />
            <wire x2="768" y1="560" y2="1008" x1="768" />
            <wire x2="1408" y1="1008" y2="1008" x1="768" />
            <wire x2="768" y1="1008" y2="1184" x1="768" />
            <wire x2="1408" y1="1184" y2="1184" x1="768" />
        </branch>
        <bustap x2="592" y1="736" y2="736" x1="496" />
        <branch name="s(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="616" y="736" type="branch" />
            <wire x2="616" y1="736" y2="736" x1="592" />
            <wire x2="640" y1="736" y2="736" x1="616" />
            <wire x2="640" y1="720" y2="736" x1="640" />
            <wire x2="832" y1="720" y2="720" x1="640" />
            <wire x2="896" y1="720" y2="720" x1="832" />
            <wire x2="832" y1="720" y2="816" x1="832" />
            <wire x2="1408" y1="816" y2="816" x1="832" />
            <wire x2="832" y1="816" y2="1120" x1="832" />
            <wire x2="1408" y1="1120" y2="1120" x1="832" />
        </branch>
        <instance x="912" y="592" name="XLXI_1" orien="R0" />
        <instance x="896" y="752" name="XLXI_2" orien="R0" />
        <instance x="1408" y="688" name="XLXI_3" orien="R0" />
        <instance x="1408" y="880" name="XLXI_4" orien="R0" />
        <instance x="1408" y="1072" name="XLXI_5" orien="R0" />
        <instance x="1408" y="1248" name="XLXI_6" orien="R0" />
        <instance x="2112" y="768" name="XLXI_7" orien="R0" />
        <instance x="2128" y="912" name="XLXI_8" orien="R0" />
        <instance x="2128" y="1088" name="XLXI_9" orien="R0" />
        <instance x="2128" y="1280" name="XLXI_10" orien="R0" />
        <instance x="2560" y="1040" name="XLXI_11" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="1264" y1="560" y2="560" x1="1136" />
            <wire x2="1264" y1="560" y2="752" x1="1264" />
            <wire x2="1408" y1="752" y2="752" x1="1264" />
            <wire x2="1408" y1="560" y2="560" x1="1264" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1168" y1="720" y2="720" x1="1120" />
            <wire x2="1248" y1="720" y2="720" x1="1168" />
            <wire x2="1168" y1="720" y2="944" x1="1168" />
            <wire x2="1408" y1="944" y2="944" x1="1168" />
            <wire x2="1248" y1="624" y2="720" x1="1248" />
            <wire x2="1408" y1="624" y2="624" x1="1248" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="1888" y1="592" y2="592" x1="1664" />
            <wire x2="1888" y1="592" y2="640" x1="1888" />
            <wire x2="2112" y1="640" y2="640" x1="1888" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="2128" y1="784" y2="784" x1="1664" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1888" y1="976" y2="976" x1="1664" />
            <wire x2="1888" y1="960" y2="976" x1="1888" />
            <wire x2="2128" y1="960" y2="960" x1="1888" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="2128" y1="1152" y2="1152" x1="1664" />
        </branch>
        <branch name="I0">
            <wire x2="1312" y1="1520" y2="1520" x1="528" />
            <wire x2="1312" y1="704" y2="1520" x1="1312" />
            <wire x2="2112" y1="704" y2="704" x1="1312" />
        </branch>
        <branch name="I1">
            <wire x2="1632" y1="1760" y2="1760" x1="624" />
            <wire x2="1360" y1="848" y2="1664" x1="1360" />
            <wire x2="1632" y1="1664" y2="1664" x1="1360" />
            <wire x2="1632" y1="1664" y2="1760" x1="1632" />
            <wire x2="2128" y1="848" y2="848" x1="1360" />
        </branch>
        <branch name="I2">
            <wire x2="1712" y1="2016" y2="2016" x1="624" />
            <wire x2="1712" y1="1024" y2="2016" x1="1712" />
            <wire x2="2128" y1="1024" y2="1024" x1="1712" />
        </branch>
        <branch name="I3">
            <wire x2="592" y1="2288" y2="2288" x1="432" />
            <wire x2="2128" y1="1216" y2="1216" x1="592" />
            <wire x2="592" y1="1216" y2="2288" x1="592" />
        </branch>
        <iomarker fontsize="28" x="528" y="1520" name="I0" orien="R180" />
        <iomarker fontsize="28" x="624" y="1760" name="I1" orien="R180" />
        <iomarker fontsize="28" x="624" y="2016" name="I2" orien="R180" />
        <iomarker fontsize="28" x="432" y="2288" name="I3" orien="R180" />
        <branch name="XLXN_25">
            <wire x2="2560" y1="672" y2="672" x1="2368" />
            <wire x2="2560" y1="672" y2="784" x1="2560" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="2464" y1="816" y2="816" x1="2384" />
            <wire x2="2464" y1="816" y2="848" x1="2464" />
            <wire x2="2560" y1="848" y2="848" x1="2464" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="2464" y1="992" y2="992" x1="2384" />
            <wire x2="2464" y1="912" y2="992" x1="2464" />
            <wire x2="2560" y1="912" y2="912" x1="2464" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="2560" y1="1184" y2="1184" x1="2384" />
            <wire x2="2560" y1="976" y2="1184" x1="2560" />
        </branch>
        <branch name="o">
            <wire x2="2848" y1="880" y2="880" x1="2816" />
        </branch>
        <iomarker fontsize="28" x="2848" y="880" name="o" orien="R0" />
    </sheet>
</drawing>