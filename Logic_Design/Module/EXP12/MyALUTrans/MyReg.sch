<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="I(3:0)" />
        <signal name="I(0)" />
        <signal name="I(1)" />
        <signal name="I(2)" />
        <signal name="I(3)" />
        <signal name="Load" />
        <signal name="clock" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="O(3:0)" />
        <signal name="O(3)" />
        <signal name="O(0)" />
        <signal name="O(2)" />
        <signal name="O(1)" />
        <signal name="XLXN_32" />
        <port polarity="Input" name="I(3:0)" />
        <port polarity="Input" name="Load" />
        <port polarity="Input" name="clock" />
        <port polarity="Output" name="O(3:0)" />
        <blockdef name="m2_1">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="96" y1="-64" y2="-192" x1="96" />
            <line x2="96" y1="-96" y2="-64" x1="256" />
            <line x2="256" y1="-160" y2="-96" x1="256" />
            <line x2="256" y1="-192" y2="-160" x1="96" />
            <line x2="96" y1="-32" y2="-32" x1="176" />
            <line x2="176" y1="-80" y2="-32" x1="176" />
            <line x2="96" y1="-32" y2="-32" x1="0" />
            <line x2="256" y1="-128" y2="-128" x1="320" />
            <line x2="96" y1="-96" y2="-96" x1="0" />
            <line x2="96" y1="-160" y2="-160" x1="0" />
        </blockdef>
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <block symbolname="m2_1" name="XLXI_1">
            <blockpin signalname="O(1)" name="D0" />
            <blockpin signalname="I(1)" name="D1" />
            <blockpin signalname="Load" name="S0" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_2">
            <blockpin signalname="O(2)" name="D0" />
            <blockpin signalname="I(2)" name="D1" />
            <blockpin signalname="Load" name="S0" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_3">
            <blockpin signalname="O(3)" name="D0" />
            <blockpin signalname="I(3)" name="D1" />
            <blockpin signalname="Load" name="S0" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="m2_1" name="XLXI_4">
            <blockpin signalname="O(0)" name="D0" />
            <blockpin signalname="I(0)" name="D1" />
            <blockpin signalname="Load" name="S0" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_5">
            <blockpin signalname="clock" name="C" />
            <blockpin signalname="XLXN_11" name="D" />
            <blockpin signalname="O(0)" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_6">
            <blockpin signalname="clock" name="C" />
            <blockpin signalname="XLXN_12" name="D" />
            <blockpin signalname="O(1)" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_7">
            <blockpin signalname="clock" name="C" />
            <blockpin signalname="XLXN_13" name="D" />
            <blockpin signalname="O(2)" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_8">
            <blockpin signalname="clock" name="C" />
            <blockpin signalname="XLXN_14" name="D" />
            <blockpin signalname="O(3)" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="704" y="704" name="XLXI_1" orien="R0" />
        <instance x="720" y="1152" name="XLXI_2" orien="R0" />
        <instance x="720" y="1536" name="XLXI_3" orien="R0" />
        <branch name="I(3:0)">
            <wire x2="384" y1="1952" y2="1952" x1="240" />
            <wire x2="384" y1="176" y2="272" x1="384" />
            <wire x2="384" y1="272" y2="608" x1="384" />
            <wire x2="384" y1="608" y2="1056" x1="384" />
            <wire x2="384" y1="1056" y2="1440" x1="384" />
            <wire x2="384" y1="1440" y2="1952" x1="384" />
        </branch>
        <iomarker fontsize="28" x="240" y="1952" name="I(3:0)" orien="R180" />
        <instance x="704" y="368" name="XLXI_4" orien="R0" />
        <bustap x2="480" y1="272" y2="272" x1="384" />
        <branch name="I(0)">
            <wire x2="704" y1="272" y2="272" x1="480" />
        </branch>
        <bustap x2="480" y1="608" y2="608" x1="384" />
        <branch name="I(1)">
            <wire x2="496" y1="608" y2="608" x1="480" />
            <wire x2="592" y1="608" y2="608" x1="496" />
            <wire x2="704" y1="608" y2="608" x1="592" />
        </branch>
        <bustap x2="480" y1="1056" y2="1056" x1="384" />
        <branch name="I(2)">
            <wire x2="720" y1="1056" y2="1056" x1="480" />
        </branch>
        <bustap x2="480" y1="1440" y2="1440" x1="384" />
        <branch name="I(3)">
            <wire x2="496" y1="1440" y2="1440" x1="480" />
            <wire x2="512" y1="1440" y2="1440" x1="496" />
            <wire x2="720" y1="1440" y2="1440" x1="512" />
        </branch>
        <instance x="1504" y="496" name="XLXI_5" orien="R0" />
        <instance x="1504" y="1216" name="XLXI_7" orien="R0" />
        <instance x="1504" y="1568" name="XLXI_8" orien="R0" />
        <branch name="Load">
            <wire x2="704" y1="336" y2="336" x1="560" />
            <wire x2="560" y1="336" y2="672" x1="560" />
            <wire x2="704" y1="672" y2="672" x1="560" />
            <wire x2="560" y1="672" y2="1120" x1="560" />
            <wire x2="720" y1="1120" y2="1120" x1="560" />
            <wire x2="560" y1="1120" y2="1504" x1="560" />
            <wire x2="560" y1="1504" y2="1728" x1="560" />
            <wire x2="560" y1="1728" y2="1840" x1="560" />
            <wire x2="720" y1="1504" y2="1504" x1="560" />
        </branch>
        <iomarker fontsize="28" x="560" y="1840" name="Load" orien="R90" />
        <branch name="clock">
            <wire x2="1264" y1="1872" y2="1872" x1="1104" />
            <wire x2="1504" y1="368" y2="368" x1="1264" />
            <wire x2="1264" y1="368" y2="736" x1="1264" />
            <wire x2="1264" y1="736" y2="1088" x1="1264" />
            <wire x2="1264" y1="1088" y2="1440" x1="1264" />
            <wire x2="1264" y1="1440" y2="1872" x1="1264" />
            <wire x2="1504" y1="1440" y2="1440" x1="1264" />
            <wire x2="1504" y1="1088" y2="1088" x1="1264" />
            <wire x2="1504" y1="736" y2="736" x1="1264" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="1504" y1="240" y2="240" x1="1024" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="1248" y1="1024" y2="1024" x1="1040" />
            <wire x2="1248" y1="960" y2="1024" x1="1248" />
            <wire x2="1504" y1="960" y2="960" x1="1248" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="1248" y1="1408" y2="1408" x1="1040" />
            <wire x2="1248" y1="1312" y2="1408" x1="1248" />
            <wire x2="1504" y1="1312" y2="1312" x1="1248" />
        </branch>
        <iomarker fontsize="28" x="1104" y="1872" name="clock" orien="R180" />
        <branch name="O(3:0)">
            <wire x2="2592" y1="112" y2="112" x1="2368" />
            <wire x2="2368" y1="112" y2="240" x1="2368" />
            <wire x2="2368" y1="240" y2="608" x1="2368" />
            <wire x2="2368" y1="608" y2="960" x1="2368" />
            <wire x2="2368" y1="960" y2="1312" x1="2368" />
            <wire x2="2368" y1="1312" y2="1584" x1="2368" />
        </branch>
        <iomarker fontsize="28" x="2592" y="112" name="O(3:0)" orien="R0" />
        <instance x="1504" y="864" name="XLXI_6" orien="R0" />
        <branch name="XLXN_12">
            <wire x2="1040" y1="576" y2="576" x1="1024" />
            <wire x2="1040" y1="576" y2="608" x1="1040" />
            <wire x2="1504" y1="608" y2="608" x1="1040" />
        </branch>
        <bustap x2="2272" y1="240" y2="240" x1="2368" />
        <branch name="O(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2064" y="240" type="branch" />
            <wire x2="624" y1="128" y2="208" x1="624" />
            <wire x2="704" y1="208" y2="208" x1="624" />
            <wire x2="1984" y1="128" y2="128" x1="624" />
            <wire x2="1984" y1="128" y2="240" x1="1984" />
            <wire x2="2048" y1="240" y2="240" x1="1984" />
            <wire x2="2064" y1="240" y2="240" x1="2048" />
            <wire x2="2272" y1="240" y2="240" x1="2064" />
            <wire x2="1984" y1="240" y2="240" x1="1888" />
        </branch>
        <bustap x2="2272" y1="960" y2="960" x1="2368" />
        <branch name="O(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2112" y="960" type="branch" />
            <wire x2="640" y1="848" y2="992" x1="640" />
            <wire x2="720" y1="992" y2="992" x1="640" />
            <wire x2="1984" y1="848" y2="848" x1="640" />
            <wire x2="1984" y1="848" y2="960" x1="1984" />
            <wire x2="2112" y1="960" y2="960" x1="1984" />
            <wire x2="2272" y1="960" y2="960" x1="2112" />
            <wire x2="1984" y1="960" y2="960" x1="1888" />
        </branch>
        <bustap x2="2272" y1="1312" y2="1312" x1="2368" />
        <branch name="O(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="1312" type="branch" />
            <wire x2="720" y1="1376" y2="1376" x1="656" />
            <wire x2="656" y1="1376" y2="1584" x1="656" />
            <wire x2="1984" y1="1584" y2="1584" x1="656" />
            <wire x2="1984" y1="1312" y2="1312" x1="1888" />
            <wire x2="2144" y1="1312" y2="1312" x1="1984" />
            <wire x2="2272" y1="1312" y2="1312" x1="2144" />
            <wire x2="1984" y1="1312" y2="1584" x1="1984" />
        </branch>
        <bustap x2="2272" y1="608" y2="608" x1="2368" />
        <branch name="O(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2080" y="608" type="branch" />
            <wire x2="640" y1="448" y2="544" x1="640" />
            <wire x2="704" y1="544" y2="544" x1="640" />
            <wire x2="1984" y1="448" y2="448" x1="640" />
            <wire x2="1984" y1="448" y2="608" x1="1984" />
            <wire x2="2080" y1="608" y2="608" x1="1984" />
            <wire x2="2272" y1="608" y2="608" x1="2080" />
            <wire x2="1984" y1="608" y2="608" x1="1888" />
        </branch>
    </sheet>
</drawing>