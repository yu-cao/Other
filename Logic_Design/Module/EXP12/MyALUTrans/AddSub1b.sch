<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="A" />
        <signal name="XLXN_2" />
        <signal name="Ctrl" />
        <signal name="B" />
        <signal name="Ci" />
        <signal name="S" />
        <signal name="Co" />
        <port polarity="Input" name="A" />
        <port polarity="Input" name="Ctrl" />
        <port polarity="Input" name="B" />
        <port polarity="Input" name="Ci" />
        <port polarity="Output" name="S" />
        <port polarity="Output" name="Co" />
        <blockdef name="Adder1b">
            <timestamp>2017-11-22T5:27:31</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <block symbolname="Adder1b" name="XLXI_1">
            <blockpin signalname="A" name="A" />
            <blockpin signalname="XLXN_2" name="B" />
            <blockpin signalname="Ci" name="C" />
            <blockpin signalname="S" name="S" />
            <blockpin signalname="Co" name="Co" />
        </block>
        <block symbolname="xor2" name="XLXI_5">
            <blockpin signalname="Ctrl" name="I0" />
            <blockpin signalname="B" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1632" y="1264" name="XLXI_1" orien="R0">
        </instance>
        <branch name="A">
            <wire x2="1632" y1="1104" y2="1104" x1="1200" />
        </branch>
        <iomarker fontsize="28" x="1200" y="1104" name="A" orien="R180" />
        <instance x="1088" y="1312" name="XLXI_5" orien="R0" />
        <branch name="XLXN_2">
            <wire x2="1488" y1="1216" y2="1216" x1="1344" />
            <wire x2="1488" y1="1168" y2="1216" x1="1488" />
            <wire x2="1632" y1="1168" y2="1168" x1="1488" />
        </branch>
        <branch name="Ctrl">
            <wire x2="1088" y1="1248" y2="1248" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1056" y="1248" name="Ctrl" orien="R180" />
        <branch name="B">
            <wire x2="1088" y1="1184" y2="1184" x1="1056" />
        </branch>
        <iomarker fontsize="28" x="1056" y="1184" name="B" orien="R180" />
        <branch name="Ci">
            <wire x2="1584" y1="1344" y2="1344" x1="1056" />
            <wire x2="1632" y1="1232" y2="1232" x1="1584" />
            <wire x2="1584" y1="1232" y2="1344" x1="1584" />
        </branch>
        <iomarker fontsize="28" x="1056" y="1344" name="Ci" orien="R180" />
        <branch name="S">
            <wire x2="2176" y1="1104" y2="1104" x1="2016" />
        </branch>
        <branch name="Co">
            <wire x2="2176" y1="1232" y2="1232" x1="2016" />
        </branch>
        <iomarker fontsize="28" x="2176" y="1104" name="S" orien="R0" />
        <iomarker fontsize="28" x="2176" y="1232" name="Co" orien="R0" />
    </sheet>
</drawing>