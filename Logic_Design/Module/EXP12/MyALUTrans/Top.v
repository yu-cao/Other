`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:57:03 12/20/2017 
// Design Name: 
// Module Name:    Top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
    input wire clk,
    input wire [3:0]SW,
    input wire [1:0]SW2,
    input wire mode,
    input wire [1:0]busSelect,
    input wire [2:0]load,
    output wire [3:0]AN,
    output wire [7:0]SEGMENT,
    output wire Buzzer
);

    wire [3:0] A;
    wire [3:0] B;
    wire [1:0] btn_out;
    wire [3:0] C;
    wire Co;
    wire [3:0] ALUResult;
    wire [3:0] globalBus;
    wire [15:0] createdNumber;
    wire [11:0] regABCIn;
    wire [31:0] clk_div;
    MyReg regxA(clk_div[17],regABCIn[3:0],load[0],A);
    MyReg regxB(clk_div[17],regABCIn[7:4],load[1],B);
    MyReg regxC(clk_div[17],regABCIn[11:8],load[2],C);

    Mux4to14b muxxa(createdNumber[3:0],globalBus,4'b0,4'b0,{1'b0,mode},regABCIn[3:0]);
    Mux4to14b muxxb(createdNumber[7:4],globalBus,4'b0,4'b0,{1'b0,mode},regABCIn[7:4]);
    Mux4to14b muxxc(ALUResult,globalBus,4'b0,4'b0,{1'b0,mode},regABCIn[11:8]);
    Mux4to14b muxgb(A,B,C,4'b0,busSelect,globalBus);

    pbdebounce mo(clk_div[17],SW[0],btn_out[0]);
    pbdebounce m1(clk_div[17],SW[1],btn_out[1]);

    clkdiv m2(clk,0,clk_div);
    CreateNumber({2'b0,btn_out},{2'b0,SW[3:2]},createdNumber);

    myALU m5(A,B,SW2,ALUResult,Co);
    
    DispNum m6(clk,{A,B,ALUResult,C},4'b0,4'b0,1'b0,AN,SEGMENT);
    VCC XLXI_5 (.P(Buzzer));
    
endmodule