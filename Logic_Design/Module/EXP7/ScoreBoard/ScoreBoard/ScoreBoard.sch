<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <blockdef name="Mux4To1_single">
            <timestamp>2017-11-9T5:12:35</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="Mux4To1">
            <timestamp>2017-11-2T6:27:34</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <block symbolname="Mux4To1_single" name="XLXI_1">
            <blockpin name="S(1:0)" />
            <blockpin name="I0" />
            <blockpin name="I1" />
            <blockpin name="I2" />
            <blockpin name="I3" />
            <blockpin name="o" />
        </block>
        <block symbolname="Mux4To1_single" name="XLXI_2">
            <blockpin name="S(1:0)" />
            <blockpin name="I0" />
            <blockpin name="I1" />
            <blockpin name="I2" />
            <blockpin name="I3" />
            <blockpin name="o" />
        </block>
        <block symbolname="Mux4To1" name="XLXI_3">
            <blockpin name="s(1:0)" />
            <blockpin name="I0(3:0)" />
            <blockpin name="I1(3:0)" />
            <blockpin name="I2(3:0)" />
            <blockpin name="I3(3:0)" />
            <blockpin name="o(3:0)" />
        </block>
        <block symbolname="Mux4To1" name="XLXI_4">
            <blockpin name="s(1:0)" />
            <blockpin name="I0(3:0)" />
            <blockpin name="I1(3:0)" />
            <blockpin name="I2(3:0)" />
            <blockpin name="I3(3:0)" />
            <blockpin name="o(3:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="864" y="1392" name="XLXI_1" orien="R0">
        </instance>
        <instance x="864" y="1888" name="XLXI_2" orien="R0">
        </instance>
        <instance x="864" y="912" name="XLXI_3" orien="R0">
        </instance>
        <instance x="864" y="2352" name="XLXI_4" orien="R0">
        </instance>
    </sheet>
</drawing>