<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="point" />
        <signal name="p" />
        <signal name="LE" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="D0" />
        <signal name="D1" />
        <signal name="D2" />
        <signal name="D3" />
        <signal name="XLXN_40" />
        <signal name="XLXN_42" />
        <signal name="XLXN_44" />
        <signal name="XLXN_47" />
        <signal name="g" />
        <signal name="f" />
        <signal name="e" />
        <signal name="d" />
        <signal name="c" />
        <signal name="b" />
        <signal name="a" />
        <port polarity="Input" name="point" />
        <port polarity="Output" name="p" />
        <port polarity="Input" name="LE" />
        <port polarity="Input" name="D0" />
        <port polarity="Input" name="D1" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D3" />
        <port polarity="Output" name="g" />
        <port polarity="Output" name="f" />
        <port polarity="Output" name="e" />
        <port polarity="Output" name="d" />
        <port polarity="Output" name="c" />
        <port polarity="Output" name="b" />
        <port polarity="Output" name="a" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="or2" name="XLXI_1">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="g" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_2">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="f" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="e" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="d" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="c" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_6">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="b" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_7">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_11" name="I1" />
            <blockpin signalname="a" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_8">
            <blockpin signalname="XLXN_12" name="I0" />
            <blockpin signalname="XLXN_13" name="I1" />
            <blockpin signalname="XLXN_14" name="I2" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_9">
            <blockpin signalname="XLXN_19" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_21" name="I2" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_10">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="XLXN_25" name="I1" />
            <blockpin signalname="XLXN_27" name="I2" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_11">
            <blockpin signalname="XLXN_15" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_17" name="I2" />
            <blockpin signalname="XLXN_31" name="I3" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_12">
            <blockpin signalname="XLXN_22" name="I0" />
            <blockpin signalname="XLXN_23" name="I1" />
            <blockpin signalname="XLXN_32" name="I2" />
            <blockpin signalname="XLXN_33" name="I3" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_13">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="XLXN_28" name="I2" />
            <blockpin signalname="XLXN_29" name="I3" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_14">
            <blockpin signalname="XLXN_30" name="I0" />
            <blockpin signalname="XLXN_31" name="I1" />
            <blockpin signalname="XLXN_32" name="I2" />
            <blockpin signalname="XLXN_33" name="I3" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_15">
            <blockpin signalname="XLXN_44" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_16">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_17">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_18">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_20">
            <blockpin signalname="XLXN_44" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_21">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_22">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_23">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_26">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_40" name="I1" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_27">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_40" name="I3" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_28">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_30">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_42" name="I2" />
            <blockpin signalname="XLXN_40" name="I3" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_36">
            <blockpin signalname="point" name="I" />
            <blockpin signalname="p" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_37">
            <blockpin signalname="D0" name="I" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_38">
            <blockpin signalname="D1" name="I" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_39">
            <blockpin signalname="D2" name="I" />
            <blockpin signalname="XLXN_42" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_40">
            <blockpin signalname="D3" name="I" />
            <blockpin signalname="XLXN_40" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_24">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_25">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_31">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_40" name="I3" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_32">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_42" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_33">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_31" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_34">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_40" name="I3" />
            <blockpin signalname="XLXN_32" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_35">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_44" name="I2" />
            <blockpin signalname="XLXN_40" name="I3" />
            <blockpin signalname="XLXN_33" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_41">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_42" name="I2" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_42">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_42" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="1008" y="4752" name="XLXI_1" orien="R90" />
        <instance x="1872" y="4752" name="XLXI_2" orien="R90" />
        <instance x="2512" y="4736" name="XLXI_3" orien="R90" />
        <instance x="3216" y="4736" name="XLXI_4" orien="R90" />
        <instance x="3952" y="4736" name="XLXI_5" orien="R90" />
        <instance x="4688" y="4736" name="XLXI_6" orien="R90" />
        <instance x="5472" y="4736" name="XLXI_7" orien="R90" />
        <instance x="1008" y="4032" name="XLXI_8" orien="R90" />
        <instance x="2512" y="4048" name="XLXI_9" orien="R90" />
        <instance x="3968" y="4048" name="XLXI_10" orien="R90" />
        <instance x="1856" y="4048" name="XLXI_11" orien="R90" />
        <instance x="3200" y="4064" name="XLXI_12" orien="R90" />
        <instance x="4672" y="4080" name="XLXI_13" orien="R90" />
        <instance x="5456" y="4064" name="XLXI_14" orien="R90" />
        <instance x="1280" y="3504" name="XLXI_15" orien="R90" />
        <instance x="1552" y="3504" name="XLXI_16" orien="R90" />
        <instance x="1808" y="3504" name="XLXI_17" orien="R90" />
        <instance x="2032" y="3504" name="XLXI_18" orien="R90" />
        <instance x="2480" y="3504" name="XLXI_20" orien="R90" />
        <instance x="3376" y="3520" name="XLXI_21" orien="R90" />
        <instance x="3600" y="3520" name="XLXI_22" orien="R90" />
        <instance x="2768" y="3504" name="XLXI_26" orien="R90" />
        <instance x="976" y="3504" name="XLXI_27" orien="R90" />
        <instance x="672" y="3504" name="XLXI_28" orien="R90" />
        <instance x="3776" y="3504" name="XLXI_30" orien="R90" />
        <instance x="496" y="3024" name="XLXI_36" orien="R90" />
        <instance x="5312" y="2368" name="XLXI_37" orien="R90" />
        <instance x="5536" y="2368" name="XLXI_38" orien="R90" />
        <instance x="5728" y="2368" name="XLXI_39" orien="R90" />
        <instance x="5936" y="2368" name="XLXI_40" orien="R90" />
        <branch name="point">
            <wire x2="528" y1="2112" y2="3024" x1="528" />
        </branch>
        <branch name="p">
            <wire x2="528" y1="3248" y2="5200" x1="528" />
        </branch>
        <branch name="LE">
            <wire x2="336" y1="2112" y2="4608" x1="336" />
            <wire x2="1072" y1="4608" y2="4608" x1="336" />
            <wire x2="1072" y1="4608" y2="4752" x1="1072" />
            <wire x2="1936" y1="4608" y2="4608" x1="1072" />
            <wire x2="1936" y1="4608" y2="4752" x1="1936" />
            <wire x2="2576" y1="4608" y2="4608" x1="1936" />
            <wire x2="2576" y1="4608" y2="4736" x1="2576" />
            <wire x2="3280" y1="4608" y2="4608" x1="2576" />
            <wire x2="3280" y1="4608" y2="4736" x1="3280" />
            <wire x2="4016" y1="4608" y2="4608" x1="3280" />
            <wire x2="4016" y1="4608" y2="4736" x1="4016" />
            <wire x2="4752" y1="4608" y2="4608" x1="4016" />
            <wire x2="5536" y1="4608" y2="4608" x1="4752" />
            <wire x2="5536" y1="4608" y2="4736" x1="5536" />
            <wire x2="4752" y1="4608" y2="4736" x1="4752" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1136" y1="4288" y2="4752" x1="1136" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="2000" y1="4528" y2="4752" x1="2000" />
            <wire x2="2016" y1="4528" y2="4528" x1="2000" />
            <wire x2="2016" y1="4304" y2="4528" x1="2016" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="2640" y1="4304" y2="4736" x1="2640" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="3344" y1="4528" y2="4736" x1="3344" />
            <wire x2="3360" y1="4528" y2="4528" x1="3344" />
            <wire x2="3360" y1="4320" y2="4528" x1="3360" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="4080" y1="4512" y2="4736" x1="4080" />
            <wire x2="4096" y1="4512" y2="4512" x1="4080" />
            <wire x2="4096" y1="4304" y2="4512" x1="4096" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="4816" y1="4528" y2="4736" x1="4816" />
            <wire x2="4832" y1="4528" y2="4528" x1="4816" />
            <wire x2="4832" y1="4336" y2="4528" x1="4832" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="5600" y1="4528" y2="4736" x1="5600" />
            <wire x2="5616" y1="4528" y2="4528" x1="5600" />
            <wire x2="5616" y1="4320" y2="4528" x1="5616" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="832" y1="3760" y2="4032" x1="832" />
            <wire x2="1072" y1="4032" y2="4032" x1="832" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="1136" y1="3760" y2="4032" x1="1136" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="1408" y1="4032" y2="4032" x1="1200" />
            <wire x2="1408" y1="3760" y2="4032" x1="1408" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1680" y1="3760" y2="4048" x1="1680" />
            <wire x2="1920" y1="4048" y2="4048" x1="1680" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1936" y1="3760" y2="3904" x1="1936" />
            <wire x2="1984" y1="3904" y2="3904" x1="1936" />
            <wire x2="1984" y1="3904" y2="4048" x1="1984" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="2048" y1="3904" y2="4048" x1="2048" />
            <wire x2="2160" y1="3904" y2="3904" x1="2048" />
            <wire x2="2160" y1="3760" y2="3904" x1="2160" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="2384" y1="3760" y2="4048" x1="2384" />
            <wire x2="2576" y1="4048" y2="4048" x1="2384" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="2608" y1="3760" y2="3904" x1="2608" />
            <wire x2="2640" y1="3904" y2="3904" x1="2608" />
            <wire x2="2640" y1="3904" y2="4048" x1="2640" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="2864" y1="4048" y2="4048" x1="2704" />
            <wire x2="2864" y1="3760" y2="4048" x1="2864" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="3168" y1="3760" y2="4064" x1="3168" />
            <wire x2="3264" y1="4064" y2="4064" x1="3168" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="3328" y1="3920" y2="4064" x1="3328" />
            <wire x2="3504" y1="3920" y2="3920" x1="3328" />
            <wire x2="3504" y1="3776" y2="3920" x1="3504" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="3728" y1="3776" y2="4048" x1="3728" />
            <wire x2="4032" y1="4048" y2="4048" x1="3728" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="3936" y1="3760" y2="3904" x1="3936" />
            <wire x2="4096" y1="3904" y2="3904" x1="3936" />
            <wire x2="4096" y1="3904" y2="4048" x1="4096" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="4240" y1="3760" y2="4080" x1="4240" />
            <wire x2="4736" y1="4080" y2="4080" x1="4240" />
        </branch>
        <instance x="4352" y="3520" name="XLXI_24" orien="R90" />
        <instance x="4560" y="3520" name="XLXI_25" orien="R90" />
        <instance x="4784" y="3520" name="XLXI_31" orien="R90" />
        <instance x="5072" y="3520" name="XLXI_32" orien="R90" />
        <instance x="5376" y="3520" name="XLXI_33" orien="R90" />
        <instance x="5664" y="3520" name="XLXI_34" orien="R90" />
        <instance x="5968" y="3520" name="XLXI_35" orien="R90" />
        <instance x="4112" y="3504" name="XLXI_23" orien="R90" />
        <branch name="XLXN_27">
            <wire x2="4800" y1="4048" y2="4048" x1="4160" />
            <wire x2="4800" y1="4048" y2="4080" x1="4800" />
            <wire x2="4480" y1="3776" y2="3920" x1="4480" />
            <wire x2="4800" y1="3920" y2="3920" x1="4480" />
            <wire x2="4800" y1="3920" y2="4048" x1="4800" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="4688" y1="3776" y2="3904" x1="4688" />
            <wire x2="4864" y1="3904" y2="3904" x1="4688" />
            <wire x2="4864" y1="3904" y2="4080" x1="4864" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="4944" y1="4080" y2="4080" x1="4928" />
            <wire x2="4944" y1="3776" y2="4080" x1="4944" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="5232" y1="3776" y2="4064" x1="5232" />
            <wire x2="5520" y1="4064" y2="4064" x1="5232" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="5584" y1="3968" y2="3968" x1="2112" />
            <wire x2="5584" y1="3968" y2="4064" x1="5584" />
            <wire x2="2112" y1="3968" y2="4048" x1="2112" />
            <wire x2="5536" y1="3776" y2="3920" x1="5536" />
            <wire x2="5584" y1="3920" y2="3920" x1="5536" />
            <wire x2="5584" y1="3920" y2="3968" x1="5584" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="5648" y1="3984" y2="3984" x1="3392" />
            <wire x2="5648" y1="3984" y2="4064" x1="5648" />
            <wire x2="3392" y1="3984" y2="4064" x1="3392" />
            <wire x2="5824" y1="3920" y2="3920" x1="5648" />
            <wire x2="5648" y1="3920" y2="3984" x1="5648" />
            <wire x2="5824" y1="3776" y2="3920" x1="5824" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="6128" y1="4016" y2="4016" x1="3456" />
            <wire x2="6128" y1="4016" y2="4064" x1="6128" />
            <wire x2="3456" y1="4016" y2="4064" x1="3456" />
            <wire x2="6128" y1="4064" y2="4064" x1="5712" />
            <wire x2="6128" y1="3776" y2="4016" x1="6128" />
        </branch>
        <branch name="D2">
            <wire x2="864" y1="2896" y2="3504" x1="864" />
            <wire x2="1168" y1="2896" y2="2896" x1="864" />
            <wire x2="1168" y1="2896" y2="3504" x1="1168" />
            <wire x2="2608" y1="2896" y2="2896" x1="1168" />
            <wire x2="2608" y1="2896" y2="3504" x1="2608" />
            <wire x2="3568" y1="2896" y2="2896" x1="2608" />
            <wire x2="3568" y1="2896" y2="3520" x1="3568" />
            <wire x2="3728" y1="2896" y2="2896" x1="3568" />
            <wire x2="3728" y1="2896" y2="3520" x1="3728" />
            <wire x2="4480" y1="2896" y2="2896" x1="3728" />
            <wire x2="4480" y1="2896" y2="3520" x1="4480" />
            <wire x2="4752" y1="2896" y2="2896" x1="4480" />
            <wire x2="4752" y1="2896" y2="3520" x1="4752" />
            <wire x2="4976" y1="2896" y2="2896" x1="4752" />
            <wire x2="4976" y1="2896" y2="3520" x1="4976" />
            <wire x2="5600" y1="2896" y2="2896" x1="4976" />
            <wire x2="5664" y1="2896" y2="2896" x1="5600" />
            <wire x2="5856" y1="2896" y2="2896" x1="5664" />
            <wire x2="5856" y1="2896" y2="3520" x1="5856" />
            <wire x2="5600" y1="2896" y2="3504" x1="5600" />
            <wire x2="5568" y1="3504" y2="3520" x1="5568" />
            <wire x2="5600" y1="3504" y2="3504" x1="5568" />
            <wire x2="5760" y1="2256" y2="2256" x1="5664" />
            <wire x2="5760" y1="2256" y2="2368" x1="5760" />
            <wire x2="5664" y1="2256" y2="2896" x1="5664" />
            <wire x2="5760" y1="2096" y2="2256" x1="5760" />
        </branch>
        <branch name="D3">
            <wire x2="928" y1="3008" y2="3504" x1="928" />
            <wire x2="3264" y1="3008" y2="3008" x1="928" />
            <wire x2="3264" y1="3008" y2="3504" x1="3264" />
            <wire x2="3792" y1="3008" y2="3008" x1="3264" />
            <wire x2="3792" y1="3008" y2="3520" x1="3792" />
            <wire x2="4304" y1="3008" y2="3008" x1="3792" />
            <wire x2="4304" y1="3008" y2="3504" x1="4304" />
            <wire x2="4544" y1="3008" y2="3008" x1="4304" />
            <wire x2="4544" y1="3008" y2="3520" x1="4544" />
            <wire x2="5328" y1="3008" y2="3008" x1="4544" />
            <wire x2="5632" y1="3008" y2="3008" x1="5328" />
            <wire x2="5888" y1="3008" y2="3008" x1="5632" />
            <wire x2="5632" y1="3008" y2="3520" x1="5632" />
            <wire x2="5328" y1="3008" y2="3520" x1="5328" />
            <wire x2="5968" y1="2256" y2="2256" x1="5888" />
            <wire x2="5968" y1="2256" y2="2368" x1="5968" />
            <wire x2="5888" y1="2256" y2="3008" x1="5888" />
            <wire x2="5968" y1="2096" y2="2256" x1="5968" />
        </branch>
        <iomarker fontsize="28" x="5344" y="2080" name="D0" orien="R270" />
        <iomarker fontsize="28" x="5568" y="2080" name="D1" orien="R270" />
        <iomarker fontsize="28" x="5760" y="2096" name="D2" orien="R270" />
        <iomarker fontsize="28" x="5968" y="2096" name="D3" orien="R270" />
        <iomarker fontsize="28" x="528" y="2112" name="point" orien="R270" />
        <iomarker fontsize="28" x="336" y="2112" name="LE" orien="R270" />
        <branch name="XLXN_40">
            <wire x2="1232" y1="3360" y2="3504" x1="1232" />
            <wire x2="1472" y1="3360" y2="3360" x1="1232" />
            <wire x2="1472" y1="3360" y2="3504" x1="1472" />
            <wire x2="1744" y1="3360" y2="3360" x1="1472" />
            <wire x2="2000" y1="3360" y2="3360" x1="1744" />
            <wire x2="2224" y1="3360" y2="3360" x1="2000" />
            <wire x2="2224" y1="3360" y2="3504" x1="2224" />
            <wire x2="2672" y1="3360" y2="3360" x1="2224" />
            <wire x2="2896" y1="3360" y2="3360" x1="2672" />
            <wire x2="4032" y1="3360" y2="3360" x1="2896" />
            <wire x2="5040" y1="3360" y2="3360" x1="4032" />
            <wire x2="5968" y1="3360" y2="3360" x1="5040" />
            <wire x2="6224" y1="3360" y2="3360" x1="5968" />
            <wire x2="6224" y1="3360" y2="3520" x1="6224" />
            <wire x2="5968" y1="3360" y2="3520" x1="5968" />
            <wire x2="5040" y1="3360" y2="3520" x1="5040" />
            <wire x2="4032" y1="3360" y2="3504" x1="4032" />
            <wire x2="2896" y1="3360" y2="3504" x1="2896" />
            <wire x2="2672" y1="3360" y2="3504" x1="2672" />
            <wire x2="2000" y1="3360" y2="3504" x1="2000" />
            <wire x2="1744" y1="3360" y2="3504" x1="1744" />
            <wire x2="5968" y1="3520" y2="3520" x1="5920" />
            <wire x2="5968" y1="2592" y2="3360" x1="5968" />
        </branch>
        <branch name="XLXN_42">
            <wire x2="1408" y1="3264" y2="3504" x1="1408" />
            <wire x2="1936" y1="3264" y2="3264" x1="1408" />
            <wire x2="2160" y1="3264" y2="3264" x1="1936" />
            <wire x2="2448" y1="3264" y2="3264" x1="2160" />
            <wire x2="2448" y1="3264" y2="3504" x1="2448" />
            <wire x2="3200" y1="3264" y2="3264" x1="2448" />
            <wire x2="3968" y1="3264" y2="3264" x1="3200" />
            <wire x2="5264" y1="3264" y2="3264" x1="3968" />
            <wire x2="5760" y1="3264" y2="3264" x1="5264" />
            <wire x2="6096" y1="3264" y2="3264" x1="5760" />
            <wire x2="6096" y1="3264" y2="3520" x1="6096" />
            <wire x2="5264" y1="3264" y2="3520" x1="5264" />
            <wire x2="3968" y1="3264" y2="3504" x1="3968" />
            <wire x2="3200" y1="3264" y2="3504" x1="3200" />
            <wire x2="2160" y1="3264" y2="3504" x1="2160" />
            <wire x2="1936" y1="3264" y2="3504" x1="1936" />
            <wire x2="5760" y1="2592" y2="3264" x1="5760" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="800" y1="3184" y2="3504" x1="800" />
            <wire x2="1344" y1="3184" y2="3184" x1="800" />
            <wire x2="1344" y1="3184" y2="3504" x1="1344" />
            <wire x2="2384" y1="3184" y2="3184" x1="1344" />
            <wire x2="2384" y1="3184" y2="3504" x1="2384" />
            <wire x2="2544" y1="3184" y2="3184" x1="2384" />
            <wire x2="2544" y1="3184" y2="3504" x1="2544" />
            <wire x2="4912" y1="3184" y2="3184" x1="2544" />
            <wire x2="5504" y1="3184" y2="3184" x1="4912" />
            <wire x2="5568" y1="3184" y2="3184" x1="5504" />
            <wire x2="5792" y1="3184" y2="3184" x1="5568" />
            <wire x2="5792" y1="3184" y2="3520" x1="5792" />
            <wire x2="6160" y1="3184" y2="3184" x1="5792" />
            <wire x2="6160" y1="3184" y2="3520" x1="6160" />
            <wire x2="5504" y1="3184" y2="3520" x1="5504" />
            <wire x2="4912" y1="3184" y2="3520" x1="4912" />
            <wire x2="5568" y1="2592" y2="3184" x1="5568" />
        </branch>
        <instance x="2256" y="3504" name="XLXI_41" orien="R90" />
        <branch name="XLXN_47">
            <wire x2="736" y1="3088" y2="3504" x1="736" />
            <wire x2="3072" y1="3088" y2="3088" x1="736" />
            <wire x2="3072" y1="3088" y2="3504" x1="3072" />
            <wire x2="3840" y1="3088" y2="3088" x1="3072" />
            <wire x2="3840" y1="3088" y2="3504" x1="3840" />
            <wire x2="4416" y1="3088" y2="3088" x1="3840" />
            <wire x2="4416" y1="3088" y2="3520" x1="4416" />
            <wire x2="4624" y1="3088" y2="3088" x1="4416" />
            <wire x2="5344" y1="3088" y2="3088" x1="4624" />
            <wire x2="5728" y1="3088" y2="3088" x1="5344" />
            <wire x2="5728" y1="3088" y2="3520" x1="5728" />
            <wire x2="4624" y1="3088" y2="3520" x1="4624" />
            <wire x2="5344" y1="2592" y2="3088" x1="5344" />
        </branch>
        <instance x="3008" y="3504" name="XLXI_42" orien="R90" />
        <iomarker fontsize="28" x="528" y="5200" name="p" orien="R90" />
        <branch name="g">
            <wire x2="1104" y1="5008" y2="5040" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="1104" y="5040" name="g" orien="R90" />
        <branch name="f">
            <wire x2="1968" y1="5008" y2="5040" x1="1968" />
        </branch>
        <iomarker fontsize="28" x="1968" y="5040" name="f" orien="R90" />
        <branch name="e">
            <wire x2="2608" y1="4992" y2="5024" x1="2608" />
        </branch>
        <iomarker fontsize="28" x="2608" y="5024" name="e" orien="R90" />
        <branch name="d">
            <wire x2="3312" y1="4992" y2="5024" x1="3312" />
        </branch>
        <iomarker fontsize="28" x="3312" y="5024" name="d" orien="R90" />
        <branch name="c">
            <wire x2="4048" y1="4992" y2="5024" x1="4048" />
        </branch>
        <iomarker fontsize="28" x="4048" y="5024" name="c" orien="R90" />
        <branch name="b">
            <wire x2="4784" y1="4992" y2="5024" x1="4784" />
        </branch>
        <iomarker fontsize="28" x="4784" y="5024" name="b" orien="R90" />
        <branch name="a">
            <wire x2="5568" y1="4992" y2="5024" x1="5568" />
        </branch>
        <iomarker fontsize="28" x="5568" y="5024" name="a" orien="R90" />
        <branch name="D1">
            <wire x2="1104" y1="2768" y2="3504" x1="1104" />
            <wire x2="1680" y1="2768" y2="2768" x1="1104" />
            <wire x2="1680" y1="2768" y2="3504" x1="1680" />
            <wire x2="1872" y1="2768" y2="2768" x1="1680" />
            <wire x2="3152" y1="2768" y2="2768" x1="1872" />
            <wire x2="3152" y1="2768" y2="2960" x1="3152" />
            <wire x2="3504" y1="2768" y2="2768" x1="3152" />
            <wire x2="3504" y1="2768" y2="3520" x1="3504" />
            <wire x2="3664" y1="2768" y2="2768" x1="3504" />
            <wire x2="3904" y1="2768" y2="2768" x1="3664" />
            <wire x2="3904" y1="2768" y2="3504" x1="3904" />
            <wire x2="4240" y1="2768" y2="2768" x1="3904" />
            <wire x2="4240" y1="2768" y2="3504" x1="4240" />
            <wire x2="4688" y1="2768" y2="2768" x1="4240" />
            <wire x2="4688" y1="2768" y2="3520" x1="4688" />
            <wire x2="5200" y1="2768" y2="2768" x1="4688" />
            <wire x2="5456" y1="2768" y2="2768" x1="5200" />
            <wire x2="5200" y1="2768" y2="3520" x1="5200" />
            <wire x2="3664" y1="2768" y2="3520" x1="3664" />
            <wire x2="1872" y1="2768" y2="3504" x1="1872" />
            <wire x2="3136" y1="2960" y2="3504" x1="3136" />
            <wire x2="3152" y1="2960" y2="2960" x1="3136" />
            <wire x2="5568" y1="2256" y2="2256" x1="5456" />
            <wire x2="5568" y1="2256" y2="2368" x1="5568" />
            <wire x2="5456" y1="2256" y2="2768" x1="5456" />
            <wire x2="5568" y1="2080" y2="2256" x1="5568" />
        </branch>
        <branch name="D0">
            <wire x2="1040" y1="2640" y2="3504" x1="1040" />
            <wire x2="1616" y1="2640" y2="2640" x1="1040" />
            <wire x2="1616" y1="2640" y2="3504" x1="1616" />
            <wire x2="1872" y1="2640" y2="2640" x1="1616" />
            <wire x2="2096" y1="2640" y2="2640" x1="1872" />
            <wire x2="2096" y1="2640" y2="3504" x1="2096" />
            <wire x2="2320" y1="2640" y2="2640" x1="2096" />
            <wire x2="2320" y1="2640" y2="3504" x1="2320" />
            <wire x2="2832" y1="2640" y2="2640" x1="2320" />
            <wire x2="2832" y1="2640" y2="3504" x1="2832" />
            <wire x2="3440" y1="2640" y2="2640" x1="2832" />
            <wire x2="3440" y1="2640" y2="3520" x1="3440" />
            <wire x2="4176" y1="2640" y2="2640" x1="3440" />
            <wire x2="4176" y1="2640" y2="3504" x1="4176" />
            <wire x2="4848" y1="2640" y2="2640" x1="4176" />
            <wire x2="4848" y1="2640" y2="3520" x1="4848" />
            <wire x2="5136" y1="2640" y2="2640" x1="4848" />
            <wire x2="5200" y1="2640" y2="2640" x1="5136" />
            <wire x2="5440" y1="2640" y2="2640" x1="5200" />
            <wire x2="6032" y1="2640" y2="2640" x1="5440" />
            <wire x2="6032" y1="2640" y2="3520" x1="6032" />
            <wire x2="5440" y1="2640" y2="3520" x1="5440" />
            <wire x2="5136" y1="2640" y2="3520" x1="5136" />
            <wire x2="5344" y1="2256" y2="2256" x1="5200" />
            <wire x2="5344" y1="2256" y2="2368" x1="5344" />
            <wire x2="5200" y1="2256" y2="2640" x1="5200" />
            <wire x2="5344" y1="2080" y2="2256" x1="5344" />
        </branch>
    </sheet>
</drawing>