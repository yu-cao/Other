<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Scan(1:0)" />
        <signal name="Scan(1)" />
        <signal name="Scan(0)" />
        <signal name="XLXN_4" />
        <signal name="Hexs(15:0)" />
        <signal name="Hexs(3:0)" />
        <signal name="Hexs(7:4)" />
        <signal name="Hexs(11:8)" />
        <signal name="Hexs(15:12)" />
        <signal name="HEX(3:0)" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="AN(3:0)" />
        <signal name="AN(3)" />
        <signal name="AN(2)" />
        <signal name="AN(1)" />
        <signal name="AN(0)" />
        <signal name="p" />
        <signal name="LE" />
        <signal name="point(3:0)" />
        <signal name="LES(3:0)" />
        <signal name="point(3)" />
        <signal name="point(2)" />
        <signal name="point(1)" />
        <signal name="point(0)" />
        <signal name="LES(3)" />
        <signal name="LES(2)" />
        <signal name="LES(1)" />
        <signal name="LES(0)" />
        <port polarity="Input" name="Scan(1:0)" />
        <port polarity="Input" name="Hexs(15:0)" />
        <port polarity="Output" name="HEX(3:0)" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Output" name="p" />
        <port polarity="Output" name="LE" />
        <port polarity="Input" name="point(3:0)" />
        <port polarity="Input" name="LES(3:0)" />
        <blockdef name="Mux4To1">
            <timestamp>2017-11-2T6:27:34</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="Mux4To1_single">
            <timestamp>2017-11-9T5:12:35</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="d2_4e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="Mux4To1" name="XLXI_1">
            <blockpin signalname="Scan(1:0)" name="s(1:0)" />
            <blockpin signalname="Hexs(3:0)" name="I0(3:0)" />
            <blockpin signalname="Hexs(7:4)" name="I1(3:0)" />
            <blockpin signalname="Hexs(11:8)" name="I2(3:0)" />
            <blockpin signalname="Hexs(15:12)" name="I3(3:0)" />
            <blockpin signalname="HEX(3:0)" name="o(3:0)" />
        </block>
        <block symbolname="Mux4To1_single" name="XLXI_2">
            <blockpin signalname="Scan(1:0)" name="S(1:0)" />
            <blockpin signalname="point(0)" name="I0" />
            <blockpin signalname="point(1)" name="I1" />
            <blockpin signalname="point(2)" name="I2" />
            <blockpin signalname="point(3)" name="I3" />
            <blockpin signalname="p" name="o" />
        </block>
        <block symbolname="Mux4To1_single" name="XLXI_3">
            <blockpin signalname="Scan(1:0)" name="S(1:0)" />
            <blockpin signalname="LES(0)" name="I0" />
            <blockpin signalname="LES(1)" name="I1" />
            <blockpin signalname="LES(2)" name="I2" />
            <blockpin signalname="LES(3)" name="I3" />
            <blockpin signalname="LE" name="o" />
        </block>
        <block symbolname="d2_4e" name="XLXI_4">
            <blockpin signalname="Scan(0)" name="A0" />
            <blockpin signalname="Scan(1)" name="A1" />
            <blockpin signalname="XLXN_4" name="E" />
            <blockpin signalname="XLXN_13" name="D0" />
            <blockpin signalname="XLXN_14" name="D1" />
            <blockpin signalname="XLXN_15" name="D2" />
            <blockpin signalname="XLXN_16" name="D3" />
        </block>
        <block symbolname="vcc" name="XLXI_5">
            <blockpin signalname="XLXN_4" name="P" />
        </block>
        <block symbolname="inv" name="XLXI_6">
            <blockpin signalname="XLXN_13" name="I" />
            <blockpin signalname="AN(0)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_7">
            <blockpin signalname="XLXN_14" name="I" />
            <blockpin signalname="AN(1)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_8">
            <blockpin signalname="XLXN_15" name="I" />
            <blockpin signalname="AN(2)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="XLXN_16" name="I" />
            <blockpin signalname="AN(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1168" y="992" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1168" y="1520" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1168" y="1952" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1168" y="2496" name="XLXI_4" orien="R0" />
        <branch name="Scan(1:0)">
            <wire x2="912" y1="560" y2="560" x1="720" />
            <wire x2="912" y1="560" y2="704" x1="912" />
            <wire x2="1168" y1="704" y2="704" x1="912" />
            <wire x2="912" y1="704" y2="1232" x1="912" />
            <wire x2="1168" y1="1232" y2="1232" x1="912" />
            <wire x2="912" y1="1232" y2="1664" x1="912" />
            <wire x2="1168" y1="1664" y2="1664" x1="912" />
            <wire x2="912" y1="1664" y2="2176" x1="912" />
            <wire x2="912" y1="2176" y2="2240" x1="912" />
            <wire x2="912" y1="2240" y2="2304" x1="912" />
        </branch>
        <iomarker fontsize="28" x="720" y="560" name="Scan(1:0)" orien="R180" />
        <bustap x2="1008" y1="2240" y2="2240" x1="912" />
        <branch name="Scan(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="2240" type="branch" />
            <wire x2="1088" y1="2240" y2="2240" x1="1008" />
            <wire x2="1168" y1="2240" y2="2240" x1="1088" />
        </branch>
        <bustap x2="1008" y1="2176" y2="2176" x1="912" />
        <branch name="Scan(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1088" y="2176" type="branch" />
            <wire x2="1088" y1="2176" y2="2176" x1="1008" />
            <wire x2="1168" y1="2176" y2="2176" x1="1088" />
        </branch>
        <instance x="944" y="2352" name="XLXI_5" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1008" y1="2352" y2="2368" x1="1008" />
            <wire x2="1168" y1="2368" y2="2368" x1="1008" />
        </branch>
        <branch name="Hexs(15:0)">
            <wire x2="736" y1="832" y2="832" x1="608" />
            <wire x2="736" y1="832" y2="896" x1="736" />
            <wire x2="736" y1="896" y2="960" x1="736" />
            <wire x2="736" y1="960" y2="976" x1="736" />
            <wire x2="736" y1="704" y2="736" x1="736" />
            <wire x2="736" y1="736" y2="816" x1="736" />
            <wire x2="736" y1="816" y2="832" x1="736" />
        </branch>
        <iomarker fontsize="28" x="608" y="832" name="Hexs(15:0)" orien="R180" />
        <bustap x2="832" y1="736" y2="736" x1="736" />
        <branch name="Hexs(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="840" y="736" type="branch" />
            <wire x2="848" y1="736" y2="736" x1="832" />
            <wire x2="864" y1="736" y2="736" x1="848" />
            <wire x2="864" y1="736" y2="768" x1="864" />
            <wire x2="1168" y1="768" y2="768" x1="864" />
        </branch>
        <bustap x2="832" y1="816" y2="816" x1="736" />
        <branch name="Hexs(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="840" y="816" type="branch" />
            <wire x2="848" y1="816" y2="816" x1="832" />
            <wire x2="864" y1="816" y2="816" x1="848" />
            <wire x2="864" y1="816" y2="832" x1="864" />
            <wire x2="1168" y1="832" y2="832" x1="864" />
        </branch>
        <bustap x2="832" y1="896" y2="896" x1="736" />
        <branch name="Hexs(11:8)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="840" y="896" type="branch" />
            <wire x2="848" y1="896" y2="896" x1="832" />
            <wire x2="864" y1="896" y2="896" x1="848" />
            <wire x2="1168" y1="896" y2="896" x1="864" />
        </branch>
        <bustap x2="832" y1="960" y2="960" x1="736" />
        <branch name="Hexs(15:12)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="840" y="960" type="branch" />
            <wire x2="848" y1="960" y2="960" x1="832" />
            <wire x2="864" y1="960" y2="960" x1="848" />
            <wire x2="1168" y1="960" y2="960" x1="864" />
        </branch>
        <branch name="HEX(3:0)">
            <wire x2="1664" y1="704" y2="704" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1664" y="704" name="HEX(3:0)" orien="R0" />
        <branch name="XLXN_13">
            <wire x2="1584" y1="2176" y2="2176" x1="1552" />
        </branch>
        <instance x="1584" y="2208" name="XLXI_6" orien="R0" />
        <branch name="XLXN_14">
            <wire x2="1584" y1="2240" y2="2240" x1="1552" />
        </branch>
        <instance x="1584" y="2272" name="XLXI_7" orien="R0" />
        <branch name="XLXN_15">
            <wire x2="1584" y1="2304" y2="2304" x1="1552" />
        </branch>
        <instance x="1584" y="2336" name="XLXI_8" orien="R0" />
        <branch name="XLXN_16">
            <wire x2="1584" y1="2368" y2="2368" x1="1552" />
        </branch>
        <instance x="1584" y="2400" name="XLXI_9" orien="R0" />
        <branch name="AN(3:0)">
            <wire x2="2080" y1="2128" y2="2176" x1="2080" />
            <wire x2="2080" y1="2176" y2="2240" x1="2080" />
            <wire x2="2080" y1="2240" y2="2304" x1="2080" />
            <wire x2="2080" y1="2304" y2="2368" x1="2080" />
            <wire x2="2080" y1="2368" y2="2416" x1="2080" />
            <wire x2="2176" y1="2416" y2="2416" x1="2080" />
        </branch>
        <iomarker fontsize="28" x="2176" y="2416" name="AN(3:0)" orien="R0" />
        <bustap x2="1984" y1="2368" y2="2368" x1="2080" />
        <branch name="AN(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="2368" type="branch" />
            <wire x2="1952" y1="2368" y2="2368" x1="1808" />
            <wire x2="1968" y1="2368" y2="2368" x1="1952" />
            <wire x2="1984" y1="2368" y2="2368" x1="1968" />
        </branch>
        <bustap x2="1984" y1="2304" y2="2304" x1="2080" />
        <branch name="AN(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="2304" type="branch" />
            <wire x2="1952" y1="2304" y2="2304" x1="1808" />
            <wire x2="1968" y1="2304" y2="2304" x1="1952" />
            <wire x2="1984" y1="2304" y2="2304" x1="1968" />
        </branch>
        <bustap x2="1984" y1="2240" y2="2240" x1="2080" />
        <branch name="AN(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="2240" type="branch" />
            <wire x2="1952" y1="2240" y2="2240" x1="1808" />
            <wire x2="1968" y1="2240" y2="2240" x1="1952" />
            <wire x2="1984" y1="2240" y2="2240" x1="1968" />
        </branch>
        <bustap x2="1984" y1="2176" y2="2176" x1="2080" />
        <branch name="AN(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1968" y="2176" type="branch" />
            <wire x2="1952" y1="2176" y2="2176" x1="1808" />
            <wire x2="1968" y1="2176" y2="2176" x1="1952" />
            <wire x2="1984" y1="2176" y2="2176" x1="1968" />
        </branch>
        <branch name="p">
            <wire x2="1664" y1="1232" y2="1232" x1="1552" />
        </branch>
        <branch name="LE">
            <wire x2="1664" y1="1664" y2="1664" x1="1552" />
        </branch>
        <iomarker fontsize="28" x="1664" y="1232" name="p" orien="R0" />
        <iomarker fontsize="28" x="1664" y="1664" name="LE" orien="R0" />
        <branch name="point(3:0)">
            <wire x2="736" y1="1232" y2="1232" x1="560" />
            <wire x2="736" y1="1232" y2="1296" x1="736" />
            <wire x2="736" y1="1296" y2="1360" x1="736" />
            <wire x2="736" y1="1360" y2="1424" x1="736" />
            <wire x2="736" y1="1424" y2="1488" x1="736" />
            <wire x2="736" y1="1488" y2="1504" x1="736" />
            <wire x2="736" y1="1504" y2="1536" x1="736" />
        </branch>
        <branch name="LES(3:0)">
            <wire x2="736" y1="1696" y2="1696" x1="576" />
            <wire x2="736" y1="1696" y2="1728" x1="736" />
            <wire x2="736" y1="1728" y2="1792" x1="736" />
            <wire x2="736" y1="1792" y2="1856" x1="736" />
            <wire x2="736" y1="1856" y2="1920" x1="736" />
            <wire x2="736" y1="1920" y2="1968" x1="736" />
        </branch>
        <iomarker fontsize="28" x="560" y="1232" name="point(3:0)" orien="R180" />
        <iomarker fontsize="28" x="576" y="1696" name="LES(3:0)" orien="R180" />
        <bustap x2="832" y1="1488" y2="1488" x1="736" />
        <branch name="point(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1488" type="branch" />
            <wire x2="848" y1="1488" y2="1488" x1="832" />
            <wire x2="864" y1="1488" y2="1488" x1="848" />
            <wire x2="1168" y1="1488" y2="1488" x1="864" />
        </branch>
        <bustap x2="832" y1="1424" y2="1424" x1="736" />
        <branch name="point(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1424" type="branch" />
            <wire x2="848" y1="1424" y2="1424" x1="832" />
            <wire x2="864" y1="1424" y2="1424" x1="848" />
            <wire x2="1168" y1="1424" y2="1424" x1="864" />
        </branch>
        <bustap x2="832" y1="1360" y2="1360" x1="736" />
        <branch name="point(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1360" type="branch" />
            <wire x2="848" y1="1360" y2="1360" x1="832" />
            <wire x2="864" y1="1360" y2="1360" x1="848" />
            <wire x2="1168" y1="1360" y2="1360" x1="864" />
        </branch>
        <bustap x2="832" y1="1296" y2="1296" x1="736" />
        <branch name="point(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1296" type="branch" />
            <wire x2="848" y1="1296" y2="1296" x1="832" />
            <wire x2="864" y1="1296" y2="1296" x1="848" />
            <wire x2="1168" y1="1296" y2="1296" x1="864" />
        </branch>
        <bustap x2="832" y1="1920" y2="1920" x1="736" />
        <branch name="LES(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1920" type="branch" />
            <wire x2="848" y1="1920" y2="1920" x1="832" />
            <wire x2="864" y1="1920" y2="1920" x1="848" />
            <wire x2="1168" y1="1920" y2="1920" x1="864" />
        </branch>
        <bustap x2="832" y1="1856" y2="1856" x1="736" />
        <branch name="LES(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1856" type="branch" />
            <wire x2="848" y1="1856" y2="1856" x1="832" />
            <wire x2="864" y1="1856" y2="1856" x1="848" />
            <wire x2="1168" y1="1856" y2="1856" x1="864" />
        </branch>
        <bustap x2="832" y1="1792" y2="1792" x1="736" />
        <branch name="LES(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1792" type="branch" />
            <wire x2="848" y1="1792" y2="1792" x1="832" />
            <wire x2="864" y1="1792" y2="1792" x1="848" />
            <wire x2="1168" y1="1792" y2="1792" x1="864" />
        </branch>
        <bustap x2="832" y1="1728" y2="1728" x1="736" />
        <branch name="LES(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1728" type="branch" />
            <wire x2="848" y1="1728" y2="1728" x1="832" />
            <wire x2="864" y1="1728" y2="1728" x1="848" />
            <wire x2="1168" y1="1728" y2="1728" x1="864" />
        </branch>
    </sheet>
</drawing>