<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clkd(31:0)" />
        <signal name="clkd(18:17)" />
        <signal name="clk" />
        <signal name="RST" />
        <signal name="HEXS(15:0)" />
        <signal name="point(3:0)" />
        <signal name="LES(3:0)" />
        <signal name="AN(3:0)" />
        <signal name="p" />
        <signal name="XLXN_12" />
        <signal name="Segment(7:0)" />
        <signal name="Segment(7)" />
        <signal name="Segment(6)" />
        <signal name="Segment(5)" />
        <signal name="Segment(4)" />
        <signal name="Segment(3)" />
        <signal name="Segment(2)" />
        <signal name="Segment(1)" />
        <signal name="Segment(0)" />
        <signal name="HEX(3:0)" />
        <signal name="HEX(2)" />
        <signal name="HEX(1)" />
        <signal name="HEX(0)" />
        <signal name="HEX(3)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="RST" />
        <port polarity="Input" name="HEXS(15:0)" />
        <port polarity="Input" name="point(3:0)" />
        <port polarity="Input" name="LES(3:0)" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Output" name="Segment(7:0)" />
        <blockdef name="clkdiv">
            <timestamp>2017-11-9T5:43:32</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="MyMC14495">
            <timestamp>2017-11-2T5:38:32</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-400" y2="-400" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="DisplaySync">
            <timestamp>2017-11-9T6:1:40</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="clkdiv" name="XLXI_1">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="RST" name="rst" />
            <blockpin signalname="clkd(31:0)" name="clkdiv(31:0)" />
        </block>
        <block symbolname="MyMC14495" name="XLXI_2">
            <blockpin signalname="p" name="point" />
            <blockpin signalname="XLXN_12" name="LE" />
            <blockpin signalname="HEX(0)" name="D0" />
            <blockpin signalname="HEX(1)" name="D1" />
            <blockpin signalname="HEX(2)" name="D2" />
            <blockpin signalname="HEX(3)" name="D3" />
            <blockpin signalname="Segment(7)" name="p" />
            <blockpin signalname="Segment(6)" name="g" />
            <blockpin signalname="Segment(5)" name="f" />
            <blockpin signalname="Segment(4)" name="e" />
            <blockpin signalname="Segment(3)" name="d" />
            <blockpin signalname="Segment(2)" name="c" />
            <blockpin signalname="Segment(1)" name="b" />
            <blockpin signalname="Segment(0)" name="a" />
        </block>
        <block symbolname="DisplaySync" name="XLXI_3">
            <blockpin signalname="clkd(18:17)" name="Scan(1:0)" />
            <blockpin signalname="HEXS(15:0)" name="Hexs(15:0)" />
            <blockpin signalname="point(3:0)" name="point(3:0)" />
            <blockpin signalname="LES(3:0)" name="LES(3:0)" />
            <blockpin signalname="HEX(3:0)" name="HEX(3:0)" />
            <blockpin signalname="AN(3:0)" name="AN(3:0)" />
            <blockpin signalname="p" name="p" />
            <blockpin signalname="XLXN_12" name="LE" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="736" y="512" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1904" y="704" name="XLXI_2" orien="R0">
        </instance>
        <instance x="736" y="1200" name="XLXI_3" orien="R0">
        </instance>
        <branch name="clkd(31:0)">
            <wire x2="1264" y1="736" y2="736" x1="592" />
            <wire x2="592" y1="736" y2="976" x1="592" />
            <wire x2="592" y1="976" y2="1216" x1="592" />
            <wire x2="1264" y1="416" y2="416" x1="1120" />
            <wire x2="1264" y1="416" y2="736" x1="1264" />
        </branch>
        <bustap x2="688" y1="976" y2="976" x1="592" />
        <branch name="clkd(18:17)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="712" y="976" type="branch" />
            <wire x2="720" y1="976" y2="976" x1="688" />
            <wire x2="736" y1="976" y2="976" x1="720" />
        </branch>
        <branch name="clk">
            <wire x2="736" y1="416" y2="416" x1="704" />
        </branch>
        <iomarker fontsize="28" x="704" y="416" name="clk" orien="R180" />
        <branch name="RST">
            <wire x2="736" y1="480" y2="480" x1="704" />
        </branch>
        <iomarker fontsize="28" x="704" y="480" name="RST" orien="R180" />
        <branch name="HEXS(15:0)">
            <wire x2="736" y1="1040" y2="1040" x1="400" />
        </branch>
        <branch name="point(3:0)">
            <wire x2="736" y1="1104" y2="1104" x1="400" />
        </branch>
        <branch name="LES(3:0)">
            <wire x2="736" y1="1168" y2="1168" x1="400" />
        </branch>
        <iomarker fontsize="28" x="400" y="1040" name="HEXS(15:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="1104" name="point(3:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="1168" name="LES(3:0)" orien="R180" />
        <branch name="AN(3:0)">
            <wire x2="1824" y1="1040" y2="1040" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="1824" y="1040" name="AN(3:0)" orien="R0" />
        <branch name="p">
            <wire x2="1776" y1="1104" y2="1104" x1="1120" />
            <wire x2="1904" y1="224" y2="224" x1="1776" />
            <wire x2="1776" y1="224" y2="624" x1="1776" />
            <wire x2="1776" y1="624" y2="1104" x1="1776" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="1744" y1="1168" y2="1168" x1="1120" />
            <wire x2="1904" y1="304" y2="304" x1="1744" />
            <wire x2="1744" y1="304" y2="384" x1="1744" />
            <wire x2="1744" y1="384" y2="1168" x1="1744" />
        </branch>
        <branch name="Segment(7:0)">
            <wire x2="2464" y1="176" y2="224" x1="2464" />
            <wire x2="2464" y1="224" y2="288" x1="2464" />
            <wire x2="2464" y1="288" y2="352" x1="2464" />
            <wire x2="2464" y1="352" y2="416" x1="2464" />
            <wire x2="2464" y1="416" y2="480" x1="2464" />
            <wire x2="2464" y1="480" y2="544" x1="2464" />
            <wire x2="2464" y1="544" y2="608" x1="2464" />
            <wire x2="2464" y1="608" y2="672" x1="2464" />
            <wire x2="2464" y1="672" y2="736" x1="2464" />
            <wire x2="2672" y1="736" y2="736" x1="2464" />
        </branch>
        <iomarker fontsize="28" x="2672" y="736" name="Segment(7:0)" orien="R0" />
        <bustap x2="2368" y1="224" y2="224" x1="2464" />
        <branch name="Segment(7)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="224" type="branch" />
            <wire x2="2336" y1="224" y2="224" x1="2288" />
            <wire x2="2368" y1="224" y2="224" x1="2336" />
        </branch>
        <bustap x2="2368" y1="288" y2="288" x1="2464" />
        <branch name="Segment(6)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="288" type="branch" />
            <wire x2="2336" y1="288" y2="288" x1="2288" />
            <wire x2="2368" y1="288" y2="288" x1="2336" />
        </branch>
        <bustap x2="2368" y1="352" y2="352" x1="2464" />
        <branch name="Segment(5)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="352" type="branch" />
            <wire x2="2336" y1="352" y2="352" x1="2288" />
            <wire x2="2368" y1="352" y2="352" x1="2336" />
        </branch>
        <bustap x2="2368" y1="416" y2="416" x1="2464" />
        <branch name="Segment(4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="416" type="branch" />
            <wire x2="2336" y1="416" y2="416" x1="2288" />
            <wire x2="2368" y1="416" y2="416" x1="2336" />
        </branch>
        <bustap x2="2368" y1="480" y2="480" x1="2464" />
        <branch name="Segment(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="480" type="branch" />
            <wire x2="2336" y1="480" y2="480" x1="2288" />
            <wire x2="2368" y1="480" y2="480" x1="2336" />
        </branch>
        <bustap x2="2368" y1="544" y2="544" x1="2464" />
        <branch name="Segment(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="544" type="branch" />
            <wire x2="2336" y1="544" y2="544" x1="2288" />
            <wire x2="2368" y1="544" y2="544" x1="2336" />
        </branch>
        <bustap x2="2368" y1="608" y2="608" x1="2464" />
        <branch name="Segment(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="608" type="branch" />
            <wire x2="2336" y1="608" y2="608" x1="2288" />
            <wire x2="2368" y1="608" y2="608" x1="2336" />
        </branch>
        <bustap x2="2368" y1="672" y2="672" x1="2464" />
        <branch name="Segment(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="672" type="branch" />
            <wire x2="2336" y1="672" y2="672" x1="2288" />
            <wire x2="2368" y1="672" y2="672" x1="2336" />
        </branch>
        <branch name="HEX(3:0)">
            <wire x2="1536" y1="976" y2="976" x1="1120" />
            <wire x2="1536" y1="320" y2="384" x1="1536" />
            <wire x2="1536" y1="384" y2="464" x1="1536" />
            <wire x2="1536" y1="464" y2="544" x1="1536" />
            <wire x2="1536" y1="544" y2="624" x1="1536" />
            <wire x2="1536" y1="624" y2="976" x1="1536" />
        </branch>
        <bustap x2="1632" y1="544" y2="544" x1="1536" />
        <branch name="HEX(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1656" y="544" type="branch" />
            <wire x2="1664" y1="544" y2="544" x1="1632" />
            <wire x2="1680" y1="544" y2="544" x1="1664" />
            <wire x2="1904" y1="544" y2="544" x1="1680" />
        </branch>
        <bustap x2="1632" y1="464" y2="464" x1="1536" />
        <branch name="HEX(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1656" y="464" type="branch" />
            <wire x2="1664" y1="464" y2="464" x1="1632" />
            <wire x2="1680" y1="464" y2="464" x1="1664" />
            <wire x2="1904" y1="464" y2="464" x1="1680" />
        </branch>
        <bustap x2="1632" y1="384" y2="384" x1="1536" />
        <branch name="HEX(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1656" y="384" type="branch" />
            <wire x2="1664" y1="384" y2="384" x1="1632" />
            <wire x2="1680" y1="384" y2="384" x1="1664" />
            <wire x2="1904" y1="384" y2="384" x1="1680" />
        </branch>
        <bustap x2="1632" y1="624" y2="624" x1="1536" />
        <branch name="HEX(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1664" y="624" type="branch" />
            <wire x2="1664" y1="624" y2="624" x1="1632" />
            <wire x2="1696" y1="624" y2="624" x1="1664" />
            <wire x2="1904" y1="624" y2="624" x1="1696" />
        </branch>
    </sheet>
</drawing>