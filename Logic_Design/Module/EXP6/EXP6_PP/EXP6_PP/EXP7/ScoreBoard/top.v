`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:42:44 11/09/2017 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module top(
    input wire clk, 
	input wire [7:0] SW,
	input wire [3:0] btn,
	output wire [3:0] AN,
	output wire [7:0] SEGMENT
    //output wire Buzzer
);
wire [15:0] num;

	CreateNumber c0(btn,num);
	
	DispNum d0(clk, num, SW[7:4], SW[3:0], 1'b0, AN, SEGMENT);
	 
    //assign Buzzer = 1'b1;
endmodule
