<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="s(0)" />
        <signal name="s(1)" />
        <signal name="s(1:0)" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="o(3:0)" />
        <signal name="o(3)" />
        <signal name="o(2)" />
        <signal name="o(1)" />
        <signal name="o(0)" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="I0(3:0)" />
        <signal name="I1(3:0)" />
        <signal name="I2(3:0)" />
        <signal name="I3(3:0)" />
        <signal name="I0(2)" />
        <signal name="I0(0)" />
        <signal name="I0(3)" />
        <signal name="I0(1)" />
        <signal name="I1(3)" />
        <signal name="I1(2)" />
        <signal name="I1(1)" />
        <signal name="I1(0)" />
        <signal name="I2(3)" />
        <signal name="I2(2)" />
        <signal name="I2(1)" />
        <signal name="I2(0)" />
        <signal name="I3(3)" />
        <signal name="I3(2)" />
        <signal name="I3(1)" />
        <signal name="I3(0)" />
        <port polarity="Input" name="s(1:0)" />
        <port polarity="Output" name="o(3:0)" />
        <port polarity="Input" name="I0(3:0)" />
        <port polarity="Input" name="I1(3:0)" />
        <port polarity="Input" name="I2(3:0)" />
        <port polarity="Input" name="I3(3:0)" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="s(1)" name="I" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="s(0)" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_7" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="s(0)" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="XLXN_7" name="I0" />
            <blockpin signalname="s(1)" name="I1" />
            <blockpin signalname="XLXN_42" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="s(0)" name="I0" />
            <blockpin signalname="s(1)" name="I1" />
            <blockpin signalname="XLXN_41" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="I0(0)" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I1(0)" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="I2(0)" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="I3(0)" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="I0(1)" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="I1(1)" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="I2(1)" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="I3(1)" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_15">
            <blockpin signalname="I0(2)" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="I1(2)" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_17">
            <blockpin signalname="I2(2)" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_18">
            <blockpin signalname="I3(2)" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_19">
            <blockpin signalname="I0(3)" name="I0" />
            <blockpin signalname="XLXN_44" name="I1" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="I1(3)" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="I2(3)" name="I0" />
            <blockpin signalname="XLXN_42" name="I1" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_22">
            <blockpin signalname="I3(3)" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_23">
            <blockpin signalname="XLXN_18" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_16" name="I2" />
            <blockpin signalname="XLXN_15" name="I3" />
            <blockpin signalname="o(0)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_24">
            <blockpin signalname="XLXN_22" name="I0" />
            <blockpin signalname="XLXN_21" name="I1" />
            <blockpin signalname="XLXN_20" name="I2" />
            <blockpin signalname="XLXN_19" name="I3" />
            <blockpin signalname="o(1)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_25">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="XLXN_25" name="I1" />
            <blockpin signalname="XLXN_24" name="I2" />
            <blockpin signalname="XLXN_23" name="I3" />
            <blockpin signalname="o(2)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_26">
            <blockpin signalname="XLXN_30" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_28" name="I2" />
            <blockpin signalname="XLXN_27" name="I3" />
            <blockpin signalname="o(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="576" y="320" name="XLXI_2" orien="R0" />
        <instance x="992" y="272" name="XLXI_3" orien="R0" />
        <instance x="992" y="416" name="XLXI_4" orien="R0" />
        <instance x="992" y="704" name="XLXI_6" orien="R0" />
        <instance x="992" y="560" name="XLXI_5" orien="R0" />
        <instance x="576" y="176" name="XLXI_1" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="816" y1="288" y2="288" x1="800" />
            <wire x2="816" y1="288" y2="496" x1="816" />
            <wire x2="992" y1="496" y2="496" x1="816" />
            <wire x2="816" y1="208" y2="288" x1="816" />
            <wire x2="992" y1="208" y2="208" x1="816" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="896" y1="144" y2="144" x1="800" />
            <wire x2="896" y1="144" y2="288" x1="896" />
            <wire x2="992" y1="288" y2="288" x1="896" />
            <wire x2="992" y1="144" y2="144" x1="896" />
        </branch>
        <branch name="s(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="224" y="288" type="branch" />
            <wire x2="224" y1="288" y2="288" x1="192" />
            <wire x2="464" y1="288" y2="288" x1="224" />
            <wire x2="576" y1="288" y2="288" x1="464" />
            <wire x2="464" y1="288" y2="352" x1="464" />
            <wire x2="992" y1="352" y2="352" x1="464" />
            <wire x2="464" y1="352" y2="640" x1="464" />
            <wire x2="992" y1="640" y2="640" x1="464" />
        </branch>
        <branch name="s(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="224" y="144" type="branch" />
            <wire x2="224" y1="144" y2="144" x1="192" />
            <wire x2="544" y1="144" y2="144" x1="224" />
            <wire x2="576" y1="144" y2="144" x1="544" />
            <wire x2="544" y1="144" y2="432" x1="544" />
            <wire x2="992" y1="432" y2="432" x1="544" />
            <wire x2="544" y1="432" y2="576" x1="544" />
            <wire x2="992" y1="576" y2="576" x1="544" />
        </branch>
        <branch name="s(1:0)">
            <wire x2="96" y1="208" y2="208" x1="80" />
            <wire x2="96" y1="208" y2="288" x1="96" />
            <wire x2="96" y1="288" y2="320" x1="96" />
            <wire x2="96" y1="96" y2="144" x1="96" />
            <wire x2="96" y1="144" y2="208" x1="96" />
        </branch>
        <iomarker fontsize="28" x="80" y="208" name="s(1:0)" orien="R180" />
        <bustap x2="192" y1="144" y2="144" x1="96" />
        <bustap x2="192" y1="288" y2="288" x1="96" />
        <instance x="1872" y="272" name="XLXI_7" orien="R0" />
        <instance x="1872" y="416" name="XLXI_8" orien="R0" />
        <instance x="1872" y="560" name="XLXI_9" orien="R0" />
        <instance x="1872" y="704" name="XLXI_10" orien="R0" />
        <instance x="1872" y="944" name="XLXI_11" orien="R0" />
        <instance x="1872" y="1088" name="XLXI_12" orien="R0" />
        <instance x="1872" y="1232" name="XLXI_13" orien="R0" />
        <instance x="1872" y="1376" name="XLXI_14" orien="R0" />
        <instance x="1872" y="1664" name="XLXI_15" orien="R0" />
        <instance x="1872" y="1808" name="XLXI_16" orien="R0" />
        <instance x="1872" y="1952" name="XLXI_17" orien="R0" />
        <instance x="1872" y="2112" name="XLXI_18" orien="R0" />
        <instance x="1872" y="2336" name="XLXI_19" orien="R0" />
        <instance x="1872" y="2624" name="XLXI_21" orien="R0" />
        <instance x="1872" y="2480" name="XLXI_20" orien="R0" />
        <instance x="1872" y="2768" name="XLXI_22" orien="R0" />
        <instance x="2288" y="544" name="XLXI_23" orien="R0" />
        <instance x="2288" y="1216" name="XLXI_24" orien="R0" />
        <instance x="2288" y="1936" name="XLXI_25" orien="R0" />
        <instance x="2288" y="2608" name="XLXI_26" orien="R0" />
        <branch name="XLXN_15">
            <wire x2="2288" y1="176" y2="176" x1="2128" />
            <wire x2="2288" y1="176" y2="288" x1="2288" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="2208" y1="320" y2="320" x1="2128" />
            <wire x2="2208" y1="320" y2="352" x1="2208" />
            <wire x2="2288" y1="352" y2="352" x1="2208" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="2208" y1="464" y2="464" x1="2128" />
            <wire x2="2208" y1="416" y2="464" x1="2208" />
            <wire x2="2288" y1="416" y2="416" x1="2208" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="2288" y1="608" y2="608" x1="2128" />
            <wire x2="2288" y1="480" y2="608" x1="2288" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="2288" y1="848" y2="848" x1="2128" />
            <wire x2="2288" y1="848" y2="960" x1="2288" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="2208" y1="992" y2="992" x1="2128" />
            <wire x2="2208" y1="992" y2="1024" x1="2208" />
            <wire x2="2288" y1="1024" y2="1024" x1="2208" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="2208" y1="1136" y2="1136" x1="2128" />
            <wire x2="2208" y1="1088" y2="1136" x1="2208" />
            <wire x2="2288" y1="1088" y2="1088" x1="2208" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="2288" y1="1280" y2="1280" x1="2128" />
            <wire x2="2288" y1="1152" y2="1280" x1="2288" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="2288" y1="1568" y2="1568" x1="2128" />
            <wire x2="2288" y1="1568" y2="1680" x1="2288" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="2208" y1="1712" y2="1712" x1="2128" />
            <wire x2="2208" y1="1712" y2="1744" x1="2208" />
            <wire x2="2288" y1="1744" y2="1744" x1="2208" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="2208" y1="1856" y2="1856" x1="2128" />
            <wire x2="2208" y1="1808" y2="1856" x1="2208" />
            <wire x2="2288" y1="1808" y2="1808" x1="2208" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="2288" y1="2016" y2="2016" x1="2128" />
            <wire x2="2288" y1="1872" y2="2016" x1="2288" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="2288" y1="2240" y2="2240" x1="2128" />
            <wire x2="2288" y1="2240" y2="2352" x1="2288" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="2208" y1="2384" y2="2384" x1="2128" />
            <wire x2="2208" y1="2384" y2="2416" x1="2208" />
            <wire x2="2288" y1="2416" y2="2416" x1="2208" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="2208" y1="2528" y2="2528" x1="2128" />
            <wire x2="2208" y1="2480" y2="2528" x1="2208" />
            <wire x2="2288" y1="2480" y2="2480" x1="2208" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="2288" y1="2672" y2="2672" x1="2128" />
            <wire x2="2288" y1="2544" y2="2672" x1="2288" />
        </branch>
        <branch name="o(3:0)">
            <wire x2="2896" y1="320" y2="384" x1="2896" />
            <wire x2="2896" y1="384" y2="1056" x1="2896" />
            <wire x2="2896" y1="1056" y2="1392" x1="2896" />
            <wire x2="3104" y1="1392" y2="1392" x1="2896" />
            <wire x2="2896" y1="1392" y2="1776" x1="2896" />
            <wire x2="2896" y1="1776" y2="2448" x1="2896" />
            <wire x2="2896" y1="2448" y2="2480" x1="2896" />
        </branch>
        <iomarker fontsize="28" x="3104" y="1392" name="o(3:0)" orien="R0" />
        <bustap x2="2800" y1="2448" y2="2448" x1="2896" />
        <branch name="o(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2672" y="2448" type="branch" />
            <wire x2="2672" y1="2448" y2="2448" x1="2544" />
            <wire x2="2800" y1="2448" y2="2448" x1="2672" />
        </branch>
        <bustap x2="2800" y1="1776" y2="1776" x1="2896" />
        <branch name="o(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2672" y="1776" type="branch" />
            <wire x2="2672" y1="1776" y2="1776" x1="2544" />
            <wire x2="2800" y1="1776" y2="1776" x1="2672" />
        </branch>
        <bustap x2="2800" y1="1056" y2="1056" x1="2896" />
        <branch name="o(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2672" y="1056" type="branch" />
            <wire x2="2672" y1="1056" y2="1056" x1="2544" />
            <wire x2="2800" y1="1056" y2="1056" x1="2672" />
        </branch>
        <bustap x2="2800" y1="384" y2="384" x1="2896" />
        <branch name="o(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2672" y="384" type="branch" />
            <wire x2="2672" y1="384" y2="384" x1="2544" />
            <wire x2="2800" y1="384" y2="384" x1="2672" />
        </branch>
        <branch name="XLXN_41">
            <wire x2="1280" y1="608" y2="608" x1="1248" />
            <wire x2="1552" y1="608" y2="608" x1="1280" />
            <wire x2="1280" y1="608" y2="1248" x1="1280" />
            <wire x2="1280" y1="1248" y2="1984" x1="1280" />
            <wire x2="1280" y1="1984" y2="2640" x1="1280" />
            <wire x2="1872" y1="2640" y2="2640" x1="1280" />
            <wire x2="1872" y1="1984" y2="1984" x1="1280" />
            <wire x2="1872" y1="1248" y2="1248" x1="1280" />
            <wire x2="1552" y1="576" y2="608" x1="1552" />
            <wire x2="1872" y1="576" y2="576" x1="1552" />
        </branch>
        <branch name="XLXN_42">
            <wire x2="1328" y1="464" y2="464" x1="1248" />
            <wire x2="1552" y1="464" y2="464" x1="1328" />
            <wire x2="1328" y1="464" y2="1104" x1="1328" />
            <wire x2="1328" y1="1104" y2="1824" x1="1328" />
            <wire x2="1328" y1="1824" y2="2496" x1="1328" />
            <wire x2="1872" y1="2496" y2="2496" x1="1328" />
            <wire x2="1872" y1="1824" y2="1824" x1="1328" />
            <wire x2="1872" y1="1104" y2="1104" x1="1328" />
            <wire x2="1552" y1="432" y2="464" x1="1552" />
            <wire x2="1872" y1="432" y2="432" x1="1552" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="1392" y1="320" y2="320" x1="1248" />
            <wire x2="1552" y1="320" y2="320" x1="1392" />
            <wire x2="1392" y1="320" y2="960" x1="1392" />
            <wire x2="1392" y1="960" y2="1680" x1="1392" />
            <wire x2="1392" y1="1680" y2="2352" x1="1392" />
            <wire x2="1872" y1="2352" y2="2352" x1="1392" />
            <wire x2="1872" y1="1680" y2="1680" x1="1392" />
            <wire x2="1872" y1="960" y2="960" x1="1392" />
            <wire x2="1552" y1="288" y2="320" x1="1552" />
            <wire x2="1872" y1="288" y2="288" x1="1552" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="1456" y1="176" y2="176" x1="1248" />
            <wire x2="1552" y1="176" y2="176" x1="1456" />
            <wire x2="1456" y1="176" y2="816" x1="1456" />
            <wire x2="1456" y1="816" y2="1536" x1="1456" />
            <wire x2="1456" y1="1536" y2="2208" x1="1456" />
            <wire x2="1872" y1="2208" y2="2208" x1="1456" />
            <wire x2="1872" y1="1536" y2="1536" x1="1456" />
            <wire x2="1872" y1="816" y2="816" x1="1456" />
            <wire x2="1552" y1="144" y2="176" x1="1552" />
            <wire x2="1872" y1="144" y2="144" x1="1552" />
        </branch>
        <branch name="I1(3:0)">
            <wire x2="1568" y1="1744" y2="1744" x1="496" />
            <wire x2="1728" y1="1744" y2="1744" x1="1568" />
            <wire x2="1728" y1="1744" y2="1760" x1="1728" />
            <wire x2="1568" y1="1744" y2="2432" x1="1568" />
            <wire x2="1728" y1="2432" y2="2432" x1="1568" />
            <wire x2="1728" y1="2432" y2="2448" x1="1728" />
            <wire x2="1568" y1="320" y2="352" x1="1568" />
            <wire x2="1568" y1="352" y2="1024" x1="1568" />
            <wire x2="1568" y1="1024" y2="1744" x1="1568" />
            <wire x2="1744" y1="1024" y2="1024" x1="1568" />
            <wire x2="1744" y1="1024" y2="1040" x1="1744" />
            <wire x2="1728" y1="1728" y2="1744" x1="1728" />
            <wire x2="1728" y1="2400" y2="2416" x1="1728" />
            <wire x2="1728" y1="2416" y2="2432" x1="1728" />
            <wire x2="1744" y1="1008" y2="1024" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="496" y="1600" name="I0(3:0)" orien="R180" />
        <iomarker fontsize="28" x="496" y="1744" name="I1(3:0)" orien="R180" />
        <iomarker fontsize="28" x="496" y="1888" name="I2(3:0)" orien="R180" />
        <iomarker fontsize="28" x="496" y="2048" name="I3(3:0)" orien="R180" />
        <branch name="I0(3:0)">
            <wire x2="1520" y1="1600" y2="1600" x1="496" />
            <wire x2="1728" y1="1600" y2="1600" x1="1520" />
            <wire x2="1728" y1="1600" y2="1616" x1="1728" />
            <wire x2="1520" y1="1600" y2="2288" x1="1520" />
            <wire x2="1728" y1="2288" y2="2288" x1="1520" />
            <wire x2="1728" y1="2288" y2="2304" x1="1728" />
            <wire x2="1648" y1="208" y2="208" x1="1520" />
            <wire x2="1648" y1="208" y2="224" x1="1648" />
            <wire x2="1520" y1="208" y2="880" x1="1520" />
            <wire x2="1520" y1="880" y2="1600" x1="1520" />
            <wire x2="1744" y1="880" y2="880" x1="1520" />
            <wire x2="1744" y1="880" y2="896" x1="1744" />
            <wire x2="1648" y1="192" y2="208" x1="1648" />
            <wire x2="1728" y1="1584" y2="1600" x1="1728" />
            <wire x2="1728" y1="2256" y2="2272" x1="1728" />
            <wire x2="1728" y1="2272" y2="2288" x1="1728" />
            <wire x2="1744" y1="864" y2="880" x1="1744" />
        </branch>
        <branch name="I3(3:0)">
            <wire x2="1664" y1="2048" y2="2048" x1="496" />
            <wire x2="1728" y1="2048" y2="2048" x1="1664" />
            <wire x2="1728" y1="2048" y2="2064" x1="1728" />
            <wire x2="1664" y1="2048" y2="2688" x1="1664" />
            <wire x2="1664" y1="2688" y2="2704" x1="1664" />
            <wire x2="1664" y1="608" y2="640" x1="1664" />
            <wire x2="1664" y1="640" y2="1312" x1="1664" />
            <wire x2="1664" y1="1312" y2="2048" x1="1664" />
            <wire x2="1728" y1="2032" y2="2048" x1="1728" />
        </branch>
        <bustap x2="1824" y1="1600" y2="1600" x1="1728" />
        <branch name="I0(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="1600" type="branch" />
            <wire x2="1856" y1="1600" y2="1600" x1="1824" />
            <wire x2="1872" y1="1600" y2="1600" x1="1856" />
        </branch>
        <bustap x2="1744" y1="208" y2="208" x1="1648" />
        <branch name="I0(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1808" y="208" type="branch" />
            <wire x2="1808" y1="208" y2="208" x1="1744" />
            <wire x2="1872" y1="208" y2="208" x1="1808" />
        </branch>
        <branch name="I2(3:0)">
            <wire x2="1616" y1="1888" y2="1888" x1="496" />
            <wire x2="1728" y1="1888" y2="1888" x1="1616" />
            <wire x2="1728" y1="1888" y2="1904" x1="1728" />
            <wire x2="1616" y1="1888" y2="2576" x1="1616" />
            <wire x2="1728" y1="2576" y2="2576" x1="1616" />
            <wire x2="1728" y1="2576" y2="2592" x1="1728" />
            <wire x2="1616" y1="464" y2="496" x1="1616" />
            <wire x2="1616" y1="496" y2="1168" x1="1616" />
            <wire x2="1616" y1="1168" y2="1888" x1="1616" />
            <wire x2="1744" y1="1168" y2="1168" x1="1616" />
            <wire x2="1744" y1="1168" y2="1184" x1="1744" />
            <wire x2="1728" y1="1872" y2="1888" x1="1728" />
            <wire x2="1728" y1="2544" y2="2560" x1="1728" />
            <wire x2="1728" y1="2560" y2="2576" x1="1728" />
            <wire x2="1744" y1="1152" y2="1168" x1="1744" />
        </branch>
        <bustap x2="1824" y1="2272" y2="2272" x1="1728" />
        <branch name="I0(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="2272" type="branch" />
            <wire x2="1856" y1="2272" y2="2272" x1="1824" />
            <wire x2="1872" y1="2272" y2="2272" x1="1856" />
        </branch>
        <bustap x2="1840" y1="880" y2="880" x1="1744" />
        <branch name="I0(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="880" type="branch" />
            <wire x2="1856" y1="880" y2="880" x1="1840" />
            <wire x2="1872" y1="880" y2="880" x1="1856" />
        </branch>
        <bustap x2="1824" y1="2416" y2="2416" x1="1728" />
        <branch name="I1(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="2416" type="branch" />
            <wire x2="1856" y1="2416" y2="2416" x1="1824" />
            <wire x2="1872" y1="2416" y2="2416" x1="1856" />
        </branch>
        <bustap x2="1824" y1="1744" y2="1744" x1="1728" />
        <branch name="I1(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="1744" type="branch" />
            <wire x2="1856" y1="1744" y2="1744" x1="1824" />
            <wire x2="1872" y1="1744" y2="1744" x1="1856" />
        </branch>
        <bustap x2="1840" y1="1024" y2="1024" x1="1744" />
        <branch name="I1(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="1024" type="branch" />
            <wire x2="1856" y1="1024" y2="1024" x1="1840" />
            <wire x2="1872" y1="1024" y2="1024" x1="1856" />
        </branch>
        <bustap x2="1664" y1="352" y2="352" x1="1568" />
        <branch name="I1(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1768" y="352" type="branch" />
            <wire x2="1776" y1="352" y2="352" x1="1664" />
            <wire x2="1872" y1="352" y2="352" x1="1776" />
        </branch>
        <bustap x2="1824" y1="2560" y2="2560" x1="1728" />
        <branch name="I2(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="2560" type="branch" />
            <wire x2="1856" y1="2560" y2="2560" x1="1824" />
            <wire x2="1872" y1="2560" y2="2560" x1="1856" />
        </branch>
        <bustap x2="1824" y1="1888" y2="1888" x1="1728" />
        <branch name="I2(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="1888" type="branch" />
            <wire x2="1856" y1="1888" y2="1888" x1="1824" />
            <wire x2="1872" y1="1888" y2="1888" x1="1856" />
        </branch>
        <bustap x2="1840" y1="1168" y2="1168" x1="1744" />
        <branch name="I2(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="1168" type="branch" />
            <wire x2="1856" y1="1168" y2="1168" x1="1840" />
            <wire x2="1872" y1="1168" y2="1168" x1="1856" />
        </branch>
        <bustap x2="1712" y1="496" y2="496" x1="1616" />
        <branch name="I2(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1792" y="496" type="branch" />
            <wire x2="1792" y1="496" y2="496" x1="1712" />
            <wire x2="1872" y1="496" y2="496" x1="1792" />
        </branch>
        <bustap x2="1760" y1="2688" y2="2688" x1="1664" />
        <branch name="I3(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1768" y="2688" type="branch" />
            <wire x2="1776" y1="2688" y2="2688" x1="1760" />
            <wire x2="1824" y1="2688" y2="2688" x1="1776" />
            <wire x2="1824" y1="2688" y2="2704" x1="1824" />
            <wire x2="1872" y1="2704" y2="2704" x1="1824" />
        </branch>
        <bustap x2="1824" y1="2048" y2="2048" x1="1728" />
        <branch name="I3(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1848" y="2048" type="branch" />
            <wire x2="1856" y1="2048" y2="2048" x1="1824" />
            <wire x2="1872" y1="2048" y2="2048" x1="1856" />
        </branch>
        <bustap x2="1760" y1="1312" y2="1312" x1="1664" />
        <branch name="I3(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1816" y="1312" type="branch" />
            <wire x2="1824" y1="1312" y2="1312" x1="1760" />
            <wire x2="1872" y1="1312" y2="1312" x1="1824" />
        </branch>
        <bustap x2="1760" y1="640" y2="640" x1="1664" />
        <branch name="I3(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1816" y="640" type="branch" />
            <wire x2="1824" y1="640" y2="640" x1="1760" />
            <wire x2="1872" y1="640" y2="640" x1="1824" />
        </branch>
    </sheet>
</drawing>