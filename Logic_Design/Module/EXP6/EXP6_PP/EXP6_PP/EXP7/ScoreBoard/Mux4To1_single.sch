<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_10" />
        <signal name="XLXN_12" />
        <signal name="S(0)" />
        <signal name="S(1)" />
        <signal name="S(1:0)" />
        <signal name="I0" />
        <signal name="I1" />
        <signal name="I2" />
        <signal name="I3" />
        <signal name="o" />
        <port polarity="Input" name="S(1:0)" />
        <port polarity="Input" name="I0" />
        <port polarity="Input" name="I1" />
        <port polarity="Input" name="I2" />
        <port polarity="Input" name="I3" />
        <port polarity="Output" name="o" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="XLXN_12" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="S(0)" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="S(1)" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="S(1)" name="I0" />
            <blockpin signalname="S(0)" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="I0" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="XLXN_1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="I1" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="XLXN_2" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="I2" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I3" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_9">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_2" name="I2" />
            <blockpin signalname="XLXN_1" name="I3" />
            <blockpin signalname="o" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_10">
            <blockpin signalname="S(0)" name="I" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_11">
            <blockpin signalname="S(1)" name="I" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1008" y="432" name="XLXI_1" orien="R0" />
        <instance x="1008" y="640" name="XLXI_2" orien="R0" />
        <instance x="1008" y="864" name="XLXI_3" orien="R0" />
        <instance x="1008" y="1088" name="XLXI_4" orien="R0" />
        <instance x="1488" y="464" name="XLXI_5" orien="R0" />
        <instance x="1488" y="672" name="XLXI_6" orien="R0" />
        <instance x="1488" y="896" name="XLXI_7" orien="R0" />
        <instance x="1488" y="1120" name="XLXI_8" orien="R0" />
        <instance x="2032" y="848" name="XLXI_9" orien="R0" />
        <instance x="528" y="400" name="XLXI_10" orien="R0" />
        <instance x="528" y="304" name="XLXI_11" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="2032" y1="368" y2="368" x1="1744" />
            <wire x2="2032" y1="368" y2="592" x1="2032" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="1888" y1="576" y2="576" x1="1744" />
            <wire x2="1888" y1="576" y2="656" x1="1888" />
            <wire x2="2032" y1="656" y2="656" x1="1888" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1888" y1="800" y2="800" x1="1744" />
            <wire x2="1888" y1="720" y2="800" x1="1888" />
            <wire x2="2032" y1="720" y2="720" x1="1888" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="2032" y1="1024" y2="1024" x1="1744" />
            <wire x2="2032" y1="784" y2="1024" x1="2032" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1488" y1="336" y2="336" x1="1264" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1488" y1="544" y2="544" x1="1264" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1488" y1="768" y2="768" x1="1264" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1488" y1="992" y2="992" x1="1264" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="928" y1="272" y2="272" x1="752" />
            <wire x2="928" y1="272" y2="304" x1="928" />
            <wire x2="928" y1="304" y2="512" x1="928" />
            <wire x2="1008" y1="512" y2="512" x1="928" />
            <wire x2="1008" y1="304" y2="304" x1="928" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="848" y1="368" y2="368" x1="752" />
            <wire x2="1008" y1="368" y2="368" x1="848" />
            <wire x2="848" y1="368" y2="736" x1="848" />
            <wire x2="1008" y1="736" y2="736" x1="848" />
        </branch>
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="272" y="368" type="branch" />
            <wire x2="272" y1="368" y2="368" x1="224" />
            <wire x2="320" y1="368" y2="368" x1="272" />
            <wire x2="496" y1="368" y2="368" x1="320" />
            <wire x2="528" y1="368" y2="368" x1="496" />
            <wire x2="496" y1="368" y2="576" x1="496" />
            <wire x2="1008" y1="576" y2="576" x1="496" />
            <wire x2="496" y1="576" y2="960" x1="496" />
            <wire x2="1008" y1="960" y2="960" x1="496" />
        </branch>
        <branch name="S(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="272" y="272" type="branch" />
            <wire x2="272" y1="272" y2="272" x1="224" />
            <wire x2="320" y1="272" y2="272" x1="272" />
            <wire x2="400" y1="272" y2="272" x1="320" />
            <wire x2="528" y1="272" y2="272" x1="400" />
            <wire x2="400" y1="272" y2="800" x1="400" />
            <wire x2="400" y1="800" y2="1024" x1="400" />
            <wire x2="1008" y1="1024" y2="1024" x1="400" />
            <wire x2="1008" y1="800" y2="800" x1="400" />
        </branch>
        <branch name="S(1:0)">
            <wire x2="128" y1="320" y2="320" x1="96" />
            <wire x2="128" y1="320" y2="368" x1="128" />
            <wire x2="128" y1="368" y2="400" x1="128" />
            <wire x2="128" y1="240" y2="272" x1="128" />
            <wire x2="128" y1="272" y2="320" x1="128" />
        </branch>
        <branch name="I0">
            <wire x2="1280" y1="1216" y2="1216" x1="192" />
            <wire x2="1488" y1="400" y2="400" x1="1280" />
            <wire x2="1280" y1="400" y2="1216" x1="1280" />
        </branch>
        <branch name="I1">
            <wire x2="1312" y1="1264" y2="1264" x1="192" />
            <wire x2="1488" y1="608" y2="608" x1="1312" />
            <wire x2="1312" y1="608" y2="1264" x1="1312" />
        </branch>
        <branch name="I2">
            <wire x2="1360" y1="1328" y2="1328" x1="192" />
            <wire x2="1488" y1="832" y2="832" x1="1360" />
            <wire x2="1360" y1="832" y2="1328" x1="1360" />
        </branch>
        <branch name="I3">
            <wire x2="1392" y1="1376" y2="1376" x1="192" />
            <wire x2="1488" y1="1056" y2="1056" x1="1392" />
            <wire x2="1392" y1="1056" y2="1376" x1="1392" />
        </branch>
        <iomarker fontsize="28" x="192" y="1216" name="I0" orien="R180" />
        <iomarker fontsize="28" x="192" y="1264" name="I1" orien="R180" />
        <iomarker fontsize="28" x="192" y="1328" name="I2" orien="R180" />
        <iomarker fontsize="28" x="192" y="1376" name="I3" orien="R180" />
        <branch name="o">
            <wire x2="2320" y1="688" y2="688" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="688" name="o" orien="R0" />
        <iomarker fontsize="28" x="96" y="320" name="S(1:0)" orien="R180" />
        <bustap x2="224" y1="272" y2="272" x1="128" />
        <bustap x2="224" y1="368" y2="368" x1="128" />
    </sheet>
</drawing>