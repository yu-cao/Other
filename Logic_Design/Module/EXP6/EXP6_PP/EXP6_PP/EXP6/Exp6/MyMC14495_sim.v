// Verilog test fixture created from schematic E:\EXP6\Exp6\MC14495.sch - Wed Nov 01 15:35:29 2017

`timescale 1ns / 1ps

module MC14495_MC14495_sch_tb();

// Inputs
   reg LE;
   reg point;
   reg D3;
   reg D2;
   reg D1;
   reg D0;

// Output
   wire p;
   wire g;
   wire f;
   wire a;
   wire b;
   wire e;
   wire d;
   wire c;

// Bidirs

// Instantiate the UUT
   MC14495 UUT (
		.LE(LE), 
		.p(p), 
		.point(point), 
		.g(g), 
		.f(f), 
		.a(a), 
		.b(b), 
		.e(e), 
		.d(d), 
		.c(c), 
		.D3(D3), 
		.D2(D2), 
		.D1(D1), 
		.D0(D0)
   );
// Initialize Inputs
integer i;
initial begin
	D3 = 0;
	D2 = 0;
	D1 = 0;
	D0 = 0;
	LE = 0;
	point = 0;
	for (i=0; i<=15;i=i+1) begin
		#50;
		{D3,D2,D1,D0}=i;
		point = i;
	end
		
	#50;
	LE = 1;
end

endmodule
