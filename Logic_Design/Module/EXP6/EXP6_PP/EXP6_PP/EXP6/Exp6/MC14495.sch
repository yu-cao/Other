<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="LE" />
        <signal name="p" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_36" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_52" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54" />
        <signal name="XLXN_55" />
        <signal name="XLXN_56" />
        <signal name="XLXN_60" />
        <signal name="point" />
        <signal name="g" />
        <signal name="f" />
        <signal name="a" />
        <signal name="b" />
        <signal name="e" />
        <signal name="d" />
        <signal name="c" />
        <signal name="XLXN_63" />
        <signal name="XLXN_65" />
        <signal name="XLXN_66" />
        <signal name="XLXN_67" />
        <signal name="XLXN_68" />
        <signal name="XLXN_69" />
        <signal name="D3" />
        <signal name="D2" />
        <signal name="XLXN_72" />
        <signal name="XLXN_73" />
        <signal name="XLXN_74" />
        <signal name="XLXN_75" />
        <signal name="XLXN_77" />
        <signal name="D1" />
        <signal name="XLXN_82" />
        <signal name="XLXN_83" />
        <signal name="XLXN_84" />
        <signal name="XLXN_86" />
        <signal name="D0" />
        <signal name="XLXN_88" />
        <port polarity="Input" name="LE" />
        <port polarity="Output" name="p" />
        <port polarity="Input" name="point" />
        <port polarity="Output" name="g" />
        <port polarity="Output" name="f" />
        <port polarity="Output" name="a" />
        <port polarity="Output" name="b" />
        <port polarity="Output" name="e" />
        <port polarity="Output" name="d" />
        <port polarity="Output" name="c" />
        <port polarity="Input" name="D3" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D1" />
        <port polarity="Input" name="D0" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="point" name="I" />
            <blockpin signalname="p" name="O" />
        </block>
        <block symbolname="and4" name="AD0">
            <blockpin signalname="XLXN_69" name="I0" />
            <blockpin signalname="XLXN_67" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="and4" name="AD1">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_75" name="I3" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and4" name="AD9">
            <blockpin signalname="XLXN_69" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_65" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_44" name="O" />
        </block>
        <block symbolname="and4" name="AD12">
            <blockpin signalname="XLXN_69" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_65" name="I2" />
            <blockpin signalname="XLXN_75" name="I3" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="and4" name="AD16">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_67" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_75" name="I3" />
            <blockpin signalname="XLXN_53" name="O" />
        </block>
        <block symbolname="and4" name="AD17">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_65" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_54" name="O" />
        </block>
        <block symbolname="and4" name="AD18">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_67" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="and4" name="AD19">
            <blockpin signalname="XLXN_69" name="I0" />
            <blockpin signalname="XLXN_67" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_75" name="I3" />
            <blockpin signalname="XLXN_56" name="O" />
        </block>
        <block symbolname="and4" name="AD20">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_65" name="I1" />
            <blockpin signalname="XLXN_67" name="I2" />
            <blockpin signalname="XLXN_75" name="I3" />
            <blockpin signalname="XLXN_60" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_25">
            <blockpin signalname="XLXN_15" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="XLXN_17" name="I2" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_27">
            <blockpin signalname="XLXN_43" name="I0" />
            <blockpin signalname="XLXN_22" name="I1" />
            <blockpin signalname="XLXN_23" name="I2" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_30">
            <blockpin signalname="XLXN_46" name="I0" />
            <blockpin signalname="XLXN_47" name="I1" />
            <blockpin signalname="XLXN_48" name="I2" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_33">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_6" name="I1" />
            <blockpin signalname="g" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_34">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="f" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_35">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="e" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_36">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="d" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_37">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_11" name="I1" />
            <blockpin signalname="c" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_38">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="b" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_39">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_14" name="I1" />
            <blockpin signalname="a" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_40">
            <blockpin signalname="XLXN_39" name="I0" />
            <blockpin signalname="XLXN_40" name="I1" />
            <blockpin signalname="XLXN_41" name="I2" />
            <blockpin signalname="XLXN_55" name="I3" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_41">
            <blockpin signalname="XLXN_44" name="I0" />
            <blockpin signalname="XLXN_45" name="I1" />
            <blockpin signalname="XLXN_56" name="I2" />
            <blockpin signalname="XLXN_60" name="I3" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_42">
            <blockpin signalname="XLXN_52" name="I0" />
            <blockpin signalname="XLXN_48" name="I1" />
            <blockpin signalname="XLXN_49" name="I2" />
            <blockpin signalname="XLXN_53" name="I3" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_43">
            <blockpin signalname="XLXN_54" name="I0" />
            <blockpin signalname="XLXN_55" name="I1" />
            <blockpin signalname="XLXN_56" name="I2" />
            <blockpin signalname="XLXN_60" name="I3" />
            <blockpin signalname="XLXN_14" name="O" />
        </block>
        <block symbolname="and3" name="AD2">
            <blockpin signalname="XLXN_67" name="I0" />
            <blockpin signalname="XLXN_65" name="I1" />
            <blockpin signalname="XLXN_75" name="I2" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and3" name="AD3">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="D0" name="I1" />
            <blockpin signalname="XLXN_75" name="I2" />
            <blockpin signalname="XLXN_39" name="O" />
        </block>
        <block symbolname="and3" name="AD4">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="XLXN_65" name="I1" />
            <blockpin signalname="XLXN_75" name="I2" />
            <blockpin signalname="XLXN_40" name="O" />
        </block>
        <block symbolname="and3" name="AD5">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_65" name="I1" />
            <blockpin signalname="XLXN_75" name="I2" />
            <blockpin signalname="XLXN_41" name="O" />
        </block>
        <block symbolname="and3" name="AD6">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_67" name="I1" />
            <blockpin signalname="XLXN_65" name="I2" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and3" name="AD7">
            <blockpin signalname="XLXN_67" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="XLXN_75" name="I2" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
        <block symbolname="and2" name="AD8">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_75" name="I1" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="and3" name="AD10">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_45" name="O" />
        </block>
        <block symbolname="and3" name="AD11">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="and3" name="AD13">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_52" name="O" />
        </block>
        <block symbolname="and3" name="AD14">
            <blockpin signalname="XLXN_69" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_48" name="O" />
        </block>
        <block symbolname="and3" name="AD15">
            <blockpin signalname="XLXN_69" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_49" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_44">
            <blockpin signalname="D3" name="I" />
            <blockpin signalname="XLXN_75" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_45">
            <blockpin signalname="D2" name="I" />
            <blockpin signalname="XLXN_65" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_46">
            <blockpin signalname="D1" name="I" />
            <blockpin signalname="XLXN_67" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_47">
            <blockpin signalname="D0" name="I" />
            <blockpin signalname="XLXN_69" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7609" height="5382">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <branch name="LE">
            <wire x2="192" y1="1136" y2="2240" x1="192" />
            <wire x2="752" y1="2240" y2="2240" x1="192" />
            <wire x2="752" y1="2240" y2="2352" x1="752" />
            <wire x2="1552" y1="2240" y2="2240" x1="752" />
            <wire x2="1552" y1="2240" y2="2368" x1="1552" />
            <wire x2="2320" y1="2240" y2="2240" x1="1552" />
            <wire x2="2320" y1="2240" y2="2384" x1="2320" />
            <wire x2="3072" y1="2240" y2="2240" x1="2320" />
            <wire x2="3072" y1="2240" y2="2384" x1="3072" />
            <wire x2="3808" y1="2240" y2="2240" x1="3072" />
            <wire x2="3808" y1="2240" y2="2384" x1="3808" />
            <wire x2="4656" y1="2240" y2="2240" x1="3808" />
            <wire x2="5312" y1="2240" y2="2240" x1="4656" />
            <wire x2="5312" y1="2240" y2="2384" x1="5312" />
            <wire x2="4656" y1="2240" y2="2384" x1="4656" />
        </branch>
        <iomarker fontsize="28" x="192" y="1136" name="LE" orien="R270" />
        <instance x="224" y="1168" name="XLXI_1" orien="R90" />
        <branch name="p">
            <wire x2="256" y1="1392" y2="2384" x1="256" />
        </branch>
        <iomarker fontsize="28" x="256" y="2384" name="p" orien="R90" />
        <instance x="416" y="1584" name="AD0" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="656" y="1584" name="AD1" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="2736" y="1584" name="AD9" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="3472" y="1584" name="AD12" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="4464" y="1584" name="AD16" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="4704" y="1584" name="AD17" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="4992" y="1584" name="AD18" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="5216" y="1584" name="AD19" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="5440" y="1584" name="AD20" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="688" y="1936" name="XLXI_25" orien="R90" />
        <instance x="2256" y="1936" name="XLXI_27" orien="R90" />
        <instance x="688" y="2352" name="XLXI_33" orien="R90" />
        <instance x="1488" y="2368" name="XLXI_34" orien="R90" />
        <instance x="3744" y="1936" name="XLXI_30" orien="R90" />
        <instance x="2256" y="2384" name="XLXI_35" orien="R90" />
        <instance x="3744" y="2384" name="XLXI_37" orien="R90" />
        <instance x="4592" y="2384" name="XLXI_38" orien="R90" />
        <instance x="5248" y="2384" name="XLXI_39" orien="R90" />
        <branch name="XLXN_6">
            <wire x2="816" y1="2192" y2="2352" x1="816" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="1616" y1="2224" y2="2368" x1="1616" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="2384" y1="2192" y2="2384" x1="2384" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="3136" y1="2192" y2="2384" x1="3136" />
        </branch>
        <instance x="3008" y="2384" name="XLXI_36" orien="R90" />
        <branch name="XLXN_11">
            <wire x2="3872" y1="2192" y2="2384" x1="3872" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="4720" y1="2224" y2="2384" x1="4720" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="5376" y1="2240" y2="2384" x1="5376" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="576" y1="1840" y2="1936" x1="576" />
            <wire x2="752" y1="1936" y2="1936" x1="576" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="816" y1="1840" y2="1936" x1="816" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="1056" y1="1936" y2="1936" x1="880" />
            <wire x2="1056" y1="1840" y2="1936" x1="1056" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="2384" y1="1840" y2="1936" x1="2384" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="2624" y1="1936" y2="1936" x1="2448" />
            <wire x2="2624" y1="1840" y2="1936" x1="2624" />
        </branch>
        <instance x="1456" y="1968" name="XLXI_40" orien="R90" />
        <branch name="XLXN_39">
            <wire x2="1296" y1="1840" y2="1968" x1="1296" />
            <wire x2="1520" y1="1968" y2="1968" x1="1296" />
        </branch>
        <branch name="XLXN_40">
            <wire x2="1536" y1="1840" y2="1904" x1="1536" />
            <wire x2="1584" y1="1904" y2="1904" x1="1536" />
            <wire x2="1584" y1="1904" y2="1968" x1="1584" />
        </branch>
        <branch name="XLXN_41">
            <wire x2="1648" y1="1904" y2="1968" x1="1648" />
            <wire x2="1776" y1="1904" y2="1904" x1="1648" />
            <wire x2="1776" y1="1840" y2="1904" x1="1776" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="2112" y1="1840" y2="1936" x1="2112" />
            <wire x2="2320" y1="1936" y2="1936" x1="2112" />
        </branch>
        <branch name="XLXN_45">
            <wire x2="3136" y1="1888" y2="1888" x1="3104" />
            <wire x2="3104" y1="1888" y2="1936" x1="3104" />
            <wire x2="3136" y1="1840" y2="1888" x1="3136" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="3376" y1="1840" y2="1936" x1="3376" />
            <wire x2="3808" y1="1936" y2="1936" x1="3376" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="3632" y1="1840" y2="1888" x1="3632" />
            <wire x2="3872" y1="1888" y2="1888" x1="3632" />
            <wire x2="3872" y1="1888" y2="1936" x1="3872" />
        </branch>
        <branch name="XLXN_48">
            <wire x2="4112" y1="1936" y2="1936" x1="3936" />
            <wire x2="4688" y1="1936" y2="1936" x1="4112" />
            <wire x2="4688" y1="1936" y2="1968" x1="4688" />
            <wire x2="4112" y1="1840" y2="1936" x1="4112" />
        </branch>
        <instance x="4560" y="1968" name="XLXI_42" orien="R90" />
        <branch name="XLXN_49">
            <wire x2="4384" y1="1840" y2="1856" x1="4384" />
            <wire x2="4752" y1="1856" y2="1856" x1="4384" />
            <wire x2="4752" y1="1856" y2="1968" x1="4752" />
        </branch>
        <branch name="XLXN_52">
            <wire x2="3872" y1="1840" y2="1872" x1="3872" />
            <wire x2="4016" y1="1872" y2="1872" x1="3872" />
            <wire x2="4016" y1="1872" y2="1968" x1="4016" />
            <wire x2="4624" y1="1968" y2="1968" x1="4016" />
        </branch>
        <branch name="XLXN_53">
            <wire x2="4624" y1="1840" y2="1904" x1="4624" />
            <wire x2="4816" y1="1904" y2="1904" x1="4624" />
            <wire x2="4816" y1="1904" y2="1968" x1="4816" />
        </branch>
        <instance x="5216" y="1984" name="XLXI_43" orien="R90" />
        <branch name="XLXN_54">
            <wire x2="4864" y1="1840" y2="1984" x1="4864" />
            <wire x2="5280" y1="1984" y2="1984" x1="4864" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="1792" y1="1968" y2="1968" x1="1712" />
            <wire x2="1792" y1="1968" y2="2304" x1="1792" />
            <wire x2="5152" y1="2304" y2="2304" x1="1792" />
            <wire x2="5152" y1="1840" y2="1872" x1="5152" />
            <wire x2="5344" y1="1872" y2="1872" x1="5152" />
            <wire x2="5344" y1="1872" y2="1984" x1="5344" />
            <wire x2="5152" y1="1872" y2="2304" x1="5152" />
        </branch>
        <branch name="XLXN_60">
            <wire x2="3264" y1="1936" y2="1936" x1="3232" />
            <wire x2="5600" y1="1920" y2="1920" x1="3264" />
            <wire x2="5600" y1="1920" y2="1984" x1="5600" />
            <wire x2="3264" y1="1920" y2="1936" x1="3264" />
            <wire x2="5600" y1="1984" y2="1984" x1="5472" />
            <wire x2="5600" y1="1840" y2="1920" x1="5600" />
        </branch>
        <branch name="XLXN_56">
            <wire x2="5376" y1="1904" y2="1904" x1="3168" />
            <wire x2="5408" y1="1904" y2="1904" x1="5376" />
            <wire x2="5408" y1="1904" y2="1984" x1="5408" />
            <wire x2="3168" y1="1904" y2="1936" x1="3168" />
            <wire x2="5376" y1="1840" y2="1904" x1="5376" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="2896" y1="1840" y2="1936" x1="2896" />
            <wire x2="3040" y1="1936" y2="1936" x1="2896" />
        </branch>
        <instance x="2976" y="1936" name="XLXI_41" orien="R90" />
        <instance x="928" y="1584" name="AD2" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="1168" y="1584" name="AD3" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="1408" y="1584" name="AD4" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="1648" y="1584" name="AD5" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="2256" y="1584" name="AD7" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="2528" y="1584" name="AD8" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="3008" y="1584" name="AD10" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="3248" y="1584" name="AD11" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="3744" y="1584" name="AD13" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="3984" y="1584" name="AD14" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <instance x="4256" y="1584" name="AD15" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <branch name="point">
            <wire x2="256" y1="1136" y2="1168" x1="256" />
        </branch>
        <iomarker fontsize="28" x="256" y="1136" name="point" orien="R270" />
        <branch name="g">
            <wire x2="784" y1="2608" y2="2640" x1="784" />
        </branch>
        <iomarker fontsize="28" x="784" y="2640" name="g" orien="R90" />
        <branch name="f">
            <wire x2="1584" y1="2624" y2="2656" x1="1584" />
        </branch>
        <iomarker fontsize="28" x="1584" y="2656" name="f" orien="R90" />
        <branch name="a">
            <wire x2="5344" y1="2640" y2="2672" x1="5344" />
        </branch>
        <iomarker fontsize="28" x="5344" y="2672" name="a" orien="R90" />
        <branch name="b">
            <wire x2="4688" y1="2640" y2="2672" x1="4688" />
        </branch>
        <iomarker fontsize="28" x="4688" y="2672" name="b" orien="R90" />
        <branch name="e">
            <wire x2="2352" y1="2640" y2="2672" x1="2352" />
        </branch>
        <iomarker fontsize="28" x="2352" y="2672" name="e" orien="R90" />
        <branch name="d">
            <wire x2="3104" y1="2640" y2="2672" x1="3104" />
        </branch>
        <iomarker fontsize="28" x="3104" y="2672" name="d" orien="R90" />
        <branch name="c">
            <wire x2="3840" y1="2640" y2="2672" x1="3840" />
        </branch>
        <iomarker fontsize="28" x="3840" y="2672" name="c" orien="R90" />
        <instance x="5664" y="800" name="XLXI_44" orien="R90" />
        <instance x="5408" y="784" name="XLXI_45" orien="R90" />
        <instance x="5200" y="624" name="XLXI_46" orien="R90" />
        <instance x="4976" y="624" name="XLXI_47" orien="R90" />
        <instance x="1984" y="1584" name="AD6" orien="R90">
            <attrtext style="alignment:VLEFT;fontsize:28;fontname:Arial" attrname="InstName" x="256" y="-8" type="instance" />
        </instance>
        <branch name="XLXN_65">
            <wire x2="1056" y1="1472" y2="1584" x1="1056" />
            <wire x2="1536" y1="1472" y2="1472" x1="1056" />
            <wire x2="1536" y1="1472" y2="1584" x1="1536" />
            <wire x2="1776" y1="1472" y2="1472" x1="1536" />
            <wire x2="1776" y1="1472" y2="1584" x1="1776" />
            <wire x2="2176" y1="1472" y2="1472" x1="1776" />
            <wire x2="2176" y1="1472" y2="1584" x1="2176" />
            <wire x2="2928" y1="1472" y2="1472" x1="2176" />
            <wire x2="2928" y1="1472" y2="1584" x1="2928" />
            <wire x2="3664" y1="1472" y2="1472" x1="2928" />
            <wire x2="3664" y1="1472" y2="1584" x1="3664" />
            <wire x2="4896" y1="1472" y2="1472" x1="3664" />
            <wire x2="4896" y1="1472" y2="1584" x1="4896" />
            <wire x2="5440" y1="1472" y2="1472" x1="4896" />
            <wire x2="5568" y1="1472" y2="1472" x1="5440" />
            <wire x2="5568" y1="1472" y2="1584" x1="5568" />
            <wire x2="5440" y1="1008" y2="1472" x1="5440" />
        </branch>
        <branch name="XLXN_67">
            <wire x2="544" y1="1408" y2="1584" x1="544" />
            <wire x2="992" y1="1408" y2="1408" x1="544" />
            <wire x2="2112" y1="1408" y2="1408" x1="992" />
            <wire x2="2112" y1="1408" y2="1584" x1="2112" />
            <wire x2="2320" y1="1408" y2="1408" x1="2112" />
            <wire x2="2320" y1="1408" y2="1584" x1="2320" />
            <wire x2="4592" y1="1408" y2="1408" x1="2320" />
            <wire x2="4592" y1="1408" y2="1584" x1="4592" />
            <wire x2="5120" y1="1408" y2="1408" x1="4592" />
            <wire x2="5120" y1="1408" y2="1584" x1="5120" />
            <wire x2="5232" y1="1408" y2="1408" x1="5120" />
            <wire x2="5344" y1="1408" y2="1408" x1="5232" />
            <wire x2="5344" y1="1408" y2="1584" x1="5344" />
            <wire x2="5632" y1="1408" y2="1408" x1="5344" />
            <wire x2="5632" y1="1408" y2="1584" x1="5632" />
            <wire x2="992" y1="1408" y2="1584" x1="992" />
            <wire x2="5232" y1="848" y2="1408" x1="5232" />
        </branch>
        <branch name="XLXN_69">
            <wire x2="480" y1="1376" y2="1584" x1="480" />
            <wire x2="2800" y1="1376" y2="1376" x1="480" />
            <wire x2="2800" y1="1376" y2="1584" x1="2800" />
            <wire x2="3536" y1="1376" y2="1376" x1="2800" />
            <wire x2="3536" y1="1376" y2="1584" x1="3536" />
            <wire x2="4048" y1="1376" y2="1376" x1="3536" />
            <wire x2="4048" y1="1376" y2="1584" x1="4048" />
            <wire x2="4320" y1="1376" y2="1376" x1="4048" />
            <wire x2="5008" y1="1376" y2="1376" x1="4320" />
            <wire x2="5280" y1="1376" y2="1376" x1="5008" />
            <wire x2="5280" y1="1376" y2="1584" x1="5280" />
            <wire x2="4320" y1="1360" y2="1360" x1="4240" />
            <wire x2="4320" y1="1360" y2="1376" x1="4320" />
            <wire x2="4240" y1="1360" y2="1584" x1="4240" />
            <wire x2="4320" y1="1584" y2="1584" x1="4240" />
            <wire x2="5008" y1="848" y2="1376" x1="5008" />
        </branch>
        <branch name="D3">
            <wire x2="672" y1="1296" y2="1584" x1="672" />
            <wire x2="2992" y1="1296" y2="1296" x1="672" />
            <wire x2="2992" y1="1296" y2="1584" x1="2992" />
            <wire x2="3440" y1="1296" y2="1296" x1="2992" />
            <wire x2="3440" y1="1296" y2="1584" x1="3440" />
            <wire x2="3936" y1="1296" y2="1296" x1="3440" />
            <wire x2="3936" y1="1296" y2="1584" x1="3936" />
            <wire x2="4176" y1="1296" y2="1296" x1="3936" />
            <wire x2="4176" y1="1296" y2="1584" x1="4176" />
            <wire x2="4960" y1="1296" y2="1296" x1="4176" />
            <wire x2="4960" y1="1296" y2="1584" x1="4960" />
            <wire x2="5248" y1="1296" y2="1296" x1="4960" />
            <wire x2="5616" y1="1296" y2="1296" x1="5248" />
            <wire x2="5248" y1="1296" y2="1584" x1="5248" />
            <wire x2="5616" y1="736" y2="1296" x1="5616" />
            <wire x2="5696" y1="736" y2="736" x1="5616" />
            <wire x2="5696" y1="736" y2="800" x1="5696" />
            <wire x2="5696" y1="592" y2="736" x1="5696" />
        </branch>
        <branch name="D2">
            <wire x2="608" y1="1216" y2="1584" x1="608" />
            <wire x2="848" y1="1216" y2="1216" x1="608" />
            <wire x2="848" y1="1216" y2="1584" x1="848" />
            <wire x2="2384" y1="1216" y2="1216" x1="848" />
            <wire x2="2384" y1="1216" y2="1584" x1="2384" />
            <wire x2="3200" y1="1216" y2="1216" x1="2384" />
            <wire x2="3200" y1="1216" y2="1584" x1="3200" />
            <wire x2="3376" y1="1216" y2="1216" x1="3200" />
            <wire x2="3376" y1="1216" y2="1584" x1="3376" />
            <wire x2="4112" y1="1216" y2="1216" x1="3376" />
            <wire x2="4112" y1="1216" y2="1584" x1="4112" />
            <wire x2="4448" y1="1216" y2="1216" x1="4112" />
            <wire x2="4448" y1="1216" y2="1584" x1="4448" />
            <wire x2="4656" y1="1216" y2="1216" x1="4448" />
            <wire x2="4656" y1="1216" y2="1584" x1="4656" />
            <wire x2="5184" y1="1216" y2="1216" x1="4656" />
            <wire x2="5184" y1="1216" y2="1584" x1="5184" />
            <wire x2="5360" y1="1216" y2="1216" x1="5184" />
            <wire x2="5376" y1="1216" y2="1216" x1="5360" />
            <wire x2="5376" y1="1216" y2="1248" x1="5376" />
            <wire x2="5392" y1="1248" y2="1248" x1="5376" />
            <wire x2="5440" y1="704" y2="704" x1="5360" />
            <wire x2="5440" y1="704" y2="784" x1="5440" />
            <wire x2="5360" y1="704" y2="1216" x1="5360" />
            <wire x2="5392" y1="1216" y2="1248" x1="5392" />
            <wire x2="5408" y1="1216" y2="1216" x1="5392" />
            <wire x2="5408" y1="1216" y2="1584" x1="5408" />
            <wire x2="5440" y1="544" y2="704" x1="5440" />
        </branch>
        <branch name="XLXN_75">
            <wire x2="912" y1="1536" y2="1584" x1="912" />
            <wire x2="1120" y1="1536" y2="1536" x1="912" />
            <wire x2="1120" y1="1536" y2="1584" x1="1120" />
            <wire x2="1360" y1="1536" y2="1536" x1="1120" />
            <wire x2="1360" y1="1536" y2="1552" x1="1360" />
            <wire x2="1360" y1="1552" y2="1584" x1="1360" />
            <wire x2="1600" y1="1536" y2="1536" x1="1360" />
            <wire x2="1600" y1="1536" y2="1584" x1="1600" />
            <wire x2="1840" y1="1536" y2="1536" x1="1600" />
            <wire x2="1840" y1="1536" y2="1584" x1="1840" />
            <wire x2="2448" y1="1536" y2="1536" x1="1840" />
            <wire x2="2448" y1="1536" y2="1584" x1="2448" />
            <wire x2="2656" y1="1536" y2="1536" x1="2448" />
            <wire x2="2656" y1="1536" y2="1584" x1="2656" />
            <wire x2="3728" y1="1536" y2="1536" x1="2656" />
            <wire x2="3728" y1="1536" y2="1584" x1="3728" />
            <wire x2="4720" y1="1536" y2="1536" x1="3728" />
            <wire x2="4720" y1="1536" y2="1584" x1="4720" />
            <wire x2="5472" y1="1536" y2="1536" x1="4720" />
            <wire x2="5696" y1="1536" y2="1536" x1="5472" />
            <wire x2="5696" y1="1536" y2="1584" x1="5696" />
            <wire x2="5472" y1="1536" y2="1584" x1="5472" />
            <wire x2="5696" y1="1024" y2="1536" x1="5696" />
        </branch>
        <branch name="D1">
            <wire x2="784" y1="1136" y2="1584" x1="784" />
            <wire x2="1232" y1="1136" y2="1136" x1="784" />
            <wire x2="1232" y1="1136" y2="1584" x1="1232" />
            <wire x2="1472" y1="1136" y2="1136" x1="1232" />
            <wire x2="1712" y1="1136" y2="1136" x1="1472" />
            <wire x2="2048" y1="1136" y2="1136" x1="1712" />
            <wire x2="2592" y1="1136" y2="1136" x1="2048" />
            <wire x2="2864" y1="1136" y2="1136" x1="2592" />
            <wire x2="2880" y1="1136" y2="1136" x1="2864" />
            <wire x2="3072" y1="1136" y2="1136" x1="2880" />
            <wire x2="3136" y1="1136" y2="1136" x1="3072" />
            <wire x2="3136" y1="1136" y2="1568" x1="3136" />
            <wire x2="3136" y1="1568" y2="1584" x1="3136" />
            <wire x2="3312" y1="1136" y2="1136" x1="3136" />
            <wire x2="3312" y1="1136" y2="1584" x1="3312" />
            <wire x2="3600" y1="1136" y2="1136" x1="3312" />
            <wire x2="3808" y1="1136" y2="1136" x1="3600" />
            <wire x2="3872" y1="1136" y2="1136" x1="3808" />
            <wire x2="3872" y1="1136" y2="1584" x1="3872" />
            <wire x2="4384" y1="1136" y2="1136" x1="3872" />
            <wire x2="4528" y1="1136" y2="1136" x1="4384" />
            <wire x2="4768" y1="1136" y2="1136" x1="4528" />
            <wire x2="4832" y1="1136" y2="1136" x1="4768" />
            <wire x2="5136" y1="1136" y2="1136" x1="4832" />
            <wire x2="4832" y1="1136" y2="1584" x1="4832" />
            <wire x2="4384" y1="1136" y2="1584" x1="4384" />
            <wire x2="3600" y1="1136" y2="1584" x1="3600" />
            <wire x2="2864" y1="1136" y2="1584" x1="2864" />
            <wire x2="1472" y1="1136" y2="1584" x1="1472" />
            <wire x2="5136" y1="576" y2="1136" x1="5136" />
            <wire x2="5232" y1="576" y2="576" x1="5136" />
            <wire x2="5232" y1="576" y2="624" x1="5232" />
            <wire x2="5232" y1="448" y2="576" x1="5232" />
        </branch>
        <branch name="D0">
            <wire x2="704" y1="1024" y2="1584" x1="704" />
            <wire x2="720" y1="1584" y2="1584" x1="704" />
            <wire x2="1312" y1="1024" y2="1024" x1="704" />
            <wire x2="1312" y1="1024" y2="1280" x1="1312" />
            <wire x2="1720" y1="1024" y2="1024" x1="1312" />
            <wire x2="1728" y1="1024" y2="1024" x1="1720" />
            <wire x2="1728" y1="1024" y2="1200" x1="1728" />
            <wire x2="2032" y1="1024" y2="1024" x1="1728" />
            <wire x2="2040" y1="1024" y2="1024" x1="2032" />
            <wire x2="2600" y1="1024" y2="1024" x1="2040" />
            <wire x2="2608" y1="1024" y2="1024" x1="2600" />
            <wire x2="2608" y1="1024" y2="1200" x1="2608" />
            <wire x2="3056" y1="1024" y2="1024" x1="2608" />
            <wire x2="3064" y1="1024" y2="1024" x1="3056" />
            <wire x2="3328" y1="1024" y2="1024" x1="3064" />
            <wire x2="3792" y1="1024" y2="1024" x1="3328" />
            <wire x2="3800" y1="1024" y2="1024" x1="3792" />
            <wire x2="4536" y1="1024" y2="1024" x1="3800" />
            <wire x2="4544" y1="1024" y2="1024" x1="4536" />
            <wire x2="4544" y1="1024" y2="1200" x1="4544" />
            <wire x2="4752" y1="1024" y2="1024" x1="4544" />
            <wire x2="4752" y1="1024" y2="1584" x1="4752" />
            <wire x2="4768" y1="1584" y2="1584" x1="4752" />
            <wire x2="4848" y1="1024" y2="1024" x1="4752" />
            <wire x2="5072" y1="1024" y2="1024" x1="4848" />
            <wire x2="5504" y1="1024" y2="1024" x1="5072" />
            <wire x2="5504" y1="1024" y2="1584" x1="5504" />
            <wire x2="5072" y1="1024" y2="1280" x1="5072" />
            <wire x2="3792" y1="1024" y2="1200" x1="3792" />
            <wire x2="3808" y1="1200" y2="1200" x1="3792" />
            <wire x2="3808" y1="1200" y2="1584" x1="3808" />
            <wire x2="3056" y1="1024" y2="1200" x1="3056" />
            <wire x2="3072" y1="1200" y2="1200" x1="3056" />
            <wire x2="3072" y1="1200" y2="1584" x1="3072" />
            <wire x2="2032" y1="1024" y2="1200" x1="2032" />
            <wire x2="2048" y1="1200" y2="1200" x1="2032" />
            <wire x2="2048" y1="1200" y2="1584" x1="2048" />
            <wire x2="1296" y1="1280" y2="1584" x1="1296" />
            <wire x2="1312" y1="1280" y2="1280" x1="1296" />
            <wire x2="1728" y1="1200" y2="1200" x1="1712" />
            <wire x2="1712" y1="1200" y2="1584" x1="1712" />
            <wire x2="2608" y1="1200" y2="1200" x1="2592" />
            <wire x2="2592" y1="1200" y2="1584" x1="2592" />
            <wire x2="4544" y1="1200" y2="1200" x1="4528" />
            <wire x2="4528" y1="1200" y2="1584" x1="4528" />
            <wire x2="5008" y1="496" y2="496" x1="4848" />
            <wire x2="5008" y1="496" y2="624" x1="5008" />
            <wire x2="4848" y1="496" y2="1024" x1="4848" />
            <wire x2="5008" y1="384" y2="496" x1="5008" />
            <wire x2="5056" y1="1280" y2="1584" x1="5056" />
            <wire x2="5072" y1="1280" y2="1280" x1="5056" />
        </branch>
        <iomarker fontsize="28" x="5008" y="384" name="D0" orien="R270" />
        <iomarker fontsize="28" x="5232" y="448" name="D1" orien="R270" />
        <iomarker fontsize="28" x="5440" y="544" name="D2" orien="R270" />
        <iomarker fontsize="28" x="5696" y="592" name="D3" orien="R270" />
    </sheet>
</drawing>