<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="BTN(0)" />
        <signal name="BTN(1)" />
        <signal name="SW(3)" />
        <signal name="SW(2)" />
        <signal name="SW(1)" />
        <signal name="SW(0)" />
        <signal name="AN(3:0)" />
        <signal name="BTN(1:0)" />
        <signal name="SW(7:0)" />
        <signal name="SW(7)" />
        <signal name="SW(6)" />
        <signal name="SW(5)" />
        <signal name="SW(4)" />
        <signal name="AN(3)" />
        <signal name="AN(2)" />
        <signal name="AN(1)" />
        <signal name="AN(0)" />
        <signal name="SEGMENT(7:0)" />
        <signal name="SEGMENT(0)" />
        <signal name="SEGMENT(1)" />
        <signal name="SEGMENT(2)" />
        <signal name="SEGMENT(3)" />
        <signal name="SEGMENT(4)" />
        <signal name="SEGMENT(5)" />
        <signal name="SEGMENT(6)" />
        <signal name="SEGMENT(7)" />
        <signal name="Buzzer" />
        <signal name="XLXN_1" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Input" name="BTN(1:0)" />
        <port polarity="Input" name="SW(7:0)" />
        <port polarity="Output" name="SEGMENT(7:0)" />
        <port polarity="Output" name="Buzzer" />
        <blockdef name="MC14495">
            <timestamp>2017-11-1T7:39:4</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-400" y2="-400" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="inv4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="160" y1="-96" y2="-96" x1="224" />
            <line x2="160" y1="-160" y2="-160" x1="224" />
            <line x2="160" y1="-224" y2="-224" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-96" y2="-96" x1="0" />
            <line x2="64" y1="-160" y2="-160" x1="0" />
            <line x2="64" y1="-224" y2="-224" x1="0" />
            <line x2="128" y1="-256" y2="-224" x1="64" />
            <line x2="64" y1="-224" y2="-192" x1="128" />
            <line x2="64" y1="-192" y2="-256" x1="64" />
            <circle r="16" cx="144" cy="-32" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="128" y1="-128" y2="-96" x1="64" />
            <line x2="64" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="-128" x1="64" />
            <circle r="16" cx="144" cy="-96" />
            <line x2="128" y1="-192" y2="-160" x1="64" />
            <line x2="64" y1="-160" y2="-128" x1="128" />
            <line x2="64" y1="-128" y2="-192" x1="64" />
            <circle r="16" cx="144" cy="-160" />
            <circle r="16" cx="144" cy="-224" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="MC14495" name="XLXI_1">
            <blockpin signalname="BTN(0)" name="LE" />
            <blockpin signalname="BTN(1)" name="point" />
            <blockpin signalname="SW(3)" name="D3" />
            <blockpin signalname="SW(2)" name="D2" />
            <blockpin signalname="SW(1)" name="D1" />
            <blockpin signalname="SW(0)" name="D0" />
            <blockpin signalname="SEGMENT(7)" name="p" />
            <blockpin signalname="SEGMENT(6)" name="g" />
            <blockpin signalname="SEGMENT(5)" name="f" />
            <blockpin signalname="SEGMENT(0)" name="a" />
            <blockpin signalname="SEGMENT(1)" name="b" />
            <blockpin signalname="SEGMENT(4)" name="e" />
            <blockpin signalname="SEGMENT(3)" name="d" />
            <blockpin signalname="SEGMENT(2)" name="c" />
        </block>
        <block symbolname="inv4" name="XLXI_6">
            <blockpin signalname="SW(4)" name="I0" />
            <blockpin signalname="SW(5)" name="I1" />
            <blockpin signalname="SW(6)" name="I2" />
            <blockpin signalname="SW(7)" name="I3" />
            <blockpin signalname="AN(0)" name="O0" />
            <blockpin signalname="AN(1)" name="O1" />
            <blockpin signalname="AN(2)" name="O2" />
            <blockpin signalname="AN(3)" name="O3" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="Buzzer" name="P" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1168" y="1264" name="XLXI_1" orien="R0">
        </instance>
        <branch name="BTN(0)">
            <wire x2="864" y1="784" y2="784" x1="816" />
            <wire x2="1168" y1="784" y2="784" x1="864" />
        </branch>
        <branch name="BTN(1)">
            <wire x2="1168" y1="864" y2="864" x1="816" />
        </branch>
        <branch name="SW(3)">
            <wire x2="1168" y1="944" y2="944" x1="720" />
        </branch>
        <branch name="SW(2)">
            <wire x2="1168" y1="1024" y2="1024" x1="720" />
        </branch>
        <branch name="SW(1)">
            <wire x2="1168" y1="1104" y2="1104" x1="720" />
        </branch>
        <branch name="SW(0)">
            <wire x2="736" y1="1184" y2="1184" x1="720" />
            <wire x2="1168" y1="1184" y2="1184" x1="736" />
        </branch>
        <branch name="AN(3:0)">
            <wire x2="1808" y1="1488" y2="1536" x1="1808" />
            <wire x2="1808" y1="1536" y2="1600" x1="1808" />
            <wire x2="2224" y1="1600" y2="1600" x1="1808" />
            <wire x2="1808" y1="1600" y2="1664" x1="1808" />
            <wire x2="1808" y1="1664" y2="1712" x1="1808" />
            <wire x2="1808" y1="1712" y2="1776" x1="1808" />
        </branch>
        <branch name="BTN(1:0)">
            <wire x2="720" y1="768" y2="768" x1="480" />
            <wire x2="720" y1="768" y2="784" x1="720" />
            <wire x2="720" y1="784" y2="864" x1="720" />
            <wire x2="720" y1="864" y2="896" x1="720" />
        </branch>
        <branch name="SW(7:0)">
            <wire x2="624" y1="1616" y2="1616" x1="464" />
            <wire x2="624" y1="1616" y2="1696" x1="624" />
            <wire x2="624" y1="1696" y2="1792" x1="624" />
            <wire x2="624" y1="1792" y2="1984" x1="624" />
            <wire x2="624" y1="672" y2="944" x1="624" />
            <wire x2="624" y1="944" y2="1024" x1="624" />
            <wire x2="624" y1="1024" y2="1104" x1="624" />
            <wire x2="624" y1="1104" y2="1184" x1="624" />
            <wire x2="624" y1="1184" y2="1488" x1="624" />
            <wire x2="624" y1="1488" y2="1600" x1="624" />
            <wire x2="624" y1="1600" y2="1616" x1="624" />
        </branch>
        <iomarker fontsize="28" x="464" y="1616" name="SW(7:0)" orien="R180" />
        <bustap x2="720" y1="944" y2="944" x1="624" />
        <bustap x2="720" y1="1024" y2="1024" x1="624" />
        <bustap x2="720" y1="1488" y2="1488" x1="624" />
        <bustap x2="720" y1="1600" y2="1600" x1="624" />
        <bustap x2="720" y1="1696" y2="1696" x1="624" />
        <bustap x2="720" y1="1792" y2="1792" x1="624" />
        <branch name="SW(7)">
            <wire x2="1024" y1="1488" y2="1488" x1="720" />
            <wire x2="1024" y1="1488" y2="1536" x1="1024" />
            <wire x2="1152" y1="1536" y2="1536" x1="1024" />
        </branch>
        <branch name="SW(6)">
            <wire x2="1152" y1="1600" y2="1600" x1="720" />
        </branch>
        <branch name="SW(5)">
            <wire x2="1024" y1="1696" y2="1696" x1="720" />
            <wire x2="1152" y1="1664" y2="1664" x1="1024" />
            <wire x2="1024" y1="1664" y2="1696" x1="1024" />
        </branch>
        <branch name="SW(4)">
            <wire x2="992" y1="1792" y2="1792" x1="720" />
            <wire x2="1152" y1="1728" y2="1728" x1="992" />
            <wire x2="992" y1="1728" y2="1792" x1="992" />
        </branch>
        <instance x="1152" y="1760" name="XLXI_6" orien="R0" />
        <iomarker fontsize="28" x="2224" y="1600" name="AN(3:0)" orien="R0" />
        <bustap x2="1712" y1="1536" y2="1536" x1="1808" />
        <bustap x2="1712" y1="1600" y2="1600" x1="1808" />
        <bustap x2="1712" y1="1664" y2="1664" x1="1808" />
        <bustap x2="1712" y1="1712" y2="1712" x1="1808" />
        <branch name="AN(3)">
            <wire x2="1712" y1="1536" y2="1536" x1="1376" />
        </branch>
        <branch name="AN(2)">
            <wire x2="1712" y1="1600" y2="1600" x1="1376" />
        </branch>
        <branch name="AN(1)">
            <wire x2="1712" y1="1664" y2="1664" x1="1376" />
        </branch>
        <branch name="AN(0)">
            <wire x2="1536" y1="1728" y2="1728" x1="1376" />
            <wire x2="1536" y1="1712" y2="1728" x1="1536" />
            <wire x2="1712" y1="1712" y2="1712" x1="1536" />
        </branch>
        <branch name="SEGMENT(7:0)">
            <wire x2="1968" y1="704" y2="800" x1="1968" />
            <wire x2="1968" y1="800" y2="848" x1="1968" />
            <wire x2="1968" y1="848" y2="912" x1="1968" />
            <wire x2="1968" y1="912" y2="992" x1="1968" />
            <wire x2="1968" y1="992" y2="1024" x1="1968" />
            <wire x2="2352" y1="1024" y2="1024" x1="1968" />
            <wire x2="1968" y1="1024" y2="1072" x1="1968" />
            <wire x2="1968" y1="1072" y2="1136" x1="1968" />
            <wire x2="1968" y1="1136" y2="1200" x1="1968" />
            <wire x2="1968" y1="1200" y2="1264" x1="1968" />
            <wire x2="1968" y1="1264" y2="1360" x1="1968" />
        </branch>
        <iomarker fontsize="28" x="2352" y="1024" name="SEGMENT(7:0)" orien="R0" />
        <bustap x2="1872" y1="800" y2="800" x1="1968" />
        <bustap x2="1872" y1="848" y2="848" x1="1968" />
        <bustap x2="1872" y1="912" y2="912" x1="1968" />
        <bustap x2="1872" y1="992" y2="992" x1="1968" />
        <bustap x2="1872" y1="1072" y2="1072" x1="1968" />
        <bustap x2="1872" y1="1136" y2="1136" x1="1968" />
        <bustap x2="1872" y1="1200" y2="1200" x1="1968" />
        <bustap x2="1872" y1="1264" y2="1264" x1="1968" />
        <branch name="SEGMENT(0)">
            <wire x2="1712" y1="976" y2="976" x1="1552" />
            <wire x2="1712" y1="800" y2="976" x1="1712" />
            <wire x2="1872" y1="800" y2="800" x1="1712" />
        </branch>
        <branch name="SEGMENT(1)">
            <wire x2="1728" y1="1040" y2="1040" x1="1552" />
            <wire x2="1728" y1="848" y2="1040" x1="1728" />
            <wire x2="1872" y1="848" y2="848" x1="1728" />
        </branch>
        <branch name="SEGMENT(2)">
            <wire x2="1744" y1="1232" y2="1232" x1="1552" />
            <wire x2="1744" y1="912" y2="1232" x1="1744" />
            <wire x2="1872" y1="912" y2="912" x1="1744" />
        </branch>
        <branch name="SEGMENT(3)">
            <wire x2="1760" y1="1168" y2="1168" x1="1552" />
            <wire x2="1760" y1="992" y2="1168" x1="1760" />
            <wire x2="1872" y1="992" y2="992" x1="1760" />
        </branch>
        <branch name="SEGMENT(4)">
            <wire x2="1712" y1="1104" y2="1104" x1="1552" />
            <wire x2="1712" y1="1072" y2="1104" x1="1712" />
            <wire x2="1872" y1="1072" y2="1072" x1="1712" />
        </branch>
        <branch name="SEGMENT(5)">
            <wire x2="1696" y1="912" y2="912" x1="1552" />
            <wire x2="1696" y1="912" y2="1136" x1="1696" />
            <wire x2="1872" y1="1136" y2="1136" x1="1696" />
        </branch>
        <branch name="SEGMENT(6)">
            <wire x2="1680" y1="848" y2="848" x1="1552" />
            <wire x2="1680" y1="848" y2="1200" x1="1680" />
            <wire x2="1872" y1="1200" y2="1200" x1="1680" />
        </branch>
        <branch name="SEGMENT(7)">
            <wire x2="1664" y1="784" y2="784" x1="1552" />
            <wire x2="1664" y1="784" y2="1264" x1="1664" />
            <wire x2="1872" y1="1264" y2="1264" x1="1664" />
        </branch>
        <branch name="Buzzer">
            <wire x2="592" y1="2128" y2="2192" x1="592" />
            <wire x2="1488" y1="2192" y2="2192" x1="592" />
        </branch>
        <instance x="528" y="2128" name="XLXI_7" orien="R0" />
        <iomarker fontsize="28" x="1488" y="2192" name="Buzzer" orien="R0" />
        <iomarker fontsize="28" x="480" y="768" name="BTN(1:0)" orien="R180" />
        <bustap x2="816" y1="784" y2="784" x1="720" />
        <bustap x2="816" y1="864" y2="864" x1="720" />
        <bustap x2="720" y1="1104" y2="1104" x1="624" />
        <bustap x2="720" y1="1184" y2="1184" x1="624" />
    </sheet>
</drawing>