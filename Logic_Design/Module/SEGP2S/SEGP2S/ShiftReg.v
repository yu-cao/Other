`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:42:21 12/21/2017 
// Design Name: 
// Module Name:    ShiftReg 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ShiftReg(clk, num, led);
input wire clk;
input wire [63:0] num;
output wire [3:0] led;
wire CLK,DT,CLR;
reg EN;
assign led = {CLK,DT,EN,CLR};

reg [64:0] shift;
reg [11:0] delay = -1;//全部为1

assign zero = |shift[63:0];//shift合成1位，判断是否全为0

always @ (posedge clk)
begin
	  if(zero)
	     shift <= {shift[63:0], 1'b0};
	  else
	  begin
	      if(&delay)//达到延迟时间
	           begin
				  EN <= 1'b0;
              shift <= {num, 1'b1};			  
              end
			 else
			     EN <= 1'b1;
		delay <= delay + 1'b1;
	 end
end

assign CLR = 1'b1;
assign CLK = ~clk & zero;
assign DT = shift[64];

endmodule
