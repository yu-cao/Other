`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:07:04 12/21/2017 
// Design Name: 
// Module Name:    TOP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Top(clk,SEGCLK,SEGDT,SEGEN,SEGCLR,buzzer,LED);
input wire clk;
output wire SEGCLK;
output wire SEGDT;
output wire SEGEN;
output wire SEGCLR;
output wire buzzer;
output wire [7:0] LED;

reg [31:0]clkdiv;

always@(posedge clk) begin
clkdiv <= clkdiv + 1'b1;
end

SEG_DRV s(clkdiv[3],32'h01234567,{SEGCLK,SEGDT,SEGEN,SEGCLR});

assign buzzer=1;
assign LED = 8'b11111111;
endmodule
