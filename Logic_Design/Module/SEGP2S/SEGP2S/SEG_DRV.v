`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:09:45 12/21/2017 
// Design Name: 
// Module Name:    SEG_DRV 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SEG_DRV(clk,num,seg);
input wire clk;
input wire [31:0] num;
output wire  [3:0] seg;

wire [63:0] segment;
wire [7:0] temp;

mymc14495 
m1(num[0],num[1],num[2],num[3],1'b0,1'b0,segment[0],segment[1],segment[2],segment[3],segment[4],segment[5],segment[6],temp[0]),
m2(num[4],num[5],num[6],num[7],1'b0,1'b0,segment[8],segment[9],segment[10],segment[11],segment[12],segment[13],segment[14],temp[1]),
m3(num[8],num[9],num[10],num[11],1'b0,1'b0,segment[16],segment[17],segment[18],segment[19],segment[20],segment[21],segment[22],temp[2]),
m4(num[12],num[13],num[14],num[15],1'b0,1'b0,segment[24],segment[25],segment[26],segment[27],segment[28],segment[29],segment[30],temp[3]),
m5(num[16],num[17],num[18],num[19],1'b0,1'b0,segment[32],segment[33],segment[34],segment[35],segment[36],segment[37],segment[38],temp[4]),
m6(num[20],num[21],num[22],num[23],1'b0,1'b0,segment[40],segment[41],segment[42],segment[43],segment[44],segment[45],segment[46],temp[5]),
m7(num[24],num[25],num[26],num[27],1'b0,1'b0,segment[48],segment[49],segment[50],segment[51],segment[52],segment[53],segment[54],temp[6]),
m8(num[28],num[29],num[30],num[31],1'b0,1'b0,segment[56],segment[57],segment[58],segment[59],segment[60],segment[61],segment[62],temp[7]);

assign {segment[63], segment[55], segment[47], segment[39],segment[31], segment[23], segment[15], segment[7]} = 8'hFF;

ShiftReg s(clk, segment,seg);

endmodule

