`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:39:02 12/09/2017 
// Design Name: 
// Module Name:    Top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Top(
	input wire clk,
	output wire [3:0]AN,
	output wire [7:0]SEGMENT,
//	output wire Rc,//��LED��ʾ
	output wire Buzzer,
	output wire [7:0]LED
    );
	
	wire [3:0]num;
    wire clk_1s;
	
	counter_1s m0(clk, clk_1s);
	
	counter_4bit m1(clk_1s, num[0],num[1],num[2],num[3], LED[0]);
	
	DispNum m3(clk,{12'b0,num},4'b0,4'b0,1'b0, AN, SEGMENT);

	assign Buzzer = 1'b1;
	assign LED[7:1]=7'b1111111;

	
endmodule

