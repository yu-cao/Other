<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="D" />
        <signal name="C" />
        <signal name="XLXN_4" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="Q" />
        <signal name="Qbar" />
        <port polarity="Input" name="D" />
        <port polarity="Input" name="C" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qbar" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_1">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="D" name="I1" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_2">
            <blockpin signalname="XLXN_4" name="I0" />
            <blockpin signalname="C" name="I1" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_3">
            <blockpin signalname="Qbar" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_4">
            <blockpin signalname="XLXN_8" name="I0" />
            <blockpin signalname="Q" name="I1" />
            <blockpin signalname="Qbar" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_5">
            <blockpin signalname="D" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1024" y="1200" name="XLXI_1" orien="R0" />
        <instance x="1024" y="1504" name="XLXI_2" orien="R0" />
        <instance x="1616" y="1264" name="XLXI_3" orien="R0" />
        <instance x="1616" y="1488" name="XLXI_4" orien="R0" />
        <branch name="D">
            <wire x2="752" y1="1072" y2="1072" x1="656" />
            <wire x2="752" y1="1072" y2="1440" x1="752" />
            <wire x2="768" y1="1440" y2="1440" x1="752" />
            <wire x2="1024" y1="1072" y2="1072" x1="752" />
        </branch>
        <branch name="C">
            <wire x2="944" y1="1264" y2="1264" x1="656" />
            <wire x2="944" y1="1264" y2="1376" x1="944" />
            <wire x2="1024" y1="1376" y2="1376" x1="944" />
            <wire x2="944" y1="1136" y2="1264" x1="944" />
            <wire x2="1024" y1="1136" y2="1136" x1="944" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1024" y1="1440" y2="1440" x1="992" />
        </branch>
        <instance x="768" y="1472" name="XLXI_5" orien="R0" />
        <iomarker fontsize="28" x="656" y="1264" name="C" orien="R180" />
        <iomarker fontsize="28" x="656" y="1072" name="D" orien="R180" />
        <branch name="XLXN_7">
            <wire x2="1440" y1="1104" y2="1104" x1="1280" />
            <wire x2="1440" y1="1104" y2="1136" x1="1440" />
            <wire x2="1616" y1="1136" y2="1136" x1="1440" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1440" y1="1408" y2="1408" x1="1280" />
            <wire x2="1440" y1="1408" y2="1424" x1="1440" />
            <wire x2="1616" y1="1424" y2="1424" x1="1440" />
        </branch>
        <branch name="Q">
            <wire x2="1536" y1="1280" y2="1360" x1="1536" />
            <wire x2="1616" y1="1360" y2="1360" x1="1536" />
            <wire x2="1920" y1="1280" y2="1280" x1="1536" />
            <wire x2="1920" y1="1168" y2="1168" x1="1872" />
            <wire x2="1920" y1="1168" y2="1280" x1="1920" />
            <wire x2="2080" y1="1168" y2="1168" x1="1920" />
        </branch>
        <branch name="Qbar">
            <wire x2="1616" y1="1200" y2="1200" x1="1552" />
            <wire x2="1552" y1="1200" y2="1264" x1="1552" />
            <wire x2="1952" y1="1264" y2="1264" x1="1552" />
            <wire x2="1952" y1="1264" y2="1392" x1="1952" />
            <wire x2="2048" y1="1392" y2="1392" x1="1952" />
            <wire x2="1952" y1="1392" y2="1392" x1="1872" />
        </branch>
        <iomarker fontsize="28" x="2080" y="1168" name="Q" orien="R0" />
        <iomarker fontsize="28" x="2048" y="1392" name="Qbar" orien="R0" />
    </sheet>
</drawing>