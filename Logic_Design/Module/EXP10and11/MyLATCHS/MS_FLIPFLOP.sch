<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="S" />
        <signal name="R" />
        <signal name="C" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="Q" />
        <signal name="Qbar" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="R" />
        <port polarity="Input" name="C" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qbar" />
        <blockdef name="CSR_LATCH">
            <timestamp>2017-12-6T5:49:19</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="CSR_LATCH" name="XLXI_1">
            <blockpin signalname="S" name="S" />
            <blockpin signalname="R" name="R" />
            <blockpin signalname="C" name="C" />
            <blockpin signalname="XLXN_6" name="Q" />
            <blockpin signalname="XLXN_5" name="Qbar" />
        </block>
        <block symbolname="CSR_LATCH" name="XLXI_2">
            <blockpin signalname="XLXN_6" name="S" />
            <blockpin signalname="XLXN_5" name="R" />
            <blockpin signalname="XLXN_4" name="C" />
            <blockpin signalname="Q" name="Q" />
            <blockpin signalname="Qbar" name="Qbar" />
        </block>
        <block symbolname="inv" name="XLXI_3">
            <blockpin signalname="C" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="992" y="1168" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1680" y="1184" name="XLXI_2" orien="R0">
        </instance>
        <branch name="S">
            <wire x2="992" y1="1008" y2="1008" x1="848" />
        </branch>
        <branch name="R">
            <wire x2="992" y1="1072" y2="1072" x1="848" />
        </branch>
        <branch name="C">
            <wire x2="912" y1="1136" y2="1136" x1="848" />
            <wire x2="928" y1="1136" y2="1136" x1="912" />
            <wire x2="992" y1="1136" y2="1136" x1="928" />
            <wire x2="928" y1="1136" y2="1312" x1="928" />
            <wire x2="1120" y1="1312" y2="1312" x1="928" />
        </branch>
        <iomarker fontsize="28" x="848" y="1136" name="C" orien="R180" />
        <iomarker fontsize="28" x="848" y="1072" name="R" orien="R180" />
        <iomarker fontsize="28" x="848" y="1008" name="S" orien="R180" />
        <instance x="1120" y="1344" name="XLXI_3" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1504" y1="1312" y2="1312" x1="1344" />
            <wire x2="1504" y1="1152" y2="1312" x1="1504" />
            <wire x2="1680" y1="1152" y2="1152" x1="1504" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1520" y1="1136" y2="1136" x1="1376" />
            <wire x2="1520" y1="1088" y2="1136" x1="1520" />
            <wire x2="1680" y1="1088" y2="1088" x1="1520" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1520" y1="1008" y2="1008" x1="1376" />
            <wire x2="1520" y1="1008" y2="1024" x1="1520" />
            <wire x2="1680" y1="1024" y2="1024" x1="1520" />
        </branch>
        <branch name="Q">
            <wire x2="2096" y1="1024" y2="1024" x1="2064" />
        </branch>
        <branch name="Qbar">
            <wire x2="2096" y1="1152" y2="1152" x1="2064" />
        </branch>
        <iomarker fontsize="28" x="2096" y="1024" name="Q" orien="R0" />
        <iomarker fontsize="28" x="2096" y="1152" name="Qbar" orien="R0" />
    </sheet>
</drawing>