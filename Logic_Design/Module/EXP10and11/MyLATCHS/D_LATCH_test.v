// Verilog test fixture created from schematic E:\Logic_Design\exp10\MyLATCHS\D_LATCH.sch - Wed Dec 06 13:42:56 2017

`timescale 1ns / 1ps

module D_LATCH_D_LATCH_sch_tb();

// Inputs
   reg D;
   reg C;

// Output
   wire Q;
   wire Qbar;

// Bidirs

// Instantiate the UUT
   D_LATCH UUT (
		.D(D), 
		.C(C), 
		.Q(Q), 
		.Qbar(Qbar)
   );
// Initialize Inputs
       initial begin
		 D=0;
		 C=1;
		 #50;
		 
		 
		C=1;
		D=1;
		#50;
		
		D=0;
		C=0;
		#50;
		
		C=0;D=1;#50;
		
		D=1;
		end




endmodule
