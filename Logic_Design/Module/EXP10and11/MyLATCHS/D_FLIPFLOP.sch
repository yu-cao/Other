<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="C" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="Sbar" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="Rbar" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="Q" />
        <signal name="Qbar" />
        <signal name="D" />
        <port polarity="Input" name="C" />
        <port polarity="Input" name="Sbar" />
        <port polarity="Input" name="Rbar" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qbar" />
        <port polarity="Input" name="D" />
        <blockdef name="nand3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="216" y1="-128" y2="-128" x1="256" />
            <circle r="12" cx="204" cy="-128" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <block symbolname="nand3" name="XLXI_1">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="Sbar" name="I2" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_2">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="Rbar" name="I1" />
            <blockpin signalname="XLXN_4" name="I2" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_3">
            <blockpin signalname="XLXN_10" name="I0" />
            <blockpin signalname="C" name="I1" />
            <blockpin signalname="XLXN_5" name="I2" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_4">
            <blockpin signalname="D" name="I0" />
            <blockpin signalname="Rbar" name="I1" />
            <blockpin signalname="XLXN_9" name="I2" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_5">
            <blockpin signalname="Qbar" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="Sbar" name="I2" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="nand3" name="XLXI_6">
            <blockpin signalname="Rbar" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="Q" name="I2" />
            <blockpin signalname="Qbar" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="704" y="528" name="XLXI_1" orien="R0" />
        <instance x="672" y="1120" name="XLXI_3" orien="R0" />
        <instance x="672" y="1360" name="XLXI_4" orien="R0" />
        <instance x="1456" y="816" name="XLXI_5" orien="R0" />
        <instance x="1440" y="1168" name="XLXI_6" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="672" y1="864" y2="928" x1="672" />
            <wire x2="1008" y1="864" y2="864" x1="672" />
            <wire x2="704" y1="464" y2="544" x1="704" />
            <wire x2="1008" y1="544" y2="544" x1="704" />
            <wire x2="1008" y1="544" y2="720" x1="1008" />
            <wire x2="1008" y1="720" y2="864" x1="1008" />
            <wire x2="1232" y1="720" y2="720" x1="1008" />
            <wire x2="1008" y1="720" y2="720" x1="944" />
            <wire x2="1232" y1="688" y2="720" x1="1232" />
            <wire x2="1456" y1="688" y2="688" x1="1232" />
        </branch>
        <branch name="Sbar">
            <wire x2="560" y1="176" y2="336" x1="560" />
            <wire x2="704" y1="336" y2="336" x1="560" />
            <wire x2="1136" y1="176" y2="176" x1="560" />
            <wire x2="1296" y1="176" y2="176" x1="1136" />
            <wire x2="1136" y1="176" y2="624" x1="1136" />
            <wire x2="1456" y1="624" y2="624" x1="1136" />
        </branch>
        <iomarker fontsize="28" x="288" y="880" name="C" orien="R180" />
        <branch name="XLXN_9">
            <wire x2="672" y1="1104" y2="1168" x1="672" />
            <wire x2="1008" y1="1104" y2="1104" x1="672" />
            <wire x2="1008" y1="992" y2="992" x1="928" />
            <wire x2="1008" y1="992" y2="1104" x1="1008" />
            <wire x2="1216" y1="992" y2="992" x1="1008" />
            <wire x2="1216" y1="992" y2="1040" x1="1216" />
            <wire x2="1440" y1="1040" y2="1040" x1="1216" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="704" y1="400" y2="400" x1="464" />
            <wire x2="464" y1="400" y2="1392" x1="464" />
            <wire x2="960" y1="1392" y2="1392" x1="464" />
            <wire x2="672" y1="1056" y2="1072" x1="672" />
            <wire x2="960" y1="1072" y2="1072" x1="672" />
            <wire x2="960" y1="1072" y2="1232" x1="960" />
            <wire x2="960" y1="1232" y2="1392" x1="960" />
            <wire x2="960" y1="1232" y2="1232" x1="928" />
        </branch>
        <branch name="C">
            <wire x2="560" y1="880" y2="880" x1="288" />
            <wire x2="560" y1="880" y2="992" x1="560" />
            <wire x2="672" y1="992" y2="992" x1="560" />
            <wire x2="560" y1="784" y2="880" x1="560" />
            <wire x2="688" y1="784" y2="784" x1="560" />
        </branch>
        <branch name="XLXN_4">
            <wire x2="1040" y1="576" y2="576" x1="688" />
            <wire x2="688" y1="576" y2="656" x1="688" />
            <wire x2="976" y1="400" y2="400" x1="960" />
            <wire x2="1040" y1="400" y2="400" x1="976" />
            <wire x2="1040" y1="400" y2="576" x1="1040" />
        </branch>
        <instance x="688" y="848" name="XLXI_2" orien="R0" />
        <branch name="Rbar">
            <wire x2="640" y1="720" y2="1232" x1="640" />
            <wire x2="640" y1="1232" y2="1440" x1="640" />
            <wire x2="1168" y1="1440" y2="1440" x1="640" />
            <wire x2="1280" y1="1440" y2="1440" x1="1168" />
            <wire x2="672" y1="1232" y2="1232" x1="640" />
            <wire x2="688" y1="720" y2="720" x1="640" />
            <wire x2="1440" y1="1104" y2="1104" x1="1168" />
            <wire x2="1168" y1="1104" y2="1440" x1="1168" />
        </branch>
        <branch name="Q">
            <wire x2="1440" y1="832" y2="976" x1="1440" />
            <wire x2="1792" y1="832" y2="832" x1="1440" />
            <wire x2="1792" y1="688" y2="688" x1="1712" />
            <wire x2="1856" y1="688" y2="688" x1="1792" />
            <wire x2="1792" y1="688" y2="832" x1="1792" />
        </branch>
        <branch name="Qbar">
            <wire x2="1456" y1="752" y2="896" x1="1456" />
            <wire x2="1792" y1="896" y2="896" x1="1456" />
            <wire x2="1792" y1="896" y2="1040" x1="1792" />
            <wire x2="1872" y1="1040" y2="1040" x1="1792" />
            <wire x2="1792" y1="1040" y2="1040" x1="1696" />
        </branch>
        <iomarker fontsize="28" x="1856" y="688" name="Q" orien="R0" />
        <iomarker fontsize="28" x="1872" y="1040" name="Qbar" orien="R0" />
        <iomarker fontsize="28" x="1296" y="176" name="Sbar" orien="R0" />
        <iomarker fontsize="28" x="1280" y="1440" name="Rbar" orien="R0" />
        <branch name="D">
            <wire x2="656" y1="1296" y2="1296" x1="288" />
            <wire x2="672" y1="1296" y2="1296" x1="656" />
        </branch>
        <iomarker fontsize="28" x="288" y="1296" name="D" orien="R180" />
    </sheet>
</drawing>