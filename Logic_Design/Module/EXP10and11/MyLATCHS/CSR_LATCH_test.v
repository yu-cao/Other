// Verilog test fixture created from schematic E:\Logic_Design\exp10\MyLATCHS\CSR_LATCH.sch - Wed Dec 06 13:13:51 2017

`timescale 1ns / 1ps

module CSR_LATCH_CSR_LATCH_sch_tb();

// Inputs
   reg S;
   reg R;
   reg C;

// Output
   wire Q;
   wire Qbar;

// Bidirs

// Instantiate the UUT
   CSR_LATCH UUT (
		.S(S), 
		.R(R), 
		.C(C), 
		.Q(Q), 
		.Qbar(Qbar)
   );
// Initialize Inputs

       initial begin
		C=1;R=1;S=1; #50;
		R=1;S=0; #50;
		R=1;S=1; #50;
		R=0;S=1; #50;
		R=1;S=1; #50;
		R=0;S=0; #50;
		R=1;S=1; #50;	 
		C=0;R=1;S=1; #50;
		R=1;S=0; #50;
		R=1;S=1; #50;
		R=0;S=1; #50;
		R=1;S=1; #50;
		R=0;S=0; #50;
		R=1;S=1; #50;
		end
endmodule
