// Verilog test fixture created from schematic E:\Logic_Design\exp10\MyLATCHS\D_FLIPFLOP.sch - Wed Dec 06 14:12:52 2017

`timescale 1ns / 1ps

module D_FLIPFLOP_D_FLIPFLOP_sch_tb();

// Inputs
   reg C;
   reg Sbar;
   reg Rbar;
   reg D;

// Output
   wire Q;
   wire Qbar;

// Bidirs

// Instantiate the UUT
   D_FLIPFLOP UUT (
		.C(C), 
		.Sbar(Sbar), 
		.Rbar(Rbar), 
		.Q(Q), 
		.Qbar(Qbar), 
		.D(D)
   );
// Initialize Inputs

initial begin
	Sbar = 1;
	Rbar = 1;		
	D = 0; #150;
	D = 1; #150;

end

always begin
	C=0; #50;
	C=1; #50;
end

		
		
endmodule
