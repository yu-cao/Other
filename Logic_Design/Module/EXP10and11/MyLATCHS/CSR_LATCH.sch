<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="S" />
        <signal name="R" />
        <signal name="C" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="Q" />
        <signal name="Qbar" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="R" />
        <port polarity="Input" name="C" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qbar" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_1">
            <blockpin signalname="C" name="I0" />
            <blockpin signalname="S" name="I1" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_3">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="C" name="I1" />
            <blockpin signalname="XLXN_6" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_4">
            <blockpin signalname="Qbar" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_5">
            <blockpin signalname="XLXN_6" name="I0" />
            <blockpin signalname="Q" name="I1" />
            <blockpin signalname="Qbar" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="896" y="736" name="XLXI_1" orien="R0" />
        <instance x="912" y="1056" name="XLXI_3" orien="R0" />
        <instance x="1360" y="800" name="XLXI_4" orien="R0" />
        <instance x="1360" y="1008" name="XLXI_5" orien="R0" />
        <branch name="S">
            <wire x2="896" y1="608" y2="608" x1="864" />
        </branch>
        <iomarker fontsize="28" x="864" y="608" name="S" orien="R180" />
        <branch name="R">
            <wire x2="912" y1="992" y2="992" x1="880" />
        </branch>
        <iomarker fontsize="28" x="880" y="992" name="R" orien="R180" />
        <branch name="C">
            <wire x2="864" y1="784" y2="784" x1="800" />
            <wire x2="864" y1="784" y2="928" x1="864" />
            <wire x2="912" y1="928" y2="928" x1="864" />
            <wire x2="896" y1="672" y2="672" x1="864" />
            <wire x2="864" y1="672" y2="784" x1="864" />
        </branch>
        <iomarker fontsize="28" x="800" y="784" name="C" orien="R180" />
        <branch name="XLXN_4">
            <wire x2="1248" y1="640" y2="640" x1="1152" />
            <wire x2="1248" y1="640" y2="672" x1="1248" />
            <wire x2="1360" y1="672" y2="672" x1="1248" />
        </branch>
        <branch name="XLXN_6">
            <wire x2="1184" y1="960" y2="960" x1="1168" />
            <wire x2="1360" y1="944" y2="944" x1="1184" />
            <wire x2="1184" y1="944" y2="960" x1="1184" />
        </branch>
        <branch name="Q">
            <wire x2="1680" y1="816" y2="816" x1="1296" />
            <wire x2="1296" y1="816" y2="880" x1="1296" />
            <wire x2="1360" y1="880" y2="880" x1="1296" />
            <wire x2="1680" y1="704" y2="704" x1="1616" />
            <wire x2="1680" y1="704" y2="816" x1="1680" />
            <wire x2="1744" y1="704" y2="704" x1="1680" />
        </branch>
        <branch name="Qbar">
            <wire x2="1360" y1="736" y2="736" x1="1328" />
            <wire x2="1328" y1="736" y2="832" x1="1328" />
            <wire x2="1648" y1="832" y2="832" x1="1328" />
            <wire x2="1648" y1="832" y2="912" x1="1648" />
            <wire x2="1744" y1="912" y2="912" x1="1648" />
            <wire x2="1648" y1="912" y2="912" x1="1616" />
        </branch>
        <iomarker fontsize="28" x="1744" y="704" name="Q" orien="R0" />
        <iomarker fontsize="28" x="1744" y="912" name="Qbar" orien="R0" />
    </sheet>
</drawing>