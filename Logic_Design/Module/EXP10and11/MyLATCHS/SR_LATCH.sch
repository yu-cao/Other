<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="S" />
        <signal name="R" />
        <signal name="Q" />
        <signal name="Qbar" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <port polarity="Input" name="S" />
        <port polarity="Input" name="R" />
        <port polarity="Output" name="Q" />
        <port polarity="Output" name="Qbar" />
        <blockdef name="nand2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
        </blockdef>
        <block symbolname="nand2" name="XLXI_1">
            <blockpin signalname="Qbar" name="I0" />
            <blockpin signalname="S" name="I1" />
            <blockpin signalname="Q" name="O" />
        </block>
        <block symbolname="nand2" name="XLXI_2">
            <blockpin signalname="R" name="I0" />
            <blockpin signalname="Q" name="I1" />
            <blockpin signalname="Qbar" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1008" y="736" name="XLXI_1" orien="R0" />
        <instance x="1008" y="976" name="XLXI_2" orien="R0" />
        <branch name="S">
            <wire x2="1008" y1="608" y2="608" x1="880" />
        </branch>
        <iomarker fontsize="28" x="880" y="608" name="S" orien="R180" />
        <branch name="R">
            <wire x2="1008" y1="912" y2="912" x1="880" />
        </branch>
        <iomarker fontsize="28" x="880" y="912" name="R" orien="R180" />
        <branch name="Q">
            <wire x2="1312" y1="800" y2="800" x1="928" />
            <wire x2="928" y1="800" y2="848" x1="928" />
            <wire x2="1008" y1="848" y2="848" x1="928" />
            <wire x2="1312" y1="640" y2="640" x1="1264" />
            <wire x2="1360" y1="640" y2="640" x1="1312" />
            <wire x2="1312" y1="640" y2="800" x1="1312" />
        </branch>
        <branch name="Qbar">
            <wire x2="1008" y1="672" y2="672" x1="944" />
            <wire x2="944" y1="672" y2="752" x1="944" />
            <wire x2="1296" y1="752" y2="752" x1="944" />
            <wire x2="1296" y1="752" y2="880" x1="1296" />
            <wire x2="1360" y1="880" y2="880" x1="1296" />
            <wire x2="1296" y1="880" y2="880" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="1360" y="640" name="Q" orien="R0" />
        <iomarker fontsize="28" x="1360" y="880" name="Qbar" orien="R0" />
    </sheet>
</drawing>