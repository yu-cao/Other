`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:28:55 12/16/2017
// Design Name:   revcounter
// Module Name:   E:/Logic_Design/exp10_11/Final/MyCounter/MyCounter/test_rev.v
// Project Name:  MyCounter
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: revcounter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module test_rev;

	// Inputs
	reg clk;
	reg s;

	// Outputs
	wire [15:0] cnt;
	wire Rc;

	// Instantiate the Unit Under Test (UUT)
	revcounter uut (
		.clk(clk), 
		.s(s), 
		.cnt(cnt), 
		.Rc(Rc)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		s = 1;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
forever begin
	clk = 1'b0; #50;
	clk = 1'b1; #50;
end

	end
      
endmodule

