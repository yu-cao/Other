`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:15:42 12/13/2017 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
   input wire clk, 
   input wire sw,
	input wire [3:0] btn,
	output wire [3:0] AN,
	output wire [7:0] SEGMENT,
    output wire LED,
	output wire Buzzer
    );
wire clk_s;
wire [15:0] q;


    clk_1s(clk,clk_s);
    revcounter(clk_s,sw,q,LED);
    DispNum(clk, q, btn, 4'b0,1'b0, AN, SEGMENT);
    
    assign Buzzer=1;
endmodule