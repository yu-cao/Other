`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:16:50 12/13/2017 
// Design Name: 
// Module Name:    revcounter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module revcounter(
clk,s,cnt,Rc
    );
input wire clk,s;
output wire Rc;
output reg [15:0] cnt;
initial begin cnt=0; end
assign Rc = (~s & (~|cnt)) | (s & (&cnt));
always @ (posedge clk) begin
if(s)
cnt <= cnt+1;
else
cnt<=cnt-1;
end

endmodule