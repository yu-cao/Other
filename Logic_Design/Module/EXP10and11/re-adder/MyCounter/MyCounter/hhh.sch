<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Scan(1:0)" />
        <signal name="Hexs(15:0)" />
        <signal name="Hexs(3:0)" />
        <signal name="Hexs(7:4)" />
        <signal name="Hexs(11:8)" />
        <signal name="Hexs(15:12)" />
        <signal name="Scan(1)" />
        <signal name="Scan(0)" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="AN(3:0)" />
        <signal name="AN(3)" />
        <signal name="AN(2)" />
        <signal name="AN(1)" />
        <signal name="AN(0)" />
        <signal name="point(3:0)" />
        <signal name="LES(3:0)" />
        <signal name="point(3)" />
        <signal name="point(2)" />
        <signal name="point(1)" />
        <signal name="point(0)" />
        <signal name="LES(3)" />
        <signal name="LES(2)" />
        <signal name="LES(1)" />
        <signal name="LES(0)" />
        <signal name="p" />
        <signal name="LE" />
        <signal name="HEX(3:0)" />
        <port polarity="Input" name="Scan(1:0)" />
        <port polarity="Input" name="Hexs(15:0)" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Input" name="point(3:0)" />
        <port polarity="Input" name="LES(3:0)" />
        <port polarity="Output" name="p" />
        <port polarity="Output" name="LE" />
        <port polarity="Output" name="HEX(3:0)" />
        <blockdef name="Mux4to14b">
            <timestamp>2017-11-9T5:26:20</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="d2_4e">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-384" height="320" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="320" y1="-128" y2="-128" x1="384" />
            <line x2="320" y1="-192" y2="-192" x1="384" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="320" y1="-320" y2="-320" x1="384" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="inv4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="160" y1="-96" y2="-96" x1="224" />
            <line x2="160" y1="-160" y2="-160" x1="224" />
            <line x2="160" y1="-224" y2="-224" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-96" y2="-96" x1="0" />
            <line x2="64" y1="-160" y2="-160" x1="0" />
            <line x2="64" y1="-224" y2="-224" x1="0" />
            <line x2="128" y1="-256" y2="-224" x1="64" />
            <line x2="64" y1="-224" y2="-192" x1="128" />
            <line x2="64" y1="-192" y2="-256" x1="64" />
            <circle r="16" cx="144" cy="-32" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <line x2="128" y1="-128" y2="-96" x1="64" />
            <line x2="64" y1="-96" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="-128" x1="64" />
            <circle r="16" cx="144" cy="-96" />
            <line x2="128" y1="-192" y2="-160" x1="64" />
            <line x2="64" y1="-160" y2="-128" x1="128" />
            <line x2="64" y1="-128" y2="-192" x1="64" />
            <circle r="16" cx="144" cy="-160" />
            <circle r="16" cx="144" cy="-224" />
        </blockdef>
        <blockdef name="MUX4TO1">
            <timestamp>2017-11-9T8:3:10</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <block symbolname="Mux4to14b" name="XLXI_1">
            <blockpin signalname="Scan(1:0)" name="s(1:0)" />
            <blockpin signalname="Hexs(3:0)" name="I0(3:0)" />
            <blockpin signalname="Hexs(7:4)" name="I1(3:0)" />
            <blockpin signalname="Hexs(11:8)" name="I2(3:0)" />
            <blockpin signalname="Hexs(15:12)" name="I3(3:0)" />
            <blockpin signalname="HEX(3:0)" name="o(3:0)" />
        </block>
        <block symbolname="d2_4e" name="XLXI_3">
            <blockpin signalname="Scan(0)" name="A0" />
            <blockpin signalname="Scan(1)" name="A1" />
            <blockpin signalname="XLXN_28" name="E" />
            <blockpin signalname="XLXN_32" name="D0" />
            <blockpin signalname="XLXN_31" name="D1" />
            <blockpin signalname="XLXN_30" name="D2" />
            <blockpin signalname="XLXN_29" name="D3" />
        </block>
        <block symbolname="vcc" name="XLXI_4">
            <blockpin signalname="XLXN_28" name="P" />
        </block>
        <block symbolname="inv4" name="XLXI_5">
            <blockpin signalname="XLXN_29" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_31" name="I2" />
            <blockpin signalname="XLXN_32" name="I3" />
            <blockpin signalname="AN(3)" name="O0" />
            <blockpin signalname="AN(2)" name="O1" />
            <blockpin signalname="AN(1)" name="O2" />
            <blockpin signalname="AN(0)" name="O3" />
        </block>
        <block symbolname="MUX4TO1" name="XLXI_6">
            <blockpin signalname="Scan(1:0)" name="s(1:0)" />
            <blockpin signalname="point(0)" name="I0" />
            <blockpin signalname="point(1)" name="I1" />
            <blockpin signalname="point(2)" name="I2" />
            <blockpin signalname="point(3)" name="I3" />
            <blockpin signalname="p" name="o" />
        </block>
        <block symbolname="MUX4TO1" name="XLXI_7">
            <blockpin signalname="Scan(1:0)" name="s(1:0)" />
            <blockpin signalname="LES(0)" name="I0" />
            <blockpin signalname="LES(1)" name="I1" />
            <blockpin signalname="LES(2)" name="I2" />
            <blockpin signalname="LES(3)" name="I3" />
            <blockpin signalname="LE" name="o" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="2048" y="864" name="XLXI_1" orien="R0">
        </instance>
        <branch name="Scan(1:0)">
            <wire x2="1504" y1="256" y2="256" x1="864" />
            <wire x2="1504" y1="256" y2="576" x1="1504" />
            <wire x2="2048" y1="576" y2="576" x1="1504" />
            <wire x2="1504" y1="576" y2="1232" x1="1504" />
            <wire x2="1504" y1="1232" y2="1312" x1="1504" />
            <wire x2="1504" y1="1312" y2="1648" x1="1504" />
            <wire x2="1760" y1="1648" y2="1648" x1="1504" />
            <wire x2="1504" y1="1648" y2="2240" x1="1504" />
            <wire x2="1504" y1="2240" y2="2528" x1="1504" />
            <wire x2="1760" y1="2240" y2="2240" x1="1504" />
        </branch>
        <iomarker fontsize="28" x="2656" y="576" name="HEX(3:0)" orien="R0" />
        <iomarker fontsize="28" x="864" y="256" name="Scan(1:0)" orien="R180" />
        <iomarker fontsize="28" x="736" y="832" name="Hexs(15:0)" orien="R180" />
        <branch name="Hexs(15:0)">
            <wire x2="912" y1="832" y2="832" x1="736" />
            <wire x2="912" y1="832" y2="896" x1="912" />
            <wire x2="912" y1="896" y2="976" x1="912" />
            <wire x2="912" y1="976" y2="1056" x1="912" />
            <wire x2="912" y1="640" y2="704" x1="912" />
            <wire x2="912" y1="704" y2="784" x1="912" />
            <wire x2="912" y1="784" y2="832" x1="912" />
        </branch>
        <bustap x2="1008" y1="704" y2="704" x1="912" />
        <bustap x2="1008" y1="976" y2="976" x1="912" />
        <bustap x2="1008" y1="896" y2="896" x1="912" />
        <bustap x2="1008" y1="784" y2="784" x1="912" />
        <branch name="Hexs(3:0)">
            <wire x2="1520" y1="704" y2="704" x1="1008" />
            <wire x2="1520" y1="640" y2="704" x1="1520" />
            <wire x2="2048" y1="640" y2="640" x1="1520" />
        </branch>
        <branch name="Hexs(7:4)">
            <wire x2="1536" y1="784" y2="784" x1="1008" />
            <wire x2="1536" y1="704" y2="784" x1="1536" />
            <wire x2="2048" y1="704" y2="704" x1="1536" />
        </branch>
        <branch name="Hexs(11:8)">
            <wire x2="1552" y1="896" y2="896" x1="1008" />
            <wire x2="1552" y1="768" y2="896" x1="1552" />
            <wire x2="2048" y1="768" y2="768" x1="1552" />
        </branch>
        <branch name="Hexs(15:12)">
            <wire x2="1568" y1="976" y2="976" x1="1008" />
            <wire x2="1568" y1="832" y2="976" x1="1568" />
            <wire x2="2048" y1="832" y2="832" x1="1568" />
        </branch>
        <instance x="1776" y="1552" name="XLXI_3" orien="R0" />
        <bustap x2="1600" y1="1312" y2="1312" x1="1504" />
        <branch name="Scan(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1616" y="1312" type="branch" />
            <wire x2="1616" y1="1312" y2="1312" x1="1600" />
            <wire x2="1632" y1="1312" y2="1312" x1="1616" />
            <wire x2="1776" y1="1296" y2="1296" x1="1632" />
            <wire x2="1632" y1="1296" y2="1312" x1="1632" />
        </branch>
        <bustap x2="1600" y1="1232" y2="1232" x1="1504" />
        <branch name="Scan(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1616" y="1232" type="branch" />
            <wire x2="1616" y1="1232" y2="1232" x1="1600" />
            <wire x2="1776" y1="1232" y2="1232" x1="1616" />
        </branch>
        <instance x="1632" y="1424" name="XLXI_4" orien="R0" />
        <branch name="XLXN_28">
            <wire x2="1696" y1="1424" y2="1488" x1="1696" />
            <wire x2="1760" y1="1488" y2="1488" x1="1696" />
            <wire x2="1760" y1="1424" y2="1488" x1="1760" />
            <wire x2="1776" y1="1424" y2="1424" x1="1760" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="2192" y1="1424" y2="1424" x1="2160" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="2192" y1="1360" y2="1360" x1="2160" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="2192" y1="1296" y2="1296" x1="2160" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="2192" y1="1232" y2="1232" x1="2160" />
        </branch>
        <instance x="2192" y="1456" name="XLXI_5" orien="R0" />
        <branch name="AN(3:0)">
            <wire x2="2576" y1="1008" y2="1216" x1="2576" />
            <wire x2="2576" y1="1216" y2="1280" x1="2576" />
            <wire x2="2576" y1="1280" y2="1360" x1="2576" />
            <wire x2="2576" y1="1360" y2="1424" x1="2576" />
            <wire x2="2576" y1="1424" y2="1648" x1="2576" />
            <wire x2="2832" y1="1648" y2="1648" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="2832" y="1648" name="AN(3:0)" orien="R0" />
        <bustap x2="2480" y1="1424" y2="1424" x1="2576" />
        <branch name="AN(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="1424" type="branch" />
            <wire x2="2464" y1="1424" y2="1424" x1="2416" />
            <wire x2="2480" y1="1424" y2="1424" x1="2464" />
        </branch>
        <bustap x2="2480" y1="1360" y2="1360" x1="2576" />
        <branch name="AN(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="1360" type="branch" />
            <wire x2="2464" y1="1360" y2="1360" x1="2416" />
            <wire x2="2480" y1="1360" y2="1360" x1="2464" />
        </branch>
        <bustap x2="2480" y1="1280" y2="1280" x1="2576" />
        <branch name="AN(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2464" y="1280" type="branch" />
            <wire x2="2448" y1="1296" y2="1296" x1="2416" />
            <wire x2="2464" y1="1280" y2="1280" x1="2448" />
            <wire x2="2480" y1="1280" y2="1280" x1="2464" />
            <wire x2="2448" y1="1280" y2="1296" x1="2448" />
        </branch>
        <bustap x2="2480" y1="1216" y2="1216" x1="2576" />
        <branch name="AN(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2472" y="1216" type="branch" />
            <wire x2="2464" y1="1232" y2="1232" x1="2416" />
            <wire x2="2480" y1="1216" y2="1216" x1="2464" />
            <wire x2="2464" y1="1216" y2="1232" x1="2464" />
        </branch>
        <instance x="1760" y="1936" name="XLXI_6" orien="R0">
        </instance>
        <instance x="1760" y="2528" name="XLXI_7" orien="R0">
        </instance>
        <branch name="point(3:0)">
            <wire x2="960" y1="1616" y2="1616" x1="832" />
            <wire x2="960" y1="1616" y2="1696" x1="960" />
            <wire x2="960" y1="1696" y2="1776" x1="960" />
            <wire x2="960" y1="1776" y2="1856" x1="960" />
            <wire x2="960" y1="1856" y2="1920" x1="960" />
            <wire x2="960" y1="1920" y2="2016" x1="960" />
        </branch>
        <branch name="LES(3:0)">
            <wire x2="928" y1="2224" y2="2224" x1="784" />
            <wire x2="928" y1="2224" y2="2320" x1="928" />
            <wire x2="928" y1="2320" y2="2384" x1="928" />
            <wire x2="928" y1="2384" y2="2448" x1="928" />
            <wire x2="928" y1="2448" y2="2496" x1="928" />
            <wire x2="928" y1="2496" y2="2624" x1="928" />
        </branch>
        <iomarker fontsize="28" x="832" y="1616" name="point(3:0)" orien="R180" />
        <iomarker fontsize="28" x="784" y="2224" name="LES(3:0)" orien="R180" />
        <bustap x2="1056" y1="1920" y2="1920" x1="960" />
        <branch name="point(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1920" type="branch" />
            <wire x2="1072" y1="1920" y2="1920" x1="1056" />
            <wire x2="1264" y1="1920" y2="1920" x1="1072" />
            <wire x2="1760" y1="1904" y2="1904" x1="1264" />
            <wire x2="1264" y1="1904" y2="1920" x1="1264" />
        </branch>
        <bustap x2="1056" y1="1856" y2="1856" x1="960" />
        <branch name="point(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1856" type="branch" />
            <wire x2="1072" y1="1856" y2="1856" x1="1056" />
            <wire x2="1280" y1="1856" y2="1856" x1="1072" />
            <wire x2="1760" y1="1840" y2="1840" x1="1280" />
            <wire x2="1280" y1="1840" y2="1856" x1="1280" />
        </branch>
        <bustap x2="1056" y1="1776" y2="1776" x1="960" />
        <branch name="point(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1080" y="1776" type="branch" />
            <wire x2="1760" y1="1776" y2="1776" x1="1056" />
        </branch>
        <bustap x2="1056" y1="1696" y2="1696" x1="960" />
        <branch name="point(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1072" y="1696" type="branch" />
            <wire x2="1072" y1="1696" y2="1696" x1="1056" />
            <wire x2="1296" y1="1696" y2="1696" x1="1072" />
            <wire x2="1296" y1="1696" y2="1712" x1="1296" />
            <wire x2="1760" y1="1712" y2="1712" x1="1296" />
        </branch>
        <bustap x2="1024" y1="2496" y2="2496" x1="928" />
        <branch name="LES(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1040" y="2496" type="branch" />
            <wire x2="1040" y1="2496" y2="2496" x1="1024" />
            <wire x2="1760" y1="2496" y2="2496" x1="1040" />
        </branch>
        <bustap x2="1024" y1="2448" y2="2448" x1="928" />
        <branch name="LES(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1040" y="2448" type="branch" />
            <wire x2="1040" y1="2448" y2="2448" x1="1024" />
            <wire x2="1280" y1="2448" y2="2448" x1="1040" />
            <wire x2="1760" y1="2432" y2="2432" x1="1280" />
            <wire x2="1280" y1="2432" y2="2448" x1="1280" />
        </branch>
        <bustap x2="1024" y1="2384" y2="2384" x1="928" />
        <bustap x2="1024" y1="2320" y2="2320" x1="928" />
        <branch name="LES(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1048" y="2320" type="branch" />
            <wire x2="1248" y1="2320" y2="2320" x1="1024" />
            <wire x2="1760" y1="2304" y2="2304" x1="1248" />
            <wire x2="1248" y1="2304" y2="2320" x1="1248" />
        </branch>
        <branch name="LES(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1048" y="2384" type="branch" />
            <wire x2="1264" y1="2384" y2="2384" x1="1024" />
            <wire x2="1264" y1="2368" y2="2384" x1="1264" />
            <wire x2="1760" y1="2368" y2="2368" x1="1264" />
        </branch>
        <branch name="p">
            <wire x2="2176" y1="1648" y2="1648" x1="2144" />
        </branch>
        <iomarker fontsize="28" x="2176" y="1648" name="p" orien="R0" />
        <branch name="LE">
            <wire x2="2176" y1="2240" y2="2240" x1="2144" />
        </branch>
        <iomarker fontsize="28" x="2176" y="2240" name="LE" orien="R0" />
        <branch name="HEX(3:0)">
            <wire x2="2496" y1="576" y2="576" x1="2432" />
            <wire x2="2656" y1="576" y2="576" x1="2496" />
        </branch>
    </sheet>
</drawing>