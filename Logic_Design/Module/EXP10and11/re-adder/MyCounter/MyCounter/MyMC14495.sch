<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="LE" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="XLXN_9" />
        <signal name="XLXN_10" />
        <signal name="XLXN_11" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="XLXN_35" />
        <signal name="XLXN_36" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_42" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_51" />
        <signal name="XLXN_53" />
        <signal name="XLXN_55" />
        <signal name="XLXN_56" />
        <signal name="XLXN_57" />
        <signal name="XLXN_58" />
        <signal name="XLXN_59" />
        <signal name="XLXN_62" />
        <signal name="XLXN_63" />
        <signal name="XLXN_66" />
        <signal name="D2" />
        <signal name="XLXN_70" />
        <signal name="D3" />
        <signal name="XLXN_71" />
        <signal name="D0" />
        <signal name="XLXN_73" />
        <signal name="XLXN_74" />
        <signal name="XLXN_75" />
        <signal name="XLXN_77" />
        <signal name="D1" />
        <signal name="XLXN_82" />
        <signal name="XLXN_83" />
        <signal name="XLXN_84" />
        <signal name="XLXN_85" />
        <signal name="XLXN_86" />
        <signal name="XLXN_87" />
        <signal name="XLXN_88" />
        <signal name="XLXN_89" />
        <signal name="XLXN_91" />
        <signal name="XLXN_92" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="XLXN_96" />
        <signal name="XLXN_98" />
        <signal name="g" />
        <signal name="f" />
        <signal name="e" />
        <signal name="d" />
        <signal name="c" />
        <signal name="b" />
        <signal name="a" />
        <signal name="XLXN_106" />
        <signal name="point" />
        <signal name="p" />
        <signal name="XLXN_109" />
        <port polarity="Input" name="LE" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D3" />
        <port polarity="Input" name="D0" />
        <port polarity="Input" name="D1" />
        <port polarity="Output" name="g" />
        <port polarity="Output" name="f" />
        <port polarity="Output" name="e" />
        <port polarity="Output" name="d" />
        <port polarity="Output" name="c" />
        <port polarity="Output" name="b" />
        <port polarity="Output" name="a" />
        <port polarity="Input" name="point" />
        <port polarity="Output" name="p" />
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="or2" name="XLXI_1">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_13" name="I1" />
            <blockpin signalname="a" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_2">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_12" name="I1" />
            <blockpin signalname="b" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_3">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_11" name="I1" />
            <blockpin signalname="c" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_4">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_10" name="I1" />
            <blockpin signalname="d" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_9" name="I1" />
            <blockpin signalname="e" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_6">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="f" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_7">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="g" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_8">
            <blockpin signalname="XLXN_16" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_18" name="I2" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_9">
            <blockpin signalname="XLXN_24" name="I0" />
            <blockpin signalname="XLXN_25" name="I1" />
            <blockpin signalname="XLXN_27" name="I2" />
            <blockpin signalname="XLXN_9" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_10">
            <blockpin signalname="XLXN_31" name="I0" />
            <blockpin signalname="XLXN_32" name="I1" />
            <blockpin signalname="XLXN_34" name="I2" />
            <blockpin signalname="XLXN_11" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_11">
            <blockpin signalname="XLXN_21" name="I0" />
            <blockpin signalname="XLXN_22" name="I1" />
            <blockpin signalname="XLXN_23" name="I2" />
            <blockpin signalname="XLXN_39" name="I3" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_12">
            <blockpin signalname="XLXN_28" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_46" name="I3" />
            <blockpin signalname="XLXN_10" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_13">
            <blockpin signalname="XLXN_33" name="I0" />
            <blockpin signalname="XLXN_34" name="I1" />
            <blockpin signalname="XLXN_35" name="I2" />
            <blockpin signalname="XLXN_36" name="I3" />
            <blockpin signalname="XLXN_12" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_14">
            <blockpin signalname="XLXN_38" name="I0" />
            <blockpin signalname="XLXN_39" name="I1" />
            <blockpin signalname="XLXN_40" name="I2" />
            <blockpin signalname="XLXN_46" name="I3" />
            <blockpin signalname="XLXN_13" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_15">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="XLXN_98" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_16">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_85" name="I3" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_17">
            <blockpin signalname="XLXN_98" name="I0" />
            <blockpin signalname="XLXN_91" name="I1" />
            <blockpin signalname="XLXN_85" name="I2" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_18">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_85" name="I2" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_19">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="XLXN_91" name="I1" />
            <blockpin signalname="XLXN_85" name="I2" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_20">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_91" name="I1" />
            <blockpin signalname="XLXN_85" name="I2" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_21">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_98" name="I1" />
            <blockpin signalname="XLXN_91" name="I2" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_22">
            <blockpin signalname="XLXN_98" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="XLXN_85" name="I2" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_23">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_85" name="I1" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_24">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_91" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_28" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_25">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_26">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_31" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_27">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_91" name="I2" />
            <blockpin signalname="XLXN_85" name="I3" />
            <blockpin signalname="XLXN_32" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_28">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_33" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_29">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_34" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_30">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_35" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_31">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_98" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_85" name="I3" />
            <blockpin signalname="XLXN_36" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_32">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_91" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_38" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_33">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_98" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_39" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_34">
            <blockpin signalname="XLXN_55" name="I0" />
            <blockpin signalname="XLXN_98" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_85" name="I3" />
            <blockpin signalname="XLXN_40" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_35">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_91" name="I1" />
            <blockpin signalname="XLXN_98" name="I2" />
            <blockpin signalname="XLXN_85" name="I3" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_36">
            <blockpin signalname="D0" name="I" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_37">
            <blockpin signalname="D1" name="I" />
            <blockpin signalname="XLXN_98" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_38">
            <blockpin signalname="D2" name="I" />
            <blockpin signalname="XLXN_91" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_39">
            <blockpin signalname="D3" name="I" />
            <blockpin signalname="XLXN_85" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_41">
            <blockpin signalname="point" name="I" />
            <blockpin signalname="p" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="4256" y="2912" name="XLXI_1" orien="R90" />
        <instance x="3456" y="2912" name="XLXI_2" orien="R90" />
        <instance x="2800" y="2896" name="XLXI_3" orien="R90" />
        <instance x="2176" y="2896" name="XLXI_4" orien="R90" />
        <instance x="1648" y="2896" name="XLXI_5" orien="R90" />
        <instance x="1088" y="2912" name="XLXI_6" orien="R90" />
        <instance x="640" y="2912" name="XLXI_7" orien="R90" />
        <instance x="624" y="2272" name="XLXI_8" orien="R90" />
        <instance x="1664" y="2288" name="XLXI_9" orien="R90" />
        <instance x="2752" y="2288" name="XLXI_10" orien="R90" />
        <instance x="1040" y="2304" name="XLXI_11" orien="R90" />
        <instance x="2176" y="2288" name="XLXI_12" orien="R90" />
        <instance x="3312" y="2288" name="XLXI_13" orien="R90" />
        <instance x="4080" y="2304" name="XLXI_14" orien="R90" />
        <instance x="240" y="1792" name="XLXI_15" orien="R90" />
        <instance x="496" y="1792" name="XLXI_16" orien="R90" />
        <instance x="752" y="1808" name="XLXI_17" orien="R90" />
        <instance x="944" y="1808" name="XLXI_18" orien="R90" />
        <instance x="1152" y="1808" name="XLXI_19" orien="R90" />
        <instance x="1360" y="1808" name="XLXI_20" orien="R90" />
        <instance x="1552" y="1824" name="XLXI_21" orien="R90" />
        <instance x="1728" y="1808" name="XLXI_22" orien="R90" />
        <instance x="1904" y="1824" name="XLXI_23" orien="R90" />
        <instance x="2080" y="1824" name="XLXI_24" orien="R90" />
        <instance x="2352" y="1840" name="XLXI_25" orien="R90" />
        <instance x="2512" y="1840" name="XLXI_26" orien="R90" />
        <instance x="2704" y="1840" name="XLXI_27" orien="R90" />
        <instance x="2976" y="1856" name="XLXI_28" orien="R90" />
        <instance x="3168" y="1872" name="XLXI_29" orien="R90" />
        <instance x="3376" y="1856" name="XLXI_30" orien="R90" />
        <instance x="3600" y="1888" name="XLXI_31" orien="R90" />
        <instance x="3872" y="1888" name="XLXI_32" orien="R90" />
        <instance x="4176" y="1872" name="XLXI_33" orien="R90" />
        <instance x="4496" y="1856" name="XLXI_34" orien="R90" />
        <instance x="4800" y="1888" name="XLXI_35" orien="R90" />
        <instance x="4080" y="480" name="XLXI_36" orien="R90" />
        <instance x="4352" y="496" name="XLXI_37" orien="R90" />
        <instance x="4608" y="512" name="XLXI_38" orien="R90" />
        <instance x="4880" y="528" name="XLXI_39" orien="R90" />
        <branch name="LE">
            <wire x2="208" y1="368" y2="2800" x1="208" />
            <wire x2="704" y1="2800" y2="2800" x1="208" />
            <wire x2="704" y1="2800" y2="2912" x1="704" />
            <wire x2="1152" y1="2800" y2="2800" x1="704" />
            <wire x2="1152" y1="2800" y2="2912" x1="1152" />
            <wire x2="1712" y1="2800" y2="2800" x1="1152" />
            <wire x2="1712" y1="2800" y2="2896" x1="1712" />
            <wire x2="2240" y1="2800" y2="2800" x1="1712" />
            <wire x2="2240" y1="2800" y2="2896" x1="2240" />
            <wire x2="2864" y1="2800" y2="2800" x1="2240" />
            <wire x2="2864" y1="2800" y2="2896" x1="2864" />
            <wire x2="3520" y1="2800" y2="2800" x1="2864" />
            <wire x2="4368" y1="2800" y2="2800" x1="3520" />
            <wire x2="4368" y1="2800" y2="2816" x1="4368" />
            <wire x2="3520" y1="2800" y2="2912" x1="3520" />
            <wire x2="4368" y1="2816" y2="2816" x1="4320" />
            <wire x2="4320" y1="2816" y2="2912" x1="4320" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="752" y1="2528" y2="2720" x1="752" />
            <wire x2="768" y1="2720" y2="2720" x1="752" />
            <wire x2="768" y1="2720" y2="2912" x1="768" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="1200" y1="2560" y2="2736" x1="1200" />
            <wire x2="1216" y1="2736" y2="2736" x1="1200" />
            <wire x2="1216" y1="2736" y2="2912" x1="1216" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="1776" y1="2720" y2="2896" x1="1776" />
            <wire x2="1792" y1="2720" y2="2720" x1="1776" />
            <wire x2="1792" y1="2544" y2="2720" x1="1792" />
        </branch>
        <branch name="XLXN_10">
            <wire x2="2304" y1="2720" y2="2896" x1="2304" />
            <wire x2="2336" y1="2720" y2="2720" x1="2304" />
            <wire x2="2336" y1="2544" y2="2720" x1="2336" />
        </branch>
        <branch name="XLXN_11">
            <wire x2="2880" y1="2544" y2="2720" x1="2880" />
            <wire x2="2928" y1="2720" y2="2720" x1="2880" />
            <wire x2="2928" y1="2720" y2="2896" x1="2928" />
        </branch>
        <branch name="XLXN_12">
            <wire x2="3472" y1="2544" y2="2720" x1="3472" />
            <wire x2="3584" y1="2720" y2="2720" x1="3472" />
            <wire x2="3584" y1="2720" y2="2912" x1="3584" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="4240" y1="2560" y2="2736" x1="4240" />
            <wire x2="4384" y1="2736" y2="2736" x1="4240" />
            <wire x2="4384" y1="2736" y2="2912" x1="4384" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="400" y1="2048" y2="2272" x1="400" />
            <wire x2="688" y1="2272" y2="2272" x1="400" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="656" y1="2048" y2="2160" x1="656" />
            <wire x2="752" y1="2160" y2="2160" x1="656" />
            <wire x2="752" y1="2160" y2="2272" x1="752" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="880" y1="2272" y2="2272" x1="816" />
            <wire x2="880" y1="2064" y2="2272" x1="880" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="1072" y1="2064" y2="2304" x1="1072" />
            <wire x2="1104" y1="2304" y2="2304" x1="1072" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="1168" y1="2176" y2="2304" x1="1168" />
            <wire x2="1280" y1="2176" y2="2176" x1="1168" />
            <wire x2="1280" y1="2064" y2="2176" x1="1280" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="1232" y1="2192" y2="2304" x1="1232" />
            <wire x2="1488" y1="2192" y2="2192" x1="1232" />
            <wire x2="1488" y1="2064" y2="2192" x1="1488" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="1680" y1="2080" y2="2288" x1="1680" />
            <wire x2="1728" y1="2288" y2="2288" x1="1680" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="1792" y1="2176" y2="2288" x1="1792" />
            <wire x2="1856" y1="2176" y2="2176" x1="1792" />
            <wire x2="1856" y1="2064" y2="2176" x1="1856" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="2000" y1="2288" y2="2288" x1="1856" />
            <wire x2="2000" y1="2080" y2="2288" x1="2000" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="2240" y1="2080" y2="2288" x1="2240" />
        </branch>
        <branch name="XLXN_30">
            <wire x2="2304" y1="2192" y2="2288" x1="2304" />
            <wire x2="2480" y1="2192" y2="2192" x1="2304" />
            <wire x2="2480" y1="2096" y2="2192" x1="2480" />
        </branch>
        <branch name="XLXN_31">
            <wire x2="2640" y1="2096" y2="2288" x1="2640" />
            <wire x2="2816" y1="2288" y2="2288" x1="2640" />
        </branch>
        <branch name="XLXN_32">
            <wire x2="2864" y1="2096" y2="2192" x1="2864" />
            <wire x2="2880" y1="2192" y2="2192" x1="2864" />
            <wire x2="2880" y1="2192" y2="2288" x1="2880" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="3104" y1="2112" y2="2288" x1="3104" />
            <wire x2="3376" y1="2288" y2="2288" x1="3104" />
        </branch>
        <branch name="XLXN_34">
            <wire x2="3296" y1="2176" y2="2176" x1="2944" />
            <wire x2="3296" y1="2176" y2="2208" x1="3296" />
            <wire x2="3440" y1="2208" y2="2208" x1="3296" />
            <wire x2="3440" y1="2208" y2="2288" x1="3440" />
            <wire x2="2944" y1="2176" y2="2288" x1="2944" />
            <wire x2="3296" y1="2128" y2="2176" x1="3296" />
        </branch>
        <branch name="XLXN_35">
            <wire x2="3504" y1="2112" y2="2288" x1="3504" />
        </branch>
        <branch name="XLXN_36">
            <wire x2="3760" y1="2288" y2="2288" x1="3568" />
            <wire x2="3760" y1="2144" y2="2288" x1="3760" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="4032" y1="2144" y2="2304" x1="4032" />
            <wire x2="4144" y1="2304" y2="2304" x1="4032" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="4208" y1="2272" y2="2272" x1="1296" />
            <wire x2="4208" y1="2272" y2="2304" x1="4208" />
            <wire x2="1296" y1="2272" y2="2304" x1="1296" />
            <wire x2="4336" y1="2208" y2="2208" x1="4208" />
            <wire x2="4208" y1="2208" y2="2272" x1="4208" />
            <wire x2="4336" y1="2128" y2="2208" x1="4336" />
        </branch>
        <branch name="XLXN_40">
            <wire x2="4272" y1="2240" y2="2240" x1="2368" />
            <wire x2="4272" y1="2240" y2="2304" x1="4272" />
            <wire x2="2368" y1="2240" y2="2288" x1="2368" />
            <wire x2="4656" y1="2224" y2="2224" x1="4272" />
            <wire x2="4272" y1="2224" y2="2240" x1="4272" />
            <wire x2="4656" y1="2112" y2="2224" x1="4656" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="2432" y1="2160" y2="2288" x1="2432" />
            <wire x2="4400" y1="2160" y2="2160" x1="2432" />
            <wire x2="4400" y1="2160" y2="2208" x1="4400" />
            <wire x2="4960" y1="2208" y2="2208" x1="4400" />
            <wire x2="4960" y1="2208" y2="2304" x1="4960" />
            <wire x2="4960" y1="2304" y2="2304" x1="4336" />
            <wire x2="4960" y1="2144" y2="2208" x1="4960" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="304" y1="1408" y2="1792" x1="304" />
            <wire x2="2144" y1="1408" y2="1408" x1="304" />
            <wire x2="2144" y1="1408" y2="1824" x1="2144" />
            <wire x2="2752" y1="1408" y2="1408" x1="2144" />
            <wire x2="2752" y1="1408" y2="1840" x1="2752" />
            <wire x2="2768" y1="1840" y2="1840" x1="2752" />
            <wire x2="3232" y1="1408" y2="1408" x1="2752" />
            <wire x2="3232" y1="1408" y2="1872" x1="3232" />
            <wire x2="3456" y1="1408" y2="1408" x1="3232" />
            <wire x2="4128" y1="1408" y2="1408" x1="3456" />
            <wire x2="4544" y1="1408" y2="1408" x1="4128" />
            <wire x2="4544" y1="1408" y2="1424" x1="4544" />
            <wire x2="4560" y1="1424" y2="1424" x1="4544" />
            <wire x2="4560" y1="1424" y2="1856" x1="4560" />
            <wire x2="3456" y1="1408" y2="1632" x1="3456" />
            <wire x2="3440" y1="1632" y2="1856" x1="3440" />
            <wire x2="3456" y1="1632" y2="1632" x1="3440" />
            <wire x2="4112" y1="704" y2="1056" x1="4112" />
            <wire x2="4128" y1="1056" y2="1056" x1="4112" />
            <wire x2="4128" y1="1056" y2="1408" x1="4128" />
        </branch>
        <branch name="D3">
            <wire x2="496" y1="1360" y2="1792" x1="496" />
            <wire x2="2336" y1="1360" y2="1360" x1="496" />
            <wire x2="2336" y1="1360" y2="1824" x1="2336" />
            <wire x2="2720" y1="1360" y2="1360" x1="2336" />
            <wire x2="2720" y1="1360" y2="1840" x1="2720" />
            <wire x2="3168" y1="1360" y2="1360" x1="2720" />
            <wire x2="3168" y1="1360" y2="1856" x1="3168" />
            <wire x2="3344" y1="1360" y2="1360" x1="3168" />
            <wire x2="3344" y1="1360" y2="1472" x1="3344" />
            <wire x2="3360" y1="1472" y2="1472" x1="3344" />
            <wire x2="3360" y1="1472" y2="1872" x1="3360" />
            <wire x2="4192" y1="1360" y2="1360" x1="3344" />
            <wire x2="4432" y1="1360" y2="1360" x1="4192" />
            <wire x2="4720" y1="1360" y2="1360" x1="4432" />
            <wire x2="4432" y1="1360" y2="1872" x1="4432" />
            <wire x2="4192" y1="1360" y2="1888" x1="4192" />
            <wire x2="2720" y1="1840" y2="1840" x1="2704" />
            <wire x2="4192" y1="1888" y2="1888" x1="4128" />
            <wire x2="4720" y1="496" y2="1360" x1="4720" />
            <wire x2="4912" y1="496" y2="496" x1="4720" />
            <wire x2="4912" y1="496" y2="528" x1="4912" />
            <wire x2="4912" y1="224" y2="224" x1="4896" />
            <wire x2="4912" y1="224" y2="496" x1="4912" />
        </branch>
        <branch name="D0">
            <wire x2="1008" y1="1104" y2="1104" x1="560" />
            <wire x2="1424" y1="1104" y2="1104" x1="1008" />
            <wire x2="1616" y1="1104" y2="1104" x1="1424" />
            <wire x2="1968" y1="1104" y2="1104" x1="1616" />
            <wire x2="2416" y1="1104" y2="1104" x1="1968" />
            <wire x2="3040" y1="1104" y2="1104" x1="2416" />
            <wire x2="3664" y1="1104" y2="1104" x1="3040" />
            <wire x2="3664" y1="1104" y2="1888" x1="3664" />
            <wire x2="3936" y1="1104" y2="1104" x1="3664" />
            <wire x2="3968" y1="1104" y2="1104" x1="3936" />
            <wire x2="4240" y1="1104" y2="1104" x1="3968" />
            <wire x2="4800" y1="1104" y2="1104" x1="4240" />
            <wire x2="4800" y1="1104" y2="1120" x1="4800" />
            <wire x2="4864" y1="1120" y2="1120" x1="4800" />
            <wire x2="4864" y1="1120" y2="1888" x1="4864" />
            <wire x2="4240" y1="1104" y2="1872" x1="4240" />
            <wire x2="3936" y1="1104" y2="1888" x1="3936" />
            <wire x2="3040" y1="1104" y2="1856" x1="3040" />
            <wire x2="2416" y1="1104" y2="1840" x1="2416" />
            <wire x2="1968" y1="1104" y2="1824" x1="1968" />
            <wire x2="1616" y1="1104" y2="1824" x1="1616" />
            <wire x2="1424" y1="1104" y2="1808" x1="1424" />
            <wire x2="1008" y1="1104" y2="1808" x1="1008" />
            <wire x2="560" y1="1104" y2="1792" x1="560" />
            <wire x2="3968" y1="448" y2="1104" x1="3968" />
            <wire x2="4112" y1="448" y2="448" x1="3968" />
            <wire x2="4112" y1="448" y2="480" x1="4112" />
            <wire x2="4112" y1="240" y2="448" x1="4112" />
        </branch>
        <branch name="D1">
            <wire x2="608" y1="1200" y2="1216" x1="608" />
            <wire x2="624" y1="1216" y2="1216" x1="608" />
            <wire x2="624" y1="1216" y2="1792" x1="624" />
            <wire x2="1088" y1="1200" y2="1200" x1="608" />
            <wire x2="1088" y1="1200" y2="1504" x1="1088" />
            <wire x2="1216" y1="1200" y2="1200" x1="1088" />
            <wire x2="1216" y1="1200" y2="1808" x1="1216" />
            <wire x2="2208" y1="1200" y2="1200" x1="1216" />
            <wire x2="2208" y1="1200" y2="1824" x1="2208" />
            <wire x2="2480" y1="1200" y2="1200" x1="2208" />
            <wire x2="2480" y1="1200" y2="1840" x1="2480" />
            <wire x2="2576" y1="1200" y2="1200" x1="2480" />
            <wire x2="2576" y1="1200" y2="1840" x1="2576" />
            <wire x2="2832" y1="1200" y2="1200" x1="2576" />
            <wire x2="2832" y1="1200" y2="1840" x1="2832" />
            <wire x2="3104" y1="1200" y2="1200" x1="2832" />
            <wire x2="3104" y1="1200" y2="1856" x1="3104" />
            <wire x2="3504" y1="1200" y2="1200" x1="3104" />
            <wire x2="3504" y1="1200" y2="1856" x1="3504" />
            <wire x2="4000" y1="1200" y2="1200" x1="3504" />
            <wire x2="4288" y1="1200" y2="1200" x1="4000" />
            <wire x2="4000" y1="1200" y2="1888" x1="4000" />
            <wire x2="1072" y1="1504" y2="1808" x1="1072" />
            <wire x2="1088" y1="1504" y2="1504" x1="1072" />
            <wire x2="4384" y1="432" y2="432" x1="4288" />
            <wire x2="4384" y1="432" y2="496" x1="4384" />
            <wire x2="4288" y1="432" y2="1200" x1="4288" />
            <wire x2="4384" y1="240" y2="432" x1="4384" />
        </branch>
        <branch name="XLXN_85">
            <wire x2="752" y1="1616" y2="1792" x1="752" />
            <wire x2="960" y1="1616" y2="1616" x1="752" />
            <wire x2="960" y1="1616" y2="1808" x1="960" />
            <wire x2="1136" y1="1616" y2="1616" x1="960" />
            <wire x2="1344" y1="1616" y2="1616" x1="1136" />
            <wire x2="1344" y1="1616" y2="1808" x1="1344" />
            <wire x2="1552" y1="1616" y2="1616" x1="1344" />
            <wire x2="1552" y1="1616" y2="1808" x1="1552" />
            <wire x2="1920" y1="1616" y2="1616" x1="1552" />
            <wire x2="2032" y1="1616" y2="1616" x1="1920" />
            <wire x2="2960" y1="1616" y2="1616" x1="2032" />
            <wire x2="3856" y1="1616" y2="1616" x1="2960" />
            <wire x2="4752" y1="1616" y2="1616" x1="3856" />
            <wire x2="5040" y1="1616" y2="1616" x1="4752" />
            <wire x2="5040" y1="1616" y2="1632" x1="5040" />
            <wire x2="5056" y1="1632" y2="1632" x1="5040" />
            <wire x2="5056" y1="1632" y2="1888" x1="5056" />
            <wire x2="4752" y1="1616" y2="1856" x1="4752" />
            <wire x2="3856" y1="1616" y2="1888" x1="3856" />
            <wire x2="2960" y1="1616" y2="1840" x1="2960" />
            <wire x2="2032" y1="1616" y2="1824" x1="2032" />
            <wire x2="1920" y1="1616" y2="1808" x1="1920" />
            <wire x2="1136" y1="1616" y2="1808" x1="1136" />
            <wire x2="960" y1="1808" y2="1808" x1="944" />
            <wire x2="4912" y1="752" y2="1184" x1="4912" />
            <wire x2="5056" y1="1184" y2="1184" x1="4912" />
            <wire x2="5056" y1="1184" y2="1632" x1="5056" />
        </branch>
        <branch name="D2">
            <wire x2="432" y1="1264" y2="1792" x1="432" />
            <wire x2="704" y1="1264" y2="1264" x1="432" />
            <wire x2="704" y1="1264" y2="1504" x1="704" />
            <wire x2="1856" y1="1264" y2="1264" x1="704" />
            <wire x2="1856" y1="1264" y2="1808" x1="1856" />
            <wire x2="2544" y1="1264" y2="1264" x1="1856" />
            <wire x2="2544" y1="1264" y2="1840" x1="2544" />
            <wire x2="2640" y1="1264" y2="1264" x1="2544" />
            <wire x2="2640" y1="1264" y2="1840" x1="2640" />
            <wire x2="3296" y1="1264" y2="1264" x1="2640" />
            <wire x2="3296" y1="1264" y2="1872" x1="3296" />
            <wire x2="3568" y1="1264" y2="1264" x1="3296" />
            <wire x2="3568" y1="1264" y2="1856" x1="3568" />
            <wire x2="3792" y1="1264" y2="1264" x1="3568" />
            <wire x2="4064" y1="1264" y2="1264" x1="3792" />
            <wire x2="4336" y1="1264" y2="1264" x1="4064" />
            <wire x2="4400" y1="1264" y2="1264" x1="4336" />
            <wire x2="4400" y1="1264" y2="1552" x1="4400" />
            <wire x2="4688" y1="1552" y2="1552" x1="4400" />
            <wire x2="4688" y1="1552" y2="1856" x1="4688" />
            <wire x2="4464" y1="1264" y2="1264" x1="4400" />
            <wire x2="4336" y1="1264" y2="1568" x1="4336" />
            <wire x2="4368" y1="1568" y2="1568" x1="4336" />
            <wire x2="4368" y1="1568" y2="1872" x1="4368" />
            <wire x2="3792" y1="1264" y2="1888" x1="3792" />
            <wire x2="688" y1="1504" y2="1792" x1="688" />
            <wire x2="704" y1="1504" y2="1504" x1="688" />
            <wire x2="4640" y1="496" y2="496" x1="4464" />
            <wire x2="4640" y1="496" y2="512" x1="4640" />
            <wire x2="4464" y1="496" y2="1264" x1="4464" />
            <wire x2="4640" y1="240" y2="496" x1="4640" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="880" y1="1696" y2="1808" x1="880" />
            <wire x2="1280" y1="1696" y2="1696" x1="880" />
            <wire x2="1280" y1="1696" y2="1808" x1="1280" />
            <wire x2="1488" y1="1696" y2="1696" x1="1280" />
            <wire x2="1488" y1="1696" y2="1808" x1="1488" />
            <wire x2="1744" y1="1696" y2="1696" x1="1488" />
            <wire x2="1744" y1="1696" y2="1824" x1="1744" />
            <wire x2="2272" y1="1696" y2="1696" x1="1744" />
            <wire x2="2272" y1="1696" y2="1824" x1="2272" />
            <wire x2="2912" y1="1696" y2="1696" x1="2272" />
            <wire x2="2912" y1="1696" y2="1760" x1="2912" />
            <wire x2="4080" y1="1696" y2="1696" x1="2912" />
            <wire x2="4640" y1="1696" y2="1696" x1="4080" />
            <wire x2="4928" y1="1696" y2="1696" x1="4640" />
            <wire x2="4928" y1="1696" y2="1888" x1="4928" />
            <wire x2="4080" y1="1696" y2="1792" x1="4080" />
            <wire x2="2896" y1="1760" y2="1840" x1="2896" />
            <wire x2="2912" y1="1760" y2="1760" x1="2896" />
            <wire x2="4064" y1="1792" y2="1888" x1="4064" />
            <wire x2="4080" y1="1792" y2="1792" x1="4064" />
            <wire x2="4640" y1="736" y2="1696" x1="4640" />
        </branch>
        <branch name="XLXN_98">
            <wire x2="368" y1="1488" y2="1792" x1="368" />
            <wire x2="832" y1="1488" y2="1488" x1="368" />
            <wire x2="832" y1="1488" y2="1648" x1="832" />
            <wire x2="1680" y1="1488" y2="1488" x1="832" />
            <wire x2="1680" y1="1488" y2="1824" x1="1680" />
            <wire x2="1776" y1="1488" y2="1488" x1="1680" />
            <wire x2="1776" y1="1488" y2="1808" x1="1776" />
            <wire x2="1792" y1="1808" y2="1808" x1="1776" />
            <wire x2="3728" y1="1488" y2="1488" x1="1776" />
            <wire x2="3728" y1="1488" y2="1888" x1="3728" />
            <wire x2="4296" y1="1488" y2="1488" x1="3728" />
            <wire x2="4304" y1="1488" y2="1488" x1="4296" />
            <wire x2="4304" y1="1488" y2="1872" x1="4304" />
            <wire x2="4384" y1="1488" y2="1488" x1="4304" />
            <wire x2="4616" y1="1488" y2="1488" x1="4384" />
            <wire x2="4624" y1="1488" y2="1488" x1="4616" />
            <wire x2="4944" y1="1488" y2="1488" x1="4624" />
            <wire x2="4944" y1="1488" y2="1520" x1="4944" />
            <wire x2="4992" y1="1520" y2="1520" x1="4944" />
            <wire x2="4992" y1="1520" y2="1888" x1="4992" />
            <wire x2="4624" y1="1488" y2="1856" x1="4624" />
            <wire x2="816" y1="1648" y2="1808" x1="816" />
            <wire x2="832" y1="1648" y2="1648" x1="816" />
            <wire x2="4384" y1="720" y2="1488" x1="4384" />
        </branch>
        <iomarker fontsize="28" x="4112" y="240" name="D0" orien="R270" />
        <iomarker fontsize="28" x="4384" y="240" name="D1" orien="R270" />
        <iomarker fontsize="28" x="4640" y="240" name="D2" orien="R270" />
        <iomarker fontsize="28" x="4896" y="224" name="D3" orien="R180" />
        <branch name="g">
            <wire x2="736" y1="3168" y2="3200" x1="736" />
        </branch>
        <iomarker fontsize="28" x="736" y="3200" name="g" orien="R90" />
        <branch name="f">
            <wire x2="1184" y1="3168" y2="3200" x1="1184" />
        </branch>
        <iomarker fontsize="28" x="1184" y="3200" name="f" orien="R90" />
        <branch name="e">
            <wire x2="1744" y1="3152" y2="3184" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="1744" y="3184" name="e" orien="R90" />
        <branch name="d">
            <wire x2="2272" y1="3152" y2="3184" x1="2272" />
        </branch>
        <iomarker fontsize="28" x="2272" y="3184" name="d" orien="R90" />
        <branch name="c">
            <wire x2="2896" y1="3152" y2="3184" x1="2896" />
        </branch>
        <iomarker fontsize="28" x="2896" y="3184" name="c" orien="R90" />
        <branch name="b">
            <wire x2="3552" y1="3168" y2="3200" x1="3552" />
        </branch>
        <iomarker fontsize="28" x="3552" y="3200" name="b" orien="R90" />
        <branch name="a">
            <wire x2="4352" y1="3168" y2="3200" x1="4352" />
        </branch>
        <iomarker fontsize="28" x="4352" y="3200" name="a" orien="R90" />
        <instance x="80" y="1632" name="XLXI_41" orien="R90" />
        <branch name="point">
            <wire x2="96" y1="528" y2="1488" x1="96" />
            <wire x2="112" y1="1488" y2="1488" x1="96" />
            <wire x2="112" y1="1488" y2="1632" x1="112" />
        </branch>
        <branch name="p">
            <wire x2="112" y1="1856" y2="2848" x1="112" />
            <wire x2="128" y1="2848" y2="2848" x1="112" />
            <wire x2="144" y1="2848" y2="2848" x1="128" />
            <wire x2="144" y1="2848" y2="3040" x1="144" />
        </branch>
        <iomarker fontsize="28" x="144" y="3040" name="p" orien="R90" />
        <iomarker fontsize="28" x="208" y="368" name="LE" orien="R270" />
        <iomarker fontsize="28" x="96" y="528" name="point" orien="R270" />
    </sheet>
</drawing>