#ifndef BOOK_MANAGER_H
#define BOOK_MANAGER_H

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSql>

#include <QObject>
#include <QVector>
#include <QDebug>
#include <QWidget>
#include <QLineEdit>
#include <QLayout>
#include <QComboBox>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>
#include <QTabWidget>
#include <QTreeWidget>
#include <QPalette>
#include <QBrush>
#include <QPixmap>
#include <QDateTime>
#include <QStringList>
#include <QPluginLoader>
#include <QMessageBox>
#include <QCheckBox>
#include <QTableWidget>
#include <QTimer>

class book_manager:public QObject
{
    Q_OBJECT
public:
    book_manager(QObject *parent = 0);
    ~book_manager();
private:
    enum{MANAGER,USER,VISITOR,USER_LOGIN,USER_REGISTER};//user的各种身份代称
    QString currentUser;

    int userType;//决定user的身份
    int current_page;
    int max_page;

    int userCurrentPage;
    int userMaxPage;
    int userNum;

    int current_row_number;

    QVBoxLayout *vboxLayout;
    QVBoxLayout *vboxLayout0;
    QVBoxLayout *vboxLayout1;
    QVBoxLayout *vboxLayout2;
    QVBoxLayout *vboxLayout3;
    QVBoxLayout *vboxLayout4;
    QVBoxLayout *vboxLayout5;
    QVBoxLayout *vboxLayout6;
    QVBoxLayout *vboxLayout7;
    QVBoxLayout *vboxLayout9;
    QVBoxLayout *vboxLayout10;
    QVBoxLayout *vboxLayout11;
    QVBoxLayout *vboxLayout12;

    QHBoxLayout *hboxLayout;
    QHBoxLayout *hboxLayout1;
    QHBoxLayout *hboxLayout2;
    QHBoxLayout *hboxLayout3;
    QHBoxLayout *hboxLayout4;
    QHBoxLayout *hboxLayout5;
    QHBoxLayout *hboxLayout6;
    QHBoxLayout *hboxLayout7;
    QHBoxLayout *hboxLayout8;
    QHBoxLayout *hboxLayout9;
    QHBoxLayout *hboxLayout10;
    QHBoxLayout *hboxLayout11;
    QHBoxLayout *hboxLayout12;
    QHBoxLayout *hboxLayout13;

    //主界面的几个模块
    QTimer *timer;
    QTableWidget *tablewidget;
    QGridLayout *gridLayout ;
    QWidget *titleImage;

    QLabel *spaceLabel0;
    QLabel *spaceLabel1;
    QGroupBox *groupbox;
    QGroupBox *groupbox1;
    QPushButton *clearbutton;
    QPushButton *searchbutton;
    QPushButton *registerbutton;
    QPushButton *loginButton;
    QPushButton *quitLoginButton;
    QLabel *label;

    QTreeWidget *tree;
    QTreeWidgetItem *root;

    //查询函数使用的各种东西
    QLineEdit *nameLine;//书名
    QLineEdit *publishLine;//出版社名
    QLineEdit *priceLine;//价格下限
    QLineEdit *priceLine1;//价格上限
    QLineEdit *authorLine;//作者名称
    QComboBox *dateBox;//时间下限
    QComboBox *dateBox1;//时间上限
    QComboBox *isLent;//是否已经被借出
    QWidget *window;

    QPushButton *nextbutton;
    QPushButton *lastbutton;

    QSqlDatabase db;
    QSqlQuery query;

    QTabWidget *mainTabWidget;
    QTabWidget *loginTabWidget;
    QWidget *user_loginWidget;
    QWidget *manager_loginWidget;

    //登陆界面的模块
    QLineEdit *username1;//用户名(非管理员)
    QLineEdit *password1;//用户密码
    QLineEdit *username2;//管理员用户名
    QLineEdit *password2;//管理员密码

    QPushButton *okbutton;

    QGridLayout *gridLayout2;
    QTabWidget *manageTabWidget;
    QWidget *addBookWidget;
    QLabel *label2;

    QLineEdit *nameLine2;
    QComboBox *typeBox2;
    QLineEdit *priceLine2;
    QLineEdit *authorLine2;
    QLineEdit *numLine2;
    QLineEdit *publishLine2;
    QComboBox *dateBox2;

    QWidget *window1;
    QWidget *window2;
    QWidget *window3;
    QWidget *window4;
    QWidget *window5;

    //注册使用时使用的模块
    QWidget *registerWindow;
    QLabel *label3;
    QLineEdit *nameLine3;//注册的名字
    QLineEdit *passwordLine1;//密码设定
    QLineEdit *passwordLine2;//重复密码验证
    QPushButton *registerOkButton;

    QGridLayout *gridLayout3;
    QLabel *label4;
    QVector<QString>result;

    QLabel *label5;
    QLineEdit *idLine;

    QWidget *deletebookWidget;
    QPushButton *deleteOkButton;

    QTableWidget *tablewidget1;

    QPushButton *deleteUserButton;
    QPushButton *grantLentButton;
    QPushButton *lastPageButton;
    QPushButton *nextPageButton;
    QTableWidgetItem *item0[13];
    QTableWidgetItem *item1[13];
    QVector<QString*>userMessage;

    //借书书籍节目的模块
    QTableWidgetItem *item2[10];
    QTableWidgetItem *item3[10];
    QPushButton *lendBookButton;

    //归还书籍界面的模块
    QTableWidget *tablewidget2;
    QVector<QString*>loanMessage;
    QTableWidgetItem *item4[3];
    QPushButton *returnBookButton;

    QTableWidget *tablewidget3;

    QPushButton *addBookOkButton;
    QPushButton *addBookClearButton;

    QComboBox *bookType[13];
    QComboBox *publishDate[13];
    QString bookMessage[7];

    QVector<QString*>multiBookMessage;

    QWidget* createLoginWindow(int type);
    void setLayout();
    bool createConnection();

    void setWindowShowBook(int type);
    void setWindowSearchBook(int type);
    void setWindowAddBook();
    void setWindowUserManage();
    void setWindowLoanBook();
    void setWindowMutiAddBook();
    void setWindowTitle();

    void updateManageWindow();
    void updateShowBookWindow();
    void updateLoanBookWindow();
    void updateTitle(int type);

    bool addBook();

    void loadUserMessage();
    void loadLoanMessage();

private slots:
    void searchBook();
    void showTime();
    void clear();
    void quitLogin();
    void returnBook();
    void managerLogin();
    void deleteLogin();
    void bookLastPage();
    void bookNextPage();
    void deleteBook();
    void searchBookByType(QTreeWidgetItem*);
    void userLogin();
    void setWindowRegister();
    void setWindowLogin();
    void Register();
    void deleteUser();
    void grantLent();
    void userNextPage();
    void userLastPage();
    void lendBook();
    void mutiAddBook();
    void clearAddBookMessage();
};

#endif // BOOK_MANAGER_H
