&fnof;(x)=log<sub>2</sub>N<sup>2</sup>

&spades;&clubs;&hearts;&diams;&infin;
&ang;&sect;&copy;&times;&divide;&oplus;
&int;
&equiv;
&ne;
&le;
&ge;
&frac12;
&frac56;
&permil;
&because;
&there4;
&rArr;
&laquo;&raquo;
&Sigma;&sigma;
&Gamma;&gamma;
&Theta;&lambda;

