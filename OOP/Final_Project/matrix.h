#ifndef MATRIX_H
#define MATRIX_H

#include <QDialog>
#include<QTextEdit>
#include<QPushButton>
#include<QLabel>
#include<QLineEdit>

class Matrix;

class Matrix : public QDialog
{
	Q_OBJECT

public:
	explicit Matrix(QWidget *parent = 0);
	~Matrix();

private:
	QPushButton *retButton;
	QPushButton *addButton;
	QPushButton *subButton;
	QPushButton *muliButton;
	QPushButton *clrButton;

	QLabel *inputLabel1;
	QLabel *inputLabel2;
	QLabel *outputLabel;
	QTextEdit *input1;
	QTextEdit *input2;
	QTextEdit *output;

	QLabel *capLabel;
	QLineEdit *capacity;

	QLabel *remindLabel;

	private slots:
	void buttonLoginbackClick();
	void buttonMultiplyClick();
	void buttonAddClick();
	void buttonSubtractClick();
	void buttonClearClick();

};

#endif // MATRIX_H
