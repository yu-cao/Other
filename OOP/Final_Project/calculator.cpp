#include "Calculator.h"
#include<QLabel>
#include<QLineEdit>
#include<QPushButton>
#include<QGridLayout>

void CalculateInput(const char *S, char OPS[], int &len);
void CalculateOutput(char B[], int len, double &result);

Calculator::Calculator(QWidget *parent) :
    QDialog(parent)
{
	this->setWindowTitle(QString("Calculator"));//设置标题

	inputLine = new QLineEdit;
	inputLine->setText(input);

	//对0~9的按钮进行初始化
	zeroButton = new QPushButton("0");
	oneButton = new QPushButton("1");
	twoButton = new QPushButton("2");
	threeButton = new QPushButton("3");
	fourButton = new QPushButton("4");
	fiveButton = new QPushButton("5");
	sixButton = new QPushButton("6");
	sevenButton = new QPushButton("7");
	eightButton = new QPushButton("8");
	nineButton = new QPushButton("9");
	decButton = new QPushButton(".");

	//具体操作面板
	botButton = new QPushButton("-");
	timButton = new QPushButton("Time");
	addButton = new QPushButton("+");
	subButton = new QPushButton("-");
	mulButton = new QPushButton("*");
	divButton = new QPushButton("/");
	equButton = new QPushButton("=");
	lefButton = new QPushButton("(");
	rigButton = new QPushButton(")");
	CEButton = new QPushButton("CE");
	ACButton = new QPushButton("AC");
	retButton = new QPushButton("Return");

	QGridLayout *mainLayout = new QGridLayout(this); //栅格化布局管理

	inputLine->setFixedHeight(50);//调整行的高度
	CEButton->setFixedHeight(50);
	ACButton->setFixedHeight(50);

	mainLayout->addWidget(inputLine, 0, 0, 1, 3);
	mainLayout->setRowStretch(0, 100);
	mainLayout->addWidget(CEButton, 0, 3, 1, 1);
	mainLayout->addWidget(ACButton, 0, 4, 1, 1);

	mainLayout->addWidget(oneButton, 1, 0, 1, 1);
	mainLayout->addWidget(twoButton, 1, 1, 1, 1);
	mainLayout->addWidget(threeButton, 1, 2, 1, 1);
	mainLayout->addWidget(botButton, 1, 3, 1, 1);
	mainLayout->addWidget(timButton, 1, 4, 1, 1);

	mainLayout->addWidget(fourButton, 2, 0, 1, 1);
	mainLayout->addWidget(fiveButton, 2, 1, 1, 1);
	mainLayout->addWidget(sixButton, 2, 2, 1, 1);
	mainLayout->addWidget(addButton, 2, 3, 1, 1);
	mainLayout->addWidget(subButton, 2, 4, 1, 1);

	mainLayout->addWidget(sevenButton, 3, 0, 1, 1);
	mainLayout->addWidget(eightButton, 3, 1, 1, 1);
	mainLayout->addWidget(nineButton, 3, 2, 1, 1);
	mainLayout->addWidget(mulButton, 3, 3, 1, 1);
	mainLayout->addWidget(divButton, 3, 4, 1, 1);

	mainLayout->addWidget(zeroButton, 4, 0, 1, 1);
	mainLayout->addWidget(decButton, 4, 1, 1, 1);
	mainLayout->addWidget(lefButton, 4, 2, 1, 1);
	mainLayout->addWidget(rigButton, 4, 3, 1, 1);
	mainLayout->addWidget(equButton, 4, 4, 1, 1);
	mainLayout->addWidget(retButton, 5, 5, 1, 1);

	connect(zeroButton, SIGNAL(clicked()), this, SLOT(buttonZeroClicked()));
	connect(oneButton, SIGNAL(clicked()), this, SLOT(buttonOneClicked()));
	connect(twoButton, SIGNAL(clicked()), this, SLOT(buttonTwoClicked()));
	connect(threeButton, SIGNAL(clicked()), this, SLOT(buttonThreeClicked()));
	connect(fourButton, SIGNAL(clicked()), this, SLOT(buttonFourClicked()));
	connect(fiveButton, SIGNAL(clicked()), this, SLOT(buttonFiveClicked()));
	connect(sixButton, SIGNAL(clicked()), this, SLOT(buttonSixClicked()));
	connect(sevenButton, SIGNAL(clicked()), this, SLOT(buttonSevenClicked()));
	connect(eightButton, SIGNAL(clicked()), this, SLOT(buttonEightClicked()));
	connect(nineButton, SIGNAL(clicked()), this, SLOT(buttonNineClicked()));

	connect(addButton, SIGNAL(clicked()), this, SLOT(buttonAddClicked()));
	connect(subButton, SIGNAL(clicked()), this, SLOT(buttonSubClicked()));
	connect(mulButton, SIGNAL(clicked()), this, SLOT(buttonMulClicked()));
	connect(divButton, SIGNAL(clicked()), this, SLOT(buttonDivClicked()));

	connect(botButton, SIGNAL(clicked(bool)), this, SLOT(buttonBotClicked()));//正负号
	connect(timButton, SIGNAL(clicked(bool)), this, SLOT(buttonTimClicked()));//时间
	connect(decButton, SIGNAL(clicked(bool)), this, SLOT(buttonDecClicked()));//点号

	connect(lefButton, SIGNAL(clicked(bool)), this, SLOT(buttonLefClicked()));//左括号
	connect(rigButton, SIGNAL(clicked(bool)), this, SLOT(buttonRigClicked()));//右括号

	connect(CEButton, SIGNAL(clicked()), this, SLOT(buttonCEClicked()));
	connect(ACButton, SIGNAL(clicked()), this, SLOT(buttonACClicked()));

	connect(equButton, SIGNAL(clicked()), this, SLOT(buttonEquClicked())); //等于号*/
	connect(retButton, &QPushButton::clicked, this, &Calculator::buttonLoginbackClick);

}

Calculator::~Calculator()
{
}

void Calculator::buttonLoginbackClick()
{
	accept();
}


void Calculator::buttonZeroClicked()  //以下是实现相应的槽函数
{
	if (input == "0")
		input = '0';
	else input = input + '0';
	inputLine->setText(input);
}

void Calculator::buttonOneClicked()
{
	if (input == "0")
		input = '1';
	else input = input + '1';
	inputLine->setText(input);
}

void Calculator::buttonTwoClicked()
{
	if (input == "0")
		input = '2';
	else input = input + '2';
	inputLine->setText(input);
}

void Calculator::buttonThreeClicked()
{
	if (input == "0")
		input = '3';
	else input = input + '3';
	inputLine->setText(input);
}

void Calculator::buttonFourClicked()
{
	if (input == "0")
		input = '4';
	else input = input + '4';
	inputLine->setText(input);
}

void Calculator::buttonFiveClicked()
{
	if (input == "0")
		input = '5';
	else input = input + '5';
	inputLine->setText(input);
}

void Calculator::buttonSixClicked()
{
	if (input == "0")
		input = '6';
	else input = input + '6';
	inputLine->setText(input);
}

void Calculator::buttonSevenClicked()
{
	if (input == "0")
		input = '7';
	else input = input + '7';
	inputLine->setText(input);
}

void Calculator::buttonEightClicked()
{
	if (input == "0")
		input = '8';
	else input = input + '8';
	inputLine->setText(input);
}

void Calculator::buttonNineClicked()
{
	if (input == "0")
		input = '9';
	else input = input + '9';
	inputLine->setText(input);
}

void Calculator::buttonAddClicked()
{
	if (input == "0")
		input = '+';
	else input = input + '+';
	inputLine->setText(input);
}

void Calculator::buttonSubClicked()
{
	if (input == "0")
		input = '-';
	else input = input + '-';
	inputLine->setText(input);
}

void Calculator::buttonMulClicked()
{
	if (input == "0")
		input = '*';
	else input = input + '*';
	inputLine->setText(input);
}

void Calculator::buttonDivClicked()
{
	if (input == "0")
		input = '/';
	else input = input + '/';
	inputLine->setText(input);
}

void Calculator::buttonTimClicked() //Time
{
	input = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz"); //格式化输出当前时间
	inputLine->setText(input);
}

void Calculator::buttonDecClicked() //小数点
{
	if (input == "0")
		input = '.';
	else input = input + '.';
	inputLine->setText(input);
}

void Calculator::buttonBotClicked() //加减号
{
	if (input == "0")
		input = '-';
	else
	{
		QString::iterator p = input.end();
		p--;
		if ('-' == *p)
			input = input.left(input.length() - 1);
		else input = input + '-';
	}
	inputLine->setText(input);
}

void Calculator::buttonLefClicked()//左括号
{
	if (input == "0")
		input = '(';
	else input = input + '(';
	inputLine->setText(input);
}

void Calculator::buttonRigClicked()//右括号
{
	if (input == "0")
		input = ')';
	else input = input + ')';
	inputLine->setText(input);
}

void Calculator::buttonCEClicked()
{
	input = input.left(input.length() - 1); //减去一字符
	inputLine->setText(input);
}
void Calculator::buttonACClicked() //直接清空
{
	input = "";
	inputLine->setText(input);
}

void Calculator::buttonEquClicked()//等于号
{
	std::string str = input.toStdString();
	const char *S = str.c_str();
	char OPS[50];
	int len;
	double result;
	QString change;
	CalculateInput(S, OPS, len);
	CalculateOutput(OPS, len, result);
	change = QString::number(result, 10, 6);
	input = input + "=" + change;
	inputLine->setText(input);
}

//该函数将整个文档框中的表达式压入堆栈
//将整个表达式由中缀表达式转化成为后缀表达式
//输入为整个表达式的char[]形式，准备储存的OPS[]和准备后续使用的len（用以统计整个OPS的长度）
void CalculateInput(const char *S, char OPS[], int &len)
{
	QStack<char> OPE;
	int i, j = 0;
	for (i = 0; i < strlen(S); i++)
	{
		switch (S[i])
		{
		case'+':
			if (OPE.isEmpty())
				OPE.push(S[i]);
			else if (OPE.top() == '*' || OPE.top() == '/')
			{
				OPS[j++] = OPE.pop();
				i--;
			}
			else
				OPE.push(S[i]);
			break;
		case'-':
			if ('(' != S[i - 1] && 0 != i)
			{
				if (OPE.isEmpty())
					OPE.push(S[i]);
				else if (OPE.top() == '*' || OPE.top() == '/')
				{
					OPS[j++] = OPE.pop();
					i--;
				}
				else
					OPE.push(S[i]);
			}
			else
			{
				while ((S[i] >= '0'&&S[i] <= '9') || S[i] == '.' || ('-' == S[i] && (S[i - 1]<'0' || S[i - 1]>'9')))
				{
					OPS[j++] = S[i];
					if ('-' == S[i])
						OPS[j++] = '@';
					i++;
				}
				i--;
				OPS[j++] = '#';  //数字中的间隔符
			}
			break;
		case'*':
		case'/':
			OPE.push(S[i]);
			break;
		case'(':
			OPE.push(S[i]);
			break;
		case')':
			while (OPE.top() != '(')
			{
				OPS[j++] = OPE.pop();
			}
			OPE.pop();
			break;
		default:
			while (S[i] >= '0'&&S[i] <= '9' || S[i] == '.' || ('-' == S[i] && S[i - 1]<'0'&&S[i - 1]>'9'))
			{
				OPS[j++] = S[i++];
			}
			i--;
			OPS[j++] = '#';  //数字中的间隔符
			break;
		}
	}
	while (!OPE.isEmpty())
	{
		OPS[j++] = OPE.pop();
	}
	len = j;
}

//该函数将之前的表达式进行计算然后通过result返回结果
//输入的是之前的储存表达式OPS[],计算结果result通过地址传递形式带回
void CalculateOutput(char B[], int len, double &result)
{
	bool flag = true;
	int i;
	double a;
	double b;
	double c;
	QStack<double>SZ;
	for (i = 0; i < len; i++)
	{
		switch (B[i])
		{
		case'+':
		{
			a = SZ.pop();
			b = SZ.pop();
			c = b + a;
			SZ.push(c);
		}
		break;
		case'-':
		{
			if ('@' != B[i + 1])
			{
				a = SZ.pop();
				b = SZ.pop();
				c = b - a;
				SZ.push(c);
			}
			else
			{
				int jx = 0;
				double dx;
				char *stx = new char[10];
				while (B[i] != '#')
				{
					if ('@' != B[i])
						stx[jx++] = B[i];
					i++;

				}
				dx = atof(stx);
				SZ.push(dx);
				delete stx;
			}
		}
		break;
		case'*':
		{
			a = SZ.pop();
			b = SZ.pop();
			c = b * a;
			SZ.push(c);
		}
		break;
		case'/':
		{
			a = SZ.pop();
			b = SZ.pop();
			if (a == 0)
			{
				flag = false;
				return;
			}
			c = b / a;
			SZ.push(c);
		}
		break;
		default:
			int j = 0;
			double d;
			char *st = new char[10];
			while (B[i] != '#')
			{
				st[j++] = B[i];
				i++;
			}
			d = atof(st);
			SZ.push(d);
			delete st;
			break;
		}
	}
	result = SZ.top();
	return;
}
