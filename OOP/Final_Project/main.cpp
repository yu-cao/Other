#include "mainwindow.h"
#include <QApplication>
#include"calculator.h"
#include"equation.h"
#include"sort.h"
#include"matrix.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
	Calculator func1;
	Equation func2;
	Sort func3;
	Matrix func4;
    w.show();

    return a.exec();
}
