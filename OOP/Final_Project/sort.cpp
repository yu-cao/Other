#include "sort.h"
#include<QGridLayout>

int cmpIn(const void *a, const void *b);
int cmpDe(const void *a, const void *b);

Sort::Sort(QWidget *parent) :
	QDialog(parent)
{

	this->setWindowTitle(QString("Sorting Digital Array"));

	inputLabel = new QLabel(tr("Input"));
	outputLabel = new QLabel(tr("Sorted:"));
	number = new QLabel(tr("Huge of data(eg. 5)"));

	input = new QLineEdit;
	output = new QLineEdit;
	num = new QLineEdit;

	retButton = new QPushButton(this);
	retButton->setText("Return");

	clsButton = new QPushButton(this);
	clsButton->setText("AC");

	sortIncreaseButton = new QPushButton(this);
	sortIncreaseButton->setText("Sort increasingly");

	sortDecreaseButton = new QPushButton(this);
	sortDecreaseButton->setText("Sort decreasingly");

	QGridLayout *mainLayout = new QGridLayout(this);
	mainLayout->addWidget(number, 1, 0, 1, 1);
	mainLayout->addWidget(num, 1, 1, 1, 1);
	mainLayout->addWidget(inputLabel, 2, 0, 1, 1);
	mainLayout->addWidget(input, 2, 1, 1, 1);
	mainLayout->addWidget(outputLabel, 2, 2, 1, 1);
	mainLayout->addWidget(output, 2, 3, 1, 1);
	mainLayout->addWidget(clsButton, 3, 0, 1, 1);
	mainLayout->addWidget(sortIncreaseButton, 3, 1, 1, 1);
	mainLayout->addWidget(sortDecreaseButton, 3, 2, 1, 1);
	mainLayout->addWidget(retButton, 3, 3, 1, 1);

	connect(retButton, &QPushButton::clicked, this, &Sort::buttonLoginbackClick);
	connect(sortIncreaseButton, SIGNAL(clicked(bool)), this, SLOT(buttonSortInClick()));
	connect(sortDecreaseButton, SIGNAL(clicked(bool)), this, SLOT(buttonSortDeClick()));
	connect(clsButton, SIGNAL(clicked(bool)), this, SLOT(buttonACClick()));

}

Sort::~Sort()
{
}

void Sort::buttonLoginbackClick()
{
	accept();
}

void Sort::buttonACClick()
{
	num->setText("");
	input->setText("");
	output->setText("");
}

void Sort::buttonSortInClick()
{
	QString Num = num->text();
	int N = Num.toInt();//数据的大小

	QString str = input->text();
	QStringList list = str.split(" ", QString::SkipEmptyParts);

	double A[10000];

	for (int i = 0; i < N; ++i) {
		QString temp1 = list.at(i);
		double temp = temp1.toDouble();
		A[i] = temp;
	}

	qsort(A, N, sizeof(A[0]), cmpIn);

	QString out = "";
	for (int i = 0; i < N; ++i) {
		QString str2;
		str2.setNum(A[i]);
		out += str2;
		out += " ";
	}
	output->setText(out);

}

int cmpIn(const void *a, const void *b)
{
	double tmp = (*(double *)a - *(double *)b);
	if (tmp > 0) return 1;
	else return -1; //升序
}

void Sort::buttonSortDeClick()
{
	QString Num = num->text();
	int N = Num.toInt();//数据的大小

	QString str = input->text();
	QStringList list = str.split(" ", QString::SkipEmptyParts);

	double A[10000];

	for (int i = 0; i < N; ++i) {
		QString temp1 = list.at(i);
		double temp = temp1.toDouble();
		A[i] = temp;
	}

	qsort(A, N, sizeof(A[0]), cmpDe);

	QString out = "";
	for (int i = 0; i < N; ++i) {
		QString str2;
		str2.setNum(A[i]);
		out += str2;
		out += " ";
	}
	output->setText(out);

}

int cmpDe(const void *a, const void *b)
{
	double tmp = (*(double *)b - *(double *)a);
	if (tmp > 0) return 1;
	else return -1; //升序
}
