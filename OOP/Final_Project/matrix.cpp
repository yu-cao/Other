#include "matrix.h"
#include<QGridLayout>
#include<QTextStream>

Matrix::Matrix(QWidget *parent) :
	QDialog(parent)
{
	this->setWindowTitle(QString("Matrix Calculate"));//设置标题

	retButton = new QPushButton(this);
	retButton->setText("Return");

	addButton = new QPushButton(this);
	addButton->setText("+");

	subButton = new QPushButton(this);
	subButton->setText("-");

	muliButton = new QPushButton(this);
	muliButton->setText("*");

	clrButton = new QPushButton(this);
	clrButton->setText("AC");

	inputLabel1 = new QLabel(tr("Matrix1"));
	inputLabel2 = new QLabel(tr("Matrix2"));
	outputLabel = new QLabel(tr("Result"));
	remindLabel = new QLabel(tr("Note whether match of two matrix when do multiply"));//Note whether match of two matrix when do multiply注意乘法时两个矩阵是否匹配

	input1 = new QTextEdit;
	input2 = new QTextEdit;
	output = new QTextEdit;

	capLabel = new QLabel(tr("Huge of Matrix(eg. 3*2)"));
	capacity = new QLineEdit;

	QGridLayout *mainLayout = new QGridLayout(this);
	mainLayout->addWidget(capLabel, 0, 0, 1, 1);
	mainLayout->addWidget(capacity, 0, 1, 1, 1);
	mainLayout->addWidget(inputLabel1, 1, 0, 1, 1);
	mainLayout->addWidget(input1, 1, 1, 1, 1);
	mainLayout->addWidget(inputLabel2, 2, 0, 1, 1);
	mainLayout->addWidget(input2, 2, 1, 1, 1);
	mainLayout->addWidget(outputLabel, 3, 0, 1, 1);
	mainLayout->addWidget(output, 3, 1, 1, 1);
	mainLayout->addWidget(addButton, 4, 0, 1, 1);
	mainLayout->addWidget(subButton, 4, 1, 1, 1);
	mainLayout->addWidget(muliButton, 5, 0, 1, 1);
	mainLayout->addWidget(retButton, 4, 4, 1, 1);
	mainLayout->addWidget(clrButton, 5, 4, 1, 1);
	mainLayout->addWidget(remindLabel, 5, 1, 1, 1);

	connect(addButton, SIGNAL(clicked(bool)), this, SLOT(buttonAddClick()));
	connect(subButton, SIGNAL(clicked(bool)), this, SLOT(buttonSubtractClick()));
	connect(muliButton, SIGNAL(clicked(bool)), this, SLOT(buttonMultiplyClick()));
	connect(clrButton, SIGNAL(clicked(bool)), this, SLOT(buttonClearClick()));
	connect(retButton, &QPushButton::clicked, this, &Matrix::buttonLoginbackClick);

}

Matrix::~Matrix()
{
}

void Matrix::buttonLoginbackClick()
{
	accept();
}

void Matrix::buttonClearClick()
{
	capacity->setText("");
	input1->setText("");
	input2->setText("");
	output->setText("");
}

void Matrix::buttonAddClick()
{
	//以下代码可能可以重构成一个函数
	QString str0 = capacity->text();
	QStringList list0 = str0.split("*", QString::SkipEmptyParts);
	int M, N;
	str0 = list0.at(0);
	M = str0.toInt();
	str0 = list0.at(1);
	N = str0.toInt();

	//读取矩阵1里面的数据
	//数组开太大会导致堆栈超限的crash
	QString str1 = input1->toPlainText();
	QStringList num_str1 = str1.split("\n");//按照换行标记分割
	QString num[100];//矩阵行数为最大为100
	double A[100][100];//矩阵最大可以开到100*100

	for (int i = 0; i<M; ++i) {
		num[i] = num_str1.at(i);
		QStringList num_str2 = num[i].split(" ");
		for (int j = 0; j<N; ++j) {
			str0 = num_str2.at(j);
			int count = str0.toDouble();
			A[i][j] = count;
		}
	}
	//以上代码可以思考重构

	QString str2 = input2->toPlainText();
	QStringList num_str2 = str2.split("\n");//按照换行标记分割
	QString num1[100];//矩阵行数为最大为100
	double B[100][100];//矩阵最大可以开到100*100

	for (int i = 0; i<M; ++i) {
		num1[i] = num_str2.at(i);
		QStringList num_str3 = num1[i].split(" ");
		for (int j = 0; j<N; ++j) {
			str0 = num_str3.at(j);
			int count = str0.toDouble();
			B[i][j] = count;
		}
	}
	//以上代码可以思考重构
	QString out = "";
	QString tmp;
	for (int i = 0; i<M; ++i) {
		for (int j = 0; j<N; ++j) {
			A[i][j] += B[i][j];
			tmp.setNum(A[i][j]);
			out += tmp;
			out += " ";
		}
		out += "\n";
	}
	output->setText(out);
}

void Matrix::buttonSubtractClick()
{
	//以下代码可能可以重构成一个函数
	QString str0 = capacity->text();
	QStringList list0 = str0.split("*", QString::SkipEmptyParts);
	int M, N;
	str0 = list0.at(0);
	M = str0.toInt();
	str0 = list0.at(1);
	N = str0.toInt();

	//读取矩阵1里面的数据
	//数组开太大会导致堆栈超限的crash
	QString str1 = input1->toPlainText();
	QStringList num_str1 = str1.split("\n");//按照换行标记分割
	QString num[100];//矩阵行数为最大为100
	double A[100][100];//矩阵最大可以开到100*100

	for (int i = 0; i<M; ++i) {
		num[i] = num_str1.at(i);
		QStringList num_str2 = num[i].split(" ");
		for (int j = 0; j<N; ++j) {
			str0 = num_str2.at(j);
			int count = str0.toDouble();
			A[i][j] = count;
		}
	}
	//以上代码可以思考重构

	QString str2 = input2->toPlainText();
	QStringList num_str2 = str2.split("\n");//按照换行标记分割
	QString num1[100];//矩阵行数为最大为100
	double B[100][100];//矩阵最大可以开到100*100

	for (int i = 0; i<M; ++i) {
		num1[i] = num_str2.at(i);
		QStringList num_str3 = num1[i].split(" ");
		for (int j = 0; j<N; ++j) {
			str0 = num_str3.at(j);
			int count = str0.toDouble();
			B[i][j] = count;
		}
	}
	//以上代码可以思考重构
	QString out = "";
	QString tmp;
	for (int i = 0; i<M; ++i) {
		for (int j = 0; j<N; ++j) {
			A[i][j] -= B[i][j];
			tmp.setNum(A[i][j]);
			out += tmp;
			out += " ";
		}
		out += "\n";
	}
	output->setText(out);
}

void Matrix::buttonMultiplyClick()
{
	//以下代码可能可以重构成一个函数
	QString str0 = capacity->text();
	QStringList list0 = str0.split("*", QString::SkipEmptyParts);
	int M, N;
	str0 = list0.at(0);
	M = str0.toInt();
	str0 = list0.at(1);
	N = str0.toInt();

	//读取矩阵1里面的数据
	//数组开太大会导致堆栈超限的crash
	QString str1 = input1->toPlainText();
	QStringList num_str1 = str1.split("\n");//按照换行标记分割
	QString num[100];//矩阵行数为最大为100
	double A[100][100] = { 0 };//矩阵最大可以开到100*100

	for (int i = 0; i<M; ++i) {
		num[i] = num_str1.at(i);
		QStringList num_str2 = num[i].split(" ");
		for (int j = 0; j<N; ++j) {
			str0 = num_str2.at(j);
			int count = str0.toDouble();
			A[i][j] = count;
		}
	}
	//以上代码可以思考重构

	QString str2 = input2->toPlainText();
	QStringList num_str2 = str2.split("\n");//按照换行标记分割
	QString num1[100];//矩阵行数为最大为100
	double B[100][100] = { 0 };//矩阵最大可以开到100*100

	for (int i = 0; i<N; ++i) {
		num1[i] = num_str2.at(i);
		QStringList num_str3 = num1[i].split(" ");
		for (int j = 0; j<M; ++j) {
			str0 = num_str3.at(j);
			int count = str0.toDouble();
			B[i][j] = count;
		}
	}
	//以上代码可以思考重构
	QString out = "";
	QString tmp;
	double newMaxtrix[100][100] = { 0 };
	for (int i = 0; i < M; ++i)
		for (int j = 0; j < M; ++j)
			for (int k = 0; k < N; ++k)
				newMaxtrix[i][j] += A[i][k] * B[k][j];
	for (int i = 0; i<M; ++i) {
		for (int j = 0; j<M; ++j) {
			tmp.setNum(newMaxtrix[i][j]);
			out += tmp;
			out += " ";
		}
		out += "\n";
	}
	output->setText(out);
}

