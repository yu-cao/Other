#include "integral.h"
#include<QGridLayout>
#include<vector>
#include<string>
#include<qstack.h>

using namespace std;
double fun(double x, const char *S);
//template<class T>
double romberg(/*const T &f,*/ double a, double b,const char *S, double eps = 1e-8)
{
	//a是上界
	vector<double> t;
	double h = b - a, last, curr;
	int k = 1, i = 1;
	t.push_back(h*(fun(a,S) + fun(b,S)) / 2);//梯形
	do {
		last = t.back();
		curr = 0;
		double x = a + h / 2;
		for (int j = 0; j < k; ++j) {
			curr += fun(x,S);
			x += h;
		}
		curr = (t[0] + h * curr) / 2;
		double k1 = 4.0 / 3.0, k2 = 1.0 / 3.0;
		for (int j = 0; j < i; j++) {
			double temp = k1 * curr - k2 * t[j];
			t[j] = curr;
			curr = temp;
			k2 /= 4 * k1 - k2;//防止溢出
			k1 = k2 + 1;
		}
		t.push_back(curr);
		k *= 2;
		h /= 2;
		i++;
	} while (fabs(last - curr) > eps);
	return t.back();
}

Integral::Integral(QWidget *parent) :
	QDialog(parent)
{
	this->setWindowTitle(QString("Integral Calculate"));//设置标题

	inputupLabel = new QLabel(tr("Lower Limit:"));//积分上限
	inputdownLabel = new QLabel(tr("Upper Limit:"));//积分下限
	functionLabel = new QLabel(tr("Integrand Function:"));//被积函数
	outputLabel = new QLabel(tr("Result:"));//积分结果

	inputUp = new QLineEdit;
	inputDown = new QLineEdit;
	calculatorFunction = new QLineEdit;
	output = new QLineEdit;

	buttonSovleClick = new QPushButton(this);
	buttonSovleClick->setText(tr("Begin to calculate"));//开始积分

	clrButton = new QPushButton(this);
	clrButton->setText(tr("AC"));//清除

	retButton = new QPushButton(this);
	retButton->setText(tr("Return"));//返回主界面

	QGridLayout *mainLayout = new QGridLayout(this);
	mainLayout->addWidget(inputupLabel, 0, 0, 1, 1);
	mainLayout->addWidget(inputUp, 0, 1, 1, 1);
	mainLayout->addWidget(inputdownLabel, 1, 0, 1, 1);
	mainLayout->addWidget(inputDown, 1, 1, 1, 1);
	mainLayout->addWidget(outputLabel, 3, 0, 1, 1);
	mainLayout->addWidget(output, 3, 1, 1, 1);
	mainLayout->addWidget(functionLabel, 2, 0, 1, 1);
	mainLayout->addWidget(calculatorFunction, 2, 1, 1, 1);
	mainLayout->addWidget(buttonSovleClick, 4, 0, 1, 1);
	mainLayout->addWidget(clrButton, 4, 1, 1, 1);
	mainLayout->addWidget(retButton, 4, 2, 1, 1);

	connect(clrButton, SIGNAL(clicked(bool)), this, SLOT(buttonACClick()));
	connect(retButton, &QPushButton::clicked, this, &Integral::buttonLoginbackClick);
	connect(buttonSovleClick, SIGNAL(clicked(bool)), this, SLOT(buttonSolveClick()));

}

Integral::~Integral()
{
}

void Integral::buttonLoginbackClick()
{
	accept();
}

void Integral::buttonACClick()
{
	inputUp->setText("");
	inputDown->setText("");
	calculatorFunction->setText("");
	output->setText("");
}

void Integral::buttonSolveClick()
{
	//输入函数那一栏函数必须是关于x的表达式
	//但注意，如x平方不能输入成为x^2而必须是x*x

	QString str0 = inputUp->text();
	QString str1 = inputDown->text();
	int M, N;
	M = str0.toInt();
	N = str1.toInt();


	QString str2 = calculatorFunction->text();
	std::string str = str2.toStdString();
	const char *S = str.c_str();

	double ans = romberg(M, N, S);
	QString answer;
	answer.setNum(ans);
	output->setText(answer);

}

//应该是const char *S，现在在测试
double fun(double x, const char *S)
{
	int OPS[50] = { 0 }, len;
	//char S[] = "-3*x*(-x*x-x+3)";
	QStack<char> OPE;
	int i, j = 0;
	for (i = 0; i < strlen(S); i++)
	{
		switch (S[i])
		{
		case'+':
			if (OPE.isEmpty())
				OPE.push(S[i]);
			//因为减数与被减数的处理必须先行完成才能避免后续混乱，所以-优先级比+要高一点
			else if (OPE.top() == '*' || OPE.top() == '/'|| OPE.top() == '-')
			{
				OPS[j++] = OPE.pop();
				i--;
			}
			else
				OPE.push(S[i]);
			break;
		case'-':
			if ('(' != S[i - 1] && 0 != i)
			{
				if (OPE.isEmpty())
					OPE.push(S[i]);
				else if (OPE.top() == '*' || OPE.top() == '/'||OPE.top()=='-')
				{
					OPS[j++] = OPE.pop();
					i--;
				}
				else
					OPE.push(S[i]);
			}
			else
			{
				//处理例如*(-x+3)这种情况
				while ((S[i] >= '0'&&S[i] <= '9') || S[i] == 'x' || ('-' == S[i] && !(S[i - 1] >= '0' && S[i - 1] <= '9' || S[i - 1] == 'x')))
				{
					OPS[j++] = S[i];
					if ('-' == S[i])
						OPS[j++] = '@';
					i++;
				}
				i--;
				OPS[j++] = '#';  //数字中的间隔符
			}
			break;
		case'*':
		case'/':
			OPE.push(S[i]);
			break;
		case'(':
			OPE.push(S[i]);
			break;
		case')':
			while (OPE.top() != '(')
			{
				OPS[j++] = OPE.pop();
			}
			OPE.pop();
			break;
		default:
			while (S[i] >= '0'&&S[i] <= '9' || S[i] == 'x' || ('-' == S[i] && !(S[i - 1] >= '0' && S[i - 1] <= '9' || S[i - 1] == 'x')))
			{
				OPS[j++] = S[i++];
			}
			i--;//为了适应主循环的i++
			OPS[j++] = '#';  //数字中的间隔符
			break;
		}
	}
	while (!OPE.isEmpty())
	{
		OPS[j++] = OPE.pop();
	}
	len = j;

	double a;
	double b;
	double c;
	QStack<double>SZ;
	for (i = 0; i < len; i++)
	{
		switch (OPS[i])
		{
		case'+':
		{
			//a,b已经是压入堆栈的数
			a = SZ.pop();
			b = SZ.pop();
			c = a + b;
			SZ.push(c);
		}
		break;
		case'-':
		{
			//@是对于负数的处理，即-@x代表着-x
			if ('@' != OPS[i + 1])
			{
				a = SZ.pop();
				b = SZ.pop();
				c = b - a;//注意，先pop出来的是减数

				SZ.push(c);
			}
			else
			{
				int jx = 0;
				double dx;
				char *stx = new char[10];
				while (OPS[i] != '#')
				{
					if ('@' != OPS[i])
						stx[jx++] = OPS[i];
					i++;

				}
				//处理跟x有关的情况
				if (OPS[i - 1] != 'x') {
					dx = atof(stx);
				}
				else {
					dx = -x;
				}

				SZ.push(dx);
				delete stx;
			}
		}
		break;
		case'*':
		{
			a = SZ.pop();
			b = SZ.pop();
			c = a * b;

			SZ.push(c);
		}
		break;
		case'/':
		{
			a = SZ.pop();
			b = SZ.pop();

			//不允许分母==0的情况
			c = b / a;

			SZ.push(c);
		}
		break;
		default:
			int j = 0;
			double d;
			char *st = new char[10];
			while (OPS[i] != '#')
			{
				st[j++] = OPS[i];
				i++;
			}
			//处理跟x有关的情况
			if (OPS[i - 1] != 'x') {
				d = atof(st);
			}
			else {
				d = x;
			}
			SZ.push(d);
			delete st;
			break;
		}
	}
	double result = SZ.top();
	return result;
}

