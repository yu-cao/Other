#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow;
class QPushButton;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
	QPushButton *pFunction1;
	QPushButton *pFunction2;
	QPushButton *pFunction3;
	QPushButton *pFunction4;
	QPushButton *pFunction5;

private slots:
	void To_f1();
	void To_f2();
	void To_f3();
	void To_f4();
	void To_f5();
};

#endif // MAINWINDOW_H
