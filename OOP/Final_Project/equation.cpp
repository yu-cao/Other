#include "Equation.h"
#include<vector>
using namespace std;
vector <double>equation(vector<double> coef, int n);

Equation::Equation(QWidget *parent) :
	QDialog(parent)
{
	this->setWindowTitle(QString("Solve Equation"));//设置标题

	inputLabel = new QLabel(tr("Polynomial:"));
	outputLabel = new QLabel(tr("X="));
	inputLine = new QLineEdit;
	outputLine = new QLineEdit;

	retButton = new QPushButton(this);
	retButton->setText(tr("Return"));

	solveButton = new QPushButton(this);
	solveButton->setText(tr("Solve the Equation"));

	clrButton = new QPushButton(this);
	clrButton->setText(tr("AC"));

	QGridLayout *mainLayout = new QGridLayout(this);
	mainLayout->addWidget(inputLabel, 2, 0, 1, 1);
	mainLayout->addWidget(inputLine, 2, 1, 1, 1);
	mainLayout->addWidget(outputLabel, 2, 2, 1, 1);
	mainLayout->addWidget(outputLine, 2, 3, 1, 1);
	mainLayout->addWidget(solveButton, 3, 2, 1, 1);
	mainLayout->addWidget(clrButton, 3, 0, 1, 1);
	mainLayout->addWidget(retButton, 3, 3, 1, 1);

	connect(retButton, &QPushButton::clicked, this, &Equation::buttonLoginbackClick);
	connect(solveButton, SIGNAL(clicked(bool)), this, SLOT(buttonSovleClick()));
	connect(clrButton, SIGNAL(clicked(bool)), this, SLOT(buttonACClick()));
}

Equation::~Equation()
{
}
void Equation::buttonLoginbackClick()
{
	accept();
}

void Equation::buttonACClick()
{
	inputLine->setText("");
	outputLine->setText("");
}

const double EPS = 1E-12;//精确度
const double inf = 1E+12;//定义的无穷大

void Equation::buttonSovleClick()
{
	vector<double> coef;
	QString str = inputLine->text();
	QStringList list = str.split(" ", QString::SkipEmptyParts);

	double number;
	QString s = list.at(0);
	double N = s.toInt();//读取第一个数，代表最高次
	for (int i = 1; i <= N + 1; ++i) {
		s = list.at(i);
		number = s.toDouble();
		coef.push_back(number);
	}
	vector <double>result;

	result = equation(coef, N);

	//输出到Output里面
	QString out = "";
	if (result.size() == 0) {
		out += "Can't solve.";
		outputLine->setText(out);
	}
	else {
		for (int i = 0; i<result.size(); ++i) {
			double op = result.at(i);
			QString str2;
			str2.setNum(op);
			out += str2;
			out += " ";
		}
		outputLine->setText(out);
	}

}


int sign(double x)
{
	return x<-EPS ? -1 : x>EPS;
}

double get(const vector <double>&coef, double x)
{
	double e = 1, s = 0;
	for (int i = 0; i < coef.size(); ++i) s += coef[i] * e, e *= x;
	return s;
}

double find(const vector<double>&coef, int n, double lo, double hi)
{
	double sign_lo, sign_hi;
	if ((sign_lo = sign(get(coef, lo))) == 0) return lo;
	if ((sign_hi = sign(get(coef, hi))) == 0) return hi;
	if (sign_lo * sign_hi > 0) return inf;
	for (int step = 0; step < 100 && hi - lo > EPS; ++step) {
		double m = (lo + hi)* .5;
		int sign_mid = sign(get(coef, m));
		if (sign_mid == 0) return m;
		if (sign_lo * sign_mid < 0) {
			hi = m;
		}
		else {
			lo = m;
		}
	}
	return (lo + hi)* .5;
}

vector <double>equation(vector<double> coef, int n)
{
	vector<double> ret;
	if (n == 1) {
		if (sign(coef[1])) ret.push_back(-coef[0] / coef[1]);
		return ret;
	}
	vector<double> dcoef(n);
	for (int i = 0; i < n; ++i) dcoef[i] = coef[i + 1] * (i + 1);
	vector<double> droot = equation(dcoef, n - 1);
	droot.insert(droot.begin(), -inf);
	droot.push_back(inf);
	for (int i = 0; i + 1 < droot.size(); ++i) {
		double tmp = find(coef, n, droot[i], droot[i + 1]);
		if (tmp < inf) ret.push_back(tmp);
	}
	return ret;
}
