#ifndef INTEGRAL_H
#define INTEGRAL_H

#include <QDialog>
#include<QPushButton>
#include<QLabel>
#include<QLineEdit>

class Integral : public QDialog
{
	Q_OBJECT
private:
	QLabel *inputupLabel;
	QLabel *inputdownLabel;
	QLabel *functionLabel;
	QLineEdit *inputUp;
	QLineEdit *inputDown;
	QLineEdit *calculatorFunction;

	QLabel *outputLabel;
	QLineEdit *output;

	QPushButton *buttonSovleClick;
	QPushButton *retButton;
	QPushButton *clrButton;

public:
	explicit Integral(QWidget *parent = 0);
	~Integral();

private slots:
	void buttonSolveClick();
	void buttonLoginbackClick();
	void buttonACClick();
};

#endif // INTEGRAL_H
