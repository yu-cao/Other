#ifndef CALCULATOR_H
#define CALCULATOR_H

#include<QDialog>
#include<QPushButton>
#include<QLineEdit>
#include<QHBoxLayout>
#include<QVBoxLayout>
#include<QGridLayout>
#include<QString>
#include<QStack>
#include<QTime>

class Calculator;

class Calculator : public QDialog
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = 0);
    ~Calculator();

private:
	QLineEdit * inputLine;//显示栏
	QString input = "";//输入

	//以下是0~9的按钮
	QPushButton *zeroButton;
	QPushButton *oneButton;
	QPushButton *twoButton;
	QPushButton *threeButton;
	QPushButton *fourButton;
	QPushButton *fiveButton;
	QPushButton *sixButton;
	QPushButton *sevenButton;
	QPushButton *eightButton;
	QPushButton *nineButton;

	//以下是操作按钮界面
	QPushButton *addButton;//加法
	QPushButton *subButton;//减法
	QPushButton *divButton;//除法
	QPushButton *mulButton;//乘法
	QPushButton *equButton;//等于（运算）
	QPushButton *timButton;//显示计算机当前的时间
	QPushButton *decButton;//小数点
	QPushButton *botButton;//正负号
	QPushButton *retButton;//返回主界面

	QPushButton *CEButton;
	QPushButton *ACButton;
	QPushButton *lefButton;
	QPushButton *rigButton;

private slots:
	void buttonLoginbackClick();//返回主界面

	//0~9按钮的信号槽
	void buttonZeroClicked();
	void buttonOneClicked();
	void buttonTwoClicked();
	void buttonThreeClicked();
	void buttonFourClicked();
	void buttonFiveClicked();
	void buttonSixClicked();
	void buttonSevenClicked();
	void buttonEightClicked();
	void buttonNineClicked();

	//操作面板
	void buttonAddClicked();//+
	void buttonSubClicked();//-
	void buttonMulClicked();//乘
	void buttonDivClicked();//除
	void buttonTimClicked();//时间
	void buttonDecClicked();//小数点
	void buttonBotClicked();//加减号

	void buttonEquClicked();//等于号

	void buttonLefClicked();//左括号
	void buttonRigClicked();//右括号

	void buttonCEClicked();//CE
	void buttonACClicked();//AC
};

#endif // CALCULATOR_H
