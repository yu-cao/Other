#include "mainwindow.h"
#include<QDialog>
#include<QPushButton>
#include"calculator.h"
#include"equation.h"
#include"sort.h"
#include"matrix.h"
#include"integral.h"
#include<QGridLayout>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent)
{
	QWidget *widget = new QWidget; //构建一个QWidget布局将设置的布局添加进这个QWidget
	this->setCentralWidget(widget);

	pFunction1 = new QPushButton(this);
	pFunction1->setText(tr("Calculator"));

	pFunction2 = new QPushButton(this);
	pFunction2->setText(tr("Solve Equation"));

	pFunction3 = new QPushButton(this);
	pFunction3->setText(tr("Sorting Digital Array"));

	pFunction4 = new QPushButton(this);
	pFunction4->setText(tr("Matrix Calculate"));

	pFunction5 = new QPushButton(this);
	pFunction5->setText(tr("Integral Calculate"));

	QGridLayout *mainLayout = new QGridLayout(this);

	mainLayout->addWidget(pFunction1, 0, 0, 1, 1);
	mainLayout->addWidget(pFunction2, 0, 3, 1, 1);
	mainLayout->addWidget(pFunction3, 1, 0, 1, 1);
	mainLayout->addWidget(pFunction4, 1, 3, 1, 1);
	mainLayout->addWidget(pFunction5, 2, 0, 1, 1);

	widget->setLayout(mainLayout);

	connect(pFunction1, &QPushButton::clicked, this, &MainWindow::To_f1);
	connect(pFunction2, &QPushButton::clicked, this, &MainWindow::To_f2);
	connect(pFunction3, &QPushButton::clicked, this, &MainWindow::To_f3);
	connect(pFunction4, &QPushButton::clicked, this, &MainWindow::To_f4);
	connect(pFunction5, &QPushButton::clicked, this, &MainWindow::To_f5);

}

MainWindow::~MainWindow()
{
}

void MainWindow::To_f1()
{
	close();
	Calculator dlg;
	if (dlg.exec() == QDialog::Accepted) show();
}

void MainWindow::To_f2()
{
	close();
	Equation dlg;
	if (dlg.exec() == QDialog::Accepted) show();
}

void MainWindow::To_f3()
{
	close();
	Sort dlg;
	if (dlg.exec() == QDialog::Accepted) show();
}

void MainWindow::To_f4()
{
	close();
	Matrix dlg;
	if (dlg.exec() == QDialog::Accepted) show();
}

void MainWindow::To_f5()
{
	close();
	Integral dlg;
	if (dlg.exec() == QDialog::Accepted) show();
}

