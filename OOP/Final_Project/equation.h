#ifndef EQUATION_H
#define EQUATION_H
#include<vector>
#include<QDialog>
#include<QPushButton>
#include<QLineEdit>
#include<QGridLayout>
#include<QLabel>

class Equation;

class Equation : public QDialog
{
	Q_OBJECT

public:
	explicit Equation(QWidget *parent = 0);
	~Equation();

private:
	QLabel *inputLabel;
	QLabel *outputLabel;
	QLineEdit *inputLine;
	QLineEdit *outputLine;
	QPushButton *solveButton;//求解按钮
	QPushButton *clrButton;//清空按钮
	QPushButton *retButton;//返回按钮

private slots:
	void buttonLoginbackClick();//返回到主窗口
	void buttonSovleClick();//开始解方程
	void buttonACClick();//清除输入与输出

};

#endif // FUNCTION2_H
