#ifndef SORT_H
#define SORT_H

#include <QDialog>
#include<QPushButton>
#include<QLineEdit>
#include<QLabel>

class Sort;

class Sort : public QDialog
{
	Q_OBJECT

public:
	explicit Sort(QWidget *parent = 0);
	~Sort();

private:
	QLineEdit *input;
	QLabel *inputLabel;

	QLineEdit *output;
	QLabel *outputLabel;

	QLabel *number;
	QLineEdit *num;

	QPushButton *retButton;
	QPushButton *clsButton;
	QPushButton *sortIncreaseButton;
	QPushButton *sortDecreaseButton;

private slots:
	void buttonLoginbackClick();
	void buttonSortInClick();//按下按钮后按照升序排序
	void buttonSortDeClick();//按下按钮后按照降序排序
	void buttonACClick();//清除所有输入与输出

};

#endif // SORT_H
