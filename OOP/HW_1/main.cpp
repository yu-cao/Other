/*********************************************************
@Cao YU       2017/10/01      
@Student ID: 3160100440
@Heterogeneous linked list implementation in C           
**********************************************************/


/*关于作业01有几点请大家注意一下：
1.注意题目要求，是异质链表，可以存储不同类型的数据，比如一个node里面的数据类型可以是int，也可以是float（写两种就够了...)。
2.关于操作，插入，删除，打印这三个操作是最基本的，请不要漏掉，要考虑不同的数据类型。
3.实验报告记得要附上运行截图，另外最好将源代码单独放在一个文件以.cpp为结尾（实验报告里面保留着）。
*/

#include<stdio.h>
#include<stdlib.h>
#include"Head.h"

//在此程序中，使用三个不同的数据类型作为演示
#define INTEGER 0
#define CHARACTER 1
#define DOUBLE 2

//警告：程序中链表操作必须存在哨兵节点（sentinel node），否则将会丢失第一个节点数据
//本程序中对哨兵节点下标为0，第一个节点下标为1，以此类推
int main(void)
{
	struct Node* p = MakeList();
	struct Node* q = MakeList();
	int list_length;

	//以下的apple，banana，name，mz均为测试数据，仅作为测试用途，故未按照规范命名
	int apple = 10;
	int banana = 13;
	char name = 't';
	char mz = 'g';
	double d = 0.12;
	//展现创建完一个空链表的情况
	printf("p:");
	printf("The list of NULL\n");
	PrintList(p);
	system("pause");
	
	printf("Insert element 10 behind the dummy node.\n");
	p = InsertKList(p, INTEGER, &apple, 1);
	PrintList(p);
	system("pause");

	printf("Insert element g between the dummy node and the first node\n");
	p = InsertKList(p, CHARACTER, &mz, 1);
	PrintList(p);	
	system("pause");

	printf("Insert element 0.12 in index 2(Between g and 10)\n");
	p = InsertKList(p, DOUBLE, &d, 2);
	PrintList(p);
	system("pause");

	printf("Check the length of the list\n");
	list_length = GetListLength(p);
	printf("%d\n", list_length);
	system("pause");

	printf("Find the index of element g in the list\n");
	printf("%d\n", SearchXIndex(p, CHARACTER, &mz));
	system("pause");

	printf("Find the second node of element\n");
	q = SearchKth(p, 2);
	PrintList(q);
	system("pause");

	//随意对链表q插入一些元素
	q = InsertKList(q, CHARACTER, &mz, 1);
	q = InsertKList(q, INTEGER, &apple, 2);

	printf("p:");
	PrintList(p);
	printf("\nq:");
	PrintList(q);
	printf("\nUnit the two list:\n");
	p = UnionList(p, q);
	PrintList(p);
	system("pause");

	printf("Delete the element in index 2\n");
	p = DeleteKth(p, 2);
	PrintList(p);
	system("pause");

	printf("Reverse the list\n");
	p = ReverseList(p);
	PrintList(p);
	system("pause");

	printf("Destroy the list\n");
	p=DestroyList(p);
	PrintList(p);
	printf("\n");

	return 0;
}

//建立一个空的链表头（sentinel node）
struct Node* MakeList(void)
{
	struct Node* head_node;

	head_node = (struct Node*)malloc(sizeof(struct Node));

	head_node->pNext = NULL;

	return head_node;

}

//在第K个节点前插入一个节点  
//K>0&&K<=链表总结点数    
//假设K==1，等于在哨兵节点后面插入一个新的节点
//注意：此处Data必须为指针形式
//注意：K的输入必须小于链表长度，不然将会返回一条错误语句
//警告：传入函数中的Data必须与Type相对应，否则可能会出现不可预料的情况
//Correct: p=InsertKList(list,type,&data,k);
//ERROR: p=InsertKList(list,type,data,k);
//ERROR: p=InsertKList(list,INTEGER,10,k);
//ERROR: p=InsertKList(list,CHARACTER,&data,k);当此时data数据为int或double时
struct Node* InsertKList(struct Node* list, int Type,void* Data,int K) 
{
	struct Node* new_node;
	struct Node* p=list;

	if (K > GetListLength(list)+1) {
		printf("ERROR:K is bigger than the (index+1) of last node!");
	}

	new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->ElementType = Type;
	new_node->pData = Data;
	new_node->pNext = NULL;

	if (K == 1) {
		new_node->pNext = list->pNext;
		list->pNext = new_node;
		return list;
	}

	for (; K != 1;K--) {
		p = p->pNext;
	}

	new_node->pNext = p->pNext;
	p->pNext = new_node;
	
	return list;
}

//获得链表的长度，返回为链表的长度
//如果返回值是0，意味着链表为空链表
int GetListLength(struct Node* list)
{
	int length_count=0;
	struct Node* p=list->pNext;

	for (;p!=NULL;length_count++) {
		p = p->pNext;
	}

	return length_count;
}

//寻找一个特定的数据X，返回该数据所在的节点下标（index）
//如果搜寻不到，将会返回一条NOT FOUND的错误
//注意：此处Data必须为指针形式
//警告：传入函数中的Data必须与Type相对应，否则会返回NOT FOUND的错误或者出现其他不可预料的情况
//Correct: int_store=SearchXIndex(list,type,&data);
//ERROR: int_store=SearchXIndex(list,type,data);
//ERROR: int_store=SearchXIndex(list,type,10);
int SearchXIndex(struct Node* list, int Type, void* Data)
{
	struct Node* p= list;
	int count = 0;//Record the index of node

	for (;p->pData != Data&&p->ElementType!=Type;) {
		p = p->pNext;
		count++;
		if (p == NULL) {
			printf("ERROR:Not Found!");
			return 0;
		}
	}
	return count;

}

//找到第K个节点下标所对应的地址，并返回该地址
struct Node* SearchKth(struct Node* list, int K) 
{
	struct Node* p=list;
	struct Node* q;
	q = MakeList();

	int count;
	for (count = 1; count != K; count++) {
		p = p->pNext;
	}

	q = InsertKList(q, p->pNext->ElementType, p->pNext->pData, 1);

	return q;
}

//删除第K个节点，并返回哨兵节点的指针
struct Node* DeleteKth(struct Node* list, int K)
{
	int count;
	struct Node* p=list->pNext;
	struct Node* pre = list;

	if (K > GetListLength(list)) {
		printf("ERROR:K is bigger than the index of last node!");
		return list;
	}

	for (count = 1; count != K; count++) {
		p = p->pNext;
		pre = pre->pNext;
	}
	pre->pNext = p->pNext;
	free(p);

	return list;

}

//删除整个链表，并反问哨兵节点的指针（此时应指向NULL）
struct Node* DestroyList(struct Node* list)
{
	struct Node* p=list;
	struct Node* q=list;

	for (; p != NULL;) {
		list = list->pNext;
		free(p);
		p = list;
	}

	q->pNext = NULL;
	return q;
}

//将两个链表拼接起来，返回哨兵节点的指针
//拼接方式为list2拼接到list1的后面
struct Node* UnionList(struct Node* list1, struct Node* list2)
{
	struct Node* p = list1;
	for (; p->pNext != NULL;) {
		p = p->pNext;
	}
	p->pNext = list2->pNext;

	return list1;
}

//打印输出整个链表
void PrintList(struct Node* list)
{
	if (list == NULL||list->pNext==NULL) {
		printf("NULL ");
	}
	else {
		for (list=list->pNext; list != NULL;) {
			if (list->ElementType == INTEGER) {
				printf("%d ", *(int*)list->pData);
			}
			else if (list->ElementType == CHARACTER) {
				printf("%c ", *(char*)list->pData);
			}
			else if (list->ElementType == DOUBLE) {
				printf("%lf ", *(double*)list->pData);
			}
			list = list->pNext;
		}
	}
}

//反转整个链表
struct Node* ReverseList(struct Node* list)
{
	struct Node* p;
	struct Node* pre;

	if (list->pNext == NULL) {
		return list;
	}

	if (list->pNext->pNext == NULL) {
		return list;
	}

	p = list->pNext->pNext;
	pre = p;
	list->pNext->pNext = NULL;

	for (; p != NULL;) {
		p = p->pNext;
		pre->pNext = list->pNext;
		list->pNext = pre;
		pre = p;
	}

	return list;
}