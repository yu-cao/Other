struct Node* MakeList(void);
struct Node* InsertKList(struct Node* list, int Type, void* Data, int K);
int GetListLength(struct Node* list);
int SearchXIndex(struct Node* list, int Type, void* Data);
struct Node* SearchKth(struct Node* list, int K);
struct Node* DeleteKth(struct Node* list, int K);
struct Node* DestroyList(struct Node* list);
struct Node* UnionList(struct Node* list1, struct Node* list2);
void PrintList(struct Node* list);
struct Node* ReverseList(struct Node* list);

struct Node {
	void * pData;//储存指向具体数据的指针
	int ElementType;//指明结构中储藏的数据类型
	struct Node * pNext;
};