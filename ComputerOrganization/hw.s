#编写一个MIPS过程来将有符号整数（2的补码）转换为在 $v0 中返回的正确归一化的浮点值。
#Write a MIPS procedure to convert a signed integer (2’s complement) to a properly normalized floating point value returned in $v0.
#即为int to float

#a0是int的传入值
int2float:
    addi    $s0,$zero,31    #准备判断int的前面31位里面有没有出现1导致最高位

    add     $s3,$zero,$a0   #将$a0保存起来为$s3

    addi    $s1,$zero,1
    sll     $s1,$s1,31      #将$s1移动到符号位上
    and     $s4,$s1,$s3     
    beq		$zero,$s4,while	#如果等于0，代表int是正数，不用对符号位进行赋值操作
    addi    $s5,$zero,1
    sll     $s5,$s5,31      #对符号位进行赋值，结果存在$s5中
    
while:
    srl     $s6,$s6,1       #从最初的符号位开始向右移动
    addi    $s0,$s0,-1      #循环count
    and     $t6,$s6,$s3     #通过and操作来确定该位是否为1，并保存在$t6中

    bne     $t0,$zero,judge #当$t0是1时，进行判断是否已经小于了23

out2:#没有小于23位
    add     $t0,$zero,s0
    addi    $t1,$t1,31
    sub     $t2,$t1,$t0     #代表最高有效位前面的位数
    sllv    $s7,$s3,$t2
    srl     $s7,$s7,9       #确定了最后23位

    addi    $t3,$t3,127     #偏码
    add		$t3,$s0,$t3		#补上正常的
    sll     $t3,$t3,23
    add     $s7,$s7,$t3     #补充阶码
    add     $v0,$s5,$s7     #拼接上符号位
    j		EXIT			# jump to EXIT
    
judge:
    slti    $t1,$s0,23      #当$s0<23时，即为后面的只剩下23位了，此时$t1=1
    bne     $t1,$zero,out1  #当$t1==1时，前往out1
    j		$ra				#当没有小于23位时，跳回去

out1:#小于23位，直接取后23位解决

    add     $s2,$zero,$a0   
    sll     $s2,$s2,9       
    srl     $s2,$s2,9       #将$a0的后23位取出来
    add     $t0,$t0,$s2     #将$a0的后23为先存入一个临时寄存器$t0中

    addi    $t3,$t3,127     #偏码
    add		$t3,$s0,$t3		#补上正常的
    sll     $t3,$t3,23
    add     $t0,$t0,$t3     #补充阶码
    add     $v0,$s5,$t0     #拼接上符号位
    j		EXIT			# jump to EXIT

EXIT:

