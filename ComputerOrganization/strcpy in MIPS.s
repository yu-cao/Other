void strcpy(char x[], char y[])
{
    int i;

    i = 0;
    while((x[i] = y[i]) != '\0')
        i += 1;
}

strcpy:
    addi	$sp, $sp, -4
    sw		$s0, 0($sp)		        # save $s0
    add		$s0, $zero, $zero		# $s0 = i = 0
L1:
    add		$t1, $s0, $a1		    # address of y[i] in $t1
    lbu     $t2, 0($t1)             # $t2 = y[i]

    add		$t3, $s0, $a0		    # address of x[i] in $t3
    sb		$t2, 0($t3)             # x[i] = y[i]

    beq		$t2, $zero, L2	        # if y[i] == 0, then L2
    addi	$s0, $s0, 1			    # i++
    j		L1				        # jump to L1
L2:
    lw		$s0, 0($sp)             # y[i] == 0: end of the string
                                    # restore old $s0
    addi	$sp, $sp, 4			    # free the stack
    jr		$ra					    # return