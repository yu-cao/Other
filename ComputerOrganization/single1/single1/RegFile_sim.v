`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:12:54 05/27/2018
// Design Name:   RegFile
// Module Name:   E:/single1/single1/RegFile_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: RegFile
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module RegFile_sim;

	// Inputs
	reg clk;
	reg [4:0] a1;
	reg [4:0] a2;
	reg [4:0] a3;
	reg wr;
	reg [4:0] wreg;
	reg [31:0] wdata;

	// Outputs
	wire [31:0] d1;
	wire [31:0] d2;
	wire [31:0] d3;

	// Instantiate the Unit Under Test (UUT)
	RegFile uut (
		.clk(clk), 
		.a1(a1), 
		.a2(a2), 
		.a3(a3), 
		.d1(d1), 
		.d2(d2), 
		.d3(d3), 
		.wr(wr), 
		.wreg(wreg), 
		.wdata(wdata)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		a1 = 0;
		a2 = 0;
		a3 = 0;
		wr = 0;
		wreg = 0;
		wdata = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		fork
		forever #10 clk=~clk;
		begin
			a1 = 5'b01000;
			a2 = 5'b01001;
			a3 = 5'b10000;
			#100;
			
			wreg = 5'b10000;
			wdata = 5'd10;
			wr = 1;
			#100;
			
			wr = 0;
			wdata = 5'd2;
			a1 = 5'b10000;//确认a1对应的是d1
			#100;
			
			wr = 1;
			#100;
			
			wr = 0;
		end
		join
	end
      
endmodule

