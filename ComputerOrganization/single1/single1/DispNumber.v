`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:12:53 03/15/2018 
// Design Name: 
// Module Name:    DispNumber 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module DispNumber(
    input wire [15:0]HEX,
	 input wire [3:0]LE,
	 input wire [3:0]point,
	 input wire clk,
	 output wire [3:0]AN,
	 output wire [7:0]SEGMENT
    );

	 wire [3:0] o;
    wire V0,V5,leout,btn,pointout;
	 wire [31:0] clkdiv;
	
	 clkdiv  m10 (.clk(clk),.rst(1'b0),.clkdiv(clkdiv[31:0]));

    Mux4to14b  subm0 (.I0(HEX[3:0]), 
                     .I1(HEX[7:4]), 
                     .I2(HEX[11:8]), 
                     .I3(HEX[15:12]), 
                     .s(clkdiv[18:17]), 
                     .o(o[3:0]));					
    Mux4to1  subm1 (.I0(point[0]), 
                   .I1(point[1]), 
                   .I2(point[2]), 
                   .I3(point[3]), 
                   .s(clkdiv[18:17]), 
                   .o(pointout));
    Mux4to1  subm2 (.I0(LE[0]), 
                   .I1(LE[1]), 
                   .I2(LE[2]), 
                   .I3(LE[3]), 
                   .s(clkdiv[18:17]), 
                   .o(leout));				 		 
    Mux4to14b  subm3 (.I0({V5, V5, V5, V0}), 
                     .I1({V5, V5, V0, V5}), 
                     .I2({V5, V0, V5, V5}), 
                     .I3({V0, V5, V5, V5}), 
                     .s(clkdiv[18:17]), 
                     .o(AN[3:0]));					
    MyMC14495  subm4 (.D0(o[0]), 
                      .D1(o[1]), 
                      .D2(o[2]), 
                      .D3(o[3]), 
                      .LE(leout), 
                      .point(pointout), 
                      .A(SEGMENT[0]), 
                      .B(SEGMENT[1]), 
                      .C(SEGMENT[2]), 
                      .D(SEGMENT[3]), 
                      .E(SEGMENT[4]), 
                      .F(SEGMENT[5]), 
                      .G(SEGMENT[6]), 
                      .P(SEGMENT[7]));	
							 
	VCC  line1 (.P(V5));
   GND  line2 (.G(V0));
endmodule
