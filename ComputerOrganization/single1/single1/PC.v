`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:38:20 05/26/2018 
// Design Name: 
// Module Name:    PC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module PC(clk,pc,npc);
	input wire clk;
	input wire [31:0]npc;//�µ�PC��ַ
	output reg [31:0]pc;
	
	initial pc=32'b0;
	always @(posedge clk) begin
		pc=npc;
	end
	
endmodule
