`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:06:16 05/26/2018 
// Design Name: 
// Module Name:    ALUControl 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
// 已通过仿真
// R指令：ALUop：10
// func字段 	操作 	ALU控制信号
// 10 0000 	加法		0010
// 10 0010 	减法		0110
// 10 0100 	and 		0000
// 10 0101 	or			0001
// 10 1010 	slt		0111
// lw/sw指令：ALUop：00
// XX XXXX	lw			0010
// XX XXXX	sw			0010
// beq指令：ALUop：01
// XX XXXX	beq		0110
// addi指令：ALUop:00
// XX XXXX	addi		0010
module ALUControl(ALUop, Func, ALUoper);
input [1:0] ALUop;
input [5:0] Func;
output [2:0] ALUoper;

wire t1,t2,t3;
 
or (t1, Func[0], Func[3]);
and(t2, Func[1], ALUop[1]);
or (ALUoper[2], t2, t3);
and(t3, ALUop[0], ~ALUop[1]);
or (ALUoper[1], ~Func[2], ~ALUop[1]);
and(ALUoper[0], t1, ALUop[1]);


endmodule
