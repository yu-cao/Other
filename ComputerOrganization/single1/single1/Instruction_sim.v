`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:55:05 05/26/2018
// Design Name:   Instruction
// Module Name:   E:/single1/single1/Instruction_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Instruction
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Instruction_sim;

	// Inputs
	reg clk;
	reg [31:0] pc_in;

	// Outputs
	wire [31:0] instruct;

	// Instantiate the Unit Under Test (UUT)
	Instruction uut (
		.clk(clk), 
		.pc_in(pc_in), 
		.instruct(instruct)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		pc_in = 0;
        
		// Add stimulus here
		fork 
		forever #100 clk = ~clk;
		begin
		#100;
		pc_in = 4;
		#100;
		pc_in = 8;
		#100;
		pc_in = 12;
		#100;
		end
		join
	end
      
endmodule

