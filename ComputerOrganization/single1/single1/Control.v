`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:59:33 05/26/2018 
// Design Name: 
// Module Name:    Control 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Control(input wire [5:0]op,
				output wire RegDst,
				output wire Jump,
				output wire ALUsrc,
				output wire [1:0]ALUop,
				output wire MemToReg,
				output wire MemRead,
				output wire MemWrite,
				output wire Branch,
				output wire RegWrite);
wire r,lw,sw,beq,j;	 
wire addi;

and (r, ~op[5], ~op[4], ~op[3], ~op[2], ~op[1], ~op[0]);		// 0000 00
and (lw, op[5], ~op[4], ~op[3], ~op[2], op[1], op[0]);		// 1000 11
and (sw, op[5], ~op[4], op[3], ~op[2], op[1], op[0]); 		// 1010 11
and (beq, ~op[5], ~op[4], ~op[3], op[2], ~op[1], ~op[0]);	// 0001 00
and (j, ~op[5], ~op[4], ~op[3], ~op[2], op[1], ~op[0]); 		// 0000 10
and (addi, ~op[5], ~op[4], op[3], ~op[2], ~op[1], ~op[0]);	// 0010 00

assign RegDst = r;
assign Jump = j;
or (ALUsrc, lw, sw,addi);
assign ALUop[1]=r;
assign ALUop[0]=beq;
assign MemToReg=lw;
assign MemRead=lw;
assign MemWrite=sw;
assign Branch=beq;
or (RegWrite,r,lw,addi);

endmodule
