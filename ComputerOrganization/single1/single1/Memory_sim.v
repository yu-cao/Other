`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:23:34 05/27/2018
// Design Name:   Memory
// Module Name:   E:/single1/single1/Memory_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Memory
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Memory_sim;

	// Inputs
	reg clk;
	reg [31:0] addr;
	reg MemWrite;
	reg [31:0] WData;

	// Outputs
	wire [31:0] RData;

	// Instantiate the Unit Under Test (UUT)
	Memory uut (
		.clk(clk),
		.addr(addr), 
		.MemWrite(MemWrite), 
		.WData(WData), 
		.RData(RData)
	);

	initial begin
		// Initialize Inputs
		addr = 0;
		MemWrite = 0;
		clk = 0;
		WData = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		fork
		forever #10 clk=~clk;
		begin
		addr = 1;
		#100;
		addr = 2;
		#100;
		addr = 3;
		#100;
		addr = 10;
		#100;
		MemWrite = 1;
		#100;
		MemWrite = 0;
		#100;
		addr = 4;
		end
		join

	end
      
endmodule

