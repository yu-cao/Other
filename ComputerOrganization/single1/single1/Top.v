`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:09:31 05/31/2018 
// Design Name: 
// Module Name:    Top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Top(
input wire clk,//主板时钟
input [4:0]SW,//寄存器查看
input choose,//选择PC或者Reg
input next,//下一个指令
output wire [3:0]AN,
output wire [7:0]SEGMENT
    );
	 wire [31:0]out;
	 wire [31:0]PC;
	 wire [31:0]Regshow;
	 
	 Single_CPU m1(.clk(next),.clk_100mhz(clk),.SW(SW),.op(),.OutReg(Regshow),.PCshow(PC),.TEST_ALU_B());
	 
	 MUX32_2x1 m2(.A(PC),.B(Regshow),.sel(choose),.C(out));//choose == 0 展示PC
	 
	 DispNumber m0(.HEX(out[15:0]),.LE(4'b0000),.point(4'b0),.clk(clk),.AN(AN),.SEGMENT(SEGMENT[7:0]));

endmodule
