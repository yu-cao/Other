`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:41:36 05/26/2018 
// Design Name: 
// Module Name:    ShiftLeft_2b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ShiftLeft_2b(input wire [31:0]A,
					output wire [31:0]B
    );
assign B[31:2]=A[29:0];
assign B[1:0]=2'b0;

endmodule
