`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:40:14 05/27/2018
// Design Name:   Control
// Module Name:   E:/single1/single1/Control_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Control
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Control_sim;

	// Inputs
	reg [5:0] op;

	// Outputs
	wire RegDst;
	wire Jump;
	wire ALUsrc;
	wire [1:0] ALUop;
	wire MemToReg;
	wire MemRead;
	wire MemWrite;
	wire Branch;
	wire RegWrite;

	// Instantiate the Unit Under Test (UUT)
	Control uut (
		.op(op), 
		.RegDst(RegDst), 
		.Jump(Jump), 
		.ALUsrc(ALUsrc), 
		.ALUop(ALUop), 
		.MemToReg(MemToReg), 
		.MemRead(MemRead), 
		.MemWrite(MemWrite), 
		.Branch(Branch), 
		.RegWrite(RegWrite)
	);

	initial begin
		// Initialize Inputs
		op = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		op = 6'b001000;//addi
		//#100;
		//op = 6'b100011;//lw
		
	end
      
endmodule

