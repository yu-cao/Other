`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:57:11 05/27/2018 
// Design Name: 
// Module Name:    Extend26to28 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Extend26to28(input wire [25:0]A,
					output wire [27:0]B
    );
	assign B[27:2]=A[25:0];
	assign B[1:0]=0;

endmodule
