`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:17:56 05/28/2018
// Design Name:   ALU
// Module Name:   E:/single1/single1/ALU_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ALU
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ALU_sim;

	// Inputs
	reg [2:0] ins;
	reg [31:0] A;
	reg [31:0] B;

	// Outputs
	wire [31:0] O;
	wire Z;

	// Instantiate the Unit Under Test (UUT)
	ALU uut (
		.ins(ins), 
		.A(A), 
		.B(B), 
		.O(O), 
		.Z(Z)
	);

	initial begin
		// Initialize Inputs
		ins = 0;
		A = 0;
		B = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		A=12;
		B=12;
		#100;//and
		
		ins = 3'b001;
		#100;//or
		
		ins = 3'b010;//+
		#100;
		
		ins = 3'b110;//-
		#100;
		
		ins = 3'b111;//slt
		#100;
	end
      
endmodule

