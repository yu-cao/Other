<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="PCshow(31:0)" />
        <signal name="op(31:0)" />
        <signal name="op(31:26)" />
        <signal name="RegWrite" />
        <signal name="op(25:21)" />
        <signal name="SW(4:0)" />
        <signal name="op(20:16)" />
        <signal name="RegDst" />
        <signal name="XLXN_27(4:0)" />
        <signal name="op(15:0)" />
        <signal name="OutReg(31:0)" />
        <signal name="XLXN_30(31:0)" />
        <signal name="XLXN_32(31:0)" />
        <signal name="ALUsrc" />
        <signal name="op(5:0)" />
        <signal name="ALUop(1:0)" />
        <signal name="XLXN_40(2:0)" />
        <signal name="TEST_ALU_B(31:0)" />
        <signal name="XLXN_43(31:0)" />
        <signal name="XLXN_52(31:0)" />
        <signal name="XLXN_53" />
        <signal name="PC4(31:0)" />
        <signal name="XLXN_62(31:0)" />
        <signal name="XLXN_66" />
        <signal name="XLXN_69" />
        <signal name="XLXN_71" />
        <signal name="MemWrite" />
        <signal name="MemtoReg" />
        <signal name="XLXN_84(31:0)" />
        <signal name="PC4(31:28)" />
        <signal name="XLXN_108(27:0)" />
        <signal name="XLXN_109(31:0)" />
        <signal name="XLXN_110(31:0)" />
        <signal name="Jump" />
        <signal name="XLXN_115(31:0)" />
        <signal name="op(25:0)" />
        <signal name="op(15:11)" />
        <signal name="XLXN_124(31:0)" />
        <signal name="clk" />
        <signal name="XLXN_127(31:0)" />
        <signal name="XLXN_128(31:0)" />
        <signal name="clk_100mhz" />
        <port polarity="Output" name="PCshow(31:0)" />
        <port polarity="Output" name="op(31:0)" />
        <port polarity="Input" name="SW(4:0)" />
        <port polarity="Output" name="OutReg(31:0)" />
        <port polarity="Output" name="TEST_ALU_B(31:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="clk_100mhz" />
        <blockdef name="PC">
            <timestamp>2018-5-26T9:8:52</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="Instruction">
            <timestamp>2018-5-26T9:9:25</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="Control">
            <timestamp>2018-5-26T9:12:56</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="RegFile">
            <timestamp>2018-5-26T9:22:0</timestamp>
            <rect width="256" x="64" y="-448" height="448" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-428" height="24" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="SignExtend">
            <timestamp>2018-5-26T14:48:3</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="ALUControl">
            <timestamp>2018-5-26T15:32:48</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="ALU">
            <timestamp>2018-5-26T15:33:29</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="ShiftLeft_2b">
            <timestamp>2018-5-26T15:42:32</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="adder32b">
            <timestamp>2017-3-4T5:27:26</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-140" height="24" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Number_4">
            <timestamp>2018-5-26T15:46:46</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="Memory">
            <timestamp>2018-5-27T1:33:20</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="Extend26to28">
            <timestamp>2018-5-27T1:57:45</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Catch28with4_to32">
            <timestamp>2018-5-27T2:2:51</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="MUX5_2x1">
            <timestamp>2018-5-27T2:57:10</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <blockdef name="MUX32_2x1">
            <timestamp>2018-5-27T2:56:43</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <blockdef name="add">
            <timestamp>2018-5-27T6:40:7</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="PC" name="XLXI_1">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="XLXN_115(31:0)" name="npc(31:0)" />
            <blockpin signalname="PCshow(31:0)" name="pc(31:0)" />
        </block>
        <block symbolname="Instruction" name="XLXI_3">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="PCshow(31:0)" name="pc_in(31:0)" />
            <blockpin signalname="op(31:0)" name="instruct(31:0)" />
        </block>
        <block symbolname="Control" name="XLXI_4">
            <blockpin signalname="op(31:26)" name="op(5:0)" />
            <blockpin signalname="RegDst" name="RegDst" />
            <blockpin signalname="Jump" name="Jump" />
            <blockpin signalname="ALUsrc" name="ALUsrc" />
            <blockpin signalname="MemtoReg" name="MemToReg" />
            <blockpin name="MemRead" />
            <blockpin signalname="MemWrite" name="MemWrite" />
            <blockpin signalname="XLXN_69" name="Branch" />
            <blockpin signalname="RegWrite" name="RegWrite" />
            <blockpin signalname="ALUop(1:0)" name="ALUop(1:0)" />
        </block>
        <block symbolname="RegFile" name="XLXI_5">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="RegWrite" name="wr" />
            <blockpin signalname="op(25:21)" name="a1(4:0)" />
            <blockpin signalname="op(20:16)" name="a2(4:0)" />
            <blockpin signalname="SW(4:0)" name="a3(4:0)" />
            <blockpin signalname="XLXN_27(4:0)" name="wreg(4:0)" />
            <blockpin signalname="XLXN_84(31:0)" name="wdata(31:0)" />
            <blockpin signalname="XLXN_43(31:0)" name="d1(31:0)" />
            <blockpin signalname="XLXN_30(31:0)" name="d2(31:0)" />
            <blockpin signalname="OutReg(31:0)" name="d3(31:0)" />
        </block>
        <block symbolname="SignExtend" name="XLXI_7">
            <blockpin signalname="op(15:0)" name="A(15:0)" />
            <blockpin signalname="XLXN_32(31:0)" name="B(31:0)" />
        </block>
        <block symbolname="ALUControl" name="XLXI_9">
            <blockpin signalname="ALUop(1:0)" name="ALUop(1:0)" />
            <blockpin signalname="op(5:0)" name="Func(5:0)" />
            <blockpin signalname="XLXN_40(2:0)" name="ALUoper(2:0)" />
        </block>
        <block symbolname="ALU" name="XLXI_10">
            <blockpin signalname="XLXN_40(2:0)" name="ins(2:0)" />
            <blockpin signalname="XLXN_43(31:0)" name="A(31:0)" />
            <blockpin signalname="TEST_ALU_B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_71" name="Z" />
            <blockpin signalname="XLXN_127(31:0)" name="O(31:0)" />
        </block>
        <block symbolname="ShiftLeft_2b" name="XLXI_11">
            <blockpin signalname="XLXN_32(31:0)" name="A(31:0)" />
            <blockpin signalname="XLXN_62(31:0)" name="B(31:0)" />
        </block>
        <block symbolname="adder32b" name="XLXI_12">
            <blockpin signalname="XLXN_53" name="mode" />
            <blockpin signalname="XLXN_52(31:0)" name="A(31:0)" />
            <blockpin signalname="PCshow(31:0)" name="B(31:0)" />
            <blockpin name="CF" />
            <blockpin name="SF" />
            <blockpin name="ZF" />
            <blockpin signalname="PC4(31:0)" name="S(31:0)" />
        </block>
        <block symbolname="Number_4" name="XLXI_13">
            <blockpin signalname="XLXN_52(31:0)" name="num(31:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_14">
            <blockpin signalname="XLXN_53" name="G" />
        </block>
        <block symbolname="and2" name="XLXI_18">
            <blockpin signalname="XLXN_71" name="I0" />
            <blockpin signalname="XLXN_69" name="I1" />
            <blockpin signalname="XLXN_66" name="O" />
        </block>
        <block symbolname="Memory" name="XLXI_19">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="MemWrite" name="MemWrite" />
            <blockpin signalname="XLXN_127(31:0)" name="addr(31:0)" />
            <blockpin signalname="XLXN_30(31:0)" name="WData(31:0)" />
            <blockpin signalname="XLXN_128(31:0)" name="RData(31:0)" />
        </block>
        <block symbolname="Extend26to28" name="XLXI_22">
            <blockpin signalname="op(25:0)" name="A(25:0)" />
            <blockpin signalname="XLXN_108(27:0)" name="B(27:0)" />
        </block>
        <block symbolname="Catch28with4_to32" name="XLXI_23">
            <blockpin signalname="XLXN_108(27:0)" name="A(27:0)" />
            <blockpin signalname="PC4(31:28)" name="B(3:0)" />
            <blockpin signalname="XLXN_109(31:0)" name="C(31:0)" />
        </block>
        <block symbolname="MUX5_2x1" name="XLXI_35">
            <blockpin signalname="RegDst" name="sel" />
            <blockpin signalname="op(20:16)" name="A(4:0)" />
            <blockpin signalname="op(15:11)" name="B(4:0)" />
            <blockpin signalname="XLXN_27(4:0)" name="C(4:0)" />
        </block>
        <block symbolname="MUX32_2x1" name="XLXI_36">
            <blockpin signalname="MemtoReg" name="sel" />
            <blockpin signalname="XLXN_127(31:0)" name="A(31:0)" />
            <blockpin signalname="XLXN_128(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_84(31:0)" name="C(31:0)" />
        </block>
        <block symbolname="MUX32_2x1" name="XLXI_37">
            <blockpin signalname="Jump" name="sel" />
            <blockpin signalname="XLXN_110(31:0)" name="A(31:0)" />
            <blockpin signalname="XLXN_109(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_115(31:0)" name="C(31:0)" />
        </block>
        <block symbolname="MUX32_2x1" name="XLXI_38">
            <blockpin signalname="XLXN_66" name="sel" />
            <blockpin signalname="PC4(31:0)" name="A(31:0)" />
            <blockpin signalname="XLXN_124(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_110(31:0)" name="C(31:0)" />
        </block>
        <block symbolname="MUX32_2x1" name="XLXI_40">
            <blockpin signalname="ALUsrc" name="sel" />
            <blockpin signalname="XLXN_30(31:0)" name="A(31:0)" />
            <blockpin signalname="XLXN_32(31:0)" name="B(31:0)" />
            <blockpin signalname="TEST_ALU_B(31:0)" name="C(31:0)" />
        </block>
        <block symbolname="add" name="XLXI_41">
            <blockpin signalname="PC4(31:0)" name="A(31:0)" />
            <blockpin signalname="XLXN_62(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_124(31:0)" name="S(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="304" y="1552" name="XLXI_1" orien="R0">
        </instance>
        <instance x="864" y="1488" name="XLXI_3" orien="R0">
        </instance>
        <branch name="PCshow(31:0)">
            <wire x2="768" y1="1456" y2="1456" x1="688" />
            <wire x2="864" y1="1456" y2="1456" x1="768" />
            <wire x2="768" y1="1456" y2="1632" x1="768" />
            <wire x2="1280" y1="480" y2="480" x1="768" />
            <wire x2="768" y1="480" y2="1456" x1="768" />
        </branch>
        <instance x="1584" y="1200" name="XLXI_4" orien="R0">
        </instance>
        <branch name="op(31:0)">
            <wire x2="1328" y1="1392" y2="1392" x1="1248" />
            <wire x2="1328" y1="1392" y2="1472" x1="1328" />
            <wire x2="1328" y1="1472" y2="1536" x1="1328" />
            <wire x2="1328" y1="1536" y2="1792" x1="1328" />
            <wire x2="1328" y1="1792" y2="2096" x1="1328" />
            <wire x2="1328" y1="2096" y2="2224" x1="1328" />
            <wire x2="1328" y1="656" y2="1264" x1="1328" />
            <wire x2="1328" y1="1264" y2="1392" x1="1328" />
        </branch>
        <bustap x2="1424" y1="656" y2="656" x1="1328" />
        <branch name="op(31:26)">
            <wire x2="1472" y1="656" y2="656" x1="1424" />
            <wire x2="1584" y1="656" y2="656" x1="1472" />
        </branch>
        <bustap x2="1424" y1="1472" y2="1472" x1="1328" />
        <branch name="op(25:21)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1620" y="1472" type="branch" />
            <wire x2="1616" y1="1472" y2="1472" x1="1424" />
            <wire x2="2032" y1="1472" y2="1472" x1="1616" />
        </branch>
        <iomarker fontsize="28" x="272" y="2032" name="SW(4:0)" orien="R180" />
        <bustap x2="1424" y1="1536" y2="1536" x1="1328" />
        <branch name="op(20:16)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1616" y="1536" type="branch" />
            <wire x2="1456" y1="1536" y2="1536" x1="1424" />
            <wire x2="1616" y1="1536" y2="1536" x1="1456" />
            <wire x2="2032" y1="1536" y2="1536" x1="1616" />
            <wire x2="1456" y1="1536" y2="1728" x1="1456" />
            <wire x2="1488" y1="1728" y2="1728" x1="1456" />
        </branch>
        <instance x="2032" y="1760" name="XLXI_5" orien="R0">
        </instance>
        <branch name="XLXN_27(4:0)">
            <wire x2="2032" y1="1664" y2="1664" x1="1872" />
        </branch>
        <bustap x2="1424" y1="2096" y2="2096" x1="1328" />
        <branch name="op(15:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1456" y="2096" type="branch" />
            <wire x2="1456" y1="2096" y2="2096" x1="1424" />
            <wire x2="1792" y1="2096" y2="2096" x1="1456" />
            <wire x2="2080" y1="2096" y2="2096" x1="1792" />
        </branch>
        <instance x="2080" y="2128" name="XLXI_7" orien="R0">
        </instance>
        <branch name="OutReg(31:0)">
            <wire x2="2496" y1="1728" y2="1728" x1="2416" />
        </branch>
        <iomarker fontsize="28" x="2496" y="1728" name="OutReg(31:0)" orien="R0" />
        <branch name="XLXN_30(31:0)">
            <wire x2="2432" y1="1536" y2="1536" x1="2416" />
            <wire x2="2432" y1="1536" y2="1584" x1="2432" />
            <wire x2="2608" y1="1584" y2="1584" x1="2432" />
            <wire x2="2608" y1="1584" y2="1696" x1="2608" />
            <wire x2="4480" y1="1696" y2="1696" x1="2608" />
            <wire x2="3104" y1="1584" y2="1584" x1="2608" />
        </branch>
        <branch name="XLXN_32(31:0)">
            <wire x2="2736" y1="2096" y2="2096" x1="2464" />
            <wire x2="2800" y1="816" y2="816" x1="2736" />
            <wire x2="2736" y1="816" y2="1648" x1="2736" />
            <wire x2="2736" y1="1648" y2="2096" x1="2736" />
            <wire x2="3104" y1="1648" y2="1648" x1="2736" />
        </branch>
        <bustap x2="1792" y1="2096" y2="2192" x1="1792" />
        <branch name="op(5:0)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1792" y="2224" type="branch" />
            <wire x2="1792" y1="2192" y2="2224" x1="1792" />
            <wire x2="1792" y1="2224" y2="2288" x1="1792" />
            <wire x2="3312" y1="2288" y2="2288" x1="1792" />
        </branch>
        <branch name="XLXN_40(2:0)">
            <wire x2="3776" y1="2224" y2="2224" x1="3696" />
            <wire x2="3776" y1="1344" y2="2224" x1="3776" />
            <wire x2="3888" y1="1344" y2="1344" x1="3776" />
        </branch>
        <branch name="TEST_ALU_B(31:0)">
            <wire x2="3616" y1="1520" y2="1520" x1="3488" />
            <wire x2="3872" y1="1520" y2="1520" x1="3616" />
            <wire x2="3616" y1="1520" y2="1632" x1="3616" />
            <wire x2="3888" y1="1472" y2="1472" x1="3872" />
            <wire x2="3872" y1="1472" y2="1520" x1="3872" />
        </branch>
        <instance x="3888" y="1504" name="XLXI_10" orien="R0">
        </instance>
        <branch name="XLXN_43(31:0)">
            <wire x2="3152" y1="1344" y2="1344" x1="2416" />
            <wire x2="3152" y1="1344" y2="1408" x1="3152" />
            <wire x2="3888" y1="1408" y2="1408" x1="3152" />
        </branch>
        <branch name="XLXN_52(31:0)">
            <wire x2="1280" y1="384" y2="384" x1="1056" />
        </branch>
        <instance x="1280" y="512" name="XLXI_12" orien="R0">
        </instance>
        <branch name="XLXN_53">
            <wire x2="1280" y1="288" y2="288" x1="1136" />
        </branch>
        <instance x="672" y="416" name="XLXI_13" orien="R0">
        </instance>
        <instance x="1008" y="224" name="XLXI_14" orien="R90" />
        <branch name="XLXN_62(31:0)">
            <wire x2="3200" y1="816" y2="816" x1="3184" />
            <wire x2="3312" y1="640" y2="640" x1="3200" />
            <wire x2="3200" y1="640" y2="816" x1="3200" />
        </branch>
        <instance x="3312" y="2320" name="XLXI_9" orien="R0">
        </instance>
        <instance x="4480" y="1728" name="XLXI_19" orien="R0">
        </instance>
        <branch name="XLXN_84(31:0)">
            <wire x2="2032" y1="1728" y2="1728" x1="1968" />
            <wire x2="1968" y1="1728" y2="1952" x1="1968" />
            <wire x2="5344" y1="1952" y2="1952" x1="1968" />
            <wire x2="5408" y1="1552" y2="1552" x1="5344" />
            <wire x2="5408" y1="1552" y2="1776" x1="5408" />
            <wire x2="5408" y1="1776" y2="1776" x1="5344" />
            <wire x2="5344" y1="1776" y2="1952" x1="5344" />
        </branch>
        <instance x="2800" y="848" name="XLXI_11" orien="R0">
        </instance>
        <branch name="PC4(31:0)">
            <wire x2="2832" y1="480" y2="480" x1="1664" />
            <wire x2="3008" y1="480" y2="480" x1="2832" />
            <wire x2="3008" y1="480" y2="544" x1="3008" />
            <wire x2="3264" y1="544" y2="544" x1="3008" />
            <wire x2="3264" y1="544" y2="576" x1="3264" />
            <wire x2="3312" y1="576" y2="576" x1="3264" />
            <wire x2="3760" y1="336" y2="336" x1="3008" />
            <wire x2="3760" y1="336" y2="576" x1="3760" />
            <wire x2="4096" y1="576" y2="576" x1="3760" />
            <wire x2="3008" y1="336" y2="480" x1="3008" />
        </branch>
        <instance x="3552" y="1024" name="XLXI_18" orien="R0" />
        <branch name="XLXN_69">
            <wire x2="1984" y1="1040" y2="1040" x1="1968" />
            <wire x2="3552" y1="896" y2="896" x1="1984" />
            <wire x2="1984" y1="896" y2="1040" x1="1984" />
        </branch>
        <branch name="XLXN_71">
            <wire x2="3552" y1="960" y2="960" x1="3472" />
            <wire x2="3472" y1="960" y2="1056" x1="3472" />
            <wire x2="4288" y1="1056" y2="1056" x1="3472" />
            <wire x2="4288" y1="1056" y2="1344" x1="4288" />
            <wire x2="4288" y1="1344" y2="1344" x1="4272" />
        </branch>
        <branch name="XLXN_66">
            <wire x2="3824" y1="928" y2="928" x1="3808" />
            <wire x2="4096" y1="512" y2="512" x1="3824" />
            <wire x2="3824" y1="512" y2="928" x1="3824" />
        </branch>
        <bustap x2="2832" y1="480" y2="384" x1="2832" />
        <branch name="PC4(31:28)">
            <wire x2="2848" y1="224" y2="224" x1="2832" />
            <wire x2="2832" y1="224" y2="384" x1="2832" />
        </branch>
        <instance x="2336" y="320" name="XLXI_22" orien="R0">
        </instance>
        <instance x="2848" y="256" name="XLXI_23" orien="R0">
        </instance>
        <branch name="XLXN_108(27:0)">
            <wire x2="2784" y1="288" y2="288" x1="2720" />
            <wire x2="2784" y1="160" y2="288" x1="2784" />
            <wire x2="2848" y1="160" y2="160" x1="2784" />
        </branch>
        <branch name="XLXN_109(31:0)">
            <wire x2="4624" y1="160" y2="160" x1="3232" />
            <wire x2="4624" y1="160" y2="512" x1="4624" />
            <wire x2="4864" y1="512" y2="512" x1="4624" />
        </branch>
        <branch name="XLXN_110(31:0)">
            <wire x2="4544" y1="512" y2="512" x1="4480" />
            <wire x2="4544" y1="448" y2="512" x1="4544" />
            <wire x2="4864" y1="448" y2="448" x1="4544" />
        </branch>
        <branch name="XLXN_115(31:0)">
            <wire x2="288" y1="48" y2="1520" x1="288" />
            <wire x2="304" y1="1520" y2="1520" x1="288" />
            <wire x2="5264" y1="48" y2="48" x1="288" />
            <wire x2="5264" y1="48" y2="384" x1="5264" />
            <wire x2="5264" y1="384" y2="384" x1="5248" />
        </branch>
        <iomarker fontsize="28" x="768" y="1632" name="PCshow(31:0)" orien="R90" />
        <bustap x2="1424" y1="1264" y2="1264" x1="1328" />
        <branch name="op(25:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="1264" type="branch" />
            <wire x2="1440" y1="1264" y2="1264" x1="1424" />
            <wire x2="2112" y1="1264" y2="1264" x1="1440" />
            <wire x2="2336" y1="288" y2="288" x1="2112" />
            <wire x2="2112" y1="288" y2="1264" x1="2112" />
        </branch>
        <branch name="ALUop(1:0)">
            <wire x2="3072" y1="1168" y2="1168" x1="1968" />
            <wire x2="3072" y1="1168" y2="2224" x1="3072" />
            <wire x2="3312" y1="2224" y2="2224" x1="3072" />
        </branch>
        <branch name="SW(4:0)">
            <wire x2="1952" y1="2032" y2="2032" x1="272" />
            <wire x2="2032" y1="1600" y2="1600" x1="1952" />
            <wire x2="1952" y1="1600" y2="2032" x1="1952" />
        </branch>
        <branch name="MemWrite">
            <wire x2="3568" y1="976" y2="976" x1="1968" />
            <wire x2="3568" y1="976" y2="1568" x1="3568" />
            <wire x2="4480" y1="1568" y2="1568" x1="3568" />
        </branch>
        <branch name="RegDst">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1424" y="1664" type="branch" />
            <wire x2="1296" y1="544" y2="1664" x1="1296" />
            <wire x2="1424" y1="1664" y2="1664" x1="1296" />
            <wire x2="1488" y1="1664" y2="1664" x1="1424" />
            <wire x2="2080" y1="544" y2="544" x1="1296" />
            <wire x2="2080" y1="544" y2="656" x1="2080" />
            <wire x2="2080" y1="656" y2="656" x1="1968" />
        </branch>
        <branch name="RegWrite">
            <attrtext style="alignment:SOFT-TCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1776" y="1408" type="branch" />
            <wire x2="1776" y1="1248" y2="1408" x1="1776" />
            <wire x2="2032" y1="1408" y2="1408" x1="1776" />
            <wire x2="2080" y1="1248" y2="1248" x1="1776" />
            <wire x2="2080" y1="1104" y2="1104" x1="1968" />
            <wire x2="2080" y1="1104" y2="1248" x1="2080" />
        </branch>
        <branch name="MemtoReg">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4944" y="1232" type="branch" />
            <wire x2="2144" y1="848" y2="848" x1="1968" />
            <wire x2="2144" y1="848" y2="1232" x1="2144" />
            <wire x2="4944" y1="1232" y2="1232" x1="2144" />
            <wire x2="4944" y1="1232" y2="1552" x1="4944" />
            <wire x2="4960" y1="1552" y2="1552" x1="4944" />
        </branch>
        <branch name="ALUsrc">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2992" y="1520" type="branch" />
            <wire x2="2560" y1="784" y2="784" x1="1968" />
            <wire x2="2560" y1="784" y2="1520" x1="2560" />
            <wire x2="2992" y1="1520" y2="1520" x1="2560" />
            <wire x2="3104" y1="1520" y2="1520" x1="2992" />
        </branch>
        <branch name="Jump">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="4768" y="384" type="branch" />
            <wire x2="2080" y1="720" y2="720" x1="1968" />
            <wire x2="4768" y1="704" y2="704" x1="2080" />
            <wire x2="2080" y1="704" y2="720" x1="2080" />
            <wire x2="4864" y1="384" y2="384" x1="4768" />
            <wire x2="4768" y1="384" y2="704" x1="4768" />
        </branch>
        <instance x="1488" y="1824" name="XLXI_35" orien="R0">
        </instance>
        <instance x="4960" y="1712" name="XLXI_36" orien="R0">
        </instance>
        <instance x="4864" y="544" name="XLXI_37" orien="R0">
        </instance>
        <instance x="4096" y="672" name="XLXI_38" orien="R0">
        </instance>
        <instance x="3104" y="1680" name="XLXI_40" orien="R0">
        </instance>
        <bustap x2="1424" y1="1792" y2="1792" x1="1328" />
        <branch name="op(15:11)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1440" y="1792" type="branch" />
            <wire x2="1440" y1="1792" y2="1792" x1="1424" />
            <wire x2="1488" y1="1792" y2="1792" x1="1440" />
        </branch>
        <instance x="3312" y="672" name="XLXI_41" orien="R0">
        </instance>
        <branch name="XLXN_124(31:0)">
            <wire x2="3744" y1="576" y2="576" x1="3696" />
            <wire x2="3744" y1="576" y2="640" x1="3744" />
            <wire x2="4096" y1="640" y2="640" x1="3744" />
        </branch>
        <branch name="clk">
            <wire x2="256" y1="1456" y2="1456" x1="176" />
            <wire x2="304" y1="1456" y2="1456" x1="256" />
            <wire x2="2032" y1="1344" y2="1344" x1="256" />
            <wire x2="256" y1="1344" y2="1456" x1="256" />
        </branch>
        <iomarker fontsize="28" x="176" y="1456" name="clk" orien="R180" />
        <iomarker fontsize="28" x="1328" y="2224" name="op(31:0)" orien="R90" />
        <iomarker fontsize="28" x="3616" y="1632" name="TEST_ALU_B(31:0)" orien="R90" />
        <branch name="XLXN_127(31:0)">
            <wire x2="4336" y1="1472" y2="1472" x1="4272" />
            <wire x2="4336" y1="1472" y2="1552" x1="4336" />
            <wire x2="4336" y1="1552" y2="1552" x1="4272" />
            <wire x2="4272" y1="1552" y2="1632" x1="4272" />
            <wire x2="4272" y1="1632" y2="1776" x1="4272" />
            <wire x2="4928" y1="1776" y2="1776" x1="4272" />
            <wire x2="4480" y1="1632" y2="1632" x1="4272" />
            <wire x2="4928" y1="1616" y2="1776" x1="4928" />
            <wire x2="4960" y1="1616" y2="1616" x1="4928" />
        </branch>
        <branch name="XLXN_128(31:0)">
            <wire x2="4912" y1="1504" y2="1504" x1="4864" />
            <wire x2="4912" y1="1504" y2="1680" x1="4912" />
            <wire x2="4960" y1="1680" y2="1680" x1="4912" />
        </branch>
        <branch name="clk_100mhz">
            <wire x2="640" y1="1392" y2="1392" x1="192" />
            <wire x2="848" y1="1392" y2="1392" x1="640" />
            <wire x2="864" y1="1392" y2="1392" x1="848" />
            <wire x2="640" y1="1296" y2="1392" x1="640" />
            <wire x2="4352" y1="1296" y2="1296" x1="640" />
            <wire x2="4352" y1="1296" y2="1504" x1="4352" />
            <wire x2="4480" y1="1504" y2="1504" x1="4352" />
        </branch>
        <iomarker fontsize="28" x="192" y="1392" name="clk_100mhz" orien="R180" />
    </sheet>
</drawing>