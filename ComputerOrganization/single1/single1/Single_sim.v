// Verilog test fixture created from schematic E:\single1\single1\Single_CPU.sch - Sun May 27 11:03:24 2018

module Single_CPU_Single_CPU_sch_tb();

// Inputs
   reg [4:0] SW;
   reg clk;
	reg clk_100mhz;

// Output
   wire [31:0] PCshow;
   wire [31:0] OutReg;
	wire [31:0] op;
	wire [31:0] TEST_ALU_B;

// Bidirs

// Instantiate the UUT
   Single_CPU UUT (
		.clk_100mhz,
		.PCshow(PCshow), 
		.SW(SW), 
		.OutReg(OutReg), 
		.clk(clk),
		.op(op),
		.TEST_ALU_B(TEST_ALU_B)
   );
	initial begin
		// Initialize Inputs
		clk = 0;
		SW = 0;
		clk_100mhz = 0;
		#200;

		// Add stimulus here
		fork
		forever #100 clk=~clk;
		forever #10 clk_100mhz=~clk_100mhz;
		begin
		//SW = 5'b10001;//��$s1
		//SW = 5'b01000;//��$t0
		SW = 5'b01001;//��$t1
		#1000;
		
		end
		join
	end



endmodule
