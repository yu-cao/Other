`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:50:11 05/26/2018 
// Design Name: 
// Module Name:    Instruction 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Instruction(clk,pc_in,instruct);
input wire clk;
input wire [31:0]pc_in;
output wire [31:0]instruct;

Instruction_Memory m0 (.clka(clk),.addra(pc_in),.douta(instruct));

endmodule
