`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:35:52 05/31/2018
// Design Name:   Top
// Module Name:   E:/single1/single1/Top_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Top_sim;

	// Inputs
	reg clk;
	reg [4:0] SW;
	reg choose;
	reg next;

	// Outputs
	wire [3:0] AN;
	wire [7:0] SEGMENT;

	// Instantiate the Unit Under Test (UUT)
	Top uut (
		.clk(clk), 
		.SW(SW), 
		.choose(choose), 
		.next(next), 
		.AN(AN), 
		.SEGMENT(SEGMENT)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		SW = 0;
		choose = 0;
		next = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		fork
		forever #10 clk=~clk;
		forever #100 next=~next;
		begin
		//choose = 1;
		//SW = 5'b10001;//��$s1
		//SW = 5'b01000;//��$t0
		SW = 5'b01001;//��$t1
		#1000;
		
		end
		join
		
		
		
		
	end
      
endmodule

