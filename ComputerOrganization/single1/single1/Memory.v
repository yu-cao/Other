`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:09:27 05/27/2018 
// Design Name: 
// Module Name:    Memory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
//�Ѿ�ͨ������
module Memory(
input clk,
input [31:0]addr,
input MemWrite,
input [31:0]WData,
output [31:0]RData
    );

Data_Memory m0(.clka(clk),.wea(MemWrite),.addra(addr[9:0]),.dina(WData),.douta(RData));

endmodule
