`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:59:58 05/27/2018 
// Design Name: 
// Module Name:    Catch28with4_to32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Catch28with4_to32(
input wire [27:0]A,
input wire [3:0]B,
output wire [31:0]C
    );
assign C[31:28]=B[3:0];
assign C[27:0]=A[27:0];

endmodule
