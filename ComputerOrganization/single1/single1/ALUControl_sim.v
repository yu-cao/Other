`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:21:44 05/26/2018
// Design Name:   ALUControl
// Module Name:   E:/single1/single1/ALUControl_sim.v
// Project Name:  single1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ALUControl
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ALUControl_sim;

	// Inputs
	reg [1:0] ALUop;
	reg [5:0] Func;

	// Outputs
	wire [2:0] ALUoper;

	// Instantiate the Unit Under Test (UUT)
	ALUControl uut (
		.ALUop(ALUop), 
		.Func(Func), 
		.ALUoper(ALUoper)
	);

	initial begin
		// Initialize Inputs
		ALUop = 0;
		Func = 0;

		// Wait 100 ns for global reset to finish
		#100;
		Func = 6'b110100;//随便的数据，为XX XXXX类型
       #100;
		 
		// Add stimulus here
		ALUop[0] = 1;//beq
		#100;
		
		ALUop[0]=0;
		ALUop[1]=1;
		Func = 6'b100000;
		#100;
		Func = 6'b100010;
		#100;
		Func = 6'b100100;
		#100;
		Func = 6'b100101;
		#100;
		Func = 6'b101010;
		#100;
		
		
	end
      
endmodule

