/*
1.	目标
以有限数组模拟内存，以磁盘文件作为虚拟存储，编程实现大内存存取，目标是实现4GB范围内任意地址的内存读写
2.	实现功能
lw\sw\lh\lhu\sh\lb\lbu\sb
3.	读取方式
单页映射（完成）
反向页表（完成）
正向页表（未完成）
多级页表（未完成）
大内存处理不当导致正向页表和多级页表的实现没有任何的意义，故滞空。
4.	实现原理
使用4KB数组代替虚拟内存，4byte数组代替物理内存，简单模拟大内存存取，结合之前的MIPS模拟器代码，实现全部内存读写功能。

register.txt:
$zero 0
$at 1
$v0 2
$v1 3
$a0 4
$a1 5
$a2 6
$a3 7
$t0 8
$t1 9
$t2 10
$t3 11
$t4 12
$t5 13
$t6 14
$t7 15
$s0 16
$s1 17
$s2 18
$s3 19
$s4 20
$s5 21
$s6 22
$s7 23
$t8 24
$t9 25
$k0 26
$k1 27
$gp 28
$sp 29
$fp 30
$ra 31

operator.txt:
lw 23 0 WORDI
lb 20 0 WORDI
lbu 24 0 WORDI
lh 21 0 WORDI
lhu 25 0 WORDI
sw 2b 0 WORDI
sb 28 0 WORDI
sh 29 0 WORDI
add 0 20 ALUR
addu 0 21 ALUR
sub 0 22 ALUR
subu 0 23 ALUR
slt 0 2a ALUR
sltu 0 2b ALUR
and 0 24 ALUR
or 0 25 ALUR
xor 0 26 ALUR
nor 0 27 ALUR
sll 0 0 SHIFTR
srl 0 2 SHIFTR
sra 0 3 SHIFTR
mult 0 18 MULTR
multu 0 19 MULTR
div 0 1a MULTR
divu 0 1b MULTR
addi 8h 0 ALUI
addiu 9h 0 ALUI
andi c 0 ALUI
ori d 0 ALUI
xori e 0 ALUI
lui f 0 LUI
slti a 0 ALUI
sltiu b 0 ALUI
beq 4 0 BRANCH
bne 5 0 BRANCH
blez 6 0 SBRANCH
bgtz 7 0 SBRANCH
bltz 1 0 SBRANCH
bgez 1 1 SBRANCH
j 2 0 JUMP
jal 3 0 JUMP
jalr 0 9 JALR
jr 0 8 JR
mfhi 0 10 TRANSR
mthi 0 11 TRANSR
mflo 0 12 TRANSW
mtlo 0 13 TRANSW
eret 10 18 CP0
mfco 10 0 CP0
mtco 10 4 CP0
break 0 d INTERRUPT
syscall 0 c INTERRUPT
*/
#include<iostream>
#include<fstream>
#include<map>
#include<time.h>
#include<stdlib.h> 
#include<string>
#include<string.h>
#include<cmath>
#include<iomanip>
#include<memory.h>
using namespace std;
#define k 1024

int code;
int reg[32];
void mipsGet();
int num(const string& im);

class mapping
{
    public:
        static void initial()
        {
            ifstream input;
            input.open("register.txt");
            if(input.fail()) cout<<"Fail!!!"<<endl;
            string str;
            char *temp1, *temp2;
            while(getline(input , str))
            {
                temp1 = strtok((char* )str.c_str() , " ");
                temp2 = strtok(NULL , " ");
                registers.insert(pair<string , int> (temp1 , atoi(temp2)));
            }
            input.close();
            input.open("operation.txt");
            if(input.fail()) cout<<"Fail!!!"<<endl;
            char op[100];
            int opcode, funct;
            while(getline(input , str))
            {
                temp1 = strtok((char* )str.c_str() , " ");
                strcpy(op, temp1);
                temp1 = strtok(NULL , " ");
                sscanf(temp1 , "%x" , &opcode);
                temp1 = strtok(NULL , " ");
                sscanf(temp1 , "%x" , &funct);
                temp1 = strtok(NULL , " ");
                int tmp = (opcode<<16) + funct;
                operations.insert(pair<string , int>(op , tmp));
            }
        }
    public:
        //assembler
        static map<string , int> registers;
        static map<string , int> operations;
};

map<string , int> mapping::registers;
map<string , int> mapping::operations;

class visitWay
{
	private:
		int page;
		int ofs;
		int opcode;
		char Memory[4*k];	//simulate the virtual memory(4*k means 4*G) 
		char Mem[4];		//simlate the physical memory(4 means 4*k)
	public:
		visitWay()
		{
			memset(Memory, 0, 4*k);
		}
		void sigle_getMemory(int addr);   //single page table
		void inverted_getMemory(int addr);//inverted page table
	//	void forward_getMemory(int addr); //Forward page table
	//	void multilevel_getMemory(int addr);//Multilevel page table
};

int main(void)
{
	mapping::initial();
	visitWay a;
	memset(reg, 0 , 32);
	int choice;
	int addr;
	do
	{
		choice = 0;
		printf("please choose the way\n");
		printf("0. exit the program\n");
		printf("1. single page table\n");
		printf("2. inverted page table\n");
		printf("3. forward page table\n");
		printf("4. Multilevel page table\n");
		scanf("%d", &choice);
		getchar();
		if(choice != 0)
		{
			mipsGet();
			cout<<"the mechine code is "<<hex<<code<<endl;
		}
		addr = (code & 0xFFFF) + reg[(code & 0x1F0000)>>21];
		switch(choice)
		{
			case 0:
			{
				cout<<"Exit success"<<endl;
				break;				
			}
			case 1:
			{
				a.sigle_getMemory(addr);
				break;				
			}
			case 2:
			{
				a.inverted_getMemory(addr);
				break;
			}
			case 3:
			{
		//		a.forward_getMemory(addr);
				break;	
			}
			case 4:
			{
		//		a.multilevel_getMemory(addr);
				break;
			}
			default:
				cout<<"Error input"<<endl;
		}
		code = 0;

	}while(choice != 0);
	getchar();
	return 0;
}

int num(const string& im)
{
	int number = 0;
    if( im[0] == '0' && (im[1] == 'x' || im[1] == 'X') )
		sscanf(im.c_str() , "%x" , &number);
	else
		sscanf(im.c_str() , "%d" , &number);
	number = number & 0x0000ffff;
	return number;
}

void mipsGet()
{
	cout<<"请输入MIPS语句"<<endl;
	char ins[100];
	memset(ins,0,100);
	gets(ins);
	cout<<ins<<endl;
	char* op = strtok(ins , " ,;");
	char* rt = strtok(ins , " ,;");
	char* ext = strtok(ins , " ,;()");
	char* rs = strtok(ins , " ,;()");
	code += ((mapping::operations[op]>>16) << 26 );
	code += (mapping::registers[rs] << 21 );
	code += (mapping::registers[rt] << 16 );
	code += num(ext);
}

//single page table
void visitWay::sigle_getMemory(int addr)
{
	srand((unsigned)time(NULL));
	int pag[4],i;
	//pag[i] = ALU();
	page = addr / 1;
	ofs = addr % 1;
	for(i = 0; i < 4/1; i ++)
	{
		pag[i] = rand()%(4*k);
		if(pag[i] == page)
			addr = page * 1 + ofs;
	}

	if(1 == 4 && pag[i] != ofs)
	{
		pag[0] = page;
		addr = pag[0] * 1 + ofs;
	}

	opcode = (code>>26) & 0x0000003F;
	int rt;
	rt = (opcode & 0x1F0000)>>16;
	if(opcode == 0x20)//lb
	{
		int temp = Memory[page + ofs];
		temp = temp + ((temp & 0x80)<<24)>>24;
		reg[rt] = temp;
		cout<<"取字节成功,单页映射法"<<endl; 
	}
	if(opcode == 0x24)//lbu
	{
		reg[rt] = Memory[4*page + ofs];
		cout<<"取字节（无符号）成功，单页映射法"<<endl; 
	}
	else if(opcode == 0x21)//lh
	{
		int temp = (Memory[4*page + ofs]<<8) | Memory[4*page + ofs + 1];//big endian
		temp = temp + ((temp & 0x8000)<<16)>>16;
		reg[rt] = temp;
		cout<<"取半字成功，单页映射法"<<endl;
	}
	else if(opcode == 0x25)
	{
		reg[rt] = (Memory[4*page + ofs]<<8) | Memory[4*page + ofs + 1];
		cout<<"取半字（无符号）成功，单页映射法"<<endl;
	}
	else if(opcode == 0x23)
	{
		reg[rt] = (Memory[4*page + ofs]<<24) 
				  | (Memory[4*page + ofs + 1]<<16)
				  | (Memory[4*page + ofs + 2]<<8)
				  | (Memory[4*page + ofs + 3]);
		cout<<"取字成功，单页映射法"<<endl; 
	}
	else if(opcode == 0x28)//sb
	{
		Memory[4*page + ofs] = reg[rt];
		cout<<"存字节成功，单页映射法"<<endl; 
	}
	else if(opcode == 0x29)//sh
	{
		Memory[4*page + ofs] = (reg[rt]>>8)&0xFF;
		Memory[4*page + ofs + 1] = reg[rt]; 
		cout<<"存半字成功，单页映射法"<<endl;
	}
	else if(opcode == 0x2B)
	{
		Memory[4*page + ofs] = (reg[rt]>>24)&0xFF;
		Memory[4*page + ofs] = (reg[rt]>>16)&0xFF;
		Memory[4*page + ofs] = (reg[rt]>>8)&0xFF;
		Memory[4*page + ofs] = (reg[rt])&0xFF;	
		cout<<"存字成功，单页映射法"<<endl;
	}
	else 
		cout<<"error input MIPS"<<endl;
	cout<<endl;
}

//inverted page table
void visitWay::inverted_getMemory(int addr)
{
	page = addr / 4;
	ofs = addr % 4;
	opcode = (code>>26) & 0x0000003F;
	int rt;
	rt = (opcode & 0x1F0000)>>16;
	if(opcode == 0x20)//lb
	{
		int temp = Memory[4*page + ofs];
		temp = temp + ((temp & 0x80)<<24)>>24;
		reg[rt] = temp;
		cout<<"取字节成功，反向页表法"<<endl; 
	}
	if(opcode == 0x24)//lbu
	{
		reg[rt] = Memory[4*page + ofs];
		cout<<"取字节（无符号）成功，反向页表法"<<endl; 
	}
	else if(opcode == 0x21)//lh
	{
		int temp = (Memory[4*page + ofs]<<8) | Memory[4*page + ofs + 1];//big endian
		temp = temp + ((temp & 0x8000)<<16)>>16;
		reg[rt] = temp;
		cout<<"取半字成功，反向页表法"<<endl;
	}
	else if(opcode == 0x25)
	{
		reg[rt] = (Memory[4*page + ofs]<<8) | Memory[4*page + ofs + 1];
		cout<<"取半字（无符号）成功，反向页表法"<<endl;
	}
	else if(opcode == 0x23)
	{
		reg[rt] = (Memory[4*page + ofs]<<24) 
				  | (Memory[4*page + ofs + 1]<<16)
				  | (Memory[4*page + ofs + 2]<<8)
				  | (Memory[4*page + ofs + 3]);
		cout<<"取字成功，反向页表法"<<endl; 
	}
	else if(opcode == 0x28)//sb
	{
		Memory[4*page + ofs] = reg[rt];
		cout<<"存字节成功，反向页表法"<<endl; 
	}
	else if(opcode == 0x29)//sh
	{
		Memory[4*page + ofs] = (reg[rt]>>8)&0xFF;
		Memory[4*page + ofs + 1] = reg[rt]; 
		cout<<"存半字成功，反向页表法"<<endl;
	}
	else if(opcode == 0x2B)
	{
		Memory[4*page + ofs] = (reg[rt]>>24)&0xFF;
		Memory[4*page + ofs] = (reg[rt]>>16)&0xFF;
		Memory[4*page + ofs] = (reg[rt]>>8)&0xFF;
		Memory[4*page + ofs] = (reg[rt])&0xFF;	
		cout<<"存字成功，反向页表法"<<endl;
	}
	else 
		cout<<"error input MIPS"<<endl;
	cout<<endl;	
}