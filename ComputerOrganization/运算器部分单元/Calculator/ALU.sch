<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1(31:0)" />
        <signal name="XLXN_2(31:0)" />
        <signal name="XLXN_4(31:0)" />
        <signal name="XLXN_5(31:0)" />
        <signal name="XLXN_6(31:0)" />
        <signal name="XLXN_7(31:0)" />
        <signal name="XLXN_8(31:0)" />
        <signal name="Co" />
        <signal name="res(31:0)" />
        <signal name="zero" />
        <signal name="B(31:0)" />
        <signal name="A(31:0)" />
        <signal name="ALU_Ctr(2:0)" />
        <signal name="ALU_Ctr(2)" />
        <signal name="XLXN_24(31:0)" />
        <signal name="XLXN_26(31:0)" />
        <port polarity="Output" name="Co" />
        <port polarity="Output" name="res(31:0)" />
        <port polarity="Output" name="zero" />
        <port polarity="Input" name="B(31:0)" />
        <port polarity="Input" name="A(31:0)" />
        <port polarity="Input" name="ALU_Ctr(2:0)" />
        <blockdef name="S31">
            <timestamp>2017-11-27T10:53:52</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Xor32">
            <timestamp>2017-11-27T10:47:54</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="And32">
            <timestamp>2017-11-27T10:42:22</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="Or32">
            <timestamp>2017-11-27T10:45:8</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="ADC32">
            <timestamp>2017-11-27T8:35:46</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Nor32">
            <timestamp>2017-11-27T10:51:9</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="MUX8T1_32">
            <timestamp>2017-11-27T11:0:1</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-556" height="24" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
        </blockdef>
        <blockdef name="Or_Bit32">
            <timestamp>2017-11-27T10:55:45</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Srl32">
            <timestamp>2017-11-27T10:52:1</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="Xnor32">
            <timestamp>2017-11-27T10:53:7</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="And32" name="XLXI_3">
            <blockpin signalname="A(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_1(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="Or32" name="XLXI_4">
            <blockpin signalname="A(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_2(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="Xor32" name="XLXI_6">
            <blockpin signalname="A(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_5(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="Nor32" name="XLXI_7">
            <blockpin signalname="A(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="MUX8T1_32" name="XLXI_8">
            <blockpin signalname="XLXN_1(31:0)" name="I0(31:0)" />
            <blockpin signalname="XLXN_2(31:0)" name="I7(31:0)" />
            <blockpin signalname="XLXN_4(31:0)" name="I1(31:0)" />
            <blockpin signalname="XLXN_4(31:0)" name="I2(31:0)" />
            <blockpin signalname="XLXN_5(31:0)" name="I3(31:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="I6(31:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="I5(31:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="I4(31:0)" />
            <blockpin signalname="ALU_Ctr(2:0)" name="S(2:0)" />
            <blockpin signalname="res(31:0)" name="O(31:0)" />
        </block>
        <block symbolname="Or_Bit32" name="XLXI_15">
            <blockpin signalname="res(31:0)" name="res(31:0)" />
            <blockpin signalname="zero" name="zero" />
        </block>
        <block symbolname="Srl32" name="XLXI_16">
            <blockpin signalname="A(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_7(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="Xnor32" name="XLXI_17">
            <blockpin signalname="A(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_8(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="S31" name="XLXI_1">
            <blockpin signalname="ALU_Ctr(2)" name="S" />
            <blockpin signalname="XLXN_24(31:0)" name="So(31:0)" />
        </block>
        <block symbolname="Xor32" name="XLXI_2">
            <blockpin signalname="XLXN_24(31:0)" name="A(31:0)" />
            <blockpin signalname="B(31:0)" name="B(31:0)" />
            <blockpin signalname="XLXN_26(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="ADC32" name="XLXI_20">
            <blockpin signalname="A(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_26(31:0)" name="b(31:0)" />
            <blockpin signalname="ALU_Ctr(2)" name="C0" />
            <blockpin signalname="XLXN_4(31:0)" name="s(31:0)" />
            <blockpin signalname="Co" name="Co" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1408" y="416" name="XLXI_3" orien="R0">
        </instance>
        <instance x="2240" y="1584" name="XLXI_8" orien="R0">
        </instance>
        <instance x="2816" y="1520" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1392" y="752" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1392" y="1584" name="XLXI_6" orien="R0">
        </instance>
        <instance x="1408" y="1872" name="XLXI_7" orien="R0">
        </instance>
        <instance x="1408" y="2176" name="XLXI_16" orien="R0">
        </instance>
        <instance x="1424" y="2416" name="XLXI_17" orien="R0">
        </instance>
        <branch name="XLXN_1(31:0)">
            <wire x2="2016" y1="320" y2="320" x1="1792" />
            <wire x2="2016" y1="320" y2="1040" x1="2016" />
            <wire x2="2240" y1="1040" y2="1040" x1="2016" />
        </branch>
        <branch name="XLXN_2(31:0)">
            <wire x2="2000" y1="656" y2="656" x1="1776" />
            <wire x2="2000" y1="656" y2="1104" x1="2000" />
            <wire x2="2240" y1="1104" y2="1104" x1="2000" />
        </branch>
        <branch name="XLXN_4(31:0)">
            <wire x2="1984" y1="1056" y2="1056" x1="1792" />
            <wire x2="1984" y1="1056" y2="1168" x1="1984" />
            <wire x2="2240" y1="1168" y2="1168" x1="1984" />
            <wire x2="1984" y1="1168" y2="1232" x1="1984" />
            <wire x2="2240" y1="1232" y2="1232" x1="1984" />
        </branch>
        <branch name="XLXN_5(31:0)">
            <wire x2="2000" y1="1488" y2="1488" x1="1776" />
            <wire x2="2000" y1="1296" y2="1488" x1="2000" />
            <wire x2="2240" y1="1296" y2="1296" x1="2000" />
        </branch>
        <branch name="XLXN_6(31:0)">
            <wire x2="2016" y1="1776" y2="1776" x1="1792" />
            <wire x2="2016" y1="1360" y2="1776" x1="2016" />
            <wire x2="2240" y1="1360" y2="1360" x1="2016" />
        </branch>
        <branch name="XLXN_7(31:0)">
            <wire x2="2032" y1="2080" y2="2080" x1="1792" />
            <wire x2="2032" y1="1424" y2="2080" x1="2032" />
            <wire x2="2240" y1="1424" y2="1424" x1="2032" />
        </branch>
        <branch name="XLXN_8(31:0)">
            <wire x2="2048" y1="2320" y2="2320" x1="1808" />
            <wire x2="2048" y1="1488" y2="2320" x1="2048" />
            <wire x2="2240" y1="1488" y2="1488" x1="2048" />
        </branch>
        <branch name="Co">
            <wire x2="1824" y1="1184" y2="1184" x1="1792" />
        </branch>
        <iomarker fontsize="28" x="1824" y="1184" name="Co" orien="R0" />
        <branch name="res(31:0)">
            <wire x2="2768" y1="1040" y2="1040" x1="2624" />
            <wire x2="3040" y1="1040" y2="1040" x1="2768" />
            <wire x2="2768" y1="1040" y2="1488" x1="2768" />
            <wire x2="2816" y1="1488" y2="1488" x1="2768" />
        </branch>
        <iomarker fontsize="28" x="3040" y="1040" name="res(31:0)" orien="R0" />
        <branch name="zero">
            <wire x2="3232" y1="1488" y2="1488" x1="3200" />
        </branch>
        <iomarker fontsize="28" x="3232" y="1488" name="zero" orien="R0" />
        <instance x="336" y="816" name="XLXI_1" orien="R0">
        </instance>
        <instance x="336" y="1872" name="XLXI_2" orien="R0">
        </instance>
        <branch name="A(31:0)">
            <wire x2="960" y1="272" y2="272" x1="304" />
            <wire x2="960" y1="272" y2="320" x1="960" />
            <wire x2="1408" y1="320" y2="320" x1="960" />
            <wire x2="960" y1="320" y2="656" x1="960" />
            <wire x2="1392" y1="656" y2="656" x1="960" />
            <wire x2="960" y1="656" y2="1056" x1="960" />
            <wire x2="1408" y1="1056" y2="1056" x1="960" />
            <wire x2="960" y1="1056" y2="1488" x1="960" />
            <wire x2="1392" y1="1488" y2="1488" x1="960" />
            <wire x2="960" y1="1488" y2="1776" x1="960" />
            <wire x2="1408" y1="1776" y2="1776" x1="960" />
            <wire x2="960" y1="1776" y2="2096" x1="960" />
            <wire x2="976" y1="2096" y2="2096" x1="960" />
            <wire x2="960" y1="2096" y2="2320" x1="960" />
            <wire x2="960" y1="2320" y2="2464" x1="960" />
            <wire x2="1424" y1="2320" y2="2320" x1="960" />
            <wire x2="1408" y1="2080" y2="2080" x1="976" />
            <wire x2="976" y1="2080" y2="2096" x1="976" />
        </branch>
        <branch name="B(31:0)">
            <wire x2="336" y1="1840" y2="1840" x1="256" />
            <wire x2="256" y1="1840" y2="1936" x1="256" />
            <wire x2="608" y1="1936" y2="1936" x1="256" />
            <wire x2="608" y1="1936" y2="2544" x1="608" />
            <wire x2="1248" y1="2544" y2="2544" x1="608" />
            <wire x2="1248" y1="2544" y2="2592" x1="1248" />
            <wire x2="1264" y1="2592" y2="2592" x1="1248" />
            <wire x2="608" y1="2544" y2="2544" x1="400" />
            <wire x2="1264" y1="304" y2="384" x1="1264" />
            <wire x2="1408" y1="384" y2="384" x1="1264" />
            <wire x2="1264" y1="384" y2="720" x1="1264" />
            <wire x2="1392" y1="720" y2="720" x1="1264" />
            <wire x2="1264" y1="720" y2="1120" x1="1264" />
            <wire x2="1264" y1="1120" y2="1552" x1="1264" />
            <wire x2="1264" y1="1552" y2="1840" x1="1264" />
            <wire x2="1264" y1="1840" y2="2144" x1="1264" />
            <wire x2="1264" y1="2144" y2="2384" x1="1264" />
            <wire x2="1264" y1="2384" y2="2592" x1="1264" />
            <wire x2="1424" y1="2384" y2="2384" x1="1264" />
            <wire x2="1408" y1="2144" y2="2144" x1="1264" />
            <wire x2="1408" y1="1840" y2="1840" x1="1264" />
            <wire x2="1392" y1="1552" y2="1552" x1="1264" />
        </branch>
        <iomarker fontsize="28" x="400" y="2544" name="B(31:0)" orien="R180" />
        <iomarker fontsize="28" x="304" y="272" name="A(31:0)" orien="R180" />
        <branch name="ALU_Ctr(2:0)">
            <wire x2="448" y1="128" y2="128" x1="304" />
            <wire x2="2112" y1="128" y2="128" x1="448" />
            <wire x2="2112" y1="128" y2="1552" x1="2112" />
            <wire x2="2240" y1="1552" y2="1552" x1="2112" />
        </branch>
        <iomarker fontsize="28" x="304" y="128" name="ALU_Ctr(2:0)" orien="R180" />
        <branch name="ALU_Ctr(2)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="448" y="336" type="branch" />
            <wire x2="288" y1="448" y2="784" x1="288" />
            <wire x2="336" y1="784" y2="784" x1="288" />
            <wire x2="288" y1="784" y2="1184" x1="288" />
            <wire x2="1408" y1="1184" y2="1184" x1="288" />
            <wire x2="448" y1="448" y2="448" x1="288" />
            <wire x2="448" y1="224" y2="336" x1="448" />
            <wire x2="448" y1="336" y2="432" x1="448" />
            <wire x2="448" y1="432" y2="448" x1="448" />
        </branch>
        <bustap x2="448" y1="128" y2="224" x1="448" />
        <branch name="XLXN_24(31:0)">
            <wire x2="256" y1="1696" y2="1776" x1="256" />
            <wire x2="336" y1="1776" y2="1776" x1="256" />
            <wire x2="800" y1="1696" y2="1696" x1="256" />
            <wire x2="800" y1="784" y2="784" x1="720" />
            <wire x2="800" y1="784" y2="1696" x1="800" />
        </branch>
        <instance x="1408" y="1216" name="XLXI_20" orien="R0">
        </instance>
        <branch name="XLXN_26(31:0)">
            <wire x2="784" y1="1776" y2="1776" x1="720" />
            <wire x2="784" y1="1712" y2="1776" x1="784" />
            <wire x2="1328" y1="1712" y2="1712" x1="784" />
            <wire x2="1328" y1="1120" y2="1712" x1="1328" />
            <wire x2="1408" y1="1120" y2="1120" x1="1328" />
        </branch>
    </sheet>
</drawing>