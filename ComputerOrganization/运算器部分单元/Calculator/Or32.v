`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:42:33 11/27/2017 
// Design Name: 
// Module Name:    Or32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Or32(input [31:0]A,
				input [31:0]B,
				output [31:0]res
    );
	 assign res[31:0] = A[31:0]|B[31:0];


endmodule
