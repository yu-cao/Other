//右移位，从高位向低位推了1bit，类似SRA，但是这里直接是用0补在最高位（符号位）上
//S=1时移位发生
module srl1b(A, S, Z);

    parameter N = 32;

	//port definitions
	input wire [(N-1):0] A;
	input wire S;
    output wire [(N-1):0] Z;
    wire [(N-1):0] B;

    assign B[N-1] = 1'b0;//直接末尾位置0
    assign B[(N-2):0] = A[N-1:1];
    
	mux_2to1 m1 (.X(A), .Y(B), .S(S), .Z(Z));

endmodule