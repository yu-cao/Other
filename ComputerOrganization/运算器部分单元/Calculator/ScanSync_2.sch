<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Scan(2:0)" />
        <signal name="point(7:0)" />
        <signal name="LES(7:0)" />
        <signal name="Hexs(31:0)" />
        <signal name="Hexs(3:0),G0,G0,G0,G0" />
        <signal name="Hexs(7:4),G0,G0,G0,G0" />
        <signal name="Hexs(11:8),G0,G0,G0,G0" />
        <signal name="Hexs(23:20),G0,G0,G0,G0" />
        <signal name="Hexs(27:24),G0,G0,G0,G0" />
        <signal name="Hexs(31:28),G0,G0,G0,G0" />
        <signal name="Hexs(15:12),G0,G0,G0,G0" />
        <signal name="Hexs(19:16),G0,G0,G0,G0" />
        <signal name="LES(0),point(0),G0,G0,V5,V5,V5,G0" />
        <signal name="LES(1),point(1),G0,G0,V5,V5,G0,V5" />
        <signal name="LES(2),point(2),G0,G0,V5,G0,V5,V5" />
        <signal name="LES(4),point(4),G0,G0,V5,V5,V5,G0" />
        <signal name="LES(5),point(5),G0,G0,V5,V5,G0,V5" />
        <signal name="LES(6),point(6),G0,G0,V5,G0,V5,V5" />
        <signal name="LES(7),point(7),G0,G0,G0,V5,V5,V5" />
        <signal name="LES(3),point(3),G0,G0,G0,V5,V5,V5" />
        <signal name="Hex(7:0)" />
        <signal name="Hex(7)" />
        <signal name="Hex(6)" />
        <signal name="Hex(5)" />
        <signal name="Hex(4)" />
        <signal name="Hexo(3:0)" />
        <signal name="Hexo(3)" />
        <signal name="Hexo(2)" />
        <signal name="Hexo(1)" />
        <signal name="Hexo(0)" />
        <signal name="V5" />
        <signal name="G0" />
        <signal name="COM(7:0)" />
        <signal name="COM(3)" />
        <signal name="COM(2)" />
        <signal name="COM(1)" />
        <signal name="COM(0)" />
        <signal name="COM(7)" />
        <signal name="COM(6)" />
        <signal name="AN(3:0)" />
        <signal name="p" />
        <signal name="AN(3)" />
        <signal name="AN(2)" />
        <signal name="AN(1)" />
        <signal name="AN(0)" />
        <signal name="LE" />
        <port polarity="Input" name="Scan(2:0)" />
        <port polarity="Input" name="point(7:0)" />
        <port polarity="Input" name="LES(7:0)" />
        <port polarity="Input" name="Hexs(31:0)" />
        <port polarity="Output" name="Hexo(3:0)" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Output" name="p" />
        <port polarity="Output" name="LE" />
        <blockdef name="MUX8T1_8">
            <timestamp>2017-11-22T7:6:30</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-556" height="24" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <block symbolname="MUX8T1_8" name="XLXI_1">
            <blockpin signalname="Scan(2:0)" name="s(2:0)" />
            <blockpin signalname="Hexs(3:0),G0,G0,G0,G0" name="I0(7:0)" />
            <blockpin signalname="Hexs(7:4),G0,G0,G0,G0" name="I1(7:0)" />
            <blockpin signalname="Hexs(11:8),G0,G0,G0,G0" name="I2(7:0)" />
            <blockpin signalname="Hexs(19:16),G0,G0,G0,G0" name="I4(7:0)" />
            <blockpin signalname="Hexs(23:20),G0,G0,G0,G0" name="I5(7:0)" />
            <blockpin signalname="Hexs(27:24),G0,G0,G0,G0" name="I6(7:0)" />
            <blockpin signalname="Hexs(31:28),G0,G0,G0,G0" name="I7(7:0)" />
            <blockpin signalname="Hexs(15:12),G0,G0,G0,G0" name="I3(7:0)" />
            <blockpin signalname="Hex(7:0)" name="O(7:0)" />
        </block>
        <block symbolname="MUX8T1_8" name="XLXI_2">
            <blockpin signalname="Scan(2:0)" name="s(2:0)" />
            <blockpin signalname="LES(0),point(0),G0,G0,V5,V5,V5,G0" name="I0(7:0)" />
            <blockpin signalname="LES(1),point(1),G0,G0,V5,V5,G0,V5" name="I1(7:0)" />
            <blockpin signalname="LES(2),point(2),G0,G0,V5,G0,V5,V5" name="I2(7:0)" />
            <blockpin signalname="LES(4),point(4),G0,G0,V5,V5,V5,G0" name="I4(7:0)" />
            <blockpin signalname="LES(5),point(5),G0,G0,V5,V5,G0,V5" name="I5(7:0)" />
            <blockpin signalname="LES(6),point(6),G0,G0,V5,G0,V5,V5" name="I6(7:0)" />
            <blockpin signalname="LES(7),point(7),G0,G0,G0,V5,V5,V5" name="I7(7:0)" />
            <blockpin signalname="LES(3),point(3),G0,G0,G0,V5,V5,V5" name="I3(7:0)" />
            <blockpin signalname="COM(7:0)" name="O(7:0)" />
        </block>
        <block symbolname="buf" name="XLXI_3">
            <blockpin signalname="Hex(7)" name="I" />
            <blockpin signalname="Hexo(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_4">
            <blockpin signalname="Hex(6)" name="I" />
            <blockpin signalname="Hexo(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_5">
            <blockpin signalname="Hex(5)" name="I" />
            <blockpin signalname="Hexo(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_6">
            <blockpin signalname="Hex(4)" name="I" />
            <blockpin signalname="Hexo(0)" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_7">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_8">
            <blockpin signalname="G0" name="G" />
        </block>
        <block symbolname="buf" name="XLXI_15">
            <blockpin signalname="COM(3)" name="I" />
            <blockpin signalname="AN(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_16">
            <blockpin signalname="COM(2)" name="I" />
            <blockpin signalname="AN(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_17">
            <blockpin signalname="COM(1)" name="I" />
            <blockpin signalname="AN(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_18">
            <blockpin signalname="COM(0)" name="I" />
            <blockpin signalname="AN(0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_19">
            <blockpin signalname="COM(7)" name="I" />
            <blockpin signalname="LE" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_20">
            <blockpin signalname="COM(6)" name="I" />
            <blockpin signalname="p" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1024" y="1056" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1040" y="2016" name="XLXI_2" orien="R0">
        </instance>
        <branch name="Scan(2:0)">
            <wire x2="800" y1="240" y2="240" x1="544" />
            <wire x2="800" y1="240" y2="512" x1="800" />
            <wire x2="1024" y1="512" y2="512" x1="800" />
            <wire x2="800" y1="512" y2="1472" x1="800" />
            <wire x2="1040" y1="1472" y2="1472" x1="800" />
        </branch>
        <branch name="point(7:0)">
            <wire x2="624" y1="400" y2="400" x1="496" />
        </branch>
        <branch name="LES(7:0)">
            <wire x2="640" y1="464" y2="464" x1="496" />
        </branch>
        <branch name="Hexs(31:0)">
            <wire x2="624" y1="336" y2="336" x1="496" />
        </branch>
        <branch name="Hexs(3:0),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="576" type="branch" />
            <wire x2="1024" y1="576" y2="576" x1="624" />
        </branch>
        <branch name="Hexs(7:4),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="640" type="branch" />
            <wire x2="1024" y1="640" y2="640" x1="624" />
        </branch>
        <branch name="Hexs(11:8),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="704" type="branch" />
            <wire x2="1024" y1="704" y2="704" x1="624" />
        </branch>
        <branch name="Hexs(23:20),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="832" type="branch" />
            <wire x2="1024" y1="832" y2="832" x1="624" />
        </branch>
        <branch name="Hexs(27:24),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="896" type="branch" />
            <wire x2="1024" y1="896" y2="896" x1="624" />
        </branch>
        <branch name="Hexs(31:28),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="960" type="branch" />
            <wire x2="1024" y1="960" y2="960" x1="624" />
        </branch>
        <branch name="Hexs(15:12),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="640" y="1024" type="branch" />
            <wire x2="1024" y1="1024" y2="1024" x1="640" />
        </branch>
        <branch name="Hexs(19:16),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="768" type="branch" />
            <wire x2="1024" y1="768" y2="768" x1="624" />
        </branch>
        <branch name="LES(0),point(0),G0,G0,V5,V5,V5,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="672" y="1536" type="branch" />
            <wire x2="1040" y1="1536" y2="1536" x1="672" />
        </branch>
        <branch name="LES(1),point(1),G0,G0,V5,V5,G0,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="656" y="1600" type="branch" />
            <wire x2="1040" y1="1600" y2="1600" x1="656" />
        </branch>
        <branch name="LES(2),point(2),G0,G0,V5,G0,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="688" y="1664" type="branch" />
            <wire x2="1040" y1="1664" y2="1664" x1="688" />
        </branch>
        <branch name="LES(4),point(4),G0,G0,V5,V5,V5,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="704" y="1728" type="branch" />
            <wire x2="1040" y1="1728" y2="1728" x1="704" />
        </branch>
        <branch name="LES(5),point(5),G0,G0,V5,V5,G0,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="688" y="1792" type="branch" />
            <wire x2="1040" y1="1792" y2="1792" x1="688" />
        </branch>
        <branch name="LES(6),point(6),G0,G0,V5,G0,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="672" y="1856" type="branch" />
            <wire x2="1040" y1="1856" y2="1856" x1="672" />
        </branch>
        <branch name="LES(7),point(7),G0,G0,G0,V5,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="704" y="1920" type="branch" />
            <wire x2="1040" y1="1920" y2="1920" x1="704" />
        </branch>
        <branch name="LES(3),point(3),G0,G0,G0,V5,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="704" y="1984" type="branch" />
            <wire x2="1040" y1="1984" y2="1984" x1="704" />
        </branch>
        <branch name="Hex(7:0)">
            <wire x2="1856" y1="512" y2="512" x1="1408" />
            <wire x2="1856" y1="512" y2="608" x1="1856" />
            <wire x2="1856" y1="608" y2="704" x1="1856" />
            <wire x2="1856" y1="704" y2="784" x1="1856" />
            <wire x2="1856" y1="272" y2="336" x1="1856" />
            <wire x2="1856" y1="336" y2="416" x1="1856" />
            <wire x2="1856" y1="416" y2="512" x1="1856" />
        </branch>
        <bustap x2="1952" y1="336" y2="336" x1="1856" />
        <bustap x2="1952" y1="416" y2="416" x1="1856" />
        <bustap x2="1952" y1="608" y2="608" x1="1856" />
        <bustap x2="1952" y1="704" y2="704" x1="1856" />
        <instance x="2144" y="368" name="XLXI_3" orien="R0" />
        <instance x="2144" y="448" name="XLXI_4" orien="R0" />
        <instance x="2144" y="640" name="XLXI_5" orien="R0" />
        <instance x="2144" y="736" name="XLXI_6" orien="R0" />
        <branch name="Hex(7)">
            <wire x2="2144" y1="336" y2="336" x1="1952" />
        </branch>
        <branch name="Hex(6)">
            <wire x2="2144" y1="416" y2="416" x1="1952" />
        </branch>
        <branch name="Hex(5)">
            <wire x2="2144" y1="608" y2="608" x1="1952" />
        </branch>
        <branch name="Hex(4)">
            <wire x2="2144" y1="704" y2="704" x1="1952" />
        </branch>
        <branch name="Hexo(3:0)">
            <wire x2="2704" y1="272" y2="336" x1="2704" />
            <wire x2="2704" y1="336" y2="416" x1="2704" />
            <wire x2="2704" y1="416" y2="512" x1="2704" />
            <wire x2="2864" y1="512" y2="512" x1="2704" />
            <wire x2="2704" y1="512" y2="608" x1="2704" />
            <wire x2="2704" y1="608" y2="704" x1="2704" />
            <wire x2="2704" y1="704" y2="800" x1="2704" />
        </branch>
        <bustap x2="2608" y1="336" y2="336" x1="2704" />
        <bustap x2="2608" y1="416" y2="416" x1="2704" />
        <bustap x2="2608" y1="608" y2="608" x1="2704" />
        <bustap x2="2608" y1="704" y2="704" x1="2704" />
        <branch name="Hexo(3)">
            <wire x2="2608" y1="336" y2="336" x1="2368" />
        </branch>
        <branch name="Hexo(2)">
            <wire x2="2608" y1="416" y2="416" x1="2368" />
        </branch>
        <branch name="Hexo(1)">
            <wire x2="2608" y1="608" y2="608" x1="2368" />
        </branch>
        <branch name="Hexo(0)">
            <wire x2="2608" y1="704" y2="704" x1="2368" />
        </branch>
        <branch name="V5">
            <wire x2="2816" y1="1008" y2="1120" x1="2816" />
            <wire x2="3056" y1="1120" y2="1120" x1="2816" />
        </branch>
        <instance x="2752" y="1008" name="XLXI_7" orien="R0" />
        <branch name="G0">
            <wire x2="3312" y1="944" y2="944" x1="3104" />
        </branch>
        <instance x="3248" y="1072" name="XLXI_8" orien="R0" />
        <branch name="COM(7:0)">
            <wire x2="1520" y1="1472" y2="1472" x1="1424" />
            <wire x2="1520" y1="1472" y2="1536" x1="1520" />
            <wire x2="1520" y1="1536" y2="1616" x1="1520" />
            <wire x2="1520" y1="1616" y2="1728" x1="1520" />
            <wire x2="1520" y1="1728" y2="1840" x1="1520" />
            <wire x2="1520" y1="1840" y2="1904" x1="1520" />
            <wire x2="1520" y1="1344" y2="1360" x1="1520" />
            <wire x2="1520" y1="1360" y2="1440" x1="1520" />
            <wire x2="1520" y1="1440" y2="1472" x1="1520" />
        </branch>
        <bustap x2="1616" y1="1360" y2="1360" x1="1520" />
        <bustap x2="1616" y1="1440" y2="1440" x1="1520" />
        <bustap x2="1616" y1="1536" y2="1536" x1="1520" />
        <bustap x2="1616" y1="1616" y2="1616" x1="1520" />
        <bustap x2="1616" y1="1728" y2="1728" x1="1520" />
        <bustap x2="1616" y1="1840" y2="1840" x1="1520" />
        <instance x="1776" y="1392" name="XLXI_15" orien="R0" />
        <instance x="1776" y="1472" name="XLXI_16" orien="R0" />
        <instance x="1776" y="1568" name="XLXI_17" orien="R0" />
        <instance x="1776" y="1648" name="XLXI_18" orien="R0" />
        <instance x="1776" y="1760" name="XLXI_19" orien="R0" />
        <instance x="1776" y="1872" name="XLXI_20" orien="R0" />
        <branch name="COM(3)">
            <wire x2="1776" y1="1360" y2="1360" x1="1616" />
        </branch>
        <branch name="COM(2)">
            <wire x2="1776" y1="1440" y2="1440" x1="1616" />
        </branch>
        <branch name="COM(1)">
            <wire x2="1776" y1="1536" y2="1536" x1="1616" />
        </branch>
        <branch name="COM(0)">
            <wire x2="1776" y1="1616" y2="1616" x1="1616" />
        </branch>
        <branch name="COM(7)">
            <wire x2="1776" y1="1728" y2="1728" x1="1616" />
        </branch>
        <branch name="COM(6)">
            <wire x2="1776" y1="1840" y2="1840" x1="1616" />
        </branch>
        <branch name="AN(3:0)">
            <wire x2="2352" y1="1312" y2="1360" x1="2352" />
            <wire x2="2352" y1="1360" y2="1392" x1="2352" />
            <wire x2="2544" y1="1392" y2="1392" x1="2352" />
            <wire x2="2352" y1="1392" y2="1440" x1="2352" />
            <wire x2="2352" y1="1440" y2="1536" x1="2352" />
            <wire x2="2352" y1="1536" y2="1616" x1="2352" />
            <wire x2="2352" y1="1616" y2="1648" x1="2352" />
        </branch>
        <bustap x2="2256" y1="1360" y2="1360" x1="2352" />
        <bustap x2="2256" y1="1440" y2="1440" x1="2352" />
        <bustap x2="2256" y1="1536" y2="1536" x1="2352" />
        <bustap x2="2256" y1="1616" y2="1616" x1="2352" />
        <branch name="p">
            <wire x2="2016" y1="1840" y2="1840" x1="2000" />
            <wire x2="2128" y1="1840" y2="1840" x1="2016" />
        </branch>
        <branch name="AN(3)">
            <wire x2="2256" y1="1360" y2="1360" x1="2000" />
        </branch>
        <branch name="AN(2)">
            <wire x2="2256" y1="1440" y2="1440" x1="2000" />
        </branch>
        <branch name="AN(1)">
            <wire x2="2256" y1="1536" y2="1536" x1="2000" />
        </branch>
        <branch name="AN(0)">
            <wire x2="2256" y1="1616" y2="1616" x1="2000" />
        </branch>
        <branch name="LE">
            <wire x2="2032" y1="1728" y2="1728" x1="2000" />
        </branch>
        <iomarker fontsize="28" x="544" y="240" name="Scan(2:0)" orien="R180" />
        <iomarker fontsize="28" x="496" y="400" name="point(7:0)" orien="R180" />
        <iomarker fontsize="28" x="496" y="464" name="LES(7:0)" orien="R180" />
        <iomarker fontsize="28" x="496" y="336" name="Hexs(31:0)" orien="R180" />
        <iomarker fontsize="28" x="2864" y="512" name="Hexo(3:0)" orien="R0" />
        <iomarker fontsize="28" x="2544" y="1392" name="AN(3:0)" orien="R0" />
        <iomarker fontsize="28" x="2128" y="1840" name="p" orien="R0" />
        <iomarker fontsize="28" x="2032" y="1728" name="LE" orien="R0" />
    </sheet>
</drawing>