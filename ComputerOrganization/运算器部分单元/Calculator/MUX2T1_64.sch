<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="a(63:0)" />
        <signal name="a(7:0)" />
        <signal name="a(15:8)" />
        <signal name="a(23:16)" />
        <signal name="a(31:24)" />
        <signal name="a(39:32)" />
        <signal name="a(47:40)" />
        <signal name="a(55:48)" />
        <signal name="a(63:56)" />
        <signal name="XLXN_18(7:0)" />
        <signal name="b(63:0)" />
        <signal name="b(7:0)" />
        <signal name="b(15:8)" />
        <signal name="b(23:16)" />
        <signal name="b(31:24)" />
        <signal name="b(39:32)" />
        <signal name="b(47:40)" />
        <signal name="b(55:48)" />
        <signal name="b(63:56)" />
        <signal name="SW0" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_31" />
        <signal name="XLXN_32" />
        <signal name="XLXN_33" />
        <signal name="XLXN_34" />
        <signal name="o(63:0)" />
        <signal name="o(7:0)" />
        <signal name="o(15:8)" />
        <signal name="o(23:16)" />
        <signal name="o(31:24)" />
        <signal name="o(39:32)" />
        <signal name="o(47:40)" />
        <signal name="o(55:48)" />
        <signal name="o(63:56)" />
        <port polarity="Input" name="a(63:0)" />
        <port polarity="Input" name="a(15:8)" />
        <port polarity="Input" name="b(63:0)" />
        <port polarity="Input" name="SW0" />
        <port polarity="Output" name="o(63:0)" />
        <blockdef name="MUX2T1_8">
            <timestamp>2017-11-13T12:32:16</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <block symbolname="MUX2T1_8" name="XLXI_8">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(7:0)" name="I1(7:0)" />
            <blockpin signalname="a(7:0)" name="I0(7:0)" />
            <blockpin signalname="o(7:0)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_1">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(15:8)" name="I1(7:0)" />
            <blockpin signalname="a(15:8)" name="I0(7:0)" />
            <blockpin signalname="o(15:8)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_2">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(23:16)" name="I1(7:0)" />
            <blockpin signalname="a(23:16)" name="I0(7:0)" />
            <blockpin signalname="o(23:16)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_3">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(31:24)" name="I1(7:0)" />
            <blockpin signalname="a(31:24)" name="I0(7:0)" />
            <blockpin signalname="o(31:24)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_4">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(39:32)" name="I1(7:0)" />
            <blockpin signalname="a(39:32)" name="I0(7:0)" />
            <blockpin signalname="o(39:32)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_5">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(47:40)" name="I1(7:0)" />
            <blockpin signalname="a(47:40)" name="I0(7:0)" />
            <blockpin signalname="o(47:40)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_6">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(55:48)" name="I1(7:0)" />
            <blockpin signalname="a(55:48)" name="I0(7:0)" />
            <blockpin signalname="o(55:48)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_7">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(63:56)" name="I1(7:0)" />
            <blockpin signalname="a(63:56)" name="I0(7:0)" />
            <blockpin signalname="o(63:56)" name="o(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="720" y="1040" name="XLXI_2" orien="R0">
        </instance>
        <instance x="720" y="1376" name="XLXI_3" orien="R0">
        </instance>
        <instance x="720" y="1696" name="XLXI_4" orien="R0">
        </instance>
        <instance x="720" y="1984" name="XLXI_5" orien="R0">
        </instance>
        <instance x="720" y="2304" name="XLXI_6" orien="R0">
        </instance>
        <instance x="720" y="2656" name="XLXI_7" orien="R0">
        </instance>
        <instance x="720" y="432" name="XLXI_8" orien="R0">
        </instance>
        <branch name="a(63:0)">
            <wire x2="368" y1="1168" y2="1168" x1="240" />
            <wire x2="368" y1="1168" y2="1344" x1="368" />
            <wire x2="368" y1="1344" y2="1664" x1="368" />
            <wire x2="368" y1="1664" y2="1952" x1="368" />
            <wire x2="368" y1="1952" y2="2272" x1="368" />
            <wire x2="368" y1="2272" y2="2624" x1="368" />
            <wire x2="368" y1="2624" y2="2640" x1="368" />
            <wire x2="368" y1="208" y2="400" x1="368" />
            <wire x2="368" y1="400" y2="720" x1="368" />
            <wire x2="368" y1="720" y2="1008" x1="368" />
            <wire x2="368" y1="1008" y2="1168" x1="368" />
        </branch>
        <iomarker fontsize="28" x="240" y="1168" name="a(63:0)" orien="R180" />
        <bustap x2="464" y1="400" y2="400" x1="368" />
        <bustap x2="464" y1="720" y2="720" x1="368" />
        <bustap x2="464" y1="1008" y2="1008" x1="368" />
        <bustap x2="464" y1="1344" y2="1344" x1="368" />
        <bustap x2="464" y1="1664" y2="1664" x1="368" />
        <bustap x2="464" y1="1952" y2="1952" x1="368" />
        <bustap x2="464" y1="2272" y2="2272" x1="368" />
        <bustap x2="464" y1="2624" y2="2624" x1="368" />
        <branch name="a(7:0)">
            <wire x2="720" y1="400" y2="400" x1="464" />
        </branch>
        <branch name="a(15:8)">
            <wire x2="720" y1="720" y2="720" x1="464" />
        </branch>
        <branch name="a(23:16)">
            <wire x2="720" y1="1008" y2="1008" x1="464" />
        </branch>
        <branch name="a(31:24)">
            <wire x2="720" y1="1344" y2="1344" x1="464" />
        </branch>
        <branch name="a(39:32)">
            <wire x2="720" y1="1664" y2="1664" x1="464" />
        </branch>
        <branch name="a(47:40)">
            <wire x2="720" y1="1952" y2="1952" x1="464" />
        </branch>
        <branch name="a(55:48)">
            <wire x2="720" y1="2272" y2="2272" x1="464" />
        </branch>
        <branch name="a(63:56)">
            <wire x2="720" y1="2624" y2="2624" x1="464" />
        </branch>
        <instance x="720" y="752" name="XLXI_1" orien="R0">
        </instance>
        <branch name="b(63:0)">
            <wire x2="464" y1="1232" y2="1232" x1="240" />
            <wire x2="464" y1="1232" y2="1280" x1="464" />
            <wire x2="464" y1="1280" y2="1600" x1="464" />
            <wire x2="464" y1="1600" y2="1888" x1="464" />
            <wire x2="464" y1="1888" y2="2208" x1="464" />
            <wire x2="464" y1="2208" y2="2560" x1="464" />
            <wire x2="464" y1="2560" y2="2592" x1="464" />
            <wire x2="464" y1="208" y2="336" x1="464" />
            <wire x2="464" y1="336" y2="656" x1="464" />
            <wire x2="464" y1="656" y2="944" x1="464" />
            <wire x2="464" y1="944" y2="1232" x1="464" />
        </branch>
        <bustap x2="560" y1="336" y2="336" x1="464" />
        <bustap x2="560" y1="656" y2="656" x1="464" />
        <bustap x2="560" y1="944" y2="944" x1="464" />
        <bustap x2="560" y1="1280" y2="1280" x1="464" />
        <bustap x2="560" y1="1600" y2="1600" x1="464" />
        <bustap x2="560" y1="1888" y2="1888" x1="464" />
        <bustap x2="560" y1="2208" y2="2208" x1="464" />
        <bustap x2="560" y1="2560" y2="2560" x1="464" />
        <branch name="b(7:0)">
            <wire x2="720" y1="336" y2="336" x1="560" />
        </branch>
        <branch name="b(15:8)">
            <wire x2="720" y1="656" y2="656" x1="560" />
        </branch>
        <branch name="b(23:16)">
            <wire x2="720" y1="944" y2="944" x1="560" />
        </branch>
        <branch name="b(31:24)">
            <wire x2="720" y1="1280" y2="1280" x1="560" />
        </branch>
        <branch name="b(39:32)">
            <wire x2="720" y1="1600" y2="1600" x1="560" />
        </branch>
        <branch name="b(47:40)">
            <wire x2="720" y1="1888" y2="1888" x1="560" />
        </branch>
        <branch name="b(55:48)">
            <wire x2="720" y1="2208" y2="2208" x1="560" />
        </branch>
        <branch name="b(63:56)">
            <wire x2="720" y1="2560" y2="2560" x1="560" />
        </branch>
        <branch name="SW0">
            <wire x2="576" y1="80" y2="80" x1="144" />
            <wire x2="576" y1="80" y2="272" x1="576" />
            <wire x2="720" y1="272" y2="272" x1="576" />
            <wire x2="576" y1="272" y2="592" x1="576" />
            <wire x2="720" y1="592" y2="592" x1="576" />
            <wire x2="576" y1="592" y2="880" x1="576" />
            <wire x2="720" y1="880" y2="880" x1="576" />
            <wire x2="576" y1="880" y2="1216" x1="576" />
            <wire x2="720" y1="1216" y2="1216" x1="576" />
            <wire x2="576" y1="1216" y2="1536" x1="576" />
            <wire x2="720" y1="1536" y2="1536" x1="576" />
            <wire x2="576" y1="1536" y2="1824" x1="576" />
            <wire x2="720" y1="1824" y2="1824" x1="576" />
            <wire x2="576" y1="1824" y2="2144" x1="576" />
            <wire x2="720" y1="2144" y2="2144" x1="576" />
            <wire x2="576" y1="2144" y2="2496" x1="576" />
            <wire x2="720" y1="2496" y2="2496" x1="576" />
        </branch>
        <iomarker fontsize="28" x="144" y="80" name="SW0" orien="R180" />
        <branch name="o(63:0)">
            <wire x2="1440" y1="192" y2="272" x1="1440" />
            <wire x2="1440" y1="272" y2="592" x1="1440" />
            <wire x2="1440" y1="592" y2="880" x1="1440" />
            <wire x2="1440" y1="880" y2="1216" x1="1440" />
            <wire x2="1440" y1="1216" y2="1536" x1="1440" />
            <wire x2="1440" y1="1536" y2="1664" x1="1440" />
            <wire x2="1680" y1="1664" y2="1664" x1="1440" />
            <wire x2="1440" y1="1664" y2="1824" x1="1440" />
            <wire x2="1440" y1="1824" y2="2144" x1="1440" />
            <wire x2="1440" y1="2144" y2="2496" x1="1440" />
            <wire x2="1440" y1="2496" y2="2704" x1="1440" />
        </branch>
        <iomarker fontsize="28" x="1680" y="1664" name="o(63:0)" orien="R0" />
        <bustap x2="1344" y1="272" y2="272" x1="1440" />
        <bustap x2="1344" y1="592" y2="592" x1="1440" />
        <bustap x2="1344" y1="880" y2="880" x1="1440" />
        <bustap x2="1344" y1="1216" y2="1216" x1="1440" />
        <bustap x2="1344" y1="1536" y2="1536" x1="1440" />
        <bustap x2="1344" y1="1824" y2="1824" x1="1440" />
        <bustap x2="1344" y1="2144" y2="2144" x1="1440" />
        <bustap x2="1344" y1="2496" y2="2496" x1="1440" />
        <branch name="o(7:0)">
            <wire x2="1344" y1="272" y2="272" x1="1104" />
        </branch>
        <branch name="o(15:8)">
            <wire x2="1344" y1="592" y2="592" x1="1104" />
        </branch>
        <branch name="o(23:16)">
            <wire x2="1344" y1="880" y2="880" x1="1104" />
        </branch>
        <branch name="o(31:24)">
            <wire x2="1344" y1="1216" y2="1216" x1="1104" />
        </branch>
        <branch name="o(39:32)">
            <wire x2="1344" y1="1536" y2="1536" x1="1104" />
        </branch>
        <branch name="o(47:40)">
            <wire x2="1344" y1="1824" y2="1824" x1="1104" />
        </branch>
        <branch name="o(55:48)">
            <wire x2="1344" y1="2144" y2="2144" x1="1104" />
        </branch>
        <branch name="o(63:56)">
            <wire x2="1344" y1="2496" y2="2496" x1="1104" />
        </branch>
        <iomarker fontsize="28" x="240" y="1232" name="b(63:0)" orien="R180" />
    </sheet>
</drawing>