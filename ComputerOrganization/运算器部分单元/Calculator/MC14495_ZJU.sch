<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="a" />
        <signal name="b" />
        <signal name="c" />
        <signal name="d" />
        <signal name="e" />
        <signal name="f" />
        <signal name="g" />
        <signal name="point" />
        <signal name="p" />
        <signal name="D0" />
        <signal name="D1" />
        <signal name="D2" />
        <signal name="D3" />
        <signal name="LE" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_29" />
        <signal name="XLXN_30" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_55" />
        <signal name="XLXN_56" />
        <signal name="XLXN_58" />
        <signal name="XLXN_59" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="XLXN_66" />
        <signal name="XLXN_67" />
        <signal name="XLXN_68" />
        <signal name="XLXN_69" />
        <signal name="XLXN_70" />
        <signal name="XLXN_78" />
        <signal name="XLXN_79" />
        <signal name="XLXN_84" />
        <signal name="XLXN_85" />
        <signal name="XLXN_87" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="XLXN_96" />
        <signal name="XLXN_97" />
        <signal name="XLXN_98" />
        <signal name="XLXN_99" />
        <signal name="XLXN_100" />
        <port polarity="Output" name="a" />
        <port polarity="Output" name="b" />
        <port polarity="Output" name="c" />
        <port polarity="Output" name="d" />
        <port polarity="Output" name="e" />
        <port polarity="Output" name="f" />
        <port polarity="Output" name="g" />
        <port polarity="Input" name="point" />
        <port polarity="Output" name="p" />
        <port polarity="Input" name="D0" />
        <port polarity="Input" name="D1" />
        <port polarity="Input" name="D2" />
        <port polarity="Input" name="D3" />
        <port polarity="Input" name="LE" />
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="and4" name="XLXI_1">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_42" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_10">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_29" name="I3" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_18">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_30" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_67" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_33">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_30" name="I2" />
            <blockpin signalname="XLXN_29" name="I3" />
            <blockpin signalname="XLXN_79" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_37">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_29" name="I3" />
            <blockpin signalname="XLXN_90" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_38">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_30" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_91" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_39">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="D3" name="I3" />
            <blockpin signalname="XLXN_60" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_40">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_29" name="I3" />
            <blockpin signalname="XLXN_69" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_41">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_27" name="I2" />
            <blockpin signalname="XLXN_29" name="I3" />
            <blockpin signalname="XLXN_70" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_78">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_94" name="I1" />
            <blockpin signalname="g" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_79">
            <blockpin signalname="XLXN_42" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="XLXN_55" name="I2" />
            <blockpin signalname="XLXN_94" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_81">
            <blockpin signalname="XLXN_61" name="I0" />
            <blockpin signalname="XLXN_62" name="I1" />
            <blockpin signalname="XLXN_66" name="I2" />
            <blockpin signalname="XLXN_96" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_83">
            <blockpin signalname="XLXN_78" name="I0" />
            <blockpin signalname="XLXN_79" name="I1" />
            <blockpin signalname="XLXN_84" name="I2" />
            <blockpin signalname="XLXN_98" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_86">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_95" name="I1" />
            <blockpin signalname="f" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_87">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_96" name="I1" />
            <blockpin signalname="e" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_88">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_97" name="I1" />
            <blockpin signalname="d" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_89">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_98" name="I1" />
            <blockpin signalname="c" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_90">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_99" name="I1" />
            <blockpin signalname="b" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_91">
            <blockpin signalname="LE" name="I0" />
            <blockpin signalname="XLXN_100" name="I1" />
            <blockpin signalname="a" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_92">
            <blockpin signalname="point" name="I" />
            <blockpin signalname="p" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_93">
            <blockpin signalname="D0" name="I" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_94">
            <blockpin signalname="D1" name="I" />
            <blockpin signalname="XLXN_27" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_95">
            <blockpin signalname="D2" name="I" />
            <blockpin signalname="XLXN_30" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_96">
            <blockpin signalname="D3" name="I" />
            <blockpin signalname="XLXN_29" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_97">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_55" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_98">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_56" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_99">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_58" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_100">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_30" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_59" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_101">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_27" name="I1" />
            <blockpin signalname="XLXN_30" name="I2" />
            <blockpin signalname="XLXN_61" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_102">
            <blockpin signalname="XLXN_27" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="XLXN_29" name="I2" />
            <blockpin signalname="XLXN_62" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_103">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="XLXN_29" name="I1" />
            <blockpin signalname="XLXN_66" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_104">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_68" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_105">
            <blockpin signalname="D1" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_78" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_106">
            <blockpin signalname="D0" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_85" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_107">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="D2" name="I1" />
            <blockpin signalname="D3" name="I2" />
            <blockpin signalname="XLXN_84" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_108">
            <blockpin signalname="XLXN_26" name="I0" />
            <blockpin signalname="D1" name="I1" />
            <blockpin signalname="D2" name="I2" />
            <blockpin signalname="XLXN_87" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_109">
            <blockpin signalname="XLXN_56" name="I0" />
            <blockpin signalname="XLXN_58" name="I1" />
            <blockpin signalname="XLXN_59" name="I2" />
            <blockpin signalname="XLXN_60" name="I3" />
            <blockpin signalname="XLXN_95" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_110">
            <blockpin signalname="XLXN_67" name="I0" />
            <blockpin signalname="XLXN_68" name="I1" />
            <blockpin signalname="XLXN_69" name="I2" />
            <blockpin signalname="XLXN_70" name="I3" />
            <blockpin signalname="XLXN_97" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_111">
            <blockpin signalname="XLXN_85" name="I0" />
            <blockpin signalname="XLXN_84" name="I1" />
            <blockpin signalname="XLXN_87" name="I2" />
            <blockpin signalname="XLXN_90" name="I3" />
            <blockpin signalname="XLXN_99" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_112">
            <blockpin signalname="XLXN_91" name="I0" />
            <blockpin signalname="XLXN_60" name="I1" />
            <blockpin signalname="XLXN_69" name="I2" />
            <blockpin signalname="XLXN_70" name="I3" />
            <blockpin signalname="XLXN_100" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="352" y="1200" name="XLXI_1" orien="R90" />
        <instance x="640" y="1200" name="XLXI_10" orien="R90" />
        <instance x="3040" y="1200" name="XLXI_18" orien="R90" />
        <instance x="3824" y="1200" name="XLXI_33" orien="R90" />
        <instance x="5024" y="1200" name="XLXI_37" orien="R90" />
        <instance x="5328" y="1200" name="XLXI_38" orien="R90" />
        <instance x="5632" y="1200" name="XLXI_39" orien="R90" />
        <instance x="5936" y="1200" name="XLXI_40" orien="R90" />
        <instance x="6240" y="1200" name="XLXI_41" orien="R90" />
        <instance x="704" y="2448" name="XLXI_78" orien="R90" />
        <instance x="1712" y="2432" name="XLXI_86" orien="R90" />
        <instance x="3264" y="2416" name="XLXI_88" orien="R90" />
        <instance x="3840" y="2400" name="XLXI_89" orien="R90" />
        <instance x="4720" y="2400" name="XLXI_90" orien="R90" />
        <branch name="a">
            <wire x2="5952" y1="2656" y2="2704" x1="5952" />
        </branch>
        <branch name="b">
            <wire x2="4816" y1="2656" y2="2688" x1="4816" />
        </branch>
        <iomarker fontsize="28" x="4816" y="2688" name="b" orien="R90" />
        <branch name="c">
            <wire x2="3936" y1="2656" y2="2688" x1="3936" />
        </branch>
        <iomarker fontsize="28" x="3936" y="2688" name="c" orien="R90" />
        <branch name="d">
            <wire x2="3360" y1="2672" y2="2704" x1="3360" />
        </branch>
        <iomarker fontsize="28" x="3360" y="2704" name="d" orien="R90" />
        <branch name="e">
            <wire x2="2608" y1="2688" y2="2720" x1="2608" />
        </branch>
        <branch name="f">
            <wire x2="1808" y1="2688" y2="2720" x1="1808" />
        </branch>
        <iomarker fontsize="28" x="1808" y="2720" name="f" orien="R90" />
        <branch name="g">
            <wire x2="800" y1="2704" y2="2736" x1="800" />
        </branch>
        <iomarker fontsize="28" x="800" y="2736" name="g" orien="R90" />
        <instance x="128" y="704" name="XLXI_92" orien="R90" />
        <branch name="point">
            <wire x2="160" y1="336" y2="704" x1="160" />
        </branch>
        <branch name="p">
            <wire x2="160" y1="928" y2="1632" x1="160" />
        </branch>
        <branch name="D0">
            <wire x2="704" y1="352" y2="1200" x1="704" />
            <wire x2="1328" y1="352" y2="352" x1="704" />
            <wire x2="1328" y1="352" y2="1200" x1="1328" />
            <wire x2="1920" y1="352" y2="352" x1="1328" />
            <wire x2="1920" y1="352" y2="1200" x1="1920" />
            <wire x2="2240" y1="352" y2="352" x1="1920" />
            <wire x2="2240" y1="352" y2="1200" x1="2240" />
            <wire x2="2864" y1="352" y2="352" x1="2240" />
            <wire x2="2864" y1="352" y2="1200" x1="2864" />
            <wire x2="3408" y1="352" y2="352" x1="2864" />
            <wire x2="3408" y1="352" y2="1200" x1="3408" />
            <wire x2="4224" y1="352" y2="352" x1="3408" />
            <wire x2="4224" y1="352" y2="1200" x1="4224" />
            <wire x2="5088" y1="352" y2="352" x1="4224" />
            <wire x2="5248" y1="352" y2="352" x1="5088" />
            <wire x2="5248" y1="352" y2="480" x1="5248" />
            <wire x2="5392" y1="352" y2="352" x1="5248" />
            <wire x2="5392" y1="352" y2="1200" x1="5392" />
            <wire x2="5696" y1="352" y2="352" x1="5392" />
            <wire x2="5696" y1="352" y2="1200" x1="5696" />
            <wire x2="6304" y1="352" y2="352" x1="5696" />
            <wire x2="6304" y1="352" y2="1200" x1="6304" />
            <wire x2="5088" y1="352" y2="1200" x1="5088" />
            <wire x2="5248" y1="256" y2="352" x1="5248" />
        </branch>
        <iomarker fontsize="28" x="5248" y="256" name="D0" orien="R270" />
        <branch name="D1">
            <wire x2="768" y1="384" y2="1200" x1="768" />
            <wire x2="1392" y1="384" y2="384" x1="768" />
            <wire x2="1392" y1="384" y2="1200" x1="1392" />
            <wire x2="1616" y1="384" y2="384" x1="1392" />
            <wire x2="1616" y1="384" y2="1200" x1="1616" />
            <wire x2="3168" y1="384" y2="384" x1="1616" />
            <wire x2="3168" y1="384" y2="1200" x1="3168" />
            <wire x2="3472" y1="384" y2="384" x1="3168" />
            <wire x2="3472" y1="384" y2="1200" x1="3472" />
            <wire x2="3648" y1="384" y2="384" x1="3472" />
            <wire x2="3648" y1="384" y2="1200" x1="3648" />
            <wire x2="3952" y1="384" y2="384" x1="3648" />
            <wire x2="3952" y1="384" y2="1200" x1="3952" />
            <wire x2="4288" y1="384" y2="384" x1="3952" />
            <wire x2="4288" y1="384" y2="1200" x1="4288" />
            <wire x2="4848" y1="384" y2="384" x1="4288" />
            <wire x2="4848" y1="384" y2="1200" x1="4848" />
            <wire x2="5456" y1="384" y2="384" x1="4848" />
            <wire x2="5504" y1="384" y2="384" x1="5456" />
            <wire x2="5504" y1="384" y2="480" x1="5504" />
            <wire x2="5456" y1="384" y2="1200" x1="5456" />
            <wire x2="5504" y1="256" y2="384" x1="5504" />
        </branch>
        <branch name="D2">
            <wire x2="544" y1="416" y2="1200" x1="544" />
            <wire x2="832" y1="416" y2="416" x1="544" />
            <wire x2="832" y1="416" y2="1200" x1="832" />
            <wire x2="2624" y1="416" y2="416" x1="832" />
            <wire x2="2624" y1="416" y2="1200" x1="2624" />
            <wire x2="3536" y1="416" y2="416" x1="2624" />
            <wire x2="3536" y1="416" y2="1200" x1="3536" />
            <wire x2="3712" y1="416" y2="416" x1="3536" />
            <wire x2="3712" y1="416" y2="1200" x1="3712" />
            <wire x2="4560" y1="416" y2="416" x1="3712" />
            <wire x2="4560" y1="416" y2="1200" x1="4560" />
            <wire x2="4912" y1="416" y2="416" x1="4560" />
            <wire x2="4912" y1="416" y2="1200" x1="4912" />
            <wire x2="5200" y1="416" y2="416" x1="4912" />
            <wire x2="5200" y1="416" y2="768" x1="5200" />
            <wire x2="5216" y1="768" y2="768" x1="5200" />
            <wire x2="5216" y1="768" y2="1200" x1="5216" />
            <wire x2="5792" y1="416" y2="416" x1="5200" />
            <wire x2="5792" y1="416" y2="480" x1="5792" />
            <wire x2="5888" y1="416" y2="416" x1="5792" />
            <wire x2="5888" y1="416" y2="768" x1="5888" />
            <wire x2="6128" y1="416" y2="416" x1="5888" />
            <wire x2="6128" y1="416" y2="1200" x1="6128" />
            <wire x2="5792" y1="256" y2="416" x1="5792" />
            <wire x2="5888" y1="768" y2="768" x1="5824" />
            <wire x2="5824" y1="768" y2="1200" x1="5824" />
        </branch>
        <branch name="D3">
            <wire x2="608" y1="464" y2="1200" x1="608" />
            <wire x2="3296" y1="464" y2="464" x1="608" />
            <wire x2="3296" y1="464" y2="1200" x1="3296" />
            <wire x2="3776" y1="464" y2="464" x1="3296" />
            <wire x2="3776" y1="464" y2="1200" x1="3776" />
            <wire x2="4352" y1="464" y2="464" x1="3776" />
            <wire x2="4352" y1="464" y2="1200" x1="4352" />
            <wire x2="4624" y1="464" y2="464" x1="4352" />
            <wire x2="4624" y1="464" y2="1200" x1="4624" />
            <wire x2="5584" y1="464" y2="464" x1="4624" />
            <wire x2="5584" y1="464" y2="1200" x1="5584" />
            <wire x2="5936" y1="464" y2="464" x1="5584" />
            <wire x2="6064" y1="464" y2="464" x1="5936" />
            <wire x2="6064" y1="464" y2="480" x1="6064" />
            <wire x2="5936" y1="464" y2="1200" x1="5936" />
            <wire x2="5936" y1="1200" y2="1200" x1="5888" />
            <wire x2="6064" y1="256" y2="464" x1="6064" />
        </branch>
        <instance x="5216" y="480" name="XLXI_93" orien="R90" />
        <iomarker fontsize="28" x="5504" y="256" name="D1" orien="R270" />
        <instance x="5472" y="480" name="XLXI_94" orien="R90" />
        <iomarker fontsize="28" x="5792" y="256" name="D2" orien="R270" />
        <instance x="5760" y="480" name="XLXI_95" orien="R90" />
        <iomarker fontsize="28" x="6064" y="256" name="D3" orien="R270" />
        <instance x="6032" y="480" name="XLXI_96" orien="R90" />
        <branch name="LE">
            <wire x2="48" y1="352" y2="2304" x1="48" />
            <wire x2="768" y1="2304" y2="2304" x1="48" />
            <wire x2="768" y1="2304" y2="2448" x1="768" />
            <wire x2="1776" y1="2304" y2="2304" x1="768" />
            <wire x2="1776" y1="2304" y2="2432" x1="1776" />
            <wire x2="2576" y1="2304" y2="2304" x1="1776" />
            <wire x2="3328" y1="2304" y2="2304" x1="2576" />
            <wire x2="3328" y1="2304" y2="2416" x1="3328" />
            <wire x2="3904" y1="2304" y2="2304" x1="3328" />
            <wire x2="3904" y1="2304" y2="2400" x1="3904" />
            <wire x2="4784" y1="2304" y2="2304" x1="3904" />
            <wire x2="4784" y1="2304" y2="2400" x1="4784" />
            <wire x2="5920" y1="2304" y2="2304" x1="4784" />
            <wire x2="5920" y1="2304" y2="2400" x1="5920" />
            <wire x2="2576" y1="2304" y2="2432" x1="2576" />
        </branch>
        <iomarker fontsize="28" x="48" y="352" name="LE" orien="R270" />
        <iomarker fontsize="28" x="5952" y="2704" name="a" orien="R90" />
        <instance x="5856" y="2400" name="XLXI_91" orien="R90" />
        <iomarker fontsize="28" x="2608" y="2720" name="e" orien="R90" />
        <instance x="2512" y="2432" name="XLXI_87" orien="R90" />
        <iomarker fontsize="28" x="160" y="1632" name="p" orien="R90" />
        <iomarker fontsize="28" x="160" y="336" name="point" orien="R270" />
        <branch name="XLXN_26">
            <wire x2="416" y1="784" y2="1200" x1="416" />
            <wire x2="3104" y1="784" y2="784" x1="416" />
            <wire x2="3104" y1="784" y2="1200" x1="3104" />
            <wire x2="3888" y1="784" y2="784" x1="3104" />
            <wire x2="3888" y1="784" y2="1200" x1="3888" />
            <wire x2="4496" y1="784" y2="784" x1="3888" />
            <wire x2="4496" y1="784" y2="1200" x1="4496" />
            <wire x2="4784" y1="784" y2="784" x1="4496" />
            <wire x2="5248" y1="784" y2="784" x1="4784" />
            <wire x2="6000" y1="784" y2="784" x1="5248" />
            <wire x2="6000" y1="784" y2="1200" x1="6000" />
            <wire x2="4784" y1="784" y2="1200" x1="4784" />
            <wire x2="5248" y1="704" y2="784" x1="5248" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="480" y1="832" y2="1200" x1="480" />
            <wire x2="1024" y1="832" y2="832" x1="480" />
            <wire x2="1024" y1="832" y2="1200" x1="1024" />
            <wire x2="2304" y1="832" y2="832" x1="1024" />
            <wire x2="2304" y1="832" y2="1200" x1="2304" />
            <wire x2="2560" y1="832" y2="832" x1="2304" />
            <wire x2="2560" y1="832" y2="1200" x1="2560" />
            <wire x2="5152" y1="832" y2="832" x1="2560" />
            <wire x2="5504" y1="832" y2="832" x1="5152" />
            <wire x2="5760" y1="832" y2="832" x1="5504" />
            <wire x2="5760" y1="832" y2="1200" x1="5760" />
            <wire x2="5872" y1="832" y2="832" x1="5760" />
            <wire x2="5872" y1="832" y2="1120" x1="5872" />
            <wire x2="6064" y1="1120" y2="1120" x1="5872" />
            <wire x2="6064" y1="1120" y2="1200" x1="6064" />
            <wire x2="6432" y1="832" y2="832" x1="5872" />
            <wire x2="6432" y1="832" y2="1200" x1="6432" />
            <wire x2="5152" y1="832" y2="1200" x1="5152" />
            <wire x2="5504" y1="704" y2="832" x1="5504" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="896" y1="1056" y2="1200" x1="896" />
            <wire x2="1152" y1="1056" y2="1056" x1="896" />
            <wire x2="1152" y1="1056" y2="1200" x1="1152" />
            <wire x2="1456" y1="1056" y2="1056" x1="1152" />
            <wire x2="1456" y1="1056" y2="1200" x1="1456" />
            <wire x2="1744" y1="1056" y2="1056" x1="1456" />
            <wire x2="1744" y1="1056" y2="1200" x1="1744" />
            <wire x2="2048" y1="1056" y2="1056" x1="1744" />
            <wire x2="2048" y1="1056" y2="1200" x1="2048" />
            <wire x2="2688" y1="1056" y2="1056" x1="2048" />
            <wire x2="2688" y1="1056" y2="1200" x1="2688" />
            <wire x2="2928" y1="1056" y2="1056" x1="2688" />
            <wire x2="2928" y1="1056" y2="1200" x1="2928" />
            <wire x2="4080" y1="1056" y2="1056" x1="2928" />
            <wire x2="4080" y1="1056" y2="1200" x1="4080" />
            <wire x2="5280" y1="1056" y2="1056" x1="4080" />
            <wire x2="6064" y1="1056" y2="1056" x1="5280" />
            <wire x2="6192" y1="1056" y2="1056" x1="6064" />
            <wire x2="6192" y1="1056" y2="1200" x1="6192" />
            <wire x2="6496" y1="1056" y2="1056" x1="6192" />
            <wire x2="6496" y1="1056" y2="1200" x1="6496" />
            <wire x2="5280" y1="1056" y2="1200" x1="5280" />
            <wire x2="6064" y1="704" y2="1056" x1="6064" />
        </branch>
        <instance x="960" y="1200" name="XLXI_97" orien="R90" />
        <instance x="1264" y="1200" name="XLXI_98" orien="R90" />
        <instance x="1552" y="1200" name="XLXI_99" orien="R90" />
        <instance x="1856" y="1200" name="XLXI_100" orien="R90" />
        <instance x="2800" y="1200" name="XLXI_103" orien="R90" />
        <instance x="2176" y="1200" name="XLXI_101" orien="R90" />
        <instance x="2496" y="1200" name="XLXI_102" orien="R90" />
        <instance x="3344" y="1200" name="XLXI_104" orien="R90" />
        <instance x="3584" y="1200" name="XLXI_105" orien="R90" />
        <instance x="4160" y="1200" name="XLXI_106" orien="R90" />
        <instance x="4432" y="1200" name="XLXI_107" orien="R90" />
        <instance x="4720" y="1200" name="XLXI_108" orien="R90" />
        <branch name="XLXN_30">
            <wire x2="1088" y1="944" y2="1200" x1="1088" />
            <wire x2="1680" y1="944" y2="944" x1="1088" />
            <wire x2="1680" y1="944" y2="1200" x1="1680" />
            <wire x2="1984" y1="944" y2="944" x1="1680" />
            <wire x2="1984" y1="944" y2="1200" x1="1984" />
            <wire x2="2368" y1="944" y2="944" x1="1984" />
            <wire x2="2368" y1="944" y2="1200" x1="2368" />
            <wire x2="3232" y1="944" y2="944" x1="2368" />
            <wire x2="3232" y1="944" y2="1200" x1="3232" />
            <wire x2="4016" y1="944" y2="944" x1="3232" />
            <wire x2="4016" y1="944" y2="1200" x1="4016" />
            <wire x2="5520" y1="944" y2="944" x1="4016" />
            <wire x2="5792" y1="944" y2="944" x1="5520" />
            <wire x2="6368" y1="944" y2="944" x1="5792" />
            <wire x2="6368" y1="944" y2="1200" x1="6368" />
            <wire x2="5520" y1="944" y2="1200" x1="5520" />
            <wire x2="5792" y1="704" y2="944" x1="5792" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="800" y1="1456" y2="1792" x1="800" />
        </branch>
        <instance x="672" y="1792" name="XLXI_79" orien="R90" />
        <branch name="XLXN_42">
            <wire x2="512" y1="1456" y2="1568" x1="512" />
            <wire x2="736" y1="1568" y2="1568" x1="512" />
            <wire x2="736" y1="1568" y2="1792" x1="736" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="864" y1="1568" y2="1792" x1="864" />
            <wire x2="1088" y1="1568" y2="1568" x1="864" />
            <wire x2="1088" y1="1456" y2="1568" x1="1088" />
        </branch>
        <instance x="1792" y="1776" name="XLXI_109" orien="R90" />
        <branch name="XLXN_56">
            <wire x2="1392" y1="1456" y2="1648" x1="1392" />
            <wire x2="1856" y1="1648" y2="1648" x1="1392" />
            <wire x2="1856" y1="1648" y2="1776" x1="1856" />
        </branch>
        <branch name="XLXN_58">
            <wire x2="1680" y1="1456" y2="1472" x1="1680" />
            <wire x2="1920" y1="1472" y2="1472" x1="1680" />
            <wire x2="1920" y1="1472" y2="1776" x1="1920" />
        </branch>
        <branch name="XLXN_59">
            <wire x2="1984" y1="1456" y2="1776" x1="1984" />
        </branch>
        <branch name="XLXN_60">
            <wire x2="2048" y1="1616" y2="1776" x1="2048" />
            <wire x2="5792" y1="1616" y2="1616" x1="2048" />
            <wire x2="5856" y1="1616" y2="1616" x1="5792" />
            <wire x2="5856" y1="1616" y2="1872" x1="5856" />
            <wire x2="5792" y1="1456" y2="1616" x1="5792" />
        </branch>
        <branch name="XLXN_62">
            <wire x2="2624" y1="1456" y2="1712" x1="2624" />
        </branch>
        <instance x="2496" y="1712" name="XLXI_81" orien="R90" />
        <branch name="XLXN_61">
            <wire x2="2304" y1="1456" y2="1472" x1="2304" />
            <wire x2="2560" y1="1472" y2="1472" x1="2304" />
            <wire x2="2560" y1="1472" y2="1712" x1="2560" />
        </branch>
        <branch name="XLXN_66">
            <wire x2="2688" y1="1472" y2="1712" x1="2688" />
            <wire x2="2896" y1="1472" y2="1472" x1="2688" />
            <wire x2="2896" y1="1456" y2="1472" x1="2896" />
        </branch>
        <branch name="XLXN_68">
            <wire x2="3472" y1="1472" y2="1472" x1="3456" />
            <wire x2="3456" y1="1472" y2="1792" x1="3456" />
            <wire x2="3472" y1="1456" y2="1472" x1="3472" />
        </branch>
        <branch name="XLXN_70">
            <wire x2="3584" y1="1712" y2="1792" x1="3584" />
            <wire x2="5984" y1="1712" y2="1712" x1="3584" />
            <wire x2="6400" y1="1712" y2="1712" x1="5984" />
            <wire x2="5984" y1="1712" y2="1872" x1="5984" />
            <wire x2="6400" y1="1456" y2="1712" x1="6400" />
        </branch>
        <instance x="3968" y="1792" name="XLXI_83" orien="R90" />
        <branch name="XLXN_69">
            <wire x2="3520" y1="1664" y2="1792" x1="3520" />
            <wire x2="5920" y1="1664" y2="1664" x1="3520" />
            <wire x2="6096" y1="1664" y2="1664" x1="5920" />
            <wire x2="5920" y1="1664" y2="1872" x1="5920" />
            <wire x2="6096" y1="1456" y2="1664" x1="6096" />
        </branch>
        <instance x="3328" y="1792" name="XLXI_110" orien="R90" />
        <branch name="XLXN_67">
            <wire x2="3200" y1="1456" y2="1472" x1="3200" />
            <wire x2="3392" y1="1472" y2="1472" x1="3200" />
            <wire x2="3392" y1="1472" y2="1792" x1="3392" />
        </branch>
        <branch name="XLXN_78">
            <wire x2="3712" y1="1456" y2="1744" x1="3712" />
            <wire x2="4032" y1="1744" y2="1744" x1="3712" />
            <wire x2="4032" y1="1744" y2="1792" x1="4032" />
        </branch>
        <branch name="XLXN_79">
            <wire x2="3984" y1="1456" y2="1600" x1="3984" />
            <wire x2="4096" y1="1600" y2="1600" x1="3984" />
            <wire x2="4096" y1="1600" y2="1792" x1="4096" />
        </branch>
        <branch name="XLXN_84">
            <wire x2="4160" y1="1600" y2="1792" x1="4160" />
            <wire x2="4560" y1="1600" y2="1600" x1="4160" />
            <wire x2="4928" y1="1600" y2="1600" x1="4560" />
            <wire x2="4928" y1="1600" y2="1824" x1="4928" />
            <wire x2="4560" y1="1456" y2="1600" x1="4560" />
        </branch>
        <instance x="4800" y="1824" name="XLXI_111" orien="R90" />
        <instance x="5728" y="1872" name="XLXI_112" orien="R90" />
        <branch name="XLXN_85">
            <wire x2="4288" y1="1456" y2="1760" x1="4288" />
            <wire x2="4864" y1="1760" y2="1760" x1="4288" />
            <wire x2="4864" y1="1760" y2="1824" x1="4864" />
        </branch>
        <branch name="XLXN_87">
            <wire x2="4848" y1="1456" y2="1584" x1="4848" />
            <wire x2="4992" y1="1584" y2="1584" x1="4848" />
            <wire x2="4992" y1="1584" y2="1824" x1="4992" />
        </branch>
        <branch name="XLXN_90">
            <wire x2="5184" y1="1584" y2="1584" x1="5056" />
            <wire x2="5056" y1="1584" y2="1824" x1="5056" />
            <wire x2="5184" y1="1456" y2="1584" x1="5184" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="5488" y1="1456" y2="1808" x1="5488" />
            <wire x2="5792" y1="1808" y2="1808" x1="5488" />
            <wire x2="5792" y1="1808" y2="1872" x1="5792" />
        </branch>
        <branch name="XLXN_94">
            <wire x2="800" y1="2048" y2="2064" x1="800" />
            <wire x2="832" y1="2064" y2="2064" x1="800" />
            <wire x2="832" y1="2064" y2="2448" x1="832" />
        </branch>
        <branch name="XLXN_95">
            <wire x2="1840" y1="2224" y2="2432" x1="1840" />
            <wire x2="1952" y1="2224" y2="2224" x1="1840" />
            <wire x2="1952" y1="2032" y2="2224" x1="1952" />
        </branch>
        <branch name="XLXN_96">
            <wire x2="2624" y1="1968" y2="2192" x1="2624" />
            <wire x2="2640" y1="2192" y2="2192" x1="2624" />
            <wire x2="2640" y1="2192" y2="2432" x1="2640" />
        </branch>
        <branch name="XLXN_97">
            <wire x2="3392" y1="2224" y2="2416" x1="3392" />
            <wire x2="3488" y1="2224" y2="2224" x1="3392" />
            <wire x2="3488" y1="2048" y2="2224" x1="3488" />
        </branch>
        <branch name="XLXN_98">
            <wire x2="3968" y1="2224" y2="2400" x1="3968" />
            <wire x2="4096" y1="2224" y2="2224" x1="3968" />
            <wire x2="4096" y1="2048" y2="2224" x1="4096" />
        </branch>
        <branch name="XLXN_99">
            <wire x2="4848" y1="2240" y2="2400" x1="4848" />
            <wire x2="4960" y1="2240" y2="2240" x1="4848" />
            <wire x2="4960" y1="2080" y2="2240" x1="4960" />
        </branch>
        <branch name="XLXN_100">
            <wire x2="5888" y1="2128" y2="2256" x1="5888" />
            <wire x2="5984" y1="2256" y2="2256" x1="5888" />
            <wire x2="5984" y1="2256" y2="2400" x1="5984" />
        </branch>
    </sheet>
</drawing>