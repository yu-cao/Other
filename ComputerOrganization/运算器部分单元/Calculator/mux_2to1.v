//32位2选一的选择器，S=1时选Y，S=0时选X
module mux_2to1(X, Y, S, Z);
	// parameter definitions
	parameter N = 32;

	//port definitions
	input wire [(N-1):0] X, Y;
	input wire S;

	output wire [(N-1):0] Z;

	//module body
	assign Z = S ? Y : X;
endmodule
