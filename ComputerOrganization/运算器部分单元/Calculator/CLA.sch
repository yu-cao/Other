<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="G3" />
        <signal name="P3" />
        <signal name="G2" />
        <signal name="P2" />
        <signal name="XLXN_9" />
        <signal name="G1" />
        <signal name="XLXN_11" />
        <signal name="P1" />
        <signal name="G0" />
        <signal name="P0" />
        <signal name="Ci" />
        <signal name="XLXN_16" />
        <signal name="C1" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="C2" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="C3" />
        <signal name="XLXN_28" />
        <signal name="GP" />
        <signal name="XLXN_30" />
        <signal name="XLXN_36" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="GG" />
        <port polarity="Input" name="G3" />
        <port polarity="Input" name="P3" />
        <port polarity="Input" name="G2" />
        <port polarity="Input" name="P2" />
        <port polarity="Input" name="G1" />
        <port polarity="Input" name="P1" />
        <port polarity="Input" name="G0" />
        <port polarity="Input" name="P0" />
        <port polarity="Input" name="Ci" />
        <port polarity="Output" name="C1" />
        <port polarity="Output" name="C2" />
        <port polarity="Output" name="C3" />
        <port polarity="Output" name="GP" />
        <port polarity="Output" name="GG" />
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="and3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <line x2="144" y1="-176" y2="-176" x1="64" />
            <line x2="64" y1="-80" y2="-80" x1="144" />
            <arc ex="144" ey="-176" sx="144" sy="-80" r="48" cx="144" cy="-128" />
            <line x2="64" y1="-64" y2="-192" x1="64" />
        </blockdef>
        <blockdef name="and4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-112" y2="-112" x1="144" />
            <arc ex="144" ey="-208" sx="144" sy="-112" r="48" cx="144" cy="-160" />
            <line x2="144" y1="-208" y2="-208" x1="64" />
            <line x2="64" y1="-64" y2="-256" x1="64" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
        </blockdef>
        <blockdef name="or3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="192" y1="-128" y2="-128" x1="256" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="or4" name="XLXI_1">
            <blockpin signalname="G3" name="I0" />
            <blockpin signalname="XLXN_37" name="I1" />
            <blockpin signalname="XLXN_38" name="I2" />
            <blockpin signalname="XLXN_39" name="I3" />
            <blockpin signalname="GG" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="P3" name="I0" />
            <blockpin signalname="G2" name="I1" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_3">
            <blockpin signalname="P3" name="I0" />
            <blockpin signalname="P2" name="I1" />
            <blockpin signalname="G1" name="I2" />
            <blockpin signalname="XLXN_38" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_4">
            <blockpin signalname="P3" name="I0" />
            <blockpin signalname="P2" name="I1" />
            <blockpin signalname="P1" name="I2" />
            <blockpin signalname="G0" name="I3" />
            <blockpin signalname="XLXN_39" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_5">
            <blockpin signalname="P3" name="I0" />
            <blockpin signalname="P2" name="I1" />
            <blockpin signalname="P1" name="I2" />
            <blockpin signalname="P0" name="I3" />
            <blockpin signalname="GP" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_6">
            <blockpin signalname="G2" name="I0" />
            <blockpin signalname="XLXN_24" name="I1" />
            <blockpin signalname="XLXN_25" name="I2" />
            <blockpin signalname="XLXN_26" name="I3" />
            <blockpin signalname="C3" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="P2" name="I0" />
            <blockpin signalname="G1" name="I1" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_8">
            <blockpin signalname="P2" name="I0" />
            <blockpin signalname="P1" name="I1" />
            <blockpin signalname="G0" name="I2" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="and4" name="XLXI_9">
            <blockpin signalname="P2" name="I0" />
            <blockpin signalname="P1" name="I1" />
            <blockpin signalname="P0" name="I2" />
            <blockpin signalname="Ci" name="I3" />
            <blockpin signalname="XLXN_26" name="O" />
        </block>
        <block symbolname="or3" name="XLXI_10">
            <blockpin signalname="G1" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_21" name="I2" />
            <blockpin signalname="C2" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_11">
            <blockpin signalname="G0" name="I0" />
            <blockpin signalname="XLXN_16" name="I1" />
            <blockpin signalname="C1" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="G0" name="I0" />
            <blockpin signalname="P1" name="I1" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="and3" name="XLXI_13">
            <blockpin signalname="P1" name="I0" />
            <blockpin signalname="P0" name="I1" />
            <blockpin signalname="Ci" name="I2" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="P0" name="I0" />
            <blockpin signalname="Ci" name="I1" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="960" y="4256" name="XLXI_1" orien="R90" />
        <instance x="800" y="3488" name="XLXI_2" orien="R90" />
        <instance x="1072" y="3552" name="XLXI_3" orien="R90" />
        <instance x="1456" y="3520" name="XLXI_4" orien="R90" />
        <instance x="2096" y="3520" name="XLXI_5" orien="R90" />
        <instance x="2960" y="4400" name="XLXI_6" orien="R90" />
        <instance x="2736" y="3536" name="XLXI_7" orien="R90" />
        <instance x="3008" y="3600" name="XLXI_8" orien="R90" />
        <instance x="3392" y="3568" name="XLXI_9" orien="R90" />
        <instance x="4080" y="4464" name="XLXI_10" orien="R90" />
        <instance x="5008" y="4464" name="XLXI_11" orien="R90" />
        <instance x="4080" y="3632" name="XLXI_12" orien="R90" />
        <instance x="4432" y="3664" name="XLXI_13" orien="R90" />
        <instance x="5200" y="3680" name="XLXI_14" orien="R90" />
        <branch name="G3">
            <wire x2="656" y1="2816" y2="2816" x1="640" />
            <wire x2="640" y1="2816" y2="4272" x1="640" />
            <wire x2="656" y1="4272" y2="4272" x1="640" />
            <wire x2="1024" y1="4256" y2="4256" x1="656" />
            <wire x2="656" y1="4256" y2="4272" x1="656" />
        </branch>
        <iomarker fontsize="28" x="656" y="2816" name="G3" orien="R0" />
        <iomarker fontsize="28" x="2160" y="2992" name="P3" orien="R270" />
        <branch name="G2">
            <wire x2="2688" y1="3072" y2="3072" x1="928" />
            <wire x2="2704" y1="3072" y2="3072" x1="2688" />
            <wire x2="2704" y1="3072" y2="3104" x1="2704" />
            <wire x2="2704" y1="3104" y2="4400" x1="2704" />
            <wire x2="3024" y1="4400" y2="4400" x1="2704" />
            <wire x2="928" y1="3072" y2="3488" x1="928" />
            <wire x2="2704" y1="3008" y2="3072" x1="2704" />
        </branch>
        <iomarker fontsize="28" x="2704" y="3008" name="G2" orien="R270" />
        <branch name="P2">
            <wire x2="1200" y1="3216" y2="3552" x1="1200" />
            <wire x2="1584" y1="3216" y2="3216" x1="1200" />
            <wire x2="2224" y1="3216" y2="3216" x1="1584" />
            <wire x2="2800" y1="3216" y2="3216" x1="2224" />
            <wire x2="3072" y1="3216" y2="3216" x1="2800" />
            <wire x2="3456" y1="3216" y2="3216" x1="3072" />
            <wire x2="3456" y1="3216" y2="3568" x1="3456" />
            <wire x2="3072" y1="3216" y2="3600" x1="3072" />
            <wire x2="2800" y1="3216" y2="3536" x1="2800" />
            <wire x2="2224" y1="3216" y2="3520" x1="2224" />
            <wire x2="1584" y1="3216" y2="3520" x1="1584" />
            <wire x2="3456" y1="2864" y2="3216" x1="3456" />
        </branch>
        <iomarker fontsize="28" x="3456" y="2864" name="P2" orien="R270" />
        <branch name="G1">
            <wire x2="2864" y1="3264" y2="3264" x1="1264" />
            <wire x2="3872" y1="3264" y2="3264" x1="2864" />
            <wire x2="3872" y1="3264" y2="4464" x1="3872" />
            <wire x2="4144" y1="4464" y2="4464" x1="3872" />
            <wire x2="2864" y1="3264" y2="3536" x1="2864" />
            <wire x2="1264" y1="3264" y2="3552" x1="1264" />
            <wire x2="3920" y1="2816" y2="2816" x1="3872" />
            <wire x2="3872" y1="2816" y2="3264" x1="3872" />
        </branch>
        <branch name="P1">
            <wire x2="2288" y1="3344" y2="3344" x1="1648" />
            <wire x2="3136" y1="3344" y2="3344" x1="2288" />
            <wire x2="3520" y1="3344" y2="3344" x1="3136" />
            <wire x2="4208" y1="3344" y2="3344" x1="3520" />
            <wire x2="4496" y1="3344" y2="3344" x1="4208" />
            <wire x2="4496" y1="3344" y2="3664" x1="4496" />
            <wire x2="4208" y1="3344" y2="3632" x1="4208" />
            <wire x2="3520" y1="3344" y2="3568" x1="3520" />
            <wire x2="3136" y1="3344" y2="3600" x1="3136" />
            <wire x2="2288" y1="3344" y2="3520" x1="2288" />
            <wire x2="1648" y1="3344" y2="3520" x1="1648" />
            <wire x2="4496" y1="2832" y2="3344" x1="4496" />
        </branch>
        <iomarker fontsize="28" x="4496" y="2832" name="P1" orien="R0" />
        <branch name="G0">
            <wire x2="3200" y1="3472" y2="3472" x1="1712" />
            <wire x2="4144" y1="3472" y2="3472" x1="3200" />
            <wire x2="4144" y1="3472" y2="3632" x1="4144" />
            <wire x2="3200" y1="3472" y2="3600" x1="3200" />
            <wire x2="1712" y1="3472" y2="3520" x1="1712" />
            <wire x2="5072" y1="3456" y2="3456" x1="4144" />
            <wire x2="5072" y1="3456" y2="4464" x1="5072" />
            <wire x2="4144" y1="3456" y2="3472" x1="4144" />
            <wire x2="5088" y1="2816" y2="2816" x1="5072" />
            <wire x2="5072" y1="2816" y2="3456" x1="5072" />
        </branch>
        <iomarker fontsize="28" x="5088" y="2816" name="G0" orien="R0" />
        <branch name="P0">
            <wire x2="2960" y1="3520" y2="3520" x1="2352" />
            <wire x2="2960" y1="3520" y2="3536" x1="2960" />
            <wire x2="3584" y1="3536" y2="3536" x1="2960" />
            <wire x2="3584" y1="3536" y2="3568" x1="3584" />
            <wire x2="4560" y1="3520" y2="3520" x1="3584" />
            <wire x2="5264" y1="3520" y2="3520" x1="4560" />
            <wire x2="5264" y1="3520" y2="3680" x1="5264" />
            <wire x2="4560" y1="3520" y2="3664" x1="4560" />
            <wire x2="3584" y1="3520" y2="3536" x1="3584" />
            <wire x2="5264" y1="2832" y2="3520" x1="5264" />
        </branch>
        <iomarker fontsize="28" x="5264" y="2832" name="P0" orien="R270" />
        <iomarker fontsize="28" x="5328" y="2832" name="Ci" orien="R270" />
        <branch name="XLXN_16">
            <wire x2="5136" y1="4192" y2="4464" x1="5136" />
            <wire x2="5296" y1="4192" y2="4192" x1="5136" />
            <wire x2="5296" y1="3936" y2="4192" x1="5296" />
        </branch>
        <branch name="C1">
            <wire x2="5104" y1="4720" y2="4752" x1="5104" />
        </branch>
        <iomarker fontsize="28" x="5104" y="4752" name="C1" orien="R90" />
        <iomarker fontsize="28" x="3920" y="2816" name="G1" orien="R0" />
        <branch name="XLXN_20">
            <wire x2="4176" y1="3888" y2="4176" x1="4176" />
            <wire x2="4208" y1="4176" y2="4176" x1="4176" />
            <wire x2="4208" y1="4176" y2="4464" x1="4208" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="4560" y1="4464" y2="4464" x1="4272" />
            <wire x2="4560" y1="3920" y2="4464" x1="4560" />
        </branch>
        <branch name="C2">
            <wire x2="4208" y1="4720" y2="4752" x1="4208" />
        </branch>
        <iomarker fontsize="28" x="4208" y="4752" name="C2" orien="R90" />
        <branch name="Ci">
            <wire x2="4624" y1="3568" y2="3568" x1="3648" />
            <wire x2="5328" y1="3568" y2="3568" x1="4624" />
            <wire x2="5328" y1="3568" y2="3680" x1="5328" />
            <wire x2="4624" y1="3568" y2="3664" x1="4624" />
            <wire x2="5328" y1="2832" y2="3568" x1="5328" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="2832" y1="3792" y2="4096" x1="2832" />
            <wire x2="3088" y1="4096" y2="4096" x1="2832" />
            <wire x2="3088" y1="4096" y2="4400" x1="3088" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="3136" y1="3856" y2="4128" x1="3136" />
            <wire x2="3152" y1="4128" y2="4128" x1="3136" />
            <wire x2="3152" y1="4128" y2="4400" x1="3152" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="3552" y1="4400" y2="4400" x1="3216" />
            <wire x2="3552" y1="3824" y2="4400" x1="3552" />
        </branch>
        <branch name="C3">
            <wire x2="3120" y1="4656" y2="4688" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="3120" y="4688" name="C3" orien="R90" />
        <branch name="GP">
            <wire x2="2256" y1="3776" y2="4672" x1="2256" />
        </branch>
        <iomarker fontsize="28" x="2256" y="4672" name="GP" orien="R90" />
        <branch name="P3">
            <wire x2="1136" y1="3136" y2="3136" x1="864" />
            <wire x2="1136" y1="3136" y2="3552" x1="1136" />
            <wire x2="1152" y1="3136" y2="3136" x1="1136" />
            <wire x2="1520" y1="3136" y2="3136" x1="1152" />
            <wire x2="2160" y1="3136" y2="3136" x1="1520" />
            <wire x2="2160" y1="3136" y2="3520" x1="2160" />
            <wire x2="1520" y1="3136" y2="3520" x1="1520" />
            <wire x2="864" y1="3136" y2="3488" x1="864" />
            <wire x2="2160" y1="2992" y2="3136" x1="2160" />
        </branch>
        <branch name="XLXN_37">
            <wire x2="896" y1="3744" y2="4000" x1="896" />
            <wire x2="1088" y1="4000" y2="4000" x1="896" />
            <wire x2="1088" y1="4000" y2="4256" x1="1088" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="1152" y1="4032" y2="4256" x1="1152" />
            <wire x2="1200" y1="4032" y2="4032" x1="1152" />
            <wire x2="1200" y1="3808" y2="4032" x1="1200" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="1616" y1="4256" y2="4256" x1="1216" />
            <wire x2="1616" y1="3776" y2="4256" x1="1616" />
        </branch>
        <branch name="GG">
            <wire x2="1120" y1="4512" y2="4544" x1="1120" />
        </branch>
        <iomarker fontsize="28" x="1120" y="4544" name="GG" orien="R90" />
    </sheet>
</drawing>