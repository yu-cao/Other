`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:54:20 12/11/2017 
// Design Name: 
// Module Name:    counter_4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module counter_4bit(input clk, wire Qa, wire Qb, wire Qc, wire Qd, wire Rc);

FD FFDA(.C(clk),.D(Da),.Q(Qa)),
	FFDB(.C(clk),.D(Db),.Q(Qb)),
	FFDC(.C(clk),.D(Dc),.Q(Qc)),
	FFDD(.C(clk),.D(Dd),.Q(Qd));
defparam	FFDA.INIT = 1'b0;// define initial value of the D type Flip-Flop
defparam	FFDB.INIT = 1'b0;
defparam	FFDC.INIT = 1'b0;
defparam	FFDD.INIT = 1'b0;
INV	GQa (.I(Qa), .O(nQa)),
GQb(.I(Qb), .O(nQb)),
GQc(.I(Qc), .O(nQc)),
GQd(.I(Qd), .O(nQd));
assign Da = nQa;//
XNOR2	ODb(.I0(Qa), .I1(nQb), .O(Db)),
ODc(.I0(Nor_nQa_nQb), .I1(nQc), .O(Dc)),
ODd(.I0(Nor_nQa_nQb_nQc), .I1(nQd), .O(Dd));
NOR4	ORc(.I0(nQa), .I1(nQb), .I2(nQc), .I3(nQd), .O(Rc));
NOR2	G1 (.I0(nQa), .I1(nQb), .O(Nor_nQa_nQb));
NOR3	G2(.I0(nQa), .I1(nQb), .I2(nQc), .O(Nor_nQa_nQb_nQc));
endmodule

