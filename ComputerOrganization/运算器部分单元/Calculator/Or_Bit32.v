`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:55:08 11/27/2017 
// Design Name: 
// Module Name:    Or_Bit32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Or_Bit32(input [31:0]res,
                 output zero
    );
     assign zero=(res==0)?1:0;

endmodule
