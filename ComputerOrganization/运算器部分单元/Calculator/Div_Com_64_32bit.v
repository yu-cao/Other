`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:49:10 04/07/2018 
// Design Name: 
// Module Name:    Div_Com_32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Div_Com_64_32bit(
    input [63:0]A,
	 input [31:0]B,
	 input clk,
	 output reg[31:0]Quotient,
	 output reg[31:0]Remainder
    );
	 
	 reg [63:0]tmpRemainder,tmpDivisor;
	 reg [31:0]tmpQuotient;
	 reg [1:0]state;
	 reg [5:0]count;
	 reg [1:0]signal;
	 
	 parameter Start = 2'b0, Process1 = 2'b01, Process2 = 2'b10, End = 2'b11;

    always @(posedge clk) begin
        case (state)
            Start: begin
                count <= 6'b0;
					 signal[0] <= A[63];
					 signal[1] <= B[31];
                tmpQuotient <= 32'b0;
                state <= Process1;
            end
				Process1:begin        
				    if(signal[0])begin
					     tmpRemainder <= ~A + 1;
						  state <= End;
					 end
					 else begin
					     tmpRemainder <= A;
					 end
					 if(signal[1])begin
					     tmpDivisor <= {(~B + 1),32'b0};
					 end
					 else begin
					     tmpDivisor <= {B,32'b0};
					 end
					 state <= Process2;
			   end
            Process2: begin
                if(count == 6'b100001)
                    state <= End;
                else begin
						  if(tmpRemainder >= tmpDivisor)begin
						      tmpRemainder <= tmpRemainder - tmpDivisor;
						      tmpQuotient <= {tmpQuotient[30:0],1'b1};
						  end
						  else begin
                        tmpQuotient <= {tmpQuotient[30:0],1'b0};
                    end
                    tmpDivisor <= tmpDivisor >> 1;	
                    count <= count + 1;						  
                end
            end
            End: begin
                if(signal[0])begin
					     Remainder <= ~(tmpRemainder[31:0]) + 1;
					 end
					 else begin
					     Remainder <= tmpRemainder[31:0];
					 end
					 if(signal[0] != signal[1])begin
					     Quotient <= ~tmpQuotient + 1;
					 end
					 else begin
					     Quotient <= tmpQuotient;
					 end
                state <= Start;
            end
			   default:begin
			       state <= Start;
			   end
        endcase
    end			
	     
endmodule 