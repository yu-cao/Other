`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:22:37 04/07/2018 
// Design Name: 
// Module Name:    Div_Ori_32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Div_Ori_64_32bit(
    input [63:0]A,
	 input [31:0]B,
	 input clk,
	 output reg[31:0]Quotient,
	 output reg[31:0]Remainder
    );
	 
	 reg [63:0]tmpRemainder,tmpDivisor;
	 reg [31:0]tmpQuotient;
	 reg [1:0]state;
	 reg [5:0]count;
	 
	 parameter Start = 2'b0, Process = 2'b10, End = 2'b11;

    always @(posedge clk) begin
        case (state)
            Start: begin
                count <= 6'b0;
                tmpDivisor <= {B,32'b0};
                tmpRemainder <= A;
                tmpQuotient <= 32'b0;
                state <= Process;
            end
            Process: begin
                if(count == 6'b100001)
                    state <= End;
                else begin
						  if(tmpRemainder >= tmpDivisor)begin
						      tmpRemainder <= tmpRemainder - tmpDivisor;
						      tmpQuotient <= {tmpQuotient[30:0],1'b1};
						  end
						  else begin
                        tmpQuotient <= {tmpQuotient[30:0],1'b0};
                    end
                    tmpDivisor <= tmpDivisor >> 1;	
                    count <= count + 1;						  
                end
            end
            End: begin
                Remainder <= tmpRemainder[31:0];
					 Quotient <= tmpQuotient;
                state <= Start;
            end
			   default:begin
			       state <= Start;
			   end
        endcase
    end			
	     
endmodule 