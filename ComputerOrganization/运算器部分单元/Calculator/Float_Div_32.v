`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:06:29 04/09/2018 
// Design Name: 
// Module Name:    Float_Div_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Float_Div_32(input clk,input [31:0]A, input[31:0]B, output [31:0]res );
reg [25:0]div;
reg [47:0]r0;
reg [47:0]r1;
reg [22:0]r2;
reg [1:0]state=2'b0;
reg [4:0]count=5'b0;
reg [9:0]e,ea,eb;
reg [31:0]r;
assign res= r;
always @(posedge clk)begin
case(state)
2'b00:begin
  div<=0;
  r0<={1'b1,A[22:0],23'b0};
  r1<={1'b1,B[22:0],23'b0};
  r2<=0;
  ea <= {2'b00,A[30:23]};
  eb <= {2'b00,B[30:23]};
  e<=0;
  state<=2'b01;
  count<=0;
end
2'b01:begin
  if(count == 5'b11010) state<=2'b10;
  else begin
    if(r0>=r1)begin
     div[25-count] <= 1;
	  r0 <= r0 + (r1 ^ 48'hffffffffffff) + 48'b1;
	 end
	 else begin
	  div[25-count]<=0;
	  r0 <=r0;
	 end
    count<=count+5'b1;
	 r1 <= r1>>1;
	 state<=2'b01;
  end
end
2'b10:begin
  if(div[25]==0)begin //126 0001111110  -126 = 1110000001+1 = 1110000010   -127 = 111000001
	 if(ea+(eb^10'h3ff)+10'b1 + 10'b0001111111 > 10'h0ff && ea+(eb^10'h3ff)+10'b1+ 10'b0001111111<10'h200) e<=10'hff;
	 else if(ea+(eb^10'h3ff)+10'b1 + 10'b0001111111>= 10'h200 || ea+(eb^10'h3ff)+10'b1+ 10'b0001111111==10'b0) e<=10'b0;
	 else begin e<= ea+(eb^10'h3ff)+ 10'b0001111111;end
	 
	 if(div[0]==0) r2<=div[23:1];
	 else if(div[0]==1'b1) r2<=div[23:1]+1'b1;
	 
  end
  else begin
    if(ea+(eb^10'h3ff)+10'b1 + 10'b0001111111>= 10'h0ff && ea+(eb^10'h3ff)+10'b1+ 10'b0001111111<10'h200) e<=10'hff;
	 else if(ea+(eb^10'h3ff)+10'b1+ 10'b0001111111 >= 10'h200) e<=10'b0;
	 else begin e<= ea+(eb^10'h3ff)+23'b1+ 10'b0001111111;end
	 
	 if(div[1]==0) r2<=div[24:2];
	 else if(div[1]==1'b1) r2<=div[24:2]+23'b1;
  end
  state<=2'b11;
end
2'b11:begin
  r <= {{A[31]^B[31]},e[7:0],r2};
  state<=2'b00;
end
	
	 
endcase
end

endmodule
