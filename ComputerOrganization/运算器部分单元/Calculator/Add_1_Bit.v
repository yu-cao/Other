`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:01:40 03/22/2018 
// Design Name: 
// Module Name:    Add_1_Bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Add_1_Bit(input A0,input B0,input C0,output S,output C1 );
	assign S = A0^B0^C0;
	assign C1 = (A0&B0)|(A0&C0)|(B0&C0); 

endmodule
