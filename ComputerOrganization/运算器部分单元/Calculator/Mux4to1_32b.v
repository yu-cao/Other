`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:24:26 04/07/2018 
// Design Name: 
// Module Name:    Mux4to1_32b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mux4to1_32b(
    input [31:0]I0,
	 input [31:0]I1,
    input [31:0]I2,
	 input [31:0]I3,
	 input [1:0]s,
	 output [31:0]o
    );
    
	 Mux4to14b m0(.I0(I0[3:0]),.I1(I1[3:0]),.I2(I2[3:0]),.I3(I3[3:0]),.s(s),.o(o[3:0]));
	 
	 Mux4to14b m1(.I0(I0[7:4]),.I1(I1[7:4]),.I2(I2[7:4]),.I3(I3[7:4]),.s(s),.o(o[7:4]));
	 
	 Mux4to14b m2(.I0(I0[11:8]),.I1(I1[11:8]),.I2(I2[11:8]),.I3(I3[11:8]),.s(s),.o(o[11:8]));
	 
	 Mux4to14b m3(.I0(I0[15:12]),.I1(I1[15:12]),.I2(I2[15:12]),.I3(I3[15:12]),.s(s),.o(o[15:12]));
	 
	 Mux4to14b m4(.I0(I0[19:16]),.I1(I1[19:16]),.I2(I2[19:16]),.I3(I3[19:16]),.s(s),.o(o[19:16]));
	 
	 Mux4to14b m5(.I0(I0[23:20]),.I1(I1[23:20]),.I2(I2[23:20]),.I3(I3[23:20]),.s(s),.o(o[23:20]));
	 
	 Mux4to14b m6(.I0(I0[27:24]),.I1(I1[27:24]),.I2(I2[27:24]),.I3(I3[27:24]),.s(s),.o(o[27:24]));
	 
	 Mux4to14b m7(.I0(I0[31:28]),.I1(I1[31:28]),.I2(I2[31:28]),.I3(I3[31:28]),.s(s),.o(o[31:28]));
 
endmodule

