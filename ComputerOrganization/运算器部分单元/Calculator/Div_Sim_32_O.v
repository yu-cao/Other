`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   08:22:20 04/16/2018
// Design Name:   Div_Ori_32bit
// Module Name:   D:/2017-2018 Spr-Sum/Computer Organization/ILU/Div_Sim_32_O.v
// Project Name:  ILU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Div_Ori_32bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Div_Sim_32_O;

	// Inputs
	reg High;
	reg [31:0] Low;
	reg [31:0] B;
	reg clk;

	// Outputs
	wire [31:0] Quotient;
	wire [31:0] Remainder;

	// Instantiate the Unit Under Test (UUT)
	Div_Ori_32bit uut (
		.High(High), 
		.Low(Low), 
		.B(B), 
		.clk(clk), 
		.Quotient(Quotient), 
		.Remainder(Remainder)
	);

	initial begin
		// Initialize Inputs
		High = 0;
		Low = 0;
		B = 0;
		clk = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		fork 
		forever #5 clk = ~clk;
		begin
		Low = 32'h00000000;
		B = 32'h00000100;
		#5;
		High = 1;
		#5;
		Low = 32'h00001000;
		#5;
		High = 0;
		#1000;
		Low = 32'h00001000;
		B = 32'h00000100;
		#5;
		High = 1;
		#5;
		Low = 32'h00000001;
		#5;
		High = 0;
		#500;
		end
		join
        
		// Add stimulus here

	end
      
endmodule

