`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:11:50 03/15/2018 
// Design Name: 
// Module Name:    Data_B_Trans 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Data_B_Trans(input [31:0]b, input SW, output [31:0]o   );
 assign o = b ^ {32{SW}};

endmodule
