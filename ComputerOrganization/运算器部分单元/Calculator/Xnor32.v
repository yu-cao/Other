`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:52:45 11/27/2017 
// Design Name: 
// Module Name:    Xnor32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Xnor32(input [31:0] A,
             input [31:0] B,
				 output [31:0] res
              );
				  wire [31:0]C;
    assign C=A^B;
	 assign res=C^32'hFFFFFFFF;

endmodule
