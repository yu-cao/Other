<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="ai(3:0)" />
        <signal name="bi(3:0)" />
        <signal name="ai(3)" />
        <signal name="bi(3)" />
        <signal name="ai(2)" />
        <signal name="bi(2)" />
        <signal name="ai(1)" />
        <signal name="bi(1)" />
        <signal name="ai(0)" />
        <signal name="bi(0)" />
        <signal name="C0" />
        <signal name="XLXN_12" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="S(3)" />
        <signal name="S(2)" />
        <signal name="S(1)" />
        <signal name="S(0)" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="XLXN_29" />
        <signal name="GP" />
        <signal name="GG" />
        <signal name="S(3:0)" />
        <port polarity="Input" name="ai(3:0)" />
        <port polarity="Input" name="bi(3:0)" />
        <port polarity="Input" name="C0" />
        <port polarity="Output" name="GP" />
        <port polarity="Output" name="GG" />
        <port polarity="Output" name="S(3:0)" />
        <blockdef name="ADC_1">
            <timestamp>2017-11-26T18:51:58</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-128" y2="-128" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="CLA">
            <timestamp>2017-11-26T19:15:1</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-400" y2="-400" x1="320" />
            <line x2="384" y1="-320" y2="-320" x1="320" />
            <line x2="384" y1="-240" y2="-240" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-80" y2="-80" x1="320" />
            <rect width="256" x="64" y="-512" height="576" />
        </blockdef>
        <block symbolname="ADC_1" name="XLXI_1">
            <blockpin signalname="XLXN_27" name="ci" />
            <blockpin signalname="ai(3)" name="ai" />
            <blockpin signalname="bi(3)" name="bi" />
            <blockpin signalname="XLXN_12" name="Pi" />
            <blockpin signalname="XLXN_14" name="Gi" />
            <blockpin signalname="S(3)" name="Si" />
            <blockpin name="CO" />
        </block>
        <block symbolname="ADC_1" name="XLXI_2">
            <blockpin signalname="XLXN_28" name="ci" />
            <blockpin signalname="ai(2)" name="ai" />
            <blockpin signalname="bi(2)" name="bi" />
            <blockpin signalname="XLXN_15" name="Pi" />
            <blockpin signalname="XLXN_16" name="Gi" />
            <blockpin signalname="S(2)" name="Si" />
            <blockpin name="CO" />
        </block>
        <block symbolname="ADC_1" name="XLXI_3">
            <blockpin signalname="XLXN_29" name="ci" />
            <blockpin signalname="ai(1)" name="ai" />
            <blockpin signalname="bi(1)" name="bi" />
            <blockpin signalname="XLXN_17" name="Pi" />
            <blockpin signalname="XLXN_18" name="Gi" />
            <blockpin signalname="S(1)" name="Si" />
            <blockpin name="CO" />
        </block>
        <block symbolname="ADC_1" name="XLXI_4">
            <blockpin signalname="C0" name="ci" />
            <blockpin signalname="ai(0)" name="ai" />
            <blockpin signalname="bi(0)" name="bi" />
            <blockpin signalname="XLXN_20" name="Pi" />
            <blockpin signalname="XLXN_21" name="Gi" />
            <blockpin signalname="S(0)" name="Si" />
            <blockpin name="CO" />
        </block>
        <block symbolname="CLA" name="XLXI_5">
            <blockpin signalname="XLXN_14" name="G3" />
            <blockpin signalname="XLXN_12" name="P3" />
            <blockpin signalname="XLXN_16" name="G2" />
            <blockpin signalname="XLXN_15" name="P2" />
            <blockpin signalname="XLXN_18" name="G1" />
            <blockpin signalname="XLXN_17" name="P1" />
            <blockpin signalname="XLXN_21" name="G0" />
            <blockpin signalname="XLXN_20" name="P0" />
            <blockpin signalname="C0" name="Ci" />
            <blockpin signalname="XLXN_29" name="C1" />
            <blockpin signalname="XLXN_28" name="C2" />
            <blockpin signalname="XLXN_27" name="C3" />
            <blockpin signalname="GP" name="GP" />
            <blockpin signalname="GG" name="GG" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="928" y="704" name="XLXI_1" orien="R0">
        </instance>
        <instance x="928" y="1120" name="XLXI_2" orien="R0">
        </instance>
        <instance x="944" y="1504" name="XLXI_3" orien="R0">
        </instance>
        <instance x="960" y="1872" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1904" y="1216" name="XLXI_5" orien="R0">
        </instance>
        <branch name="ai(3:0)">
            <wire x2="672" y1="160" y2="160" x1="368" />
        </branch>
        <branch name="bi(3:0)">
            <wire x2="672" y1="256" y2="256" x1="368" />
        </branch>
        <iomarker fontsize="28" x="368" y="160" name="ai(3:0)" orien="R180" />
        <iomarker fontsize="28" x="368" y="256" name="bi(3:0)" orien="R180" />
        <branch name="ai(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="576" type="branch" />
            <wire x2="928" y1="576" y2="576" x1="624" />
        </branch>
        <branch name="bi(3)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="624" y="672" type="branch" />
            <wire x2="928" y1="672" y2="672" x1="624" />
        </branch>
        <branch name="ai(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="640" y="992" type="branch" />
            <wire x2="928" y1="992" y2="992" x1="640" />
        </branch>
        <branch name="bi(2)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="640" y="1088" type="branch" />
            <wire x2="928" y1="1088" y2="1088" x1="640" />
        </branch>
        <branch name="ai(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="656" y="1376" type="branch" />
            <wire x2="944" y1="1376" y2="1376" x1="656" />
        </branch>
        <branch name="bi(1)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="656" y="1472" type="branch" />
            <wire x2="944" y1="1472" y2="1472" x1="656" />
        </branch>
        <branch name="ai(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="672" y="1744" type="branch" />
            <wire x2="960" y1="1744" y2="1744" x1="672" />
        </branch>
        <branch name="bi(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="672" y="1840" type="branch" />
            <wire x2="960" y1="1840" y2="1840" x1="672" />
        </branch>
        <branch name="C0">
            <wire x2="816" y1="1648" y2="1648" x1="624" />
            <wire x2="960" y1="1648" y2="1648" x1="816" />
            <wire x2="816" y1="1648" y2="2128" x1="816" />
            <wire x2="1904" y1="2128" y2="2128" x1="816" />
            <wire x2="1904" y1="1248" y2="2128" x1="1904" />
        </branch>
        <iomarker fontsize="28" x="624" y="1648" name="C0" orien="R180" />
        <branch name="XLXN_12">
            <wire x2="1600" y1="480" y2="480" x1="1312" />
            <wire x2="1600" y1="480" y2="800" x1="1600" />
            <wire x2="1904" y1="800" y2="800" x1="1600" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="1664" y1="544" y2="544" x1="1312" />
            <wire x2="1664" y1="544" y2="736" x1="1664" />
            <wire x2="1904" y1="736" y2="736" x1="1664" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1600" y1="896" y2="896" x1="1312" />
            <wire x2="1600" y1="896" y2="928" x1="1600" />
            <wire x2="1904" y1="928" y2="928" x1="1600" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1584" y1="960" y2="960" x1="1312" />
            <wire x2="1584" y1="864" y2="960" x1="1584" />
            <wire x2="1904" y1="864" y2="864" x1="1584" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="1616" y1="1280" y2="1280" x1="1328" />
            <wire x2="1616" y1="992" y2="1280" x1="1616" />
            <wire x2="1904" y1="992" y2="992" x1="1616" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1792" y1="1344" y2="1344" x1="1328" />
            <wire x2="1824" y1="1344" y2="1344" x1="1792" />
            <wire x2="1824" y1="576" y2="1344" x1="1824" />
            <wire x2="2352" y1="576" y2="576" x1="1824" />
            <wire x2="2352" y1="576" y2="736" x1="2352" />
            <wire x2="2352" y1="736" y2="736" x1="2288" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="1600" y1="1648" y2="1648" x1="1344" />
            <wire x2="1600" y1="1120" y2="1648" x1="1600" />
            <wire x2="1904" y1="1120" y2="1120" x1="1600" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="1584" y1="1712" y2="1712" x1="1344" />
            <wire x2="1584" y1="1056" y2="1712" x1="1584" />
            <wire x2="1904" y1="1056" y2="1056" x1="1584" />
        </branch>
        <branch name="S(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2120" y="1536" type="branch" />
            <wire x2="1328" y1="608" y2="608" x1="1312" />
            <wire x2="1520" y1="608" y2="608" x1="1328" />
            <wire x2="1520" y1="608" y2="1536" x1="1520" />
            <wire x2="2096" y1="1536" y2="1536" x1="1520" />
            <wire x2="2120" y1="1536" y2="1536" x1="2096" />
            <wire x2="2144" y1="1536" y2="1536" x1="2120" />
        </branch>
        <branch name="S(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2120" y="1584" type="branch" />
            <wire x2="1472" y1="1024" y2="1024" x1="1312" />
            <wire x2="1472" y1="1024" y2="1584" x1="1472" />
            <wire x2="2096" y1="1584" y2="1584" x1="1472" />
            <wire x2="2120" y1="1584" y2="1584" x1="2096" />
            <wire x2="2144" y1="1584" y2="1584" x1="2120" />
        </branch>
        <branch name="S(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2120" y="1632" type="branch" />
            <wire x2="1440" y1="1408" y2="1408" x1="1328" />
            <wire x2="1440" y1="1408" y2="1632" x1="1440" />
            <wire x2="2080" y1="1632" y2="1632" x1="1440" />
            <wire x2="2096" y1="1632" y2="1632" x1="2080" />
            <wire x2="2120" y1="1632" y2="1632" x1="2096" />
            <wire x2="2144" y1="1632" y2="1632" x1="2120" />
        </branch>
        <branch name="S(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2128" y="1680" type="branch" />
            <wire x2="1728" y1="1776" y2="1776" x1="1344" />
            <wire x2="1728" y1="1680" y2="1776" x1="1728" />
            <wire x2="2112" y1="1680" y2="1680" x1="1728" />
            <wire x2="2128" y1="1680" y2="1680" x1="2112" />
            <wire x2="2144" y1="1680" y2="1680" x1="2128" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="928" y1="208" y2="208" x1="912" />
            <wire x2="2400" y1="208" y2="208" x1="928" />
            <wire x2="2400" y1="208" y2="976" x1="2400" />
            <wire x2="912" y1="208" y2="480" x1="912" />
            <wire x2="928" y1="480" y2="480" x1="912" />
            <wire x2="2400" y1="976" y2="976" x1="2288" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="848" y1="304" y2="896" x1="848" />
            <wire x2="928" y1="896" y2="896" x1="848" />
            <wire x2="2480" y1="304" y2="304" x1="848" />
            <wire x2="2480" y1="304" y2="896" x1="2480" />
            <wire x2="2480" y1="896" y2="896" x1="2288" />
        </branch>
        <branch name="XLXN_29">
            <wire x2="944" y1="1280" y2="1280" x1="880" />
            <wire x2="880" y1="1280" y2="1568" x1="880" />
            <wire x2="944" y1="1568" y2="1568" x1="880" />
            <wire x2="944" y1="1568" y2="2224" x1="944" />
            <wire x2="2560" y1="2224" y2="2224" x1="944" />
            <wire x2="2560" y1="816" y2="816" x1="2288" />
            <wire x2="2560" y1="816" y2="2224" x1="2560" />
        </branch>
        <branch name="GP">
            <wire x2="2320" y1="1056" y2="1056" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="1056" name="GP" orien="R0" />
        <branch name="GG">
            <wire x2="2320" y1="1136" y2="1136" x1="2288" />
        </branch>
        <iomarker fontsize="28" x="2320" y="1136" name="GG" orien="R0" />
        <branch name="S(3:0)">
            <wire x2="2240" y1="1520" y2="1536" x1="2240" />
            <wire x2="2240" y1="1536" y2="1584" x1="2240" />
            <wire x2="2240" y1="1584" y2="1600" x1="2240" />
            <wire x2="2288" y1="1600" y2="1600" x1="2240" />
            <wire x2="2240" y1="1600" y2="1632" x1="2240" />
            <wire x2="2240" y1="1632" y2="1680" x1="2240" />
        </branch>
        <iomarker fontsize="28" x="2288" y="1600" name="S(3:0)" orien="R0" />
        <bustap x2="2144" y1="1536" y2="1536" x1="2240" />
        <bustap x2="2144" y1="1584" y2="1584" x1="2240" />
        <bustap x2="2144" y1="1632" y2="1632" x1="2240" />
        <bustap x2="2144" y1="1680" y2="1680" x1="2240" />
    </sheet>
</drawing>