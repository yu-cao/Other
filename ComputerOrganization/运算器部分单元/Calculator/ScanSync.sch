<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Scan(2:0)" />
        <signal name="Hexs(31:0)" />
        <signal name="point(7:0)" />
        <signal name="LES(7:0)" />
        <signal name="Hexs(3:0),G0,G0,G0,G0" />
        <signal name="Hexs(7:4),G0,G0,G0,G0" />
        <signal name="Hexs(11:8),G0,G0,G0,G0" />
        <signal name="Hexs(23:20),G0,G0,G0,G0" />
        <signal name="Hexs(27:24),G0,G0,G0,G0" />
        <signal name="Hexs(31:28),G0,G0,G0,G0" />
        <signal name="Hexs(15:12),G0,G0,G0,G0" />
        <signal name="Hexs(19:16),G0,G0,G0,G0" />
        <signal name="LES(0),point(0),G0,G0,V5,V5,V5,G0" />
        <signal name="LES(1),point(1),G0,G0,V5,V5,G0,V5" />
        <signal name="LES(2),point(2),G0,G0,V5,G0,V5,V5" />
        <signal name="LES(4),point(4),G0,G0,V5,V5,V5,G0" />
        <signal name="LES(5),point(5),G0,G0,V5,V5,G0,V5" />
        <signal name="LES(6),point(6),G0,G0,V5,G0,V5,V5" />
        <signal name="LES(7),point(7),G0,G0,G0,V5,V5,V5" />
        <signal name="LES(3),point(3),G0,G0,G0,V5,V5,V5" />
        <signal name="Hex(7:0)" />
        <signal name="Hex(7)" />
        <signal name="Hex(6)" />
        <signal name="Hex(5)" />
        <signal name="Hex(4)" />
        <signal name="Hexo(3:0)" />
        <signal name="Hexo(3)" />
        <signal name="Hexo(2)" />
        <signal name="Hexo(1)" />
        <signal name="Hexo(0)" />
        <signal name="V5" />
        <signal name="G0" />
        <signal name="p" />
        <signal name="COM(7:0)" />
        <signal name="COM(3)" />
        <signal name="COM(2)" />
        <signal name="COM(1)" />
        <signal name="COM(0)" />
        <signal name="COM(7)" />
        <signal name="COM(6)" />
        <signal name="AN(3:0)" />
        <signal name="AN(3)" />
        <signal name="AN(2)" />
        <signal name="AN(1)" />
        <signal name="AN(0)" />
        <signal name="LE" />
        <port polarity="Input" name="Scan(2:0)" />
        <port polarity="Input" name="Hexs(31:0)" />
        <port polarity="Input" name="point(7:0)" />
        <port polarity="Input" name="LES(7:0)" />
        <port polarity="Output" name="Hexo(3:0)" />
        <port polarity="Output" name="p" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Output" name="LE" />
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="buf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-64" y2="0" x1="64" />
        </blockdef>
        <blockdef name="MUX8T1_8">
            <timestamp>2017-11-22T7:6:30</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-556" height="24" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
        </blockdef>
        <block symbolname="MUX8T1_8" name="XLXI_9">
            <blockpin signalname="Scan(2:0)" name="s(2:0)" />
            <blockpin signalname="Hexs(3:0),G0,G0,G0,G0" name="I0(7:0)" />
            <blockpin signalname="Hexs(7:4),G0,G0,G0,G0" name="I1(7:0)" />
            <blockpin signalname="Hexs(11:8),G0,G0,G0,G0" name="I2(7:0)" />
            <blockpin signalname="Hexs(19:16),G0,G0,G0,G0" name="I4(7:0)" />
            <blockpin signalname="Hexs(23:20),G0,G0,G0,G0" name="I5(7:0)" />
            <blockpin signalname="Hexs(27:24),G0,G0,G0,G0" name="I6(7:0)" />
            <blockpin signalname="Hexs(31:28),G0,G0,G0,G0" name="I7(7:0)" />
            <blockpin signalname="Hexs(15:12),G0,G0,G0,G0" name="I3(7:0)" />
            <blockpin signalname="Hex(7:0)" name="O(7:0)" />
        </block>
        <block symbolname="MUX8T1_8" name="XLXI_10">
            <blockpin signalname="Scan(2:0)" name="s(2:0)" />
            <blockpin signalname="LES(0),point(0),G0,G0,V5,V5,V5,G0" name="I0(7:0)" />
            <blockpin signalname="LES(1),point(1),G0,G0,V5,V5,G0,V5" name="I1(7:0)" />
            <blockpin signalname="LES(2),point(2),G0,G0,V5,G0,V5,V5" name="I2(7:0)" />
            <blockpin signalname="LES(4),point(4),G0,G0,V5,V5,V5,G0" name="I4(7:0)" />
            <blockpin signalname="LES(5),point(5),G0,G0,V5,V5,G0,V5" name="I5(7:0)" />
            <blockpin signalname="LES(6),point(6),G0,G0,V5,G0,V5,V5" name="I6(7:0)" />
            <blockpin signalname="LES(7),point(7),G0,G0,G0,V5,V5,V5" name="I7(7:0)" />
            <blockpin signalname="LES(3),point(3),G0,G0,G0,V5,V5,V5" name="I3(7:0)" />
            <blockpin signalname="COM(7:0)" name="O(7:0)" />
        </block>
        <block symbolname="buf" name="XLXI_11">
            <blockpin signalname="Hex(7)" name="I" />
            <blockpin signalname="Hexo(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_12">
            <blockpin signalname="Hex(6)" name="I" />
            <blockpin signalname="Hexo(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_13">
            <blockpin signalname="Hex(5)" name="I" />
            <blockpin signalname="Hexo(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_14">
            <blockpin signalname="Hex(4)" name="I" />
            <blockpin signalname="Hexo(0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_15">
            <blockpin signalname="COM(3)" name="I" />
            <blockpin signalname="AN(3)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_16">
            <blockpin signalname="COM(2)" name="I" />
            <blockpin signalname="AN(2)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_17">
            <blockpin signalname="COM(1)" name="I" />
            <blockpin signalname="AN(1)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_18">
            <blockpin signalname="COM(0)" name="I" />
            <blockpin signalname="AN(0)" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_19">
            <blockpin signalname="COM(7)" name="I" />
            <blockpin signalname="LE" name="O" />
        </block>
        <block symbolname="buf" name="XLXI_20">
            <blockpin signalname="COM(6)" name="I" />
            <blockpin signalname="p" name="O" />
        </block>
        <block symbolname="vcc" name="XLXI_4">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_5">
            <blockpin signalname="G0" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="848" y="1216" name="XLXI_9" orien="R0">
        </instance>
        <instance x="864" y="2176" name="XLXI_10" orien="R0">
        </instance>
        <branch name="Scan(2:0)">
            <wire x2="624" y1="400" y2="400" x1="368" />
            <wire x2="624" y1="400" y2="672" x1="624" />
            <wire x2="848" y1="672" y2="672" x1="624" />
            <wire x2="624" y1="672" y2="1632" x1="624" />
            <wire x2="864" y1="1632" y2="1632" x1="624" />
        </branch>
        <iomarker fontsize="28" x="368" y="400" name="Scan(2:0)" orien="R180" />
        <branch name="point(7:0)">
            <wire x2="448" y1="560" y2="560" x1="320" />
        </branch>
        <branch name="LES(7:0)">
            <wire x2="464" y1="624" y2="624" x1="320" />
        </branch>
        <iomarker fontsize="28" x="320" y="560" name="point(7:0)" orien="R180" />
        <iomarker fontsize="28" x="320" y="624" name="LES(7:0)" orien="R180" />
        <iomarker fontsize="28" x="320" y="496" name="Hexs(31:0)" orien="R180" />
        <branch name="Hexs(31:0)">
            <wire x2="448" y1="496" y2="496" x1="320" />
        </branch>
        <branch name="Hexs(3:0),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="736" type="branch" />
            <wire x2="848" y1="736" y2="736" x1="448" />
        </branch>
        <branch name="Hexs(7:4),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="800" type="branch" />
            <wire x2="848" y1="800" y2="800" x1="448" />
        </branch>
        <branch name="Hexs(11:8),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="864" type="branch" />
            <wire x2="848" y1="864" y2="864" x1="448" />
        </branch>
        <branch name="Hexs(23:20),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="992" type="branch" />
            <wire x2="848" y1="992" y2="992" x1="448" />
        </branch>
        <branch name="Hexs(27:24),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="1056" type="branch" />
            <wire x2="848" y1="1056" y2="1056" x1="448" />
        </branch>
        <branch name="Hexs(31:28),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="1120" type="branch" />
            <wire x2="848" y1="1120" y2="1120" x1="448" />
        </branch>
        <branch name="Hexs(15:12),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="464" y="1184" type="branch" />
            <wire x2="848" y1="1184" y2="1184" x1="464" />
        </branch>
        <branch name="Hexs(19:16),G0,G0,G0,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="448" y="928" type="branch" />
            <wire x2="848" y1="928" y2="928" x1="448" />
        </branch>
        <branch name="LES(0),point(0),G0,G0,V5,V5,V5,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="496" y="1696" type="branch" />
            <wire x2="864" y1="1696" y2="1696" x1="496" />
        </branch>
        <branch name="LES(1),point(1),G0,G0,V5,V5,G0,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="480" y="1760" type="branch" />
            <wire x2="864" y1="1760" y2="1760" x1="480" />
        </branch>
        <branch name="LES(2),point(2),G0,G0,V5,G0,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="512" y="1824" type="branch" />
            <wire x2="864" y1="1824" y2="1824" x1="512" />
        </branch>
        <branch name="LES(4),point(4),G0,G0,V5,V5,V5,G0">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="528" y="1888" type="branch" />
            <wire x2="864" y1="1888" y2="1888" x1="528" />
        </branch>
        <branch name="LES(5),point(5),G0,G0,V5,V5,G0,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="512" y="1952" type="branch" />
            <wire x2="864" y1="1952" y2="1952" x1="512" />
        </branch>
        <branch name="LES(6),point(6),G0,G0,V5,G0,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="496" y="2016" type="branch" />
            <wire x2="864" y1="2016" y2="2016" x1="496" />
        </branch>
        <branch name="LES(7),point(7),G0,G0,G0,V5,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="528" y="2080" type="branch" />
            <wire x2="864" y1="2080" y2="2080" x1="528" />
        </branch>
        <branch name="LES(3),point(3),G0,G0,G0,V5,V5,V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="528" y="2144" type="branch" />
            <wire x2="864" y1="2144" y2="2144" x1="528" />
        </branch>
        <branch name="Hex(7:0)">
            <wire x2="1680" y1="672" y2="672" x1="1232" />
            <wire x2="1680" y1="672" y2="768" x1="1680" />
            <wire x2="1680" y1="768" y2="864" x1="1680" />
            <wire x2="1680" y1="864" y2="944" x1="1680" />
            <wire x2="1680" y1="432" y2="496" x1="1680" />
            <wire x2="1680" y1="496" y2="576" x1="1680" />
            <wire x2="1680" y1="576" y2="672" x1="1680" />
        </branch>
        <bustap x2="1776" y1="496" y2="496" x1="1680" />
        <bustap x2="1776" y1="576" y2="576" x1="1680" />
        <bustap x2="1776" y1="768" y2="768" x1="1680" />
        <bustap x2="1776" y1="864" y2="864" x1="1680" />
        <instance x="1968" y="528" name="XLXI_11" orien="R0" />
        <instance x="1968" y="608" name="XLXI_12" orien="R0" />
        <instance x="1968" y="800" name="XLXI_13" orien="R0" />
        <instance x="1968" y="896" name="XLXI_14" orien="R0" />
        <branch name="Hex(7)">
            <wire x2="1968" y1="496" y2="496" x1="1776" />
        </branch>
        <branch name="Hex(6)">
            <wire x2="1968" y1="576" y2="576" x1="1776" />
        </branch>
        <branch name="Hex(5)">
            <wire x2="1968" y1="768" y2="768" x1="1776" />
        </branch>
        <branch name="Hex(4)">
            <wire x2="1968" y1="864" y2="864" x1="1776" />
        </branch>
        <branch name="Hexo(3:0)">
            <wire x2="2528" y1="432" y2="496" x1="2528" />
            <wire x2="2528" y1="496" y2="576" x1="2528" />
            <wire x2="2528" y1="576" y2="672" x1="2528" />
            <wire x2="2688" y1="672" y2="672" x1="2528" />
            <wire x2="2528" y1="672" y2="768" x1="2528" />
            <wire x2="2528" y1="768" y2="864" x1="2528" />
            <wire x2="2528" y1="864" y2="960" x1="2528" />
        </branch>
        <iomarker fontsize="28" x="2688" y="672" name="Hexo(3:0)" orien="R0" />
        <bustap x2="2432" y1="496" y2="496" x1="2528" />
        <bustap x2="2432" y1="576" y2="576" x1="2528" />
        <bustap x2="2432" y1="768" y2="768" x1="2528" />
        <bustap x2="2432" y1="864" y2="864" x1="2528" />
        <branch name="Hexo(3)">
            <wire x2="2432" y1="496" y2="496" x1="2192" />
        </branch>
        <branch name="Hexo(2)">
            <wire x2="2432" y1="576" y2="576" x1="2192" />
        </branch>
        <branch name="Hexo(1)">
            <wire x2="2432" y1="768" y2="768" x1="2192" />
        </branch>
        <branch name="Hexo(0)">
            <wire x2="2432" y1="864" y2="864" x1="2192" />
        </branch>
        <branch name="V5">
            <wire x2="2640" y1="1168" y2="1280" x1="2640" />
            <wire x2="2880" y1="1280" y2="1280" x1="2640" />
        </branch>
        <instance x="2576" y="1168" name="XLXI_4" orien="R0" />
        <branch name="G0">
            <wire x2="3136" y1="1104" y2="1104" x1="2928" />
        </branch>
        <instance x="3072" y="1232" name="XLXI_5" orien="R0" />
        <branch name="COM(7:0)">
            <wire x2="1344" y1="1632" y2="1632" x1="1248" />
            <wire x2="1344" y1="1632" y2="1696" x1="1344" />
            <wire x2="1344" y1="1696" y2="1776" x1="1344" />
            <wire x2="1344" y1="1776" y2="1888" x1="1344" />
            <wire x2="1344" y1="1888" y2="2000" x1="1344" />
            <wire x2="1344" y1="2000" y2="2064" x1="1344" />
            <wire x2="1344" y1="1504" y2="1520" x1="1344" />
            <wire x2="1344" y1="1520" y2="1600" x1="1344" />
            <wire x2="1344" y1="1600" y2="1632" x1="1344" />
        </branch>
        <bustap x2="1440" y1="1520" y2="1520" x1="1344" />
        <bustap x2="1440" y1="1600" y2="1600" x1="1344" />
        <bustap x2="1440" y1="1696" y2="1696" x1="1344" />
        <bustap x2="1440" y1="1776" y2="1776" x1="1344" />
        <bustap x2="1440" y1="1888" y2="1888" x1="1344" />
        <bustap x2="1440" y1="2000" y2="2000" x1="1344" />
        <instance x="1600" y="1552" name="XLXI_15" orien="R0" />
        <instance x="1600" y="1632" name="XLXI_16" orien="R0" />
        <instance x="1600" y="1728" name="XLXI_17" orien="R0" />
        <instance x="1600" y="1808" name="XLXI_18" orien="R0" />
        <instance x="1600" y="1920" name="XLXI_19" orien="R0" />
        <instance x="1600" y="2032" name="XLXI_20" orien="R0" />
        <branch name="COM(3)">
            <wire x2="1600" y1="1520" y2="1520" x1="1440" />
        </branch>
        <branch name="COM(2)">
            <wire x2="1600" y1="1600" y2="1600" x1="1440" />
        </branch>
        <branch name="COM(1)">
            <wire x2="1600" y1="1696" y2="1696" x1="1440" />
        </branch>
        <branch name="COM(0)">
            <wire x2="1600" y1="1776" y2="1776" x1="1440" />
        </branch>
        <branch name="COM(7)">
            <wire x2="1600" y1="1888" y2="1888" x1="1440" />
        </branch>
        <branch name="COM(6)">
            <wire x2="1600" y1="2000" y2="2000" x1="1440" />
        </branch>
        <branch name="AN(3:0)">
            <wire x2="2176" y1="1472" y2="1520" x1="2176" />
            <wire x2="2176" y1="1520" y2="1552" x1="2176" />
            <wire x2="2368" y1="1552" y2="1552" x1="2176" />
            <wire x2="2176" y1="1552" y2="1600" x1="2176" />
            <wire x2="2176" y1="1600" y2="1696" x1="2176" />
            <wire x2="2176" y1="1696" y2="1776" x1="2176" />
            <wire x2="2176" y1="1776" y2="1808" x1="2176" />
        </branch>
        <iomarker fontsize="28" x="2368" y="1552" name="AN(3:0)" orien="R0" />
        <bustap x2="2080" y1="1520" y2="1520" x1="2176" />
        <bustap x2="2080" y1="1600" y2="1600" x1="2176" />
        <bustap x2="2080" y1="1696" y2="1696" x1="2176" />
        <bustap x2="2080" y1="1776" y2="1776" x1="2176" />
        <branch name="p">
            <wire x2="1840" y1="2000" y2="2000" x1="1824" />
            <wire x2="1952" y1="2000" y2="2000" x1="1840" />
        </branch>
        <iomarker fontsize="28" x="1952" y="2000" name="p" orien="R0" />
        <branch name="AN(3)">
            <wire x2="2080" y1="1520" y2="1520" x1="1824" />
        </branch>
        <branch name="AN(2)">
            <wire x2="2080" y1="1600" y2="1600" x1="1824" />
        </branch>
        <branch name="AN(1)">
            <wire x2="2080" y1="1696" y2="1696" x1="1824" />
        </branch>
        <branch name="AN(0)">
            <wire x2="2080" y1="1776" y2="1776" x1="1824" />
        </branch>
        <branch name="LE">
            <wire x2="1856" y1="1888" y2="1888" x1="1824" />
        </branch>
        <iomarker fontsize="28" x="1856" y="1888" name="LE" orien="R0" />
    </sheet>
</drawing>