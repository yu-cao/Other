`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    02:09:37 04/09/2018 
// Design Name: 
// Module Name:    Float_Add_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Float_Add_32(input clk,input sw, input[31:0]A,input[31:0]B,output [31:0]res);
  reg [3:0]state = 3'b000;
  reg [25:0]r0,r1;
  reg [25:0]r2;
  reg si = 0;
  reg [7:0]e,ea,eb;
  reg [31:0]r;
  assign res = r;
  always@(posedge clk)begin
  case(state)
  3'b000:begin
    r0<=A[31]==0?{3'b001,A[22:0]}:(({3'b001,A[22:0]}^26'h3ffffff)+26'h1);
	 r1<=(B[31])==0?{3'b001,B[22:0]}:(({3'b001,B[22:0]}^26'h3ffffff)+26'h1);
	 r2<=0;
	 ea<=A[30:23];
	 eb<=B[30:23];
	 e<=0;
	 state <=3'b001;
  end
  3'b001:begin
    if(ea==eb) state<=3'b010;
	 else begin
	  if(ea<eb)begin
	   ea<=ea+8'b1;
		r0<={A[31],r0[25:1]};
	  end
	  else if(ea>eb)begin
	   eb<=eb+8'b1;
		r1<={B[31],r1[25:1]};
	  end
	  state <= 3'b001;
	 end
  end
  3'b010:begin
    r2<=r0+r1;
	 e<=ea;
	 state<=3'b011;
  end
  3'b011:begin
    si<=r2[25];
	 if(r2[25]==1'b1)r2<=(r2^26'h3ffffff)+26'h1;
	 if(r2==26'b0) begin e<=0; state<=3'b101;end//8'he9 = 0111 0100 1 
    else state<=3'b100;
  end
  3'b100:begin
    if(r2[24:23]==2'b00 && e!=0)begin
		if(e==8'h00) e<=e;
		else   e<=e+8'hff;
	   r2<=r2<<1;
		state<=3'b100;
	 end
	 else if(r2[24]==1'b1)begin
	  if(e == 8'hff) e<=e;
	  else  e<=e+8'b1;
	  if(r2[0]==1) r2<=(r2+26'b1)>>1;
	  else r2<=r2>>1;
	  state<=3'b101;
	 end
	 else state<=3'b101;
	end
	3'b101:begin
	  r <={si,e,r2[22:0]};
	  state<=3'b000;
	end
  endcase
  
  end
  

endmodule
