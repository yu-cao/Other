<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="ci" />
        <signal name="ai" />
        <signal name="bi" />
        <signal name="Pi" />
        <signal name="XLXN_5" />
        <signal name="Gi" />
        <signal name="Si" />
        <signal name="CO" />
        <port polarity="Input" name="ci" />
        <port polarity="Input" name="ai" />
        <port polarity="Input" name="bi" />
        <port polarity="Output" name="Pi" />
        <port polarity="Output" name="Gi" />
        <port polarity="Output" name="Si" />
        <port polarity="Output" name="CO" />
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="xor2" name="XLXI_1">
            <blockpin signalname="bi" name="I0" />
            <blockpin signalname="ai" name="I1" />
            <blockpin signalname="Pi" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_2">
            <blockpin signalname="ci" name="I0" />
            <blockpin signalname="Pi" name="I1" />
            <blockpin signalname="Si" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="ai" name="I0" />
            <blockpin signalname="bi" name="I1" />
            <blockpin signalname="Gi" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="ci" name="I0" />
            <blockpin signalname="Pi" name="I1" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_5">
            <blockpin signalname="Gi" name="I0" />
            <blockpin signalname="XLXN_5" name="I1" />
            <blockpin signalname="CO" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="816" y="1136" name="XLXI_1" orien="R0" />
        <instance x="1424" y="1200" name="XLXI_2" orien="R0" />
        <instance x="816" y="1472" name="XLXI_3" orien="R0" />
        <instance x="1424" y="1504" name="XLXI_4" orien="R0" />
        <instance x="1872" y="1552" name="XLXI_5" orien="R0" />
        <branch name="ci">
            <wire x2="1392" y1="832" y2="832" x1="560" />
            <wire x2="1408" y1="832" y2="832" x1="1392" />
            <wire x2="1408" y1="832" y2="1136" x1="1408" />
            <wire x2="1424" y1="1136" y2="1136" x1="1408" />
            <wire x2="1408" y1="1136" y2="1440" x1="1408" />
            <wire x2="1424" y1="1440" y2="1440" x1="1408" />
        </branch>
        <iomarker fontsize="28" x="560" y="832" name="ci" orien="R180" />
        <branch name="ai">
            <wire x2="800" y1="1008" y2="1008" x1="592" />
            <wire x2="816" y1="1008" y2="1008" x1="800" />
            <wire x2="800" y1="1008" y2="1408" x1="800" />
            <wire x2="816" y1="1408" y2="1408" x1="800" />
        </branch>
        <branch name="bi">
            <wire x2="784" y1="1184" y2="1184" x1="608" />
            <wire x2="784" y1="1184" y2="1344" x1="784" />
            <wire x2="816" y1="1344" y2="1344" x1="784" />
            <wire x2="816" y1="1072" y2="1072" x1="784" />
            <wire x2="784" y1="1072" y2="1184" x1="784" />
        </branch>
        <iomarker fontsize="28" x="592" y="1008" name="ai" orien="R180" />
        <iomarker fontsize="28" x="608" y="1184" name="bi" orien="R180" />
        <branch name="Pi">
            <wire x2="1200" y1="1040" y2="1040" x1="1072" />
            <wire x2="1392" y1="1040" y2="1040" x1="1200" />
            <wire x2="1392" y1="1040" y2="1072" x1="1392" />
            <wire x2="1424" y1="1072" y2="1072" x1="1392" />
            <wire x2="1392" y1="1072" y2="1376" x1="1392" />
            <wire x2="1424" y1="1376" y2="1376" x1="1392" />
            <wire x2="1200" y1="1040" y2="1712" x1="1200" />
            <wire x2="2032" y1="1712" y2="1712" x1="1200" />
        </branch>
        <branch name="XLXN_5">
            <wire x2="1776" y1="1408" y2="1408" x1="1680" />
            <wire x2="1776" y1="1408" y2="1424" x1="1776" />
            <wire x2="1872" y1="1424" y2="1424" x1="1776" />
        </branch>
        <branch name="Gi">
            <wire x2="1376" y1="1376" y2="1376" x1="1072" />
            <wire x2="1376" y1="1376" y2="1488" x1="1376" />
            <wire x2="1792" y1="1488" y2="1488" x1="1376" />
            <wire x2="1872" y1="1488" y2="1488" x1="1792" />
            <wire x2="1792" y1="1488" y2="1648" x1="1792" />
            <wire x2="2032" y1="1648" y2="1648" x1="1792" />
        </branch>
        <branch name="Si">
            <wire x2="1712" y1="1104" y2="1104" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="1712" y="1104" name="Si" orien="R0" />
        <branch name="CO">
            <wire x2="2160" y1="1456" y2="1456" x1="2128" />
        </branch>
        <iomarker fontsize="28" x="2160" y="1456" name="CO" orien="R0" />
        <iomarker fontsize="28" x="2032" y="1648" name="Gi" orien="R0" />
        <iomarker fontsize="28" x="2032" y="1712" name="Pi" orien="R0" />
    </sheet>
</drawing>