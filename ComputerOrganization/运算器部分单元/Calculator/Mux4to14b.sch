<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(1:0)" />
        <signal name="s(0)" />
        <signal name="s(1)" />
        <signal name="XLXN_5" />
        <signal name="XLXN_8" />
        <signal name="I1(3:0)" />
        <signal name="I2(3:0)" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="I0(3:0)" />
        <signal name="I3(3:0)" />
        <signal name="I0(0)" />
        <signal name="I0(1)" />
        <signal name="I0(2)" />
        <signal name="I0(3)" />
        <signal name="I1(0)" />
        <signal name="I1(1)" />
        <signal name="I1(2)" />
        <signal name="I1(3)" />
        <signal name="I2(0)" />
        <signal name="I2(1)" />
        <signal name="I2(2)" />
        <signal name="I2(3)" />
        <signal name="I3(3)" />
        <signal name="I3(2)" />
        <signal name="I3(1)" />
        <signal name="I3(0)" />
        <signal name="XLXN_59" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="XLXN_63" />
        <signal name="XLXN_64" />
        <signal name="XLXN_65" />
        <signal name="XLXN_66" />
        <signal name="XLXN_67" />
        <signal name="XLXN_68" />
        <signal name="XLXN_69" />
        <signal name="XLXN_70" />
        <signal name="XLXN_71" />
        <signal name="XLXN_73" />
        <signal name="XLXN_74" />
        <signal name="XLXN_75" />
        <signal name="o(3:0)" />
        <signal name="o(0)" />
        <signal name="o(1)" />
        <signal name="o(2)" />
        <signal name="o(3)" />
        <port polarity="Input" name="s(1:0)" />
        <port polarity="Input" name="I1(3:0)" />
        <port polarity="Input" name="I2(3:0)" />
        <port polarity="Input" name="I0(3:0)" />
        <port polarity="Input" name="I3(3:0)" />
        <port polarity="Output" name="o(3:0)" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <block symbolname="inv" name="XLXI_1">
            <blockpin signalname="s(0)" name="I" />
            <blockpin signalname="XLXN_8" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_2">
            <blockpin signalname="s(1)" name="I" />
            <blockpin signalname="XLXN_5" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_17" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="XLXN_5" name="I0" />
            <blockpin signalname="s(0)" name="I1" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="XLXN_8" name="I1" />
            <blockpin signalname="XLXN_19" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="s(0)" name="I1" />
            <blockpin signalname="XLXN_20" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="I0(0)" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_59" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I1(0)" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="XLXN_60" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="I2(0)" name="I0" />
            <blockpin signalname="XLXN_19" name="I1" />
            <blockpin signalname="XLXN_61" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="I3(0)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_62" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="I0(1)" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_63" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="I1(1)" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="XLXN_64" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="I2(1)" name="I0" />
            <blockpin signalname="XLXN_19" name="I1" />
            <blockpin signalname="XLXN_65" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="I3(1)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_66" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_15">
            <blockpin signalname="I0(2)" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_67" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="I1(2)" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="XLXN_68" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_17">
            <blockpin signalname="I2(2)" name="I0" />
            <blockpin signalname="XLXN_19" name="I1" />
            <blockpin signalname="XLXN_69" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_18">
            <blockpin signalname="I3(2)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_70" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_19">
            <blockpin signalname="I0(3)" name="I0" />
            <blockpin signalname="XLXN_17" name="I1" />
            <blockpin signalname="XLXN_71" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_20">
            <blockpin signalname="I1(3)" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="XLXN_73" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="I2(3)" name="I0" />
            <blockpin signalname="XLXN_19" name="I1" />
            <blockpin signalname="XLXN_74" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_22">
            <blockpin signalname="I3(3)" name="I0" />
            <blockpin signalname="XLXN_20" name="I1" />
            <blockpin signalname="XLXN_75" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_23">
            <blockpin signalname="XLXN_62" name="I0" />
            <blockpin signalname="XLXN_61" name="I1" />
            <blockpin signalname="XLXN_60" name="I2" />
            <blockpin signalname="XLXN_59" name="I3" />
            <blockpin signalname="o(0)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_24">
            <blockpin signalname="XLXN_66" name="I0" />
            <blockpin signalname="XLXN_65" name="I1" />
            <blockpin signalname="XLXN_64" name="I2" />
            <blockpin signalname="XLXN_63" name="I3" />
            <blockpin signalname="o(1)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_25">
            <blockpin signalname="XLXN_70" name="I0" />
            <blockpin signalname="XLXN_69" name="I1" />
            <blockpin signalname="XLXN_68" name="I2" />
            <blockpin signalname="XLXN_67" name="I3" />
            <blockpin signalname="o(2)" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_26">
            <blockpin signalname="XLXN_75" name="I0" />
            <blockpin signalname="XLXN_74" name="I1" />
            <blockpin signalname="XLXN_73" name="I2" />
            <blockpin signalname="XLXN_71" name="I3" />
            <blockpin signalname="o(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <branch name="s(1:0)">
            <wire x2="752" y1="240" y2="240" x1="320" />
            <wire x2="752" y1="240" y2="352" x1="752" />
            <wire x2="752" y1="352" y2="448" x1="752" />
            <wire x2="752" y1="448" y2="528" x1="752" />
        </branch>
        <bustap x2="848" y1="352" y2="352" x1="752" />
        <branch name="s(0)">
            <wire x2="928" y1="352" y2="352" x1="848" />
            <wire x2="1088" y1="352" y2="352" x1="928" />
            <wire x2="928" y1="352" y2="512" x1="928" />
            <wire x2="1440" y1="512" y2="512" x1="928" />
            <wire x2="928" y1="512" y2="832" x1="928" />
            <wire x2="1440" y1="832" y2="832" x1="928" />
        </branch>
        <bustap x2="848" y1="448" y2="448" x1="752" />
        <branch name="s(1)">
            <wire x2="1024" y1="448" y2="448" x1="848" />
            <wire x2="1088" y1="448" y2="448" x1="1024" />
            <wire x2="1024" y1="448" y2="736" x1="1024" />
            <wire x2="1440" y1="736" y2="736" x1="1024" />
            <wire x2="1024" y1="736" y2="896" x1="1024" />
            <wire x2="1440" y1="896" y2="896" x1="1024" />
        </branch>
        <instance x="1088" y="384" name="XLXI_1" orien="R0" />
        <instance x="1088" y="480" name="XLXI_2" orien="R0" />
        <instance x="1440" y="480" name="XLXI_3" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="1376" y1="448" y2="448" x1="1312" />
            <wire x2="1376" y1="448" y2="576" x1="1376" />
            <wire x2="1440" y1="576" y2="576" x1="1376" />
            <wire x2="1440" y1="416" y2="416" x1="1376" />
            <wire x2="1376" y1="416" y2="448" x1="1376" />
        </branch>
        <instance x="1440" y="640" name="XLXI_4" orien="R0" />
        <instance x="1440" y="800" name="XLXI_5" orien="R0" />
        <branch name="XLXN_8">
            <wire x2="1392" y1="352" y2="352" x1="1312" />
            <wire x2="1440" y1="352" y2="352" x1="1392" />
            <wire x2="1392" y1="352" y2="672" x1="1392" />
            <wire x2="1440" y1="672" y2="672" x1="1392" />
        </branch>
        <instance x="1440" y="960" name="XLXI_6" orien="R0" />
        <iomarker fontsize="28" x="320" y="240" name="s(1:0)" orien="R180" />
        <instance x="2368" y="256" name="XLXI_7" orien="R0" />
        <instance x="2368" y="416" name="XLXI_8" orien="R0" />
        <instance x="2368" y="560" name="XLXI_9" orien="R0" />
        <instance x="2368" y="720" name="XLXI_10" orien="R0" />
        <instance x="2352" y="928" name="XLXI_11" orien="R0" />
        <instance x="2352" y="1088" name="XLXI_12" orien="R0" />
        <instance x="2352" y="1248" name="XLXI_13" orien="R0" />
        <instance x="2352" y="1408" name="XLXI_14" orien="R0" />
        <instance x="2352" y="1632" name="XLXI_15" orien="R0" />
        <instance x="2352" y="1776" name="XLXI_16" orien="R0" />
        <instance x="2368" y="1920" name="XLXI_17" orien="R0" />
        <instance x="2368" y="2080" name="XLXI_18" orien="R0" />
        <instance x="2368" y="2272" name="XLXI_19" orien="R0" />
        <instance x="2368" y="2448" name="XLXI_20" orien="R0" />
        <instance x="2368" y="2592" name="XLXI_21" orien="R0" />
        <instance x="2368" y="2752" name="XLXI_22" orien="R0" />
        <instance x="2944" y="544" name="XLXI_23" orien="R0" />
        <instance x="2928" y="1216" name="XLXI_24" orien="R0" />
        <instance x="2896" y="1920" name="XLXI_25" orien="R0" />
        <instance x="2896" y="2592" name="XLXI_26" orien="R0" />
        <branch name="I1(3:0)">
            <wire x2="2208" y1="1744" y2="1744" x1="192" />
            <wire x2="2208" y1="1744" y2="2384" x1="2208" />
            <wire x2="2208" y1="2384" y2="2752" x1="2208" />
            <wire x2="2208" y1="32" y2="352" x1="2208" />
            <wire x2="2208" y1="352" y2="1024" x1="2208" />
            <wire x2="2208" y1="1024" y2="1712" x1="2208" />
            <wire x2="2208" y1="1712" y2="1744" x1="2208" />
        </branch>
        <iomarker fontsize="28" x="192" y="1744" name="I1(3:0)" orien="R180" />
        <iomarker fontsize="28" x="176" y="1920" name="I2(3:0)" orien="R180" />
        <branch name="I3(3:0)">
            <wire x2="1936" y1="2112" y2="2112" x1="176" />
            <wire x2="1936" y1="2112" y2="2688" x1="1936" />
            <wire x2="1936" y1="2688" y2="2816" x1="1936" />
            <wire x2="1936" y1="64" y2="656" x1="1936" />
            <wire x2="1936" y1="656" y2="1344" x1="1936" />
            <wire x2="1936" y1="1344" y2="2016" x1="1936" />
            <wire x2="1936" y1="2016" y2="2112" x1="1936" />
        </branch>
        <iomarker fontsize="28" x="176" y="2112" name="I3(3:0)" orien="R180" />
        <branch name="XLXN_17">
            <wire x2="1760" y1="384" y2="384" x1="1696" />
            <wire x2="1760" y1="384" y2="800" x1="1760" />
            <wire x2="1760" y1="800" y2="1504" x1="1760" />
            <wire x2="1760" y1="1504" y2="2144" x1="1760" />
            <wire x2="2368" y1="2144" y2="2144" x1="1760" />
            <wire x2="2352" y1="1504" y2="1504" x1="1760" />
            <wire x2="2352" y1="800" y2="800" x1="1760" />
            <wire x2="2032" y1="384" y2="384" x1="1760" />
            <wire x2="2368" y1="128" y2="128" x1="2032" />
            <wire x2="2032" y1="128" y2="384" x1="2032" />
        </branch>
        <branch name="I0(3:0)">
            <wire x2="2096" y1="1456" y2="1456" x1="192" />
            <wire x2="2096" y1="1456" y2="1568" x1="2096" />
            <wire x2="2096" y1="1568" y2="2208" x1="2096" />
            <wire x2="2096" y1="2208" y2="2768" x1="2096" />
            <wire x2="2096" y1="48" y2="192" x1="2096" />
            <wire x2="2096" y1="192" y2="864" x1="2096" />
            <wire x2="2096" y1="864" y2="1456" x1="2096" />
        </branch>
        <bustap x2="2192" y1="192" y2="192" x1="2096" />
        <branch name="I0(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2280" y="192" type="branch" />
            <wire x2="2288" y1="192" y2="192" x1="2192" />
            <wire x2="2368" y1="192" y2="192" x1="2288" />
        </branch>
        <bustap x2="2192" y1="864" y2="864" x1="2096" />
        <branch name="I0(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="864" type="branch" />
            <wire x2="2272" y1="864" y2="864" x1="2192" />
            <wire x2="2352" y1="864" y2="864" x1="2272" />
        </branch>
        <bustap x2="2192" y1="1568" y2="1568" x1="2096" />
        <branch name="I0(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2272" y="1568" type="branch" />
            <wire x2="2272" y1="1568" y2="1568" x1="2192" />
            <wire x2="2352" y1="1568" y2="1568" x1="2272" />
        </branch>
        <bustap x2="2192" y1="2208" y2="2208" x1="2096" />
        <branch name="I0(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2280" y="2208" type="branch" />
            <wire x2="2288" y1="2208" y2="2208" x1="2192" />
            <wire x2="2368" y1="2208" y2="2208" x1="2288" />
        </branch>
        <bustap x2="2304" y1="352" y2="352" x1="2208" />
        <branch name="I1(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2336" y="352" type="branch" />
            <wire x2="2336" y1="352" y2="352" x1="2304" />
            <wire x2="2368" y1="352" y2="352" x1="2336" />
        </branch>
        <bustap x2="2304" y1="1024" y2="1024" x1="2208" />
        <branch name="I1(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="1024" type="branch" />
            <wire x2="2336" y1="1024" y2="1024" x1="2304" />
            <wire x2="2352" y1="1024" y2="1024" x1="2336" />
        </branch>
        <bustap x2="2304" y1="1712" y2="1712" x1="2208" />
        <branch name="I1(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2328" y="1712" type="branch" />
            <wire x2="2336" y1="1712" y2="1712" x1="2304" />
            <wire x2="2352" y1="1712" y2="1712" x1="2336" />
        </branch>
        <bustap x2="2304" y1="2384" y2="2384" x1="2208" />
        <branch name="I1(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2336" y="2384" type="branch" />
            <wire x2="2336" y1="2384" y2="2384" x1="2304" />
            <wire x2="2368" y1="2384" y2="2384" x1="2336" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1792" y1="544" y2="544" x1="1696" />
            <wire x2="2048" y1="544" y2="544" x1="1792" />
            <wire x2="1792" y1="544" y2="960" x1="1792" />
            <wire x2="1792" y1="960" y2="1648" x1="1792" />
            <wire x2="1792" y1="1648" y2="2320" x1="1792" />
            <wire x2="2368" y1="2320" y2="2320" x1="1792" />
            <wire x2="2352" y1="1648" y2="1648" x1="1792" />
            <wire x2="2352" y1="960" y2="960" x1="1792" />
            <wire x2="2048" y1="288" y2="544" x1="2048" />
            <wire x2="2368" y1="288" y2="288" x1="2048" />
        </branch>
        <branch name="I2(3:0)">
            <wire x2="1728" y1="1920" y2="1920" x1="176" />
            <wire x2="1728" y1="1920" y2="2560" x1="1728" />
            <wire x2="1728" y1="2560" y2="2768" x1="1728" />
            <wire x2="1728" y1="64" y2="496" x1="1728" />
            <wire x2="1728" y1="496" y2="1168" x1="1728" />
            <wire x2="1728" y1="1168" y2="1184" x1="1728" />
            <wire x2="1728" y1="1184" y2="1200" x1="1728" />
            <wire x2="1728" y1="1200" y2="1216" x1="1728" />
            <wire x2="1728" y1="1216" y2="1888" x1="1728" />
            <wire x2="1728" y1="1888" y2="1920" x1="1728" />
        </branch>
        <bustap x2="1824" y1="496" y2="496" x1="1728" />
        <branch name="I2(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1856" y="496" type="branch" />
            <wire x2="1856" y1="496" y2="496" x1="1824" />
            <wire x2="2368" y1="496" y2="496" x1="1856" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="1824" y1="704" y2="704" x1="1696" />
            <wire x2="2064" y1="704" y2="704" x1="1824" />
            <wire x2="1824" y1="704" y2="1120" x1="1824" />
            <wire x2="2352" y1="1120" y2="1120" x1="1824" />
            <wire x2="1824" y1="1120" y2="1184" x1="1824" />
            <wire x2="1824" y1="1184" y2="1216" x1="1824" />
            <wire x2="1824" y1="1216" y2="1792" x1="1824" />
            <wire x2="1824" y1="1792" y2="2464" x1="1824" />
            <wire x2="2368" y1="2464" y2="2464" x1="1824" />
            <wire x2="2368" y1="1792" y2="1792" x1="1824" />
            <wire x2="2064" y1="432" y2="704" x1="2064" />
            <wire x2="2368" y1="432" y2="432" x1="2064" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="1856" y1="864" y2="864" x1="1696" />
            <wire x2="2080" y1="864" y2="864" x1="1856" />
            <wire x2="1856" y1="864" y2="1280" x1="1856" />
            <wire x2="1856" y1="1280" y2="1952" x1="1856" />
            <wire x2="1856" y1="1952" y2="2624" x1="1856" />
            <wire x2="2368" y1="2624" y2="2624" x1="1856" />
            <wire x2="2368" y1="1952" y2="1952" x1="1856" />
            <wire x2="2352" y1="1280" y2="1280" x1="1856" />
            <wire x2="2080" y1="592" y2="864" x1="2080" />
            <wire x2="2368" y1="592" y2="592" x1="2080" />
        </branch>
        <bustap x2="1632" y1="1200" y2="1200" x1="1728" />
        <branch name="I2(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1584" y="1200" type="branch" />
            <wire x2="1584" y1="1200" y2="1200" x1="1536" />
            <wire x2="1632" y1="1200" y2="1200" x1="1584" />
            <wire x2="1536" y1="1200" y2="1280" x1="1536" />
            <wire x2="1840" y1="1280" y2="1280" x1="1536" />
            <wire x2="1840" y1="1184" y2="1280" x1="1840" />
            <wire x2="2352" y1="1184" y2="1184" x1="1840" />
        </branch>
        <bustap x2="1632" y1="1888" y2="1888" x1="1728" />
        <branch name="I2(2)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1584" y="1888" type="branch" />
            <wire x2="2368" y1="1856" y2="1856" x1="1536" />
            <wire x2="1536" y1="1856" y2="1888" x1="1536" />
            <wire x2="1584" y1="1888" y2="1888" x1="1536" />
            <wire x2="1632" y1="1888" y2="1888" x1="1584" />
        </branch>
        <bustap x2="1632" y1="2560" y2="2560" x1="1728" />
        <branch name="I2(3)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1600" y="2560" type="branch" />
            <wire x2="2368" y1="2528" y2="2528" x1="1568" />
            <wire x2="1568" y1="2528" y2="2560" x1="1568" />
            <wire x2="1600" y1="2560" y2="2560" x1="1568" />
            <wire x2="1632" y1="2560" y2="2560" x1="1600" />
        </branch>
        <bustap x2="2032" y1="2688" y2="2688" x1="1936" />
        <branch name="I3(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2200" y="2688" type="branch" />
            <wire x2="2208" y1="2688" y2="2688" x1="2032" />
            <wire x2="2368" y1="2688" y2="2688" x1="2208" />
        </branch>
        <bustap x2="2032" y1="2016" y2="2016" x1="1936" />
        <branch name="I3(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2200" y="2016" type="branch" />
            <wire x2="2208" y1="2016" y2="2016" x1="2032" />
            <wire x2="2368" y1="2016" y2="2016" x1="2208" />
        </branch>
        <bustap x2="2032" y1="1344" y2="1344" x1="1936" />
        <branch name="I3(1)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2192" y="1344" type="branch" />
            <wire x2="2192" y1="1344" y2="1344" x1="2032" />
            <wire x2="2352" y1="1344" y2="1344" x1="2192" />
        </branch>
        <bustap x2="2032" y1="656" y2="656" x1="1936" />
        <branch name="I3(0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2200" y="656" type="branch" />
            <wire x2="2208" y1="656" y2="656" x1="2032" />
            <wire x2="2368" y1="656" y2="656" x1="2208" />
        </branch>
        <branch name="XLXN_59">
            <wire x2="2944" y1="160" y2="160" x1="2624" />
            <wire x2="2944" y1="160" y2="288" x1="2944" />
        </branch>
        <branch name="XLXN_60">
            <wire x2="2784" y1="320" y2="320" x1="2624" />
            <wire x2="2784" y1="320" y2="352" x1="2784" />
            <wire x2="2944" y1="352" y2="352" x1="2784" />
        </branch>
        <branch name="XLXN_61">
            <wire x2="2784" y1="464" y2="464" x1="2624" />
            <wire x2="2784" y1="416" y2="464" x1="2784" />
            <wire x2="2944" y1="416" y2="416" x1="2784" />
        </branch>
        <branch name="XLXN_62">
            <wire x2="2944" y1="624" y2="624" x1="2624" />
            <wire x2="2944" y1="480" y2="624" x1="2944" />
        </branch>
        <branch name="XLXN_63">
            <wire x2="2928" y1="832" y2="832" x1="2608" />
            <wire x2="2928" y1="832" y2="960" x1="2928" />
        </branch>
        <branch name="XLXN_64">
            <wire x2="2768" y1="992" y2="992" x1="2608" />
            <wire x2="2768" y1="992" y2="1024" x1="2768" />
            <wire x2="2928" y1="1024" y2="1024" x1="2768" />
        </branch>
        <branch name="XLXN_65">
            <wire x2="2768" y1="1152" y2="1152" x1="2608" />
            <wire x2="2768" y1="1088" y2="1152" x1="2768" />
            <wire x2="2928" y1="1088" y2="1088" x1="2768" />
        </branch>
        <branch name="XLXN_66">
            <wire x2="2928" y1="1312" y2="1312" x1="2608" />
            <wire x2="2928" y1="1152" y2="1312" x1="2928" />
        </branch>
        <branch name="XLXN_67">
            <wire x2="2896" y1="1536" y2="1536" x1="2608" />
            <wire x2="2896" y1="1536" y2="1664" x1="2896" />
        </branch>
        <branch name="XLXN_68">
            <wire x2="2752" y1="1680" y2="1680" x1="2608" />
            <wire x2="2752" y1="1680" y2="1728" x1="2752" />
            <wire x2="2896" y1="1728" y2="1728" x1="2752" />
        </branch>
        <branch name="XLXN_69">
            <wire x2="2752" y1="1824" y2="1824" x1="2624" />
            <wire x2="2752" y1="1792" y2="1824" x1="2752" />
            <wire x2="2896" y1="1792" y2="1792" x1="2752" />
        </branch>
        <branch name="XLXN_70">
            <wire x2="2896" y1="1984" y2="1984" x1="2624" />
            <wire x2="2896" y1="1856" y2="1984" x1="2896" />
        </branch>
        <branch name="XLXN_71">
            <wire x2="2896" y1="2176" y2="2176" x1="2624" />
            <wire x2="2896" y1="2176" y2="2336" x1="2896" />
        </branch>
        <branch name="XLXN_73">
            <wire x2="2640" y1="2352" y2="2352" x1="2624" />
            <wire x2="2640" y1="2352" y2="2400" x1="2640" />
            <wire x2="2896" y1="2400" y2="2400" x1="2640" />
        </branch>
        <branch name="XLXN_74">
            <wire x2="2752" y1="2496" y2="2496" x1="2624" />
            <wire x2="2752" y1="2464" y2="2496" x1="2752" />
            <wire x2="2896" y1="2464" y2="2464" x1="2752" />
        </branch>
        <branch name="XLXN_75">
            <wire x2="2896" y1="2656" y2="2656" x1="2624" />
            <wire x2="2896" y1="2528" y2="2656" x1="2896" />
        </branch>
        <branch name="o(3:0)">
            <wire x2="3680" y1="128" y2="384" x1="3680" />
            <wire x2="3680" y1="384" y2="1056" x1="3680" />
            <wire x2="3680" y1="1056" y2="1328" x1="3680" />
            <wire x2="4560" y1="1328" y2="1328" x1="3680" />
            <wire x2="3680" y1="1328" y2="1760" x1="3680" />
            <wire x2="3680" y1="1760" y2="2432" x1="3680" />
            <wire x2="3680" y1="2432" y2="2640" x1="3680" />
        </branch>
        <iomarker fontsize="28" x="4560" y="1328" name="o(3:0)" orien="R0" />
        <bustap x2="3584" y1="384" y2="384" x1="3680" />
        <branch name="o(0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="3392" y="384" type="branch" />
            <wire x2="3392" y1="384" y2="384" x1="3200" />
            <wire x2="3584" y1="384" y2="384" x1="3392" />
        </branch>
        <bustap x2="3584" y1="1056" y2="1056" x1="3680" />
        <branch name="o(1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3384" y="1056" type="branch" />
            <wire x2="3392" y1="1056" y2="1056" x1="3184" />
            <wire x2="3584" y1="1056" y2="1056" x1="3392" />
        </branch>
        <bustap x2="3584" y1="1760" y2="1760" x1="3680" />
        <branch name="o(2)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3368" y="1760" type="branch" />
            <wire x2="3376" y1="1760" y2="1760" x1="3152" />
            <wire x2="3584" y1="1760" y2="1760" x1="3376" />
        </branch>
        <bustap x2="3584" y1="2432" y2="2432" x1="3680" />
        <branch name="o(3)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3368" y="2432" type="branch" />
            <wire x2="3376" y1="2432" y2="2432" x1="3152" />
            <wire x2="3584" y1="2432" y2="2432" x1="3376" />
        </branch>
        <iomarker fontsize="28" x="192" y="1456" name="I0(3:0)" orien="R180" />
    </sheet>
</drawing>