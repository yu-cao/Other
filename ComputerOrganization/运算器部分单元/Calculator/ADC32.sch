<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(31:0)" />
        <signal name="s(3:0)" />
        <signal name="s(7:4)" />
        <signal name="s(11:8)" />
        <signal name="s(15:12)" />
        <signal name="s(19:16)" />
        <signal name="s(23:20)" />
        <signal name="s(27:24)" />
        <signal name="s(31:28)" />
        <signal name="XLXN_13" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="XLXN_16" />
        <signal name="XLXN_17" />
        <signal name="XLXN_18" />
        <signal name="XLXN_19" />
        <signal name="XLXN_20" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <signal name="XLXN_27" />
        <signal name="XLXN_28" />
        <signal name="a(31:0)" />
        <signal name="b(31:0)" />
        <signal name="a(3:0)" />
        <signal name="a(7:4)" />
        <signal name="a(11:8)" />
        <signal name="a(15:12)" />
        <signal name="a(19:16)" />
        <signal name="a(23:20)" />
        <signal name="a(27:24)" />
        <signal name="a(31:28)" />
        <signal name="b(3:0)" />
        <signal name="b(7:4)" />
        <signal name="b(11:8)" />
        <signal name="b(15:12)" />
        <signal name="b(19:16)" />
        <signal name="b(23:20)" />
        <signal name="b(27:24)" />
        <signal name="b(31:28)" />
        <signal name="C0" />
        <signal name="XLXN_49" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51" />
        <signal name="XLXN_53" />
        <signal name="XLXN_54" />
        <signal name="XLXN_55" />
        <signal name="XLXN_56" />
        <signal name="XLXN_57" />
        <signal name="XLXN_58" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="Co" />
        <signal name="XLXN_64" />
        <port polarity="Output" name="s(31:0)" />
        <port polarity="Input" name="a(31:0)" />
        <port polarity="Input" name="b(31:0)" />
        <port polarity="Input" name="C0" />
        <port polarity="Output" name="Co" />
        <blockdef name="ADD4b">
            <timestamp>2017-11-26T19:16:35</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="CLA">
            <timestamp>2017-11-26T19:15:1</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-400" y2="-400" x1="320" />
            <line x2="384" y1="-320" y2="-320" x1="320" />
            <line x2="384" y1="-240" y2="-240" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-80" y2="-80" x1="320" />
            <rect width="256" x="64" y="-512" height="576" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="ADD4b" name="XLXI_2">
            <blockpin signalname="a(3:0)" name="ai(3:0)" />
            <blockpin signalname="b(3:0)" name="bi(3:0)" />
            <blockpin signalname="C0" name="C0" />
            <blockpin signalname="XLXN_14" name="GP" />
            <blockpin signalname="XLXN_13" name="GG" />
            <blockpin signalname="s(3:0)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_4">
            <blockpin signalname="a(7:4)" name="ai(3:0)" />
            <blockpin signalname="b(7:4)" name="bi(3:0)" />
            <blockpin signalname="XLXN_49" name="C0" />
            <blockpin signalname="XLXN_16" name="GP" />
            <blockpin signalname="XLXN_15" name="GG" />
            <blockpin signalname="s(7:4)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_5">
            <blockpin signalname="a(11:8)" name="ai(3:0)" />
            <blockpin signalname="b(11:8)" name="bi(3:0)" />
            <blockpin signalname="XLXN_50" name="C0" />
            <blockpin signalname="XLXN_18" name="GP" />
            <blockpin signalname="XLXN_17" name="GG" />
            <blockpin signalname="s(11:8)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_6">
            <blockpin signalname="a(15:12)" name="ai(3:0)" />
            <blockpin signalname="b(15:12)" name="bi(3:0)" />
            <blockpin signalname="XLXN_51" name="C0" />
            <blockpin signalname="XLXN_20" name="GP" />
            <blockpin signalname="XLXN_19" name="GG" />
            <blockpin signalname="s(15:12)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_12">
            <blockpin signalname="a(19:16)" name="ai(3:0)" />
            <blockpin signalname="b(19:16)" name="bi(3:0)" />
            <blockpin signalname="XLXN_64" name="C0" />
            <blockpin signalname="XLXN_22" name="GP" />
            <blockpin signalname="XLXN_21" name="GG" />
            <blockpin signalname="s(19:16)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_13">
            <blockpin signalname="a(23:20)" name="ai(3:0)" />
            <blockpin signalname="b(23:20)" name="bi(3:0)" />
            <blockpin signalname="XLXN_53" name="C0" />
            <blockpin signalname="XLXN_24" name="GP" />
            <blockpin signalname="XLXN_23" name="GG" />
            <blockpin signalname="s(23:20)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_14">
            <blockpin signalname="a(27:24)" name="ai(3:0)" />
            <blockpin signalname="b(27:24)" name="bi(3:0)" />
            <blockpin signalname="XLXN_54" name="C0" />
            <blockpin signalname="XLXN_26" name="GP" />
            <blockpin signalname="XLXN_25" name="GG" />
            <blockpin signalname="s(27:24)" name="S(3:0)" />
        </block>
        <block symbolname="ADD4b" name="XLXI_15">
            <blockpin signalname="a(31:28)" name="ai(3:0)" />
            <blockpin signalname="b(31:28)" name="bi(3:0)" />
            <blockpin signalname="XLXN_55" name="C0" />
            <blockpin signalname="XLXN_28" name="GP" />
            <blockpin signalname="XLXN_27" name="GG" />
            <blockpin signalname="s(31:28)" name="S(3:0)" />
        </block>
        <block symbolname="CLA" name="XLXI_16">
            <blockpin signalname="XLXN_19" name="G3" />
            <blockpin signalname="XLXN_20" name="P3" />
            <blockpin signalname="XLXN_17" name="G2" />
            <blockpin signalname="XLXN_18" name="P2" />
            <blockpin signalname="XLXN_15" name="G1" />
            <blockpin signalname="XLXN_16" name="P1" />
            <blockpin signalname="XLXN_13" name="G0" />
            <blockpin signalname="XLXN_14" name="P0" />
            <blockpin signalname="C0" name="Ci" />
            <blockpin signalname="XLXN_49" name="C1" />
            <blockpin signalname="XLXN_50" name="C2" />
            <blockpin signalname="XLXN_51" name="C3" />
            <blockpin signalname="XLXN_56" name="GP" />
            <blockpin signalname="XLXN_58" name="GG" />
        </block>
        <block symbolname="CLA" name="XLXI_17">
            <blockpin signalname="XLXN_27" name="G3" />
            <blockpin signalname="XLXN_28" name="P3" />
            <blockpin signalname="XLXN_25" name="G2" />
            <blockpin signalname="XLXN_26" name="P2" />
            <blockpin signalname="XLXN_23" name="G1" />
            <blockpin signalname="XLXN_24" name="P1" />
            <blockpin signalname="XLXN_21" name="G0" />
            <blockpin signalname="XLXN_22" name="P0" />
            <blockpin signalname="XLXN_64" name="Ci" />
            <blockpin signalname="XLXN_53" name="C1" />
            <blockpin signalname="XLXN_54" name="C2" />
            <blockpin signalname="XLXN_55" name="C3" />
            <blockpin signalname="XLXN_60" name="GP" />
            <blockpin signalname="XLXN_62" name="GG" />
        </block>
        <block symbolname="and2" name="XLXI_18">
            <blockpin signalname="C0" name="I0" />
            <blockpin signalname="XLXN_56" name="I1" />
            <blockpin signalname="XLXN_57" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_19">
            <blockpin signalname="XLXN_57" name="I0" />
            <blockpin signalname="XLXN_58" name="I1" />
            <blockpin signalname="XLXN_64" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_20">
            <blockpin signalname="XLXN_62" name="I0" />
            <blockpin signalname="XLXN_61" name="I1" />
            <blockpin signalname="Co" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_21">
            <blockpin signalname="XLXN_64" name="I0" />
            <blockpin signalname="XLXN_60" name="I1" />
            <blockpin signalname="XLXN_61" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="7040" height="5440">
        <instance x="688" y="1200" name="XLXI_2" orien="R90">
        </instance>
        <instance x="1312" y="1232" name="XLXI_4" orien="R90">
        </instance>
        <instance x="1984" y="1232" name="XLXI_5" orien="R90">
        </instance>
        <instance x="2640" y="1216" name="XLXI_6" orien="R90">
        </instance>
        <instance x="3488" y="1264" name="XLXI_12" orien="R90">
        </instance>
        <instance x="4112" y="1296" name="XLXI_13" orien="R90">
        </instance>
        <instance x="4784" y="1296" name="XLXI_14" orien="R90">
        </instance>
        <instance x="5440" y="1280" name="XLXI_15" orien="R90">
        </instance>
        <instance x="1584" y="3792" name="XLXI_16" orien="R90">
        </instance>
        <instance x="4480" y="3776" name="XLXI_17" orien="R90">
        </instance>
        <branch name="s(31:0)">
            <wire x2="720" y1="2208" y2="2208" x1="704" />
            <wire x2="1344" y1="2208" y2="2208" x1="720" />
            <wire x2="2016" y1="2208" y2="2208" x1="1344" />
            <wire x2="2672" y1="2208" y2="2208" x1="2016" />
            <wire x2="3040" y1="2208" y2="2208" x1="2672" />
            <wire x2="3040" y1="2208" y2="2352" x1="3040" />
            <wire x2="3072" y1="2352" y2="2352" x1="3040" />
            <wire x2="3520" y1="2208" y2="2208" x1="3040" />
            <wire x2="4144" y1="2208" y2="2208" x1="3520" />
            <wire x2="4816" y1="2208" y2="2208" x1="4144" />
            <wire x2="5472" y1="2208" y2="2208" x1="4816" />
            <wire x2="5616" y1="2208" y2="2208" x1="5472" />
        </branch>
        <iomarker fontsize="28" x="3072" y="2352" name="s(31:0)" orien="R0" />
        <bustap x2="720" y1="2208" y2="2112" x1="720" />
        <branch name="s(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="720" y="1848" type="branch" />
            <wire x2="720" y1="1584" y2="1856" x1="720" />
            <wire x2="720" y1="1856" y2="2112" x1="720" />
        </branch>
        <bustap x2="1344" y1="2208" y2="2112" x1="1344" />
        <branch name="s(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1344" y="1864" type="branch" />
            <wire x2="1344" y1="1616" y2="1872" x1="1344" />
            <wire x2="1344" y1="1872" y2="2112" x1="1344" />
        </branch>
        <bustap x2="2016" y1="2208" y2="2112" x1="2016" />
        <branch name="s(11:8)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2016" y="1864" type="branch" />
            <wire x2="2016" y1="1616" y2="1872" x1="2016" />
            <wire x2="2016" y1="1872" y2="2112" x1="2016" />
        </branch>
        <bustap x2="2672" y1="2208" y2="2112" x1="2672" />
        <branch name="s(15:12)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2672" y="1856" type="branch" />
            <wire x2="2672" y1="1600" y2="1856" x1="2672" />
            <wire x2="2672" y1="1856" y2="2112" x1="2672" />
        </branch>
        <bustap x2="3520" y1="2208" y2="2112" x1="3520" />
        <branch name="s(19:16)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3520" y="1880" type="branch" />
            <wire x2="3520" y1="1648" y2="1888" x1="3520" />
            <wire x2="3520" y1="1888" y2="2112" x1="3520" />
        </branch>
        <bustap x2="4144" y1="2208" y2="2112" x1="4144" />
        <branch name="s(23:20)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4144" y="1896" type="branch" />
            <wire x2="4144" y1="1680" y2="1904" x1="4144" />
            <wire x2="4144" y1="1904" y2="2112" x1="4144" />
        </branch>
        <bustap x2="4816" y1="2208" y2="2112" x1="4816" />
        <branch name="s(27:24)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4816" y="1896" type="branch" />
            <wire x2="4816" y1="1680" y2="1904" x1="4816" />
            <wire x2="4816" y1="1904" y2="2112" x1="4816" />
        </branch>
        <bustap x2="5472" y1="2208" y2="2112" x1="5472" />
        <branch name="s(31:28)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5472" y="1888" type="branch" />
            <wire x2="5472" y1="1664" y2="1888" x1="5472" />
            <wire x2="5472" y1="1888" y2="2112" x1="5472" />
        </branch>
        <branch name="XLXN_13">
            <wire x2="784" y1="1584" y2="2688" x1="784" />
            <wire x2="1744" y1="2688" y2="2688" x1="784" />
            <wire x2="1744" y1="2688" y2="3792" x1="1744" />
        </branch>
        <branch name="XLXN_14">
            <wire x2="848" y1="1584" y2="2672" x1="848" />
            <wire x2="1680" y1="2672" y2="2672" x1="848" />
            <wire x2="1680" y1="2672" y2="3792" x1="1680" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1408" y1="1616" y2="4256" x1="1408" />
            <wire x2="2064" y1="4256" y2="4256" x1="1408" />
            <wire x2="2064" y1="4176" y2="4256" x1="2064" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="1472" y1="1616" y2="2656" x1="1472" />
            <wire x2="1808" y1="2656" y2="2656" x1="1472" />
            <wire x2="1808" y1="2656" y2="3792" x1="1808" />
        </branch>
        <branch name="XLXN_17">
            <wire x2="1936" y1="2704" y2="3792" x1="1936" />
            <wire x2="2080" y1="2704" y2="2704" x1="1936" />
            <wire x2="2080" y1="1616" y2="2704" x1="2080" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1872" y1="2688" y2="3792" x1="1872" />
            <wire x2="2144" y1="2688" y2="2688" x1="1872" />
            <wire x2="2144" y1="1616" y2="2688" x1="2144" />
        </branch>
        <branch name="XLXN_19">
            <wire x2="2064" y1="2720" y2="3792" x1="2064" />
            <wire x2="2736" y1="2720" y2="2720" x1="2064" />
            <wire x2="2736" y1="1600" y2="2720" x1="2736" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="2000" y1="2672" y2="3792" x1="2000" />
            <wire x2="2800" y1="2672" y2="2672" x1="2000" />
            <wire x2="2800" y1="1600" y2="2672" x1="2800" />
        </branch>
        <branch name="XLXN_21">
            <wire x2="3584" y1="1648" y2="2704" x1="3584" />
            <wire x2="4640" y1="2704" y2="2704" x1="3584" />
            <wire x2="4640" y1="2704" y2="3776" x1="4640" />
        </branch>
        <branch name="XLXN_22">
            <wire x2="3648" y1="1648" y2="2688" x1="3648" />
            <wire x2="4576" y1="2688" y2="2688" x1="3648" />
            <wire x2="4576" y1="2688" y2="3776" x1="4576" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="4208" y1="1680" y2="4240" x1="4208" />
            <wire x2="4960" y1="4240" y2="4240" x1="4208" />
            <wire x2="4960" y1="4160" y2="4240" x1="4960" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="4272" y1="1680" y2="2672" x1="4272" />
            <wire x2="4704" y1="2672" y2="2672" x1="4272" />
            <wire x2="4704" y1="2672" y2="3776" x1="4704" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="4832" y1="2720" y2="3776" x1="4832" />
            <wire x2="4880" y1="2720" y2="2720" x1="4832" />
            <wire x2="4880" y1="1680" y2="2720" x1="4880" />
        </branch>
        <branch name="XLXN_26">
            <wire x2="4768" y1="2704" y2="3776" x1="4768" />
            <wire x2="4944" y1="2704" y2="2704" x1="4768" />
            <wire x2="4944" y1="1680" y2="2704" x1="4944" />
        </branch>
        <branch name="XLXN_27">
            <wire x2="4960" y1="2720" y2="3776" x1="4960" />
            <wire x2="5536" y1="2720" y2="2720" x1="4960" />
            <wire x2="5536" y1="1664" y2="2720" x1="5536" />
        </branch>
        <branch name="XLXN_28">
            <wire x2="4896" y1="2688" y2="3776" x1="4896" />
            <wire x2="5600" y1="2688" y2="2688" x1="4896" />
            <wire x2="5600" y1="1664" y2="2688" x1="5600" />
        </branch>
        <branch name="a(31:0)">
            <wire x2="848" y1="512" y2="512" x1="608" />
            <wire x2="1472" y1="512" y2="512" x1="848" />
            <wire x2="2144" y1="512" y2="512" x1="1472" />
            <wire x2="2800" y1="512" y2="512" x1="2144" />
            <wire x2="3648" y1="512" y2="512" x1="2800" />
            <wire x2="4272" y1="512" y2="512" x1="3648" />
            <wire x2="4944" y1="512" y2="512" x1="4272" />
            <wire x2="5600" y1="512" y2="512" x1="4944" />
            <wire x2="5680" y1="512" y2="512" x1="5600" />
        </branch>
        <branch name="b(31:0)">
            <wire x2="784" y1="736" y2="736" x1="656" />
            <wire x2="1408" y1="736" y2="736" x1="784" />
            <wire x2="2080" y1="736" y2="736" x1="1408" />
            <wire x2="2736" y1="736" y2="736" x1="2080" />
            <wire x2="3584" y1="736" y2="736" x1="2736" />
            <wire x2="4208" y1="736" y2="736" x1="3584" />
            <wire x2="4880" y1="736" y2="736" x1="4208" />
            <wire x2="5536" y1="736" y2="736" x1="4880" />
            <wire x2="5840" y1="736" y2="736" x1="5536" />
        </branch>
        <iomarker fontsize="28" x="608" y="512" name="a(31:0)" orien="R180" />
        <iomarker fontsize="28" x="656" y="736" name="b(31:0)" orien="R180" />
        <bustap x2="848" y1="512" y2="608" x1="848" />
        <branch name="a(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="848" y="904" type="branch" />
            <wire x2="848" y1="608" y2="912" x1="848" />
            <wire x2="848" y1="912" y2="1200" x1="848" />
        </branch>
        <bustap x2="1472" y1="512" y2="608" x1="1472" />
        <branch name="a(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1472" y="920" type="branch" />
            <wire x2="1472" y1="608" y2="928" x1="1472" />
            <wire x2="1472" y1="928" y2="1232" x1="1472" />
        </branch>
        <bustap x2="2144" y1="512" y2="608" x1="2144" />
        <branch name="a(11:8)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="912" type="branch" />
            <wire x2="2144" y1="608" y2="912" x1="2144" />
            <wire x2="2144" y1="912" y2="1216" x1="2144" />
            <wire x2="2144" y1="1216" y2="1232" x1="2144" />
        </branch>
        <bustap x2="2800" y1="512" y2="608" x1="2800" />
        <branch name="a(15:12)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2800" y="912" type="branch" />
            <wire x2="2800" y1="608" y2="912" x1="2800" />
            <wire x2="2800" y1="912" y2="1216" x1="2800" />
        </branch>
        <bustap x2="3648" y1="512" y2="608" x1="3648" />
        <branch name="a(19:16)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3648" y="936" type="branch" />
            <wire x2="3648" y1="608" y2="944" x1="3648" />
            <wire x2="3648" y1="944" y2="1264" x1="3648" />
        </branch>
        <bustap x2="4272" y1="512" y2="608" x1="4272" />
        <branch name="a(23:20)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4272" y="952" type="branch" />
            <wire x2="4272" y1="608" y2="960" x1="4272" />
            <wire x2="4272" y1="960" y2="1296" x1="4272" />
        </branch>
        <bustap x2="4944" y1="512" y2="608" x1="4944" />
        <branch name="a(27:24)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4944" y="952" type="branch" />
            <wire x2="4944" y1="608" y2="960" x1="4944" />
            <wire x2="4944" y1="960" y2="1296" x1="4944" />
        </branch>
        <bustap x2="5600" y1="512" y2="608" x1="5600" />
        <branch name="a(31:28)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5600" y="944" type="branch" />
            <wire x2="5600" y1="608" y2="944" x1="5600" />
            <wire x2="5600" y1="944" y2="1280" x1="5600" />
        </branch>
        <bustap x2="784" y1="736" y2="832" x1="784" />
        <branch name="b(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="784" y="1016" type="branch" />
            <wire x2="784" y1="832" y2="1024" x1="784" />
            <wire x2="784" y1="1024" y2="1200" x1="784" />
        </branch>
        <bustap x2="1408" y1="736" y2="832" x1="1408" />
        <branch name="b(7:4)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1408" y="1032" type="branch" />
            <wire x2="1408" y1="832" y2="1040" x1="1408" />
            <wire x2="1408" y1="1040" y2="1232" x1="1408" />
        </branch>
        <bustap x2="2080" y1="736" y2="832" x1="2080" />
        <branch name="b(11:8)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2080" y="1032" type="branch" />
            <wire x2="2080" y1="832" y2="1040" x1="2080" />
            <wire x2="2080" y1="1040" y2="1232" x1="2080" />
        </branch>
        <bustap x2="2736" y1="736" y2="832" x1="2736" />
        <branch name="b(15:12)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2736" y="1024" type="branch" />
            <wire x2="2736" y1="832" y2="1024" x1="2736" />
            <wire x2="2736" y1="1024" y2="1216" x1="2736" />
        </branch>
        <bustap x2="3584" y1="736" y2="832" x1="3584" />
        <branch name="b(19:16)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="3584" y="1048" type="branch" />
            <wire x2="3584" y1="832" y2="1056" x1="3584" />
            <wire x2="3584" y1="1056" y2="1264" x1="3584" />
        </branch>
        <bustap x2="4208" y1="736" y2="832" x1="4208" />
        <branch name="b(23:20)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4208" y="1064" type="branch" />
            <wire x2="4208" y1="832" y2="1072" x1="4208" />
            <wire x2="4208" y1="1072" y2="1296" x1="4208" />
        </branch>
        <bustap x2="4880" y1="736" y2="832" x1="4880" />
        <branch name="b(27:24)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="4880" y="1064" type="branch" />
            <wire x2="4880" y1="832" y2="1072" x1="4880" />
            <wire x2="4880" y1="1072" y2="1296" x1="4880" />
        </branch>
        <bustap x2="5536" y1="736" y2="832" x1="5536" />
        <branch name="b(31:28)">
            <attrtext style="alignment:SOFT-TVCENTER;fontsize:28;fontname:Arial" attrname="Name" x="5536" y="1056" type="branch" />
            <wire x2="5536" y1="832" y2="1056" x1="5536" />
            <wire x2="5536" y1="1056" y2="1280" x1="5536" />
        </branch>
        <branch name="C0">
            <wire x2="720" y1="976" y2="976" x1="384" />
            <wire x2="720" y1="976" y2="1184" x1="720" />
            <wire x2="720" y1="1184" y2="1200" x1="720" />
            <wire x2="1552" y1="1184" y2="1184" x1="720" />
            <wire x2="1552" y1="1184" y2="3760" x1="1552" />
            <wire x2="1552" y1="3760" y2="3792" x1="1552" />
            <wire x2="1552" y1="3760" y2="3760" x1="1488" />
            <wire x2="1488" y1="3760" y2="4368" x1="1488" />
        </branch>
        <branch name="XLXN_49">
            <wire x2="1344" y1="1168" y2="1168" x1="1232" />
            <wire x2="1344" y1="1168" y2="1232" x1="1344" />
            <wire x2="1232" y1="1168" y2="4240" x1="1232" />
            <wire x2="1984" y1="4240" y2="4240" x1="1232" />
            <wire x2="1984" y1="4176" y2="4240" x1="1984" />
        </branch>
        <branch name="XLXN_50">
            <wire x2="1904" y1="4176" y2="4192" x1="1904" />
            <wire x2="2256" y1="4192" y2="4192" x1="1904" />
            <wire x2="2256" y1="1152" y2="1152" x1="2016" />
            <wire x2="2256" y1="1152" y2="4192" x1="2256" />
            <wire x2="2016" y1="1152" y2="1232" x1="2016" />
        </branch>
        <branch name="XLXN_51">
            <wire x2="1824" y1="4176" y2="4208" x1="1824" />
            <wire x2="2240" y1="4208" y2="4208" x1="1824" />
            <wire x2="2240" y1="1168" y2="4208" x1="2240" />
            <wire x2="2672" y1="1168" y2="1168" x1="2240" />
            <wire x2="2672" y1="1168" y2="1216" x1="2672" />
        </branch>
        <branch name="XLXN_53">
            <wire x2="4144" y1="1216" y2="1216" x1="4064" />
            <wire x2="4144" y1="1216" y2="1296" x1="4144" />
            <wire x2="4064" y1="1216" y2="4224" x1="4064" />
            <wire x2="4880" y1="4224" y2="4224" x1="4064" />
            <wire x2="4880" y1="4160" y2="4224" x1="4880" />
        </branch>
        <branch name="XLXN_54">
            <wire x2="4336" y1="1248" y2="4208" x1="4336" />
            <wire x2="4800" y1="4208" y2="4208" x1="4336" />
            <wire x2="4816" y1="1248" y2="1248" x1="4336" />
            <wire x2="4816" y1="1248" y2="1296" x1="4816" />
            <wire x2="4800" y1="4160" y2="4208" x1="4800" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="4720" y1="4160" y2="4176" x1="4720" />
            <wire x2="5072" y1="4176" y2="4176" x1="4720" />
            <wire x2="5072" y1="1216" y2="4176" x1="5072" />
            <wire x2="5472" y1="1216" y2="1216" x1="5072" />
            <wire x2="5472" y1="1216" y2="1280" x1="5472" />
        </branch>
        <iomarker fontsize="28" x="384" y="976" name="C0" orien="R180" />
        <instance x="1424" y="4368" name="XLXI_18" orien="R90" />
        <instance x="1632" y="4384" name="XLXI_19" orien="R90" />
        <branch name="XLXN_56">
            <wire x2="1552" y1="4272" y2="4368" x1="1552" />
            <wire x2="1744" y1="4272" y2="4272" x1="1552" />
            <wire x2="1744" y1="4176" y2="4272" x1="1744" />
        </branch>
        <branch name="XLXN_57">
            <wire x2="1408" y1="4304" y2="4688" x1="1408" />
            <wire x2="1520" y1="4688" y2="4688" x1="1408" />
            <wire x2="1696" y1="4304" y2="4304" x1="1408" />
            <wire x2="1696" y1="4304" y2="4384" x1="1696" />
            <wire x2="1520" y1="4624" y2="4688" x1="1520" />
        </branch>
        <branch name="XLXN_58">
            <wire x2="1664" y1="4176" y2="4288" x1="1664" />
            <wire x2="1760" y1="4288" y2="4288" x1="1664" />
            <wire x2="1760" y1="4288" y2="4384" x1="1760" />
        </branch>
        <instance x="4576" y="4400" name="XLXI_20" orien="R90" />
        <instance x="4368" y="4384" name="XLXI_21" orien="R90" />
        <branch name="XLXN_60">
            <wire x2="4496" y1="4272" y2="4384" x1="4496" />
            <wire x2="4640" y1="4272" y2="4272" x1="4496" />
            <wire x2="4640" y1="4160" y2="4272" x1="4640" />
        </branch>
        <branch name="XLXN_61">
            <wire x2="4368" y1="4368" y2="4720" x1="4368" />
            <wire x2="4464" y1="4720" y2="4720" x1="4368" />
            <wire x2="4704" y1="4368" y2="4368" x1="4368" />
            <wire x2="4704" y1="4368" y2="4400" x1="4704" />
            <wire x2="4464" y1="4640" y2="4720" x1="4464" />
        </branch>
        <branch name="XLXN_62">
            <wire x2="4560" y1="4160" y2="4288" x1="4560" />
            <wire x2="4640" y1="4288" y2="4288" x1="4560" />
            <wire x2="4640" y1="4288" y2="4400" x1="4640" />
        </branch>
        <branch name="Co">
            <wire x2="4672" y1="4656" y2="4688" x1="4672" />
        </branch>
        <iomarker fontsize="28" x="4672" y="4688" name="Co" orien="R90" />
        <branch name="XLXN_64">
            <wire x2="1728" y1="4640" y2="4704" x1="1728" />
            <wire x2="1792" y1="4704" y2="4704" x1="1728" />
            <wire x2="1792" y1="4304" y2="4704" x1="1792" />
            <wire x2="4432" y1="4304" y2="4304" x1="1792" />
            <wire x2="4432" y1="4304" y2="4384" x1="4432" />
            <wire x2="5056" y1="4304" y2="4304" x1="4432" />
            <wire x2="3520" y1="1184" y2="1264" x1="3520" />
            <wire x2="4448" y1="1184" y2="1184" x1="3520" />
            <wire x2="4448" y1="1184" y2="3760" x1="4448" />
            <wire x2="4448" y1="3760" y2="3776" x1="4448" />
            <wire x2="5056" y1="3760" y2="3760" x1="4448" />
            <wire x2="5056" y1="3760" y2="4304" x1="5056" />
        </branch>
    </sheet>
</drawing>