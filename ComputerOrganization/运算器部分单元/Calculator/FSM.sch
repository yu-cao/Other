<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clk" />
        <signal name="Qa" />
        <signal name="XLXN_8" />
        <signal name="Qb" />
        <signal name="nQb" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="Qc" />
        <signal name="XLXN_18" />
        <signal name="nQd" />
        <signal name="XLXN_20" />
        <signal name="Qd" />
        <signal name="nQa" />
        <signal name="nQc" />
        <signal name="Rc" />
        <signal name="XLXN_21" />
        <signal name="XLXN_24" />
        <signal name="XLXN_25" />
        <signal name="XLXN_26" />
        <port polarity="Input" name="clk" />
        <port polarity="Output" name="Qa" />
        <port polarity="Output" name="Qb" />
        <port polarity="Output" name="Qc" />
        <port polarity="Output" name="Qd" />
        <port polarity="Output" name="Rc" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="nor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="216" y1="-96" y2="-96" x1="256" />
            <circle r="12" cx="204" cy="-96" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
        </blockdef>
        <blockdef name="nor3">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="72" y1="-128" y2="-128" x1="0" />
            <line x2="48" y1="-192" y2="-192" x1="0" />
            <line x2="216" y1="-128" y2="-128" x1="256" />
            <circle r="12" cx="204" cy="-128" />
            <line x2="48" y1="-64" y2="-80" x1="48" />
            <line x2="48" y1="-192" y2="-176" x1="48" />
            <line x2="48" y1="-80" y2="-80" x1="112" />
            <line x2="48" y1="-176" y2="-176" x1="112" />
            <arc ex="48" ey="-176" sx="48" sy="-80" r="56" cx="16" cy="-128" />
            <arc ex="192" ey="-128" sx="112" sy="-80" r="88" cx="116" cy="-168" />
            <arc ex="112" ey="-176" sx="192" sy="-128" r="88" cx="116" cy="-88" />
        </blockdef>
        <blockdef name="nor4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="216" y1="-160" y2="-160" x1="256" />
            <circle r="12" cx="204" cy="-160" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="xnor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
            <circle r="8" cx="220" cy="-96" />
            <line x2="256" y1="-96" y2="-96" x1="228" />
            <line x2="60" y1="-28" y2="-28" x1="60" />
        </blockdef>
        <blockdef name="fd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <rect width="256" x="64" y="-320" height="256" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
        </blockdef>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="Qa" name="I" />
            <blockpin signalname="nQa" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_10">
            <blockpin signalname="Qb" name="I" />
            <blockpin signalname="nQb" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_11">
            <blockpin signalname="Qc" name="I" />
            <blockpin signalname="nQc" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_12">
            <blockpin signalname="Qd" name="I" />
            <blockpin signalname="nQd" name="O" />
        </block>
        <block symbolname="nor2" name="XLXI_13">
            <blockpin signalname="nQa" name="I0" />
            <blockpin signalname="nQb" name="I1" />
            <blockpin signalname="XLXN_15" name="O" />
        </block>
        <block symbolname="nor3" name="XLXI_14">
            <blockpin signalname="nQa" name="I0" />
            <blockpin signalname="nQb" name="I1" />
            <blockpin signalname="nQc" name="I2" />
            <blockpin signalname="XLXN_18" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_16">
            <blockpin signalname="nQb" name="I0" />
            <blockpin signalname="Qa" name="I1" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_17">
            <blockpin signalname="nQc" name="I0" />
            <blockpin signalname="XLXN_15" name="I1" />
            <blockpin signalname="XLXN_24" name="O" />
        </block>
        <block symbolname="xnor2" name="XLXI_18">
            <blockpin signalname="nQd" name="I0" />
            <blockpin signalname="XLXN_18" name="I1" />
            <blockpin signalname="XLXN_25" name="O" />
        </block>
        <block symbolname="nor4" name="XLXI_19">
            <blockpin signalname="nQa" name="I0" />
            <blockpin signalname="nQb" name="I1" />
            <blockpin signalname="nQc" name="I2" />
            <blockpin signalname="nQd" name="I3" />
            <blockpin signalname="Rc" name="O" />
        </block>
        <block symbolname="fd" name="XLXI_20">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="nQa" name="D" />
            <blockpin signalname="Qa" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_25">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_21" name="D" />
            <blockpin signalname="Qb" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_26">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_24" name="D" />
            <blockpin signalname="Qc" name="Q" />
        </block>
        <block symbolname="fd" name="XLXI_27">
            <blockpin signalname="clk" name="C" />
            <blockpin signalname="XLXN_25" name="D" />
            <blockpin signalname="Qd" name="Q" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="clk">
            <wire x2="1968" y1="704" y2="704" x1="1776" />
            <wire x2="1968" y1="704" y2="768" x1="1968" />
            <wire x2="1968" y1="768" y2="1040" x1="1968" />
            <wire x2="1984" y1="1040" y2="1040" x1="1968" />
            <wire x2="1968" y1="1040" y2="1392" x1="1968" />
            <wire x2="2000" y1="1392" y2="1392" x1="1968" />
            <wire x2="1968" y1="1392" y2="1696" x1="1968" />
            <wire x2="2000" y1="1696" y2="1696" x1="1968" />
            <wire x2="1984" y1="768" y2="768" x1="1968" />
        </branch>
        <iomarker fontsize="28" x="1776" y="704" name="clk" orien="R180" />
        <instance x="1776" y="1072" name="XLXI_10" orien="R180" />
        <instance x="1760" y="1440" name="XLXI_11" orien="R180" />
        <instance x="1760" y="1792" name="XLXI_12" orien="R180" />
        <instance x="864" y="1280" name="XLXI_13" orien="R0" />
        <instance x="864" y="1728" name="XLXI_14" orien="R0" />
        <instance x="1456" y="976" name="XLXI_16" orien="R0" />
        <instance x="1536" y="1344" name="XLXI_17" orien="R0" />
        <instance x="1536" y="1696" name="XLXI_18" orien="R0" />
        <instance x="1328" y="768" name="XLXI_9" orien="R180" />
        <branch name="Qa">
            <wire x2="1408" y1="800" y2="800" x1="1328" />
            <wire x2="2384" y1="800" y2="800" x1="1408" />
            <wire x2="1408" y1="800" y2="848" x1="1408" />
            <wire x2="1456" y1="848" y2="848" x1="1408" />
            <wire x2="2384" y1="640" y2="640" x1="2368" />
            <wire x2="2384" y1="640" y2="800" x1="2384" />
            <wire x2="2432" y1="640" y2="640" x1="2384" />
        </branch>
        <branch name="nQb">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="848" y="1104" type="branch" />
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1272" y="1104" type="branch" />
            <wire x2="848" y1="1104" y2="1152" x1="848" />
            <wire x2="864" y1="1152" y2="1152" x1="848" />
            <wire x2="848" y1="1152" y2="1600" x1="848" />
            <wire x2="864" y1="1600" y2="1600" x1="848" />
            <wire x2="848" y1="1600" y2="2160" x1="848" />
            <wire x2="976" y1="2160" y2="2160" x1="848" />
            <wire x2="1440" y1="1104" y2="1104" x1="848" />
            <wire x2="1552" y1="1104" y2="1104" x1="1440" />
            <wire x2="1456" y1="912" y2="912" x1="1440" />
            <wire x2="1440" y1="912" y2="1104" x1="1440" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1328" y1="1184" y2="1184" x1="1120" />
            <wire x2="1328" y1="1184" y2="1216" x1="1328" />
            <wire x2="1536" y1="1216" y2="1216" x1="1328" />
        </branch>
        <branch name="XLXN_18">
            <wire x2="1328" y1="1600" y2="1600" x1="1120" />
            <wire x2="1328" y1="1568" y2="1600" x1="1328" />
            <wire x2="1536" y1="1568" y2="1568" x1="1328" />
        </branch>
        <branch name="nQd">
            <wire x2="1520" y1="1760" y2="1760" x1="880" />
            <wire x2="1520" y1="1760" y2="1824" x1="1520" />
            <wire x2="1536" y1="1824" y2="1824" x1="1520" />
            <wire x2="880" y1="1760" y2="2032" x1="880" />
            <wire x2="976" y1="2032" y2="2032" x1="880" />
            <wire x2="1536" y1="1632" y2="1632" x1="1520" />
            <wire x2="1520" y1="1632" y2="1760" x1="1520" />
        </branch>
        <branch name="Qd">
            <wire x2="2464" y1="1824" y2="1824" x1="1760" />
            <wire x2="2400" y1="1568" y2="1568" x1="2384" />
            <wire x2="2464" y1="1568" y2="1568" x1="2400" />
            <wire x2="2464" y1="1568" y2="1632" x1="2464" />
            <wire x2="2464" y1="1632" y2="1824" x1="2464" />
            <wire x2="2560" y1="1632" y2="1632" x1="2464" />
        </branch>
        <branch name="nQa">
            <wire x2="816" y1="640" y2="800" x1="816" />
            <wire x2="816" y1="800" y2="1216" x1="816" />
            <wire x2="864" y1="1216" y2="1216" x1="816" />
            <wire x2="816" y1="1216" y2="1664" x1="816" />
            <wire x2="864" y1="1664" y2="1664" x1="816" />
            <wire x2="816" y1="1664" y2="2224" x1="816" />
            <wire x2="976" y1="2224" y2="2224" x1="816" />
            <wire x2="1104" y1="800" y2="800" x1="816" />
            <wire x2="1984" y1="640" y2="640" x1="816" />
        </branch>
        <instance x="976" y="2288" name="XLXI_19" orien="R0" />
        <branch name="nQc">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="864" y="1376" type="branch" />
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1480" y="1376" type="branch" />
            <wire x2="864" y1="1376" y2="1536" x1="864" />
            <wire x2="1200" y1="1376" y2="1376" x1="864" />
            <wire x2="1200" y1="1376" y2="1968" x1="1200" />
            <wire x2="1520" y1="1376" y2="1376" x1="1200" />
            <wire x2="1520" y1="1376" y2="1472" x1="1520" />
            <wire x2="1536" y1="1472" y2="1472" x1="1520" />
            <wire x2="896" y1="1968" y2="2096" x1="896" />
            <wire x2="976" y1="2096" y2="2096" x1="896" />
            <wire x2="1200" y1="1968" y2="1968" x1="896" />
            <wire x2="1536" y1="1280" y2="1280" x1="1520" />
            <wire x2="1520" y1="1280" y2="1376" x1="1520" />
        </branch>
        <branch name="Qc">
            <wire x2="2464" y1="1472" y2="1472" x1="1760" />
            <wire x2="2464" y1="1264" y2="1264" x1="2384" />
            <wire x2="2464" y1="1264" y2="1328" x1="2464" />
            <wire x2="2464" y1="1328" y2="1472" x1="2464" />
            <wire x2="2528" y1="1328" y2="1328" x1="2464" />
        </branch>
        <branch name="Qb">
            <wire x2="2448" y1="1104" y2="1104" x1="1776" />
            <wire x2="2448" y1="912" y2="912" x1="2368" />
            <wire x2="2448" y1="912" y2="976" x1="2448" />
            <wire x2="2512" y1="976" y2="976" x1="2448" />
            <wire x2="2448" y1="976" y2="1056" x1="2448" />
            <wire x2="2448" y1="1056" y2="1104" x1="2448" />
        </branch>
        <iomarker fontsize="28" x="2432" y="640" name="Qa" orien="R0" />
        <iomarker fontsize="28" x="2512" y="976" name="Qb" orien="R0" />
        <iomarker fontsize="28" x="2528" y="1328" name="Qc" orien="R0" />
        <iomarker fontsize="28" x="2560" y="1632" name="Qd" orien="R0" />
        <branch name="Rc">
            <wire x2="1264" y1="2128" y2="2128" x1="1232" />
        </branch>
        <iomarker fontsize="28" x="1264" y="2128" name="Rc" orien="R0" />
        <instance x="1984" y="896" name="XLXI_20" orien="R0" />
        <instance x="1984" y="1168" name="XLXI_25" orien="R0" />
        <instance x="2000" y="1520" name="XLXI_26" orien="R0" />
        <instance x="2000" y="1824" name="XLXI_27" orien="R0" />
        <branch name="XLXN_21">
            <wire x2="1840" y1="880" y2="880" x1="1712" />
            <wire x2="1840" y1="880" y2="976" x1="1840" />
            <wire x2="1920" y1="976" y2="976" x1="1840" />
            <wire x2="1984" y1="912" y2="912" x1="1920" />
            <wire x2="1920" y1="912" y2="976" x1="1920" />
        </branch>
        <branch name="XLXN_24">
            <wire x2="1888" y1="1248" y2="1248" x1="1792" />
            <wire x2="1888" y1="1248" y2="1264" x1="1888" />
            <wire x2="2000" y1="1264" y2="1264" x1="1888" />
        </branch>
        <branch name="XLXN_25">
            <wire x2="1888" y1="1600" y2="1600" x1="1792" />
            <wire x2="2000" y1="1568" y2="1568" x1="1888" />
            <wire x2="1888" y1="1568" y2="1600" x1="1888" />
        </branch>
    </sheet>
</drawing>