//左移位，数字从低位向高位移动了1bit，最右边插入的数字为0
//当S=1时发生移位
module sll1b(A, S, Z);
    parameter N = 32;

	//port definitions
	input wire [(N-1):0] A;
	input wire S;
    output wire [(N-1):0] Z;
    wire [(N-1):0] B;

    assign B[0] = 1'b0;//直接在低位置0
    assign B[(N-1):1] = A[N-2:0];
    
	mux_2to1 m1 (.X(A), .Y(B), .S(S), .Z(Z));

endmodule
