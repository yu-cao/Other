<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(2:0)" />
        <signal name="I0(31:0)" />
        <signal name="I0(7:0)" />
        <signal name="I0(15:8)" />
        <signal name="I0(23:16)" />
        <signal name="I0(31:24)" />
        <signal name="I1(31:0)" />
        <signal name="I1(7:0)" />
        <signal name="I1(15:8)" />
        <signal name="I1(23:16)" />
        <signal name="I1(31:24)" />
        <signal name="I2(31:0)" />
        <signal name="I2(7:0)" />
        <signal name="I2(15:8)" />
        <signal name="I2(23:16)" />
        <signal name="I2(31:24)" />
        <signal name="I4(31:0)" />
        <signal name="I4(7:0)" />
        <signal name="I4(15:8)" />
        <signal name="I4(23:16)" />
        <signal name="I4(31:24)" />
        <signal name="I5(31:0)" />
        <signal name="I5(7:0)" />
        <signal name="I5(15:8)" />
        <signal name="I5(23:16)" />
        <signal name="I5(31:24)" />
        <signal name="I6(31:0)" />
        <signal name="I6(7:0)" />
        <signal name="I6(15:8)" />
        <signal name="I6(23:16)" />
        <signal name="I6(31:24)" />
        <signal name="I7(31:0)" />
        <signal name="I7(7:0)" />
        <signal name="I7(15:8)" />
        <signal name="I7(23:16)" />
        <signal name="I7(31:24)" />
        <signal name="I3(31:0)" />
        <signal name="I3(7:0)" />
        <signal name="I3(15:8)" />
        <signal name="I3(23:16)" />
        <signal name="I3(31:24)" />
        <signal name="O(31:0)" />
        <signal name="O(7:0)" />
        <signal name="O(15:8)" />
        <signal name="O(23:16)" />
        <signal name="O(31:24)" />
        <port polarity="Input" name="s(2:0)" />
        <port polarity="Input" name="I0(31:0)" />
        <port polarity="Input" name="I1(31:0)" />
        <port polarity="Input" name="I2(31:0)" />
        <port polarity="Input" name="I4(31:0)" />
        <port polarity="Input" name="I5(31:0)" />
        <port polarity="Input" name="I6(31:0)" />
        <port polarity="Input" name="I7(31:0)" />
        <port polarity="Input" name="I3(31:0)" />
        <port polarity="Output" name="O(31:0)" />
        <blockdef name="MUX8T1_8">
            <timestamp>2017-11-22T7:6:30</timestamp>
            <rect width="256" x="64" y="-576" height="576" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-556" height="24" />
            <line x2="384" y1="-544" y2="-544" x1="320" />
        </blockdef>
        <block symbolname="MUX8T1_8" name="XLXI_7">
            <blockpin signalname="s(2:0)" name="s(2:0)" />
            <blockpin signalname="I0(7:0)" name="I0(7:0)" />
            <blockpin signalname="I1(7:0)" name="I1(7:0)" />
            <blockpin signalname="I2(7:0)" name="I2(7:0)" />
            <blockpin signalname="I4(7:0)" name="I4(7:0)" />
            <blockpin signalname="I5(7:0)" name="I5(7:0)" />
            <blockpin signalname="I6(7:0)" name="I6(7:0)" />
            <blockpin signalname="I7(7:0)" name="I7(7:0)" />
            <blockpin signalname="I3(7:0)" name="I3(7:0)" />
            <blockpin signalname="O(7:0)" name="O(7:0)" />
        </block>
        <block symbolname="MUX8T1_8" name="XLXI_8">
            <blockpin signalname="s(2:0)" name="s(2:0)" />
            <blockpin signalname="I0(15:8)" name="I0(7:0)" />
            <blockpin signalname="I1(15:8)" name="I1(7:0)" />
            <blockpin signalname="I2(15:8)" name="I2(7:0)" />
            <blockpin signalname="I4(15:8)" name="I4(7:0)" />
            <blockpin signalname="I5(15:8)" name="I5(7:0)" />
            <blockpin signalname="I6(15:8)" name="I6(7:0)" />
            <blockpin signalname="I7(15:8)" name="I7(7:0)" />
            <blockpin signalname="I3(15:8)" name="I3(7:0)" />
            <blockpin signalname="O(15:8)" name="O(7:0)" />
        </block>
        <block symbolname="MUX8T1_8" name="XLXI_9">
            <blockpin signalname="s(2:0)" name="s(2:0)" />
            <blockpin signalname="I0(23:16)" name="I0(7:0)" />
            <blockpin signalname="I1(23:16)" name="I1(7:0)" />
            <blockpin signalname="I2(23:16)" name="I2(7:0)" />
            <blockpin signalname="I4(23:16)" name="I4(7:0)" />
            <blockpin signalname="I5(23:16)" name="I5(7:0)" />
            <blockpin signalname="I6(23:16)" name="I6(7:0)" />
            <blockpin signalname="I7(23:16)" name="I7(7:0)" />
            <blockpin signalname="I3(23:16)" name="I3(7:0)" />
            <blockpin signalname="O(23:16)" name="O(7:0)" />
        </block>
        <block symbolname="MUX8T1_8" name="XLXI_10">
            <blockpin signalname="s(2:0)" name="s(2:0)" />
            <blockpin signalname="I0(31:24)" name="I0(7:0)" />
            <blockpin signalname="I1(31:24)" name="I1(7:0)" />
            <blockpin signalname="I2(31:24)" name="I2(7:0)" />
            <blockpin signalname="I4(31:24)" name="I4(7:0)" />
            <blockpin signalname="I5(31:24)" name="I5(7:0)" />
            <blockpin signalname="I6(31:24)" name="I6(7:0)" />
            <blockpin signalname="I7(31:24)" name="I7(7:0)" />
            <blockpin signalname="I3(31:24)" name="I3(7:0)" />
            <blockpin signalname="O(31:24)" name="O(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <branch name="s(2:0)">
            <wire x2="512" y1="544" y2="544" x1="352" />
            <wire x2="512" y1="544" y2="1472" x1="512" />
            <wire x2="624" y1="1472" y2="1472" x1="512" />
            <wire x2="1664" y1="544" y2="544" x1="512" />
            <wire x2="1664" y1="544" y2="1472" x1="1664" />
            <wire x2="1712" y1="1472" y2="1472" x1="1664" />
            <wire x2="2704" y1="544" y2="544" x1="1664" />
            <wire x2="3824" y1="544" y2="544" x1="2704" />
            <wire x2="3824" y1="544" y2="1472" x1="3824" />
            <wire x2="3840" y1="1472" y2="1472" x1="3824" />
            <wire x2="2704" y1="544" y2="1472" x1="2704" />
            <wire x2="2784" y1="1472" y2="1472" x1="2704" />
        </branch>
        <iomarker fontsize="28" x="352" y="544" name="s(2:0)" orien="R180" />
        <branch name="I0(31:0)">
            <wire x2="560" y1="704" y2="704" x1="352" />
            <wire x2="1632" y1="704" y2="704" x1="560" />
            <wire x2="2672" y1="704" y2="704" x1="1632" />
            <wire x2="3728" y1="704" y2="704" x1="2672" />
            <wire x2="3760" y1="704" y2="704" x1="3728" />
        </branch>
        <iomarker fontsize="28" x="352" y="704" name="I0(31:0)" orien="R180" />
        <bustap x2="560" y1="704" y2="800" x1="560" />
        <bustap x2="1632" y1="704" y2="800" x1="1632" />
        <bustap x2="2672" y1="704" y2="800" x1="2672" />
        <bustap x2="3728" y1="704" y2="800" x1="3728" />
        <branch name="I0(7:0)">
            <wire x2="560" y1="800" y2="1536" x1="560" />
            <wire x2="624" y1="1536" y2="1536" x1="560" />
        </branch>
        <branch name="I0(15:8)">
            <wire x2="1632" y1="800" y2="1536" x1="1632" />
            <wire x2="1712" y1="1536" y2="1536" x1="1632" />
        </branch>
        <branch name="I0(23:16)">
            <wire x2="2672" y1="800" y2="1536" x1="2672" />
            <wire x2="2784" y1="1536" y2="1536" x1="2672" />
        </branch>
        <branch name="I0(31:24)">
            <wire x2="3728" y1="800" y2="1536" x1="3728" />
            <wire x2="3840" y1="1536" y2="1536" x1="3728" />
        </branch>
        <branch name="I1(31:0)">
            <wire x2="544" y1="768" y2="768" x1="352" />
            <wire x2="1600" y1="768" y2="768" x1="544" />
            <wire x2="2640" y1="768" y2="768" x1="1600" />
            <wire x2="3632" y1="768" y2="768" x1="2640" />
            <wire x2="3664" y1="768" y2="768" x1="3632" />
        </branch>
        <iomarker fontsize="28" x="352" y="768" name="I1(31:0)" orien="R180" />
        <bustap x2="544" y1="768" y2="864" x1="544" />
        <bustap x2="1600" y1="768" y2="864" x1="1600" />
        <bustap x2="2640" y1="768" y2="864" x1="2640" />
        <bustap x2="3632" y1="768" y2="864" x1="3632" />
        <branch name="I1(7:0)">
            <wire x2="544" y1="864" y2="1600" x1="544" />
            <wire x2="624" y1="1600" y2="1600" x1="544" />
        </branch>
        <branch name="I1(15:8)">
            <wire x2="1600" y1="864" y2="1600" x1="1600" />
            <wire x2="1712" y1="1600" y2="1600" x1="1600" />
        </branch>
        <branch name="I1(23:16)">
            <wire x2="2640" y1="864" y2="1600" x1="2640" />
            <wire x2="2784" y1="1600" y2="1600" x1="2640" />
        </branch>
        <branch name="I1(31:24)">
            <wire x2="3632" y1="864" y2="1600" x1="3632" />
            <wire x2="3840" y1="1600" y2="1600" x1="3632" />
        </branch>
        <branch name="I2(31:0)">
            <wire x2="480" y1="832" y2="832" x1="352" />
            <wire x2="1552" y1="832" y2="832" x1="480" />
            <wire x2="2608" y1="832" y2="832" x1="1552" />
            <wire x2="3584" y1="832" y2="832" x1="2608" />
            <wire x2="3600" y1="832" y2="832" x1="3584" />
        </branch>
        <iomarker fontsize="28" x="352" y="832" name="I2(31:0)" orien="R180" />
        <bustap x2="480" y1="832" y2="928" x1="480" />
        <bustap x2="1552" y1="832" y2="928" x1="1552" />
        <bustap x2="2608" y1="832" y2="928" x1="2608" />
        <bustap x2="3584" y1="832" y2="928" x1="3584" />
        <branch name="I2(7:0)">
            <wire x2="480" y1="928" y2="1664" x1="480" />
            <wire x2="624" y1="1664" y2="1664" x1="480" />
        </branch>
        <branch name="I2(15:8)">
            <wire x2="1552" y1="928" y2="1664" x1="1552" />
            <wire x2="1712" y1="1664" y2="1664" x1="1552" />
        </branch>
        <branch name="I2(23:16)">
            <wire x2="2608" y1="928" y2="1664" x1="2608" />
            <wire x2="2784" y1="1664" y2="1664" x1="2608" />
        </branch>
        <branch name="I2(31:24)">
            <wire x2="3584" y1="928" y2="1664" x1="3584" />
            <wire x2="3840" y1="1664" y2="1664" x1="3584" />
        </branch>
        <branch name="I4(31:0)">
            <wire x2="432" y1="912" y2="912" x1="352" />
            <wire x2="1504" y1="912" y2="912" x1="432" />
            <wire x2="2576" y1="912" y2="912" x1="1504" />
            <wire x2="3536" y1="912" y2="912" x1="2576" />
            <wire x2="3552" y1="912" y2="912" x1="3536" />
        </branch>
        <iomarker fontsize="28" x="352" y="912" name="I4(31:0)" orien="R180" />
        <bustap x2="432" y1="912" y2="1008" x1="432" />
        <bustap x2="1504" y1="912" y2="1008" x1="1504" />
        <bustap x2="2576" y1="912" y2="1008" x1="2576" />
        <bustap x2="3536" y1="912" y2="1008" x1="3536" />
        <branch name="I4(7:0)">
            <wire x2="432" y1="1008" y2="1728" x1="432" />
            <wire x2="624" y1="1728" y2="1728" x1="432" />
        </branch>
        <branch name="I4(15:8)">
            <wire x2="1504" y1="1008" y2="1728" x1="1504" />
            <wire x2="1712" y1="1728" y2="1728" x1="1504" />
        </branch>
        <branch name="I4(23:16)">
            <wire x2="2576" y1="1008" y2="1728" x1="2576" />
            <wire x2="2784" y1="1728" y2="1728" x1="2576" />
        </branch>
        <branch name="I4(31:24)">
            <wire x2="3536" y1="1008" y2="1728" x1="3536" />
            <wire x2="3840" y1="1728" y2="1728" x1="3536" />
        </branch>
        <branch name="I5(31:0)">
            <wire x2="576" y1="2304" y2="2304" x1="336" />
            <wire x2="1664" y1="2304" y2="2304" x1="576" />
            <wire x2="2736" y1="2304" y2="2304" x1="1664" />
            <wire x2="3792" y1="2304" y2="2304" x1="2736" />
            <wire x2="3856" y1="2304" y2="2304" x1="3792" />
        </branch>
        <iomarker fontsize="28" x="336" y="2304" name="I5(31:0)" orien="R180" />
        <bustap x2="576" y1="2304" y2="2208" x1="576" />
        <bustap x2="1664" y1="2304" y2="2208" x1="1664" />
        <bustap x2="2736" y1="2304" y2="2208" x1="2736" />
        <bustap x2="3792" y1="2304" y2="2208" x1="3792" />
        <branch name="I5(7:0)">
            <wire x2="624" y1="1792" y2="1792" x1="576" />
            <wire x2="576" y1="1792" y2="2208" x1="576" />
        </branch>
        <branch name="I5(15:8)">
            <wire x2="1712" y1="1792" y2="1792" x1="1664" />
            <wire x2="1664" y1="1792" y2="2208" x1="1664" />
        </branch>
        <branch name="I5(23:16)">
            <wire x2="2784" y1="1792" y2="1792" x1="2736" />
            <wire x2="2736" y1="1792" y2="2208" x1="2736" />
        </branch>
        <branch name="I5(31:24)">
            <wire x2="3840" y1="1792" y2="1792" x1="3792" />
            <wire x2="3792" y1="1792" y2="2208" x1="3792" />
        </branch>
        <branch name="I6(31:0)">
            <wire x2="528" y1="2416" y2="2416" x1="336" />
            <wire x2="1616" y1="2416" y2="2416" x1="528" />
            <wire x2="2688" y1="2416" y2="2416" x1="1616" />
            <wire x2="3744" y1="2416" y2="2416" x1="2688" />
            <wire x2="3872" y1="2416" y2="2416" x1="3744" />
        </branch>
        <iomarker fontsize="28" x="336" y="2416" name="I6(31:0)" orien="R180" />
        <bustap x2="528" y1="2416" y2="2320" x1="528" />
        <bustap x2="1616" y1="2416" y2="2320" x1="1616" />
        <bustap x2="2688" y1="2416" y2="2320" x1="2688" />
        <bustap x2="3744" y1="2416" y2="2320" x1="3744" />
        <branch name="I6(7:0)">
            <wire x2="624" y1="1856" y2="1856" x1="528" />
            <wire x2="528" y1="1856" y2="2320" x1="528" />
        </branch>
        <branch name="I6(15:8)">
            <wire x2="1712" y1="1856" y2="1856" x1="1616" />
            <wire x2="1616" y1="1856" y2="2320" x1="1616" />
        </branch>
        <branch name="I6(23:16)">
            <wire x2="2784" y1="1856" y2="1856" x1="2688" />
            <wire x2="2688" y1="1856" y2="2320" x1="2688" />
        </branch>
        <branch name="I6(31:24)">
            <wire x2="3840" y1="1856" y2="1856" x1="3744" />
            <wire x2="3744" y1="1856" y2="2320" x1="3744" />
        </branch>
        <branch name="I7(31:0)">
            <wire x2="480" y1="2544" y2="2544" x1="352" />
            <wire x2="1568" y1="2544" y2="2544" x1="480" />
            <wire x2="2640" y1="2544" y2="2544" x1="1568" />
            <wire x2="2656" y1="2544" y2="2544" x1="2640" />
            <wire x2="3712" y1="2544" y2="2544" x1="2656" />
            <wire x2="3888" y1="2544" y2="2544" x1="3712" />
        </branch>
        <iomarker fontsize="28" x="352" y="2544" name="I7(31:0)" orien="R180" />
        <bustap x2="480" y1="2544" y2="2448" x1="480" />
        <bustap x2="1568" y1="2544" y2="2448" x1="1568" />
        <bustap x2="2656" y1="2544" y2="2448" x1="2656" />
        <bustap x2="3712" y1="2544" y2="2448" x1="3712" />
        <branch name="I7(7:0)">
            <wire x2="624" y1="1920" y2="1920" x1="480" />
            <wire x2="480" y1="1920" y2="2448" x1="480" />
        </branch>
        <branch name="I7(15:8)">
            <wire x2="1712" y1="1920" y2="1920" x1="1568" />
            <wire x2="1568" y1="1920" y2="2448" x1="1568" />
        </branch>
        <branch name="I7(23:16)">
            <wire x2="2784" y1="1920" y2="1920" x1="2656" />
            <wire x2="2656" y1="1920" y2="2448" x1="2656" />
        </branch>
        <branch name="I7(31:24)">
            <wire x2="3840" y1="1920" y2="1920" x1="3712" />
            <wire x2="3712" y1="1920" y2="2448" x1="3712" />
        </branch>
        <branch name="I3(31:0)">
            <wire x2="432" y1="2672" y2="2672" x1="336" />
            <wire x2="1520" y1="2672" y2="2672" x1="432" />
            <wire x2="2624" y1="2672" y2="2672" x1="1520" />
            <wire x2="3680" y1="2672" y2="2672" x1="2624" />
            <wire x2="3888" y1="2672" y2="2672" x1="3680" />
        </branch>
        <iomarker fontsize="28" x="336" y="2672" name="I3(31:0)" orien="R180" />
        <bustap x2="432" y1="2672" y2="2576" x1="432" />
        <bustap x2="1520" y1="2672" y2="2576" x1="1520" />
        <bustap x2="2624" y1="2672" y2="2576" x1="2624" />
        <bustap x2="3680" y1="2672" y2="2576" x1="3680" />
        <branch name="I3(7:0)">
            <wire x2="624" y1="1984" y2="1984" x1="432" />
            <wire x2="432" y1="1984" y2="2576" x1="432" />
        </branch>
        <branch name="I3(15:8)">
            <wire x2="1712" y1="1984" y2="1984" x1="1520" />
            <wire x2="1520" y1="1984" y2="2576" x1="1520" />
        </branch>
        <branch name="I3(23:16)">
            <wire x2="2784" y1="1984" y2="1984" x1="2624" />
            <wire x2="2624" y1="1984" y2="2576" x1="2624" />
        </branch>
        <branch name="I3(31:24)">
            <wire x2="3840" y1="1984" y2="1984" x1="3680" />
            <wire x2="3680" y1="1984" y2="2576" x1="3680" />
        </branch>
        <branch name="O(31:0)">
            <wire x2="1008" y1="2976" y2="2976" x1="960" />
            <wire x2="2128" y1="2976" y2="2976" x1="1008" />
            <wire x2="3216" y1="2976" y2="2976" x1="2128" />
            <wire x2="4224" y1="2976" y2="2976" x1="3216" />
            <wire x2="4336" y1="2976" y2="2976" x1="4224" />
        </branch>
        <iomarker fontsize="28" x="4336" y="2976" name="O(31:0)" orien="R0" />
        <bustap x2="1008" y1="2976" y2="2880" x1="1008" />
        <bustap x2="2128" y1="2976" y2="2880" x1="2128" />
        <bustap x2="3216" y1="2976" y2="2880" x1="3216" />
        <bustap x2="4224" y1="2976" y2="2880" x1="4224" />
        <branch name="O(7:0)">
            <wire x2="1072" y1="1472" y2="1472" x1="1008" />
            <wire x2="1072" y1="1472" y2="2080" x1="1072" />
            <wire x2="1072" y1="2080" y2="2080" x1="1008" />
            <wire x2="1008" y1="2080" y2="2880" x1="1008" />
        </branch>
        <branch name="O(15:8)">
            <wire x2="2128" y1="1472" y2="1472" x1="2096" />
            <wire x2="2128" y1="1472" y2="2880" x1="2128" />
        </branch>
        <branch name="O(23:16)">
            <wire x2="3216" y1="1472" y2="1472" x1="3168" />
            <wire x2="3216" y1="1472" y2="2880" x1="3216" />
        </branch>
        <branch name="O(31:24)">
            <wire x2="4288" y1="1472" y2="1472" x1="4224" />
            <wire x2="4288" y1="1472" y2="2080" x1="4288" />
            <wire x2="4288" y1="2080" y2="2080" x1="4224" />
            <wire x2="4224" y1="2080" y2="2880" x1="4224" />
        </branch>
        <instance x="624" y="2016" name="XLXI_7" orien="R0">
        </instance>
        <instance x="1712" y="2016" name="XLXI_8" orien="R0">
        </instance>
        <instance x="2784" y="2016" name="XLXI_9" orien="R0">
        </instance>
        <instance x="3840" y="2016" name="XLXI_10" orien="R0">
        </instance>
    </sheet>
</drawing>