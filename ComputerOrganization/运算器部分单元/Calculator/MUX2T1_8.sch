<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="SW0" />
        <signal name="XLXN_3" />
        <signal name="I1(7:0)" />
        <signal name="I1(0)" />
        <signal name="I1(1)" />
        <signal name="I1(2)" />
        <signal name="I1(3)" />
        <signal name="I1(4)" />
        <signal name="I1(5)" />
        <signal name="I1(6)" />
        <signal name="I1(7)" />
        <signal name="I0(7:0)" />
        <signal name="I0(0)" />
        <signal name="I0(1)" />
        <signal name="I0(2)" />
        <signal name="I0(3)" />
        <signal name="I0(4)" />
        <signal name="I0(5)" />
        <signal name="I0(6)" />
        <signal name="I0(7)" />
        <signal name="XLXN_37" />
        <signal name="XLXN_38" />
        <signal name="XLXN_39" />
        <signal name="XLXN_40" />
        <signal name="XLXN_41" />
        <signal name="XLXN_42" />
        <signal name="XLXN_43" />
        <signal name="XLXN_45" />
        <signal name="XLXN_46" />
        <signal name="XLXN_47" />
        <signal name="XLXN_48" />
        <signal name="XLXN_49" />
        <signal name="XLXN_50" />
        <signal name="XLXN_51" />
        <signal name="XLXN_52" />
        <signal name="XLXN_53" />
        <signal name="o(7:0)" />
        <signal name="o(0)" />
        <signal name="o(1)" />
        <signal name="o(2)" />
        <signal name="o(3)" />
        <signal name="o(4)" />
        <signal name="o(5)" />
        <signal name="o(6)" />
        <signal name="o(7)" />
        <port polarity="Input" name="SW0" />
        <port polarity="Input" name="I1(7:0)" />
        <port polarity="Input" name="I0(7:0)" />
        <port polarity="Output" name="o(7:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="I0(0)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_38" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="I1(1)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_39" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="I0(1)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_40" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="I1(2)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_41" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="I0(2)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_42" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="I1(3)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_43" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="I0(3)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_45" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I1(4)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_46" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_9">
            <blockpin signalname="I0(4)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_47" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="I1(5)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_48" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="I0(5)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_49" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="I1(6)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_50" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="I0(6)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_51" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="I1(7)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_52" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_15">
            <blockpin signalname="I0(7)" name="I0" />
            <blockpin signalname="XLXN_3" name="I1" />
            <blockpin signalname="XLXN_53" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="I1(0)" name="I0" />
            <blockpin signalname="SW0" name="I1" />
            <blockpin signalname="XLXN_37" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_17">
            <blockpin signalname="XLXN_38" name="I0" />
            <blockpin signalname="XLXN_37" name="I1" />
            <blockpin signalname="o(0)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_18">
            <blockpin signalname="XLXN_40" name="I0" />
            <blockpin signalname="XLXN_39" name="I1" />
            <blockpin signalname="o(1)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_19">
            <blockpin signalname="XLXN_42" name="I0" />
            <blockpin signalname="XLXN_41" name="I1" />
            <blockpin signalname="o(2)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_20">
            <blockpin signalname="XLXN_45" name="I0" />
            <blockpin signalname="XLXN_43" name="I1" />
            <blockpin signalname="o(3)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_21">
            <blockpin signalname="XLXN_47" name="I0" />
            <blockpin signalname="XLXN_46" name="I1" />
            <blockpin signalname="o(4)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_22">
            <blockpin signalname="XLXN_49" name="I0" />
            <blockpin signalname="XLXN_48" name="I1" />
            <blockpin signalname="o(5)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_23">
            <blockpin signalname="XLXN_51" name="I0" />
            <blockpin signalname="XLXN_50" name="I1" />
            <blockpin signalname="o(6)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_24">
            <blockpin signalname="XLXN_53" name="I0" />
            <blockpin signalname="XLXN_52" name="I1" />
            <blockpin signalname="o(7)" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_25">
            <blockpin signalname="SW0" name="I" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1552" y="480" name="XLXI_2" orien="R0" />
        <instance x="1552" y="688" name="XLXI_3" orien="R0" />
        <instance x="1552" y="848" name="XLXI_4" orien="R0" />
        <instance x="1552" y="1008" name="XLXI_5" orien="R0" />
        <instance x="1552" y="1168" name="XLXI_6" orien="R0" />
        <instance x="1552" y="1344" name="XLXI_7" orien="R0" />
        <instance x="1552" y="1520" name="XLXI_8" orien="R0" />
        <instance x="1552" y="1696" name="XLXI_9" orien="R0" />
        <instance x="1552" y="1872" name="XLXI_10" orien="R0" />
        <instance x="1552" y="2032" name="XLXI_11" orien="R0" />
        <instance x="1552" y="2192" name="XLXI_12" orien="R0" />
        <instance x="1552" y="2352" name="XLXI_13" orien="R0" />
        <instance x="1552" y="2528" name="XLXI_14" orien="R0" />
        <instance x="1552" y="2688" name="XLXI_15" orien="R0" />
        <instance x="1552" y="160" name="XLXI_16" orien="R0" />
        <instance x="1552" y="304" name="XLXI_1" orien="R0" />
        <instance x="2048" y="240" name="XLXI_17" orien="R0" />
        <instance x="2048" y="560" name="XLXI_18" orien="R0" />
        <instance x="2048" y="928" name="XLXI_19" orien="R0" />
        <instance x="2048" y="1248" name="XLXI_20" orien="R0" />
        <instance x="2048" y="1584" name="XLXI_21" orien="R0" />
        <instance x="2048" y="1952" name="XLXI_22" orien="R0" />
        <instance x="2032" y="2272" name="XLXI_23" orien="R0" />
        <instance x="2032" y="2592" name="XLXI_24" orien="R0" />
        <branch name="SW0">
            <wire x2="544" y1="208" y2="208" x1="336" />
            <wire x2="1248" y1="208" y2="208" x1="544" />
            <wire x2="1248" y1="208" y2="352" x1="1248" />
            <wire x2="1552" y1="352" y2="352" x1="1248" />
            <wire x2="1248" y1="352" y2="720" x1="1248" />
            <wire x2="1552" y1="720" y2="720" x1="1248" />
            <wire x2="1248" y1="720" y2="1040" x1="1248" />
            <wire x2="1552" y1="1040" y2="1040" x1="1248" />
            <wire x2="1248" y1="1040" y2="1392" x1="1248" />
            <wire x2="1552" y1="1392" y2="1392" x1="1248" />
            <wire x2="1248" y1="1392" y2="1744" x1="1248" />
            <wire x2="1552" y1="1744" y2="1744" x1="1248" />
            <wire x2="1248" y1="1744" y2="2064" x1="1248" />
            <wire x2="1552" y1="2064" y2="2064" x1="1248" />
            <wire x2="1248" y1="2064" y2="2400" x1="1248" />
            <wire x2="1552" y1="2400" y2="2400" x1="1248" />
            <wire x2="544" y1="208" y2="288" x1="544" />
            <wire x2="800" y1="288" y2="288" x1="544" />
            <wire x2="1552" y1="32" y2="32" x1="1248" />
            <wire x2="1248" y1="32" y2="208" x1="1248" />
        </branch>
        <iomarker fontsize="28" x="336" y="208" name="SW0" orien="R180" />
        <branch name="XLXN_3">
            <wire x2="1408" y1="288" y2="288" x1="1024" />
            <wire x2="1408" y1="288" y2="560" x1="1408" />
            <wire x2="1552" y1="560" y2="560" x1="1408" />
            <wire x2="1408" y1="560" y2="880" x1="1408" />
            <wire x2="1552" y1="880" y2="880" x1="1408" />
            <wire x2="1408" y1="880" y2="1216" x1="1408" />
            <wire x2="1552" y1="1216" y2="1216" x1="1408" />
            <wire x2="1408" y1="1216" y2="1568" x1="1408" />
            <wire x2="1552" y1="1568" y2="1568" x1="1408" />
            <wire x2="1408" y1="1568" y2="1904" x1="1408" />
            <wire x2="1408" y1="1904" y2="2224" x1="1408" />
            <wire x2="1552" y1="2224" y2="2224" x1="1408" />
            <wire x2="1408" y1="2224" y2="2560" x1="1408" />
            <wire x2="1552" y1="2560" y2="2560" x1="1408" />
            <wire x2="1552" y1="1904" y2="1904" x1="1408" />
            <wire x2="1408" y1="176" y2="288" x1="1408" />
            <wire x2="1552" y1="176" y2="176" x1="1408" />
        </branch>
        <instance x="800" y="320" name="XLXI_25" orien="R0" />
        <branch name="I1(7:0)">
            <wire x2="1280" y1="896" y2="896" x1="560" />
            <wire x2="1280" y1="896" y2="1104" x1="1280" />
            <wire x2="1280" y1="1104" y2="1456" x1="1280" />
            <wire x2="1280" y1="1456" y2="1808" x1="1280" />
            <wire x2="1280" y1="1808" y2="2128" x1="1280" />
            <wire x2="1280" y1="2128" y2="2464" x1="1280" />
            <wire x2="1280" y1="2464" y2="2528" x1="1280" />
            <wire x2="1280" y1="80" y2="96" x1="1280" />
            <wire x2="1280" y1="96" y2="416" x1="1280" />
            <wire x2="1280" y1="416" y2="784" x1="1280" />
            <wire x2="1280" y1="784" y2="896" x1="1280" />
        </branch>
        <iomarker fontsize="28" x="560" y="896" name="I1(7:0)" orien="R180" />
        <bustap x2="1376" y1="96" y2="96" x1="1280" />
        <bustap x2="1376" y1="416" y2="416" x1="1280" />
        <bustap x2="1376" y1="784" y2="784" x1="1280" />
        <bustap x2="1376" y1="1104" y2="1104" x1="1280" />
        <bustap x2="1376" y1="1456" y2="1456" x1="1280" />
        <bustap x2="1376" y1="1808" y2="1808" x1="1280" />
        <bustap x2="1376" y1="2128" y2="2128" x1="1280" />
        <bustap x2="1376" y1="2464" y2="2464" x1="1280" />
        <branch name="I1(0)">
            <wire x2="1552" y1="96" y2="96" x1="1376" />
        </branch>
        <branch name="I1(1)">
            <wire x2="1552" y1="416" y2="416" x1="1376" />
        </branch>
        <branch name="I1(2)">
            <wire x2="1552" y1="784" y2="784" x1="1376" />
        </branch>
        <branch name="I1(3)">
            <wire x2="1552" y1="1104" y2="1104" x1="1376" />
        </branch>
        <branch name="I1(4)">
            <wire x2="1552" y1="1456" y2="1456" x1="1376" />
        </branch>
        <branch name="I1(5)">
            <wire x2="1552" y1="1808" y2="1808" x1="1376" />
        </branch>
        <branch name="I1(6)">
            <wire x2="1552" y1="2128" y2="2128" x1="1376" />
        </branch>
        <branch name="I1(7)">
            <wire x2="1552" y1="2464" y2="2464" x1="1376" />
        </branch>
        <branch name="I0(7:0)">
            <wire x2="1344" y1="640" y2="640" x1="560" />
            <wire x2="1344" y1="640" y2="944" x1="1344" />
            <wire x2="1344" y1="944" y2="1280" x1="1344" />
            <wire x2="1344" y1="1280" y2="1632" x1="1344" />
            <wire x2="1344" y1="1632" y2="1968" x1="1344" />
            <wire x2="1344" y1="1968" y2="2288" x1="1344" />
            <wire x2="1344" y1="2288" y2="2624" x1="1344" />
            <wire x2="1344" y1="2624" y2="2656" x1="1344" />
            <wire x2="1344" y1="144" y2="240" x1="1344" />
            <wire x2="1344" y1="240" y2="624" x1="1344" />
            <wire x2="1344" y1="624" y2="640" x1="1344" />
        </branch>
        <iomarker fontsize="28" x="560" y="640" name="I0(7:0)" orien="R180" />
        <bustap x2="1440" y1="240" y2="240" x1="1344" />
        <bustap x2="1440" y1="624" y2="624" x1="1344" />
        <bustap x2="1440" y1="944" y2="944" x1="1344" />
        <bustap x2="1440" y1="1280" y2="1280" x1="1344" />
        <bustap x2="1440" y1="1632" y2="1632" x1="1344" />
        <bustap x2="1440" y1="1968" y2="1968" x1="1344" />
        <bustap x2="1440" y1="2288" y2="2288" x1="1344" />
        <bustap x2="1440" y1="2624" y2="2624" x1="1344" />
        <branch name="I0(0)">
            <wire x2="1552" y1="240" y2="240" x1="1440" />
        </branch>
        <branch name="I0(1)">
            <wire x2="1552" y1="624" y2="624" x1="1440" />
        </branch>
        <branch name="I0(2)">
            <wire x2="1552" y1="944" y2="944" x1="1440" />
        </branch>
        <branch name="I0(3)">
            <wire x2="1552" y1="1280" y2="1280" x1="1440" />
        </branch>
        <branch name="I0(4)">
            <wire x2="1552" y1="1632" y2="1632" x1="1440" />
        </branch>
        <branch name="I0(5)">
            <wire x2="1552" y1="1968" y2="1968" x1="1440" />
        </branch>
        <branch name="I0(6)">
            <wire x2="1552" y1="2288" y2="2288" x1="1440" />
        </branch>
        <branch name="I0(7)">
            <wire x2="1552" y1="2624" y2="2624" x1="1440" />
        </branch>
        <branch name="XLXN_37">
            <wire x2="1920" y1="64" y2="64" x1="1808" />
            <wire x2="1920" y1="64" y2="112" x1="1920" />
            <wire x2="2048" y1="112" y2="112" x1="1920" />
        </branch>
        <branch name="XLXN_38">
            <wire x2="1920" y1="208" y2="208" x1="1808" />
            <wire x2="1920" y1="176" y2="208" x1="1920" />
            <wire x2="2048" y1="176" y2="176" x1="1920" />
        </branch>
        <branch name="XLXN_39">
            <wire x2="1920" y1="384" y2="384" x1="1808" />
            <wire x2="1920" y1="384" y2="432" x1="1920" />
            <wire x2="2048" y1="432" y2="432" x1="1920" />
        </branch>
        <branch name="XLXN_40">
            <wire x2="1920" y1="592" y2="592" x1="1808" />
            <wire x2="1920" y1="496" y2="592" x1="1920" />
            <wire x2="2048" y1="496" y2="496" x1="1920" />
        </branch>
        <branch name="XLXN_41">
            <wire x2="1920" y1="752" y2="752" x1="1808" />
            <wire x2="1920" y1="752" y2="800" x1="1920" />
            <wire x2="2048" y1="800" y2="800" x1="1920" />
        </branch>
        <branch name="XLXN_42">
            <wire x2="1920" y1="912" y2="912" x1="1808" />
            <wire x2="1920" y1="864" y2="912" x1="1920" />
            <wire x2="2048" y1="864" y2="864" x1="1920" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="2032" y1="1072" y2="1072" x1="1808" />
            <wire x2="2032" y1="1072" y2="1104" x1="2032" />
            <wire x2="2032" y1="1104" y2="1120" x1="2032" />
            <wire x2="2048" y1="1120" y2="1120" x1="2032" />
        </branch>
        <branch name="XLXN_45">
            <wire x2="1920" y1="1248" y2="1248" x1="1808" />
            <wire x2="1920" y1="1184" y2="1248" x1="1920" />
            <wire x2="2048" y1="1184" y2="1184" x1="1920" />
        </branch>
        <branch name="XLXN_46">
            <wire x2="1920" y1="1424" y2="1424" x1="1808" />
            <wire x2="1920" y1="1424" y2="1456" x1="1920" />
            <wire x2="2048" y1="1456" y2="1456" x1="1920" />
        </branch>
        <branch name="XLXN_47">
            <wire x2="1920" y1="1600" y2="1600" x1="1808" />
            <wire x2="1920" y1="1520" y2="1600" x1="1920" />
            <wire x2="2048" y1="1520" y2="1520" x1="1920" />
        </branch>
        <branch name="XLXN_48">
            <wire x2="1920" y1="1776" y2="1776" x1="1808" />
            <wire x2="1920" y1="1776" y2="1824" x1="1920" />
            <wire x2="2048" y1="1824" y2="1824" x1="1920" />
        </branch>
        <branch name="XLXN_49">
            <wire x2="1920" y1="1936" y2="1936" x1="1808" />
            <wire x2="1920" y1="1888" y2="1936" x1="1920" />
            <wire x2="2048" y1="1888" y2="1888" x1="1920" />
        </branch>
        <branch name="XLXN_50">
            <wire x2="1920" y1="2096" y2="2096" x1="1808" />
            <wire x2="1920" y1="2096" y2="2144" x1="1920" />
            <wire x2="2032" y1="2144" y2="2144" x1="1920" />
        </branch>
        <branch name="XLXN_51">
            <wire x2="1920" y1="2256" y2="2256" x1="1808" />
            <wire x2="1920" y1="2208" y2="2256" x1="1920" />
            <wire x2="2032" y1="2208" y2="2208" x1="1920" />
        </branch>
        <branch name="XLXN_52">
            <wire x2="1920" y1="2432" y2="2432" x1="1808" />
            <wire x2="1920" y1="2432" y2="2464" x1="1920" />
            <wire x2="2032" y1="2464" y2="2464" x1="1920" />
        </branch>
        <branch name="XLXN_53">
            <wire x2="1920" y1="2592" y2="2592" x1="1808" />
            <wire x2="1920" y1="2528" y2="2592" x1="1920" />
            <wire x2="2032" y1="2528" y2="2528" x1="1920" />
        </branch>
        <branch name="o(7:0)">
            <wire x2="2688" y1="64" y2="144" x1="2688" />
            <wire x2="2688" y1="144" y2="464" x1="2688" />
            <wire x2="2688" y1="464" y2="832" x1="2688" />
            <wire x2="2688" y1="832" y2="1120" x1="2688" />
            <wire x2="2896" y1="1120" y2="1120" x1="2688" />
            <wire x2="2688" y1="1120" y2="1152" x1="2688" />
            <wire x2="2688" y1="1152" y2="1488" x1="2688" />
            <wire x2="2688" y1="1488" y2="1856" x1="2688" />
            <wire x2="2688" y1="1856" y2="2176" x1="2688" />
            <wire x2="2688" y1="2176" y2="2496" x1="2688" />
            <wire x2="2688" y1="2496" y2="2624" x1="2688" />
        </branch>
        <iomarker fontsize="28" x="2896" y="1120" name="o(7:0)" orien="R0" />
        <bustap x2="2592" y1="144" y2="144" x1="2688" />
        <bustap x2="2592" y1="464" y2="464" x1="2688" />
        <bustap x2="2592" y1="832" y2="832" x1="2688" />
        <bustap x2="2592" y1="1152" y2="1152" x1="2688" />
        <bustap x2="2592" y1="1488" y2="1488" x1="2688" />
        <bustap x2="2592" y1="1856" y2="1856" x1="2688" />
        <bustap x2="2592" y1="2176" y2="2176" x1="2688" />
        <bustap x2="2592" y1="2496" y2="2496" x1="2688" />
        <branch name="o(0)">
            <wire x2="2592" y1="144" y2="144" x1="2304" />
        </branch>
        <branch name="o(1)">
            <wire x2="2592" y1="464" y2="464" x1="2304" />
        </branch>
        <branch name="o(2)">
            <wire x2="2592" y1="832" y2="832" x1="2304" />
        </branch>
        <branch name="o(3)">
            <wire x2="2592" y1="1152" y2="1152" x1="2304" />
        </branch>
        <branch name="o(4)">
            <wire x2="2592" y1="1488" y2="1488" x1="2304" />
        </branch>
        <branch name="o(5)">
            <wire x2="2592" y1="1856" y2="1856" x1="2304" />
        </branch>
        <branch name="o(6)">
            <wire x2="2592" y1="2176" y2="2176" x1="2288" />
        </branch>
        <branch name="o(7)">
            <wire x2="2576" y1="2496" y2="2496" x1="2288" />
            <wire x2="2592" y1="2496" y2="2496" x1="2576" />
        </branch>
    </sheet>
</drawing>