`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:58:18 04/11/2018 
// Design Name: 
// Module Name:    mux16to1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux16to1_64b( 
	input [31:0]adder,
	input [31:0]andd,
	input [31:0]orr,
	input [31:0]xorr,
	input [31:0]nott,
	input [31:0]sll,
	input [31:0]sra,
	input [31:0]srl,
	input [31:0]cir,
	input [63:0]mul_O,
	input [63:0]mul_C,
	input [31:0]div_OQ,
	input [31:0]div_OR,
	input [31:0]div_CQ,
	input [31:0]div_CR,
	
	input [3:0] select,
	output reg[63:0] mux16to1_out
    );


always@(*)
	begin
			case(select)
				
				
				4'b0001:mux16to1_out={32'b0,andd};//zq
				4'b0010:mux16to1_out={32'b0,orr};//zq
				4'b0011:mux16to1_out={32'b0,xorr};//zq
				4'b0100:mux16to1_out={32'b0,nott};//zq
				4'b0101:mux16to1_out={32'b0,sll};//zq
				4'b0110:mux16to1_out={32'b0,srl};//zq
				4'b0111:mux16to1_out={32'b0,sra};//zq
				4'b1000:mux16to1_out={32'b0,cir};//zq
				
				4'b1001:mux16to1_out=mul_O;//zq
				4'b1010:mux16to1_out=mul_C;//zq
				
				4'b1011:mux16to1_out={div_OQ,div_OR};//zq
				4'b1100:mux16to1_out={div_CQ,div_CR};//zq
				
				4'b1101:mux16to1_out={32'b0,adder};//zq

				//4'b0000:mux16to1_out={32'b0,andd};
				//4'b1110:mux16to1_out={32'b0,andd};
				//4'b1111:mux16to1_out={32'b0,andd};			
			endcase
	end

endmodule
