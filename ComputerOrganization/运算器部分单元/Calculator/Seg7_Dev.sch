<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="flash" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="Scan(2:0)" />
        <signal name="SW0" />
        <signal name="XLXN_11(7:0)" />
        <signal name="Hexo(3:0)" />
        <signal name="Hexo(0)" />
        <signal name="Hexo(1)" />
        <signal name="Hexo(2)" />
        <signal name="Hexo(3)" />
        <signal name="SEG_TXT(7:0)" />
        <signal name="SEG_TXT(0)" />
        <signal name="SEG_TXT(1)" />
        <signal name="SEG_TXT(2)" />
        <signal name="SEG_TXT(3)" />
        <signal name="SEG_TXT(4)" />
        <signal name="SEG_TXT(5)" />
        <signal name="SEG_TXT(6)" />
        <signal name="SEG_TXT(7)" />
        <signal name="SEGMENT(7:0)" />
        <signal name="G0" />
        <signal name="V5" />
        <signal name="XLXN_22" />
        <signal name="AN(3:0)" />
        <signal name="LES(7:0)" />
        <signal name="point(7:0)" />
        <signal name="Hexs(31:0)" />
        <port polarity="Input" name="flash" />
        <port polarity="Input" name="Scan(2:0)" />
        <port polarity="Input" name="SW0" />
        <port polarity="Output" name="SEGMENT(7:0)" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Input" name="LES(7:0)" />
        <port polarity="Input" name="point(7:0)" />
        <port polarity="Input" name="Hexs(31:0)" />
        <blockdef name="MC14495_ZJU">
            <timestamp>2017-11-12T14:15:55</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-400" y2="-400" x1="64" />
            <line x2="0" y1="-320" y2="-320" x1="64" />
            <line x2="0" y1="-240" y2="-240" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-80" y2="-80" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Seg_map">
            <timestamp>2017-11-22T8:0:38</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="MUX2T1_8">
            <timestamp>2017-11-13T12:32:16</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="ScanSync_2">
            <timestamp>2017-11-20T12:25:37</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <block symbolname="MC14495_ZJU" name="XLXI_2">
            <blockpin signalname="XLXN_22" name="point" />
            <blockpin signalname="Hexo(0)" name="D0" />
            <blockpin signalname="Hexo(1)" name="D1" />
            <blockpin signalname="Hexo(2)" name="D2" />
            <blockpin signalname="Hexo(3)" name="D3" />
            <blockpin signalname="XLXN_3" name="LE" />
            <blockpin signalname="SEG_TXT(0)" name="a" />
            <blockpin signalname="SEG_TXT(1)" name="b" />
            <blockpin signalname="SEG_TXT(2)" name="c" />
            <blockpin signalname="SEG_TXT(3)" name="d" />
            <blockpin signalname="SEG_TXT(4)" name="e" />
            <blockpin signalname="SEG_TXT(5)" name="f" />
            <blockpin signalname="SEG_TXT(6)" name="g" />
            <blockpin signalname="SEG_TXT(7)" name="p" />
        </block>
        <block symbolname="Seg_map" name="XLXI_6">
            <blockpin signalname="Hexs(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="Scan(2:0)" name="Scan(2:0)" />
            <blockpin signalname="XLXN_11(7:0)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_7">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="XLXN_11(7:0)" name="I1(7:0)" />
            <blockpin signalname="SEG_TXT(7:0)" name="I0(7:0)" />
            <blockpin signalname="SEGMENT(7:0)" name="o(7:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_9">
            <blockpin signalname="G0" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_8">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="XLXN_2" name="I0" />
            <blockpin signalname="flash" name="I1" />
            <blockpin signalname="XLXN_3" name="O" />
        </block>
        <block symbolname="ScanSync_2" name="XLXI_11">
            <blockpin signalname="Scan(2:0)" name="Scan(2:0)" />
            <blockpin signalname="point(7:0)" name="point(7:0)" />
            <blockpin signalname="LES(7:0)" name="LES(7:0)" />
            <blockpin signalname="Hexs(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="Hexo(3:0)" name="Hexo(3:0)" />
            <blockpin signalname="AN(3:0)" name="AN(3:0)" />
            <blockpin signalname="XLXN_22" name="p" />
            <blockpin signalname="XLXN_2" name="LE" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="768" y="1920" name="XLXI_6" orien="R0">
        </instance>
        <branch name="flash">
            <wire x2="720" y1="640" y2="640" x1="416" />
        </branch>
        <iomarker fontsize="28" x="416" y="640" name="flash" orien="R180" />
        <branch name="XLXN_2">
            <wire x2="656" y1="704" y2="928" x1="656" />
            <wire x2="1184" y1="928" y2="928" x1="656" />
            <wire x2="1184" y1="928" y2="1392" x1="1184" />
            <wire x2="720" y1="704" y2="704" x1="656" />
            <wire x2="1184" y1="1392" y2="1392" x1="1120" />
        </branch>
        <branch name="Scan(2:0)">
            <wire x2="496" y1="1200" y2="1200" x1="384" />
            <wire x2="496" y1="1200" y2="1888" x1="496" />
            <wire x2="768" y1="1888" y2="1888" x1="496" />
            <wire x2="736" y1="1200" y2="1200" x1="496" />
        </branch>
        <iomarker fontsize="28" x="384" y="1200" name="Scan(2:0)" orien="R180" />
        <iomarker fontsize="28" x="384" y="1264" name="point(7:0)" orien="R180" />
        <iomarker fontsize="28" x="384" y="1328" name="LES(7:0)" orien="R180" />
        <branch name="SW0">
            <wire x2="2080" y1="1616" y2="1616" x1="352" />
            <wire x2="2096" y1="1616" y2="1616" x1="2080" />
        </branch>
        <branch name="XLXN_11(7:0)">
            <wire x2="1376" y1="1824" y2="1824" x1="1152" />
            <wire x2="2096" y1="1680" y2="1680" x1="1376" />
            <wire x2="1376" y1="1680" y2="1824" x1="1376" />
        </branch>
        <instance x="2096" y="1776" name="XLXI_7" orien="R0">
        </instance>
        <branch name="Hexo(3:0)">
            <wire x2="1264" y1="1200" y2="1200" x1="1120" />
            <wire x2="1264" y1="592" y2="672" x1="1264" />
            <wire x2="1264" y1="672" y2="752" x1="1264" />
            <wire x2="1264" y1="752" y2="832" x1="1264" />
            <wire x2="1264" y1="832" y2="912" x1="1264" />
            <wire x2="1264" y1="912" y2="1200" x1="1264" />
        </branch>
        <bustap x2="1360" y1="672" y2="672" x1="1264" />
        <bustap x2="1360" y1="752" y2="752" x1="1264" />
        <bustap x2="1360" y1="832" y2="832" x1="1264" />
        <bustap x2="1360" y1="912" y2="912" x1="1264" />
        <branch name="Hexo(0)">
            <wire x2="1376" y1="672" y2="672" x1="1360" />
            <wire x2="1488" y1="672" y2="672" x1="1376" />
        </branch>
        <branch name="Hexo(1)">
            <wire x2="1376" y1="752" y2="752" x1="1360" />
            <wire x2="1488" y1="752" y2="752" x1="1376" />
        </branch>
        <branch name="Hexo(2)">
            <wire x2="1376" y1="832" y2="832" x1="1360" />
            <wire x2="1488" y1="832" y2="832" x1="1376" />
        </branch>
        <branch name="Hexo(3)">
            <wire x2="1376" y1="912" y2="912" x1="1360" />
            <wire x2="1488" y1="912" y2="912" x1="1376" />
        </branch>
        <branch name="SEG_TXT(7:0)">
            <wire x2="2080" y1="560" y2="592" x1="2080" />
            <wire x2="2080" y1="592" y2="656" x1="2080" />
            <wire x2="2080" y1="656" y2="720" x1="2080" />
            <wire x2="2080" y1="720" y2="784" x1="2080" />
            <wire x2="2080" y1="784" y2="848" x1="2080" />
            <wire x2="2080" y1="848" y2="912" x1="2080" />
            <wire x2="2080" y1="912" y2="976" x1="2080" />
            <wire x2="2080" y1="976" y2="1040" x1="2080" />
            <wire x2="2080" y1="1040" y2="1744" x1="2080" />
            <wire x2="2096" y1="1744" y2="1744" x1="2080" />
        </branch>
        <instance x="1488" y="1072" name="XLXI_2" orien="R0">
        </instance>
        <bustap x2="1984" y1="592" y2="592" x1="2080" />
        <bustap x2="1984" y1="656" y2="656" x1="2080" />
        <bustap x2="1984" y1="720" y2="720" x1="2080" />
        <bustap x2="1984" y1="784" y2="784" x1="2080" />
        <bustap x2="1984" y1="848" y2="848" x1="2080" />
        <bustap x2="1984" y1="912" y2="912" x1="2080" />
        <bustap x2="1984" y1="976" y2="976" x1="2080" />
        <bustap x2="1984" y1="1040" y2="1040" x1="2080" />
        <branch name="SEG_TXT(0)">
            <wire x2="1968" y1="592" y2="592" x1="1872" />
            <wire x2="1984" y1="592" y2="592" x1="1968" />
        </branch>
        <branch name="SEG_TXT(1)">
            <wire x2="1968" y1="656" y2="656" x1="1872" />
            <wire x2="1984" y1="656" y2="656" x1="1968" />
        </branch>
        <branch name="SEG_TXT(2)">
            <wire x2="1968" y1="720" y2="720" x1="1872" />
            <wire x2="1984" y1="720" y2="720" x1="1968" />
        </branch>
        <branch name="SEG_TXT(3)">
            <wire x2="1968" y1="784" y2="784" x1="1872" />
            <wire x2="1984" y1="784" y2="784" x1="1968" />
        </branch>
        <branch name="SEG_TXT(4)">
            <wire x2="1968" y1="848" y2="848" x1="1872" />
            <wire x2="1984" y1="848" y2="848" x1="1968" />
        </branch>
        <branch name="SEG_TXT(5)">
            <wire x2="1968" y1="912" y2="912" x1="1872" />
            <wire x2="1984" y1="912" y2="912" x1="1968" />
        </branch>
        <branch name="SEG_TXT(6)">
            <wire x2="1968" y1="976" y2="976" x1="1872" />
            <wire x2="1984" y1="976" y2="976" x1="1968" />
        </branch>
        <branch name="SEG_TXT(7)">
            <wire x2="1968" y1="1040" y2="1040" x1="1872" />
            <wire x2="1984" y1="1040" y2="1040" x1="1968" />
        </branch>
        <iomarker fontsize="28" x="2176" y="1328" name="AN(3:0)" orien="R0" />
        <branch name="SEGMENT(7:0)">
            <wire x2="2672" y1="1616" y2="1616" x1="2480" />
        </branch>
        <iomarker fontsize="28" x="2672" y="1616" name="SEGMENT(7:0)" orien="R0" />
        <instance x="2576" y="1008" name="XLXI_9" orien="R0" />
        <branch name="G0">
            <wire x2="2640" y1="848" y2="880" x1="2640" />
            <wire x2="2832" y1="848" y2="848" x1="2640" />
        </branch>
        <iomarker fontsize="28" x="384" y="1392" name="Hexs(31:0)" orien="R180" />
        <iomarker fontsize="28" x="352" y="1616" name="SW0" orien="R180" />
        <instance x="2576" y="608" name="XLXI_8" orien="R0" />
        <branch name="V5">
            <wire x2="2640" y1="608" y2="624" x1="2640" />
            <wire x2="2832" y1="624" y2="624" x1="2640" />
        </branch>
        <instance x="736" y="1424" name="XLXI_11" orien="R0">
        </instance>
        <branch name="XLXN_22">
            <wire x2="1392" y1="1328" y2="1328" x1="1120" />
            <wire x2="1392" y1="592" y2="1328" x1="1392" />
            <wire x2="1488" y1="592" y2="592" x1="1392" />
        </branch>
        <branch name="AN(3:0)">
            <wire x2="1648" y1="1264" y2="1264" x1="1120" />
            <wire x2="1648" y1="1264" y2="1328" x1="1648" />
            <wire x2="2176" y1="1328" y2="1328" x1="1648" />
        </branch>
        <branch name="LES(7:0)">
            <wire x2="736" y1="1328" y2="1328" x1="384" />
        </branch>
        <branch name="point(7:0)">
            <wire x2="736" y1="1264" y2="1264" x1="384" />
        </branch>
        <branch name="Hexs(31:0)">
            <wire x2="448" y1="1392" y2="1392" x1="384" />
            <wire x2="736" y1="1392" y2="1392" x1="448" />
            <wire x2="448" y1="1392" y2="1824" x1="448" />
            <wire x2="768" y1="1824" y2="1824" x1="448" />
        </branch>
        <instance x="720" y="768" name="XLXI_10" orien="R0" />
        <branch name="XLXN_3">
            <wire x2="1056" y1="672" y2="672" x1="976" />
            <wire x2="1056" y1="576" y2="672" x1="1056" />
            <wire x2="1472" y1="576" y2="576" x1="1056" />
            <wire x2="1472" y1="576" y2="992" x1="1472" />
            <wire x2="1488" y1="992" y2="992" x1="1472" />
        </branch>
    </sheet>
</drawing>