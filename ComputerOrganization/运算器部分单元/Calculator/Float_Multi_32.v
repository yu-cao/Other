`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:45:51 04/08/2018 
// Design Name: 
// Module Name:    Float_Multi_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Float_Multi_32( input clk, input [31:0]A, input [31:0]B, output [31:0]res );
reg [47:0]mul,r0;
reg [1:0]state = 2'b0;
reg [4:0]count = 5'b0;
reg [23:0]r1;
reg [22:0]r2;
reg [9:0]e,ea,eb;
reg [31:0]r;
assign res= r;
always @ (posedge clk)begin
  case(state)
  2'b00:begin
    r0 <= {24'b0,1'b1,A[22:0]};
	 r1 <= {1'b1,B[22:0]};
	 r2 <=0;
	 count <= 5'b0;
	 mul <= 48'b0;
	 state <= 2'b01;  // 10'h0001111111  = 10'h1110000001
	 ea <= {2'b00,A[30:23]}+10'b1110000001;
	 eb <= {2'b00,B[30:23]}+10'b1110000001;
	 e<=0;
  end
  2'b01:begin
	 if(count == 5'b11000)state<=2'b10;
	 else begin
	   if(r1[0]==1'b1)
			mul<=mul+r0;
		else 
		   mul<=mul;
		r0<=r0<<1;
		r1<=r1>>1;
		count<=count+5'b1;
		state<=2'b01;
	 end
  end
  2'b10:begin
    if(mul[47]==1'b1)begin
	   if(ea+eb>10'b0111111111 && ea+eb<10'b1111111110)begin e<=0;end
		else if(ea+eb<10'b1000000000 && ea+eb>10'b0001111110)begin e<=10'b0011111111;end
		else begin e<=ea+eb+10'b0010000000; end
		r2<=(mul[23]==1'b1)? (mul[46:24]+23'b1):mul[46:24];
	 end
	 else begin
		if(ea+eb>10'b0111111111)begin e<=0;end
		else if(ea+eb<10'b1000000000 && ea+eb>10'b0011111111)begin e<=10'b0011111111;end
		else begin e<=ea+eb+10'b0001111111; end
		r2<=(mul[22]==1'b1)? (mul[45:23]+23'b1):mul[45:23];
	 end
	 state<=2'b11;
  end
  2'b11:begin
    r<={{A[31]^B[31]},e[7:0],r2};
	 state<=2'b00;
  end
  endcase
end
endmodule
