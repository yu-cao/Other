<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Disp_num(63:0)" />
        <signal name="Disp_num(31:0)" />
        <signal name="Disp_num(63:32)" />
        <signal name="V5" />
        <signal name="G0" />
        <signal name="G0,G0,G0" />
        <signal name="G0,G0,V5" />
        <signal name="G0,V5,G0" />
        <signal name="G0,V5,V5" />
        <signal name="SSeg_map(63:0)" />
        <signal name="SSeg_map(7:0)" />
        <signal name="SSeg_map(15:8)" />
        <signal name="SSeg_map(23:16)" />
        <signal name="SSeg_map(31:24)" />
        <signal name="SSeg_map(39:32)" />
        <signal name="SSeg_map(47:40)" />
        <signal name="SSeg_map(55:48)" />
        <signal name="SSeg_map(63:56)" />
        <signal name="V5,G0,G0" />
        <signal name="V5,G0,V5" />
        <signal name="V5,V5,G0" />
        <signal name="V5,V5,V5" />
        <port polarity="Input" name="Disp_num(63:0)" />
        <port polarity="Output" name="SSeg_map(63:0)" />
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="Seg_map_S">
            <timestamp>2017-11-22T8:1:5</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="vcc" name="XLXI_9">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_10">
            <blockpin signalname="G0" name="G" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_11">
            <blockpin signalname="Disp_num(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="G0,G0,G0" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(7:0)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_12">
            <blockpin signalname="Disp_num(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="G0,G0,V5" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(15:8)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_13">
            <blockpin signalname="Disp_num(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="G0,V5,G0" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(23:16)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_14">
            <blockpin signalname="Disp_num(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="G0,V5,V5" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(31:24)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_15">
            <blockpin signalname="Disp_num(63:32)" name="Hexs(31:0)" />
            <blockpin signalname="V5,G0,G0" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(39:32)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_16">
            <blockpin signalname="Disp_num(63:32)" name="Hexs(31:0)" />
            <blockpin signalname="V5,G0,V5" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(47:40)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_17">
            <blockpin signalname="Disp_num(63:32)" name="Hexs(31:0)" />
            <blockpin signalname="V5,V5,G0" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(55:48)" name="Seg_map(7:0)" />
        </block>
        <block symbolname="Seg_map_S" name="XLXI_18">
            <blockpin signalname="Disp_num(63:32)" name="Hexs(31:0)" />
            <blockpin signalname="V5,V5,V5" name="Scan(2:0)" />
            <blockpin signalname="SSeg_map(63:56)" name="Seg_map(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="Disp_num(63:0)">
            <wire x2="880" y1="288" y2="288" x1="592" />
            <wire x2="880" y1="288" y2="1440" x1="880" />
            <wire x2="880" y1="1440" y2="2336" x1="880" />
            <wire x2="880" y1="160" y2="208" x1="880" />
            <wire x2="880" y1="208" y2="288" x1="880" />
        </branch>
        <iomarker fontsize="28" x="592" y="288" name="Disp_num(63:0)" orien="R180" />
        <bustap x2="976" y1="208" y2="208" x1="880" />
        <bustap x2="976" y1="1440" y2="1440" x1="880" />
        <branch name="Disp_num(31:0)">
            <wire x2="1008" y1="208" y2="208" x1="976" />
            <wire x2="1232" y1="208" y2="208" x1="1008" />
            <wire x2="1008" y1="208" y2="512" x1="1008" />
            <wire x2="1232" y1="512" y2="512" x1="1008" />
            <wire x2="1008" y1="512" y2="848" x1="1008" />
            <wire x2="1232" y1="848" y2="848" x1="1008" />
            <wire x2="1008" y1="848" y2="1136" x1="1008" />
            <wire x2="1232" y1="1136" y2="1136" x1="1008" />
        </branch>
        <branch name="Disp_num(63:32)">
            <wire x2="976" y1="1440" y2="1712" x1="976" />
            <wire x2="1232" y1="1712" y2="1712" x1="976" />
            <wire x2="976" y1="1712" y2="1952" x1="976" />
            <wire x2="1232" y1="1952" y2="1952" x1="976" />
            <wire x2="976" y1="1952" y2="2240" x1="976" />
            <wire x2="1232" y1="2240" y2="2240" x1="976" />
            <wire x2="1232" y1="1440" y2="1440" x1="976" />
        </branch>
        <instance x="2112" y="448" name="XLXI_9" orien="R0" />
        <instance x="2208" y="976" name="XLXI_10" orien="R0" />
        <branch name="V5">
            <wire x2="2176" y1="448" y2="464" x1="2176" />
            <wire x2="2512" y1="464" y2="464" x1="2176" />
        </branch>
        <branch name="G0">
            <wire x2="2272" y1="784" y2="848" x1="2272" />
            <wire x2="2352" y1="784" y2="784" x1="2272" />
            <wire x2="2352" y1="784" y2="864" x1="2352" />
            <wire x2="2560" y1="864" y2="864" x1="2352" />
        </branch>
        <branch name="G0,G0,G0">
            <wire x2="1232" y1="272" y2="272" x1="1072" />
        </branch>
        <branch name="G0,G0,V5">
            <wire x2="1232" y1="576" y2="576" x1="1088" />
        </branch>
        <branch name="G0,V5,G0">
            <wire x2="1232" y1="912" y2="912" x1="1104" />
        </branch>
        <branch name="G0,V5,V5">
            <wire x2="1232" y1="1200" y2="1200" x1="1088" />
        </branch>
        <branch name="SSeg_map(63:0)">
            <wire x2="1984" y1="96" y2="208" x1="1984" />
            <wire x2="1984" y1="208" y2="512" x1="1984" />
            <wire x2="1984" y1="512" y2="848" x1="1984" />
            <wire x2="1984" y1="848" y2="1136" x1="1984" />
            <wire x2="1984" y1="1136" y2="1440" x1="1984" />
            <wire x2="2000" y1="1440" y2="1440" x1="1984" />
            <wire x2="2256" y1="1440" y2="1440" x1="2000" />
            <wire x2="2304" y1="1440" y2="1440" x1="2256" />
            <wire x2="1984" y1="1440" y2="1712" x1="1984" />
            <wire x2="1984" y1="1712" y2="1952" x1="1984" />
            <wire x2="1984" y1="1952" y2="2240" x1="1984" />
            <wire x2="1984" y1="2240" y2="2400" x1="1984" />
        </branch>
        <iomarker fontsize="28" x="2304" y="1440" name="SSeg_map(63:0)" orien="R0" />
        <bustap x2="1888" y1="208" y2="208" x1="1984" />
        <bustap x2="1888" y1="512" y2="512" x1="1984" />
        <bustap x2="1888" y1="848" y2="848" x1="1984" />
        <bustap x2="1888" y1="1136" y2="1136" x1="1984" />
        <bustap x2="1888" y1="1440" y2="1440" x1="1984" />
        <bustap x2="1888" y1="1712" y2="1712" x1="1984" />
        <bustap x2="1888" y1="1952" y2="1952" x1="1984" />
        <bustap x2="1888" y1="2240" y2="2240" x1="1984" />
        <branch name="SSeg_map(7:0)">
            <wire x2="1888" y1="208" y2="208" x1="1616" />
        </branch>
        <branch name="SSeg_map(15:8)">
            <wire x2="1888" y1="512" y2="512" x1="1616" />
        </branch>
        <branch name="SSeg_map(23:16)">
            <wire x2="1888" y1="848" y2="848" x1="1616" />
        </branch>
        <branch name="SSeg_map(31:24)">
            <wire x2="1888" y1="1136" y2="1136" x1="1616" />
        </branch>
        <branch name="SSeg_map(39:32)">
            <wire x2="1888" y1="1440" y2="1440" x1="1616" />
        </branch>
        <branch name="SSeg_map(47:40)">
            <wire x2="1888" y1="1712" y2="1712" x1="1616" />
        </branch>
        <branch name="SSeg_map(55:48)">
            <wire x2="1888" y1="1952" y2="1952" x1="1616" />
        </branch>
        <branch name="SSeg_map(63:56)">
            <wire x2="1888" y1="2240" y2="2240" x1="1616" />
        </branch>
        <branch name="V5,G0,G0">
            <wire x2="1232" y1="1504" y2="1504" x1="1120" />
        </branch>
        <branch name="V5,G0,V5">
            <wire x2="1232" y1="1776" y2="1776" x1="1120" />
        </branch>
        <branch name="V5,V5,G0">
            <wire x2="1232" y1="2016" y2="2016" x1="1136" />
        </branch>
        <branch name="V5,V5,V5">
            <wire x2="1232" y1="2304" y2="2304" x1="1136" />
        </branch>
        <instance x="1232" y="304" name="XLXI_11" orien="R0">
        </instance>
        <instance x="1232" y="608" name="XLXI_12" orien="R0">
        </instance>
        <instance x="1232" y="944" name="XLXI_13" orien="R0">
        </instance>
        <instance x="1232" y="1232" name="XLXI_14" orien="R0">
        </instance>
        <instance x="1232" y="1536" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1232" y="1808" name="XLXI_16" orien="R0">
        </instance>
        <instance x="1232" y="2048" name="XLXI_17" orien="R0">
        </instance>
        <instance x="1232" y="2336" name="XLXI_18" orien="R0">
        </instance>
    </sheet>
</drawing>