`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:43:35 04/09/2018 
// Design Name: 
// Module Name:    FLU_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FLU_32(input clk, input [2:0]sw,input [31:0]A,input [31:0]B, output[31:0]res, output inf,output zero,output nan);

wire [31:0]add,sub,mul,div,slt;

Float_Add_32 adder(.clk(clk),.A(A),.B(B),.res(add));
Float_Sub_32 suber(.clk(clk),.A(A),.B(B),.res(sub));
Float_Multi_32 Muler(.clk(clk),.A(A),.B(B),.res(mul));
Float_Div_32 Diver(.clk(clk),.A(A),.B(B),.res(div));
Float_slt slter(.A(A),.B(B),.res(slt));

MUX8T1_32 mux(.s(sw),.I0(add),.I1(sub),.I2(mul),.I3(div),.I4(slt),.I5(32'b0),.I6(32'b0),.I7(32'b0),.O(res));
assign zero = (res[30:23]==8'h00);
assign inf = (res[30:23]==8'hff && res[22:0]==23'b0);
assign nan = (res[30:23]==8'hff && res[22:0]!=23'b0);
endmodule
