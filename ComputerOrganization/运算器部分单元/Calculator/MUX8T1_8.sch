<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(2:0)" />
        <signal name="s(1:0)" />
        <signal name="I0(7:0)" />
        <signal name="I0(3:0)" />
        <signal name="I1(7:0)" />
        <signal name="I2(7:0)" />
        <signal name="I4(7:0)" />
        <signal name="I5(7:0)" />
        <signal name="I6(7:0)" />
        <signal name="I7(7:0)" />
        <signal name="I3(7:0)" />
        <signal name="I1(3:0)" />
        <signal name="I2(3:0)" />
        <signal name="I3(3:0)" />
        <signal name="I4(3:0)" />
        <signal name="I5(3:0)" />
        <signal name="I6(3:0)" />
        <signal name="I7(3:0)" />
        <signal name="XLXN_33" />
        <signal name="s(2)" />
        <signal name="O0(3:0)" />
        <signal name="I0(7:4)" />
        <signal name="I1(7:4)" />
        <signal name="I2(7:4)" />
        <signal name="I3(7:4)" />
        <signal name="I4(7:4)" />
        <signal name="I5(7:4)" />
        <signal name="I6(7:4)" />
        <signal name="I7(7:4)" />
        <signal name="O0(0)" />
        <signal name="O0(1)" />
        <signal name="O0(2)" />
        <signal name="O0(3)" />
        <signal name="O1(3:0)" />
        <signal name="O1(0)" />
        <signal name="O1(1)" />
        <signal name="O1(2)" />
        <signal name="O1(3)" />
        <signal name="O2(3:0)" />
        <signal name="O2(0)" />
        <signal name="O2(1)" />
        <signal name="O2(2)" />
        <signal name="O2(3)" />
        <signal name="O3(3:0)" />
        <signal name="O3(0)" />
        <signal name="O3(1)" />
        <signal name="O3(2)" />
        <signal name="O3(3)" />
        <signal name="XLXN_78" />
        <signal name="XLXN_79" />
        <signal name="XLXN_80" />
        <signal name="XLXN_81" />
        <signal name="XLXN_83" />
        <signal name="XLXN_84" />
        <signal name="XLXN_85" />
        <signal name="XLXN_86" />
        <signal name="XLXN_87" />
        <signal name="XLXN_88" />
        <signal name="XLXN_89" />
        <signal name="XLXN_90" />
        <signal name="XLXN_91" />
        <signal name="XLXN_92" />
        <signal name="XLXN_93" />
        <signal name="XLXN_94" />
        <signal name="O(7:0)" />
        <signal name="O(0)" />
        <signal name="O(1)" />
        <signal name="O(2)" />
        <signal name="O(3)" />
        <signal name="O(4)" />
        <signal name="O(5)" />
        <signal name="O(6)" />
        <signal name="O(7)" />
        <port polarity="Input" name="s(2:0)" />
        <port polarity="Input" name="I0(7:0)" />
        <port polarity="Input" name="I1(7:0)" />
        <port polarity="Input" name="I2(7:0)" />
        <port polarity="Input" name="I4(7:0)" />
        <port polarity="Input" name="I5(7:0)" />
        <port polarity="Input" name="I6(7:0)" />
        <port polarity="Input" name="I7(7:0)" />
        <port polarity="Input" name="I3(7:0)" />
        <port polarity="Output" name="O(7:0)" />
        <blockdef name="MUX4T1">
            <timestamp>2017-11-22T7:3:15</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <block symbolname="inv" name="XLXI_9">
            <blockpin signalname="s(2)" name="I" />
            <blockpin signalname="XLXN_33" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_10">
            <blockpin signalname="O0(0)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_78" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_11">
            <blockpin signalname="O1(0)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_79" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_12">
            <blockpin signalname="O0(1)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_80" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_13">
            <blockpin signalname="O1(1)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_81" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_14">
            <blockpin signalname="O0(2)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_83" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_15">
            <blockpin signalname="O1(2)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_84" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_16">
            <blockpin signalname="O0(3)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_85" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_17">
            <blockpin signalname="O1(3)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_86" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_32">
            <blockpin signalname="O2(0)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_87" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_33">
            <blockpin signalname="O3(0)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_88" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_34">
            <blockpin signalname="O2(1)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_89" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_35">
            <blockpin signalname="O3(1)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_90" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_36">
            <blockpin signalname="O2(2)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_91" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_37">
            <blockpin signalname="O3(2)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_92" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_38">
            <blockpin signalname="O2(3)" name="I0" />
            <blockpin signalname="XLXN_33" name="I1" />
            <blockpin signalname="XLXN_93" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_39">
            <blockpin signalname="O3(3)" name="I0" />
            <blockpin signalname="s(2)" name="I1" />
            <blockpin signalname="XLXN_94" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_40">
            <blockpin signalname="XLXN_79" name="I0" />
            <blockpin signalname="XLXN_78" name="I1" />
            <blockpin signalname="O(0)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_41">
            <blockpin signalname="XLXN_81" name="I0" />
            <blockpin signalname="XLXN_80" name="I1" />
            <blockpin signalname="O(1)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_42">
            <blockpin signalname="XLXN_84" name="I0" />
            <blockpin signalname="XLXN_83" name="I1" />
            <blockpin signalname="O(2)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_43">
            <blockpin signalname="XLXN_86" name="I0" />
            <blockpin signalname="XLXN_85" name="I1" />
            <blockpin signalname="O(3)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_44">
            <blockpin signalname="XLXN_88" name="I0" />
            <blockpin signalname="XLXN_87" name="I1" />
            <blockpin signalname="O(4)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_45">
            <blockpin signalname="XLXN_90" name="I0" />
            <blockpin signalname="XLXN_89" name="I1" />
            <blockpin signalname="O(5)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_46">
            <blockpin signalname="XLXN_92" name="I0" />
            <blockpin signalname="XLXN_91" name="I1" />
            <blockpin signalname="O(6)" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_47">
            <blockpin signalname="XLXN_94" name="I0" />
            <blockpin signalname="XLXN_93" name="I1" />
            <blockpin signalname="O(7)" name="O" />
        </block>
        <block symbolname="MUX4T1" name="XLXI_48">
            <blockpin signalname="s(1:0)" name="s(1:0)" />
            <blockpin signalname="I0(3:0)" name="I0(3:0)" />
            <blockpin signalname="I1(3:0)" name="I1(3:0)" />
            <blockpin signalname="I2(3:0)" name="I2(3:0)" />
            <blockpin signalname="I3(3:0)" name="I3(3:0)" />
            <blockpin signalname="O0(3:0)" name="O(3:0)" />
        </block>
        <block symbolname="MUX4T1" name="XLXI_49">
            <blockpin signalname="s(1:0)" name="s(1:0)" />
            <blockpin signalname="I4(3:0)" name="I0(3:0)" />
            <blockpin signalname="I5(3:0)" name="I1(3:0)" />
            <blockpin signalname="I6(3:0)" name="I2(3:0)" />
            <blockpin signalname="I7(3:0)" name="I3(3:0)" />
            <blockpin signalname="O1(3:0)" name="O(3:0)" />
        </block>
        <block symbolname="MUX4T1" name="XLXI_50">
            <blockpin signalname="s(1:0)" name="s(1:0)" />
            <blockpin signalname="I0(7:4)" name="I0(3:0)" />
            <blockpin signalname="I1(7:4)" name="I1(3:0)" />
            <blockpin signalname="I2(7:4)" name="I2(3:0)" />
            <blockpin signalname="I3(7:4)" name="I3(3:0)" />
            <blockpin signalname="O2(3:0)" name="O(3:0)" />
        </block>
        <block symbolname="MUX4T1" name="XLXI_51">
            <blockpin signalname="s(1:0)" name="s(1:0)" />
            <blockpin signalname="I4(7:4)" name="I0(3:0)" />
            <blockpin signalname="I5(7:4)" name="I1(3:0)" />
            <blockpin signalname="I6(7:4)" name="I2(3:0)" />
            <blockpin signalname="I7(7:4)" name="I3(3:0)" />
            <blockpin signalname="O3(3:0)" name="O(3:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <branch name="s(2:0)">
            <wire x2="192" y1="192" y2="192" x1="160" />
            <wire x2="192" y1="192" y2="256" x1="192" />
            <wire x2="192" y1="256" y2="288" x1="192" />
            <wire x2="192" y1="112" y2="144" x1="192" />
            <wire x2="192" y1="144" y2="192" x1="192" />
        </branch>
        <iomarker fontsize="28" x="160" y="192" name="s(2:0)" orien="R180" />
        <bustap x2="288" y1="144" y2="144" x1="192" />
        <bustap x2="288" y1="256" y2="256" x1="192" />
        <instance x="1008" y="176" name="XLXI_9" orien="R0" />
        <branch name="s(1:0)">
            <wire x2="400" y1="256" y2="256" x1="288" />
            <wire x2="400" y1="256" y2="416" x1="400" />
            <wire x2="512" y1="416" y2="416" x1="400" />
            <wire x2="400" y1="416" y2="1056" x1="400" />
            <wire x2="512" y1="1056" y2="1056" x1="400" />
            <wire x2="400" y1="1056" y2="1696" x1="400" />
            <wire x2="400" y1="1696" y2="2320" x1="400" />
            <wire x2="512" y1="2320" y2="2320" x1="400" />
            <wire x2="512" y1="1696" y2="1696" x1="400" />
        </branch>
        <instance x="1712" y="192" name="XLXI_10" orien="R0" />
        <instance x="1712" y="336" name="XLXI_11" orien="R0" />
        <instance x="1712" y="560" name="XLXI_12" orien="R0" />
        <instance x="1712" y="704" name="XLXI_13" orien="R0" />
        <instance x="1696" y="928" name="XLXI_14" orien="R0" />
        <instance x="1696" y="1088" name="XLXI_15" orien="R0" />
        <instance x="1696" y="1296" name="XLXI_16" orien="R0" />
        <instance x="1696" y="1440" name="XLXI_17" orien="R0" />
        <instance x="1696" y="1712" name="XLXI_32" orien="R0" />
        <instance x="1696" y="1856" name="XLXI_33" orien="R0" />
        <instance x="1696" y="2080" name="XLXI_34" orien="R0" />
        <instance x="1696" y="2224" name="XLXI_35" orien="R0" />
        <instance x="1680" y="2448" name="XLXI_36" orien="R0" />
        <instance x="1680" y="2608" name="XLXI_37" orien="R0" />
        <instance x="1680" y="2816" name="XLXI_38" orien="R0" />
        <instance x="1680" y="2960" name="XLXI_39" orien="R0" />
        <branch name="I0(7:0)">
            <wire x2="208" y1="480" y2="480" x1="160" />
            <wire x2="208" y1="464" y2="480" x1="208" />
        </branch>
        <iomarker fontsize="28" x="160" y="480" name="I0(7:0)" orien="R180" />
        <bustap x2="304" y1="480" y2="480" x1="208" />
        <branch name="I0(3:0)">
            <wire x2="512" y1="480" y2="480" x1="304" />
        </branch>
        <branch name="I1(7:0)">
            <wire x2="208" y1="544" y2="544" x1="160" />
            <wire x2="208" y1="528" y2="544" x1="208" />
        </branch>
        <bustap x2="304" y1="544" y2="544" x1="208" />
        <iomarker fontsize="28" x="160" y="544" name="I1(7:0)" orien="R180" />
        <iomarker fontsize="28" x="192" y="608" name="I2(7:0)" orien="R180" />
        <branch name="I4(7:0)">
            <wire x2="256" y1="1120" y2="1120" x1="208" />
            <wire x2="256" y1="1104" y2="1120" x1="256" />
        </branch>
        <bustap x2="352" y1="1120" y2="1120" x1="256" />
        <iomarker fontsize="28" x="208" y="1120" name="I4(7:0)" orien="R180" />
        <branch name="I5(7:0)">
            <wire x2="256" y1="1184" y2="1184" x1="208" />
            <wire x2="256" y1="1168" y2="1184" x1="256" />
        </branch>
        <bustap x2="352" y1="1184" y2="1184" x1="256" />
        <iomarker fontsize="28" x="208" y="1184" name="I5(7:0)" orien="R180" />
        <branch name="I6(7:0)">
            <wire x2="256" y1="1248" y2="1248" x1="208" />
            <wire x2="256" y1="1232" y2="1248" x1="256" />
        </branch>
        <bustap x2="352" y1="1248" y2="1248" x1="256" />
        <iomarker fontsize="28" x="208" y="1248" name="I6(7:0)" orien="R180" />
        <branch name="I7(7:0)">
            <wire x2="256" y1="1312" y2="1312" x1="208" />
            <wire x2="256" y1="1296" y2="1312" x1="256" />
        </branch>
        <bustap x2="352" y1="1312" y2="1312" x1="256" />
        <iomarker fontsize="28" x="208" y="1312" name="I7(7:0)" orien="R180" />
        <iomarker fontsize="28" x="192" y="672" name="I3(7:0)" orien="R180" />
        <bustap x2="336" y1="672" y2="672" x1="240" />
        <bustap x2="336" y1="608" y2="608" x1="240" />
        <branch name="I3(7:0)">
            <wire x2="240" y1="672" y2="672" x1="192" />
            <wire x2="240" y1="656" y2="672" x1="240" />
        </branch>
        <branch name="I2(7:0)">
            <wire x2="240" y1="608" y2="608" x1="192" />
            <wire x2="240" y1="592" y2="608" x1="240" />
        </branch>
        <branch name="I1(3:0)">
            <wire x2="512" y1="544" y2="544" x1="304" />
        </branch>
        <branch name="I2(3:0)">
            <wire x2="512" y1="608" y2="608" x1="336" />
        </branch>
        <branch name="I3(3:0)">
            <wire x2="512" y1="672" y2="672" x1="336" />
        </branch>
        <branch name="I4(3:0)">
            <wire x2="512" y1="1120" y2="1120" x1="352" />
        </branch>
        <branch name="I5(3:0)">
            <wire x2="512" y1="1184" y2="1184" x1="352" />
        </branch>
        <branch name="I6(3:0)">
            <wire x2="512" y1="1248" y2="1248" x1="352" />
        </branch>
        <branch name="I7(3:0)">
            <wire x2="512" y1="1312" y2="1312" x1="352" />
        </branch>
        <branch name="s(2)">
            <wire x2="880" y1="144" y2="144" x1="288" />
            <wire x2="1008" y1="144" y2="144" x1="880" />
            <wire x2="880" y1="144" y2="208" x1="880" />
            <wire x2="1440" y1="208" y2="208" x1="880" />
            <wire x2="1712" y1="208" y2="208" x1="1440" />
            <wire x2="1440" y1="208" y2="576" x1="1440" />
            <wire x2="1712" y1="576" y2="576" x1="1440" />
            <wire x2="1440" y1="576" y2="960" x1="1440" />
            <wire x2="1696" y1="960" y2="960" x1="1440" />
            <wire x2="1440" y1="960" y2="1312" x1="1440" />
            <wire x2="1696" y1="1312" y2="1312" x1="1440" />
            <wire x2="1440" y1="1312" y2="1728" x1="1440" />
            <wire x2="1696" y1="1728" y2="1728" x1="1440" />
            <wire x2="1440" y1="1728" y2="2096" x1="1440" />
            <wire x2="1696" y1="2096" y2="2096" x1="1440" />
            <wire x2="1440" y1="2096" y2="2480" x1="1440" />
            <wire x2="1440" y1="2480" y2="2832" x1="1440" />
            <wire x2="1680" y1="2832" y2="2832" x1="1440" />
            <wire x2="1680" y1="2480" y2="2480" x1="1440" />
        </branch>
        <branch name="XLXN_33">
            <wire x2="1472" y1="144" y2="144" x1="1232" />
            <wire x2="1472" y1="144" y2="432" x1="1472" />
            <wire x2="1472" y1="432" y2="800" x1="1472" />
            <wire x2="1696" y1="800" y2="800" x1="1472" />
            <wire x2="1472" y1="800" y2="1168" x1="1472" />
            <wire x2="1696" y1="1168" y2="1168" x1="1472" />
            <wire x2="1472" y1="1168" y2="1584" x1="1472" />
            <wire x2="1696" y1="1584" y2="1584" x1="1472" />
            <wire x2="1472" y1="1584" y2="1952" x1="1472" />
            <wire x2="1696" y1="1952" y2="1952" x1="1472" />
            <wire x2="1472" y1="1952" y2="2320" x1="1472" />
            <wire x2="1472" y1="2320" y2="2688" x1="1472" />
            <wire x2="1680" y1="2688" y2="2688" x1="1472" />
            <wire x2="1520" y1="2320" y2="2320" x1="1472" />
            <wire x2="1680" y1="2320" y2="2320" x1="1520" />
            <wire x2="1712" y1="432" y2="432" x1="1472" />
            <wire x2="1472" y1="64" y2="144" x1="1472" />
            <wire x2="1712" y1="64" y2="64" x1="1472" />
        </branch>
        <branch name="O0(3:0)">
            <wire x2="1536" y1="416" y2="416" x1="896" />
            <wire x2="1536" y1="416" y2="496" x1="1536" />
            <wire x2="1536" y1="496" y2="864" x1="1536" />
            <wire x2="1536" y1="864" y2="1232" x1="1536" />
            <wire x2="1536" y1="1232" y2="1280" x1="1536" />
            <wire x2="1536" y1="96" y2="128" x1="1536" />
            <wire x2="1536" y1="128" y2="416" x1="1536" />
        </branch>
        <branch name="I0(7:4)">
            <wire x2="512" y1="1760" y2="1760" x1="240" />
        </branch>
        <branch name="I1(7:4)">
            <wire x2="512" y1="1824" y2="1824" x1="240" />
        </branch>
        <branch name="I2(7:4)">
            <wire x2="512" y1="1888" y2="1888" x1="240" />
        </branch>
        <branch name="I3(7:4)">
            <wire x2="512" y1="1952" y2="1952" x1="240" />
        </branch>
        <branch name="I4(7:4)">
            <wire x2="512" y1="2384" y2="2384" x1="272" />
        </branch>
        <branch name="I5(7:4)">
            <wire x2="512" y1="2448" y2="2448" x1="272" />
        </branch>
        <branch name="I6(7:4)">
            <wire x2="512" y1="2512" y2="2512" x1="288" />
        </branch>
        <branch name="I7(7:4)">
            <wire x2="512" y1="2576" y2="2576" x1="272" />
        </branch>
        <bustap x2="1632" y1="128" y2="128" x1="1536" />
        <bustap x2="1632" y1="496" y2="496" x1="1536" />
        <bustap x2="1632" y1="864" y2="864" x1="1536" />
        <bustap x2="1632" y1="1232" y2="1232" x1="1536" />
        <branch name="O0(0)">
            <wire x2="1712" y1="128" y2="128" x1="1632" />
        </branch>
        <branch name="O0(1)">
            <wire x2="1712" y1="496" y2="496" x1="1632" />
        </branch>
        <branch name="O0(2)">
            <wire x2="1696" y1="864" y2="864" x1="1632" />
        </branch>
        <branch name="O0(3)">
            <wire x2="1696" y1="1232" y2="1232" x1="1632" />
        </branch>
        <branch name="O1(3:0)">
            <wire x2="1504" y1="1056" y2="1056" x1="896" />
            <wire x2="1504" y1="1056" y2="1376" x1="1504" />
            <wire x2="1504" y1="1376" y2="1424" x1="1504" />
            <wire x2="1504" y1="240" y2="272" x1="1504" />
            <wire x2="1504" y1="272" y2="640" x1="1504" />
            <wire x2="1504" y1="640" y2="1024" x1="1504" />
            <wire x2="1504" y1="1024" y2="1056" x1="1504" />
        </branch>
        <bustap x2="1600" y1="272" y2="272" x1="1504" />
        <bustap x2="1600" y1="640" y2="640" x1="1504" />
        <bustap x2="1600" y1="1024" y2="1024" x1="1504" />
        <bustap x2="1600" y1="1376" y2="1376" x1="1504" />
        <branch name="O1(0)">
            <wire x2="1712" y1="272" y2="272" x1="1600" />
        </branch>
        <branch name="O1(1)">
            <wire x2="1712" y1="640" y2="640" x1="1600" />
        </branch>
        <branch name="O1(2)">
            <wire x2="1696" y1="1024" y2="1024" x1="1600" />
        </branch>
        <branch name="O1(3)">
            <wire x2="1696" y1="1376" y2="1376" x1="1600" />
        </branch>
        <branch name="O2(3:0)">
            <wire x2="1536" y1="1696" y2="1696" x1="896" />
            <wire x2="1536" y1="1696" y2="2016" x1="1536" />
            <wire x2="1536" y1="2016" y2="2384" x1="1536" />
            <wire x2="1536" y1="2384" y2="2752" x1="1536" />
            <wire x2="1536" y1="2752" y2="2800" x1="1536" />
            <wire x2="1536" y1="1600" y2="1648" x1="1536" />
            <wire x2="1536" y1="1648" y2="1696" x1="1536" />
        </branch>
        <bustap x2="1632" y1="1648" y2="1648" x1="1536" />
        <bustap x2="1632" y1="2016" y2="2016" x1="1536" />
        <bustap x2="1632" y1="2384" y2="2384" x1="1536" />
        <bustap x2="1632" y1="2752" y2="2752" x1="1536" />
        <branch name="O2(0)">
            <wire x2="1696" y1="1648" y2="1648" x1="1632" />
        </branch>
        <branch name="O2(1)">
            <wire x2="1696" y1="2016" y2="2016" x1="1632" />
        </branch>
        <branch name="O2(2)">
            <wire x2="1680" y1="2384" y2="2384" x1="1632" />
        </branch>
        <branch name="O2(3)">
            <wire x2="1680" y1="2752" y2="2752" x1="1632" />
        </branch>
        <branch name="O3(3:0)">
            <wire x2="912" y1="2320" y2="2320" x1="896" />
            <wire x2="1296" y1="2320" y2="2320" x1="912" />
            <wire x2="1296" y1="2208" y2="2320" x1="1296" />
            <wire x2="1504" y1="2208" y2="2208" x1="1296" />
            <wire x2="1504" y1="2208" y2="2544" x1="1504" />
            <wire x2="1504" y1="2544" y2="2896" x1="1504" />
            <wire x2="1504" y1="2896" y2="3008" x1="1504" />
            <wire x2="1504" y1="1760" y2="1792" x1="1504" />
            <wire x2="1504" y1="1792" y2="2160" x1="1504" />
            <wire x2="1504" y1="2160" y2="2208" x1="1504" />
        </branch>
        <bustap x2="1600" y1="1792" y2="1792" x1="1504" />
        <bustap x2="1600" y1="2160" y2="2160" x1="1504" />
        <bustap x2="1600" y1="2544" y2="2544" x1="1504" />
        <bustap x2="1600" y1="2896" y2="2896" x1="1504" />
        <branch name="O3(0)">
            <wire x2="1696" y1="1792" y2="1792" x1="1600" />
        </branch>
        <branch name="O3(1)">
            <wire x2="1696" y1="2160" y2="2160" x1="1600" />
        </branch>
        <branch name="O3(2)">
            <wire x2="1680" y1="2544" y2="2544" x1="1600" />
        </branch>
        <branch name="O3(3)">
            <wire x2="1680" y1="2896" y2="2896" x1="1600" />
        </branch>
        <instance x="2352" y="240" name="XLXI_40" orien="R0" />
        <instance x="2352" y="640" name="XLXI_41" orien="R0" />
        <instance x="2352" y="992" name="XLXI_42" orien="R0" />
        <instance x="2352" y="1360" name="XLXI_43" orien="R0" />
        <instance x="2416" y="2880" name="XLXI_47" orien="R0" />
        <instance x="2400" y="2528" name="XLXI_46" orien="R0" />
        <instance x="2384" y="2160" name="XLXI_45" orien="R0" />
        <instance x="2352" y="1792" name="XLXI_44" orien="R0" />
        <branch name="XLXN_78">
            <wire x2="2160" y1="96" y2="96" x1="1968" />
            <wire x2="2160" y1="96" y2="112" x1="2160" />
            <wire x2="2352" y1="112" y2="112" x1="2160" />
        </branch>
        <branch name="XLXN_79">
            <wire x2="2160" y1="240" y2="240" x1="1968" />
            <wire x2="2160" y1="176" y2="240" x1="2160" />
            <wire x2="2352" y1="176" y2="176" x1="2160" />
        </branch>
        <branch name="XLXN_80">
            <wire x2="2160" y1="464" y2="464" x1="1968" />
            <wire x2="2160" y1="464" y2="512" x1="2160" />
            <wire x2="2352" y1="512" y2="512" x1="2160" />
        </branch>
        <branch name="XLXN_81">
            <wire x2="2160" y1="608" y2="608" x1="1968" />
            <wire x2="2160" y1="576" y2="608" x1="2160" />
            <wire x2="2352" y1="576" y2="576" x1="2160" />
        </branch>
        <branch name="XLXN_83">
            <wire x2="1968" y1="832" y2="832" x1="1952" />
            <wire x2="1968" y1="832" y2="864" x1="1968" />
            <wire x2="2352" y1="864" y2="864" x1="1968" />
        </branch>
        <branch name="XLXN_84">
            <wire x2="2144" y1="992" y2="992" x1="1952" />
            <wire x2="2144" y1="928" y2="992" x1="2144" />
            <wire x2="2352" y1="928" y2="928" x1="2144" />
        </branch>
        <branch name="XLXN_85">
            <wire x2="2144" y1="1200" y2="1200" x1="1952" />
            <wire x2="2144" y1="1200" y2="1232" x1="2144" />
            <wire x2="2352" y1="1232" y2="1232" x1="2144" />
        </branch>
        <branch name="XLXN_86">
            <wire x2="2144" y1="1344" y2="1344" x1="1952" />
            <wire x2="2144" y1="1296" y2="1344" x1="2144" />
            <wire x2="2352" y1="1296" y2="1296" x1="2144" />
        </branch>
        <branch name="XLXN_87">
            <wire x2="2144" y1="1616" y2="1616" x1="1952" />
            <wire x2="2144" y1="1616" y2="1664" x1="2144" />
            <wire x2="2352" y1="1664" y2="1664" x1="2144" />
        </branch>
        <branch name="XLXN_88">
            <wire x2="2144" y1="1760" y2="1760" x1="1952" />
            <wire x2="2144" y1="1728" y2="1760" x1="2144" />
            <wire x2="2352" y1="1728" y2="1728" x1="2144" />
        </branch>
        <branch name="XLXN_89">
            <wire x2="2160" y1="1984" y2="1984" x1="1952" />
            <wire x2="2160" y1="1984" y2="2032" x1="2160" />
            <wire x2="2384" y1="2032" y2="2032" x1="2160" />
        </branch>
        <branch name="XLXN_90">
            <wire x2="2160" y1="2128" y2="2128" x1="1952" />
            <wire x2="2160" y1="2096" y2="2128" x1="2160" />
            <wire x2="2384" y1="2096" y2="2096" x1="2160" />
        </branch>
        <branch name="XLXN_91">
            <wire x2="2160" y1="2352" y2="2352" x1="1936" />
            <wire x2="2160" y1="2352" y2="2400" x1="2160" />
            <wire x2="2400" y1="2400" y2="2400" x1="2160" />
        </branch>
        <branch name="XLXN_92">
            <wire x2="2160" y1="2512" y2="2512" x1="1936" />
            <wire x2="2160" y1="2464" y2="2512" x1="2160" />
            <wire x2="2400" y1="2464" y2="2464" x1="2160" />
        </branch>
        <branch name="XLXN_93">
            <wire x2="2176" y1="2720" y2="2720" x1="1936" />
            <wire x2="2176" y1="2720" y2="2752" x1="2176" />
            <wire x2="2416" y1="2752" y2="2752" x1="2176" />
        </branch>
        <branch name="XLXN_94">
            <wire x2="2176" y1="2864" y2="2864" x1="1936" />
            <wire x2="2176" y1="2816" y2="2864" x1="2176" />
            <wire x2="2416" y1="2816" y2="2816" x1="2176" />
        </branch>
        <branch name="O(7:0)">
            <wire x2="2832" y1="64" y2="144" x1="2832" />
            <wire x2="2832" y1="144" y2="544" x1="2832" />
            <wire x2="2832" y1="544" y2="896" x1="2832" />
            <wire x2="2832" y1="896" y2="1264" x1="2832" />
            <wire x2="2832" y1="1264" y2="1456" x1="2832" />
            <wire x2="3152" y1="1456" y2="1456" x1="2832" />
            <wire x2="2832" y1="1456" y2="1696" x1="2832" />
            <wire x2="2832" y1="1696" y2="2064" x1="2832" />
            <wire x2="2832" y1="2064" y2="2432" x1="2832" />
            <wire x2="2832" y1="2432" y2="2784" x1="2832" />
            <wire x2="2832" y1="2784" y2="2976" x1="2832" />
        </branch>
        <iomarker fontsize="28" x="3152" y="1456" name="O(7:0)" orien="R0" />
        <bustap x2="2736" y1="144" y2="144" x1="2832" />
        <bustap x2="2736" y1="544" y2="544" x1="2832" />
        <bustap x2="2736" y1="896" y2="896" x1="2832" />
        <bustap x2="2736" y1="1264" y2="1264" x1="2832" />
        <bustap x2="2736" y1="1696" y2="1696" x1="2832" />
        <bustap x2="2736" y1="2064" y2="2064" x1="2832" />
        <bustap x2="2736" y1="2432" y2="2432" x1="2832" />
        <bustap x2="2736" y1="2784" y2="2784" x1="2832" />
        <branch name="O(0)">
            <wire x2="2736" y1="144" y2="144" x1="2608" />
        </branch>
        <branch name="O(1)">
            <wire x2="2736" y1="544" y2="544" x1="2608" />
        </branch>
        <branch name="O(2)">
            <wire x2="2736" y1="896" y2="896" x1="2608" />
        </branch>
        <branch name="O(3)">
            <wire x2="2736" y1="1264" y2="1264" x1="2608" />
        </branch>
        <branch name="O(4)">
            <wire x2="2736" y1="1696" y2="1696" x1="2608" />
        </branch>
        <branch name="O(5)">
            <wire x2="2736" y1="2064" y2="2064" x1="2640" />
        </branch>
        <branch name="O(6)">
            <wire x2="2736" y1="2432" y2="2432" x1="2656" />
        </branch>
        <branch name="O(7)">
            <wire x2="2736" y1="2784" y2="2784" x1="2672" />
        </branch>
        <instance x="512" y="704" name="XLXI_48" orien="R0">
        </instance>
        <instance x="512" y="1344" name="XLXI_49" orien="R0">
        </instance>
        <instance x="512" y="1984" name="XLXI_50" orien="R0">
        </instance>
        <instance x="512" y="2608" name="XLXI_51" orien="R0">
        </instance>
    </sheet>
</drawing>