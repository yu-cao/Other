`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:44:30 03/29/2018 
// Design Name: 
// Module Name:    Mul_Com_4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mul_Com_32bit(
    input [31:0]A,
	 input [31:0]B,
	 input clk,
	 output reg[63:0]S
    );

	 reg [32:0] a_reg;
	 reg [63:0] temp;
	 wire [31:0] b0;
	 wire [31:0] negb;
	 reg [5:0] count;
	 reg [1:0] state = 2'b0;
	 
	 parameter Start = 2'b0, Process1 = 2'b01, Process2 = 2'b10,End = 2'b11;
	 assign negb = -B;

    Mux4to1_32b  m0(.I0(32'b0),.I1(B[31:0]),.I2(negb[31:0]),.I3(32'b0),.s({a_reg[1],a_reg[0]}),.o(b0[31:0]));	 
	 
    always @(posedge clk) begin
        case (state)
            Start: begin
                count <= 6'b0;
                temp <= 64'b0;
					 a_reg <= {A[31:0],1'b0};
                state <= Process1;
            end
				Process1: begin
				    temp <= {b0[31:0],32'b0};
                a_reg <= a_reg >> 1;
                count <= count + 1;
					 state <= Process2;
			   end
            Process2: begin
                if(count == 6'b100000)
                    state <= End;
                else begin
					     temp <= {b0[31:0],32'b0} + {temp[63], temp[63:1]};
                    a_reg <= a_reg >> 1;	
                    count <= count + 1; 
                end
            end
            End: begin
                S <= {temp[63], temp[63:1]};
                state <= Start;
            end
			   default:begin
			       state <= Start;
			   end
        endcase
    end
	 
endmodule
