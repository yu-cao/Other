//循环移位，S=0时，进行循环左移1bit；S=1时，进行循环右移1bit
//即移出去的数字从另一头插入
module circle(A, Z, S);
    parameter N = 32;

    input wire [(N-1):0]A;
    input wire S;
    output wire [(N-1):0]Z;
    wire [(N-1):0]B;
    wire [(N-1):0]C;

    assign B[0]=A[N-1];
    assign B[(N-1):1]=A[(N-2):0];

    assign C[N-1]=A[0];
    assign C[(N-2):0]=A[(N-1):1];

    mux_2to1 m3 (.X(B),.Y(C),.S(S),.Z(Z));

endmodule
