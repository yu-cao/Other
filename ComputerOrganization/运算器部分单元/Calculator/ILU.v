`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:11:10 04/13/2018 
// Design Name: 
// Module Name:    ILU 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ILU(clk,SW,Ctrl,SAH,AL,B,ans,overflow,larger_A,equal,CF,ZF);
	input wire clk;
	input wire SAH;
	input wire [3:0]SW;//SW:4-bit switch
	input wire Ctrl;//Ctrl:Ctrl something 
	input wire [31:0]AL;//A的低32位
	input wire [31:0]B;
	output wire [63:0]ans;//ans:可能要作为乘法的输出
	output wire overflow;//OF:OverFlow
	output wire larger_A;
	output wire equal;
	output wire CF;
	output wire ZF;
	
	//adder为Add或者Sub的结果
	wire [31:0]adder,andd,orr,xorr,nott,sll,sra,srl,cir;
	wire [63:0]mul_O,mul_C;
	wire [31:0]div_OQ,div_OR,div_CQ,div_CR;//除法的输出(32bit的商+32bit的余数)

	AddSub32b Add32 (.A(AL),.B(B),.Ctrl(Ctrl),.CF(CF),.overflow(overflow),.ZF(ZF),.S(adder));
	
	Mul_Ori_32bit MO (.A(AL),.B(B),.clk(clk),.S(mul_O));

	Mul_Com_32bit MC (.A(AL),.B(B),.clk(clk),.S(mul_C));
	
	Div_Ori_32bit DO (.Ctrl(Ctrl),.High(SAH),.Low(AL),.B(B),.clk(clk),.Quotient(div_OQ),.Remainder(div_OR));
	
	Div_Com_32bit DC (.Ctrl(Ctrl),.High(SAH),.Low(AL),.B(B),.clk(clk),.Quotient(div_CQ),.Remainder(div_CR));
	
	And32b An (.x(AL),.y(B),.f(andd));
	
	Or32b O (.x(AL),.y(B),.f(orr));
	
	Xor32b X (.x(AL),.y(B),.f(xorr));
	
	Not32b N (.x(AL),.f(nott));
	
	sra1b sra1 (.A(AL),.S(Ctrl),.Z(sra),.sign(AL[31]));
	
	sll1b sll1 (.A(AL),.S(Ctrl),.Z(sll));
	
	srl1b srl1 (.A(AL),.S(Ctrl),.Z(srl));
	
	circle circ (.A(AL),.S(Ctrl),.Z(cir));
	
	slt32b slt (.A(AL),.B(B),.Z(larger_A),.ZF(equal));//一直在比较补码的大小，不是通过选择器控制的

	mux16to1_64b mux16(.adder(adder),.andd(andd),.orr(orr),.xorr(xorr),.nott(nott),
							 .sll(sll),.sra(sra),.srl(srl),.cir(cir),
							 .mul_O(mul_O),.mul_C(mul_C),
							 .div_OQ(div_OQ),.div_OR(div_OR),.div_CQ(div_CQ),.div_CR(div_CR),
							 .select(SW),.mux16to1_out(ans));
							 
	
endmodule
