`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:40:04 11/14/2017 
// Design Name: 
// Module Name:    Multi_8CH32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Multi_8CH32(input clk,
                   input rst,
						 input EN,
						 input[63:0]LES,
						 input[2:0] Test,
						 input[63:0]point_in,
						 input[31:0]Data0,
						 input[31:0]Data1,
						 input[31:0]Data2,
						 input[31:0]Data3,
						 input[31:0]Data4,
						 input[31:0]Data5,
						 input[31:0]Data6,
						 input[31:0]Data7,
						 output[7:0]point_out,
						 output[7:0]LE_out,
						 output[31:0]Disp_num
    );

    reg[31:0] disp_data=32'hAA5555AA;
	 reg[7:0] cpu_blink=8'b11111111, cpu_point=8'b00000000;

	MUX8T1_32    MUX1_DispData(.I0(Data0),
	                           .I1(Data1),
										.I2(Data2),
	                           .I3(Data3),
										.I4(Data4),
	                           .I5(Data5),
										.I6(Data6),
	                           .I7(Data7),
										.s(Test),
										.O(Disp_num)
	                           );
	
	MUX8T1_8    MUX2_Blink(.I0(cpu_blink),
	                       .I1(LES[15:8]),
								  .I2(LES[23:16]),
								  .I3(LES[31:24]),
								  .I4(LES[39:32]),
								  .I5(LES[47:40]),
								  .I6(LES[55:48]),
								  .I7(LES[63:56]),
								  .s(Test),
								  .O(LE_out)
								 );

	MUX8T1_8    MUX3_Point(.I0(cpu_point),
	                     .I1(LES[15:8]),
								.I2(LES[23:16]),
							   .I3(LES[31:24]),
							   .I4(LES[39:32]),
						   	.I5(LES[47:40]),
								.I6(LES[55:48]),
								.I7(LES[63:56]),
								.s(Test),
								.O(point_out)
								 );
								 
	always@(posedge clk)begin
         if(EN)begin
            disp_data<=Data0;
            cpu_blink<=LES[7:0];
            cpu_point<=point_in[7:0];
         end
         else begin
            disp_data<=disp_data;
            cpu_blink<=cpu_blink;
            cpu_point<=cpu_point;
         end
   end			
endmodule
