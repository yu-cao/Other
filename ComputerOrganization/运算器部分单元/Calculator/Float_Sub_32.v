`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:09:26 04/09/2018 
// Design Name: 
// Module Name:    Float_Sub_32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Float_Sub_32(input clk,input [31:0]A,input [31:0]B,output [31:0]res );
Float_Add_32 sub(.clk(clk),.A(A),.B({~B[31],B[30:0]}),.res(res));

endmodule
