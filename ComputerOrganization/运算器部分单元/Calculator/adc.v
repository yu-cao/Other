// Verilog test fixture created from schematic D:\2017-2018 Spr-Sum\Computer Organization\Lab1_MUX\Hex427\ADC32.sch - Thu Mar 15 19:05:28 2018

`timescale 1ns / 1ps

module ADC32_ADC32_sch_tb();

// Inputs
   reg [31:0] a;
   reg [31:0] b;
   reg C0;

// Output
   wire [31:0] s;
   wire Co;

// Bidirs

// Instantiate the UUT
   ADC32 UUT (
		.s(s), 
		.a(a), 
		.b(b), 
		.C0(C0), 
		.Co(Co)
   );
// Initialize Inputs
   //`ifdef auto_init
       initial begin
		a = 0;
		b = 0;
		C0 = 0;
      a = 32'h00003924;
		b = 32'h00001111;
		#100;
		C0 = 1;
		#100;
	end
	//`endif
endmodule
