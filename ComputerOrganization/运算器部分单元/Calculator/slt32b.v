`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:51:25 04/13/2018 
// Design Name: 
// Module Name:    slt32b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module slt32b(A,B,Z,ZF);
	 input wire [31:0]A;
	 input wire [31:0]B;
	 output wire Z;
	 output wire ZF;
	 
	 assign Z = (((~A[31])&(~B[31])&(A[30:0]>B[30:0]))|((~A[31])&(B[31]))|(A[31])&(B[31])&(A[30:0]>B[30:0]));
	 assign ZF = (A==B);

endmodule
