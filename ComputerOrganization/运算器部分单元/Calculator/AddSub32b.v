`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:12:31 04/10/2018 
// Design Name: 
// Module Name:    AddSub32b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Adder1b(A, B, C, Co, S);
    input A;
    input B;
    input C;
   output Co;
   output S;
   
   wire XLXN_1;
   wire XLXN_6;
   wire XLXN_7;
   wire XLXN_8;
   
   AND2  XLXI_1 (.I0(B),.I1(A),.O(XLXN_7));
   AND2  XLXI_2 (.I0(C),.I1(B),.O(XLXN_6));
   AND2  XLXI_3 (.I0(C),.I1(A),.O(XLXN_8));
   XOR2  XLXI_4 (.I0(B),.I1(A),.O(XLXN_1));
   XOR2  XLXI_5 (.I0(C),.I1(XLXN_1),.O(S));
   OR3  XLXI_6 (.I0(XLXN_8),.I1(XLXN_6),.I2(XLXN_7),.O(Co));

endmodule

module AddSub1b(A,B,Ci,Ctrl,Co,S);

    input A;
    input B;
    input Ci;
    input Ctrl;
   output Co;
   output S;
   
   wire XLXN_2;
   
   Adder1b  XLXI_1 (.A(A),.B(XLXN_2),.C(Ci),.Co(Co),.S(S));
   XOR2  XLXI_5 (.I0(Ctrl),.I1(B),.O(XLXN_2));

endmodule

//32bit加减法器
//Co为溢出
module AddSub32b(A,B,Ctrl,CF,overflow,ZF,S);

    input [31:0] A;
    input [31:0] B;
    input Ctrl;//控制加法or减法，等于1时为减法
    output CF;
	 output overflow;
	 output ZF;
    output [31:0] S;
	 wire Co;
    wire XLXN_1;
    wire XLXI_2;
    wire XLXI_3;
    wire XLXI_4;
    wire XLXI_5;
    wire XLXI_6;
    wire XLXI_7;
    wire XLXI_8;
    wire XLXI_9;
    wire XLXI_10;
    wire XLXI_11;
    wire XLXI_12;
    wire XLXI_13;
    wire XLXI_14;
    wire XLXI_15;
    wire XLXI_16;
    wire XLXI_17;
    wire XLXI_18;
    wire XLXI_19;
    wire XLXI_20;
    wire XLXN_21;
    wire XLXI_22;
    wire XLXI_23;
    wire XLXI_24;
    wire XLXI_25;
    wire XLXI_26;
    wire XLXI_27;
    wire XLXI_28;
    wire XLXI_29;
    wire XLXI_30;
    wire XLXI_31;

    AddSub1b  M0 (.A(A[0]),.B(B[0]),.Ci(Ctrl),.Ctrl(Ctrl),.Co(XLXN_1),.S(S[0]));
    AddSub1b  M1 (.A(A[1]),.B(B[1]),.Ci(XLXN_1),.Ctrl(Ctrl),.Co(XLXN_2),.S(S[1]));
    AddSub1b  M2 (.A(A[2]),.B(B[2]),.Ci(XLXN_2),.Ctrl(Ctrl),.Co(XLXN_3),.S(S[2]));
    AddSub1b  M3 (.A(A[3]),.B(B[3]),.Ci(XLXN_3),.Ctrl(Ctrl),.Co(XLXN_4),.S(S[3]));
    AddSub1b  M4 (.A(A[4]),.B(B[4]),.Ci(XLXN_4),.Ctrl(Ctrl),.Co(XLXN_5),.S(S[4]));
    AddSub1b  M5 (.A(A[5]),.B(B[5]),.Ci(XLXN_5),.Ctrl(Ctrl),.Co(XLXN_6),.S(S[5]));
    AddSub1b  M6 (.A(A[6]),.B(B[6]),.Ci(XLXN_6),.Ctrl(Ctrl),.Co(XLXN_7),.S(S[6]));
    AddSub1b  M7 (.A(A[7]),.B(B[7]),.Ci(XLXN_7),.Ctrl(Ctrl),.Co(XLXN_8),.S(S[7]));
    AddSub1b  M8 (.A(A[8]),.B(B[8]),.Ci(XLXN_8),.Ctrl(Ctrl),.Co(XLXN_9),.S(S[8]));
    AddSub1b  M9 (.A(A[9]),.B(B[9]),.Ci(XLXN_9),.Ctrl(Ctrl),.Co(XLXN_10),.S(S[9]));
    AddSub1b  M10 (.A(A[10]),.B(B[10]),.Ci(XLXN_10),.Ctrl(Ctrl),.Co(XLXN_11),.S(S[10]));
    AddSub1b  M11 (.A(A[11]),.B(B[11]),.Ci(XLXN_11),.Ctrl(Ctrl),.Co(XLXN_12),.S(S[11]));
    AddSub1b  M12 (.A(A[12]),.B(B[12]),.Ci(XLXN_12),.Ctrl(Ctrl),.Co(XLXN_13),.S(S[12]));
    AddSub1b  M13 (.A(A[13]),.B(B[13]),.Ci(XLXN_13),.Ctrl(Ctrl),.Co(XLXN_14),.S(S[13]));
    AddSub1b  M14 (.A(A[14]),.B(B[14]),.Ci(XLXN_14),.Ctrl(Ctrl),.Co(XLXN_15),.S(S[14]));
    AddSub1b  M15 (.A(A[15]),.B(B[15]),.Ci(XLXN_15),.Ctrl(Ctrl),.Co(XLXN_16),.S(S[15]));
    AddSub1b  M16 (.A(A[16]),.B(B[16]),.Ci(XLXN_16),.Ctrl(Ctrl),.Co(XLXN_17),.S(S[16]));
    AddSub1b  M17 (.A(A[17]),.B(B[17]),.Ci(XLXN_17),.Ctrl(Ctrl),.Co(XLXN_18),.S(S[17]));
    AddSub1b  M18 (.A(A[18]),.B(B[18]),.Ci(XLXN_18),.Ctrl(Ctrl),.Co(XLXN_19),.S(S[18]));
    AddSub1b  M19 (.A(A[19]),.B(B[19]),.Ci(XLXN_19),.Ctrl(Ctrl),.Co(XLXN_20),.S(S[19]));
    AddSub1b  M20 (.A(A[20]),.B(B[20]),.Ci(XLXN_20),.Ctrl(Ctrl),.Co(XLXN_21),.S(S[20]));
    AddSub1b  M21 (.A(A[21]),.B(B[21]),.Ci(XLXN_21),.Ctrl(Ctrl),.Co(XLXN_22),.S(S[21]));
    AddSub1b  M22 (.A(A[22]),.B(B[22]),.Ci(XLXN_22),.Ctrl(Ctrl),.Co(XLXN_23),.S(S[22]));
    AddSub1b  M23 (.A(A[23]),.B(B[23]),.Ci(XLXN_23),.Ctrl(Ctrl),.Co(XLXN_24),.S(S[23]));
    AddSub1b  M24 (.A(A[24]),.B(B[24]),.Ci(XLXN_24),.Ctrl(Ctrl),.Co(XLXN_25),.S(S[24]));
    AddSub1b  M25 (.A(A[25]),.B(B[25]),.Ci(XLXN_25),.Ctrl(Ctrl),.Co(XLXN_26),.S(S[25]));
    AddSub1b  M26 (.A(A[26]),.B(B[26]),.Ci(XLXN_26),.Ctrl(Ctrl),.Co(XLXN_27),.S(S[26]));
    AddSub1b  M27 (.A(A[27]),.B(B[27]),.Ci(XLXN_27),.Ctrl(Ctrl),.Co(XLXN_28),.S(S[27]));
    AddSub1b  M28 (.A(A[28]),.B(B[28]),.Ci(XLXN_28),.Ctrl(Ctrl),.Co(XLXN_29),.S(S[28]));
    AddSub1b  M29 (.A(A[29]),.B(B[29]),.Ci(XLXN_29),.Ctrl(Ctrl),.Co(XLXN_30),.S(S[29]));
    AddSub1b  M30 (.A(A[30]),.B(B[30]),.Ci(XLXN_30),.Ctrl(Ctrl),.Co(XLXN_31),.S(S[30]));
    AddSub1b  M31 (.A(A[31]),.B(B[31]),.Ci(XLXN_31),.Ctrl(Ctrl),.Co(Co),.S(S[31]));
	 
	 assign CF = Ctrl^Co;
	 assign overflow = XLXN_31^Co;
	 assign ZF = ~(|S);
endmodule

