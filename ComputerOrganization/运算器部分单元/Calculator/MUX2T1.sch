<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="a(63:0)" />
        <signal name="a(7:0)" />
        <signal name="a(15:8)" />
        <signal name="a(23:16)" />
        <signal name="a(31:24)" />
        <signal name="a(39:32)" />
        <signal name="a(47:40)" />
        <signal name="a(55:48)" />
        <signal name="a(63:56)" />
        <signal name="b(63:0)" />
        <signal name="b(7:0)" />
        <signal name="b(15:8)" />
        <signal name="b(23:16)" />
        <signal name="b(31:24)" />
        <signal name="b(39:32)" />
        <signal name="b(47:40)" />
        <signal name="b(55:48)" />
        <signal name="b(63:56)" />
        <signal name="SW0" />
        <signal name="o(63:0)" />
        <signal name="o(7:0)" />
        <signal name="o(15:8)" />
        <signal name="o(23:16)" />
        <signal name="o(31:24)" />
        <signal name="o(39:32)" />
        <signal name="o(47:40)" />
        <signal name="o(55:48)" />
        <signal name="o(63:56)" />
        <port polarity="Input" name="a(63:0)" />
        <port polarity="Input" name="b(63:0)" />
        <port polarity="Input" name="SW0" />
        <port polarity="Output" name="o(63:0)" />
        <blockdef name="MUX2T1_8">
            <timestamp>2017-11-13T12:32:16</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <block symbolname="MUX2T1_8" name="XLXI_1">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(23:16)" name="I1(7:0)" />
            <blockpin signalname="a(23:16)" name="I0(7:0)" />
            <blockpin signalname="o(23:16)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_2">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(31:24)" name="I1(7:0)" />
            <blockpin signalname="a(31:24)" name="I0(7:0)" />
            <blockpin signalname="o(31:24)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_3">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(39:32)" name="I1(7:0)" />
            <blockpin signalname="a(39:32)" name="I0(7:0)" />
            <blockpin signalname="o(39:32)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_4">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(47:40)" name="I1(7:0)" />
            <blockpin signalname="a(47:40)" name="I0(7:0)" />
            <blockpin signalname="o(47:40)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_5">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(55:48)" name="I1(7:0)" />
            <blockpin signalname="a(55:48)" name="I0(7:0)" />
            <blockpin signalname="o(55:48)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_6">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(63:56)" name="I1(7:0)" />
            <blockpin signalname="a(63:56)" name="I0(7:0)" />
            <blockpin signalname="o(63:56)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_7">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(7:0)" name="I1(7:0)" />
            <blockpin signalname="a(7:0)" name="I0(7:0)" />
            <blockpin signalname="o(7:0)" name="o(7:0)" />
        </block>
        <block symbolname="MUX2T1_8" name="XLXI_8">
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="b(15:8)" name="I1(7:0)" />
            <blockpin signalname="a(15:8)" name="I0(7:0)" />
            <blockpin signalname="o(15:8)" name="o(7:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1360" y="1024" name="XLXI_1" orien="R0">
        </instance>
        <instance x="1360" y="1360" name="XLXI_2" orien="R0">
        </instance>
        <instance x="1360" y="1680" name="XLXI_3" orien="R0">
        </instance>
        <instance x="1360" y="1968" name="XLXI_4" orien="R0">
        </instance>
        <instance x="1360" y="2288" name="XLXI_5" orien="R0">
        </instance>
        <instance x="1360" y="2640" name="XLXI_6" orien="R0">
        </instance>
        <instance x="1360" y="416" name="XLXI_7" orien="R0">
        </instance>
        <branch name="a(63:0)">
            <wire x2="1008" y1="1152" y2="1152" x1="880" />
            <wire x2="1008" y1="1152" y2="1328" x1="1008" />
            <wire x2="1008" y1="1328" y2="1648" x1="1008" />
            <wire x2="1008" y1="1648" y2="1936" x1="1008" />
            <wire x2="1008" y1="1936" y2="2256" x1="1008" />
            <wire x2="1008" y1="2256" y2="2608" x1="1008" />
            <wire x2="1008" y1="2608" y2="2624" x1="1008" />
            <wire x2="1008" y1="192" y2="384" x1="1008" />
            <wire x2="1008" y1="384" y2="704" x1="1008" />
            <wire x2="1008" y1="704" y2="992" x1="1008" />
            <wire x2="1008" y1="992" y2="1152" x1="1008" />
        </branch>
        <bustap x2="1104" y1="384" y2="384" x1="1008" />
        <bustap x2="1104" y1="704" y2="704" x1="1008" />
        <bustap x2="1104" y1="992" y2="992" x1="1008" />
        <bustap x2="1104" y1="1328" y2="1328" x1="1008" />
        <bustap x2="1104" y1="1648" y2="1648" x1="1008" />
        <bustap x2="1104" y1="1936" y2="1936" x1="1008" />
        <bustap x2="1104" y1="2256" y2="2256" x1="1008" />
        <bustap x2="1104" y1="2608" y2="2608" x1="1008" />
        <branch name="a(7:0)">
            <wire x2="1360" y1="384" y2="384" x1="1104" />
        </branch>
        <branch name="a(15:8)">
            <wire x2="1360" y1="704" y2="704" x1="1104" />
        </branch>
        <branch name="a(23:16)">
            <wire x2="1360" y1="992" y2="992" x1="1104" />
        </branch>
        <branch name="a(31:24)">
            <wire x2="1360" y1="1328" y2="1328" x1="1104" />
        </branch>
        <branch name="a(39:32)">
            <wire x2="1360" y1="1648" y2="1648" x1="1104" />
        </branch>
        <branch name="a(47:40)">
            <wire x2="1360" y1="1936" y2="1936" x1="1104" />
        </branch>
        <branch name="a(55:48)">
            <wire x2="1360" y1="2256" y2="2256" x1="1104" />
        </branch>
        <branch name="a(63:56)">
            <wire x2="1360" y1="2608" y2="2608" x1="1104" />
        </branch>
        <instance x="1360" y="736" name="XLXI_8" orien="R0">
        </instance>
        <branch name="b(63:0)">
            <wire x2="1104" y1="1216" y2="1216" x1="880" />
            <wire x2="1104" y1="1216" y2="1264" x1="1104" />
            <wire x2="1104" y1="1264" y2="1584" x1="1104" />
            <wire x2="1104" y1="1584" y2="1872" x1="1104" />
            <wire x2="1104" y1="1872" y2="2192" x1="1104" />
            <wire x2="1104" y1="2192" y2="2544" x1="1104" />
            <wire x2="1104" y1="2544" y2="2576" x1="1104" />
            <wire x2="1104" y1="192" y2="320" x1="1104" />
            <wire x2="1104" y1="320" y2="640" x1="1104" />
            <wire x2="1104" y1="640" y2="928" x1="1104" />
            <wire x2="1104" y1="928" y2="1216" x1="1104" />
        </branch>
        <bustap x2="1200" y1="320" y2="320" x1="1104" />
        <bustap x2="1200" y1="640" y2="640" x1="1104" />
        <bustap x2="1200" y1="928" y2="928" x1="1104" />
        <bustap x2="1200" y1="1264" y2="1264" x1="1104" />
        <bustap x2="1200" y1="1584" y2="1584" x1="1104" />
        <bustap x2="1200" y1="1872" y2="1872" x1="1104" />
        <bustap x2="1200" y1="2192" y2="2192" x1="1104" />
        <bustap x2="1200" y1="2544" y2="2544" x1="1104" />
        <branch name="b(7:0)">
            <wire x2="1360" y1="320" y2="320" x1="1200" />
        </branch>
        <branch name="b(15:8)">
            <wire x2="1360" y1="640" y2="640" x1="1200" />
        </branch>
        <branch name="b(23:16)">
            <wire x2="1360" y1="928" y2="928" x1="1200" />
        </branch>
        <branch name="b(31:24)">
            <wire x2="1360" y1="1264" y2="1264" x1="1200" />
        </branch>
        <branch name="b(39:32)">
            <wire x2="1360" y1="1584" y2="1584" x1="1200" />
        </branch>
        <branch name="b(47:40)">
            <wire x2="1360" y1="1872" y2="1872" x1="1200" />
        </branch>
        <branch name="b(55:48)">
            <wire x2="1360" y1="2192" y2="2192" x1="1200" />
        </branch>
        <branch name="b(63:56)">
            <wire x2="1360" y1="2544" y2="2544" x1="1200" />
        </branch>
        <branch name="SW0">
            <wire x2="1216" y1="64" y2="64" x1="784" />
            <wire x2="1216" y1="64" y2="256" x1="1216" />
            <wire x2="1360" y1="256" y2="256" x1="1216" />
            <wire x2="1216" y1="256" y2="576" x1="1216" />
            <wire x2="1360" y1="576" y2="576" x1="1216" />
            <wire x2="1216" y1="576" y2="864" x1="1216" />
            <wire x2="1360" y1="864" y2="864" x1="1216" />
            <wire x2="1216" y1="864" y2="1200" x1="1216" />
            <wire x2="1360" y1="1200" y2="1200" x1="1216" />
            <wire x2="1216" y1="1200" y2="1520" x1="1216" />
            <wire x2="1360" y1="1520" y2="1520" x1="1216" />
            <wire x2="1216" y1="1520" y2="1808" x1="1216" />
            <wire x2="1360" y1="1808" y2="1808" x1="1216" />
            <wire x2="1216" y1="1808" y2="2128" x1="1216" />
            <wire x2="1360" y1="2128" y2="2128" x1="1216" />
            <wire x2="1216" y1="2128" y2="2480" x1="1216" />
            <wire x2="1360" y1="2480" y2="2480" x1="1216" />
        </branch>
        <branch name="o(63:0)">
            <wire x2="2080" y1="176" y2="256" x1="2080" />
            <wire x2="2080" y1="256" y2="576" x1="2080" />
            <wire x2="2080" y1="576" y2="864" x1="2080" />
            <wire x2="2080" y1="864" y2="1200" x1="2080" />
            <wire x2="2080" y1="1200" y2="1520" x1="2080" />
            <wire x2="2080" y1="1520" y2="1648" x1="2080" />
            <wire x2="2320" y1="1648" y2="1648" x1="2080" />
            <wire x2="2080" y1="1648" y2="1808" x1="2080" />
            <wire x2="2080" y1="1808" y2="2128" x1="2080" />
            <wire x2="2080" y1="2128" y2="2480" x1="2080" />
            <wire x2="2080" y1="2480" y2="2688" x1="2080" />
        </branch>
        <bustap x2="1984" y1="256" y2="256" x1="2080" />
        <bustap x2="1984" y1="576" y2="576" x1="2080" />
        <bustap x2="1984" y1="864" y2="864" x1="2080" />
        <bustap x2="1984" y1="1200" y2="1200" x1="2080" />
        <bustap x2="1984" y1="1520" y2="1520" x1="2080" />
        <bustap x2="1984" y1="1808" y2="1808" x1="2080" />
        <bustap x2="1984" y1="2128" y2="2128" x1="2080" />
        <bustap x2="1984" y1="2480" y2="2480" x1="2080" />
        <branch name="o(7:0)">
            <wire x2="1984" y1="256" y2="256" x1="1744" />
        </branch>
        <branch name="o(15:8)">
            <wire x2="1984" y1="576" y2="576" x1="1744" />
        </branch>
        <branch name="o(23:16)">
            <wire x2="1984" y1="864" y2="864" x1="1744" />
        </branch>
        <branch name="o(31:24)">
            <wire x2="1984" y1="1200" y2="1200" x1="1744" />
        </branch>
        <branch name="o(39:32)">
            <wire x2="1984" y1="1520" y2="1520" x1="1744" />
        </branch>
        <branch name="o(47:40)">
            <wire x2="1984" y1="1808" y2="1808" x1="1744" />
        </branch>
        <branch name="o(55:48)">
            <wire x2="1984" y1="2128" y2="2128" x1="1744" />
        </branch>
        <branch name="o(63:56)">
            <wire x2="1984" y1="2480" y2="2480" x1="1744" />
        </branch>
        <iomarker fontsize="28" x="880" y="1152" name="a(63:0)" orien="R180" />
        <iomarker fontsize="28" x="784" y="64" name="SW0" orien="R180" />
        <iomarker fontsize="28" x="2320" y="1648" name="o(63:0)" orien="R0" />
        <iomarker fontsize="28" x="880" y="1216" name="b(63:0)" orien="R180" />
    </sheet>
</drawing>