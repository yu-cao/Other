`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:36:45 04/18/2018
// Design Name:   ILU
// Module Name:   D:/2017-2018 Spr-Sum/Computer Organization/Frame8/ILU_SIM.v
// Project Name:  Frame8
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ILU
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ILU_SIM;

	// Inputs
	reg clk;
	reg [3:0] SW;
	reg Ctrl;
	reg SAH;
	reg [31:0] AL;
	reg [31:0] B;

	// Outputs
	wire [63:0] ans;
	wire overflow;
	wire larger_A;
	wire equal;

	// Instantiate the Unit Under Test (UUT)
	ILU uut (
		.clk(clk), 
		.SW(SW), 
		.Ctrl(Ctrl), 
		.SAH(SAH), 
		.AL(AL), 
		.B(B), 
		.ans(ans), 
		.overflow(overflow), 
		.larger_A(larger_A), 
		.equal(equal)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		SW = 4'b1011;
		Ctrl = 0;
		SAH = 0;
		AL = 0;
		B = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		fork
		forever #2 clk = ~clk;
		begin
		B = 32'h00000023;
		AL = 32'h00000000;
		#5;
		SAH = 1;
		#5;
		AL = 32'h00000033;
		#5;
		SAH = 0;
		#500;
		B = 32'h00000006;
		AL = 32'h80000000;
		#5;
		SAH = 1;
		#5;
		AL = 32'h00000022;
		#5;
		SAH = 0;
		#500;
		B = 32'h00000006;
		AL = 32'h00000022;
		#5;
		SAH = 1;
		#5;
		AL = 32'h00000022;
		#5;
		SAH = 0;
		end
		join
	end
      
endmodule

