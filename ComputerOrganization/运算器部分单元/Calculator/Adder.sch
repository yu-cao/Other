<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="Co" />
        <signal name="XLXN_2" />
        <signal name="a(31:0)" />
        <signal name="b(31:0)" />
        <signal name="XLXN_6(31:0)" />
        <signal name="o(31:0)" />
        <signal name="C0" />
        <port polarity="Input" name="Co" />
        <port polarity="Input" name="a(31:0)" />
        <port polarity="Input" name="b(31:0)" />
        <port polarity="Output" name="o(31:0)" />
        <port polarity="Output" name="C0" />
        <blockdef name="ADC32">
            <timestamp>2018-3-15T10:44:9</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Data_B_Trans">
            <timestamp>2018-3-15T11:12:55</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <block symbolname="ADC32" name="XLXI_1">
            <blockpin signalname="a(31:0)" name="a(31:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="b(31:0)" />
            <blockpin signalname="Co" name="C0" />
            <blockpin signalname="o(31:0)" name="s(31:0)" />
            <blockpin signalname="C0" name="Co" />
        </block>
        <block symbolname="Data_B_Trans" name="XLXI_2">
            <blockpin signalname="Co" name="SW" />
            <blockpin signalname="b(31:0)" name="b(31:0)" />
            <blockpin signalname="XLXN_6(31:0)" name="o(31:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1712" y="1104" name="XLXI_1" orien="R0">
        </instance>
        <branch name="Co">
            <wire x2="1072" y1="1072" y2="1072" x1="1040" />
            <wire x2="1072" y1="1072" y2="1216" x1="1072" />
            <wire x2="1152" y1="1216" y2="1216" x1="1072" />
            <wire x2="1712" y1="1072" y2="1072" x1="1072" />
        </branch>
        <iomarker fontsize="28" x="1040" y="1072" name="Co" orien="R180" />
        <instance x="1152" y="1312" name="XLXI_2" orien="R0">
        </instance>
        <branch name="a(31:0)">
            <wire x2="1712" y1="944" y2="944" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="944" name="a(31:0)" orien="R180" />
        <iomarker fontsize="28" x="896" y="1008" name="b(31:0)" orien="R180" />
        <branch name="b(31:0)">
            <wire x2="928" y1="1008" y2="1008" x1="896" />
            <wire x2="928" y1="1008" y2="1280" x1="928" />
            <wire x2="1152" y1="1280" y2="1280" x1="928" />
        </branch>
        <branch name="XLXN_6(31:0)">
            <wire x2="1616" y1="1216" y2="1216" x1="1536" />
            <wire x2="1616" y1="1008" y2="1216" x1="1616" />
            <wire x2="1712" y1="1008" y2="1008" x1="1616" />
        </branch>
        <branch name="o(31:0)">
            <wire x2="2128" y1="944" y2="944" x1="2096" />
        </branch>
        <iomarker fontsize="28" x="2128" y="944" name="o(31:0)" orien="R0" />
        <branch name="C0">
            <wire x2="2128" y1="1072" y2="1072" x1="2096" />
        </branch>
        <iomarker fontsize="28" x="2128" y="1072" name="C0" orien="R0" />
    </sheet>
</drawing>