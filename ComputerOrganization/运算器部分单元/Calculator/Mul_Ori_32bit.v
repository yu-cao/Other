`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:39:20 03/29/2018 
// Design Name: 
// Module Name:    Mul_Ori_4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Mul_Ori_32bit (
    input [31:0]A,
	 input [31:0]B,
	 input clk,
	 output reg[63:0]S
    );
	 
	 reg [63:0]tres,temp;
	 reg [1:0]state=0;
	 reg [5:0]count;
	 reg [31:0]b_reg;
	 
	 parameter Start = 2'b0, Process = 2'b10, End = 2'b11;

    always @(posedge clk) begin
        case (state)
            Start: begin
                count <= 6'b0;
                tres <= 64'b0;
                b_reg <= B;
                temp <= {32'b0, A};
                state <= Process;
            end
            Process: begin
                if(count == 6'b100000)
                    state <= End;
                else begin
                    if(b_reg[0] == 1'b1)
                        tres <= tres + temp;
                    b_reg <= b_reg >> 1;
                    temp <= temp << 1;
                    count <= count + 1;
                end
            end
            End: begin
                S <= tres;
                state <= Start;
            end
			   default:begin
			       state <= Start;
			   end
        endcase
    end			
	     
endmodule       