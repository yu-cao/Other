<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="s(1:0)" />
        <signal name="XLXN_4" />
        <signal name="XLXN_7" />
        <signal name="s(0)" />
        <signal name="s(1)" />
        <signal name="XLXN_104" />
        <signal name="XLXN_107" />
        <signal name="XLXN_110" />
        <signal name="XLXN_113" />
        <signal name="I0(3:0)" />
        <signal name="I1(3:0)" />
        <signal name="I2(3:0)" />
        <signal name="I3(3:0)" />
        <signal name="I0(0)" />
        <signal name="I0(1)" />
        <signal name="I0(2)" />
        <signal name="I0(3)" />
        <signal name="I1(0)" />
        <signal name="I1(1)" />
        <signal name="I1(2)" />
        <signal name="I1(3)" />
        <signal name="I2(0)" />
        <signal name="I2(1)" />
        <signal name="I2(2)" />
        <signal name="I2(3)" />
        <signal name="XLXN_21" />
        <signal name="XLXN_22" />
        <signal name="XLXN_23" />
        <signal name="XLXN_87" />
        <signal name="XLXN_89" />
        <signal name="XLXN_90" />
        <signal name="XLXN_92" />
        <signal name="XLXN_93" />
        <signal name="XLXN_94" />
        <signal name="XLXN_95" />
        <signal name="XLXN_97" />
        <signal name="XLXN_98" />
        <signal name="I3(0)" />
        <signal name="I3(1)" />
        <signal name="I3(2)" />
        <signal name="I3(3)" />
        <signal name="XLXN_99" />
        <signal name="XLXN_100" />
        <signal name="XLXN_102" />
        <signal name="XLXN_103" />
        <signal name="O(3:0)" />
        <signal name="O(0)" />
        <signal name="O(1)" />
        <signal name="O(2)" />
        <signal name="O(3)" />
        <port polarity="Input" name="s(1:0)" />
        <port polarity="Input" name="I0(3:0)" />
        <port polarity="Input" name="I1(3:0)" />
        <port polarity="Input" name="I2(3:0)" />
        <port polarity="Input" name="I3(3:0)" />
        <port polarity="Output" name="O(3:0)" />
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <blockdef name="or4">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="48" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="48" y1="-256" y2="-256" x1="0" />
            <line x2="192" y1="-160" y2="-160" x1="256" />
            <arc ex="112" ey="-208" sx="192" sy="-160" r="88" cx="116" cy="-120" />
            <line x2="48" y1="-208" y2="-208" x1="112" />
            <line x2="48" y1="-112" y2="-112" x1="112" />
            <line x2="48" y1="-256" y2="-208" x1="48" />
            <line x2="48" y1="-64" y2="-112" x1="48" />
            <arc ex="48" ey="-208" sx="48" sy="-112" r="56" cx="16" cy="-160" />
            <arc ex="192" ey="-160" sx="112" sy="-112" r="88" cx="116" cy="-200" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <block symbolname="and2" name="XLXI_1">
            <blockpin signalname="XLXN_7" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="XLXN_104" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_3">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="XLXN_7" name="I1" />
            <blockpin signalname="XLXN_110" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_11">
            <blockpin signalname="s(1)" name="I" />
            <blockpin signalname="XLXN_4" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_12">
            <blockpin signalname="s(0)" name="I" />
            <blockpin signalname="XLXN_7" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_2">
            <blockpin signalname="s(0)" name="I0" />
            <blockpin signalname="XLXN_4" name="I1" />
            <blockpin signalname="XLXN_107" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_4">
            <blockpin signalname="s(1)" name="I0" />
            <blockpin signalname="s(0)" name="I1" />
            <blockpin signalname="XLXN_113" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_5">
            <blockpin signalname="I0(0)" name="I0" />
            <blockpin signalname="XLXN_104" name="I1" />
            <blockpin signalname="XLXN_21" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_7">
            <blockpin signalname="I2(0)" name="I0" />
            <blockpin signalname="XLXN_110" name="I1" />
            <blockpin signalname="XLXN_23" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_8">
            <blockpin signalname="I3(0)" name="I0" />
            <blockpin signalname="XLXN_113" name="I1" />
            <blockpin signalname="XLXN_87" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_6">
            <blockpin signalname="I1(0)" name="I0" />
            <blockpin signalname="XLXN_107" name="I1" />
            <blockpin signalname="XLXN_22" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_9">
            <blockpin signalname="XLXN_87" name="I0" />
            <blockpin signalname="XLXN_23" name="I1" />
            <blockpin signalname="XLXN_22" name="I2" />
            <blockpin signalname="XLXN_21" name="I3" />
            <blockpin signalname="O(0)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_35">
            <blockpin signalname="I0(1)" name="I0" />
            <blockpin signalname="XLXN_104" name="I1" />
            <blockpin signalname="XLXN_89" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_36">
            <blockpin signalname="I2(1)" name="I0" />
            <blockpin signalname="XLXN_110" name="I1" />
            <blockpin signalname="XLXN_92" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_37">
            <blockpin signalname="I3(1)" name="I0" />
            <blockpin signalname="XLXN_113" name="I1" />
            <blockpin signalname="XLXN_93" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_38">
            <blockpin signalname="I1(1)" name="I0" />
            <blockpin signalname="XLXN_107" name="I1" />
            <blockpin signalname="XLXN_90" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_39">
            <blockpin signalname="XLXN_93" name="I0" />
            <blockpin signalname="XLXN_92" name="I1" />
            <blockpin signalname="XLXN_90" name="I2" />
            <blockpin signalname="XLXN_89" name="I3" />
            <blockpin signalname="O(1)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_40">
            <blockpin signalname="I0(2)" name="I0" />
            <blockpin signalname="XLXN_104" name="I1" />
            <blockpin signalname="XLXN_94" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_41">
            <blockpin signalname="I2(2)" name="I0" />
            <blockpin signalname="XLXN_110" name="I1" />
            <blockpin signalname="XLXN_97" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_42">
            <blockpin signalname="I3(2)" name="I0" />
            <blockpin signalname="XLXN_113" name="I1" />
            <blockpin signalname="XLXN_98" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_43">
            <blockpin signalname="I1(2)" name="I0" />
            <blockpin signalname="XLXN_107" name="I1" />
            <blockpin signalname="XLXN_95" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_44">
            <blockpin signalname="XLXN_98" name="I0" />
            <blockpin signalname="XLXN_97" name="I1" />
            <blockpin signalname="XLXN_95" name="I2" />
            <blockpin signalname="XLXN_94" name="I3" />
            <blockpin signalname="O(2)" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_45">
            <blockpin signalname="I0(3)" name="I0" />
            <blockpin signalname="XLXN_104" name="I1" />
            <blockpin signalname="XLXN_99" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_46">
            <blockpin signalname="I2(3)" name="I0" />
            <blockpin signalname="XLXN_110" name="I1" />
            <blockpin signalname="XLXN_102" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_47">
            <blockpin signalname="I3(3)" name="I0" />
            <blockpin signalname="XLXN_113" name="I1" />
            <blockpin signalname="XLXN_103" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_48">
            <blockpin signalname="I1(3)" name="I0" />
            <blockpin signalname="XLXN_107" name="I1" />
            <blockpin signalname="XLXN_100" name="O" />
        </block>
        <block symbolname="or4" name="XLXI_49">
            <blockpin signalname="XLXN_103" name="I0" />
            <blockpin signalname="XLXN_102" name="I1" />
            <blockpin signalname="XLXN_100" name="I2" />
            <blockpin signalname="XLXN_99" name="I3" />
            <blockpin signalname="O(3)" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="512" y="128" name="XLXI_11" orien="R0" />
        <instance x="512" y="288" name="XLXI_12" orien="R0" />
        <branch name="s(1:0)">
            <wire x2="240" y1="176" y2="176" x1="176" />
            <wire x2="240" y1="176" y2="256" x1="240" />
            <wire x2="240" y1="96" y2="176" x1="240" />
        </branch>
        <branch name="s(0)">
            <wire x2="400" y1="256" y2="256" x1="336" />
            <wire x2="512" y1="256" y2="256" x1="400" />
            <wire x2="400" y1="256" y2="384" x1="400" />
            <wire x2="1008" y1="384" y2="384" x1="400" />
            <wire x2="400" y1="384" y2="704" x1="400" />
            <wire x2="1008" y1="704" y2="704" x1="400" />
        </branch>
        <branch name="s(1)">
            <wire x2="448" y1="96" y2="96" x1="336" />
            <wire x2="512" y1="96" y2="96" x1="448" />
            <wire x2="448" y1="96" y2="576" x1="448" />
            <wire x2="448" y1="576" y2="768" x1="448" />
            <wire x2="1008" y1="768" y2="768" x1="448" />
            <wire x2="1008" y1="576" y2="576" x1="448" />
        </branch>
        <iomarker fontsize="28" x="176" y="176" name="s(1:0)" orien="R180" />
        <instance x="1008" y="256" name="XLXI_1" orien="R0" />
        <branch name="XLXN_7">
            <wire x2="784" y1="256" y2="256" x1="736" />
            <wire x2="784" y1="256" y2="512" x1="784" />
            <wire x2="1008" y1="512" y2="512" x1="784" />
            <wire x2="784" y1="192" y2="256" x1="784" />
            <wire x2="1008" y1="192" y2="192" x1="784" />
        </branch>
        <instance x="1008" y="448" name="XLXI_2" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="848" y1="96" y2="96" x1="736" />
            <wire x2="848" y1="96" y2="128" x1="848" />
            <wire x2="848" y1="128" y2="320" x1="848" />
            <wire x2="1008" y1="320" y2="320" x1="848" />
            <wire x2="1008" y1="128" y2="128" x1="848" />
        </branch>
        <instance x="1008" y="640" name="XLXI_3" orien="R0" />
        <instance x="1008" y="832" name="XLXI_4" orien="R0" />
        <branch name="XLXN_104">
            <wire x2="1328" y1="160" y2="160" x1="1264" />
            <wire x2="1328" y1="160" y2="1008" x1="1328" />
            <wire x2="1328" y1="1008" y2="1840" x1="1328" />
            <wire x2="1968" y1="1840" y2="1840" x1="1328" />
            <wire x2="1328" y1="1840" y2="2688" x1="1328" />
            <wire x2="1968" y1="2688" y2="2688" x1="1328" />
            <wire x2="1952" y1="1008" y2="1008" x1="1328" />
            <wire x2="1936" y1="160" y2="160" x1="1328" />
        </branch>
        <branch name="XLXN_107">
            <wire x2="1376" y1="352" y2="352" x1="1264" />
            <wire x2="1376" y1="352" y2="1200" x1="1376" />
            <wire x2="1376" y1="1200" y2="2032" x1="1376" />
            <wire x2="1968" y1="2032" y2="2032" x1="1376" />
            <wire x2="1376" y1="2032" y2="2880" x1="1376" />
            <wire x2="1968" y1="2880" y2="2880" x1="1376" />
            <wire x2="1952" y1="1200" y2="1200" x1="1376" />
            <wire x2="1936" y1="352" y2="352" x1="1376" />
        </branch>
        <branch name="XLXN_110">
            <wire x2="1440" y1="544" y2="544" x1="1264" />
            <wire x2="1440" y1="544" y2="1392" x1="1440" />
            <wire x2="1440" y1="1392" y2="2224" x1="1440" />
            <wire x2="1968" y1="2224" y2="2224" x1="1440" />
            <wire x2="1440" y1="2224" y2="3072" x1="1440" />
            <wire x2="1968" y1="3072" y2="3072" x1="1440" />
            <wire x2="1952" y1="1392" y2="1392" x1="1440" />
            <wire x2="1936" y1="544" y2="544" x1="1440" />
        </branch>
        <branch name="XLXN_113">
            <wire x2="1504" y1="736" y2="736" x1="1264" />
            <wire x2="1504" y1="736" y2="1584" x1="1504" />
            <wire x2="1504" y1="1584" y2="2416" x1="1504" />
            <wire x2="1968" y1="2416" y2="2416" x1="1504" />
            <wire x2="1504" y1="2416" y2="3264" x1="1504" />
            <wire x2="1968" y1="3264" y2="3264" x1="1504" />
            <wire x2="1952" y1="1584" y2="1584" x1="1504" />
            <wire x2="1936" y1="736" y2="736" x1="1504" />
        </branch>
        <branch name="I0(3:0)">
            <wire x2="1552" y1="1488" y2="1488" x1="400" />
            <wire x2="1552" y1="1488" y2="1904" x1="1552" />
            <wire x2="1552" y1="1904" y2="2752" x1="1552" />
            <wire x2="1552" y1="2752" y2="2784" x1="1552" />
            <wire x2="1552" y1="192" y2="224" x1="1552" />
            <wire x2="1552" y1="224" y2="1072" x1="1552" />
            <wire x2="1552" y1="1072" y2="1488" x1="1552" />
        </branch>
        <branch name="I1(3:0)">
            <wire x2="1632" y1="1632" y2="1632" x1="400" />
            <wire x2="1632" y1="1632" y2="2096" x1="1632" />
            <wire x2="1632" y1="2096" y2="2944" x1="1632" />
            <wire x2="1632" y1="2944" y2="3008" x1="1632" />
            <wire x2="1632" y1="384" y2="416" x1="1632" />
            <wire x2="1632" y1="416" y2="1264" x1="1632" />
            <wire x2="1632" y1="1264" y2="1632" x1="1632" />
        </branch>
        <branch name="I2(3:0)">
            <wire x2="1712" y1="1744" y2="1744" x1="400" />
            <wire x2="1712" y1="1744" y2="2288" x1="1712" />
            <wire x2="1712" y1="2288" y2="3136" x1="1712" />
            <wire x2="1712" y1="3136" y2="3152" x1="1712" />
            <wire x2="1712" y1="576" y2="608" x1="1712" />
            <wire x2="1712" y1="608" y2="1456" x1="1712" />
            <wire x2="1712" y1="1456" y2="1744" x1="1712" />
        </branch>
        <branch name="I3(3:0)">
            <wire x2="1792" y1="1856" y2="1856" x1="400" />
            <wire x2="1792" y1="1856" y2="2480" x1="1792" />
            <wire x2="1792" y1="2480" y2="3328" x1="1792" />
            <wire x2="1792" y1="3328" y2="3376" x1="1792" />
            <wire x2="1792" y1="768" y2="800" x1="1792" />
            <wire x2="1792" y1="800" y2="1648" x1="1792" />
            <wire x2="1792" y1="1648" y2="1856" x1="1792" />
        </branch>
        <iomarker fontsize="28" x="400" y="1488" name="I0(3:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="1632" name="I1(3:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="1744" name="I2(3:0)" orien="R180" />
        <iomarker fontsize="28" x="400" y="1856" name="I3(3:0)" orien="R180" />
        <bustap x2="1648" y1="224" y2="224" x1="1552" />
        <bustap x2="1648" y1="1072" y2="1072" x1="1552" />
        <bustap x2="1648" y1="1904" y2="1904" x1="1552" />
        <bustap x2="1648" y1="2752" y2="2752" x1="1552" />
        <branch name="I0(0)">
            <wire x2="1664" y1="224" y2="224" x1="1648" />
            <wire x2="1936" y1="224" y2="224" x1="1664" />
        </branch>
        <branch name="I0(1)">
            <wire x2="1664" y1="1072" y2="1072" x1="1648" />
            <wire x2="1952" y1="1072" y2="1072" x1="1664" />
        </branch>
        <branch name="I0(2)">
            <wire x2="1664" y1="1904" y2="1904" x1="1648" />
            <wire x2="1968" y1="1904" y2="1904" x1="1664" />
        </branch>
        <bustap x2="1728" y1="416" y2="416" x1="1632" />
        <bustap x2="1728" y1="1264" y2="1264" x1="1632" />
        <bustap x2="1728" y1="2096" y2="2096" x1="1632" />
        <bustap x2="1728" y1="2944" y2="2944" x1="1632" />
        <branch name="I1(0)">
            <wire x2="1744" y1="416" y2="416" x1="1728" />
            <wire x2="1936" y1="416" y2="416" x1="1744" />
        </branch>
        <branch name="I1(1)">
            <wire x2="1744" y1="1264" y2="1264" x1="1728" />
            <wire x2="1952" y1="1264" y2="1264" x1="1744" />
        </branch>
        <branch name="I1(2)">
            <wire x2="1744" y1="2096" y2="2096" x1="1728" />
            <wire x2="1968" y1="2096" y2="2096" x1="1744" />
        </branch>
        <bustap x2="1808" y1="608" y2="608" x1="1712" />
        <bustap x2="1808" y1="1456" y2="1456" x1="1712" />
        <bustap x2="1808" y1="2288" y2="2288" x1="1712" />
        <bustap x2="1808" y1="3136" y2="3136" x1="1712" />
        <branch name="I2(0)">
            <wire x2="1824" y1="608" y2="608" x1="1808" />
            <wire x2="1936" y1="608" y2="608" x1="1824" />
        </branch>
        <branch name="I2(1)">
            <wire x2="1824" y1="1456" y2="1456" x1="1808" />
            <wire x2="1952" y1="1456" y2="1456" x1="1824" />
        </branch>
        <branch name="I2(2)">
            <wire x2="1824" y1="2288" y2="2288" x1="1808" />
            <wire x2="1968" y1="2288" y2="2288" x1="1824" />
        </branch>
        <bustap x2="1888" y1="800" y2="800" x1="1792" />
        <bustap x2="1888" y1="1648" y2="1648" x1="1792" />
        <bustap x2="1888" y1="2480" y2="2480" x1="1792" />
        <bustap x2="1888" y1="3328" y2="3328" x1="1792" />
        <branch name="XLXN_21">
            <wire x2="2400" y1="192" y2="192" x1="2192" />
            <wire x2="2400" y1="192" y2="368" x1="2400" />
            <wire x2="2560" y1="368" y2="368" x1="2400" />
        </branch>
        <instance x="1936" y="288" name="XLXI_5" orien="R0" />
        <instance x="1936" y="672" name="XLXI_7" orien="R0" />
        <instance x="1936" y="864" name="XLXI_8" orien="R0" />
        <instance x="1936" y="480" name="XLXI_6" orien="R0" />
        <instance x="2560" y="624" name="XLXI_9" orien="R0" />
        <branch name="XLXN_22">
            <wire x2="2208" y1="384" y2="384" x1="2192" />
            <wire x2="2208" y1="384" y2="432" x1="2208" />
            <wire x2="2560" y1="432" y2="432" x1="2208" />
        </branch>
        <branch name="XLXN_23">
            <wire x2="2208" y1="576" y2="576" x1="2192" />
            <wire x2="2560" y1="496" y2="496" x1="2208" />
            <wire x2="2208" y1="496" y2="576" x1="2208" />
        </branch>
        <branch name="XLXN_87">
            <wire x2="2416" y1="768" y2="768" x1="2192" />
            <wire x2="2416" y1="560" y2="768" x1="2416" />
            <wire x2="2560" y1="560" y2="560" x1="2416" />
        </branch>
        <branch name="XLXN_89">
            <wire x2="2416" y1="1040" y2="1040" x1="2208" />
            <wire x2="2416" y1="1040" y2="1216" x1="2416" />
            <wire x2="2576" y1="1216" y2="1216" x1="2416" />
        </branch>
        <instance x="1952" y="1136" name="XLXI_35" orien="R0" />
        <instance x="1952" y="1520" name="XLXI_36" orien="R0" />
        <instance x="1952" y="1712" name="XLXI_37" orien="R0" />
        <instance x="1952" y="1328" name="XLXI_38" orien="R0" />
        <instance x="2576" y="1472" name="XLXI_39" orien="R0" />
        <branch name="XLXN_90">
            <wire x2="2224" y1="1232" y2="1232" x1="2208" />
            <wire x2="2224" y1="1232" y2="1280" x1="2224" />
            <wire x2="2576" y1="1280" y2="1280" x1="2224" />
        </branch>
        <branch name="XLXN_92">
            <wire x2="2224" y1="1424" y2="1424" x1="2208" />
            <wire x2="2576" y1="1344" y2="1344" x1="2224" />
            <wire x2="2224" y1="1344" y2="1424" x1="2224" />
        </branch>
        <branch name="XLXN_93">
            <wire x2="2432" y1="1616" y2="1616" x1="2208" />
            <wire x2="2432" y1="1408" y2="1616" x1="2432" />
            <wire x2="2576" y1="1408" y2="1408" x1="2432" />
        </branch>
        <branch name="XLXN_94">
            <wire x2="2432" y1="1872" y2="1872" x1="2224" />
            <wire x2="2432" y1="1872" y2="2048" x1="2432" />
            <wire x2="2592" y1="2048" y2="2048" x1="2432" />
        </branch>
        <instance x="1968" y="1968" name="XLXI_40" orien="R0" />
        <instance x="1968" y="2352" name="XLXI_41" orien="R0" />
        <instance x="1968" y="2544" name="XLXI_42" orien="R0" />
        <instance x="1968" y="2160" name="XLXI_43" orien="R0" />
        <instance x="2592" y="2304" name="XLXI_44" orien="R0" />
        <branch name="XLXN_95">
            <wire x2="2240" y1="2064" y2="2064" x1="2224" />
            <wire x2="2240" y1="2064" y2="2112" x1="2240" />
            <wire x2="2592" y1="2112" y2="2112" x1="2240" />
        </branch>
        <branch name="XLXN_97">
            <wire x2="2240" y1="2256" y2="2256" x1="2224" />
            <wire x2="2592" y1="2176" y2="2176" x1="2240" />
            <wire x2="2240" y1="2176" y2="2256" x1="2240" />
        </branch>
        <branch name="XLXN_98">
            <wire x2="2448" y1="2448" y2="2448" x1="2224" />
            <wire x2="2448" y1="2240" y2="2448" x1="2448" />
            <wire x2="2592" y1="2240" y2="2240" x1="2448" />
        </branch>
        <branch name="I0(3)">
            <wire x2="1664" y1="2752" y2="2752" x1="1648" />
            <wire x2="1968" y1="2752" y2="2752" x1="1664" />
        </branch>
        <branch name="I1(3)">
            <wire x2="1744" y1="2944" y2="2944" x1="1728" />
            <wire x2="1968" y1="2944" y2="2944" x1="1744" />
        </branch>
        <branch name="I2(3)">
            <wire x2="1824" y1="3136" y2="3136" x1="1808" />
            <wire x2="1968" y1="3136" y2="3136" x1="1824" />
        </branch>
        <branch name="I3(0)">
            <wire x2="1936" y1="800" y2="800" x1="1888" />
        </branch>
        <branch name="I3(1)">
            <wire x2="1952" y1="1648" y2="1648" x1="1888" />
        </branch>
        <branch name="I3(2)">
            <wire x2="1968" y1="2480" y2="2480" x1="1888" />
        </branch>
        <branch name="I3(3)">
            <wire x2="1904" y1="3328" y2="3328" x1="1888" />
            <wire x2="1968" y1="3328" y2="3328" x1="1904" />
        </branch>
        <branch name="XLXN_99">
            <wire x2="2432" y1="2720" y2="2720" x1="2224" />
            <wire x2="2432" y1="2720" y2="2896" x1="2432" />
            <wire x2="2592" y1="2896" y2="2896" x1="2432" />
        </branch>
        <instance x="1968" y="2816" name="XLXI_45" orien="R0" />
        <instance x="1968" y="3200" name="XLXI_46" orien="R0" />
        <instance x="1968" y="3392" name="XLXI_47" orien="R0" />
        <instance x="1968" y="3008" name="XLXI_48" orien="R0" />
        <instance x="2592" y="3152" name="XLXI_49" orien="R0" />
        <branch name="XLXN_100">
            <wire x2="2240" y1="2912" y2="2912" x1="2224" />
            <wire x2="2240" y1="2912" y2="2960" x1="2240" />
            <wire x2="2592" y1="2960" y2="2960" x1="2240" />
        </branch>
        <branch name="XLXN_102">
            <wire x2="2240" y1="3104" y2="3104" x1="2224" />
            <wire x2="2592" y1="3024" y2="3024" x1="2240" />
            <wire x2="2240" y1="3024" y2="3104" x1="2240" />
        </branch>
        <branch name="XLXN_103">
            <wire x2="2448" y1="3296" y2="3296" x1="2224" />
            <wire x2="2448" y1="3088" y2="3296" x1="2448" />
            <wire x2="2592" y1="3088" y2="3088" x1="2448" />
        </branch>
        <branch name="O(3:0)">
            <wire x2="3120" y1="416" y2="464" x1="3120" />
            <wire x2="3120" y1="464" y2="1312" x1="3120" />
            <wire x2="3120" y1="1312" y2="1712" x1="3120" />
            <wire x2="3408" y1="1712" y2="1712" x1="3120" />
            <wire x2="3120" y1="1712" y2="2144" x1="3120" />
            <wire x2="3120" y1="2144" y2="2992" x1="3120" />
            <wire x2="3120" y1="2992" y2="3136" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="3408" y="1712" name="O(3:0)" orien="R0" />
        <bustap x2="3024" y1="464" y2="464" x1="3120" />
        <bustap x2="3024" y1="1312" y2="1312" x1="3120" />
        <bustap x2="3024" y1="2144" y2="2144" x1="3120" />
        <bustap x2="3024" y1="2992" y2="2992" x1="3120" />
        <branch name="O(0)">
            <wire x2="3024" y1="464" y2="464" x1="2816" />
        </branch>
        <branch name="O(1)">
            <wire x2="3024" y1="1312" y2="1312" x1="2832" />
        </branch>
        <branch name="O(2)">
            <wire x2="3024" y1="2144" y2="2144" x1="2848" />
        </branch>
        <branch name="O(3)">
            <wire x2="3024" y1="2992" y2="2992" x1="2848" />
        </branch>
        <bustap x2="336" y1="96" y2="96" x1="240" />
        <bustap x2="336" y1="256" y2="256" x1="240" />
    </sheet>
</drawing>