`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:42:48 04/12/2018 
// Design Name: 
// Module Name:    Div_Ori_32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Div_Ori_32bit(
	 input Ctrl,
    input High,
	 input [31:0]Low,
	 input [31:0]B,
	 input clk,
	 output [31:0]Quotient,
	 output [31:0]Remainder
    );
	reg [31:0]H;
	reg [31:0]L;
   Div_Ori_64_32bit tmp(.A({H,L}),.B(B),.clk(clk),.Quotient(Quotient),.Remainder(Remainder));
	
	always @ (posedge High)begin
		if(Ctrl)begin
			H<=Low;
		end
		else begin
			L<=Low;
		end
	end
endmodule

