`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:31:37 04/01/2018 
// Design Name: 
// Module Name:    Multiplier_unsigned_4_Bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Multiplier_unsigned_4_Bit(input clk,input [3:0]A,input [3:0]B,output [7:0]res );

reg [7:0]res;
reg [7:0]R0,R1;
reg [3:0]R2;
reg [2:0]count = 3'b0;
reg [1:0]state = 2'b0;

always@(posedge clk)begin
	case(state)
	2'b00:begin 
		R0 <= 0;
		R1 <= {4'b0,A};
		R2 <= B;
		state <= 2'b01;
		count <= 3'b000;
	end
	2'b01:begin
		if(count == 3'b100)
			state <= 2'b10;
		else begin
			if(R2[0]==1'b1)
				R0<=R0+R1;
			else 
				R0<=R0;
			R1<=R1<<1;
			R2<=R2>>1;
			count <= count+1;
			state<=2'b01;
		end
	end
	2'b10:begin
		res<=R0;
		state<=2'b00;
	end
endcase
end

endmodule
