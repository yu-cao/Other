//算数右移模块，该模块支持将数字向右进行算术右移1位
//A为32位原始输入，S为选择模块，S=1代表进行移位，sign为符号位
//sign设置应为A[N-1]
module sra1b(A, S, Z, sign);
    parameter N = 32;

	input wire sign;
	input wire [(N-1):0] A;
	input wire S;
    output wire [(N-1):0] Z;
    wire [(N-1):0] B;

    assign B[(N-2):0] = A[N-1:1];
    assign B[(N-1)] = sign;//将B的最左边的一位用符号位置换
    
	mux_2to1 m0(.X(A),.Y(B),.S(S),.Z(Z));

endmodule
