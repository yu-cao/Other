<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="RSTN" />
        <signal name="clk_100mhz" />
        <signal name="K_COL(3:0)" />
        <signal name="SW(15:0)" />
        <signal name="CR" />
        <signal name="K_ROW(4:0)" />
        <signal name="readn" />
        <signal name="XLXN_11" />
        <signal name="Pulse(3:0)" />
        <signal name="XLXN_15(4:0)" />
        <signal name="RDY" />
        <signal name="BTN_OK(3:0)" />
        <signal name="BTN_OK(2:0)" />
        <signal name="SW_OK(15:0)" />
        <signal name="SW_OK(14:12),SW_OK(15),SW_OK(0)" />
        <signal name="SW_OK(14:12)" />
        <signal name="rst" />
        <signal name="Div(20)" />
        <signal name="SW_OK(0)" />
        <signal name="Div(25)" />
        <signal name="Disp_num(31:0)" />
        <signal name="point_out(7:0)" />
        <signal name="LE_out(7:0)" />
        <signal name="SEGCLR" />
        <signal name="SEGCLK" />
        <signal name="SEGDT" />
        <signal name="SEGEN" />
        <signal name="V5" />
        <signal name="Ai(31:0)" />
        <signal name="Bi(31:0)" />
        <signal name="blink(7:0)" />
        <signal name="Div(31:0)" />
        <signal name="Div(31:0),Div(31:0)" />
        <signal name="XLXN_60" />
        <signal name="G0" />
        <signal name="N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,blink(3:0),N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,blink(7:0),blink(7:0)" />
        <signal name="Buzzer" />
        <signal name="SW_OK(1),Div(19:18)" />
        <signal name="N0" />
        <signal name="AN(3:0)" />
        <signal name="SEGMENT(7:0)" />
        <signal name="LED(7:0)" />
        <signal name="XLXN_104" />
        <signal name="LEDCLK" />
        <signal name="LEDDT" />
        <signal name="LEDCLR" />
        <signal name="LEDEN" />
        <signal name="XLXN_130(31:0)" />
        <signal name="ILU(63:0)" />
        <signal name="ILU(63:32)" />
        <signal name="ILU(31:0)" />
        <signal name="SW_OK(4:1)" />
        <signal name="SW_OK(5)" />
        <signal name="SW_OK(6)" />
        <signal name="SW_OK(9:7)" />
        <signal name="larger_A" />
        <signal name="equal" />
        <signal name="inf" />
        <signal name="zero" />
        <signal name="nan" />
        <signal name="Div(31:8),overflow,CF,ZF,larger_A,equal,inf,zero,nan" />
        <signal name="overflow" />
        <signal name="CF" />
        <signal name="ZF" />
        <port polarity="Input" name="RSTN" />
        <port polarity="Input" name="clk_100mhz" />
        <port polarity="Input" name="K_COL(3:0)" />
        <port polarity="Input" name="SW(15:0)" />
        <port polarity="Output" name="CR" />
        <port polarity="Output" name="K_ROW(4:0)" />
        <port polarity="Output" name="readn" />
        <port polarity="Output" name="RDY" />
        <port polarity="Output" name="SEGCLR" />
        <port polarity="Output" name="SEGCLK" />
        <port polarity="Output" name="SEGDT" />
        <port polarity="Output" name="SEGEN" />
        <port polarity="Output" name="Buzzer" />
        <port polarity="Output" name="AN(3:0)" />
        <port polarity="Output" name="SEGMENT(7:0)" />
        <port polarity="Output" name="LED(7:0)" />
        <port polarity="Output" name="LEDCLK" />
        <port polarity="Output" name="LEDDT" />
        <port polarity="Output" name="LEDCLR" />
        <port polarity="Output" name="LEDEN" />
        <blockdef name="clkdiv">
            <timestamp>2017-11-12T13:21:55</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="SAnti_jitter">
            <timestamp>2017-11-12T13:20:18</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-368" y2="-368" x1="64" />
            <line x2="0" y1="-256" y2="-256" x1="64" />
            <rect width="64" x="0" y="-156" height="24" />
            <line x2="0" y1="-144" y2="-144" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <rect width="64" x="320" y="-300" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <rect width="64" x="320" y="-236" height="24" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Multi_8CH32">
            <timestamp>2017-11-19T13:10:14</timestamp>
            <rect width="320" x="64" y="-896" height="896" />
            <line x2="0" y1="-864" y2="-864" x1="64" />
            <line x2="0" y1="-800" y2="-800" x1="64" />
            <line x2="0" y1="-736" y2="-736" x1="64" />
            <rect width="64" x="0" y="-684" height="24" />
            <line x2="0" y1="-672" y2="-672" x1="64" />
            <rect width="64" x="0" y="-620" height="24" />
            <line x2="0" y1="-608" y2="-608" x1="64" />
            <rect width="64" x="0" y="-556" height="24" />
            <line x2="0" y1="-544" y2="-544" x1="64" />
            <rect width="64" x="0" y="-492" height="24" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-364" height="24" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-876" height="24" />
            <line x2="448" y1="-864" y2="-864" x1="384" />
            <rect width="64" x="384" y="-460" height="24" />
            <line x2="448" y1="-448" y2="-448" x1="384" />
            <rect width="64" x="384" y="-44" height="24" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
        </blockdef>
        <blockdef name="SEnter_2_32">
            <timestamp>2017-11-12T13:15:55</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <rect width="64" x="320" y="-220" height="24" />
            <line x2="384" y1="-208" y2="-208" x1="320" />
            <rect width="64" x="320" y="-140" height="24" />
            <line x2="384" y1="-128" y2="-128" x1="320" />
            <rect width="64" x="320" y="-60" height="24" />
            <line x2="384" y1="-48" y2="-48" x1="320" />
        </blockdef>
        <blockdef name="Display">
            <timestamp>2017-11-20T9:8:39</timestamp>
            <rect width="256" x="64" y="-512" height="512" />
            <line x2="0" y1="-480" y2="-480" x1="64" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-480" y2="-480" x1="320" />
            <line x2="384" y1="-336" y2="-336" x1="320" />
            <line x2="384" y1="-192" y2="-192" x1="320" />
            <line x2="384" y1="-48" y2="-48" x1="320" />
        </blockdef>
        <blockdef name="GPIO">
            <timestamp>2017-11-19T11:57:15</timestamp>
            <rect width="304" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="432" y1="-288" y2="-288" x1="368" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
            <line x2="432" y1="-160" y2="-160" x1="368" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
            <rect width="64" x="368" y="-44" height="24" />
            <line x2="432" y1="-32" y2="-32" x1="368" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="Seg7_Dev">
            <timestamp>2017-11-14T14:4:51</timestamp>
            <rect width="288" x="64" y="-384" height="384" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="352" y="-364" height="24" />
            <line x2="416" y1="-352" y2="-352" x1="352" />
            <rect width="64" x="352" y="-44" height="24" />
            <line x2="416" y1="-32" y2="-32" x1="352" />
        </blockdef>
        <blockdef name="PIO">
            <timestamp>2017-11-14T12:14:57</timestamp>
            <rect width="320" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-236" height="24" />
            <line x2="448" y1="-224" y2="-224" x1="384" />
            <rect width="64" x="384" y="-44" height="24" />
            <line x2="448" y1="-32" y2="-32" x1="384" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="FLU_32">
            <timestamp>2018-4-16T0:54:24</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="ILU">
            <timestamp>2018-4-19T11:16:20</timestamp>
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="384" y1="160" y2="160" x1="320" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-256" y2="-256" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <rect width="64" x="320" y="-76" height="24" />
            <line x2="384" y1="-64" y2="-64" x1="320" />
            <rect width="256" x="64" y="-384" height="576" />
        </blockdef>
        <block symbolname="clkdiv" name="XLXI_1">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="XLXN_11" name="rst" />
            <blockpin signalname="Div(31:0)" name="clkdiv(31:0)" />
        </block>
        <block symbolname="SAnti_jitter" name="XLXI_2">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="RSTN" name="RSTN" />
            <blockpin signalname="readn" name="readn" />
            <blockpin signalname="K_COL(3:0)" name="Key_y(3:0)" />
            <blockpin signalname="SW(15:0)" name="SW(15:0)" />
            <blockpin signalname="RDY" name="Key_ready" />
            <blockpin signalname="CR" name="CR" />
            <blockpin signalname="XLXN_11" name="rst" />
            <blockpin signalname="K_ROW(4:0)" name="Key_x(4:0)" />
            <blockpin signalname="XLXN_15(4:0)" name="Key_out(4:0)" />
            <blockpin signalname="Pulse(3:0)" name="pulse_out(3:0)" />
            <blockpin signalname="BTN_OK(3:0)" name="BTN_OK(3:0)" />
            <blockpin signalname="SW_OK(15:0)" name="SW_OK(15:0)" />
        </block>
        <block symbolname="Multi_8CH32" name="XLXI_3">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="XLXN_11" name="rst" />
            <blockpin signalname="V5" name="EN" />
            <blockpin signalname="N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,blink(3:0),N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,blink(7:0),blink(7:0)" name="LES(63:0)" />
            <blockpin signalname="SW_OK(14:12)" name="Test(2:0)" />
            <blockpin signalname="Div(31:0),Div(31:0)" name="point_in(63:0)" />
            <blockpin signalname="Ai(31:0)" name="Data0(31:0)" />
            <blockpin signalname="Bi(31:0)" name="Data1(31:0)" />
            <blockpin signalname="ILU(63:32)" name="Data2(31:0)" />
            <blockpin signalname="ILU(31:0)" name="Data3(31:0)" />
            <blockpin signalname="XLXN_130(31:0)" name="Data4(31:0)" />
            <blockpin name="Data5(31:0)" />
            <blockpin name="Data6(31:0)" />
            <blockpin name="Data7(31:0)" />
            <blockpin signalname="point_out(7:0)" name="point_out(7:0)" />
            <blockpin signalname="LE_out(7:0)" name="LE_out(7:0)" />
            <blockpin signalname="Disp_num(31:0)" name="Disp_num(31:0)" />
        </block>
        <block symbolname="SEnter_2_32" name="XLXI_4">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="RDY" name="D_ready" />
            <blockpin signalname="BTN_OK(2:0)" name="BTN(2:0)" />
            <blockpin signalname="SW_OK(14:12),SW_OK(15),SW_OK(0)" name="Ctrl(4:0)" />
            <blockpin signalname="XLXN_15(4:0)" name="Din(4:0)" />
            <blockpin signalname="readn" name="readn" />
            <blockpin signalname="Ai(31:0)" name="Ai(31:0)" />
            <blockpin signalname="Bi(31:0)" name="Bi(31:0)" />
            <blockpin signalname="blink(7:0)" name="blink(7:0)" />
        </block>
        <block symbolname="Display" name="XLXI_6">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="rst" name="rst" />
            <blockpin signalname="Div(20)" name="Start" />
            <blockpin signalname="SW_OK(0)" name="SW0" />
            <blockpin signalname="Div(25)" name="flash" />
            <blockpin signalname="Disp_num(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="point_out(7:0)" name="points(7:0)" />
            <blockpin signalname="LE_out(7:0)" name="LES(7:0)" />
            <blockpin signalname="SEGCLR" name="seg_clm" />
            <blockpin signalname="SEGCLK" name="seg_clk" />
            <blockpin signalname="SEGDT" name="seg_sout" />
            <blockpin signalname="SEGEN" name="SEG_PEN" />
        </block>
        <block symbolname="vcc" name="XLXI_8">
            <blockpin signalname="XLXN_60" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_10">
            <blockpin signalname="G0" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_9">
            <blockpin signalname="V5" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_13">
            <blockpin signalname="Buzzer" name="P" />
        </block>
        <block symbolname="Seg7_Dev" name="XLXI_15">
            <blockpin signalname="Div(25)" name="flash" />
            <blockpin signalname="SW_OK(1),Div(19:18)" name="Scan(2:0)" />
            <blockpin signalname="Disp_num(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="point_out(7:0)" name="point(7:0)" />
            <blockpin signalname="LE_out(7:0)" name="LES(7:0)" />
            <blockpin signalname="XLXN_104" name="SW0" />
            <blockpin signalname="AN(3:0)" name="AN(3:0)" />
            <blockpin signalname="SEGMENT(7:0)" name="SEGMENT(7:0)" />
        </block>
        <block symbolname="PIO" name="XLXI_16">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="N0" name="rst" />
            <blockpin signalname="V5" name="EN" />
            <blockpin signalname="Div(31:8),overflow,CF,ZF,larger_A,equal,inf,zero,nan" name="PData_in(31:0)" />
            <blockpin signalname="LED(7:0)" name="LED(7:0)" />
            <blockpin name="GPIOf0(31:0)" />
        </block>
        <block symbolname="inv" name="XLXI_17">
            <blockpin signalname="SW_OK(0)" name="I" />
            <blockpin signalname="XLXN_104" name="O" />
        </block>
        <block symbolname="FLU_32" name="XLXI_18">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="SW_OK(9:7)" name="sw(2:0)" />
            <blockpin signalname="Ai(31:0)" name="A(31:0)" />
            <blockpin signalname="Bi(31:0)" name="B(31:0)" />
            <blockpin signalname="inf" name="inf" />
            <blockpin signalname="zero" name="zero" />
            <blockpin signalname="nan" name="nan" />
            <blockpin signalname="XLXN_130(31:0)" name="res(31:0)" />
        </block>
        <block symbolname="GPIO" name="XLXI_7">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="XLXN_11" name="rst" />
            <blockpin signalname="Div(20)" name="Start" />
            <blockpin signalname="XLXN_60" name="EN" />
            <blockpin name="P_Data(31:0)" />
            <blockpin signalname="LEDCLK" name="led_clk" />
            <blockpin signalname="LEDDT" name="led_sout" />
            <blockpin signalname="LEDCLR" name="led_clrn" />
            <blockpin signalname="LEDEN" name="LED_PEN" />
            <blockpin name="GPIOf0(31:0)" />
        </block>
        <block symbolname="ILU" name="XLXI_27">
            <blockpin signalname="clk_100mhz" name="clk" />
            <blockpin signalname="SW_OK(6)" name="Ctrl" />
            <blockpin signalname="SW_OK(5)" name="SAH" />
            <blockpin signalname="SW_OK(4:1)" name="SW(3:0)" />
            <blockpin signalname="Ai(31:0)" name="AL(31:0)" />
            <blockpin signalname="Bi(31:0)" name="B(31:0)" />
            <blockpin signalname="overflow" name="overflow" />
            <blockpin signalname="larger_A" name="larger_A" />
            <blockpin signalname="equal" name="equal" />
            <blockpin signalname="ILU(63:0)" name="ans(63:0)" />
            <blockpin signalname="CF" name="CF" />
            <blockpin signalname="ZF" name="ZF" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="3520">
        <instance x="608" y="1600" name="XLXI_1" orien="R0">
        </instance>
        <instance x="480" y="1120" name="XLXI_2" orien="R0">
        </instance>
        <branch name="RSTN">
            <wire x2="480" y1="752" y2="752" x1="208" />
        </branch>
        <iomarker fontsize="28" x="208" y="752" name="RSTN" orien="R180" />
        <branch name="K_COL(3:0)">
            <wire x2="480" y1="976" y2="976" x1="192" />
        </branch>
        <branch name="SW(15:0)">
            <wire x2="480" y1="1088" y2="1088" x1="160" />
        </branch>
        <iomarker fontsize="28" x="192" y="976" name="K_COL(3:0)" orien="R180" />
        <iomarker fontsize="28" x="160" y="1088" name="SW(15:0)" orien="R180" />
        <branch name="CR">
            <wire x2="896" y1="704" y2="704" x1="864" />
        </branch>
        <iomarker fontsize="28" x="896" y="704" name="CR" orien="R0" />
        <branch name="K_ROW(4:0)">
            <wire x2="976" y1="832" y2="832" x1="864" />
        </branch>
        <branch name="readn">
            <wire x2="1904" y1="432" y2="432" x1="400" />
            <wire x2="2064" y1="432" y2="432" x1="1904" />
            <wire x2="1904" y1="432" y2="704" x1="1904" />
            <wire x2="400" y1="432" y2="864" x1="400" />
            <wire x2="480" y1="864" y2="864" x1="400" />
            <wire x2="1904" y1="704" y2="704" x1="1840" />
        </branch>
        <iomarker fontsize="28" x="192" y="368" name="clk_100mhz" orien="R180" />
        <branch name="XLXN_11">
            <wire x2="912" y1="1360" y2="1360" x1="544" />
            <wire x2="544" y1="1360" y2="1568" x1="544" />
            <wire x2="608" y1="1568" y2="1568" x1="544" />
            <wire x2="544" y1="1568" y2="1680" x1="544" />
            <wire x2="1456" y1="1680" y2="1680" x1="544" />
            <wire x2="544" y1="1680" y2="2112" x1="544" />
            <wire x2="656" y1="2112" y2="2112" x1="544" />
            <wire x2="912" y1="768" y2="768" x1="864" />
            <wire x2="912" y1="768" y2="1360" x1="912" />
            <wire x2="1456" y1="1120" y2="1680" x1="1456" />
            <wire x2="2448" y1="1120" y2="1120" x1="1456" />
        </branch>
        <branch name="Pulse(3:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1008" y="960" type="branch" />
            <wire x2="1008" y1="960" y2="960" x1="864" />
        </branch>
        <iomarker fontsize="28" x="976" y="832" name="K_ROW(4:0)" orien="R0" />
        <branch name="XLXN_15(4:0)">
            <wire x2="1168" y1="896" y2="896" x1="864" />
            <wire x2="1168" y1="896" y2="960" x1="1168" />
            <wire x2="1456" y1="960" y2="960" x1="1168" />
        </branch>
        <branch name="RDY">
            <wire x2="1168" y1="640" y2="640" x1="864" />
            <wire x2="1168" y1="640" y2="768" x1="1168" />
            <wire x2="1456" y1="768" y2="768" x1="1168" />
            <wire x2="1168" y1="352" y2="640" x1="1168" />
            <wire x2="1360" y1="352" y2="352" x1="1168" />
        </branch>
        <iomarker fontsize="28" x="1360" y="352" name="RDY" orien="R0" />
        <branch name="BTN_OK(3:0)">
            <wire x2="1216" y1="1024" y2="1024" x1="864" />
            <wire x2="1216" y1="832" y2="1024" x1="1216" />
        </branch>
        <instance x="1456" y="992" name="XLXI_4" orien="R0">
        </instance>
        <iomarker fontsize="28" x="2064" y="432" name="readn" orien="R0" />
        <bustap x2="1312" y1="832" y2="832" x1="1216" />
        <branch name="BTN_OK(2:0)">
            <wire x2="1456" y1="832" y2="832" x1="1312" />
        </branch>
        <branch name="SW_OK(15:0)">
            <wire x2="1248" y1="1088" y2="1088" x1="864" />
            <wire x2="1248" y1="1088" y2="1200" x1="1248" />
            <wire x2="2160" y1="1200" y2="1200" x1="1248" />
            <wire x2="2160" y1="1200" y2="1312" x1="2160" />
            <wire x2="2160" y1="1312" y2="1328" x1="2160" />
            <wire x2="1248" y1="1200" y2="1792" x1="1248" />
            <wire x2="1248" y1="1792" y2="1856" x1="1248" />
            <wire x2="1248" y1="1856" y2="1920" x1="1248" />
            <wire x2="1248" y1="1920" y2="2000" x1="1248" />
            <wire x2="1488" y1="2000" y2="2000" x1="1248" />
            <wire x2="1488" y1="2000" y2="2288" x1="1488" />
            <wire x2="1248" y1="896" y2="1088" x1="1248" />
        </branch>
        <bustap x2="1344" y1="896" y2="896" x1="1248" />
        <branch name="SW_OK(14:12),SW_OK(15),SW_OK(0)">
            <wire x2="1456" y1="896" y2="896" x1="1344" />
        </branch>
        <bustap x2="2256" y1="1312" y2="1312" x1="2160" />
        <branch name="SEGCLR">
            <wire x2="2848" y1="272" y2="272" x1="2816" />
        </branch>
        <iomarker fontsize="28" x="2848" y="272" name="SEGCLR" orien="R0" />
        <branch name="SEGCLK">
            <wire x2="2848" y1="416" y2="416" x1="2816" />
        </branch>
        <iomarker fontsize="28" x="2848" y="416" name="SEGCLK" orien="R0" />
        <branch name="SEGDT">
            <wire x2="2848" y1="560" y2="560" x1="2816" />
        </branch>
        <iomarker fontsize="28" x="2848" y="560" name="SEGDT" orien="R0" />
        <branch name="SEGEN">
            <wire x2="2848" y1="704" y2="704" x1="2816" />
        </branch>
        <iomarker fontsize="28" x="2848" y="704" name="SEGEN" orien="R0" />
        <branch name="blink(7:0)">
            <wire x2="1920" y1="944" y2="944" x1="1840" />
        </branch>
        <branch name="XLXN_60">
            <wire x2="352" y1="2224" y2="2240" x1="352" />
            <wire x2="656" y1="2240" y2="2240" x1="352" />
        </branch>
        <instance x="288" y="2224" name="XLXI_8" orien="R0" />
        <instance x="2768" y="2384" name="XLXI_10" orien="R0" />
        <branch name="G0">
            <wire x2="2832" y1="2240" y2="2256" x1="2832" />
            <wire x2="3024" y1="2240" y2="2240" x1="2832" />
        </branch>
        <instance x="2064" y="1168" name="XLXI_9" orien="R0" />
        <branch name="Div(31:0)">
            <wire x2="1136" y1="1504" y2="1504" x1="992" />
        </branch>
        <branch name="LE_out(7:0)">
            <wire x2="3008" y1="160" y2="160" x1="2416" />
            <wire x2="3008" y1="160" y2="1472" x1="3008" />
            <wire x2="2416" y1="160" y2="720" x1="2416" />
            <wire x2="2432" y1="720" y2="720" x1="2416" />
            <wire x2="3008" y1="1472" y2="1472" x1="2896" />
        </branch>
        <branch name="point_out(7:0)">
            <wire x2="3184" y1="112" y2="112" x1="2384" />
            <wire x2="3184" y1="112" y2="1056" x1="3184" />
            <wire x2="2384" y1="112" y2="656" x1="2384" />
            <wire x2="2432" y1="656" y2="656" x1="2384" />
            <wire x2="3184" y1="1056" y2="1056" x1="2896" />
        </branch>
        <branch name="Disp_num(31:0)">
            <wire x2="3328" y1="48" y2="48" x1="2368" />
            <wire x2="3328" y1="48" y2="1888" x1="3328" />
            <wire x2="2368" y1="48" y2="592" x1="2368" />
            <wire x2="2432" y1="592" y2="592" x1="2368" />
            <wire x2="3328" y1="1888" y2="1888" x1="2896" />
        </branch>
        <branch name="Div(20)">
            <wire x2="560" y1="1232" y2="2176" x1="560" />
            <wire x2="656" y1="2176" y2="2176" x1="560" />
            <wire x2="2256" y1="1232" y2="1232" x1="560" />
            <wire x2="2256" y1="400" y2="400" x1="2000" />
            <wire x2="2432" y1="400" y2="400" x1="2256" />
            <wire x2="2256" y1="400" y2="1232" x1="2256" />
        </branch>
        <branch name="rst">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="2304" y="336" type="branch" />
            <wire x2="2432" y1="336" y2="336" x1="2304" />
        </branch>
        <instance x="2784" y="2496" name="XLXI_13" orien="R0" />
        <branch name="Buzzer">
            <wire x2="2848" y1="2496" y2="2512" x1="2848" />
            <wire x2="2992" y1="2512" y2="2512" x1="2848" />
        </branch>
        <iomarker fontsize="28" x="2992" y="2512" name="Buzzer" orien="R0" />
        <branch name="SW_OK(14:12)">
            <wire x2="2448" y1="1312" y2="1312" x1="2256" />
        </branch>
        <branch name="V5">
            <wire x2="2128" y1="1168" y2="1184" x1="2128" />
            <wire x2="2448" y1="1184" y2="1184" x1="2128" />
        </branch>
        <instance x="2448" y="1920" name="XLXI_3" orien="R0">
        </instance>
        <branch name="Div(31:0),Div(31:0)">
            <wire x2="2448" y1="1376" y2="1376" x1="2176" />
        </branch>
        <branch name="N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,blink(3:0),N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,N0,blink(7:0),blink(7:0)">
            <wire x2="2448" y1="1248" y2="1248" x1="2288" />
        </branch>
        <branch name="clk_100mhz">
            <wire x2="288" y1="368" y2="368" x1="192" />
            <wire x2="288" y1="368" y2="480" x1="288" />
            <wire x2="288" y1="480" y2="640" x1="288" />
            <wire x2="480" y1="640" y2="640" x1="288" />
            <wire x2="288" y1="640" y2="1248" x1="288" />
            <wire x2="288" y1="1248" y2="1504" x1="288" />
            <wire x2="608" y1="1504" y2="1504" x1="288" />
            <wire x2="288" y1="1504" y2="1728" x1="288" />
            <wire x2="288" y1="1728" y2="2048" x1="288" />
            <wire x2="656" y1="2048" y2="2048" x1="288" />
            <wire x2="1568" y1="1728" y2="1728" x1="288" />
            <wire x2="1776" y1="1728" y2="1728" x1="1568" />
            <wire x2="1568" y1="1728" y2="2224" x1="1568" />
            <wire x2="1776" y1="2224" y2="2224" x1="1568" />
            <wire x2="1360" y1="1248" y2="1248" x1="288" />
            <wire x2="1072" y1="480" y2="480" x1="288" />
            <wire x2="1072" y1="480" y2="704" x1="1072" />
            <wire x2="1456" y1="704" y2="704" x1="1072" />
            <wire x2="288" y1="272" y2="368" x1="288" />
            <wire x2="2432" y1="272" y2="272" x1="288" />
            <wire x2="1360" y1="1056" y2="1248" x1="1360" />
            <wire x2="2448" y1="1056" y2="1056" x1="1360" />
        </branch>
        <instance x="2432" y="752" name="XLXI_6" orien="R0">
        </instance>
        <branch name="Div(25)">
            <wire x2="2432" y1="528" y2="528" x1="2320" />
        </branch>
        <branch name="SW_OK(0)">
            <wire x2="2432" y1="464" y2="464" x1="2320" />
        </branch>
        <instance x="4064" y="704" name="XLXI_15" orien="R0">
        </instance>
        <branch name="Div(25)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3856" y="352" type="branch" />
            <wire x2="4064" y1="352" y2="352" x1="3856" />
        </branch>
        <branch name="SW_OK(1),Div(19:18)">
            <wire x2="4064" y1="416" y2="416" x1="3856" />
        </branch>
        <branch name="Disp_num(31:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="480" type="branch" />
            <wire x2="4064" y1="480" y2="480" x1="3872" />
        </branch>
        <branch name="point_out(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3888" y="544" type="branch" />
            <wire x2="4064" y1="544" y2="544" x1="3888" />
        </branch>
        <branch name="LE_out(7:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3888" y="608" type="branch" />
            <wire x2="4064" y1="608" y2="608" x1="3888" />
        </branch>
        <branch name="clk_100mhz">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3888" y="1088" type="branch" />
            <wire x2="4032" y1="1088" y2="1088" x1="3888" />
        </branch>
        <branch name="V5">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3904" y="1216" type="branch" />
            <wire x2="4032" y1="1216" y2="1216" x1="3904" />
        </branch>
        <branch name="AN(3:0)">
            <wire x2="4512" y1="352" y2="352" x1="4480" />
        </branch>
        <iomarker fontsize="28" x="4512" y="352" name="AN(3:0)" orien="R0" />
        <branch name="SEGMENT(7:0)">
            <wire x2="4512" y1="672" y2="672" x1="4480" />
        </branch>
        <iomarker fontsize="28" x="4512" y="672" name="SEGMENT(7:0)" orien="R0" />
        <branch name="LED(7:0)">
            <wire x2="4544" y1="1088" y2="1088" x1="4480" />
        </branch>
        <iomarker fontsize="28" x="4544" y="1088" name="LED(7:0)" orien="R0" />
        <instance x="4032" y="1312" name="XLXI_16" orien="R0">
        </instance>
        <branch name="N0">
            <wire x2="4032" y1="1152" y2="1152" x1="3904" />
        </branch>
        <branch name="XLXN_104">
            <wire x2="4064" y1="672" y2="672" x1="4032" />
        </branch>
        <instance x="3808" y="704" name="XLXI_17" orien="R0" />
        <branch name="SW_OK(0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3696" y="672" type="branch" />
            <wire x2="3808" y1="672" y2="672" x1="3696" />
        </branch>
        <branch name="Div(31:8),overflow,CF,ZF,larger_A,equal,inf,zero,nan">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="3872" y="1280" type="branch" />
            <wire x2="4032" y1="1280" y2="1280" x1="3872" />
        </branch>
        <branch name="LEDCLK">
            <wire x2="1184" y1="2048" y2="2048" x1="1088" />
        </branch>
        <branch name="LEDDT">
            <wire x2="1200" y1="2112" y2="2112" x1="1088" />
        </branch>
        <branch name="LEDCLR">
            <wire x2="1184" y1="2176" y2="2176" x1="1088" />
        </branch>
        <branch name="LEDEN">
            <wire x2="1184" y1="2240" y2="2240" x1="1088" />
        </branch>
        <instance x="656" y="2336" name="XLXI_7" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1184" y="2048" name="LEDCLK" orien="R0" />
        <iomarker fontsize="28" x="1200" y="2112" name="LEDDT" orien="R0" />
        <iomarker fontsize="28" x="1184" y="2176" name="LEDCLR" orien="R0" />
        <iomarker fontsize="28" x="1184" y="2240" name="LEDEN" orien="R0" />
        <branch name="XLXN_130(31:0)">
            <wire x2="2304" y1="2416" y2="2416" x1="2160" />
            <wire x2="2432" y1="1696" y2="1696" x1="2304" />
            <wire x2="2448" y1="1696" y2="1696" x1="2432" />
            <wire x2="2304" y1="1696" y2="2416" x1="2304" />
        </branch>
        <branch name="ILU(63:0)">
            <wire x2="2240" y1="2016" y2="2016" x1="2160" />
            <wire x2="2240" y1="1568" y2="1632" x1="2240" />
            <wire x2="2240" y1="1632" y2="1648" x1="2240" />
            <wire x2="2240" y1="1648" y2="2016" x1="2240" />
        </branch>
        <bustap x2="2336" y1="1568" y2="1568" x1="2240" />
        <branch name="ILU(63:32)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2392" y="1568" type="branch" />
            <wire x2="2400" y1="1568" y2="1568" x1="2336" />
            <wire x2="2448" y1="1568" y2="1568" x1="2400" />
        </branch>
        <bustap x2="2336" y1="1632" y2="1632" x1="2240" />
        <branch name="ILU(31:0)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="2392" y="1632" type="branch" />
            <wire x2="2400" y1="1632" y2="1632" x1="2336" />
            <wire x2="2448" y1="1632" y2="1632" x1="2400" />
        </branch>
        <branch name="SW_OK(4:1)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1432" y="1920" type="branch" />
            <wire x2="1360" y1="1920" y2="1920" x1="1344" />
            <wire x2="1440" y1="1920" y2="1920" x1="1360" />
            <wire x2="1776" y1="1920" y2="1920" x1="1440" />
        </branch>
        <bustap x2="1344" y1="1920" y2="1920" x1="1248" />
        <bustap x2="1344" y1="1856" y2="1856" x1="1248" />
        <branch name="SW_OK(5)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1560" y="1856" type="branch" />
            <wire x2="1360" y1="1856" y2="1856" x1="1344" />
            <wire x2="1568" y1="1856" y2="1856" x1="1360" />
            <wire x2="1776" y1="1856" y2="1856" x1="1568" />
        </branch>
        <bustap x2="1344" y1="1792" y2="1792" x1="1248" />
        <branch name="SW_OK(6)">
            <attrtext style="alignment:SOFT-LEFT;fontsize:28;fontname:Arial" attrname="Name" x="1560" y="1792" type="branch" />
            <wire x2="1360" y1="1792" y2="1792" x1="1344" />
            <wire x2="1568" y1="1792" y2="1792" x1="1360" />
            <wire x2="1776" y1="1792" y2="1792" x1="1568" />
        </branch>
        <bustap x2="1584" y1="2288" y2="2288" x1="1488" />
        <branch name="larger_A">
            <wire x2="2256" y1="1824" y2="1824" x1="2160" />
        </branch>
        <branch name="equal">
            <wire x2="2256" y1="1920" y2="1920" x1="2160" />
        </branch>
        <instance x="1776" y="2080" name="XLXI_27" orien="R0">
        </instance>
        <branch name="overflow">
            <wire x2="2256" y1="2112" y2="2112" x1="2160" />
        </branch>
        <branch name="Bi(31:0)">
            <wire x2="1744" y1="1504" y2="2048" x1="1744" />
            <wire x2="1776" y1="2048" y2="2048" x1="1744" />
            <wire x2="1744" y1="2048" y2="2416" x1="1744" />
            <wire x2="1776" y1="2416" y2="2416" x1="1744" />
            <wire x2="1984" y1="1504" y2="1504" x1="1744" />
            <wire x2="2448" y1="1504" y2="1504" x1="1984" />
            <wire x2="1984" y1="864" y2="864" x1="1840" />
            <wire x2="1984" y1="864" y2="1504" x1="1984" />
        </branch>
        <branch name="Ai(31:0)">
            <wire x2="1600" y1="1424" y2="1984" x1="1600" />
            <wire x2="1776" y1="1984" y2="1984" x1="1600" />
            <wire x2="1600" y1="1984" y2="2352" x1="1600" />
            <wire x2="1776" y1="2352" y2="2352" x1="1600" />
            <wire x2="2048" y1="1424" y2="1424" x1="1600" />
            <wire x2="2048" y1="1424" y2="1440" x1="2048" />
            <wire x2="2448" y1="1440" y2="1440" x1="2048" />
            <wire x2="2048" y1="784" y2="784" x1="1840" />
            <wire x2="2048" y1="784" y2="1424" x1="2048" />
        </branch>
        <branch name="SW_OK(9:7)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1680" y="2288" type="branch" />
            <wire x2="1680" y1="2288" y2="2288" x1="1584" />
            <wire x2="1776" y1="2288" y2="2288" x1="1680" />
        </branch>
        <instance x="1776" y="2448" name="XLXI_18" orien="R0">
        </instance>
        <branch name="nan">
            <wire x2="2240" y1="2352" y2="2352" x1="2160" />
        </branch>
        <branch name="zero">
            <wire x2="2240" y1="2288" y2="2288" x1="2160" />
        </branch>
        <branch name="inf">
            <wire x2="2240" y1="2224" y2="2224" x1="2160" />
        </branch>
        <branch name="CF">
            <wire x2="2256" y1="2176" y2="2176" x1="2160" />
        </branch>
        <branch name="ZF">
            <wire x2="2256" y1="2240" y2="2240" x1="2160" />
        </branch>
    </sheet>
</drawing>