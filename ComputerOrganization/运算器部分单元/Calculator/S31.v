`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:53:24 11/27/2017 
// Design Name: 
// Module Name:    S31 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module S31(input S,
            output reg[31:0] So
           );
       always@*begin
		    if(S)So[31:0]=32'hFFFFFFFF;
			 else So[31:0]=32'h00000000;
		 end
endmodule
