`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:37:24 04/09/2018 
// Design Name: 
// Module Name:    Float_slt 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Float_slt(input [31:0]A,input [31:0]B,output [31:0]res);
assign res = {31'b0,(((~A[31])&(~B[31]))&(A[30:23]>B[30:23]))|(((~A[31])&(~B[31]))&(A[30:23]==B[30:23])&(A[22:0]>B[22:0]))|
             ((( A[31])&( B[31]))&(A[30:23]<B[30:23]))|(((~A[31])&(~B[31]))&(A[30:23]==B[30:23])&(A[22:0]<B[22:0]))|
             ((~A[31])&( B[31]))};
endmodule
