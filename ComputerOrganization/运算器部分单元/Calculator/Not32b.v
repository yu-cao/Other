module Not32b(x,f);
	input [31:0] x;
	output [31:0] f;
	
	not(f[0],x[0]);
	not(f[1],x[1]);
	not(f[2],x[2]);
	not(f[3],x[3]);
	not(f[4],x[4]);
	not(f[5],x[5]);
	not(f[6],x[6]);
	not(f[7],x[7]);
	not(f[8],x[8]);
	not(f[9],x[9]);
	not(f[10],x[10]);
	not(f[11],x[11]);
	not(f[12],x[12]);
	not(f[13],x[13]);
	not(f[14],x[14]);
	not(f[15],x[15]);
	not(f[16],x[16]);
	not(f[17],x[17]);
	not(f[18],x[18]);
	not(f[19],x[19]);
	not(f[20],x[20]);
	not(f[21],x[21]);
	not(f[22],x[22]);
	not(f[23],x[23]);
	not(f[24],x[24]);
	not(f[25],x[25]);
	not(f[26],x[26]);
	not(f[27],x[27]);
	not(f[28],x[28]);
	not(f[29],x[29]);
	not(f[30],x[30]);
	not(f[31],x[31]);
	
endmodule
