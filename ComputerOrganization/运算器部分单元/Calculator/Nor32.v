`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:49:02 11/27/2017 
// Design Name: 
// Module Name:    Nor32 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Nor32(input [31:0]A,
				 input [31:0]B,
				 output [31:0] res
    );
	 wire [31:0] assist;
	 assign assist = A | B;
	 assign res = assist^32'hFFFFFFFF;

endmodule
