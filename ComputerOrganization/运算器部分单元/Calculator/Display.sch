<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="kintex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clk" />
        <signal name="rst" />
        <signal name="Start" />
        <signal name="SW0" />
        <signal name="flash" />
        <signal name="Hexs(31:0)" />
        <signal name="points(7:0)" />
        <signal name="LES(7:0)" />
        <signal name="Hexs(31:0),Hexs(31:0)" />
        <signal name="seg_clm" />
        <signal name="seg_clk" />
        <signal name="seg_sout" />
        <signal name="SEG_PEN" />
        <signal name="XLXN_14(63:0)" />
        <signal name="XLXN_15(63:0)" />
        <signal name="XLXN_16(63:0)" />
        <port polarity="Input" name="clk" />
        <port polarity="Input" name="rst" />
        <port polarity="Input" name="Start" />
        <port polarity="Input" name="SW0" />
        <port polarity="Input" name="flash" />
        <port polarity="Input" name="Hexs(31:0)" />
        <port polarity="Input" name="points(7:0)" />
        <port polarity="Input" name="LES(7:0)" />
        <port polarity="Output" name="seg_clm" />
        <port polarity="Output" name="seg_clk" />
        <port polarity="Output" name="seg_sout" />
        <port polarity="Output" name="SEG_PEN" />
        <blockdef name="P2S">
            <timestamp>2017-11-13T13:11:34</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="HexTo8SEG">
            <timestamp>2017-11-19T12:19:19</timestamp>
            <rect width="304" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-236" height="24" />
            <line x2="432" y1="-224" y2="-224" x1="368" />
        </blockdef>
        <blockdef name="SSeg_map_2">
            <timestamp>2017-11-22T8:2:42</timestamp>
            <rect width="352" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="416" y="-44" height="24" />
            <line x2="480" y1="-32" y2="-32" x1="416" />
        </blockdef>
        <blockdef name="MUX2T1">
            <timestamp>2017-11-22T7:22:50</timestamp>
            <rect width="256" x="64" y="-192" height="192" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-172" height="24" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
        </blockdef>
        <block symbolname="P2S" name="XLXI_1">
            <blockpin signalname="clk" name="clk" />
            <blockpin signalname="rst" name="rst" />
            <blockpin signalname="Start" name="Serial" />
            <blockpin signalname="XLXN_14(63:0)" name="P_Data(63:0)" />
            <blockpin signalname="seg_clk" name="s_clk" />
            <blockpin signalname="seg_clm" name="s_clrn" />
            <blockpin signalname="seg_sout" name="sout" />
            <blockpin signalname="SEG_PEN" name="EN" />
        </block>
        <block symbolname="HexTo8SEG" name="XLXI_4">
            <blockpin signalname="flash" name="flash" />
            <blockpin signalname="Hexs(31:0)" name="Hexs(31:0)" />
            <blockpin signalname="points(7:0)" name="points(7:0)" />
            <blockpin signalname="LES(7:0)" name="LES(7:0)" />
            <blockpin signalname="XLXN_15(63:0)" name="SEG_TXT(63:0)" />
        </block>
        <block symbolname="SSeg_map_2" name="XLXI_5">
            <blockpin signalname="Hexs(31:0),Hexs(31:0)" name="Disp_num(63:0)" />
            <blockpin signalname="XLXN_16(63:0)" name="SSeg_map(63:0)" />
        </block>
        <block symbolname="MUX2T1" name="XLXI_7">
            <blockpin signalname="XLXN_15(63:0)" name="a(63:0)" />
            <blockpin signalname="XLXN_16(63:0)" name="b(63:0)" />
            <blockpin signalname="SW0" name="SW0" />
            <blockpin signalname="XLXN_14(63:0)" name="o(63:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="944" y="1088" name="XLXI_4" orien="R0">
        </instance>
        <instance x="2240" y="752" name="XLXI_1" orien="R0">
        </instance>
        <branch name="clk">
            <wire x2="2240" y1="528" y2="528" x1="2208" />
        </branch>
        <iomarker fontsize="28" x="2208" y="528" name="clk" orien="R180" />
        <branch name="rst">
            <wire x2="2240" y1="592" y2="592" x1="2208" />
        </branch>
        <iomarker fontsize="28" x="2208" y="592" name="rst" orien="R180" />
        <branch name="Start">
            <wire x2="2240" y1="656" y2="656" x1="2208" />
        </branch>
        <iomarker fontsize="28" x="2208" y="656" name="Start" orien="R180" />
        <branch name="flash">
            <wire x2="944" y1="864" y2="864" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="864" name="flash" orien="R180" />
        <branch name="Hexs(31:0)">
            <wire x2="944" y1="928" y2="928" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="928" name="Hexs(31:0)" orien="R180" />
        <branch name="points(7:0)">
            <wire x2="944" y1="992" y2="992" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="992" name="points(7:0)" orien="R180" />
        <branch name="LES(7:0)">
            <wire x2="944" y1="1056" y2="1056" x1="912" />
        </branch>
        <iomarker fontsize="28" x="912" y="1056" name="LES(7:0)" orien="R180" />
        <branch name="Hexs(31:0),Hexs(31:0)">
            <attrtext style="alignment:SOFT-RIGHT;fontsize:28;fontname:Arial" attrname="Name" x="784" y="1472" type="branch" />
            <wire x2="1008" y1="1472" y2="1472" x1="784" />
        </branch>
        <branch name="seg_clm">
            <wire x2="2656" y1="592" y2="592" x1="2624" />
        </branch>
        <iomarker fontsize="28" x="2656" y="592" name="seg_clm" orien="R0" />
        <branch name="seg_clk">
            <wire x2="2656" y1="528" y2="528" x1="2624" />
        </branch>
        <iomarker fontsize="28" x="2656" y="528" name="seg_clk" orien="R0" />
        <branch name="seg_sout">
            <wire x2="2656" y1="656" y2="656" x1="2624" />
        </branch>
        <iomarker fontsize="28" x="2656" y="656" name="seg_sout" orien="R0" />
        <branch name="SEG_PEN">
            <wire x2="2656" y1="720" y2="720" x1="2624" />
        </branch>
        <iomarker fontsize="28" x="2656" y="720" name="SEG_PEN" orien="R0" />
        <branch name="XLXN_14(63:0)">
            <wire x2="2160" y1="1056" y2="1056" x1="2128" />
            <wire x2="2160" y1="720" y2="1008" x1="2160" />
            <wire x2="2160" y1="1008" y2="1056" x1="2160" />
            <wire x2="2240" y1="720" y2="720" x1="2160" />
        </branch>
        <branch name="XLXN_15(63:0)">
            <wire x2="1392" y1="864" y2="864" x1="1376" />
            <wire x2="1392" y1="864" y2="1056" x1="1392" />
            <wire x2="1744" y1="1056" y2="1056" x1="1392" />
        </branch>
        <branch name="XLXN_16(63:0)">
            <wire x2="1504" y1="1472" y2="1472" x1="1488" />
            <wire x2="1744" y1="1120" y2="1120" x1="1504" />
            <wire x2="1504" y1="1120" y2="1472" x1="1504" />
        </branch>
        <instance x="1008" y="1504" name="XLXI_5" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1680" y="1184" name="SW0" orien="R180" />
        <branch name="SW0">
            <wire x2="1744" y1="1184" y2="1184" x1="1680" />
        </branch>
        <instance x="1744" y="1216" name="XLXI_7" orien="R0">
        </instance>
    </sheet>
</drawing>