`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   02:46:20 04/09/2018
// Design Name:   Float_Add_32
// Module Name:   D:/2017-2018 Spr-Sum/Computer Organization/FLU/add_test.v
// Project Name:  FLU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Float_Add_32
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module add_test;

	// Inputs
	reg clk;
	reg [31:0] A;
	reg [31:0] B;
   
	// Outputs
	wire [31:0] res;

	// Instantiate the Unit Under Test (UUT)
	Float_Add_32 uut (
		.clk(clk), 
		.A(A), 
		.B(B),
      	
		.res(res)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		A = 0;
		B = 0;
		
      fork 
      forever #5 clk = ~clk;  
		begin
		//A = 32'h3fe00000;
		//B = 32'h3fe00000;
		#500;
		A = 32'hc0e00000;
		B = 32'hbf500000;
		#1000;
		end
		join
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

