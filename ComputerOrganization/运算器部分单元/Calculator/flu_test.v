`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   09:10:35 04/09/2018
// Design Name:   FLU_32
// Module Name:   D:/2017-2018 Spr-Sum/Computer Organization/FLU/flu_test.v
// Project Name:  FLU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FLU_32
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module flu_test;

	// Inputs
	reg clk;
	reg [2:0] sw;
	reg [31:0] A;
	reg [31:0] B;

	// Outputs
	wire [31:0] res;

	// Instantiate the Unit Under Test (UUT)
	FLU_32 uut (
		.clk(clk), 
		.sw(sw), 
		.A(A), 
		.B(B), 
		.res(res)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		sw = 0;
		A = 32'hc0e00000;
		B = 32'hbf500000;
      fork 
      forever #5 clk = ~clk;  
		begin
		//A = 32'h3fe00000;
		//B = 32'h3fe00000;
		#500;
      sw = 1;
		#1000;
		sw = 2;
		#500;
		sw = 3;
		#500 ;
		sw =4;
		#500;
		end
		join
		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

