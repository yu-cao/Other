#include <stdio.h>
#include <stdlib.h>

#define MAXN 10

typedef struct TreeNode *Tree;
struct TreeNode {
    int Element;
    Tree  Left;
    Tree  Right;
};

Tree BuildTree( int inorder[], int postorder[], int N );

int main()
{
    Tree T;
    int inorder[MAXN], postorder[MAXN], N, i;

    scanf("%d", &N);
    for (i=0; i<N; i++) scanf("%d", &inorder[i]);
    for (i=0; i<N; i++) scanf("%d", &postorder[i]);
    T = BuildTree(inorder, postorder, N);
    printf("Check:\n");

    return 0;
}

/* Your function will be put here */
Tree BuildTree( int inorder[], int postorder[], int N )
{
    if(N==0) return NULL;
    Tree T = (Tree)malloc(sizeof(struct TreeNode));

    int i=0;
    for(i=0;i<N;i++)
        if(inorder[i]==postorder[N-1])
            break;
    T->Element=inorder[i];
    T->Left=BuildTree(inorder,postorder,i);
    T->Right=BuildTree(&inorder[i+1],&postorder[i],N-i-1);
    return T;
}