#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define BLACK 0
#define RED 1

struct Node {
    int color;
    int key;
    struct Node* left;
    struct Node* right;
};

struct Node* CreateNode(int k);
struct Node* CreateTree(struct Node* T, int k);
void JudgeTree(struct Node* T, int count, const int first_count);
static int flag = 1;//1: It is a Red-Black Tree    0：NOT a Red-Black Tree

//Firstly build a BST
//Secondly judge whether it is a Red-Black Tree
int main(void)
{
    int M;//The repeat number of Tree test
    scanf("%d", &M);

    int re;
    for (re = 0; re < M; re++) {
        flag = 1;//Reset the flag before every judge

        int N;
        scanf("%d", &N);//get the number of whole node in a tree

        //The program of building a tree
        int input_key;
        scanf("%d", &input_key);
        struct Node* T = CreateNode(input_key);//get the first input number to build the root of tree

        int i;
        for (i = 1; i < N; i++) {
            //Note:i=0 has been used in creating the root
            scanf("%d", &input_key);
            T = CreateTree(T, input_key);
        }

        //The program of judge
        int first_count = 0;//Be used to count the black node number and as a standard to be compare
        int count = 0;//A parameter is used to pass into the Judge function

        if (T->color == RED) {
            //check the rule 2: The root is black
            printf("No\n");
            continue;
        }
        else {
            //Firstly, calculate the black nodes in the paths from root to the node of the most left leaf
            //Nest, compare with the other black node number in the other path
            struct Node* p = T;
            for (; p != NULL;) {
                if (p->color == BLACK) {
                    first_count++;
                }
                p = p->left;//go to leaf on the left path all the way
            }

            JudgeTree(T, count, first_count);

            //The program of output
            if (flag == 1) {
                printf("Yes\n");
            }
            else {
                printf("No\n");
            }
        }

    }
    system("pause");
    return 0;
}

//Build and initialize a node
struct Node* CreateNode(int k)
{
    struct Node* new_node=(struct Node*)malloc(sizeof(struct Node));
    if (k > 0) {
        //When input number > 0, initialize color to be black
        new_node->color = BLACK;
        new_node->key = k;
    }
    else {
        //When input number < 0, initialize color to be red
        new_node->color = RED;
        new_node->key = (-k);//change the number become a positive number
    }
    new_node->left = new_node->right = NULL;

    return new_node;
}

//Build a tree and return the root
struct Node* CreateTree(struct Node* T, int k)
{
    struct Node* new_node = T;
    if (T == NULL) {
        //If the node is not created, build it
        struct Node* new_node_1 = CreateNode(k);
        return new_node_1;
    }
    else {
        //by using function of "abs()" to ignore the sign of number and create the tree in recusive way
        if (abs(k) < new_node->key) {
            //To create the left sub-tree in recusive way
            new_node->left=CreateTree(T->left, k);
        }if (abs(k) > new_node->key) {
            //To create the right sub-tree in recusive way
            new_node->right =CreateTree(T->right, k);
        }
    }
    return new_node;
}

//Judge whether it is a Red-Black Tree
void JudgeTree(struct Node *T, int count, const int first_count)
{
    struct Node* p=T;
    int memory;
    if (p != NULL) {
        if (p->color == RED) {
            //When the node's color is red, its children only have two choice
            //One: its two children must be black(rule 4)
            //The other: it has no child
            //Other situation: NOT a red-black tree
            if (p->left != NULL&&p->right != NULL) {
                if (p->left->color != BLACK || p->right->color != BLACK) {
                    flag = 0;
                }
            }
            else if ((p->left != NULL&&p->right == NULL) || (p->left == NULL&&p->right != NULL)) {
                flag = 0;
            }
            //Judge the sub-tree in recusive way
            JudgeTree(p->left, count, first_count);
            JudgeTree(p->right, count, first_count);
        }
        if (p->color == BLACK) {
            count++;//Count the number of black node
            //Judge the sub-tree in recusive way
            JudgeTree(p->left, count, first_count);
            JudgeTree(p->right, count, first_count);
        }
    }
    else {
        //When the node is NULL, count the number of black node which has been pass and begin to judge
        if (count != first_count) {
            flag = 0;//When it violate the rule 5
        }
    }

}