#include<stdio.h>
#include<time.h>

#define TEST_NUMBER 100
/*Every time change the N just by changing the define*/

clock_t start, stop;
double duration;
double ticks;

int iterative_binary_search(int a[], int n);
int recursive_binary_search(int a[], int n, int min, int max);
int iterative_sequential_search(int a[], int n);
int recursive_sequential_search(int a[], int n, int k);
 
int main(void)
{
	int p = 2;
	size_t i; /*In case at the loop, the value of i out of max number of Type"int" */
	int a[TEST_NUMBER];
	for (i = 0; i < TEST_NUMBER; i++) {
		a[i] = i;
	}

	start = clock();/*Begin to count time*/
	
	/*YOU FUNCTION*/
	for (i = 0; i < 10000000; i++) {    /*repeat to decrease the error to the real*/
	/*When you want to test another function, only just change the comment*/
		p = iterative_binary_search(a, TEST_NUMBER);
		//p=recursive_binary_search(a, TEST_NUMBER,0,TEST_NUMBER-1);
		//p=iterative_sequential_search(a,TEST_NUMBER);
		//p=recursive_sequential_search(a,TEST_NUMBER,TEST_NUMBER-1);
	}
	
	stop = clock();/*Stop countting time*/

	ticks = stop - start;
	duration = ((double)(stop - start)) / CLK_TCK;

	printf("%f\n", ticks);
	printf("%f", duration);

	return 1;
}


int iterative_binary_search(int a[], int n)
/*n:the number of element in a[] && to just check the function performance, n also use to be a target number*/
{
	int min, max, mid;
	min = 0;
	max = n - 1;               /*give min & max a initial value*/
	while (max >= min)        
	{
		mid = (min + max) / 2; /*In the beginning of each loop must initialize the value of mid*/
		if (a[mid]<n)          /*Search the right part of a[mid]*/  
			min = mid + 1;
		else if (a[mid]>n)
			max = mid - 1;   /*Search the left part of a[mid]*/
		else return 1;       /*Find the target*/
	}
	return 0;                /*Not find*/
}


int recursive_binary_search(int a[], int n, int min, int max)   /*firstly,min=0,max=n-1*/
/*n:the number of element in a[] && to just check the function performance, n also use to be a target number*/
{
	int mid;
	mid = (min + max) / 2;
	if (min != max) {       /*go to binary search firstly*/
		if (a[mid]>n)       /*if target number:n is less than value of a[mid], use mid replace the former max, then do recursion*/
			return recursive_binary_search(a, n, min, mid);  
		else if (a[mid]<n)  /*by the same logic of previous comment, if n is more than a[mid], mid should replace the min*/
			return recursive_binary_search(a, n, mid + 1, max);
		else return 0;
	}
	else if (min == max&&min == n) /*if min, max, n are equal, means target has been found in array*/
		return 1;
	else return 0;          /*Not find*/
}


int iterative_sequential_search(int a[], int n)
/*n:the number of element in a[] && to just check the function performance, n also use to be a target number*/
{
	int i;
	for (i = 0; i <= n - 1; i++) {
		if (a[i] == n)
		{
			return 1;
			break; /*if find the target, break the loop*/
		}
	}
	return 0;      /*search the all array and not find*/ 
}


int recursive_sequential_search(int a[], int n, int k)/*k should input n-1, means the max index of a[]*/
/*n:the number of element in a[] && to just check the function performance, n also use to be a target number*/
{
	if (k == -1) return 0; /*this is flag of end of recursion when k is subtract to 0*/
	else {
		if (a[k] != n) {  /*if not find the target,use k-1 replace k,check the a[k-1] whether equal to target by recursion  */
			return recursive_sequential_search(a, n, k - 1);
		}
		else {
			return 1;     /*find the target in the array*/
		}
	}
}

