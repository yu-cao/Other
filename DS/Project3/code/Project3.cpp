/*
#Before you run the program, you'd better read the Readme.txt firstly.
*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define INF 10000

struct Vertex* findRoot(int x, struct Vertex* S);
void setUnion(struct Vertex* S, struct Edge edge, int c);
int getMinH(struct Vertex* S, int x1, int x2);
void Print(struct Vertex *S, int Nv);

struct Edge {
	int v1;
	int v2;
	int weight;
};

struct Vertex {
	int root;//A positive number represents a node, and a negative number represents the number of vertices of the set
	int H_set;
	int index;
};

int main(void)
{
	int Nv, Ne, c;
	scanf("%d %d %d", &Nv, &Ne, &c);

	int i, j;
	//The total number of vertices and H values of the entire set is only stored by the root of the set, 
	//and the remaining vertices are stored in the previous vertex, the H value which isn't in the root don't care parameter.
	struct Edge *a = (struct Edge*)malloc(sizeof(struct Edge)*Ne);
	int *visit = (int*)malloc(sizeof(int)*Ne);
	struct Vertex *S = (struct Vertex*)malloc(sizeof(struct Vertex)*Nv);

	//Set the visit array to 0
	for (i = 0; i < Ne; i++) {
		visit[i] = 0;
	}

	//Initialize the structure array b
	for (i = 0; i < Nv; i++) {
		S[i].H_set = c;
		S[i].root = -1;
		S[i].index = i;
	}

	//Get the Edge from the outside
	for (i = 0; i < Ne; i++) {
		scanf("%d %d %d", &a[i].v1, &a[i].v2, &a[i].weight);
	}

	//It can Optimize to be priority queues, minimum heap
	for (i = 0; i < Ne; i++) {
		//From small to big search each side, find the smallest edge
		int min = INF, min_index = 0;
		for (j = 0; j < Ne; j++) {
			if (min > a[j].weight&&visit[j] != 1) {
				min_index = j;
				min = a[j].weight;
			}
		}
		visit[min_index] = 1;

		//D is just the side that is small to large,and it is a[min_index].weight
		int D = a[min_index].weight;
		if (D <= getMinH(S, a[min_index].v1, a[min_index].v2)) {
			//If D is not larger the min{H1,H2}, unit the two component.
			setUnion(S, a[min_index], c);
		}
	}

	//Print the answer as project order
	Print(S, Nv);
	
	system("pause");
	return 0;
}

//Statement: the following findSet reference textbook P268 Figure8.9
//I'm going to go back and find the root of the set, and I'm going to give you the number of vertices and H of the whole set
//It's a whole set and a subindex index, which tracks its ancestral roots
struct Vertex* findRoot(int x, struct Vertex* S)
{
	if (S[x].root <= 0) {
		return &S[x];
	}
	else
		return findRoot(S[x].root, S);
}

//Give two vertices that are contained within each set
//Using the v1, v2, and weight carried by the incoming edge, the merging of two sets in it, the refresh of H
//The merger is of size-of-union
void setUnion(struct Vertex* S, struct Edge edge, int c)
{
	struct Vertex* v1, *v2;
	v1 = findRoot(edge.v1, S);
	v2 = findRoot(edge.v2, S);

	//The two vertices to connect are already in the same section
	if (v1->index == v2->index) {
		return;
	}

	//So 1 is the more larger than 2 of the tree-size (Note that it's obviously a negative number here)
	if (v1->root < v2->root) {
		v1->root += v2->root;
		v2->root = v1->index;
		v1->H_set = edge.weight + c * 1.0 / abs(v1->root);
	}
	else {
		v2->root += v1->root;
		v1->root = v2->index;
		v2->H_set = edge.weight + c * 1.0 / abs(v2->root);
	}
}

// Get the minimal H of the two component
int getMinH(struct Vertex* S, int x1, int x2)
{
	struct Vertex *v1, *v2;
	v1 = findRoot(x1, S);
	v2 = findRoot(x2, S);
	if (v1->H_set > v2->H_set) {
		return v2->H_set;
	}
	else {
		return v1->H_set;
	}
}

//Print as the project order
void Print(struct Vertex *S,int Nv)
{
	
	int i,j;
	int *PrintVisit = (int*)malloc(sizeof(int)*Nv);//To remember whether this vertex has been printed or not 

	//If flag==0 to show that it is the first number printed in one component 
	//and flag==1 to show it is being printed the other vertex of this component
	int flag = 0;  
	struct Vertex* SetRoot = NULL;

	//Initialize the visit[]
	for (i = 0; i < Nv; i++) {
		PrintVisit[i] = 0;
	}

	for (;;) {
		flag = 0;
		for (i = 0; i < Nv; i++) {
			if (PrintVisit[i] != 1 && flag == 0) {
				printf("%d", i);
				flag = 1;
				SetRoot = findRoot(i, S);
				PrintVisit[i] = 1;
			}
			else if (flag == 1 && SetRoot == findRoot(i, S)) {
				printf(" %d", i);
				PrintVisit[i] = 1;
			}
		}
		printf("\n");

		//To check whether the whole vertexes have been printed
		for (j = 0; j < Nv; j++) {
			if (PrintVisit[j] == 0) break;
		}

		if (j == Nv) break;
	}
}