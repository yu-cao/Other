/*Author:曹煜 ID:3160100440 No.01 */

//通过输入的中序遍历与后序遍历的数据，先把整个Tree构建出来
//再通过ZigZagging输出（利用广度优先搜索方法）
#include<stdio.h>
#include<stdlib.h>

struct Node {
	int Element;
	struct Node* left;
	struct Node* right;
};

struct Node* create_root(int Element);
struct Node** level_out(struct Node* T);
struct Node* create_tree(struct Node* T, int a[], int b[], int rear_a, int front_a, int rear_b, int front_b);

int main(void)
{
	int i,N;
	scanf("%d", &N);
	//读入的数据个数
	int a[30], b[30];

	//读入两个遍历并储存在数组内
	for (i = 0; i < N; i++) {
		scanf("%d", &a[i]);
	}
	for (i = 0; i < N; i++) {
		scanf("%d", &b[i]);
	}
	
	struct Node* p = create_tree(NULL,a,b,0,N-1,0,N-1);//完成树的构建

	struct Node** q;
	q=level_out(p);
	printf("%d", q[0]->Element);
	for (i = 1; i < N; i++) {
		printf(" %d", q[i]->Element);
	}

}

//创建一个节点
struct Node* create_root(int Element)
{
	struct Node* new_node;

	new_node = (struct Node*)malloc(sizeof(struct Node));
	new_node->Element = Element;
	new_node->left = new_node->right = NULL;

	return new_node;
}

//建立一棵树
struct Node* create_tree(struct Node* T,int a[],int b[],int rear_a,int front_a,int rear_b,int front_b)
{
	struct Node* new_node = create_root(b[front_b]);
	int temp = rear_a;

	for (; a[temp] != b[front_b]; temp++);//找到中序遍历的根

	int left_tree = temp - rear_a;
	if (left_tree > 0) {
		new_node->left = create_tree(new_node->left, a, b, rear_a, temp - 1, rear_b, left_tree + rear_b - 1);
	}

	int right_tree = front_a - temp;
	if (right_tree > 0) {
		new_node->right = create_tree(new_node->right, a, b, temp + 1, front_a, rear_b + left_tree, front_b - 1);
	}
	
	return new_node;
}

//输出函数
struct Node** level_out(struct Node* T)
{
	static struct Node* queue[30];//静态本地变量大小必须唯一确定
	int temp;
	int front = 0, rear = 0, flag = -1;
	queue[0] = T;

	for (; rear <= front;) {
		flag = -flag;
		int num_in_queue = front - rear;
		if (flag == 1) {
			//先左后右
			temp = front;
			rear = temp + 1;
			for (; num_in_queue >= 0; num_in_queue--) {
				if (queue[temp]->left != NULL) {
					front++;
					queue[front] = queue[temp]->left;
				}
				if (queue[temp]->right != NULL) {
					front++;
					queue[front] = queue[temp]->right;
				}
				temp--;
			}
		}
		else if (flag == -1) {
			//先右后左
			temp = front;
			rear = temp + 1;
			for (; num_in_queue >= 0; num_in_queue--) {
				if (queue[temp]->right != NULL) {
					front++;
					queue[front] = queue[temp]->right;
				}
				if (queue[temp]->left != NULL) {
					front++;
					queue[front] = queue[temp]->left;
				}
				temp--;
			}
		}

	}
	return queue;
}