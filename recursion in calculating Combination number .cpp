//计算组合数kC(n+1)=(k-1)Cn+kCn
#include<iostream>
int count(int k,int n)
{
    if(k==0) return 1;
    if(n==k) return 1;
    return count(k-1,n-1)+count(k,n-1);
}

int main(void)
{
    int check = count(5,10);
    std::cout<<check;
}