#include<iostream>
#define KEYLEN 3
#define THISKEY 0//0 1 2依次尝试，代表检测密码第1 2 3个字符频度
int main()
{
	const char *s = "ktbueluegvitnthuexmonveggmrcgxptlyhhjaogchoemqchpdnetxupbqntietiabpsmaoncnwvoutiugtagmmqsxtvxaoniiogtagmbpsmtuvvihpstpdvcrxhokvhxotawswquunewcgxptlcrxtevtubvewcnwwsxfsnptswtagakvoyyak";
	const int s_length = strlen(s);

	int count[26] = { 0 };

	for (int i = 0; i < s_length; i++)
		if (i%KEYLEN == THISKEY) count[s[i] - 'a']++;

	for (int i = 0; i < 26; i++)
		printf("%c ", (i + 'a'));
	printf("\n");

	for (int i : count)
		printf("%d ", i);
	return 0;
}
