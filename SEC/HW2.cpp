#include<iostream>
#include<string>
using namespace std;
string add_zero(string a, int zeroNum);
string multiply_1to1(char a, char b);
class test {
	string s;
public:
	friend ostream & operator<<(ostream &os, const test &ans);
	test(string ch) { s = ch; }
	test(){}
	string get_s() { return s; }
	test operator = (const test &num) {
		s = num.s;
		return *this;
	}
	friend test operator+(const test & a, const test & b);
	friend test operator-(const test & a, const test & b);
	friend test operator*(const test & a, const test & b);
	friend test operator/(const test & a, const test & b);
	friend test operator%(const test & a, const test & b);
};
test myPow(test x, test e);

//不可以输入负数
test operator+(const test & a, const test & b)
{
	test c, d;
	//如果a的位数比b的短，将结果先给b
	if (a.s.size() < b.s.size()) {
		c = b;
		d = a;
	}
	else {
		c = a;
		d = b;
	}

	int length_small = d.s.size();//较短的那个数的长度
	int length_c = c.s.size();//较长的那个数的长度

	int now_small;
	int sum;//在某一位上的加法结果
	int co = 0;//进位
	while (length_c > 0) {
		int now_c = c.s[length_c - 1] - '0';//较长数的原来位置的值a
		if (length_small > 0)
		{
			now_small = d.s[length_small - 1] - '0';
		}
		else
		{
			now_small = 0;
		}
		sum = now_c + now_small + co;
		if (sum >= 10)//计算进位
		{
			c.s[length_c - 1] = '0' + sum - 10;
			co = 1;
		}
		else
		{
			c.s[length_c - 1] = '0' + sum;
			co = 0;
		}
		length_c--;
		length_small--;
	}
	//特殊情况：eg.9+1=10需要额外位
	if (co == 1)
		c.s = "1" + c.s;//小心，不可以c.s+="1"直接加在了最后...
	return c;
}

//不可以输入负数，但是答案可以是负数
test operator-(const test & a, const test & b)
{
	int flag = 0;//判断结果是不是负数
	test c(a);//大的
	test d(b);//小的
	if (a.s.size() < b.s.size())
	{
		flag = 1;
		c = b;
		d = a;
	}
	else if (a.s.size() > b.s.size()) {}
	else
	{
		if (a.s < b.s)
		{
			flag = 1;
			c = b;
			d = a;
		}
		else if (a.s == b.s)
		{
			c.s = "0";
			return c;
		}
	}

	//与加法相似，从最低位算起
	int length_c = c.s.size(), length_d = d.s.size();
	int now_c, now_d;
	int borrow = 0;//向高位借位的指示符
	int ans;//当前位的计算结果
	while (length_c > 0)
	{
		now_c = c.s[length_c - 1] - '0';
		if (length_d > 0)
		{
			now_d = d.s[length_d - 1] - '0';
		}
		else
		{
			now_d = 0;
		}

		ans = now_c - now_d - borrow;
		if (ans < 0)
		{
			c.s[length_c - 1] = '0' + ans + 10;
			borrow = 1;
		}
		else
		{
			c.s[length_c - 1] = '0' + ans;
			borrow = 0;
		}
		length_c--;
		length_d--;
	}

	//异常处理，eg.100-99=1
	int i;
	for (i = 0; c.s[i] == '0'; i++);
	test fin;
	for (; i < c.s.size(); i++)
	{
		fin.s += c.s[i];
	}

	if (flag == 1)//结果是负数
	{
		fin.s = "-" + fin.s;
	}
	return fin;
}

//不可以输入负数
test operator*(const test & a, const test & b)
{
	string ch;
	//如果乘数或被乘数为0，答案显然为0
	if (a.s == "0" || b.s == "0") {
		test c("0");
		return c;
	}
	test c(a), d(b);
	test ans("0");

	//用普通乘法的方式进行遍历计算再按照10的次数相加
	for (int i = 0; i < c.s.size(); i++)
	{
		for (int j = 0; j < d.s.size(); j++)
		{
			ch = multiply_1to1(c.s[c.s.size() - i - 1], d.s[d.s.size() - j - 1]);
			ch = add_zero(ch, i + j);
			ans = ans + ch;
		}
	}
	return ans;
}

//不可以输入负数
test operator/(const test & a, const test & b)
{
	test c(a), d(b);
	if (b.s == "0")
	{
		cout << "ERROR! Divisor equal ZERO!" << endl;
		exit(1);
	}
	//现在答案中填充0占位，规定数值位数小于1000（可修改）
	test ans;
	for (int i = 0; i < 1000; i++)
	{
		ans.s += "0";
	}
	int length_c = c.s.size(), length_d = d.s.size();

	//当a<b时，答案必然是0
	if (a.s.size() < b.s.size())
	{
		c.s = "0";
		return c;
	}
	if (a.s.size() == b.s.size())
	{
		if (a.s < b.s)
		{
			c.s = "0";
			return c;
		}
		if (a.s == b.s)
		{
			c.s = "1";
			return c;
		}
	}

	int delta = length_c - length_d;
	for (int i = 0; i < delta; i++)
	{
		d.s += "0";
	}

	//用减法的方式对每一位进行不断减法
	while (delta >= 0)
	{
		int sum = 0;
		test temp;
		string negative = "-";
		while (true)
		{
			temp = c - d;
			if (temp.s[0] == negative[0]) break;
			sum++;
			c = temp;
		}
		ans.s[ans.s.size() - delta - 1] = sum + '0';
		delta--;
		d.s = d.s.substr(0, length_d + delta);
	}

	//处理到在前置位的0
	string check_zero = "0";
	int i;
	for (i = 0;; i++)
		if (ans.s[i] != check_zero[0]) break;
	test fin;
	for (; i < ans.s.size(); i++)
		fin.s += ans.s[i];
	return fin;
}

//不可以输入负数
test operator%(const test & a, const test & b)
{
	test c = a / b;
	test ans = a - (c*b);
	return ans;
}

//向后填0，即*10
string add_zero(string a, int zero_num) {
	string temp(zero_num, '0');
	a.append(temp);
	return a;
}

//1位乘1位的乘法
string multiply_1to1(char a,char b)
{
	if (a == '0' || b == '0') return "0";
	char *s=(char*)malloc(sizeof(char)*3);
	int ans = (a - '0')*(b - '0');
	itoa(ans, s, 10);
	string ch;
	ch += s;
	return ch;
}

//输出结果
ostream & operator<<(ostream &os, const test &ans)
{
	os << ans.s;
	return os;
}

//这个部分是测试+-*/使用的
//int main()
//{
//	string s1;
//	string s2;
//	cout << "Input:";
//	cin >> s1 >> s2;
//	/* eg.
//	 * 98765432100123456789987654321001234567899876543210 and
//	 * 12345678901234567890123456789012345678901234567890
//	 */
//	test a(s1), b(s2), c;
//	c = a + b;
//	cout << "Add Answer:" << c << endl;
//
//	c = a - b;
//	cout << "Sub Answer:" << c << endl;
//
//	c = a * b;
//	cout << "Mul Answer:" << c << endl;
//
//	c = a / b;
//	cout << "Div Answer:" << c << endl;
//
//	c = a % b;
//	cout << "Mod Answer:" << c << endl;
//}

//这个模块是进行Diffie–Hellman key exchange
//Xa与Xb是保密的，此处的case是与上课举例的ppt相同
int main()
{
	test prime("353");
	test Xa("97");
	test Xb("233");

	test g("3");
	test Yb;//Yb=(g^Xa)%prime
	test Ya;//Ya=(g^Xb)%prime

	//生成的秘钥
	test KeyA, KeyB;

	//计算(g^Xa)%p
	Yb = myPow(g, Xa);
	Yb = Yb % prime;
	cout << "The Yb which a send to b:" << Yb << endl;

	//计算(g^Xb)%p
	Ya = myPow(g, Xb);
	Ya = Ya % prime;
	cout<< "The Ya which b send to a:" << Ya << endl;

	//计算((g^Xa)%p)^Xb%p
	KeyB = myPow(Yb, Xb);
	KeyB = KeyB % prime;
	cout << "The key in B:" << KeyB << endl;

	//计算((g^Xb)%p)^Xa%p
	KeyA = myPow(Ya, Xa);
	KeyA = KeyA % prime;
	cout << "The key in A:" << KeyA << endl;

	//验证两个产生的秘钥是否相同
	if(KeyA.get_s()==KeyB.get_s())
		cout << "Two Key are equal!!" << endl;//dh算法成功
	else
		cout << "Error! Two Key are not equal!!" << endl;//dh算法失败
}


//进行快速幂演算
test myPow(test x, test e)
{
	test two("2");
	test ans("1");//返回的结果 
	test a[20000];//换算进入二进制
	int i = 0;
	do {
		a[i] = e % two;
		i++;
		e = e / two;
	} while (e.get_s() != "0");

	i--;
	for (; i >= 0; i--) {
		if (a[i].get_s() == "1")
			ans = ans * ans * x;
		else
			ans = ans * ans;
	}
	return ans;
}